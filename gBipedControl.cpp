//
// You received this file as part of Finroc
// A framework for intelligent robot control
//
// Copyright (C) Finroc GbR (finroc.org)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//
//----------------------------------------------------------------------
/*!\file    projects/biped/control/gBipedControl.cpp
 *
 * \author  Tobias Luksch and Jie Zhao
 *
 * \date    2014-01-13
 *
 */
//----------------------------------------------------------------------
#include "projects/biped/control/gBipedControl.h"
#include "projects/biped/physics_engine_simulation/gPhysicsEngineNewton.h"
#include "projects/biped/control/gAbstractionLayer.h"
#include "projects/biped/control/gControl.h"
#include "projects/biped/utils/mbbExperiments.h"
#include "projects/biped/skills/mMaxFilter.h"
#include "projects/biped/utils/mbbStartSimulation.h"
#include "plugins/scheduling/tThreadContainerElement.h"
#include "projects/biped/utils/mConvertSingleToVector.h"

//#include "libraries/sim_vis_3d/utils/mSwitchWrapper.h"
#include "rrlib/util/sFileIOUtils.h"


using namespace rrlib::util;
using namespace finroc::biped::utils;
using namespace finroc::biped::skills;
using namespace finroc::biped::physics_engine_simulation;
using namespace finroc::ib2c;
//----------------------------------------------------------------------
// External includes (system with <>, local with "")
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Internal includes with ""
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Debugging
//----------------------------------------------------------------------
#include <cassert>

//----------------------------------------------------------------------
// Namespace usage
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Namespace declaration
//----------------------------------------------------------------------
namespace finroc
{
namespace biped
{
namespace control
{

//----------------------------------------------------------------------
// Forward declarations / typedefs / enums
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Const values
//----------------------------------------------------------------------
static runtime_construction::tStandardCreateModuleAction<gBipedControl> cCREATE_ACTION_FOR_G_BIPEDCONTROL("BipedControl");

//----------------------------------------------------------------------
// Implementation
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// gBipedControl constructor
//----------------------------------------------------------------------
gBipedControl::gBipedControl(core::tFrameworkElement *parent, const std::string &name,
                             const std::string &structure_config_file) :
  tSenseControlGroup(parent, name, structure_config_file, true),// change to 'true' to make group's ports shared (so that ports in other processes can connect to its sensor outputs and controller inputs)
  simulation_group(NULL),
  hal(NULL),
  control(NULL)
{
  //  engine_newton = new tEngineNewton();
  //scheduling::tThreadContainerElement<gPhysicsEngineNewton>* simulation_group = new scheduling::tThreadContainerElement<gPhysicsEngineNewton>(this);
  this->simulation_group = new gPhysicsEngineNewton(this, "Physics Engine Newton", "resources/scenes/biped_with_plate_and_ball.descr");
  //this->simulation_group->SetCycleTime(std::chrono::microseconds(25));

  this->hal = new gAbstractionLayer(this, "Abtracion Layer");
  this->control = new gControl(this, "Control Group");

  // pause simulation firstly
  //mbbStartSimulation* start_simulation_signal = new mbbStartSimulation(this, "Start Simulation", ib2c::tStimulationMode::ENABLED);

  // experiments
  mbbExperiments* experiments = new mbbExperiments(this, "Experiments", tStimulationMode::ENABLED, 0);

  mMaxFilter* fusion_sensor_monitor = new mMaxFilter(this, "(F) Sensor Monitor Fusion", 2, 1);
  mMaxFilter* fusion_walking_init = new mMaxFilter(this, "(F) Walking Initiation", 2, 2);
  mMaxFilter* fusion_walking_termi = new mMaxFilter(this, "(F) Walking Termination", 2, 2);

  // convertor
// mConvertSingleToVector* convert_signle_to_vector = new mConvertSingleToVector(this, "Convert Single to Vector");


  // Sensor Monitor
  experiments->co_sensor_monitor.ConnectTo(fusion_sensor_monitor->ci_signal[0]);
  this->ci_sensor_monitor.ConnectTo(fusion_sensor_monitor->ci_signal[1]);

//  this->ci_switch_off_gravity.ConnectTo(this->simulation_group->ci_switch_off_gravity); fixme if physics works in finroc
//  this->ci_progress_simulation.ConnectTo(this->simulation_group->ci_progress_simulation);
//  this->ci_start_simulation.ConnectTo(start_simulation_signal->ci_start_simulation);
//  this->ci_reset_simulation.ConnectTo(this->simulation_group->ci_reset_simulation);
//  start_simulation_signal->co_pause_simulation.ConnectTo(this->simulation_group->ci_pause_simulation);

  this->ci_enable_experiments.ConnectTo(experiments->ci_enabled_experiment); // 24 ports
  this->ci_experiment_type.ConnectTo(experiments->ci_experiment_type);
  this->ci_desired_duration.ConnectTo(experiments->ci_desired_duration);
  this->ci_sphere_movement_step_width.ConnectTo(experiments->ci_sphere_movement_step_width);
  this->ci_plate_movement_step_width.ConnectTo(experiments->ci_plate_movement_step_width);
  this->ci_plate_rotation_step_width.ConnectTo(experiments->ci_plate_rotation_step_width);
  this->ci_head_force_x.ConnectTo(experiments->ci_head_force_x);
  this->ci_head_force_y.ConnectTo(experiments->ci_head_force_y);
  this->ci_head_force_z.ConnectTo(experiments->ci_head_force_z);
  this->ci_torso_force_x.ConnectTo(experiments->ci_torso_force_x);
  this->ci_torso_force_y.ConnectTo(experiments->ci_torso_force_y);
  this->ci_torso_force_z.ConnectTo(experiments->ci_torso_force_z);
  this->ci_pelvis_force_x.ConnectTo(experiments->ci_pelvis_force_x);
  this->ci_pelvis_force_y.ConnectTo(experiments->ci_pelvis_force_y);
  this->ci_pelvis_force_z.ConnectTo(experiments->ci_pelvis_force_z);
  this->ci_sphere_pose_x.ConnectTo(experiments->ci_sphere_pos_x);
  this->ci_sphere_pose_y.ConnectTo(experiments->ci_sphere_pos_y);
  this->ci_sphere_pose_z.ConnectTo(experiments->ci_sphere_pos_z);
  this->ci_plate_pos_x.ConnectTo(experiments->ci_plate_pos_x);
  this->ci_plate_pos_y.ConnectTo(experiments->ci_plate_pos_y);
  this->ci_plate_pos_z.ConnectTo(experiments->ci_plate_pos_z);
  this->ci_plate_rot_x.ConnectTo(experiments->ci_plate_rot_x);
  this->ci_plate_rot_y.ConnectTo(experiments->ci_plate_rot_y);
  this->ci_plate_rot_z.ConnectTo(experiments->ci_plate_rot_z);
  // experiments to physics engine

// experiments->GetControllerOutputs().ConnectByName(simulation_group->GetControllerInputs(), false); // 18 ports fixme
  // experiments to biped control
  experiments->so_run_time.ConnectTo(this->so_run_time);
  experiments->so_total_time.ConnectTo(this->so_total_time);
  experiments->so_max_vel.ConnectTo(this->so_max_vel);

  experiments->co_plate_pos.ConnectTo(this->simulation_group->ci_plate_pos);
  experiments->co_plate_rot.ConnectTo(this->simulation_group->ci_plate_rot);

  // simulation group to experiments
  //simulation_group->GetSensorOutputs()
  //simulation_group->GetSensorOutputs().ConnectTo(control->si_foot_left_position_x);
  //simulation_group->GetSensorOutputs().ConnectTo(control->si_foot_left_position_y);


  // control to abstraction layer
  this->control->GetControllerOutputs().ConnectByName(hal->GetControllerInputs(), false);
  // control to experiments
  this->control->so_stiffness_factor.ConnectTo(experiments->si_stiffness_factor);
  this->control->so_stand_state.ConnectTo(experiments->si_stand_state);
  this->control->so_ins_roll.ConnectTo(experiments->si_roll);
  this->control->so_ins_pitch.ConnectTo(experiments->si_pitch);
  this->control->so_left_foot_load.ConnectTo(experiments->si_left_foot_load);
  this->control->so_right_foot_load.ConnectTo(experiments->si_right_foot_load);

  this->control->so_left_foot_load.ConnectTo(this->so_left_footload);
  this->control->so_right_foot_load.ConnectTo(this->so_right_footload);
  this->control->so_left_force_deviation.ConnectTo(this->so_left_force_deviation);
  this->control->so_right_force_deviation.ConnectTo(this->so_right_force_deviation);
  this->control->so_left_ground_contact.ConnectTo(this->so_left_ground_contact);
  this->control->so_right_ground_contact.ConnectTo(this->so_right_ground_contact);

  this->control->so_new_running.ConnectTo(experiments->si_new_running);
  this->control->so_loop_times.ConnectTo(experiments->si_loop_times);

  this->ci_stimulation_walking.ConnectTo(fusion_walking_init->ci_signal[0]);
  this->ci_walking_speed.ConnectTo(fusion_walking_init->ci_signal[2]);

  experiments->co_stimulation_walking.ConnectTo(fusion_walking_init->ci_signal[1]);
  experiments->co_walking_speed.ConnectTo(fusion_walking_init->ci_signal[3]);
  experiments->co_reset_simulation.ConnectTo(this->simulation_group->ci_reset_simulation_experiment);
  experiments->co_pelvis_force.ConnectTo(this->simulation_group->ci_pelvis_force);
  experiments->co_torso_force.ConnectTo(this->simulation_group->ci_torso_force);
  experiments->co_head_force.ConnectTo(this->simulation_group->ci_head_force);

  experiments->co_push_detected_y.ConnectTo(this->control->ci_push_y);
  experiments->co_push_detected_x.ConnectTo(this->control->ci_push_x);
  experiments->co_torso_force_y.ConnectTo(this->control->ci_torso_force_y);
  experiments->co_torso_force_x.ConnectTo(this->control->ci_torso_force_x);
  experiments->co_reset_simulation.ConnectTo(this->control->ci_reset_simulation_experiments);
  experiments->co_push_started.ConnectTo(this->control->ci_push_started);
  experiments->co_push_started_x.ConnectTo(this->control->ci_push_started_x);
  experiments->co_push_detected_x.ConnectTo(this->control->ci_push_detected_x);

  fusion_walking_init->co_signal[0].ConnectTo(this->control->ci_stimulation_walking);
  fusion_walking_init->co_signal[1].ConnectTo(this->control->ci_walking_speed);


  this->ci_walking_termination.ConnectTo(fusion_walking_termi->ci_signal[0]);
  this->ci_walking_speed.ConnectTo(fusion_walking_termi->ci_signal[2]);

  experiments->co_walking_termination.ConnectTo(fusion_walking_termi->ci_signal[1]);
  experiments->co_walking_speed.ConnectTo(fusion_walking_termi->ci_signal[3]);
  experiments->ci_slow_walking.ConnectTo(this->control->co_slow_walking_on);
  experiments->co_actual_speed.ConnectTo(this->control->co_actual_speed);

  fusion_walking_termi->co_signal[0].ConnectTo(this->control->ci_walking_termination);

  //simulation_group->GetSensorOutputs().ConnectByName(this->GetSensorOutputs(), false);


  this->ci_arrow_visulisation.ConnectTo(this->control->ci_arrow_visualisation);
  this->ci_walking_direction.ConnectTo(this->control->ci_walking_direction);
  this->ci_walking_laterally.ConnectTo(this->control->ci_walking_laterally);
  this->ci_head_force_x.ConnectTo(this->control->ci_head_force_x);
  this->ci_head_force_y.ConnectTo(this->control->ci_head_force_y);
  this->ci_torso_force_x.ConnectTo(this->control->ci_torso_force_x);
  //this->ci_torso_force_y.ConnectTo(this->control->ci_torso_force_y); fixme:commented during learning for push recovery

  this->hal->so_ins_accel_x.ConnectTo(this->control->si_ins_accel[0]);
  this->hal->so_ins_accel_y.ConnectTo(this->control->si_ins_accel[1]);
  this->hal->so_ins_accel_z.ConnectTo(this->control->si_ins_accel[2]);
  this->hal->so_ins_omega_x.ConnectTo(this->control->si_ins_omega[0]);
  this->hal->so_ins_omega_y.ConnectTo(this->control->si_ins_omega[1]);
  this->hal->so_ins_omega_z.ConnectTo(this->control->si_ins_omega[2]);
  this->hal->so_ins_roll.ConnectTo(this->control->si_ins_rot[0]);
  this->hal->so_ins_pitch.ConnectTo(this->control->si_ins_rot[1]);
  this->hal->so_ins_yaw.ConnectTo(this->control->si_ins_rot[2]);

  this->hal->so_left_ankle_x_torque.ConnectTo(this->control->si_left_ankle_x_torque);
  this->hal->so_left_ankle_x_angle.ConnectTo(this->control->si_left_ankle_x_angle);
  this->hal->so_left_ankle_x_torque.ConnectTo(this->control->si_left_ankle_x_torque);
  this->hal->so_left_ankle_y_angle.ConnectTo(this->control->si_left_ankle_y_angle);
  this->hal->so_left_ankle_y_torque.ConnectTo(this->control->si_left_ankle_y_torque);
  this->hal->so_left_knee_angle.ConnectTo(this->control->si_left_knee_angle);
  this->hal->so_left_knee_torque.ConnectTo(this->control->si_left_knee_torque);
  this->hal->so_left_hip_x_angle.ConnectTo(this->control->si_left_hip_x_angle);
  this->hal->so_left_hip_x_torque.ConnectTo(this->control->si_left_hip_x_torque);
  this->hal->so_left_hip_y_angle.ConnectTo(this->control->si_left_hip_y_angle);
  this->hal->so_left_hip_y_torque.ConnectTo(this->control->si_left_hip_y_torque);
  this->hal->so_left_hip_z_angle.ConnectTo(this->control->si_left_hip_z_angle);
  this->hal->so_left_hip_z_torque.ConnectTo(this->control->si_left_hip_z_torque);


//
  this->hal->so_spine_x_angle.ConnectTo(this->control->si_spine_x_angle);
  this->hal->so_spine_x_torque.ConnectTo(this->control->si_spine_x_torque);
  this->hal->so_spine_y_angle.ConnectTo(this->control->si_spine_y_angle);
  this->hal->so_spine_y_torque.ConnectTo(this->control->si_spine_y_torque);
  this->hal->so_spine_z_angle.ConnectTo(this->control->si_spine_z_angle);
  this->hal->so_spine_z_torque.ConnectTo(this->control->si_spine_z_torque);

  this->hal->so_left_shoulder_x_angle.ConnectTo(this->control->si_left_shoulder_x_angle);
  this->hal->so_left_shoulder_x_torque.ConnectTo(this->control->si_left_shoulder_x_torque);
  this->hal->so_left_shoulder_y_angle.ConnectTo(this->control->si_left_shoulder_y_angle);
  this->hal->so_left_shoulder_y_torque.ConnectTo(this->control->si_left_shoulder_y_torque);
  this->hal->so_left_elbow_angle.ConnectTo(this->control->si_left_elbow_angle);
  this->hal->so_left_elbow_torque.ConnectTo(this->control->si_left_elbow_torque);

  this->hal->so_right_ankle_x_torque.ConnectTo(this->control->si_right_ankle_x_torque);
  this->hal->so_right_ankle_x_angle.ConnectTo(this->control->si_right_ankle_x_angle);
  this->hal->so_right_ankle_x_torque.ConnectTo(this->control->si_right_ankle_x_torque);
  this->hal->so_right_ankle_y_angle.ConnectTo(this->control->si_right_ankle_y_angle);
  this->hal->so_right_ankle_y_torque.ConnectTo(this->control->si_right_ankle_y_torque);
  this->hal->so_right_knee_angle.ConnectTo(this->control->si_right_knee_angle);
  this->hal->so_right_knee_torque.ConnectTo(this->control->si_right_knee_torque);
//
  this->hal->so_right_hip_x_angle.ConnectTo(this->control->si_right_hip_x_angle);
  this->hal->so_right_hip_x_torque.ConnectTo(this->control->si_right_hip_x_torque);
  this->hal->so_right_hip_y_angle.ConnectTo(this->control->si_right_hip_y_angle);
  this->hal->so_right_hip_y_torque.ConnectTo(this->control->si_right_hip_y_torque);
  this->hal->so_right_hip_z_angle.ConnectTo(this->control->si_right_hip_z_angle);
  this->hal->so_right_hip_z_torque.ConnectTo(this->control->si_right_hip_z_torque);
//
  this->hal->so_right_shoulder_x_angle.ConnectTo(this->control->si_right_shoulder_x_angle);
  this->hal->so_right_shoulder_x_torque.ConnectTo(this->control->si_right_shoulder_x_torque);
  this->hal->so_right_shoulder_y_angle.ConnectTo(this->control->si_right_shoulder_y_angle);
  this->hal->so_right_shoulder_y_torque.ConnectTo(this->control->si_right_shoulder_y_torque);
  this->hal->so_right_elbow_angle.ConnectTo(this->control->si_right_elbow_angle);
  this->hal->so_right_elbow_torque.ConnectTo(this->control->si_right_elbow_torque);

  // connection for mca-legacy
  /*
  //this->hal->GetControllerOutputs().ConnectByName(this->GetControllerOutputs(), false);

  this->si_ins_accel_x.ConnectTo(this->hal->si_ins_accel_x);
  this->si_ins_accel_y.ConnectTo(this->hal->si_ins_accel_y);
  this->si_ins_accel_z.ConnectTo(this->hal->si_ins_accel_z);
  this->si_ins_omega_x.ConnectTo(this->hal->si_ins_omega_x);
  this->si_ins_omega_y.ConnectTo(this->hal->si_ins_omega_y);
  this->si_ins_omega_z.ConnectTo(this->hal->si_ins_omega_z);

  this->si_ins_roll.ConnectTo(this->hal->si_ins_roll);
  this->si_ins_pitch.ConnectTo(this->hal->si_ins_pitch);
  this->si_ins_yaw.ConnectTo(this->hal->si_ins_yaw);

  this->si_left_torque_x.ConnectTo(this->hal->si_left_torque_x);
  this->si_left_torque_y.ConnectTo(this->hal->si_left_torque_y);
  this->si_right_torque_x.ConnectTo(this->hal->si_right_torque_x);
  this->si_right_torque_y.ConnectTo(this->hal->si_right_torque_y);
  //

  this->hal->so_left_force_z.ConnectTo(this->so_left_force_z);
  this->hal->so_right_force_z.ConnectTo(this->so_right_force_z);
  // this->hal->GetSensorInputs().ConnectByName(this->GetSensorInputs(), false);
  * */

}

//----------------------------------------------------------------------
// gBipedControl destructor
//----------------------------------------------------------------------
gBipedControl::~gBipedControl()
{}

void gBipedControl::PostChildInit()
{
  simulation_group->GetSensorOutputs().ConnectByName(this->GetSensorOutputs(), true);



  this->control->co_left_hip_x_angle.ConnectTo(hal->ci_left_hip_x_angle);
  this->control->co_left_hip_y_angle.ConnectTo(hal->ci_left_hip_y_angle);
  this->control->co_left_hip_z_angle.ConnectTo(hal->ci_left_hip_z_angle);
  this->control->co_left_stimulation_hip_x_angle.ConnectTo(hal->ci_left_stimulation_hip_x_angle);
  this->control->co_left_stimulation_hip_y_angle.ConnectTo(hal->ci_left_stimulation_hip_y_angle);
  this->control->co_left_stimulation_hip_z_angle.ConnectTo(hal->ci_left_stimulation_hip_z_angle);
  this->control->co_left_hip_x_torque.ConnectTo(hal->ci_left_hip_x_torque);
  this->control->co_left_hip_y_torque.ConnectTo(hal->ci_left_hip_y_torque);
  this->control->co_left_hip_z_torque.ConnectTo(hal->ci_left_hip_z_torque);
  this->control->co_left_stimulation_hip_x_torque.ConnectTo(hal->ci_left_stimulation_hip_x_torque);
  this->control->co_left_stimulation_hip_y_torque.ConnectTo(hal->ci_left_stimulation_hip_y_torque);
  this->control->co_left_stimulation_hip_z_torque.ConnectTo(hal->ci_left_stimulation_hip_z_torque);
  this->control->co_right_hip_x_angle.ConnectTo(hal->ci_right_hip_x_angle);
  this->control->co_right_hip_y_angle.ConnectTo(hal->ci_right_hip_y_angle);
  this->control->co_right_hip_z_angle.ConnectTo(hal->ci_right_hip_z_angle);
  this->control->co_right_stimulation_hip_x_angle.ConnectTo(hal->ci_right_stimulation_hip_x_angle);
  this->control->co_right_stimulation_hip_y_angle.ConnectTo(hal->ci_right_stimulation_hip_y_angle);
  this->control->co_right_stimulation_hip_z_angle.ConnectTo(hal->ci_right_stimulation_hip_z_angle);
  this->control->co_right_hip_x_torque.ConnectTo(hal->ci_right_hip_x_torque);
  this->control->co_right_hip_y_torque.ConnectTo(hal->ci_right_hip_y_torque);
  this->control->co_right_hip_z_torque.ConnectTo(hal->ci_right_hip_z_torque);
  this->control->co_right_stimulation_hip_x_torque.ConnectTo(hal->ci_right_stimulation_hip_x_torque);
  this->control->co_right_stimulation_hip_y_torque.ConnectTo(hal->ci_right_stimulation_hip_y_torque);
  this->control->co_right_stimulation_hip_z_torque.ConnectTo(hal->ci_right_stimulation_hip_z_torque);
  this->control->co_spine_x_angle.ConnectTo(hal->ci_spine_x_angle);
  this->control->co_spine_y_angle.ConnectTo(hal->ci_spine_y_angle);
  this->control->co_spine_z_angle.ConnectTo(hal->ci_spine_z_angle);
  this->control->co_stimulation_spine_x_angle.ConnectTo(hal->ci_stimulation_spine_x_angle);
  this->control->co_stimulation_spine_y_angle.ConnectTo(hal->ci_stimulation_spine_y_angle);
  this->control->co_stimulation_spine_z_angle.ConnectTo(hal->ci_stimulation_spine_z_angle);

  this->control->co_left_shoulder_x_angle.ConnectTo(hal->ci_left_shoulder_x_angle);
  this->control->co_left_shoulder_y_angle.ConnectTo(hal->ci_left_shoulder_y_angle);
  this->control->co_left_elbow_angle.ConnectTo(hal->ci_left_elbow_angle);
  this->control->co_left_stimulation_shoulder_x_angle.ConnectTo(hal->ci_left_stimulation_shoulder_x_angle);
  this->control->co_left_stimulation_shoulder_y_angle.ConnectTo(hal->ci_left_stimulation_shoulder_y_angle);
  this->control->co_left_stimulation_elbow_angle.ConnectTo(hal->ci_left_stimulation_elbow_angle);

  this->hal->GetControllerOutputs().ConnectByName(simulation_group->GetControllerInputs(), true);

  this->hal->si_ins_accel.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/ins - Accel");
  this->hal->si_ins_omega.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/ins - Omega");
  this->hal->si_ins_rot.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/ins - Angle");
  this->hal->si_left_shoulder_x_torque.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/shoulder_x_left - Torque");
  this->hal->si_left_shoulder_x_angle.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/shoulder_x_left - Angle");
  this->hal->si_left_shoulder_y_torque.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/shoulder_y_left - Torque");
  this->hal->si_left_shoulder_y_angle.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/shoulder_y_left - Angle");
  this->hal->si_left_elbow_torque.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/elbow_left - Torque");
  this->hal->si_left_elbow_angle.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/elbow_left - Angle");

  this->hal->si_right_shoulder_x_torque.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/shoulder_x_right - Torque");
  this->hal->si_right_shoulder_x_angle.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/shoulder_x_right - Angle");
  this->hal->si_right_shoulder_y_torque.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/shoulder_y_right - Torque");
  this->hal->si_right_shoulder_y_angle.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/shoulder_y_right - Angle");
  this->hal->si_right_elbow_torque.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/elbow_right - Torque");
  this->hal->si_right_elbow_angle.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/elbow_right - Angle");

  this->hal->si_spine_x_torque.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/spine_x - Torque");
  this->hal->si_spine_x_angle.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/spine_x - Angle");
  this->hal->si_spine_y_torque.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/spine_y - Torque");
  this->hal->si_spine_y_angle.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/spine_y - Angle");
  this->hal->si_spine_z_torque.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/spine_z - Torque");
  this->hal->si_spine_z_angle.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/spine_z - Angle");

  this->hal->si_left_hip_x_torque.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/hip_x_left - Torque");
  this->hal->si_left_hip_x_angle.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/hip_x_left - Angle");
  this->hal->si_left_hip_y_torque.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/hip_y_left - Torque");
  this->hal->si_left_hip_y_angle.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/hip_y_left - Angle");
  this->hal->si_left_hip_z_torque.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/hip_z_left - Torque");
  this->hal->si_left_hip_z_angle.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/hip_z_left - Angle");
  this->hal->si_left_knee_torque.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/knee_left - Torque");
  this->hal->si_left_knee_angle.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/knee_left - Angle");

  this->hal->si_left_ankle_x_torque.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/ankle_x_left - Torque");
  this->hal->si_left_ankle_x_angle.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/ankle_x_left - Angle");
  this->hal->si_left_ankle_y_torque.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/ankle_y_left - Torque");
  this->hal->si_left_ankle_y_angle.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/ankle_y_left - Angle");

  this->hal->si_right_hip_x_torque.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/hip_x_right - Torque");
  this->hal->si_right_hip_x_angle.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/hip_x_right - Angle");
  this->hal->si_right_hip_y_torque.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/hip_y_right - Torque");
  this->hal->si_right_hip_y_angle.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/hip_y_right - Angle");
  this->hal->si_right_hip_z_torque.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/hip_z_right - Torque");
  this->hal->si_right_hip_z_angle.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/hip_z_right - Angle");
  this->hal->si_right_knee_torque.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/knee_right - Torque");
  this->hal->si_right_knee_angle.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/knee_right - Angle");

  this->hal->si_right_ankle_x_torque.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/ankle_x_right - Torque");
  this->hal->si_right_ankle_x_angle.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/ankle_x_right - Angle");
  this->hal->si_right_ankle_y_torque.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/ankle_y_right - Torque");
  this->hal->si_right_ankle_y_angle.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/ankle_y_right - Angle");

  this->hal->si_left_inner_heel_force.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/inner_heel_force_left - Force");
  this->hal->si_right_inner_heel_force.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/inner_heel_force_right - Force");

  this->hal->si_left_inner_toe_force.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/inner_toe_force_left - Force");
  this->hal->si_right_inner_toe_force.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/inner_toe_force_right - Force");

  this->hal->si_left_outer_heel_force.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/outer_heel_force_left - Force");
  this->hal->si_right_outer_heel_force.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/outer_heel_force_right - Force");

  this->hal->si_left_outer_toe_force.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/outer_toe_force_left - Force");
  this->hal->si_right_outer_toe_force.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/outer_toe_force_right - Force");

  this->hal->si_left_torque.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/loadcell_left - Torque");
  this->hal->si_right_torque.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/loadcell_right - Torque");

  this->hal->si_left_force_z.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/loadcell_left - Force Z");
  this->hal->si_right_force_z.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/loadcell_right - Force Z");


  this->control->si_foot_left_pose.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/foot_left - Pose");
  this->control->si_foot_right_pose.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/foot_right - Pose");
  this->control->si_pelvis_pose.ConnectTo("/Main Thread/BipedControl/Physics Engine Newton/Sensor Output/pelvis - Pose");
// fixme

  this->hal->so_left_force_z.ConnectTo(this->control->si_left_force_z);
  this->hal->so_left_torque_x.ConnectTo(this->control->si_left_torque_x);
  this->hal->so_left_torque_y.ConnectTo(this->control->si_left_torque_y);

  this->hal->so_left_inner_toe_force.ConnectTo(this->control->si_left_inner_toe_force);
  this->hal->so_left_outer_toe_force.ConnectTo(this->control->si_left_outer_toe_force);
  this->hal->so_left_inner_heel_force.ConnectTo(this->control->si_left_inner_heel_force);
  this->hal->so_left_outer_heel_force.ConnectTo(this->control->si_left_outer_heel_force);


//

//
  this->hal->so_right_force_z.ConnectTo(this->control->si_right_force_z);
  this->hal->so_right_torque_x.ConnectTo(this->control->si_right_torque_x);
  this->hal->so_right_torque_y.ConnectTo(this->control->si_right_torque_y);
  this->hal->so_right_inner_heel_force.ConnectTo(this->control->si_right_inner_heel_force);
  this->hal->so_right_outer_heel_force.ConnectTo(this->control->si_right_outer_heel_force);
  this->hal->so_right_inner_toe_force.ConnectTo(this->control->si_right_inner_toe_force);
  this->hal->so_right_outer_toe_force.ConnectTo(this->control->si_right_outer_toe_force);


}

//----------------------------------------------------------------------
// End of namespace declaration
//----------------------------------------------------------------------
}
}
}
