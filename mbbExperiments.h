//
// You received this file as part of Finroc
// A Framework for intelligent robot control
//
// Copyright (C) Finroc GbR (finroc.org)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//----------------------------------------------------------------------
/*!\file    projects/biped/utils/mbbExperiments.h
 *
 * \author  Max Steiner
 * \author  Jie Zhao (ported to finroc)
 *
 * \date    2013-10-02
 *
 * \brief Contains mbbExperiments
 *
 * \b mbbExperiments
 *
 *
 * Create the experiments module for the simulation!
 *
 */
//----------------------------------------------------------------------
#ifndef __projects__biped__utils__mbbExperiments_h__
#define __projects__biped__utils__mbbExperiments_h__

#include "plugins/ib2c/tModule.h"
#include "rrlib/time/time.h"
#include "rrlib/math/tVector.h"
#include "rrlib/math/tPose3D.h"

//----------------------------------------------------------------------
// External includes (system with <>, local with "")
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Internal includes with ""
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Namespace declaration
//----------------------------------------------------------------------
namespace finroc
{
namespace biped
{
namespace utils
{

//----------------------------------------------------------------------
// Forward declarations / typedefs / enums
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Class declaration
//----------------------------------------------------------------------
//! SHORT_DESCRIPTION
/*!
 *
 * Create the experiments module for the simulation!
 */
class mbbExperiments : public ib2c::tModule
{

//----------------------------------------------------------------------
// Ports (These are the only variables that may be declared public)
//----------------------------------------------------------------------
public:

  tParameter<bool> par_repeat;
  tParameter<unsigned int> par_times_to_repeat;
  tParameter<bool> par_log;
  tParameter<double> par_stiffness_threshold;
  tParameter<double> par_plate_translational_movement_x;
  tParameter<double> par_plate_rot_x_freq;
  tParameter<double> par_plate_rot_y_freq;
  tParameter<double> par_plate_rot_z_freq;
  tParameter<double> par_plate_rot_x_offset;
  tParameter<double> par_plate_rot_y_offset;
  tParameter<double> par_plate_rot_x_angle;
  tParameter<double> par_learning_walking_velocity;
  tParameter<double> par_torso_force_x;
  tParameter<double> par_fast_walking;

  tInput<double> si_stiffness_factor;
  tInput<double> si_stand_state;
  tInput<double> si_roll;
  tInput<double> si_pitch;
  tInput<double> si_left_foot_load;
  tInput<double> si_right_foot_load;
  tInput<double> si_sphere_velocity_x;
  tInput<double> si_sphere_velocity_y;
  tInput<double> si_sphere_velocity_z;
  tInput<double> si_plate_velocity_x;
  tInput<double> si_plate_velocity_y;
  tInput<double> si_plate_velocity_z;
  tInput<double> si_plate_angular_velocity_x;
  tInput<double> si_plate_angular_velocity_y;
  tInput<double> si_plate_angular_velocity_z;
  tInput<double> si_new_running;
  tInput<double> si_loop_times;

  tOutput<double> so_run_time;
  tOutput<double> so_total_time;
  tOutput<double> so_max_vel;
  tOutput<double> so_experiment_state;
  tOutput<double> so_run;

  tInput<double> ci_enabled_experiment;
  tInput<double> ci_experiment_type;
  tInput<double> ci_desired_duration;
  tInput<double> ci_sphere_movement_step_width;
  tInput<double> ci_plate_movement_step_width;
  tInput<double> ci_plate_rotation_step_width;
  tInput<double> ci_head_force_x;
  tInput<double> ci_head_force_y;
  tInput<double> ci_head_force_z;
  tInput<double> ci_torso_force_x;
  tInput<double> ci_torso_force_y;
  tInput<double> ci_torso_force_z;
  tInput<double> ci_pelvis_force_x;
  tInput<double> ci_pelvis_force_y;
  tInput<double> ci_pelvis_force_z;
  tInput<double> ci_sphere_pos_x;
  tInput<double> ci_sphere_pos_y;
  tInput<double> ci_sphere_pos_z;
  tInput<double> ci_plate_pos_x;
  tInput<double> ci_plate_pos_y;
  tInput<double> ci_plate_pos_z;
  tInput<double> ci_plate_rot_x;
  tInput<double> ci_plate_rot_y;
  tInput<double> ci_plate_rot_z;
  tInput<bool> ci_reset;
  tInput<bool> ci_slow_walking;

  tOutput<double> co_sensor_monitor;
  tOutput<rrlib::math::tVec3d> co_head_force;
  tOutput<rrlib::math::tVec3d> co_torso_force;
  tOutput<rrlib::math::tVec3d> co_pelvis_force;

  tOutput<double> co_sphere_pos_x;
  tOutput<double> co_sphere_pos_y;
  tOutput<double> co_sphere_pos_z;
  tOutput<double> co_plate_pos_x;
  tOutput<double> co_plate_pos_y;
  tOutput<double> co_plate_pos_z;
  tOutput<double> co_plate_rot_x;
  tOutput<double> co_plate_rot_y;
  tOutput<double> co_plate_rot_z;
  tOutput<double> co_stimulation_walking;
  tOutput<double> co_walking_speed;
  tOutput<double> co_walking_termination;
  tOutput<bool> co_push_detected_x;
  tOutput<bool> co_push_detected_y;
  tOutput<bool> co_push_started;
  tOutput<bool> co_push_started_x;
  tOutput<double> co_torso_force_y;
  tOutput<double> co_torso_force_x;
  tOutput<double> co_actual_speed;
  tOutput<rrlib::math::tPose3D> co_plate_rot;
  tOutput<rrlib::math::tVec3d> co_plate_pos;

  tOutput<bool> co_slow_walking;
  tOutput<bool> co_reset_simulation;
//----------------------------------------------------------------------
// Public methods and typedefs
//----------------------------------------------------------------------
public:

  mbbExperiments(core::tFrameworkElement *parent, const std::string &name = "Experiments",
                 ib2c::tStimulationMode stimulation_mode = ib2c::tStimulationMode::AUTO, unsigned int number_of_inhibition_ports = 0);


  enum tExperimentType
  {
    eEXP_TYPE_NONE,
    eEXP_TYPE_FORCE_RANDOM,
    eEXP_TYPE_LEARNING,
    eEXP_TYPE_PLATE_ROTATING,
    eEXP_TYPE_OPTIMIZATION,
    eEXP_TYPE_DIMENSION
  };

  enum tExperimentState
  {
    eEXP_STATE_IDLE,
    eEXP_STATE_START,
    eEXP_STATE_END,
    eEXP_STATE_RESET,
    eEXP_STATE_RESET_DONE,
    eEXP_STATE_DIMENSION
  };
//----------------------------------------------------------------------
// Private fields and methods
//----------------------------------------------------------------------
private:
  // parameters
  bool repeat;
  int times_to_repeat;
  bool log;
  double enable_experiment;
  double stiffness_threshold;
  double plate_translational_movement_x;
  double plate_rot_x_freq;
  double plate_rot_y_freq;
  double plate_rot_z_freq;
  double plate_rot_x_offset;
  double plate_rot_y_offset;
  double plate_rot_x_angle;

  double phase_x;
  double phase_y;
  double last_plate_rot_freq_x;
  double last_plate_rot_freq_y;
  // sensor inputs
  double footload_left;
  double footload_right;
  double roll;
  double pitch;
  double new_running;

  // controller outputs
  double head_force_x;
  double head_force_y;
  double head_force_z;
  double torso_force_x;
  double torso_force_y;
  double torso_force_z;
  double pelvis_force_x;
  double pelvis_force_y;
  double pelvis_force_z;
  double sphere_pos_x;
  double sphere_pos_y;
  double sphere_pos_z;
  double plate_pos_x;
  double plate_pos_y;
  double plate_pos_z;
  double plate_rot_x;
  double plate_rot_y;
  double plate_rot_z;
  double stim_walking;
  double walking_speed;
  double walking_termi;
  double plate_rot_y_vel;
  // internal variables
  double desired_duration;
  rrlib::time::tTimestamp last_looptime;
  rrlib::time::tDuration loop_duration;
  double duration_milliseconds;
  double total_time;
  double run_time;
  int exp_type;
  int state;
  int run;
  int last_run;
  bool reset_called;
  bool off_ground;
  bool push_x;
  bool push_force_start;
  double push_duration;
  bool push_detected;
  bool push_starting;
  double push_loop_time;
  int random_loop_times;
  int random_start_milliseconds;
  int random_direction;
  int random_torso_force_x;
  rrlib::time::tTimestamp push_force_start_time;
  // random number generator
  std::uniform_real_distribution<double> uniform_distribution;
  std::mt19937 eng;

  int iteration_random_rotation;
  std::vector<double> plate_rotation;

  bool reset_started;
  int reset_time;
  int restart_waiting_time;
  int restart_landing_time;

  double fast_walking_speed;
  /*! Destructor
   *
   * The destructor of modules is declared private to avoid accidental deletion. Deleting
   * modules is already handled by the framework.
   */
  ~mbbExperiments();

  virtual void OnParameterChange();

  virtual bool ProcessTransferFunction(double activation);

  virtual ib2c::tActivity CalculateActivity(std::vector<ib2c::tActivity> &derived_activities, double activation) const;

  virtual ib2c::tTargetRating CalculateTargetRating(double activation) const;

};

//----------------------------------------------------------------------
// End of namespace declaration
//----------------------------------------------------------------------
}
}
}



#endif
