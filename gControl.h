//
// You received this file as part of Finroc
// A framework for intelligent robot control
//
// Copyright (C) Finroc GbR (finroc.org)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//
//----------------------------------------------------------------------
/*!\file    projects/biped/control/gControl.h
 *
 * \author  Jie Zhao
 *
 * \date    2014-01-08
 *
 * \brief Contains gControl
 *
 * \b gControl
 *
 * The control group for connectiong different groups!
 *
 */
//----------------------------------------------------------------------
#ifndef __projects__biped__control__gControl_h__
#define __projects__biped__control__gControl_h__

#include "plugins/structure/tSenseControlGroup.h"
#include "rrlib/math/tPose3D.h"

//----------------------------------------------------------------------
// External includes (system with <>, local with "")
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Internal includes with ""
//----------------------------------------------------------------------
using namespace rrlib::math;
//----------------------------------------------------------------------
// Namespace declaration
//----------------------------------------------------------------------
namespace finroc
{
namespace biped
{
namespace control
{
class gUpperTrunkGroup;
class gLowerTrunkGroup;
class gLegGroup;

//----------------------------------------------------------------------
// Forward declarations / typedefs / enums
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Class declaration
//----------------------------------------------------------------------
//! SHORT_DESCRIPTION
/*!
 * The control group for connectiong different groups!
 */
class gControl : public structure::tSenseControlGroup
{

//----------------------------------------------------------------------
// Ports (These are the only variables that may be declared public)
//----------------------------------------------------------------------
public:

  tControllerInput<double> ci_arrow_visualisation;
  tControllerInput<double> ci_sensor_monitor;
  tControllerInput<double> ci_stimulation_walking;
  tControllerInput<double> ci_walking_speed;
  tControllerInput<double> ci_walking_direction;
  tControllerInput<double> ci_walking_termination;
  tControllerInput<double> ci_walking_laterally;
  tControllerInput<bool> ci_push_x;
  tControllerInput<bool> ci_push_y;
  tControllerInput<double> ci_head_force_x;
  tControllerInput<double> ci_head_force_y;
  tControllerInput<double> ci_torso_force_x;
  tControllerInput<double> ci_torso_force_y;
  tControllerInput<bool> ci_reset_simulation_experiments;
  tControllerInput<bool> ci_push_started;
  tControllerInput<bool> ci_push_started_x;
  tControllerInput<bool> ci_push_detected_x;

  tControllerOutput<double> co_left_stimulation_shoulder_x_torque;
  tControllerOutput<double> co_left_shoulder_x_torque;
  tControllerOutput<double> co_left_stimulation_shoulder_x_angle;
  tControllerOutput<double> co_left_shoulder_x_angle;
  tControllerOutput<double> co_left_stimulation_shoulder_y_torque;
  tControllerOutput<double> co_left_shoulder_y_torque;
  tControllerOutput<double> co_left_stimulation_shoulder_y_angle;
  tControllerOutput<double> co_left_shoulder_y_angle;
  tControllerOutput<double> co_left_stimulation_elbow_torque;
  tControllerOutput<double> co_left_elbow_torque;
  tControllerOutput<double> co_left_stimulation_elbow_angle;
  tControllerOutput<double> co_left_elbow_angle;

  tControllerOutput<double> co_right_stimulation_shoulder_x_torque;
  tControllerOutput<double> co_right_shoulder_x_torque;
  tControllerOutput<double> co_right_stimulation_shoulder_x_angle;
  tControllerOutput<double> co_right_shoulder_x_angle;
  tControllerOutput<double> co_right_stimulation_shoulder_y_torque;
  tControllerOutput<double> co_right_shoulder_y_torque;
  tControllerOutput<double> co_right_stimulation_shoulder_y_angle;
  tControllerOutput<double> co_right_shoulder_y_angle;
  tControllerOutput<double> co_right_stimulation_elbow_torque;
  tControllerOutput<double> co_right_elbow_torque;
  tControllerOutput<double> co_right_stimulation_elbow_angle;
  tControllerOutput<double> co_right_elbow_angle;

  tControllerOutput<double> co_stimulation_spine_x_torque;
  tControllerOutput<double> co_spine_x_torque;
  tControllerOutput<double> co_stimulation_spine_x_angle;
  tControllerOutput<double> co_spine_x_angle;
  tControllerOutput<double> co_stimulation_spine_y_torque;
  tControllerOutput<double> co_spine_y_torque;
  tControllerOutput<double> co_stimulation_spine_y_angle;
  tControllerOutput<double> co_spine_y_angle;
  tControllerOutput<double> co_stimulation_spine_z_torque;
  tControllerOutput<double> co_spine_z_torque;
  tControllerOutput<double> co_stimulation_spine_z_angle;
  tControllerOutput<double> co_spine_z_angle;

  tControllerOutput<double> co_left_stimulation_hip_x_torque;
  tControllerOutput<double> co_left_hip_x_torque;
  tControllerOutput<double> co_left_stimulation_hip_x_angle;
  tControllerOutput<double> co_left_hip_x_angle;
  tControllerOutput<double> co_left_stimulation_hip_y_torque;
  tControllerOutput<double> co_left_hip_y_torque;
  tControllerOutput<double> co_left_stimulation_hip_y_angle;
  tControllerOutput<double> co_left_hip_y_angle;
  tControllerOutput<double> co_left_stimulation_hip_z_torque;
  tControllerOutput<double> co_left_hip_z_torque;
  tControllerOutput<double> co_left_stimulation_hip_z_angle;
  tControllerOutput<double> co_left_hip_z_angle;

  tControllerOutput<double> co_right_stimulation_hip_x_torque;
  tControllerOutput<double> co_right_hip_x_torque;
  tControllerOutput<double> co_right_stimulation_hip_x_angle;
  tControllerOutput<double> co_right_hip_x_angle;
  tControllerOutput<double> co_right_stimulation_hip_y_torque;
  tControllerOutput<double> co_right_hip_y_torque;
  tControllerOutput<double> co_right_stimulation_hip_y_angle;
  tControllerOutput<double> co_right_hip_y_angle;
  tControllerOutput<double> co_right_stimulation_hip_z_torque;
  tControllerOutput<double> co_right_hip_z_torque;
  tControllerOutput<double> co_right_stimulation_hip_z_angle;
  tControllerOutput<double> co_right_hip_z_angle;

  tControllerOutput<double> co_left_stimulation_knee_torque;
  tControllerOutput<double> co_left_knee_torque;
  tControllerOutput<double> co_left_stimulation_knee_angle;
  tControllerOutput<double> co_left_knee_angle;
  tControllerOutput<double> co_left_stimulation_ankle_y_torque;
  tControllerOutput<double> co_left_ankle_y_torque;
  tControllerOutput<double> co_left_stimulation_ankle_y_angle;
  tControllerOutput<double> co_left_ankle_y_angle;
  tControllerOutput<double> co_left_stimulation_ankle_x_torque;
  tControllerOutput<double> co_left_ankle_x_torque;
  tControllerOutput<double> co_left_stimulation_ankle_x_angle;
  tControllerOutput<double> co_left_ankle_x_angle;

  tControllerOutput<double> co_right_stimulation_knee_torque;
  tControllerOutput<double> co_right_knee_torque;
  tControllerOutput<double> co_right_stimulation_knee_angle;
  tControllerOutput<double> co_right_knee_angle;
  tControllerOutput<double> co_right_stimulation_ankle_y_torque;
  tControllerOutput<double> co_right_ankle_y_torque;
  tControllerOutput<double> co_right_stimulation_ankle_y_angle;
  tControllerOutput<double> co_right_ankle_y_angle;
  tControllerOutput<double> co_right_stimulation_ankle_x_torque;
  tControllerOutput<double> co_right_ankle_x_torque;
  tControllerOutput<double> co_right_stimulation_ankle_x_angle;
  tControllerOutput<double> co_right_ankle_x_angle;
  tControllerOutput<bool>   co_slow_walking_stimulation;
  tControllerOutput<bool>   co_slow_walking_on;
  tControllerOutput<double>   co_actual_speed;

  std::vector<tSensorInput<double>> si_ins_accel;
  std::vector<tSensorInput<double>> si_ins_omega;
  std::vector<tSensorInput<double>> si_ins_rot;

  tSensorInput<tPose3D> si_foot_left_pose;
  tSensorInput<tPose3D> si_foot_right_pose;
  tSensorInput<tPose3D> si_pelvis_pose;
  tSensorInput<double> si_left_force_deviation;
  tSensorInput<double> si_right_force_deviation;

  tSensorInput<double> si_left_knee_torque;
  tSensorInput<double> si_left_knee_angle;
  tSensorInput<double> si_left_ankle_y_torque;
  tSensorInput<double> si_left_ankle_y_angle;
  tSensorInput<double> si_left_ankle_x_torque;
  tSensorInput<double> si_left_ankle_x_angle;
  tSensorInput<double> si_left_force_z;
  tSensorInput<double> si_left_torque_x;
  tSensorInput<double> si_left_torque_y;
  tSensorInput<double> si_left_inner_toe_force;
  tSensorInput<double> si_left_outer_toe_force;
  tSensorInput<double> si_left_inner_heel_force;
  tSensorInput<double> si_left_outer_heel_force;
  tSensorInput<double> si_spine_x_torque;
  tSensorInput<double> si_spine_x_angle;
  tSensorInput<double> si_spine_y_torque;
  tSensorInput<double> si_spine_y_angle;
  tSensorInput<double> si_spine_z_torque;
  tSensorInput<double> si_spine_z_angle;
  tSensorInput<double> si_left_hip_x_torque;
  tSensorInput<double> si_left_hip_x_angle;
  tSensorInput<double> si_left_hip_y_torque;
  tSensorInput<double> si_left_hip_y_angle;
  tSensorInput<double> si_left_hip_z_torque;
  tSensorInput<double> si_left_hip_z_angle;
  tSensorInput<double> si_left_shoulder_x_torque;
  tSensorInput<double> si_left_shoulder_x_angle;
  tSensorInput<double> si_left_shoulder_y_torque;
  tSensorInput<double> si_left_shoulder_y_angle;
  tSensorInput<double> si_left_elbow_torque;
  tSensorInput<double> si_left_elbow_angle;

  tSensorInput<double> si_right_knee_torque;
  tSensorInput<double> si_right_knee_angle;
  tSensorInput<double> si_right_ankle_y_torque;
  tSensorInput<double> si_right_ankle_y_angle;
  tSensorInput<double> si_right_ankle_x_torque;
  tSensorInput<double> si_right_ankle_x_angle;
  tSensorInput<double> si_right_force_z;
  tSensorInput<double> si_right_torque_x;
  tSensorInput<double> si_right_torque_y;
  tSensorInput<double> si_right_inner_toe_force;
  tSensorInput<double> si_right_outer_toe_force;
  tSensorInput<double> si_right_inner_heel_force;
  tSensorInput<double> si_right_outer_heel_force;
  tSensorInput<double> si_right_hip_x_torque;
  tSensorInput<double> si_right_hip_x_angle;
  tSensorInput<double> si_right_hip_y_torque;
  tSensorInput<double> si_right_hip_y_angle;
  tSensorInput<double> si_right_hip_z_torque;
  tSensorInput<double> si_right_hip_z_angle;
  tSensorInput<double> si_right_shoulder_x_torque;
  tSensorInput<double> si_right_shoulder_x_angle;
  tSensorInput<double> si_right_shoulder_y_torque;
  tSensorInput<double> si_right_shoulder_y_angle;
  tSensorInput<double> si_right_elbow_torque;
  tSensorInput<double> si_right_elbow_angle;

  tSensorOutput<double> so_stiffness_factor;
  tSensorOutput<double> so_stand_state;
  tSensorOutput<double> so_ins_roll;
  tSensorOutput<double> so_ins_pitch;
  tSensorOutput<double> so_left_force_deviation;
  tSensorOutput<double> so_right_force_deviation;
  tSensorOutput<double> so_left_foot_load;
  tSensorOutput<double> so_right_foot_load;
  tSensorOutput<double> so_left_ground_contact;
  tSensorOutput<double> so_right_ground_contact;
  tSensorOutput<double> so_left_force_z;
  tSensorOutput<double> so_right_force_z;
  tSensorOutput<double> so_new_running;
  tSensorOutput<double> so_loop_times;

//----------------------------------------------------------------------
// Public methods and typedefs
//----------------------------------------------------------------------
public:

  gControl(core::tFrameworkElement *parent, const std::string &name = "Control",
           const std::string &structure_config_file = __FILE__".xml");

//----------------------------------------------------------------------
// Private fields and methods
//----------------------------------------------------------------------
private:

  /*! Destructor
   *
   * The destructor of modules is declared private to avoid accidental deletion. Deleting
   * modules is already handled by the framework.
   */
  ~gControl();

  gUpperTrunkGroup* upper_trunk;
  gLowerTrunkGroup* lower_trunk;
  gLegGroup* left_leg;
  gLegGroup* right_leg;

};

//----------------------------------------------------------------------
// End of namespace declaration
//----------------------------------------------------------------------
}
}
}



#endif
