//
// You received this file as part of Finroc
// A framework for intelligent robot control
//
// Copyright (C) AG Robotersysteme TU Kaiserslautern
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//
//----------------------------------------------------------------------
/*!\file    projects/biped/control/gbbLearningModuleBehaviors.h
 *
 * \author  Qi Liu & Jie Zhao
 *
 * \date    2014-12-12
 *
 * \brief Contains gbbLearningModuleBehaviors
 *
 * \b gbbLearningModuleBehaviors
 *
 * This group is used to connect different learning modules.
 *
 */
//----------------------------------------------------------------------
#ifndef __projects__biped__control__gbbLearningModuleBehaviors_h__
#define __projects__biped__control__gbbLearningModuleBehaviors_h__

#include "plugins/ib2c/tGroup.h"
#include "rrlib/math/tPose3D.h"

//----------------------------------------------------------------------
// External includes (system with <>, local with "")
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Internal includes with ""
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Namespace declaration
//----------------------------------------------------------------------
namespace finroc
{
namespace biped
{
namespace control
{

//----------------------------------------------------------------------
// Forward declarations / typedefs / enums
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Class declaration
//----------------------------------------------------------------------
//! SHORT_DESCRIPTION
/*!
 * This group is used to connect different learning modules.
 */
class gbbLearningModuleBehaviors : public ib2c::tGroup
{

//----------------------------------------------------------------------
// Ports (These are the only variables that may be declared public)
//----------------------------------------------------------------------
public:
  tInput<double> ci_walking_stimulation;

  tInput<double> ci_walking_left_1;
  tInput<double> ci_walking_left_2;
  tInput<double> ci_walking_left_3;
  tInput<double> ci_walking_left_4;
  tInput<double> ci_walking_left_5;

  tInput<double> ci_walking_right_1;
  tInput<double> ci_walking_right_2;
  tInput<double> ci_walking_right_3;
  tInput<double> ci_walking_right_4;
  tInput<double> ci_walking_right_5;
  tInput<double> ci_walking_left_phase;
  tInput<double> ci_walking_right_phase;

  tInput<double> ci_foot_load_left;
  tInput<double> ci_foot_load_right;
  tInput<double> ci_pitch_angle;
  tInput<double> ci_roll_angle;
  tInput<double> ci_com_x;
  tInput<double> ci_com_y;
  tInput<double> ci_xcom_x;
  tInput<double> ci_xcom_x_left;
  tInput<double> ci_xcom_x_right;
  tInput<double> ci_xcom_y_left;
  tInput<double> ci_xcom_y_right;
  tInput<double> ci_com_x_left;
  tInput<double> ci_com_x_right;
  tInput<double> ci_left_correction_sagittal;
  tInput<double> ci_right_correction_sagittal;
  tInput<double> ci_linear_velocity_x;
  tInput<bool> ci_reset_simulation_experiments;
  tInput<double> ci_walking_scale;
  tInput<bool> ci_push_detected_y;
  tInput<bool> ci_push_started_x;
  tInput<bool> ci_push_detected_x;

  tInput<double> ci_left_correction_lateral;
  tInput<double> ci_right_correction_lateral;
  tInput<double> ci_pelvis_pitch;
  tInput<double> ci_pelvis_roll;
  tInput<double> ci_torso_force_y;
  tInput<double> ci_torso_force_x;
  tInput<double> si_pelvis_position_x;
  tInput<double> si_pelvis_position_y;
  tInput<double> si_left_foot_position_x;
  tInput<double> si_left_foot_position_y;
  tInput<double> si_left_foot_position_z;
  tInput<double> si_right_foot_position_x;
  tInput<double> si_right_foot_position_y;
  tInput<double> si_right_foot_position_z;
  tInput<double> si_walking_velocity;
  tInput<double> si_left_hip_angle;
  tInput<double> si_right_hip_angle;
  tInput<double> si_lock_hip_left_torque;
  tInput<double> si_lock_hip_right_torque;
  tInput<double> si_left_ankle_angle;
  tInput<double> si_right_ankle_angle;
  tInput<double> si_left_ankle_angle_lateral;
  tInput<double> si_right_ankle_angle_lateral;
  tInput<double> si_left_knee_angle;
  tInput<double> si_right_knee_angle;
  tInput<double> si_left_knee_velocity;
  tInput<double> si_right_knee_velocity;
  tOutput<double> co_state_value;
  tOutput<double> co_leg_propel_action1;
  tOutput<double> co_leg_propel_action2;
  tOutput<double> co_leg_propel_action3;
  tOutput<double> co_leg_propel_action4;
  tOutput<double> co_lateral_ankle_pr_action;
  tOutput<double> co_left_learning_ankle_stiffness;
  tOutput<double> co_right_learning_ankle_stiffness;
  tOutput<double> co_hip_swing_action1;
  tOutput<double> co_hip_swing_action2;
  tOutput<double> co_hip_swing_action3;
  tOutput<double> co_hip_swing_action4;
  tOutput<double> co_left_learning_torque_factor;
  tOutput<double> co_right_learning_torque_factor;

  tOutput<double> co_output_hip_swing_1;
  tOutput<double> co_output_hip_swing_2;
  tOutput<double> co_output_hip_swing_3;
  tOutput<double> co_output_hip_swing_3_rear;
  tOutput<double> co_output_knee_flexion_1;
  tOutput<double> co_output_knee_flexion_2;
  tOutput<double> co_output_knee_flexion_2_rear;
  tOutput<double> co_output_lock_hip_1;
  tOutput<double> co_output_lock_hip_2;
  tOutput<double> co_output_lock_hip_3;


  tOutput<bool> co_obstacle_walking_stimulation;
  tOutput<int> co_obstacle_step;

  // PSO Slow walking
  tOutput<bool> co_slow_walking_stimulation;
  tOutput<bool> co_slow_walking_on;
  tOutput<bool> co_fast_walking_stimulation;
    tOutput<bool> co_fast_walking_on;
  tOutput<int> co_slow_walking_step;

  tOutput<double> so_new_running;
  tOutput<double> so_loop_times;


  // PSO Slow Walking
  tOutput<double> co_walking_velocity;
  tOutput<double> co_walking_distance;
  tOutput<double> co_step_length;
  tOutput<double> co_slow_walking_ahs_max_torque;
  tOutput<double> co_slow_walking_ahs_t1;
  tOutput<double> co_slow_walking_ahs_t2;
  tOutput<double> co_slow_walking_lp_max_torque;
  tOutput<double> co_slow_walking_lp_t1;
  tOutput<double> co_slow_walking_lp_t2;
  tOutput<double> co_slow_walking_lh_par1;
  tOutput<double> co_slow_walking_lh_par2;
  tOutput<double> co_fast_walking_lh_par1;
  tOutput<double> co_fast_walking_lh_par2;
  tOutput<double> co_slow_walking_knee_angle;
  tOutput<double> co_slow_walking_knee_angle_low;
  tOutput<double> co_slow_walking_knee_angle_high;
  tOutput<double> co_slow_walking_knee_velocity;
  tOutput<double> co_slow_walking_lk_hip_angle_low;
  tOutput<double> co_slow_walking_lk_hip_angle_high;
  tOutput<double> co_slow_walking_lk_increment_par1;
  tOutput<double> co_slow_walking_lk_increment_par2;
  tOutput<double> co_slow_walking_max_impulse;
  tOutput<double> co_slow_walk_stimulation_knee_angle;
  tOutput<double> co_slow_walk_stimulation_knee_angle_activity;


//----------------------------------------------------------------------
// Public methods and typedefs
//----------------------------------------------------------------------
public:

  gbbLearningModuleBehaviors(core::tFrameworkElement *parent, const std::string &name = "LearningModuleBehaviors",
                             ib2c::tStimulationMode stimulation_mode = ib2c::tStimulationMode::AUTO, unsigned int number_of_inhibition_ports = 0,
                             const std::string &structure_config_file = __FILE__".xml");

//----------------------------------------------------------------------
// Private fields and methods
//----------------------------------------------------------------------
private:


  /*! Destructor
   *
   * The destructor of groups is declared private to avoid accidental deletion. Deleting
   * groups is already handled by the framework.
   */
  ~gbbLearningModuleBehaviors();


};

//----------------------------------------------------------------------
// End of namespace declaration
//----------------------------------------------------------------------
}
}
}



#endif
