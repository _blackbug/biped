//
// You received this file as part of Finroc
// A Framework for intelligent robot control
//
// Copyright (C) Finroc GbR (finroc.org)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//----------------------------------------------------------------------
/*!\file    projects/biped/motor_patterns/mbbActiveHipSwingSin.h
 *
 * \author  Tobias luksch
 * \author  Jie Zhao
 *
 * \date    2013-09-26
 *
 * \brief Contains mbbActiveHipSwingSin
 *
 * \b mbbActiveHipSwingSin
 *
 *
 * Hip active swing motor pattern for the cyclic walking & walking termination!
 *
 */
//----------------------------------------------------------------------
#ifndef __projects__biped__motor_patterns__mbbActiveHipSwingSin_h__
#define __projects__biped__motor_patterns__mbbActiveHipSwingSin_h__

#include "plugins/ib2c/tModule.h"
#include "projects/biped/motor_patterns/mMotorPattern.h"
#include "rrlib/time/time.h"

//----------------------------------------------------------------------
// External includes (system with <>, local with "")
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Internal includes with ""
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Namespace declaration
//----------------------------------------------------------------------
namespace finroc
{
namespace biped
{
namespace motor_patterns
{

//----------------------------------------------------------------------
// Forward declarations / typedefs / enums
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Class declaration
//----------------------------------------------------------------------
//! SHORT_DESCRIPTION
/*!
 *
 * Hip active swing motor pattern for the cyclic walking & walking termination!
 */
//class mbbActiveHipSwingSin : public mMotorPattern
class mbbActiveHipSwingSin : public mMotorPattern
{

//----------------------------------------------------------------------
// Ports (These are the only variables that may be declared public)
//----------------------------------------------------------------------
public:

  tParameter<double> par_max_torque;   //Example for a runtime parameter. Replace or delete it!
  tParameter<double> par_opposite_hip_factor;
  tParameter<double> par_initiation_factor;
  tParameter<double> par_hip_x_torque;
  tParameter<double> par_hip_z_torque;
  tParameter<double> par_ground_contact_threshold;
  tParameter<double> par_last_two_factor;
  tParameter<double> par_last_step_factor;
  tParameter<double> par_push_recovery_factor;
  tParameter<double> par_push_recovery_factor_stop;
  tParameter<double> par_push_recovery_factor_walking;
  tParameter<double> par_com_factor;
  tParameter<double> par_xcom_factor;
  tParameter<double> par_lateral_factor;
  tParameter<double> par_push_lateral_factor;
  tParameter<double> par_direction_factor;
  tParameter<bool> par_learning_on;
  tParameter<bool> par_obstacle_walking;
  tParameter<bool> par_slow_walking;
  tParameter<double> par_fast_hip_factor;

  tInput<double> ci_scale;   //Example for input ports. Replace or delete them!
  tInput<double> ci_lateral_scale;
  tInput<double> ci_turn_left_signal;
  tInput<double> ci_turn_right_signal;
  tInput<double> ci_ground_contact;
  tInput<double> ci_termination_last_step;
  tInput<double> ci_last_step;
  tInput<double> ci_push_recovery_stand;
  tInput<bool> ci_push_recovery_walking;
  tInput<double> ci_push_recovery_walking_left;
  tInput<double> ci_push_recovery_walking_right;
  tInput<double> ci_motor_deceleration;
  tInput<double> ci_motor_stop;
  tInput<double> ci_time_deceleration;
  tInput<double> ci_time_stop;
  tInput<double> ci_hip_motor_factor_push_recovery;
  tInput<double> ci_com_x_left;
  tInput<double> ci_com_x_right;
  tInput<double> ci_xcom_x_left;
  tInput<double> ci_xcom_x_right;
  tInput<double> ci_forward_velocity_correction;
  tInput<double> ci_initiation_activity;
  tInput<double> ci_hip_swing_action1;
  tInput<double> ci_hip_swing_action2;
  tInput<double> ci_hip_swing_action3;
  tInput<double> ci_hip_swing_action4;
  tInput<double> ci_loop_times;
  tInput<double> ci_max_torque;

  tInput<bool> ci_obstacle_walking;
  tInput<bool> ci_slow_walking_stimulation;
  tInput<bool> ci_fast_walking_stimulation;
  tInput<int> ci_obstacle_step;
  tInput<double> ci_obstacle_hip_swing_1;
  tInput<double> ci_obstacle_hip_swing_2;
  tInput<double> ci_obstacle_hip_swing_3;
  tInput<double> ci_obstacle_hip_swing_3_rear;
  tInput<double> ci_slow_walking_ahs_t1;
  tInput<double> ci_slow_walking_ahs_t2;

  tInput<double> si_angle_hip_y;
  tInput<double> si_angle_opposite_hip_y;
  tInput<double> si_foot_left_position_x;
  tInput<double> si_foot_left_position_y;
  tInput<double> si_foot_left_position_z;
  tInput<double> si_foot_right_position_x;
  tInput<double> si_foot_right_position_y;
  tInput<double> si_foot_right_position_z;

  tOutput<double> co_torque_hip_y;
  tOutput<double> co_torque_hip_y_max;
  tOutput<double> co_torque_opposite_hip_y;
  tOutput<double> co_torque_hip_x;
  tOutput<double> co_torque_opposite_hip_x;
  tOutput<double> co_torque_hip_z;
  tOutput<double> co_torque_opposite_hip_z;
  tOutput<double> co_stimulate_motor_deceleration;
  tOutput<double> co_stimulate_motor_stop;
  tOutput<double> co_start_time;
  tOutput<double> co_com_factor;
  tOutput<double> co_stimulation_torque_hip_x;
  tOutput<double> co_stimulation_torque_opposite_hip_x;
  tOutput<double> co_stimulation_torque_hip_z;
  tOutput<double> co_stimulation_torque_opposite_hip_z;
  tOutput<double> co_stimulation_angle_hip_z;
  tOutput<double> co_stimulation_angle_opposite_hip_z;
  tOutput<double> co_angle_hip_z;
  tOutput<double> co_angle_opposite_hip_z;

  tOutput<double> co_initiation_phase;




//----------------------------------------------------------------------
// Public methods and typedefs
//----------------------------------------------------------------------
public:

  mbbActiveHipSwingSin(core::tFrameworkElement *parent, const std::string &name = "ActiveHipSwingSin",
                       ib2c::tStimulationMode stimulation_mode = ib2c::tStimulationMode::AUTO, unsigned int number_of_inhibition_ports = 0,
                       mMotorPattern::tCurveMode mode = tCurveMode::eSIGMOID,
                       double max_impulse_length = 0,
                       double impulse_max_start = 0.0,
                       double impulse_max_end = 0.0,
                       double direction_factor = 0.0);



//----------------------------------------------------------------------
// Private fields and methods
//----------------------------------------------------------------------
private:
  bool active;
  double max_torque;
  double opposite_hip_factor;
  double initiation_factor;
  double hip_x_torque;
  double hip_z_torque;
  double ground_contact_threshold;
  double last_two_factor;//the last but one step before termination
  double last_step_factor;
  double push_recovery_factor;
  double push_recovery_factor_stop;
  double push_recovery_factor_walking;
  double com_factor_par;
  double xcom_factor;
  double lateral_walking_factor;
  double push_lateral_factor;
  double direction_factor;
  double turn_left_signal;
  double turn_right_signal;
  double torque_hip_y;
  double torque_hip_x;
  double torque_hip_z;
  double scale;
  double lateral_scale;
  double ground_contact;
  bool inititation_phase;
  double push_recovery_stand;
  bool push_recovery_walking;
  double push_detected_walking_left;
  double push_detected_walking_right;
  double hip_motor_factor_push_recovery;
  double motor_deceleration_active;
  double motor_stop_active;
  double termi_last_step;
  double last_step;
  //double start_time;
  rrlib::time::tTimestamp start_time_stamp;
  double time_deceleration;
  double time_stop;
  double stimulate_motor_deceleration;
  double stimulate_motor_stop;
  double com_factor;
  double com_x_left;
  double com_x_right;
  double xcom_x_left;
  double xcom_x_right;
  double com_x_diff;
  double xcom_x_diff;
  double forward_velocity_correction;
  bool learning_on;
  bool obstacle_walking;
  bool slow_walking;
  bool fast_walking;
  double fast_hip_factor;

  double hip_swing_action1;
  double hip_swing_action2;
  double hip_swing_action3;
  double hip_swing_action4;
  double turn_signal;
  //Here is the right place for your variables. Replace this line by your declarations!

  /*! Destructor
   *
   * The destructor of modules is declared private to avoid accidental deletion. Deleting
   * modules is already handled by the framework.
   */
  ~mbbActiveHipSwingSin();

  virtual void OnParameterChange();   //Might be needed to react to changes in parameters independent from Update() calls. Delete otherwise!

  virtual bool ProcessTransferFunction(double activation);

  virtual ib2c::tActivity CalculateActivity(std::vector<ib2c::tActivity> &derived_activities, double activation) const;

  virtual ib2c::tTargetRating CalculateTargetRating(double activation) const;

};

//----------------------------------------------------------------------
// End of namespace declaration
//----------------------------------------------------------------------
}
}
}



#endif
