//
// You received this file as part of Finroc
// A framework for intelligent robot control
//
// Copyright (C) Finroc GbR (finroc.org)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//
//----------------------------------------------------------------------
/*!\file    projects/biped/control/gSpinalCord.h
 *
 * \author  Tobias Luksch & Jie Zhao
 * \author  Jie Zhao(ported to Finroc)
 *
 * \date    2014-01-06
 *
 * \brief Contains gSpinalCord
 *
 * \b gSpinalCord
 *
 *
 * The group for the spinal cord during locomotion.
 *
 */
//----------------------------------------------------------------------
#ifndef __projects__biped__control__gSpinalCord_h__
#define __projects__biped__control__gSpinalCord_h__

#include "plugins/structure/tSenseControlGroup.h"
#include "rrlib/math/tVector.h"
#include "rrlib/math/tPose3D.h"

//----------------------------------------------------------------------
// External includes (system with <>, local with "")
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Internal includes with ""
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Namespace declaration
//----------------------------------------------------------------------
namespace finroc
{
namespace biped
{
namespace control
{

//----------------------------------------------------------------------
// Forward declarations / typedefs / enums
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Class declaration
//----------------------------------------------------------------------
//! SHORT_DESCRIPTION
/*!
 *
 * The group for the spinal cord during locomotion.
 */
class gSpinalCord : public structure::tSenseControlGroup
{

//----------------------------------------------------------------------
// Ports (These are the only variables that may be declared public)
//----------------------------------------------------------------------
public:
  tControllerInput<double> ci_sensor_monitor;
  tControllerInput<double> ci_stimulation_stand;
  tControllerInput<double> ci_stimulation_walking;
  tControllerInput<double> ci_stimulation_walking_lateral;
  tControllerInput<double> ci_walking_scale_factor;
  tControllerInput<double> ci_turn_left_modulation_signal;
  tControllerInput<double> ci_turn_right_modulation_signal;
  tControllerInput<double> ci_left_stimulation_walking_initiation;
  tControllerInput<double> ci_right_stimulation_walking_initiation;
  tControllerInput<double> ci_stimulation_walking_termination;
  tControllerInput<double> ci_walking_scale_laterally;
  tControllerInput<double> ci_stiffness_factor;
  tControllerInput<double> ci_quiet_stand;
  tControllerInput<double> ci_push_recovery_stand;
  tControllerInput<double> ci_push_recovery_walking;
  tControllerInput<double> ci_push_detected_walking_left;
  tControllerInput<double> ci_push_detected_walking_right;
  tControllerInput<double> ci_non_useful_target_rating;
  tControllerInput<bool> ci_push_detected_walking_lateral;
  tControllerInput<bool> ci_push_started;
  tControllerInput<bool> ci_push_started_x;
  tControllerInput<bool> ci_push_detected_x;
  tControllerInput<bool> ci_push_detected_walking;

  tControllerInput<double> ci_actual_tilt_direction;
  tControllerInput<double> ci_actual_tilt_amount;
  tControllerInput<double> ci_adjusted_tilt_direction;
  tControllerInput<double> ci_adjusted_tilt_amount;
  tControllerInput<double> ci_accel_direction;
  tControllerInput<double> ci_accel_amount;
  tControllerInput<double> ci_velocity_direction;
  tControllerInput<double> ci_velocity_amount;

  tControllerInput<double> ci_linear_velocity_x;
  tControllerInput<double> ci_linear_velocity_y;
  tControllerInput<double> ci_actual_accel_x;
  tControllerInput<double> ci_actual_accel_y;
  tControllerInput<double> ci_actual_accel_z;
  tControllerInput<double> ci_actual_omega_x;
  tControllerInput<double> ci_actual_omega_y;
  tControllerInput<double> ci_actual_omega_z;
  tControllerInput<double> ci_actual_roll;
  tControllerInput<double> ci_actual_pitch;
  tControllerInput<double> ci_actual_yaw;
  tControllerInput<double> ci_adjusted_accel_x;
  tControllerInput<double> ci_adjusted_accel_y;
  tControllerInput<double> ci_adjusted_accel_z;
  tControllerInput<double> ci_adjusted_roll;
  tControllerInput<double> ci_adjusted_pitch;
  tControllerInput<double> ci_torso_force_y;
  tControllerInput<double> ci_torso_force_x;
  tControllerInput<bool> ci_reset_simulation_experiments;


  // DEC
  tControllerInput<double> ci_disturbance_estimation_activity;
  tControllerInput<double> ci_gravity_torque_activity;
  tControllerInput<double> ci_space_rotation_activity;
  tControllerInput<double> ci_external_torque_activity;
  tControllerInput<double> ci_space_translational_acc_activity;
  tControllerInput<double> ci_target_body_space_angle;
  tControllerInput<double> ci_angle_body_space;

  //PSO
  tControllerOutput<double> co_slow_walking_ahs_max_torque;
  tControllerOutput<double> co_slow_walking_ahs_t1;
  tControllerOutput<double> co_slow_walking_ahs_t2;
  tControllerOutput<double> co_slow_walking_lp_max_torque;
  tControllerOutput<double> co_slow_walking_lp_t1;
  tControllerOutput<double> co_slow_walking_lp_t2;
  tControllerOutput<double> co_slow_walking_lh_par1;
  tControllerOutput<double> co_slow_walking_lh_par2;
  tControllerOutput<double> co_fast_walking_lh_par1;
  tControllerOutput<double> co_fast_walking_lh_par2;
  tControllerOutput<double> co_slow_walking_knee_angle;
  tControllerOutput<double> co_slow_walking_knee_angle_low;
  tControllerOutput<double> co_slow_walking_knee_angle_high;
  tControllerOutput<double> co_slow_walking_knee_velocity;
  tControllerOutput<double> co_slow_walking_lk_hip_angle_low;
  tControllerOutput<double> co_slow_walking_lk_hip_angle_high;
  tControllerOutput<double> co_slow_walking_lk_increment_par1;
  tControllerOutput<double> co_slow_walking_lk_increment_par2;
  tControllerOutput<double> co_slow_walking_max_impulse;
  tControllerOutput<double> co_slow_walk_stimulation_knee_angle;
  tControllerOutput<double> co_fast_walk_stimulation_knee_angle;
  tControllerOutput<double>  co_slow_walk_stimulation_knee_angle_activity;

  tControllerOutput<double> co_stimulation_stand;
  tControllerOutput<double> co_stiffness_factor;
  tControllerOutput<double> co_quiet_stand;
  tControllerOutput<double> co_ground_contact;

  tControllerOutput<double> co_linear_velocity_x;
  tControllerOutput<double> co_linear_velocity_y;
  tControllerOutput<double> co_actual_accel_x;
  tControllerOutput<double> co_actual_accel_y;
  tControllerOutput<double> co_actual_accel_z;
  tControllerOutput<double> co_actual_omega_x;
  tControllerOutput<double> co_actual_omega_y;
  tControllerOutput<double> co_actual_omega_z;
  tControllerOutput<double> co_actual_roll;
  tControllerOutput<double> co_actual_pitch;
  tControllerOutput<double> co_actual_yaw;
  tControllerOutput<double> co_adjusted_accel_x;
  tControllerOutput<double> co_adjusted_accel_y;
  tControllerOutput<double> co_adjusted_accel_z;
  tControllerOutput<double> co_adjusted_roll;
  tControllerOutput<double> co_adjusted_pitch;

  tControllerOutput<double> co_com_x;
  tControllerOutput<double> co_com_y;
  tControllerOutput<double> co_xcom_x;
  tControllerOutput<double> co_xcom_y;
  tControllerOutput<double> co_com_error_y;
  tControllerOutput<double> co_com_error_x_left;
  tControllerOutput<double> co_com_error_x_right;
  tControllerOutput<double> co_inv_com_error_y;
  tControllerOutput<double> co_left_com_x;
  tControllerOutput<double> co_right_com_x;
  tControllerOutput<double> co_left_com_y;
  tControllerOutput<double> co_right_com_y;
  tControllerOutput<double> co_left_xcom_x;
  tControllerOutput<double> co_right_xcom_x;
  tControllerOutput<double> co_left_xcom_y;
  tControllerOutput<double> co_right_xcom_y;
  tControllerOutput<double> co_com_ratio;
  tControllerOutput<double> co_push_recovery_stand;
  tControllerOutput<double> co_push_recovery_walking;
  tControllerOutput<double> co_bent_knee;
  tControllerOutput<double> co_push_detected_walking_left;
  tControllerOutput<double> co_push_detected_walking_right;
  tControllerOutput<double> co_time_deceleration;
  tControllerOutput<double> co_time_stop;
  tControllerOutput<double> co_hip_motor_factor_push_recovery;

  tControllerOutput<double> co_stimulation_relax_spine_x;
  tControllerOutput<double> co_relax_spine_x_angle_adjustment;
  tControllerOutput<double> co_stimulation_relax_spine_y;
  tControllerOutput<double> co_relax_spine_y_angle_adjustment;
  tControllerOutput<double> co_stimulation_relax_hip_y;
  tControllerOutput<double> co_left_relax_hip_y_angle_adjustment;
  tControllerOutput<double> co_right_relax_hip_y_angle_adjustment;

  tControllerOutput<double> co_stimulation_reduce_tension_hip_y;
  tControllerOutput<double> co_left_reduce_tension_hip_y_angle_adjustment;
  tControllerOutput<double> co_right_reduce_tension_hip_y_angle_adjustment;
  tControllerOutput<double> co_stimulation_reduce_tension_ankle_y;
  tControllerOutput<double> co_left_reduce_tension_ankle_y_angle_adjustment;
  tControllerOutput<double> co_right_reduce_tension_ankle_y_angle_adjustment;

  tControllerOutput<double> co_stimulation_relax_knee;
  tControllerOutput<double> co_left_relax_knee_angle_adjustment;
  tControllerOutput<double> co_right_relax_knee_angle_adjustment;
  tControllerOutput<double> co_stimulation_balance_ankle_pitch;
  tControllerOutput<double> co_left_balance_ankle_pitch_angle_adjustment;
  tControllerOutput<double> co_right_balance_ankle_pitch_angle_adjustment;

  //walking skills
  tControllerOutput<double> co_left_stimulation_walking_initiation;
  tControllerOutput<double> co_right_stimulation_walking_initiation;
  tControllerOutput<double> co_left_stimulation_walking_lateral_initiation;
  tControllerOutput<double> co_right_stimulation_walking_lateral_initiation;
  tControllerOutput<double> co_activity_walking;
  tControllerOutput<double> co_walking_scale_factor;
  tControllerOutput<double> co_walking_scale_laterally;
  tControllerOutput<double> co_walking_lateral_activity;
  tControllerOutput<double> co_termination_scale_factor;
  tControllerOutput<double> co_left_forward_velocity_correction;
  tControllerOutput<double> co_right_forward_velocity_correction;
  tControllerOutput<double> co_left_lateral_velocity_correction;
  tControllerOutput<double> co_right_lateral_velocity_correction;
  tControllerOutput<double> co_left_frontal_velocity_correction;
  tControllerOutput<double> co_right_frontal_velocity_correction;

  tControllerOutput<double> co_left_stimulation_walking_phase_1;
  tControllerOutput<double> co_left_stimulation_walking_phase_2;
  tControllerOutput<double> co_left_stimulation_walking_phase_3;
  tControllerOutput<double> co_left_stimulation_walking_phase_4;
  tControllerOutput<double> co_left_stimulation_walking_phase_5;
  tControllerOutput<double> co_left_stimulation_lift_leg;

  tControllerOutput<double> co_right_stimulation_walking_phase_1;
  tControllerOutput<double> co_right_stimulation_walking_phase_2;
  tControllerOutput<double> co_right_stimulation_walking_phase_3;
  tControllerOutput<double> co_right_stimulation_walking_phase_4;
  tControllerOutput<double> co_right_stimulation_walking_phase_5;
  tControllerOutput<double> co_right_stimulation_lift_leg;

  tControllerOutput<double> co_left_stimulation_walking_lateral_phase_1;
  tControllerOutput<double> co_left_stimulation_walking_lateral_phase_2;
  tControllerOutput<double> co_left_stimulation_walking_lateral_phase_3;
  tControllerOutput<double> co_left_stimulation_walking_lateral_phase_4;
  tControllerOutput<double> co_left_stimulation_walking_lateral_phase_5;

  tControllerOutput<double> co_right_stimulation_walking_lateral_phase_1;
  tControllerOutput<double> co_right_stimulation_walking_lateral_phase_2;
  tControllerOutput<double> co_right_stimulation_walking_lateral_phase_3;
  tControllerOutput<double> co_right_stimulation_walking_lateral_phase_4;
  tControllerOutput<double> co_right_stimulation_walking_lateral_phase_5;

  tControllerOutput<double> co_step_left_leg_left;
  tControllerOutput<double> co_step_left_leg_right;
  tControllerOutput<double> co_step_right_leg_left;
  tControllerOutput<double> co_step_right_leg_right;
  tControllerOutput<double> co_s_leg_left;
  tControllerOutput<double> co_s_leg_right;

  tControllerOutput<double> co_walking_termination;
  tControllerOutput<double> co_termi_last_step;
  tControllerOutput<double> co_last_step;
  tControllerOutput<double> co_last_step_state;
  tControllerOutput<double> co_termi_lock_hip;
  tControllerOutput<double> co_left_leg_angle_offset;
  tControllerOutput<double> co_right_leg_angle_offset;
  tControllerOutput<double> co_stop_upright_trunk;
  tControllerOutput<double> co_maintain_stable;
  tControllerOutput<double> co_termination_hold_position;
  tControllerOutput<double> co_termination_turn_off_upright;


  tControllerOutput<double> co_left_ground_contact;
  tControllerOutput<double> co_right_ground_contact;
  tControllerOutput<double> co_left_foot_load;
  tControllerOutput<double> co_right_foot_load;
  tControllerOutput<double> co_left_force_z;
  tControllerOutput<double> co_right_force_z;

  tControllerOutput<double> co_turn_left_signal_to_left;
  tControllerOutput<double> co_turn_left_signal_to_right;
  tControllerOutput<double> co_turn_right_signal_to_left;
  tControllerOutput<double> co_turn_right_signal_to_right;

  //PSO
  tControllerOutput<double> co_walking_velocity;
  tControllerOutput<double> co_walking_distance;
  tControllerOutput<double> co_step_length;

  //DEC
  tControllerOutput<double> co_disturbance_estimation_activity;
  tControllerOutput<double> co_target_body_space_angle;
  tControllerOutput<double> co_angle_body_foot;
  tControllerOutput<double> co_estim_angle_deviation;
  tControllerOutput<double> co_estim_foot_space_angle;
  tControllerOutput<double> co_estim_foot_space_angle_lateral_left;
  tControllerOutput<double> co_estim_foot_space_angle_lateral_right;
  tControllerOutput<double> co_estim_angle_deviation_lateral_left;
  tControllerOutput<double> co_estim_angle_deviation_lateral_right;
  tControllerOutput<double> co_angle_body_foot_left_lateral;
  tControllerOutput<double> co_angle_body_foot_right_lateral;

  tControllerOutput<double> co_angle_trunk_space_prop;
  tControllerOutput<double> co_angle_trunk_space_dist;
  tControllerOutput<double> co_angle_trunk_space_lateral_prop;
  tControllerOutput<double> co_angle_trunk_space_lateral_dist;
  tControllerOutput<double> co_angle_body_space_prop;
  tControllerOutput<double> co_angle_body_space_dist;
  tControllerOutput<double> co_angle_body_space;
  tControllerOutput<double> co_angle_trunk_thigh_space_gravity;

  tControllerOutput<double> co_angle_body_space_lateral_prop_left;
  tControllerOutput<double> co_angle_body_space_lateral_prop_right;
  tControllerOutput<double> co_angle_body_space_lateral_dist_left;
  tControllerOutput<double> co_angle_body_space_lateral_dist_right;
  tControllerOutput<double> co_angle_body_space_lateral;
  tControllerOutput<double> co_target_angle_trunk_space;
  tControllerOutput<double> co_target_angle_trunk_space_lateral;
  tControllerOutput<double> co_target_angle_arm_trunk;
  tControllerOutput<double> co_estim_foot_space_angle_new;
  tControllerOutput<bool> co_walking_compensation_roll;

  // learning & optimization
  tControllerOutput<double> co_state_value;
  tControllerOutput<double> co_leg_propel_action1;
  tControllerOutput<double> co_leg_propel_action2;
  tControllerOutput<double> co_leg_propel_action3;
  tControllerOutput<double> co_leg_propel_action4;

  tControllerOutput<double> co_hip_swing_action1;
  tControllerOutput<double> co_hip_swing_action2;
  tControllerOutput<double> co_hip_swing_action3;
  tControllerOutput<double> co_hip_swing_action4;
  tControllerOutput<double> co_loop_times;
  tControllerOutput<double> co_left_learning_ankle_stiffness;
  tControllerOutput<double> co_right_learning_ankle_stiffness;

  tControllerOutput<double> co_left_learning_torque_factor;
  tControllerOutput<double> co_right_learning_torque_factor;

  tControllerOutput<double> co_obstacle_hip_swing_1;
  tControllerOutput<double> co_obstacle_hip_swing_2;
  tControllerOutput<double> co_obstacle_hip_swing_3;
  tControllerOutput<double> co_obstacle_hip_swing_3_rear;
  tControllerOutput<double> co_obstacle_knee_flexion_1;
  tControllerOutput<double> co_obstacle_knee_flexion_2;
  tControllerOutput<double> co_obstacle_knee_flexion_2_rear;
  tControllerOutput<double> co_obstacle_lock_hip_1;
  tControllerOutput<double> co_obstacle_lock_hip_2;
  tControllerOutput<double> co_obstacle_lock_hip_3;

  tControllerOutput<bool> co_obstacle_walking_stimulation;
  tControllerOutput<int> co_obstacle_step;

  // PSO Slow Walking

  tControllerOutput<bool> co_slow_walking_stimulation;
  tControllerOutput<bool> co_slow_walking_on;
  tControllerOutput<bool> co_fast_walking_stimulation;
  tControllerOutput<bool> co_fast_walking_on;
  tControllerOutput<int> co_slow_walking_step;

  tSensorInput<double> si_left_leg_foot_load;
  tSensorInput<double> si_left_leg_activity_walking_phase_4;
  tSensorInput<double> si_left_leg_target_rating_walking_phase_4;
  tSensorInput<double> si_left_leg_activity_walking_phase_5;
  tSensorInput<double> si_left_leg_target_rating_walking_phase_5;

  tSensorInput<double> si_left_leg_activity_termination_phase_4;
  tSensorInput<double> si_left_leg_target_rating_termination_phase_4;
  tSensorInput<double> si_left_leg_activity_termination_phase_5;
  tSensorInput<double> si_left_leg_target_rating_termination_phase_5;

  tSensorInput<double> si_left_shoulder_x_torque;
  tSensorInput<double> si_left_shoulder_x_angle;
  tSensorInput<double> si_left_shoulder_y_torque;
  tSensorInput<double> si_left_shoulder_y_angle;
  tSensorInput<double> si_left_elbow_torque;
  tSensorInput<double> si_left_elbow_angle;

  tSensorInput<double> si_spine_x_torque;
  tSensorInput<double> si_spine_x_angle;
  tSensorInput<double> si_spine_y_torque;
  tSensorInput<double> si_spine_y_angle;
  tSensorInput<double> si_spine_z_torque;
  tSensorInput<double> si_spine_z_angle;
  tSensorInput<double> si_left_hip_x_torque;
  tSensorInput<double> si_left_hip_x_angle;
  tSensorInput<double> si_left_hip_y_torque;
  tSensorInput<double> si_left_hip_y_angle;
  tSensorInput<double> si_left_hip_z_torque;
  tSensorInput<double> si_left_hip_z_angle;

  tSensorInput<double> si_left_knee_torque;
  tSensorInput<double> si_left_knee_angle;
  tSensorInput<double> si_left_ankle_y_torque;
  tSensorInput<double> si_left_ankle_y_angle;
  tSensorInput<double> si_left_ankle_x_torque;
  tSensorInput<double> si_left_ankle_x_angle;
  tSensorInput<double> si_left_force_z;
  tSensorInput<double> si_left_torque_x;
  tSensorInput<double> si_left_torque_y;
  tSensorInput<double> si_left_inner_toe_force;
  tSensorInput<double> si_left_outer_toe_force;
  tSensorInput<double> si_left_inner_heel_force;
  tSensorInput<double> si_left_outer_heel_force;
  tSensorInput<double> si_left_ground_contact;
  tSensorInput<double> si_left_force_deviation;


  tSensorInput<double> si_right_leg_foot_load;
  tSensorInput<double> si_right_leg_activity_walking_phase_4;
  tSensorInput<double> si_right_leg_target_rating_walking_phase_4;
  tSensorInput<double> si_right_leg_activity_walking_phase_5;
  tSensorInput<double> si_right_leg_target_rating_walking_phase_5;

  tSensorInput<double> si_right_leg_activity_termination_phase_4;
  tSensorInput<double> si_right_leg_target_rating_termination_phase_4;
  tSensorInput<double> si_right_leg_activity_termination_phase_5;
  tSensorInput<double> si_right_leg_target_rating_termination_phase_5;

  tSensorInput<double> si_right_shoulder_x_torque;
  tSensorInput<double> si_right_shoulder_x_angle;
  tSensorInput<double> si_right_shoulder_y_torque;
  tSensorInput<double> si_right_shoulder_y_angle;
  tSensorInput<double> si_right_elbow_torque;
  tSensorInput<double> si_right_elbow_angle;

  tSensorInput<double> si_right_hip_x_torque;
  tSensorInput<double> si_right_hip_x_angle;
  tSensorInput<double> si_right_hip_y_torque;
  tSensorInput<double> si_right_hip_y_angle;
  tSensorInput<double> si_right_hip_z_torque;
  tSensorInput<double> si_right_hip_z_angle;
  tSensorInput<double> si_right_knee_torque;
  tSensorInput<double> si_right_knee_angle;
  tSensorInput<double> si_right_ankle_y_torque;
  tSensorInput<double> si_right_ankle_y_angle;
  tSensorInput<double> si_right_ankle_x_torque;
  tSensorInput<double> si_right_ankle_x_angle;
  tSensorInput<double> si_right_force_z;
  tSensorInput<double> si_right_torque_x;
  tSensorInput<double> si_right_torque_y;
  tSensorInput<double> si_right_inner_toe_force;
  tSensorInput<double> si_right_outer_toe_force;
  tSensorInput<double> si_right_inner_heel_force;
  tSensorInput<double> si_right_outer_heel_force;
  tSensorInput<double> si_right_ground_contact;
  tSensorInput<double> si_right_force_deviation;

  tSensorInput<double> si_left_cop_x;
  tSensorInput<double> si_left_cop_y;
  tSensorInput<double> si_right_cop_x;
  tSensorInput<double> si_right_cop_y;

  tSensorInput<double> si_lock_hip_left_torque;
  tSensorInput<double> si_lock_hip_right_torque;
  tSensorInput<double> si_pelvis_position_x;
  tSensorInput<double> si_pelvis_position_y;
  tSensorInput<double> si_left_foot_position_x;
  tSensorInput<double> si_left_foot_position_y;
  tSensorInput<double> si_left_foot_position_z;
  tSensorInput<double> si_right_foot_position_x;
  tSensorInput<double> si_right_foot_position_y;
  tSensorInput<double> si_right_foot_position_z;

  tSensorOutput<double> so_left_force_z;
  tSensorOutput<double> so_left_torque_x;
  tSensorOutput<double> so_left_torque_y;
  tSensorOutput<double> so_left_inner_toe_force;
  tSensorOutput<double> so_left_outer_toe_force;
  tSensorOutput<double> so_left_inner_heel_force;
  tSensorOutput<double> so_left_outer_heel_force;
  tSensorOutput<double> so_right_force_z;
  tSensorOutput<double> so_right_torque_x;
  tSensorOutput<double> so_right_torque_y;
  tSensorOutput<double> so_right_inner_toe_force;
  tSensorOutput<double> so_right_outer_toe_force;
  tSensorOutput<double> so_right_inner_heel_force;
  tSensorOutput<double> so_right_outer_heel_force;
  tSensorOutput<double> so_left_force_deviation;
  tSensorOutput<double> so_right_force_deviation;
  tSensorOutput<double> so_left_foot_load;
  tSensorOutput<double> so_right_foot_load;
  tSensorOutput<double> so_ground_contact;
  tSensorOutput<double> so_left_com_x;
  tSensorOutput<double> so_right_com_x;
  tSensorOutput<double> so_left_xcom_x;
  tSensorOutput<double> so_right_xcom_x;
  tSensorOutput<double> so_xcom_x_left_correction;
  tSensorOutput<double> so_xcom_x_right_correction;
  tSensorOutput<double> so_xcom_y_left_correction;
  tSensorOutput<double> so_xcom_y_right_correction;
  tSensorOutput<double> so_falling_down;

  tSensorOutput<double> so_activity_stand;
  tSensorOutput<double> so_target_rating_stand;
  tSensorOutput<double> so_target_rating_fusion_adjust_angle;
  tSensorOutput<double> so_activity_walking;
  tSensorOutput<double> so_target_rating_walking;
  tSensorOutput<double> so_left_activity_walking_initiation;
  tSensorOutput<double> so_left_target_rating_walking_initiation;
  tSensorOutput<double> so_right_activity_walking_initiation;
  tSensorOutput<double> so_right_target_rating_walking_initiation;

  tSensorOutput<double> so_activity_walking_termination;
  tSensorOutput<double> so_target_rating_walking_termination;
  tSensorOutput<double> so_termination_finished;
  tSensorOutput<double> so_left_stability_x;
  tSensorOutput<double> so_left_stability_y;
  tSensorOutput<double> so_right_stability_x;
  tSensorOutput<double> so_right_stability_y;

  tSensorOutput<double> so_new_running;
  tSensorOutput<double> so_loop_times;


//----------------------------------------------------------------------
// Public methods and typedefs
//----------------------------------------------------------------------
public:

  gSpinalCord(core::tFrameworkElement *parent, const std::string &name = "SpinalCord",
              const std::string &structure_config_file = __FILE__".xml");

//----------------------------------------------------------------------
// Private fields and methods
//----------------------------------------------------------------------
private:

  /*! Destructor
   *
   * The destructor of modules is declared private to avoid accidental deletion. Deleting
   * modules is already handled by the framework.
   */
  ~gSpinalCord();

};

//----------------------------------------------------------------------
// End of namespace declaration
//----------------------------------------------------------------------
}
}
}



#endif
