//
// You received this file as part of Finroc
// A framework for intelligent robot control
//
// Copyright (C) Finroc GbR (finroc.org)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//
//----------------------------------------------------------------------
/*!\file    projects/biped/control/gControl.cpp
 *
 * \author  Jie Zhao (ported to finroc)
 * \author Tobias Luksch
 *
 * \date    2014-01-08
 *
 */
//----------------------------------------------------------------------
#include "projects/biped/control/gControl.h"
#include "projects/biped/control/gBrain.h"
#include "projects/biped/control/gSpinalCord.h"
#include "projects/biped/control/gUpperTrunkGroup.h"
#include "projects/biped/control/gLowerTrunkGroup.h"
#include "projects/biped/control/gLegGroup.h"
#include "projects/biped/utils/mConvertVectorToSingleValue.h"
#include "projects/biped/utils/mTransmissionUnit.h"
#include "rrlib/util/sFileIOUtils.h"
#include "rrlib/math/tPose3D.h"

//----------------------------------------------------------------------
// External includes (system with <>, local with "")
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Internal includes with ""
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Debugging
//----------------------------------------------------------------------
#include <cassert>

//----------------------------------------------------------------------
// Namespace usage
//----------------------------------------------------------------------
//using namespace finroc::biped::motor_patterns;
//using namespace finroc::biped::reflexes;
//using namespace finroc::biped::skills;
using namespace finroc::biped::utils;

//----------------------------------------------------------------------
// Namespace declaration
//----------------------------------------------------------------------
namespace finroc
{
namespace biped
{
namespace control
{

//----------------------------------------------------------------------
// Forward declarations / typedefs / enums
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Const values
//----------------------------------------------------------------------
static runtime_construction::tStandardCreateModuleAction<gControl> cCREATE_ACTION_FOR_G_CONTROL("Control");

//----------------------------------------------------------------------
// Implementation
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// gControl constructor
//----------------------------------------------------------------------
gControl::gControl(core::tFrameworkElement *parent, const std::string &name,
                   const std::string &structure_config_file) :
  tSenseControlGroup(parent, name, structure_config_file, false) // change to 'true' to make group's ports shared (so that ports in other processes can connect to its sensor outputs and controller inputs)
{
  gBrain* brain = new gBrain(this, "The Brain");
  gSpinalCord* spinal_cord = new gSpinalCord(this, "Spinal Cord");
  this->upper_trunk = new gUpperTrunkGroup(this, "Upper Trunk");
  this->lower_trunk = new gLowerTrunkGroup(this, "Lower Trunk");
  this->left_leg = new gLegGroup(this, "Left Leg");
  this->right_leg = new gLegGroup(this, "Right Leg");
  mTransmissionUnit * trans_abs_left_lock_hip_torque_control = new mTransmissionUnit(this, "Transmission Torque Lock Hip Left Control");

  mTransmissionUnit * trans_abs_right_lock_hip_torque_control = new mTransmissionUnit(this, "Transmission Torque Lock Hip Right Control");

  mConvertVectorToSingleValue* convert_pose_to_value = new mConvertVectorToSingleValue(this, "Pose Converter");

  for (size_t i = 0; i < 3; i++)
  {
    this->si_ins_accel.emplace_back("Ins Accel " + std::to_string(i), this);
    this->si_ins_omega.emplace_back("Ins Omega " + std::to_string(i), this);
    this->si_ins_rot.emplace_back("Ins Rot. " + std::to_string(i), this);
  }

  // add edges
  // --------------------------------------------------
  // distribute sensor information, some of which is done by ImportSI commands below


  // downward activity flow
  this->GetControllerInputs().ConnectByName(brain->GetControllerInputs(), false);
  this->ci_sensor_monitor.ConnectTo(spinal_cord->ci_sensor_monitor);
  this->ci_sensor_monitor.ConnectTo(this->upper_trunk->ci_sensor_monitor);
  this->ci_sensor_monitor.ConnectTo(this->lower_trunk->ci_sensor_monitor);
  this->ci_sensor_monitor.ConnectTo(this->left_leg->ci_sensor_monitor);
  this->ci_sensor_monitor.ConnectTo(this->right_leg->ci_sensor_monitor);

  brain->co_stimulation_stand.ConnectTo(spinal_cord->ci_stimulation_stand);
  brain->co_stimulation_walking.ConnectTo(spinal_cord->ci_stimulation_walking);
  brain->co_stimulation_walking_lateral.ConnectTo(spinal_cord->ci_stimulation_walking_lateral);
  brain->co_walking_scale_factor.ConnectTo(spinal_cord->ci_walking_scale_factor);
  brain->co_turn_left_modulation_signal.ConnectTo(spinal_cord->ci_turn_left_modulation_signal);
  brain->co_turn_right_modulation_signal.ConnectTo(spinal_cord->ci_turn_right_modulation_signal);
  brain->co_left_stimulation_walking_initiation.ConnectTo(spinal_cord->ci_left_stimulation_walking_initiation);
  brain->co_right_stimulation_walking_initiation.ConnectTo(spinal_cord->ci_right_stimulation_walking_initiation);
  brain->co_stimulation_walking_termination.ConnectTo(spinal_cord->ci_stimulation_walking_termination);
  brain->co_walking_laterally.ConnectTo(spinal_cord->ci_walking_scale_laterally);
  brain->co_stiffness_factor.ConnectTo(spinal_cord->ci_stiffness_factor);
  brain->co_quiet_stand.ConnectTo(spinal_cord->ci_quiet_stand);
  brain->co_push_recovery_stand.ConnectTo(spinal_cord->ci_push_recovery_stand);
  brain->co_push_recovery_walking.ConnectTo(spinal_cord->ci_push_recovery_walking);
  brain->co_push_detected_walking_left.ConnectTo(spinal_cord->ci_push_detected_walking_left);
  brain->co_push_detected_walking_right.ConnectTo(spinal_cord->ci_push_detected_walking_right);
  brain->co_actual_tilt_direction.ConnectTo(spinal_cord->ci_actual_tilt_direction);
  brain->co_actual_tilt_amount.ConnectTo(spinal_cord->ci_actual_tilt_amount);
  brain->co_adjusted_tilt_direction.ConnectTo(spinal_cord->ci_adjusted_tilt_direction);
  brain->co_adjusted_tilt_amount.ConnectTo(spinal_cord->ci_adjusted_tilt_amount);
  brain->co_accel_direction.ConnectTo(spinal_cord->ci_accel_direction);
  brain->co_accel_amount.ConnectTo(spinal_cord->ci_accel_amount);
  brain->co_velocity_direction.ConnectTo(spinal_cord->ci_velocity_direction);
  brain->co_velocity_amount.ConnectTo(spinal_cord->ci_velocity_amount);
  brain->co_linear_velocity_x.ConnectTo(spinal_cord->ci_linear_velocity_x);
  brain->co_linear_velocity_y.ConnectTo(spinal_cord->ci_linear_velocity_y);
  brain->co_actual_accel_x.ConnectTo(spinal_cord->ci_actual_accel_x);
  brain->co_actual_accel_y.ConnectTo(spinal_cord->ci_actual_accel_y);
  brain->co_actual_accel_z.ConnectTo(spinal_cord->ci_actual_accel_z);
  brain->co_actual_omega_x.ConnectTo(spinal_cord->ci_actual_omega_x);
  brain->co_actual_omega_y.ConnectTo(spinal_cord->ci_actual_omega_y);
  brain->co_actual_omega_z.ConnectTo(spinal_cord->ci_actual_omega_z);
  brain->co_actual_roll.ConnectTo(spinal_cord->ci_actual_roll);
  brain->co_actual_pitch.ConnectTo(spinal_cord->ci_actual_pitch);
  brain->co_actual_yaw.ConnectTo(spinal_cord->ci_actual_yaw);
  brain->co_adjusted_accel_x.ConnectTo(spinal_cord->ci_adjusted_accel_x);
  brain->co_adjusted_accel_y.ConnectTo(spinal_cord->ci_adjusted_accel_y);
  brain->co_adjusted_accel_z.ConnectTo(spinal_cord->ci_adjusted_accel_z);
  brain->co_adjusted_roll.ConnectTo(spinal_cord->ci_adjusted_roll);
  brain->co_adjusted_pitch.ConnectTo(spinal_cord->ci_adjusted_pitch);

  brain->co_disturbance_estimation_activity.ConnectTo(spinal_cord->ci_disturbance_estimation_activity);
  brain->co_gravity_torque_activity.ConnectTo(spinal_cord->ci_gravity_torque_activity);
  brain->co_space_rotation_activity.ConnectTo(spinal_cord->ci_space_rotation_activity);
  brain->co_external_torque_activity.ConnectTo(spinal_cord->ci_external_torque_activity);
  brain->co_space_translational_acc_activity.ConnectTo(spinal_cord->ci_space_translational_acc_activity);
  brain->co_target_body_space_angle.ConnectTo(spinal_cord->ci_target_body_space_angle);


  this->ci_reset_simulation_experiments.ConnectTo(spinal_cord->ci_reset_simulation_experiments);
  this->ci_reset_simulation_experiments.ConnectTo(this->left_leg->ci_reset_simulation_experiments);
  this->ci_reset_simulation_experiments.ConnectTo(this->right_leg->ci_reset_simulation_experiments);
  this->ci_reset_simulation_experiments.ConnectTo(this->lower_trunk->ci_reset_simulation_experiments);
  this->ci_reset_simulation_experiments.ConnectTo(this->upper_trunk->ci_reset_simulation_experiments);

  this->ci_push_started.ConnectTo(spinal_cord->ci_push_started);
  this->ci_push_started_x.ConnectTo(spinal_cord->ci_push_started_x);
  this->ci_push_detected_x.ConnectTo(spinal_cord->ci_push_detected_x);
  this->ci_push_started_x.ConnectTo(this->lower_trunk->ci_push_started_x);
  this->ci_push_y.ConnectTo(spinal_cord->ci_push_detected_walking_lateral);
  this->ci_push_x.ConnectTo(spinal_cord->ci_push_detected_walking);
  this->ci_torso_force_y.ConnectTo(spinal_cord->ci_torso_force_y);
  this->ci_torso_force_x.ConnectTo(spinal_cord->ci_torso_force_x);

  // walking termination connections -> control the joint position after walking termination
  this->ci_walking_termination.ConnectTo(this->left_leg->ci_stimulation_termination_button);
  this->ci_walking_termination.ConnectTo(this->lower_trunk->ci_stimulation_termination_button);
  this->ci_walking_termination.ConnectTo(this->right_leg->ci_stimulation_termination_button);

  spinal_cord->co_stimulation_stand.ConnectTo(this->upper_trunk->ci_stimulation_stand);
  spinal_cord->co_walking_scale_factor.ConnectTo(this->upper_trunk->ci_walking_scale_factor);
  spinal_cord->co_termination_scale_factor.ConnectTo(this->upper_trunk->ci_termination_scale_factor);
  spinal_cord->co_ground_contact.ConnectTo(this->upper_trunk->ci_ground_contact);
  spinal_cord->co_left_stimulation_walking_initiation.ConnectTo(this->upper_trunk->ci_left_stimulation_walking_initiation);
  spinal_cord->co_right_stimulation_walking_initiation.ConnectTo(this->upper_trunk->ci_right_stimulation_walking_initiation);
  spinal_cord->co_activity_walking.ConnectTo(this->upper_trunk->ci_activity_walking);
  spinal_cord->co_stiffness_factor.ConnectTo(this->upper_trunk->ci_stiffness_factor);
  spinal_cord->co_quiet_stand.ConnectTo(this->upper_trunk->ci_quiet_stand);
  spinal_cord->co_last_step_state.ConnectTo(this->upper_trunk->ci_last_step_state);
  spinal_cord->co_actual_omega_y.ConnectTo(this->upper_trunk->ci_actual_omega_y);
  spinal_cord->co_actual_pitch.ConnectTo(this->upper_trunk->ci_actual_pitch);
  spinal_cord->co_adjusted_roll.ConnectTo(this->upper_trunk->ci_adjusted_roll);
  spinal_cord->co_adjusted_pitch.ConnectTo(this->upper_trunk->ci_adjusted_pitch);
  spinal_cord->co_left_stimulation_walking_phase_1.ConnectTo(this->upper_trunk->ci_left_stimulation_walking_phase_1);
  spinal_cord->co_left_stimulation_walking_phase_2.ConnectTo(this->upper_trunk->ci_left_stimulation_walking_phase_2);
  spinal_cord->co_left_stimulation_walking_phase_3.ConnectTo(this->upper_trunk->ci_left_stimulation_walking_phase_3);
  spinal_cord->co_left_stimulation_walking_phase_4.ConnectTo(this->upper_trunk->ci_left_stimulation_walking_phase_4);
  spinal_cord->co_left_stimulation_walking_phase_5.ConnectTo(this->upper_trunk->ci_left_stimulation_walking_phase_5);
  spinal_cord->co_left_stimulation_lift_leg.ConnectTo(this->upper_trunk->ci_left_stimulation_lift_leg);
  spinal_cord->co_right_stimulation_walking_phase_1.ConnectTo(this->upper_trunk->ci_right_stimulation_walking_phase_1);
  spinal_cord->co_right_stimulation_walking_phase_2.ConnectTo(this->upper_trunk->ci_right_stimulation_walking_phase_2);
  spinal_cord->co_right_stimulation_walking_phase_3.ConnectTo(this->upper_trunk->ci_right_stimulation_walking_phase_3);
  spinal_cord->co_right_stimulation_walking_phase_4.ConnectTo(this->upper_trunk->ci_right_stimulation_walking_phase_4);
  spinal_cord->co_right_stimulation_walking_phase_5.ConnectTo(this->upper_trunk->ci_right_stimulation_walking_phase_5);
  spinal_cord->co_right_stimulation_lift_leg.ConnectTo(this->upper_trunk->ci_right_stimulation_lift_leg);
  spinal_cord->co_walking_termination.ConnectTo(this->upper_trunk->ci_stimulation_termination);
  spinal_cord->co_termi_last_step.ConnectTo(this->upper_trunk->ci_termi_last_step);
  spinal_cord->co_push_recovery_stand.ConnectTo(this->upper_trunk->ci_push_recovery_stand);
  spinal_cord->co_push_recovery_walking.ConnectTo(this->upper_trunk->ci_push_recovery_walking);
  spinal_cord->co_angle_body_space.ConnectTo(this->upper_trunk->ci_angle_body_space);
  spinal_cord->co_angle_body_space_lateral.ConnectTo(this->upper_trunk->ci_angle_body_space_lateral);
  spinal_cord->co_target_angle_arm_trunk.ConnectTo(this->upper_trunk->ci_target_angle_arm_trunk);
  spinal_cord->co_termination_hold_position.ConnectTo(this->upper_trunk->ci_termination_hold_position);


  spinal_cord->GetControllerOutputs().ConnectByName(this->lower_trunk->GetControllerInputs(), false);
  //spinal_cord->GetControllerOutputs().ConnectByName(this->left_leg->GetControllerInputs(), false, NULL, 4);

  // spinal cord connections to left leg
  spinal_cord->co_com_error_y.ConnectTo(this->left_leg->ci_com_error_y);
  spinal_cord->co_com_error_x_left.ConnectTo(this->left_leg->ci_com_error_x);
  spinal_cord->co_com_ratio.ConnectTo(this->left_leg->ci_com_y_ratio);
  spinal_cord->co_stimulation_stand.ConnectTo(this->left_leg->ci_stimulation_stand);
  spinal_cord->co_stiffness_factor.ConnectTo(this->left_leg->ci_stiffness_factor);
  spinal_cord->co_quiet_stand.ConnectTo(this->left_leg->ci_quiet_stand);
  spinal_cord->co_ground_contact.ConnectTo(this->left_leg->ci_ground_contact);
  spinal_cord->co_stimulation_relax_knee.ConnectTo(this->left_leg->ci_stimulation_relax_knee);
  spinal_cord->co_left_relax_knee_angle_adjustment.ConnectTo(this->left_leg->ci_relax_knee_angle_adjustment);
  spinal_cord->co_stimulation_balance_ankle_pitch.ConnectTo(this->left_leg->ci_stimulation_balance_ankle_pitch);
  spinal_cord->co_left_balance_ankle_pitch_angle_adjustment.ConnectTo(this->left_leg->ci_balance_ankle_pitch_angle_adjustment);
  spinal_cord->co_stimulation_reduce_tension_ankle_y.ConnectTo(this->left_leg->ci_stimulation_reduce_tension_ankle_y);
  spinal_cord->co_left_reduce_tension_ankle_y_angle_adjustment.ConnectTo(this->left_leg->ci_reduce_tension_ankle_y_angle_adjustment);
  spinal_cord->co_left_stimulation_walking_initiation.ConnectTo(this->left_leg->ci_stimulation_walking_initiation);
  spinal_cord->co_left_stimulation_walking_lateral_initiation.ConnectTo(this->left_leg->ci_stimulation_walking_lateral_initiation);
  spinal_cord->co_activity_walking.ConnectTo(this->left_leg->ci_activity_walking);
  spinal_cord->co_walking_scale_factor.ConnectTo(this->left_leg->ci_walking_scale_factor);
  spinal_cord->co_walking_scale_laterally.ConnectTo(this->left_leg->ci_walking_scale_laterally);
  spinal_cord->co_walking_lateral_activity.ConnectTo(this->left_leg->ci_walking_lateral_activity);
  spinal_cord->co_termination_scale_factor.ConnectTo(this->left_leg->ci_termination_scale_factor);
  spinal_cord->co_walking_termination.ConnectTo(this->left_leg->ci_stimulation_termination);
  spinal_cord->co_last_step.ConnectTo(this->left_leg->ci_last_step);
  spinal_cord->co_last_step_state.ConnectTo(this->left_leg->ci_last_step_state);
  spinal_cord->co_push_recovery_stand.ConnectTo(this->left_leg->ci_push_recovery_stand);
  spinal_cord->co_push_recovery_walking.ConnectTo(this->left_leg->ci_push_recovery_walking);
  spinal_cord->co_bent_knee.ConnectTo(this->left_leg->ci_bent_knee);
  spinal_cord->co_push_detected_walking_left.ConnectTo(this->left_leg->ci_push_detected_walking_left);
  spinal_cord->co_push_detected_walking_right.ConnectTo(this->left_leg->ci_push_detected_walking_right);
  spinal_cord->co_left_leg_angle_offset.ConnectTo(this->left_leg->ci_leg_angle_offset);
  spinal_cord->co_left_forward_velocity_correction.ConnectTo(this->left_leg->ci_forward_velocity_correction);
  spinal_cord->co_left_lateral_velocity_correction.ConnectTo(this->left_leg->ci_lateral_velocity_correction);
  spinal_cord->co_left_frontal_velocity_correction.ConnectTo(this->left_leg->ci_frontal_velocity_correction);
  spinal_cord->co_left_stimulation_walking_phase_1.ConnectTo(this->left_leg->ci_stimulation_walking_phase_1);
  spinal_cord->co_left_stimulation_walking_phase_2.ConnectTo(this->left_leg->ci_stimulation_walking_phase_2);
  spinal_cord->co_left_stimulation_walking_phase_3.ConnectTo(this->left_leg->ci_stimulation_walking_phase_3);
  spinal_cord->co_left_stimulation_walking_phase_4.ConnectTo(this->left_leg->ci_stimulation_walking_phase_4);
  spinal_cord->co_left_stimulation_walking_phase_5.ConnectTo(this->left_leg->ci_stimulation_walking_phase_5);
  spinal_cord->co_left_stimulation_walking_lateral_phase_1.ConnectTo(this->left_leg->ci_stimulation_walking_lateral_phase_1);
  spinal_cord->co_left_stimulation_walking_lateral_phase_2.ConnectTo(this->left_leg->ci_stimulation_walking_lateral_phase_2);
  spinal_cord->co_left_stimulation_walking_lateral_phase_3.ConnectTo(this->left_leg->ci_stimulation_walking_lateral_phase_3);
  spinal_cord->co_left_stimulation_walking_lateral_phase_4.ConnectTo(this->left_leg->ci_stimulation_walking_lateral_phase_4);
  spinal_cord->co_turn_left_signal_to_left.ConnectTo(this->left_leg->ci_turn_left_control_signal);
  spinal_cord->co_turn_right_signal_to_left.ConnectTo(this->left_leg->ci_turn_right_control_signal);
  spinal_cord->co_s_leg_left.ConnectTo(this->left_leg->ci_s_leg);
  spinal_cord->co_maintain_stable.ConnectTo(this->left_leg->ci_maintain_stable);
  spinal_cord->co_termination_hold_position.ConnectTo(this->left_leg->ci_termination_hold_position);
  spinal_cord->co_termination_turn_off_upright.ConnectTo(this->left_leg->ci_termination_turn_off_upright);


  spinal_cord->co_disturbance_estimation_activity.ConnectTo(this->left_leg->ci_disturbance_estimation_activity);
  spinal_cord->co_angle_body_foot.ConnectTo(this->left_leg->ci_body_foot_angle);
  spinal_cord->co_estim_foot_space_angle.ConnectTo(this->left_leg->ci_foot_space_angle);
  spinal_cord->co_estim_angle_deviation.ConnectTo(this->left_leg->ci_angle_deviation);
  spinal_cord->co_target_body_space_angle.ConnectTo(this->left_leg->ci_target_body_space_angle);
  spinal_cord->co_actual_pitch.ConnectTo(this->left_leg->ci_actual_pitch);
  spinal_cord->co_actual_roll.ConnectTo(this->left_leg->ci_actual_roll);
  spinal_cord->co_estim_angle_deviation_lateral_left.ConnectTo(this->left_leg->ci_angle_deviation_lateral);
  spinal_cord->co_estim_foot_space_angle_lateral_left.ConnectTo(this->left_leg->ci_foot_space_angle_lateral);
  spinal_cord->co_angle_body_space_lateral.ConnectTo(this->left_leg->ci_angle_body_space_lateral);
  spinal_cord->co_angle_body_foot_left_lateral.ConnectTo(this->left_leg->ci_body_foot_angle_lateral);
  spinal_cord->co_angle_body_space_dist.ConnectTo(this->left_leg->ci_angle_body_space_dist);
  spinal_cord->co_angle_body_space_prop.ConnectTo(this->left_leg->ci_angle_body_space_prop);
  spinal_cord->co_angle_body_space_lateral_dist_left.ConnectTo(this->left_leg->ci_angle_body_space_lateral_dist);
  spinal_cord->co_angle_body_space_lateral_prop_left.ConnectTo(this->left_leg->ci_angle_body_space_lateral_prop);
  spinal_cord->co_angle_trunk_thigh_space_gravity.ConnectTo(this->left_leg->ci_angle_trunk_thigh_space_gravity);
  spinal_cord->co_estim_foot_space_angle_new.ConnectTo(this->left_leg->ci_foot_space_angle_new);
  spinal_cord->co_leg_propel_action1.ConnectTo(this->left_leg->ci_leg_propel_action1);
  spinal_cord->co_leg_propel_action2.ConnectTo(this->left_leg->ci_leg_propel_action2);
  spinal_cord->co_leg_propel_action3.ConnectTo(this->left_leg->ci_leg_propel_action3);
  spinal_cord->co_leg_propel_action4.ConnectTo(this->left_leg->ci_leg_propel_action4);
  spinal_cord->co_loop_times.ConnectTo(this->left_leg->ci_loop_times);
  spinal_cord->co_left_learning_ankle_stiffness.ConnectTo(this->left_leg->ci_learned_stiffness_ankle_push_recovery);
  spinal_cord->co_left_learning_torque_factor.ConnectTo(this->left_leg->ci_leg_learning_torque_factor);

  spinal_cord->co_obstacle_knee_flexion_1.ConnectTo(this->left_leg->ci_obstacle_knee_flexion_1);
  spinal_cord->co_obstacle_knee_flexion_2.ConnectTo(this->left_leg->ci_obstacle_knee_flexion_2);
  spinal_cord->co_obstacle_knee_flexion_2_rear.ConnectTo(this->left_leg->ci_obstacle_knee_flexion_2_rear);
  spinal_cord->co_obstacle_walking_stimulation.ConnectTo(this->left_leg->ci_obstacle_walking_stimulation);
  spinal_cord->co_obstacle_step.ConnectTo(this->left_leg->ci_obstacle_step);


  //PSO SLOW
  //spinal_cord->co_slow_walking_stimulation.ConnectTo(this->ci_slow_walking);
  this->co_slow_walking_stimulation.ConnectTo(spinal_cord->co_slow_walking_stimulation);
  this->co_slow_walking_on.ConnectTo(spinal_cord->co_slow_walking_on);
  this->co_actual_speed.ConnectTo(spinal_cord->co_walking_velocity);

  spinal_cord->co_slow_walking_on.ConnectTo(lower_trunk->ci_slow_walking_stimulation);
  spinal_cord->co_slow_walking_ahs_max_torque.ConnectTo(lower_trunk->ci_slow_walking_max_torque);
  spinal_cord->co_slow_walking_ahs_t1.ConnectTo(lower_trunk->ci_slow_walking_ahs_t1);
  spinal_cord->co_fast_walking_stimulation.ConnectTo(upper_trunk->co_fast_walking);


  spinal_cord->co_slow_walking_lh_par1.ConnectTo(lower_trunk->ci_slow_walking_lh_par1);
  spinal_cord->co_slow_walking_lh_par2.ConnectTo(lower_trunk->ci_slow_walking_lh_par2);
  spinal_cord->co_slow_walking_lh_par1.ConnectTo(lower_trunk->ci_slow_walking_lh_par1);
  spinal_cord->co_slow_walking_lh_par2.ConnectTo(lower_trunk->ci_slow_walking_lh_par2);

  spinal_cord->co_slow_walking_lp_max_torque.ConnectTo(this->left_leg->co_slow_walking_lp_max_torque);
  spinal_cord->co_slow_walking_lp_t1.ConnectTo(this->left_leg->co_slow_walking_lp_t1);
  spinal_cord->co_slow_walking_lp_t2.ConnectTo(this->left_leg->co_slow_walking_lp_t2);
  spinal_cord->co_slow_walking_stimulation.ConnectTo(this->left_leg->co_slow_walking);
  spinal_cord->co_fast_walking_stimulation.ConnectTo(this->left_leg->co_fast_walking);
  spinal_cord->co_slow_walking_knee_angle.ConnectTo(this->left_leg->co_slow_walking_lock_knee);
  spinal_cord->co_slow_walking_knee_angle_low.ConnectTo(this->left_leg->co_slow_walking_lock_knee_low);
  spinal_cord->co_slow_walking_knee_angle_high.ConnectTo(this->left_leg->co_slow_walking_lock_knee_high);
  spinal_cord->co_slow_walking_knee_velocity.ConnectTo(this->left_leg->co_slow_walking_lock_knee_velocity);
  spinal_cord->co_slow_walking_lk_hip_angle_low.ConnectTo(this->left_leg->co_slow_walking_hip_angle_low);
  spinal_cord->co_slow_walking_lk_hip_angle_high.ConnectTo(this->left_leg->co_slow_walking_hip_angle_high);
  spinal_cord->co_slow_walking_lk_increment_par1.ConnectTo(this->left_leg->co_slow_walking_increment_par1);
  spinal_cord->co_slow_walking_lk_increment_par2.ConnectTo(this->left_leg->co_slow_walking_increment_par2);
  spinal_cord->co_slow_walking_max_impulse.ConnectTo(this->left_leg->co_slow_walking_max_impulse);
  spinal_cord->co_slow_walk_stimulation_knee_angle.ConnectTo(this->left_leg->ci_slow_walking_stimulation_knee_angle);
  spinal_cord->co_fast_walk_stimulation_knee_angle.ConnectTo(this->left_leg->ci_fast_walking_stimulation_knee_angle);
  spinal_cord->co_slow_walk_stimulation_knee_angle_activity.ConnectTo(this->left_leg->ci_slow_walking_stimulation_knee_angle_activity);

  spinal_cord->co_slow_walking_lp_max_torque.ConnectTo(this->right_leg->co_slow_walking_lp_max_torque);
  spinal_cord->co_slow_walking_lp_t1.ConnectTo(this->right_leg->co_slow_walking_lp_t1);
  spinal_cord->co_slow_walking_lp_t2.ConnectTo(this->right_leg->co_slow_walking_lp_t2);
  spinal_cord->co_slow_walking_stimulation.ConnectTo(this->right_leg->co_slow_walking);
  spinal_cord->co_fast_walking_stimulation.ConnectTo(this->right_leg->co_fast_walking);
  spinal_cord->co_slow_walking_knee_angle.ConnectTo(this->right_leg->co_slow_walking_lock_knee);
  spinal_cord->co_slow_walking_knee_angle_low.ConnectTo(this->right_leg->co_slow_walking_lock_knee_low);
  spinal_cord->co_slow_walking_knee_angle_high.ConnectTo(this->right_leg->co_slow_walking_lock_knee_high);
  spinal_cord->co_slow_walking_knee_velocity.ConnectTo(this->right_leg->co_slow_walking_lock_knee_velocity);
  spinal_cord->co_slow_walking_lk_hip_angle_low.ConnectTo(this->right_leg->co_slow_walking_hip_angle_low);
  spinal_cord->co_slow_walking_lk_hip_angle_high.ConnectTo(this->right_leg->co_slow_walking_hip_angle_high);
  spinal_cord->co_slow_walking_lk_increment_par1.ConnectTo(this->right_leg->co_slow_walking_increment_par1);
  spinal_cord->co_slow_walking_lk_increment_par2.ConnectTo(this->right_leg->co_slow_walking_increment_par2);
  spinal_cord->co_slow_walking_max_impulse.ConnectTo(this->right_leg->co_slow_walking_max_impulse);
  spinal_cord->co_slow_walk_stimulation_knee_angle.ConnectTo(this->right_leg->ci_slow_walking_stimulation_knee_angle);
  spinal_cord->co_fast_walk_stimulation_knee_angle.ConnectTo(this->right_leg->ci_fast_walking_stimulation_knee_angle);
  spinal_cord->co_slow_walk_stimulation_knee_angle_activity.ConnectTo(this->right_leg->ci_slow_walking_stimulation_knee_angle_activity);

  // spinal cord connections to right leg
  spinal_cord->co_com_error_y.ConnectTo(this->right_leg->ci_com_error_y);
  spinal_cord->co_com_error_x_right.ConnectTo(this->right_leg->ci_com_error_x);
  spinal_cord->co_com_ratio.ConnectTo(this->right_leg->ci_com_y_ratio);
  spinal_cord->co_stimulation_stand.ConnectTo(this->right_leg->ci_stimulation_stand);
  spinal_cord->co_stiffness_factor.ConnectTo(this->right_leg->ci_stiffness_factor);
  spinal_cord->co_quiet_stand.ConnectTo(this->right_leg->ci_quiet_stand);
  spinal_cord->co_ground_contact.ConnectTo(this->right_leg->ci_ground_contact);
  spinal_cord->co_stimulation_relax_knee.ConnectTo(this->right_leg->ci_stimulation_relax_knee);
  spinal_cord->co_right_relax_knee_angle_adjustment.ConnectTo(this->right_leg->ci_relax_knee_angle_adjustment);
  spinal_cord->co_stimulation_balance_ankle_pitch.ConnectTo(this->right_leg->ci_stimulation_balance_ankle_pitch);
  spinal_cord->co_right_balance_ankle_pitch_angle_adjustment.ConnectTo(this->right_leg->ci_balance_ankle_pitch_angle_adjustment);
  spinal_cord->co_stimulation_reduce_tension_ankle_y.ConnectTo(this->right_leg->ci_stimulation_reduce_tension_ankle_y);
  spinal_cord->co_right_reduce_tension_ankle_y_angle_adjustment.ConnectTo(this->right_leg->ci_reduce_tension_ankle_y_angle_adjustment);
  spinal_cord->co_right_stimulation_walking_initiation.ConnectTo(this->right_leg->ci_stimulation_walking_initiation);
  spinal_cord->co_right_stimulation_walking_lateral_initiation.ConnectTo(this->right_leg->ci_stimulation_walking_lateral_initiation);
  spinal_cord->co_activity_walking.ConnectTo(this->right_leg->ci_activity_walking);
  spinal_cord->co_walking_scale_factor.ConnectTo(this->right_leg->ci_walking_scale_factor);
  spinal_cord->co_walking_scale_laterally.ConnectTo(this->right_leg->ci_walking_scale_laterally);
  spinal_cord->co_walking_lateral_activity.ConnectTo(this->right_leg->ci_walking_lateral_activity);
  spinal_cord->co_termination_scale_factor.ConnectTo(this->right_leg->ci_termination_scale_factor);
  spinal_cord->co_walking_termination.ConnectTo(this->right_leg->ci_stimulation_termination);
  spinal_cord->co_last_step.ConnectTo(this->right_leg->ci_last_step);
  spinal_cord->co_last_step_state.ConnectTo(this->right_leg->ci_last_step_state);
  spinal_cord->co_push_recovery_stand.ConnectTo(this->right_leg->ci_push_recovery_stand);
  spinal_cord->co_push_recovery_walking.ConnectTo(this->right_leg->ci_push_recovery_walking);
  spinal_cord->co_bent_knee.ConnectTo(this->right_leg->ci_bent_knee);
  spinal_cord->co_push_detected_walking_left.ConnectTo(this->right_leg->ci_push_detected_walking_left);
  spinal_cord->co_push_detected_walking_right.ConnectTo(this->right_leg->ci_push_detected_walking_right);
  spinal_cord->co_right_leg_angle_offset.ConnectTo(this->right_leg->ci_leg_angle_offset);
  spinal_cord->co_right_forward_velocity_correction.ConnectTo(this->right_leg->ci_forward_velocity_correction);
  spinal_cord->co_right_lateral_velocity_correction.ConnectTo(this->right_leg->ci_lateral_velocity_correction);
  spinal_cord->co_right_frontal_velocity_correction.ConnectTo(this->right_leg->ci_frontal_velocity_correction);
  spinal_cord->co_right_stimulation_walking_phase_1.ConnectTo(this->right_leg->ci_stimulation_walking_phase_1);
  spinal_cord->co_right_stimulation_walking_phase_2.ConnectTo(this->right_leg->ci_stimulation_walking_phase_2);
  spinal_cord->co_right_stimulation_walking_phase_3.ConnectTo(this->right_leg->ci_stimulation_walking_phase_3);
  spinal_cord->co_right_stimulation_walking_phase_4.ConnectTo(this->right_leg->ci_stimulation_walking_phase_4);
  spinal_cord->co_right_stimulation_walking_phase_5.ConnectTo(this->right_leg->ci_stimulation_walking_phase_5);
  spinal_cord->co_right_stimulation_walking_lateral_phase_1.ConnectTo(this->right_leg->ci_stimulation_walking_lateral_phase_1);
  spinal_cord->co_right_stimulation_walking_lateral_phase_2.ConnectTo(this->right_leg->ci_stimulation_walking_lateral_phase_2);
  spinal_cord->co_right_stimulation_walking_lateral_phase_3.ConnectTo(this->right_leg->ci_stimulation_walking_lateral_phase_3);
  spinal_cord->co_right_stimulation_walking_lateral_phase_4.ConnectTo(this->right_leg->ci_stimulation_walking_lateral_phase_4);
  spinal_cord->co_turn_left_signal_to_right.ConnectTo(this->right_leg->ci_turn_left_control_signal);
  spinal_cord->co_turn_right_signal_to_right.ConnectTo(this->right_leg->ci_turn_right_control_signal);
  spinal_cord->co_s_leg_right.ConnectTo(this->right_leg->ci_s_leg);
  spinal_cord->co_maintain_stable.ConnectTo(this->right_leg->ci_maintain_stable);
  spinal_cord->co_termination_hold_position.ConnectTo(this->right_leg->ci_termination_hold_position);
  spinal_cord->co_termination_turn_off_upright.ConnectTo(this->right_leg->ci_termination_turn_off_upright);

  spinal_cord->co_disturbance_estimation_activity.ConnectTo(this->right_leg->ci_disturbance_estimation_activity);
  spinal_cord->co_angle_body_foot.ConnectTo(this->right_leg->ci_body_foot_angle);
  spinal_cord->co_estim_foot_space_angle.ConnectTo(this->right_leg->ci_foot_space_angle);
  spinal_cord->co_estim_angle_deviation.ConnectTo(this->right_leg->ci_angle_deviation);
  spinal_cord->co_target_body_space_angle.ConnectTo(this->right_leg->ci_target_body_space_angle);
  spinal_cord->co_actual_pitch.ConnectTo(this->right_leg->ci_actual_pitch);
  spinal_cord->co_actual_roll.ConnectTo(this->right_leg->ci_actual_roll);
  spinal_cord->co_estim_angle_deviation_lateral_right.ConnectTo(this->right_leg->ci_angle_deviation_lateral);
  spinal_cord->co_estim_foot_space_angle_lateral_right.ConnectTo(this->right_leg->ci_foot_space_angle_lateral);
  spinal_cord->co_angle_body_space_lateral.ConnectTo(this->right_leg->ci_angle_body_space_lateral);
  spinal_cord->co_angle_body_foot_right_lateral.ConnectTo(this->right_leg->ci_body_foot_angle_lateral);
  spinal_cord->co_angle_body_space_dist.ConnectTo(this->right_leg->ci_angle_body_space_dist);
  spinal_cord->co_angle_body_space_prop.ConnectTo(this->right_leg->ci_angle_body_space_prop);
  spinal_cord->co_angle_body_space_lateral_dist_right.ConnectTo(this->right_leg->ci_angle_body_space_lateral_dist);
  spinal_cord->co_angle_body_space_lateral_prop_right.ConnectTo(this->right_leg->ci_angle_body_space_lateral_prop);
  spinal_cord->co_angle_trunk_thigh_space_gravity.ConnectTo(this->right_leg->ci_angle_trunk_thigh_space_gravity);
  spinal_cord->co_estim_foot_space_angle_new.ConnectTo(this->right_leg->ci_foot_space_angle_new);
  spinal_cord->co_leg_propel_action1.ConnectTo(this->right_leg->ci_leg_propel_action1);
  spinal_cord->co_leg_propel_action2.ConnectTo(this->right_leg->ci_leg_propel_action2);
  spinal_cord->co_leg_propel_action3.ConnectTo(this->right_leg->ci_leg_propel_action3);
  spinal_cord->co_leg_propel_action4.ConnectTo(this->right_leg->ci_leg_propel_action4);
  spinal_cord->co_loop_times.ConnectTo(this->right_leg->ci_loop_times);
  spinal_cord->co_right_learning_ankle_stiffness.ConnectTo(this->right_leg->ci_learned_stiffness_ankle_push_recovery);
  spinal_cord->co_right_learning_torque_factor.ConnectTo(this->right_leg->ci_leg_learning_torque_factor);

  spinal_cord->co_obstacle_knee_flexion_1.ConnectTo(this->right_leg->ci_obstacle_knee_flexion_1);
  spinal_cord->co_obstacle_knee_flexion_2.ConnectTo(this->right_leg->ci_obstacle_knee_flexion_2);
  spinal_cord->co_obstacle_knee_flexion_2_rear.ConnectTo(this->right_leg->ci_obstacle_knee_flexion_2_rear);
  spinal_cord->co_obstacle_walking_stimulation.ConnectTo(this->right_leg->ci_obstacle_walking_stimulation);
  spinal_cord->co_obstacle_step.ConnectTo(this->right_leg->ci_obstacle_step);

  // downward activity and target value for left leg to this group
  this->ci_push_y.ConnectTo(this->left_leg->ci_push_detected_walking_lateral);
  this->ci_push_started_x.ConnectTo(this->left_leg->ci_push_started); // knee strategy
//  this->ci_push_detected_x.ConnectTo(this->left_leg->ci_push_started);

  this->left_leg->co_ankle_x_angle.ConnectTo(this->co_left_ankle_x_angle);
  this->left_leg->co_ankle_y_angle.ConnectTo(this->co_left_ankle_y_angle);
  this->left_leg->co_stimulation_ankle_x_angle.ConnectTo(this->co_left_stimulation_ankle_x_angle);
  this->left_leg->co_stimulation_ankle_y_angle.ConnectTo(this->co_left_stimulation_ankle_y_angle);
  this->left_leg->co_ankle_x_torque.ConnectTo(this->co_left_ankle_x_torque);
  this->left_leg->co_ankle_y_torque.ConnectTo(this->co_left_ankle_y_torque);
  this->left_leg->co_stimulation_ankle_x_torque.ConnectTo(this->co_left_stimulation_ankle_x_torque);
  this->left_leg->co_stimulation_ankle_y_torque.ConnectTo(this->co_left_stimulation_ankle_y_torque);
  this->left_leg->co_knee_angle.ConnectTo(this->co_left_knee_angle);
  this->left_leg->co_stimulation_knee_angle.ConnectTo(this->co_left_stimulation_knee_angle);
  this->left_leg->co_knee_torque.ConnectTo(this->co_left_knee_torque);
  this->left_leg->co_stimulation_knee_torque.ConnectTo(this->co_left_stimulation_knee_torque);
  // downward activity and target value for right leg to this group
  this->ci_push_y.ConnectTo(this->right_leg->ci_push_detected_walking_lateral);
  this->ci_push_started_x.ConnectTo(this->right_leg->ci_push_started);// knee strategy
//  this->ci_push_detected_x.ConnectTo(this->right_leg->ci_push_started);

  this->right_leg->co_ankle_x_angle.ConnectTo(this->co_right_ankle_x_angle);
  this->right_leg->co_ankle_y_angle.ConnectTo(this->co_right_ankle_y_angle);
  this->right_leg->co_stimulation_ankle_x_angle.ConnectTo(this->co_right_stimulation_ankle_x_angle);
  this->right_leg->co_stimulation_ankle_y_angle.ConnectTo(this->co_right_stimulation_ankle_y_angle);
  this->right_leg->co_ankle_x_torque.ConnectTo(this->co_right_ankle_x_torque);
  this->right_leg->co_ankle_y_torque.ConnectTo(this->co_right_ankle_y_torque);
  this->right_leg->co_stimulation_ankle_x_torque.ConnectTo(this->co_right_stimulation_ankle_x_torque);
  this->right_leg->co_stimulation_ankle_y_torque.ConnectTo(this->co_right_stimulation_ankle_y_torque);
  this->right_leg->co_knee_angle.ConnectTo(this->co_right_knee_angle);
  this->right_leg->co_stimulation_knee_angle.ConnectTo(this->co_right_stimulation_knee_angle);
  this->right_leg->co_knee_torque.ConnectTo(this->co_right_knee_torque);
  this->right_leg->co_stimulation_knee_torque.ConnectTo(this->co_right_stimulation_knee_torque);
  // downward activity and target value for lower trunk to this group
  this->lower_trunk->co_left_hip_x_angle.ConnectTo(this->co_left_hip_x_angle);
  this->lower_trunk->co_left_hip_y_angle.ConnectTo(this->co_left_hip_y_angle);
  this->lower_trunk->co_left_hip_z_angle.ConnectTo(this->co_left_hip_z_angle);
  this->lower_trunk->co_left_stimulation_hip_x_angle.ConnectTo(this->co_left_stimulation_hip_x_angle);
  this->lower_trunk->co_left_stimulation_hip_y_angle.ConnectTo(this->co_left_stimulation_hip_y_angle);
  this->lower_trunk->co_left_stimulation_hip_z_angle.ConnectTo(this->co_left_stimulation_hip_z_angle);
  this->lower_trunk->co_left_hip_x_torque.ConnectTo(this->co_left_hip_x_torque);
  this->lower_trunk->co_left_hip_y_torque.ConnectTo(this->co_left_hip_y_torque);
  this->lower_trunk->co_left_hip_z_torque.ConnectTo(this->co_left_hip_z_torque);
  this->lower_trunk->co_left_stimulation_hip_x_torque.ConnectTo(this->co_left_stimulation_hip_x_torque);
  this->lower_trunk->co_left_stimulation_hip_y_torque.ConnectTo(this->co_left_stimulation_hip_y_torque);
  this->lower_trunk->co_left_stimulation_hip_z_torque.ConnectTo(this->co_left_stimulation_hip_z_torque);
  this->lower_trunk->co_right_hip_x_angle.ConnectTo(this->co_right_hip_x_angle);
  this->lower_trunk->co_right_hip_y_angle.ConnectTo(this->co_right_hip_y_angle);
  this->lower_trunk->co_right_hip_z_angle.ConnectTo(this->co_right_hip_z_angle);
  this->lower_trunk->co_right_stimulation_hip_x_angle.ConnectTo(this->co_right_stimulation_hip_x_angle);
  this->lower_trunk->co_right_stimulation_hip_y_angle.ConnectTo(this->co_right_stimulation_hip_y_angle);
  this->lower_trunk->co_right_stimulation_hip_z_angle.ConnectTo(this->co_right_stimulation_hip_z_angle);
  this->lower_trunk->co_right_hip_x_torque.ConnectTo(this->co_right_hip_x_torque);
  this->lower_trunk->co_right_hip_y_torque.ConnectTo(this->co_right_hip_y_torque);
  this->lower_trunk->co_right_hip_z_torque.ConnectTo(this->co_right_hip_z_torque);
  this->lower_trunk->co_right_stimulation_hip_x_torque.ConnectTo(this->co_right_stimulation_hip_x_torque);
  this->lower_trunk->co_right_stimulation_hip_y_torque.ConnectTo(this->co_right_stimulation_hip_y_torque);
  this->lower_trunk->co_right_stimulation_hip_z_torque.ConnectTo(this->co_right_stimulation_hip_z_torque);
  this->lower_trunk->co_spine_x_angle.ConnectTo(this->co_spine_x_angle);
  this->lower_trunk->co_spine_y_angle.ConnectTo(this->co_spine_y_angle);
  this->lower_trunk->co_spine_z_angle.ConnectTo(this->co_spine_z_angle);
  this->lower_trunk->co_stimulation_spine_x_angle.ConnectTo(this->co_stimulation_spine_x_angle);
  this->lower_trunk->co_stimulation_spine_y_angle.ConnectTo(this->co_stimulation_spine_y_angle);
  this->lower_trunk->co_stimulation_spine_z_angle.ConnectTo(this->co_stimulation_spine_z_angle);

  this->lower_trunk->co_spine_x_torque.ConnectTo(this->co_spine_x_torque);
  this->lower_trunk->co_spine_y_torque.ConnectTo(this->co_spine_y_torque);
  this->lower_trunk->co_spine_z_torque.ConnectTo(this->co_spine_z_torque);
  this->lower_trunk->co_stimulation_spine_x_torque.ConnectTo(this->co_stimulation_spine_x_torque);
  this->lower_trunk->co_stimulation_spine_y_torque.ConnectTo(this->co_stimulation_spine_y_torque);
  this->lower_trunk->co_stimulation_spine_z_torque.ConnectTo(this->co_stimulation_spine_z_torque);

  this->lower_trunk->co_lock_hip_torque_left.ConnectTo(trans_abs_left_lock_hip_torque_control->in_activity);
  this->lower_trunk->co_lock_hip_torque_right.ConnectTo(trans_abs_right_lock_hip_torque_control->in_activity);

  trans_abs_left_lock_hip_torque_control->out_activity.ConnectTo(spinal_cord->si_lock_hip_left_torque);
  trans_abs_right_lock_hip_torque_control->out_activity.ConnectTo(spinal_cord->si_lock_hip_right_torque);

//  // downward activity and target value for upper trunk to this group
//  this->upper_trunk->co_left_shoulder_x_angle.ConnectTo(this->co_left_shoulder_x_angle); fixme: cannot be connected to the abstration layer!
  this->upper_trunk->co_left_shoulder_y_angle.ConnectTo(this->co_left_shoulder_y_angle);
  this->upper_trunk->co_left_elbow_angle.ConnectTo(this->co_left_elbow_angle);
//
  this->upper_trunk->co_left_shoulder_x_torque.ConnectTo(this->co_left_shoulder_x_torque);
  this->upper_trunk->co_left_shoulder_y_torque.ConnectTo(this->co_left_shoulder_y_torque);
  this->upper_trunk->co_left_elbow_torque.ConnectTo(this->co_left_elbow_torque);
////
//  this->upper_trunk->co_left_stimulation_shoulder_x_angle.ConnectTo(this->co_left_stimulation_shoulder_x_angle); fixme: cannot be connected to the abstration layer!
  this->upper_trunk->co_left_stimulation_shoulder_y_angle.ConnectTo(this->co_left_stimulation_shoulder_y_angle);
  this->upper_trunk->co_left_stimulation_elbow_angle.ConnectTo(this->co_left_stimulation_elbow_angle);
  this->upper_trunk->co_left_stimulation_shoulder_x_torque.ConnectTo(this->co_left_stimulation_shoulder_x_torque);
  this->upper_trunk->co_left_stimulation_shoulder_y_torque.ConnectTo(this->co_left_stimulation_shoulder_y_torque);
  this->upper_trunk->co_left_stimulation_elbow_torque.ConnectTo(this->co_left_stimulation_elbow_torque);
//
  this->upper_trunk->co_right_shoulder_x_angle.ConnectTo(this->co_right_shoulder_x_angle);
  this->upper_trunk->co_right_shoulder_y_angle.ConnectTo(this->co_right_shoulder_y_angle);
  this->upper_trunk->co_right_elbow_angle.ConnectTo(this->co_right_elbow_angle);
////
  this->upper_trunk->co_right_shoulder_x_torque.ConnectTo(this->co_right_shoulder_x_torque);
  this->upper_trunk->co_right_shoulder_y_torque.ConnectTo(this->co_right_shoulder_y_torque);
  this->upper_trunk->co_right_elbow_torque.ConnectTo(this->co_right_elbow_torque);
////
  this->upper_trunk->co_right_stimulation_shoulder_x_angle.ConnectTo(this->co_right_stimulation_shoulder_x_angle);
  this->upper_trunk->co_right_stimulation_shoulder_y_angle.ConnectTo(this->co_right_stimulation_shoulder_y_angle);
  this->upper_trunk->co_right_stimulation_elbow_angle.ConnectTo(this->co_right_stimulation_elbow_angle);
  this->upper_trunk->co_right_stimulation_shoulder_x_torque.ConnectTo(this->co_right_stimulation_shoulder_x_torque);
  this->upper_trunk->co_right_stimulation_shoulder_y_torque.ConnectTo(this->co_right_stimulation_shoulder_y_torque);
  this->upper_trunk->co_right_stimulation_elbow_torque.ConnectTo(this->co_right_stimulation_elbow_torque);
  this->upper_trunk->co_loop_count.ConnectTo(spinal_cord->co_loop_times);


  // upward activity and target rating flow
  this->si_foot_left_pose.ConnectTo(convert_pose_to_value->si_left_foot_pose);
  this->si_foot_right_pose.ConnectTo(convert_pose_to_value->si_right_foot_pose);
  this->si_pelvis_pose.ConnectTo(convert_pose_to_value->si_pelvis_pose);
  convert_pose_to_value->so_pelvis_position_x.ConnectTo(spinal_cord->si_pelvis_position_x);
  convert_pose_to_value->so_pelvis_position_y.ConnectTo(spinal_cord->si_pelvis_position_y);
  convert_pose_to_value->so_left_foot_position_x.ConnectTo(spinal_cord->si_left_foot_position_x);
  convert_pose_to_value->so_left_foot_position_y.ConnectTo(spinal_cord->si_left_foot_position_y);
  convert_pose_to_value->so_left_foot_position_z.ConnectTo(spinal_cord->si_left_foot_position_z);
  convert_pose_to_value->so_right_foot_position_x.ConnectTo(spinal_cord->si_right_foot_position_x);
  convert_pose_to_value->so_right_foot_position_y.ConnectTo(spinal_cord->si_right_foot_position_y);
  convert_pose_to_value->so_right_foot_position_z.ConnectTo(spinal_cord->si_right_foot_position_z);

  convert_pose_to_value->so_left_foot_position_x.ConnectTo(this->lower_trunk->si_left_foot_position_x);
  convert_pose_to_value->so_left_foot_position_y.ConnectTo(this->lower_trunk->si_left_foot_position_y);
  convert_pose_to_value->so_left_foot_position_z.ConnectTo(this->lower_trunk->si_left_foot_position_z);
  convert_pose_to_value->so_right_foot_position_x.ConnectTo(this->lower_trunk->si_right_foot_position_x);
  convert_pose_to_value->so_right_foot_position_y.ConnectTo(this->lower_trunk->si_right_foot_position_y);
  convert_pose_to_value->so_right_foot_position_z.ConnectTo(this->lower_trunk->si_right_foot_position_z);

  this->si_ins_accel[0].ConnectTo(brain->si_ins_accel[0]);
  this->si_ins_accel[1].ConnectTo(brain->si_ins_accel[1]);
  this->si_ins_accel[2].ConnectTo(brain->si_ins_accel[2]);
  this->si_ins_omega[0].ConnectTo(brain->si_ins_omega[0]);
  this->si_ins_omega[1].ConnectTo(brain->si_ins_omega[1]);
  this->si_ins_omega[2].ConnectTo(brain->si_ins_omega[2]);
  this->si_ins_rot[0].ConnectTo(brain->si_ins_rot[0]);
  this->si_ins_rot[1].ConnectTo(brain->si_ins_rot[1]);
  this->si_ins_rot[2].ConnectTo(brain->si_ins_rot[2]);
  this->si_left_ankle_y_angle.ConnectTo(brain->si_left_ankle_y_angle);
  this->si_right_ankle_y_angle.ConnectTo(brain->si_right_ankle_y_angle);

  brain->so_stiffness_factor.ConnectTo(this->so_stiffness_factor);
  brain->so_stand_state.ConnectTo(this->so_stand_state);
  brain->so_ins_roll.ConnectTo(this->so_ins_roll);
  brain->so_ins_pitch.ConnectTo(this->so_ins_pitch);
  brain->so_left_force_deviation.ConnectTo(this->so_left_force_deviation);
  brain->so_right_force_deviation.ConnectTo(this->so_right_force_deviation);
  brain->so_left_foot_load.ConnectTo(this->so_left_foot_load);
  brain->so_right_foot_load.ConnectTo(this->so_right_foot_load);

  convert_pose_to_value->so_left_foot_position_x.ConnectTo(brain->si_foot_left_position_x);
  convert_pose_to_value->so_right_foot_position_x.ConnectTo(brain->si_foot_right_position_x);

  convert_pose_to_value->so_left_foot_position_x.ConnectTo(left_leg->si_foot_left_position_x);
  convert_pose_to_value->so_right_foot_position_x.ConnectTo(left_leg->si_foot_right_position_x);
  convert_pose_to_value->so_left_foot_position_y.ConnectTo(left_leg->si_foot_left_position_y);
  convert_pose_to_value->so_right_foot_position_y.ConnectTo(left_leg->si_foot_right_position_y);
  convert_pose_to_value->so_left_foot_position_z.ConnectTo(left_leg->si_foot_left_position_z);
  convert_pose_to_value->so_right_foot_position_z.ConnectTo(left_leg->si_foot_right_position_z);

  convert_pose_to_value->so_left_foot_position_x.ConnectTo(right_leg->si_foot_left_position_x);
  convert_pose_to_value->so_right_foot_position_x.ConnectTo(right_leg->si_foot_right_position_x);
  convert_pose_to_value->so_left_foot_position_y.ConnectTo(right_leg->si_foot_left_position_y);
  convert_pose_to_value->so_right_foot_position_y.ConnectTo(right_leg->si_foot_right_position_y);
  convert_pose_to_value->so_left_foot_position_z.ConnectTo(right_leg->si_foot_left_position_z);
  convert_pose_to_value->so_right_foot_position_z.ConnectTo(right_leg->si_foot_right_position_z);

  this->si_left_force_z.ConnectTo(this->left_leg->si_force_z);
  this->si_left_torque_x.ConnectTo(this->left_leg->si_torque_x);
  this->si_left_torque_y.ConnectTo(this->left_leg->si_torque_y);
  this->si_left_inner_toe_force.ConnectTo(this->left_leg->si_inner_toe_force);
  this->si_left_outer_toe_force.ConnectTo(this->left_leg->si_outer_toe_force);

  this->si_left_inner_heel_force.ConnectTo(this->left_leg->si_inner_heel_force);
  this->si_left_outer_heel_force.ConnectTo(this->left_leg->si_outer_heel_force);
  this->si_left_ankle_x_torque.ConnectTo(this->left_leg->si_ankle_x_torque);
  this->si_left_ankle_x_angle.ConnectTo(this->left_leg->si_ankle_x_angle);
  this->si_left_ankle_x_torque.ConnectTo(this->left_leg->si_ankle_x_torque);
  this->si_left_ankle_y_angle.ConnectTo(this->left_leg->si_ankle_y_angle);
  this->si_left_ankle_y_torque.ConnectTo(this->left_leg->si_ankle_y_torque);
  this->si_left_knee_angle.ConnectTo(this->left_leg->si_knee_angle);
  this->si_left_knee_torque.ConnectTo(this->left_leg->si_knee_torque);
  this->si_left_hip_y_angle.ConnectTo(this->left_leg->si_swing_leg_hip_y);
  this->si_right_hip_y_angle.ConnectTo(this->left_leg->si_stance_leg_hip_y);
  this->si_left_hip_y_angle.ConnectTo(this->left_leg->si_hip_y_angle);

  this->si_left_hip_x_angle.ConnectTo(this->lower_trunk->si_left_hip_x_angle);
  this->si_left_hip_x_torque.ConnectTo(this->lower_trunk->si_left_hip_x_torque);
  this->si_left_hip_y_angle.ConnectTo(this->lower_trunk->si_left_hip_y_angle);
  this->si_left_hip_y_torque.ConnectTo(this->lower_trunk->si_left_hip_y_torque);
  this->si_left_hip_z_angle.ConnectTo(this->lower_trunk->si_left_hip_z_angle);
  this->si_left_hip_z_torque.ConnectTo(this->lower_trunk->si_left_hip_z_torque);

  this->si_spine_x_angle.ConnectTo(this->lower_trunk->si_spine_x_angle);
  this->si_spine_x_torque.ConnectTo(this->lower_trunk->si_spine_x_torque);
  this->si_spine_y_angle.ConnectTo(this->lower_trunk->si_spine_y_angle);
  this->si_spine_y_torque.ConnectTo(this->lower_trunk->si_spine_y_torque);
  this->si_spine_z_angle.ConnectTo(this->lower_trunk->si_spine_z_angle);
  this->si_spine_z_torque.ConnectTo(this->lower_trunk->si_spine_z_torque);

  this->si_left_shoulder_x_angle.ConnectTo(this->upper_trunk->si_left_shoulder_x_angle);
  this->si_left_shoulder_x_torque.ConnectTo(this->upper_trunk->si_left_shoulder_x_torque);
  this->si_left_shoulder_y_angle.ConnectTo(this->upper_trunk->si_left_shoulder_y_angle);
  this->si_left_shoulder_y_torque.ConnectTo(this->upper_trunk->si_left_shoulder_y_torque);
  this->si_left_elbow_angle.ConnectTo(this->upper_trunk->si_left_elbow_angle);
  this->si_left_elbow_torque.ConnectTo(this->upper_trunk->si_left_elbow_torque);

  this->si_right_force_z.ConnectTo(this->right_leg->si_force_z);
  this->si_right_torque_x.ConnectTo(this->right_leg->si_torque_x);
  this->si_right_torque_y.ConnectTo(this->right_leg->si_torque_y);
  this->si_right_inner_heel_force.ConnectTo(this->right_leg->si_inner_heel_force);
  this->si_right_outer_heel_force.ConnectTo(this->right_leg->si_outer_heel_force);
  this->si_right_inner_toe_force.ConnectTo(this->right_leg->si_inner_toe_force);
  this->si_right_outer_toe_force.ConnectTo(this->right_leg->si_outer_toe_force);

  this->si_right_ankle_x_torque.ConnectTo(this->right_leg->si_ankle_x_torque);
  this->si_right_ankle_x_angle.ConnectTo(this->right_leg->si_ankle_x_angle);
  this->si_right_ankle_x_torque.ConnectTo(this->right_leg->si_ankle_x_torque);
  this->si_right_ankle_y_angle.ConnectTo(this->right_leg->si_ankle_y_angle);
  this->si_right_ankle_y_torque.ConnectTo(this->right_leg->si_ankle_y_torque);
  this->si_right_knee_angle.ConnectTo(this->right_leg->si_knee_angle);
  this->si_right_knee_torque.ConnectTo(this->right_leg->si_knee_torque);
  this->si_right_hip_y_angle.ConnectTo(this->right_leg->si_swing_leg_hip_y);
  this->si_right_hip_y_angle.ConnectTo(this->right_leg->si_stance_leg_hip_y);// changed from left to right_hip_y_angle
  this->si_right_hip_y_angle.ConnectTo(this->right_leg->si_hip_y_angle);

  this->si_right_hip_x_angle.ConnectTo(this->lower_trunk->si_right_hip_x_angle);
  this->si_right_hip_x_torque.ConnectTo(this->lower_trunk->si_right_hip_x_torque);
  this->si_right_hip_y_angle.ConnectTo(this->lower_trunk->si_right_hip_y_angle);
  this->si_right_hip_y_torque.ConnectTo(this->lower_trunk->si_right_hip_y_torque);
  this->si_right_hip_z_angle.ConnectTo(this->lower_trunk->si_right_hip_z_angle);
  this->si_right_hip_z_torque.ConnectTo(this->lower_trunk->si_right_hip_z_torque);

  this->si_right_shoulder_x_angle.ConnectTo(this->upper_trunk->si_right_shoulder_x_angle);
  this->si_right_shoulder_x_torque.ConnectTo(this->upper_trunk->si_right_shoulder_x_torque);
  this->si_right_shoulder_y_angle.ConnectTo(this->upper_trunk->si_right_shoulder_y_angle);
  this->si_right_shoulder_y_torque.ConnectTo(this->upper_trunk->si_right_shoulder_y_torque);
  this->si_right_elbow_angle.ConnectTo(this->upper_trunk->si_right_elbow_angle);
  this->si_right_elbow_torque.ConnectTo(this->upper_trunk->si_right_elbow_torque);

  // spinal cord -> brain,
  spinal_cord->so_activity_stand.ConnectTo(brain->si_spinal_cord_activity_stand);
  spinal_cord->so_target_rating_fusion_adjust_angle.ConnectTo(brain->si_spinal_cord_target_rating_fusion_adjust_angle);
  spinal_cord->so_activity_walking.ConnectTo(brain->si_spinal_cord_activity_walking);
  spinal_cord->so_target_rating_walking.ConnectTo(brain->si_spinal_cord_target_rating_walking);
  spinal_cord->so_target_rating_walking_termination.ConnectTo(brain->si_spinal_cord_target_rating_walking_termination);
  spinal_cord->so_termination_finished.ConnectTo(brain->si_spinal_cord_termination_finished);
  spinal_cord->so_left_activity_walking_initiation.ConnectTo(brain->si_spinal_cord_left_activity_walking_initiation);
  spinal_cord->so_left_target_rating_walking_initiation.ConnectTo(brain->si_spinal_cord_left_target_rating_walking_initiation);
  spinal_cord->so_right_activity_walking_initiation.ConnectTo(brain->si_spinal_cord_right_activity_walking_initiation);
  spinal_cord->so_right_target_rating_walking_initiation.ConnectTo(brain->si_spinal_cord_right_target_rating_walking_initiation);
  spinal_cord->so_left_force_deviation.ConnectTo(brain->si_left_force_deviation);
  spinal_cord->so_right_force_deviation.ConnectTo(brain->si_right_force_deviation);
  spinal_cord->so_left_com_x.ConnectTo(brain->si_com_x_left);
  spinal_cord->so_left_xcom_x.ConnectTo(brain->si_xcom_x_left);
  spinal_cord->so_right_com_x.ConnectTo(brain->si_com_x_right);
  spinal_cord->so_right_xcom_x.ConnectTo(brain->si_xcom_x_right);
  spinal_cord->so_xcom_x_left_correction.ConnectTo(brain->si_xcom_x_left_correction);
  spinal_cord->so_xcom_x_right_correction.ConnectTo(brain->si_xcom_x_right_correction);
  spinal_cord->so_xcom_y_left_correction.ConnectTo(brain->si_xcom_y_left_correction);
  spinal_cord->so_xcom_y_right_correction.ConnectTo(brain->si_xcom_y_right_correction);
  spinal_cord->so_falling_down.ConnectTo(brain->si_falling_down);
  spinal_cord->so_ground_contact.ConnectTo(brain->si_ground_contact);
  spinal_cord->so_left_foot_load.ConnectTo(brain->si_left_foot_load);
  spinal_cord->so_right_foot_load.ConnectTo(brain->si_right_foot_load);
  spinal_cord->so_left_stability_x.ConnectTo(brain->si_left_stability_x);
  spinal_cord->so_right_stability_x.ConnectTo(brain->si_right_stability_x);
  spinal_cord->so_left_stability_y.ConnectTo(brain->si_left_stability_y);
  spinal_cord->so_right_stability_y.ConnectTo(brain->si_right_stability_y);
  spinal_cord->so_new_running.ConnectTo(this->so_new_running);
  spinal_cord->so_loop_times.ConnectTo(this->so_loop_times);


  // upper trunk -> spinal cord
  this->upper_trunk->GetSensorOutputs().ConnectByName(spinal_cord->GetSensorInputs(), false);
  // lower trunk -> spinal cord
  this->lower_trunk->GetSensorOutputs().ConnectByName(spinal_cord->GetSensorInputs(), false);

  // left leg -> spinal cord
  this->left_leg->so_foot_load.ConnectTo(spinal_cord->si_left_leg_foot_load);
  this->left_leg->so_activity_walking_phase4.ConnectTo(spinal_cord->si_left_leg_activity_walking_phase_4);
  this->left_leg->so_target_rating_walking_phase4.ConnectTo(spinal_cord->si_left_leg_target_rating_walking_phase_4);
  this->left_leg->so_activity_walking_phase5.ConnectTo(spinal_cord->si_left_leg_activity_walking_phase_5);
  this->left_leg->so_target_rating_walking_phase5.ConnectTo(spinal_cord->si_left_leg_target_rating_walking_phase_5);
  this->left_leg->so_knee_torque.ConnectTo(spinal_cord->si_left_knee_torque);
  this->left_leg->so_knee_angle.ConnectTo(spinal_cord->si_left_knee_angle);
  this->left_leg->so_ankle_x_torque.ConnectTo(spinal_cord->si_left_ankle_x_torque);
  this->left_leg->so_ankle_x_angle.ConnectTo(spinal_cord->si_left_ankle_x_angle);
  this->left_leg->so_ankle_y_torque.ConnectTo(spinal_cord->si_left_ankle_y_torque);
  this->left_leg->so_ankle_y_angle.ConnectTo(spinal_cord->si_left_ankle_y_angle);
  this->left_leg->so_force_z.ConnectTo(spinal_cord->si_left_force_z);
  this->left_leg->so_torque_x.ConnectTo(spinal_cord->si_left_torque_x);
  this->left_leg->so_torque_y.ConnectTo(spinal_cord->si_left_torque_y);
  this->left_leg->so_inner_toe_force.ConnectTo(spinal_cord->si_left_inner_toe_force);
  this->left_leg->so_inner_heel_force.ConnectTo(spinal_cord->si_left_inner_heel_force);
  this->left_leg->so_outer_toe_force.ConnectTo(spinal_cord->si_left_outer_toe_force);
  this->left_leg->so_outer_heel_force.ConnectTo(spinal_cord->si_left_outer_heel_force);
  this->left_leg->so_force_deviation.ConnectTo(spinal_cord->si_left_force_deviation);
  this->left_leg->so_ground_contact.ConnectTo(spinal_cord->si_left_ground_contact);
  this->left_leg->so_cop_x.ConnectTo(spinal_cord->si_left_cop_x);
  this->left_leg->so_cop_y.ConnectTo(spinal_cord->si_left_cop_y);
  // right leg -> spinal cord
  this->right_leg->so_foot_load.ConnectTo(spinal_cord->si_right_leg_foot_load);
  this->right_leg->so_activity_walking_phase4.ConnectTo(spinal_cord->si_right_leg_activity_walking_phase_4);
  this->right_leg->so_target_rating_walking_phase4.ConnectTo(spinal_cord->si_right_leg_target_rating_walking_phase_4);
  this->right_leg->so_activity_walking_phase5.ConnectTo(spinal_cord->si_right_leg_activity_walking_phase_5);
  this->right_leg->so_target_rating_walking_phase5.ConnectTo(spinal_cord->si_right_leg_target_rating_walking_phase_5);
  this->right_leg->so_knee_torque.ConnectTo(spinal_cord->si_right_knee_torque);
  this->right_leg->so_knee_angle.ConnectTo(spinal_cord->si_right_knee_angle);
  this->right_leg->so_ankle_x_torque.ConnectTo(spinal_cord->si_right_ankle_x_torque);
  this->right_leg->so_ankle_x_angle.ConnectTo(spinal_cord->si_right_ankle_x_angle);
  this->right_leg->so_ankle_y_torque.ConnectTo(spinal_cord->si_right_ankle_y_torque);
  this->right_leg->so_ankle_y_angle.ConnectTo(spinal_cord->si_right_ankle_y_angle);
  this->right_leg->so_force_z.ConnectTo(spinal_cord->si_right_force_z);
  this->right_leg->so_torque_x.ConnectTo(spinal_cord->si_right_torque_x);
  this->right_leg->so_torque_y.ConnectTo(spinal_cord->si_right_torque_y);
  this->right_leg->so_inner_toe_force.ConnectTo(spinal_cord->si_right_inner_toe_force);
  this->right_leg->so_inner_heel_force.ConnectTo(spinal_cord->si_right_inner_heel_force);
  this->right_leg->so_outer_toe_force.ConnectTo(spinal_cord->si_right_outer_toe_force);
  this->right_leg->so_outer_heel_force.ConnectTo(spinal_cord->si_right_outer_heel_force);
  this->right_leg->so_force_deviation.ConnectTo(spinal_cord->si_right_force_deviation);
  this->right_leg->so_ground_contact.ConnectTo(spinal_cord->si_right_ground_contact);
  this->right_leg->so_cop_x.ConnectTo(spinal_cord->si_right_cop_x);
  this->right_leg->so_cop_y.ConnectTo(spinal_cord->si_right_cop_y);
  // diagnostic outputs
  this->left_leg->so_ground_contact.ConnectTo(this->so_left_ground_contact);
  this->right_leg->so_ground_contact.ConnectTo(this->so_right_ground_contact);
  this->si_left_force_z.ConnectTo(this->so_left_force_z);
  this->si_right_force_z.ConnectTo(this->so_right_force_z);

}

//----------------------------------------------------------------------
// gControl destructor
//----------------------------------------------------------------------
gControl::~gControl()
{}

//----------------------------------------------------------------------
// End of namespace declaration
//----------------------------------------------------------------------
}
}
}
