//
// You received this file as part of Finroc
// A framework for intelligent robot control
//
// Copyright (C) AG Robotersysteme TU Kaiserslautern
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//
//----------------------------------------------------------------------
/*!\file    projects/biped/skills/mWalkingVelocity.cpp
 *
 * \author  Kartikeya Karnatak
 *
 * \date    2016-05-12
 *
 */
//----------------------------------------------------------------------
#include "projects/biped/skills/mWalkingVelocity.h"
#include "rrlib/time/time.h"
//----------------------------------------------------------------------
// External includes (system with <>, local with "")
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Internal includes with ""
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Debugging
//----------------------------------------------------------------------
#include <cassert>

//----------------------------------------------------------------------
// Namespace usage
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Namespace declaration
//----------------------------------------------------------------------
namespace finroc
{
namespace biped
{
namespace skills
{

//----------------------------------------------------------------------
// Forward declarations / typedefs / enums
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Const values
//----------------------------------------------------------------------
runtime_construction::tStandardCreateModuleAction<mWalkingVelocity> cCREATE_ACTION_FOR_M_WALKINGVELOCITY("WalkingVelocity");

//----------------------------------------------------------------------
// Implementation
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// mWalkingVelocity constructor
//----------------------------------------------------------------------
mWalkingVelocity::mWalkingVelocity(core::tFrameworkElement *parent, const std::string &name) :
		  tModule(parent, name, false), // change to 'true' to make module's ports shared (so that ports in other processes can connect to its output and/or input ports)
		  time_start(rrlib::time::cNO_TIME),
		  time_current(rrlib::time::cNO_TIME),
		  time_span(0.0),
		  step_length(0.0),
		  walking_velocity(0.0),
		  walking_velocity_x(0.0),
		  walking_velocity_y(0.0),
		  walking_distance(0.0),
		  loop_times(0.0),
		  time_enter(0),
		  filter_pos(0),
		  pelvis_position_x(0.0),
		  pelvis_position_y(0.0),
		  last_pelvis_position_x(0.0),
		  last_pelvis_position_y(0.0),
		  filter_length(0),
		  last_looptime(rrlib::time::cNO_TIME),
		  looptime(rrlib::time::cNO_TIME)
{
	this->par_filter_length.Set(60);

	for (int iter = 0; iter < MAX_FILTER_LENGTH; iter++)
	{
		this->last_walking_position_x[iter] = 0.0;
		this->last_walking_position_y[iter] = 0.0;
	}
	this->ci_reset_simulation.AddPortListenerSimple(*this);
}
//----------------------------------------------------------------------
// mWalkingVelocity destructor
//----------------------------------------------------------------------
mWalkingVelocity::~mWalkingVelocity()
{}

//----------------------------------------------------------------------
// mWalkingVelocity Update
//----------------------------------------------------------------------
void mWalkingVelocity::Update()
{
	//	if (this->ci_loop_times.HasChanged())
	//	{
	//
	////    At least one of your input ports has changed. Do something useful with its data.
	////    However, using the .HasChanged() method on each port you can check in more detail.
	//	}

	//	if( this->ci_simulation_reset.HasChanged() )
	//	{
	//		this->loop_times = 0;
	//	}

	this->looptime = rrlib::time::Now(true);
	this->filter_length = this->par_filter_length.Get();
	this->loop_times = this->ci_loop_times.Get();
	this->pelvis_position_x = this->si_pelvis_position_x.Get();
	this->pelvis_position_y = this->si_pelvis_position_y.Get();

	// calculating the walking speed and walking distance
	/*if (this->loop_times < 5)
	{
		this->step_length = 0;
		this->time_enter = 0;
	}*/

	//	if (this->loop_times == 2 && this->time_enter == 0)
	//	{
	//		this->time_start = rrlib::time::Now(true);
	//		this->last_walking_position_x[this->filter_pos] = this->pelvis_position_x;
	//		this->last_walking_position_y[this->filter_pos] = this->pelvis_position_y;
	//		this->time_enter = 1;
	//	}

	//if (this->loop_times >= 2)
	{
		this->last_walking_position_x[this->filter_pos] = this->pelvis_position_x - this->last_pelvis_position_x;
		this->last_walking_position_y[this->filter_pos] = this->pelvis_position_y - this->last_pelvis_position_y;

		this->filter_pos++;
		if (this->filter_pos >= this->filter_length)
			this->filter_pos = 0;

		/*	this->time_current = rrlib::time::Now(true);
		rrlib::time::tDuration time_millisecs = this->time_current - this->time_start;
		this->time_span = std::chrono::duration_cast<std::chrono::milliseconds>(time_millisecs).count();
		this->time_span = this->time_span / 1000;*/


		this->adjusted_position[0] = FilterValue(this->last_walking_position_x);
		this->adjusted_position[1] = FilterValue(this->last_walking_position_y);

		//this->step_length = sqrt(pow((this->last_walking_position_x[this->filter_pos] - this->last_walking_position_x[this->filter_pos - 1]), 2) +
		//		pow((this->last_walking_position_y[this->filter_pos] - this->last_walking_position_y[this->filter_pos -1]), 2));
		rrlib::time::tDuration time_diff = this->looptime - this->last_looptime;
		double time_diff_milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(time_diff).count();
		double  time_diff_seconds = time_diff_milliseconds / 1000;

		//      this->walking_velocity_x += this->adjusted_position[0] / time_diff_seconds;
		//      this->walking_velocity_y += this->adjusted_position[1] / time_diff_seconds;

		//	if (this->loop_times > 2)
		//	{
		//		this->walking_velocity = this->step_length / this->time_span;
		//	}


		if (time_diff_seconds > 0.0)
		{
			//      rrlib::time::tDuration time_diff = this->looptime - this->last_looptime;
			//      double time_diff_seconds = std::chrono::duration_cast<std::chrono::seconds>(time_diff).count();

			this->walking_velocity_x = this->adjusted_position[0] / time_diff_seconds;
			this->walking_velocity_y = this->adjusted_position[1] / time_diff_seconds;
		}
		else
		{
			this->walking_velocity_x = 0.0;
			this->walking_velocity_y = 0.0;
		}

		//this->step_length = this->adjusted_position[0] / this->loop_times;

		if ((this->reset_simulation.exchange(false)) || (this->ci_reset_simulation_experiments.HasChanged()))
		{

			for (int iter = 0; iter < MAX_FILTER_LENGTH; iter++)
			{
				this->last_walking_position_x[iter] = 0.0;
				this->last_walking_position_y[iter] = 0.0;
			}
			this->walking_velocity_x = 0.0;
			this->walking_velocity_y = 0.0;
			this->walking_distance   = 0.0;
			this->step_length        = 0.0;
			//RRLIB_LOG_PRINT(DEBUG,"WALKING VELOCITY X IS ", this->walking_velocity_x, " WALKING VELOCITY Y IS ", this->walking_velocity_y);
		}
		//	if (this->loop_times == 2 && this->time_enter == 0)
		//	{
		//		this->time_start = rrlib::time::Now(true);
		//		this->walking_initial_position_x = this->pelvis_position_x;
		//		this->walking_initial_position_y = this->pelvis_position_y;
		//		this->time_enter = 1;
		//	}
		//
		//	if (this->loop_times >= 2)
		//	{
		//		this->time_current = rrlib::time::Now(true);
		//		rrlib::time::tDuration time_millisecs = this->time_current - this->time_start;
		//		this->time_span = std::chrono::duration_cast<std::chrono::milliseconds>(time_millisecs).count();
		//		this->time_span = this->time_span / 1000;
		//		this->step_length = sqrt(pow((this->pelvis_position_x - this->walking_initial_position_x), 2) + pow((this->pelvis_position_y - this->walking_initial_position_y), 2));
		//	}
		//if (this->loop_times > 2)
		{
			//this->walking_velocity = this->step_length / time_diff_seconds;
		}
		//
		//	this->walking_distance = this->step_length * loop_times;
		this->walking_velocity = sqrt( pow( (this->walking_velocity_x), 2 ) + pow( (this->walking_velocity_y),2 ) );
	}
	this->walking_distance = this->walking_distance + ( this->pelvis_position_x - this->last_pelvis_position_x );

	double firstStepPos  = 0.0;
	int    stepLoopCount = 0;
	int	   stepCounter   = 0;

	if( stepCounter == 0 )
	{
		firstStepPos  = this->last_pelvis_position_x;
		stepCounter++;
	}

	if( this->loop_times > stepLoopCount )
	{
		this->step_length = this->pelvis_position_x - firstStepPos;
		stepLoopCount = this->loop_times;
		stepCounter   = 0;
	}
	//this->step_length = sqrt( pow( (this->pelvis_position_x - this->last_pelvis_position_x), 2) + pow( (this->pelvis_position_y -  this->last_pelvis_position_y), 2) );
	//this->step_length = this->pelvis_position_x - this->last_pelvis_position_x;

	this->co_time_span.Publish(this->time_span);
	this->co_step_length.Publish(this->step_length);
	this->co_walking_velocity.Publish(this->walking_velocity);
	this->co_walking_distance.Publish(this->walking_distance);

	this->last_looptime = this->looptime;
	this->last_pelvis_position_x = this->pelvis_position_x;
	this->last_pelvis_position_y = this->pelvis_position_y;
	//  Do something each cycle independent from changing ports.
	//
	//  this->out_signal_1.Publish(some meaningful value); can be used to publish data via your output ports.
}

double mWalkingVelocity::FilterValue(double* filter_history)
{
	double sum = 0.0;
	for (int iter = 0; iter < this->filter_length; iter++)
		sum += filter_history[ iter ];
	return sum / this->filter_length;
}


void mWalkingVelocity::OnPortChange(data_ports::tChangeContext& change_context)
{
	if ((this->ci_reset_simulation == change_context.Origin()))
	{
		this->reset_simulation = true;
	}
}
//----------------------------------------------------------------------
// End of namespace declaration
//----------------------------------------------------------------------
}
}
}
