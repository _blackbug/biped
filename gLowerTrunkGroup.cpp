//
// You received this file as part of Finroc
// A Framework for intelligent robot control
//
// Copyright (C) Finroc GbR (finroc.org)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//----------------------------------------------------------------------
/*!\file    projects/biped/control/gLowerTrunkGroup.cpp
 *
 * \author  Jie Zhao
 *
 * \date    2013-12-16
 *
 */
//----------------------------------------------------------------------
#include "projects/biped/control/gLowerTrunkGroup.h"
#include "plugins/ib2c/mbbFusion.h"

#include "projects/biped/control/gbbLowerTrunkHoldPosition.h"
#include "projects/biped/control/gLowerTrunkLateralWalking.h"
#include "projects/biped/reflexes/mbbHoldJointPosition.h"
#include "projects/biped/control/gLowerTrunkLastStep.h"

#include "projects/biped/skills/mbbTerminationFusion.h"
#include "projects/biped/skills/mbbLowerTrunkStand.h"
#include "projects/biped/skills/mbbUberSkill.h"
#include "projects/biped/skills/mbbStimulationSkill.h"
#include "projects/biped/motor_patterns/mbbActiveHipSwing.h"
#include "projects/biped/motor_patterns/mbbActiveHipSwingSin.h"
#include "projects/biped/motor_patterns/mbbHipSwingCurveWalking.h"
#include "projects/biped/motor_patterns/mbbHipSwingBentKnee.h"
#include "projects/biped/motor_patterns/mbbLateralHipShift.h"
#include "projects/biped/motor_patterns/mbbLateralHipShiftWalkingInit.h"
#include "projects/biped/motor_patterns/mbbLateralHipShiftPushRecovery.h"
#include "projects/biped/motor_patterns/mbbLowerTrunkLiftLeg.h"
#include "projects/biped/motor_patterns/mbbSingleTorquePattern.h"
#include "projects/biped/motor_patterns/mbbDoubleTorquePattern.h"
#include "projects/biped/motor_patterns/mbbDoubleLateralTorquePattern.h"
#include "projects/biped/motor_patterns/mbbTurnUpperTrunk.h"
#include "projects/biped/motor_patterns/mMotorPattern.h"
#include "projects/biped/reflexes/mbbBalanceBodyPitch.h"
#include "projects/biped/reflexes/mbbAdjustBodyPitch.h"
#include "projects/biped/reflexes/mbbBalanceJoint.h"
#include "projects/biped/reflexes/mbbLockHip.h"
#include "projects/biped/reflexes/mbbLockHipRoll.h"
#include "projects/biped/reflexes/mbbAdjustCorrespondingJointAngles.h"
#include "projects/biped/reflexes/mbbAdjustJointAngle.h"
#include "projects/biped/reflexes/mbbLateralFootPlacement.h"
#include "projects/biped/reflexes/mbbControlPelvisRoll.h"
#include "projects/biped/reflexes/mbbControlPelvisYaw.h"
#include "projects/biped/utils/mMemoryUnit.h"
//#include "projects/biped/utils/mTransmissionUnit.h"

#include "projects/biped/dec/mbbCompensationHipPitch.h"
#include "projects/biped/dec/mbbCompensationSpinePitch.h"

#include "projects/biped/dec/mbbCompensationHipRoll.h"
//#include "projects/biped/dec/mbbCompensationSpineRoll.h"
//----------------------------------------------------------------------
// External includes (system with <>, local with "")
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Internal includes with ""
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Debugging
//----------------------------------------------------------------------
#include <cassert>

//----------------------------------------------------------------------
// Namespace usage
//----------------------------------------------------------------------
using namespace finroc::ib2c;
using namespace finroc::biped::skills;
using namespace finroc::biped::reflexes;
using namespace finroc::biped::utils;
using namespace finroc::biped::motor_patterns;
using namespace finroc::biped::dec;

//----------------------------------------------------------------------
// Namespace declaration
//----------------------------------------------------------------------
namespace finroc
{
namespace biped
{
namespace control
{

//----------------------------------------------------------------------
// Forward declarations / typedefs / enums
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Const values
//----------------------------------------------------------------------
static runtime_construction::tStandardCreateModuleAction<gLowerTrunkGroup> cCREATE_ACTION_FOR_G_LOWERTRUNKGROUP("LowerTrunkGroup");

//----------------------------------------------------------------------
// Implementation
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// gLowerTrunkGroup constructor
//----------------------------------------------------------------------
gLowerTrunkGroup::gLowerTrunkGroup(core::tFrameworkElement *parent, const std::string &name,
                                   const std::string &structure_config_file) :
  tSenseControlGroup(parent, name, structure_config_file, false) // change to 'true' to make group's ports shared (so that ports in other processes can connect to its sensor outputs and controller inputs)
{
  //BEGIN  initiating fusion behaviours
  mbbTerminationFusion* fusion_walking_termi_left = new mbbTerminationFusion(this, " Last Step & Walking Left ");

  mbbTerminationFusion* fusion_walking_termi_right = new mbbTerminationFusion(this, " Last Step & Walking Right ");

  // spine
  mbbFusion<double> * fusion_abs_angle_spine_x = new mbbFusion<double>(this,  "Abs. Angle Spine X", 2, tStimulationMode::ENABLED, 1);
  fusion_abs_angle_spine_x->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
  fusion_abs_angle_spine_x->number_of_inhibition_ports.Set(1);
  fusion_abs_angle_spine_x->CheckStaticParameters();
  mbbFusion<double> * fusion_abs_angle_spine_y = new mbbFusion<double>(this,  "Abs. Angle Spine Y", 2, tStimulationMode::ENABLED, 1);
  fusion_abs_angle_spine_y->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
  fusion_abs_angle_spine_y->number_of_inhibition_ports.Set(1);
  fusion_abs_angle_spine_y->CheckStaticParameters();
  mbbFusion<double> * fusion_abs_angle_spine_z = new mbbFusion<double>(this, "Abs. Angle Spine Z", 2, tStimulationMode::ENABLED, 1);
  fusion_abs_angle_spine_z->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
  fusion_abs_angle_spine_z->number_of_inhibition_ports.Set(1);
  fusion_abs_angle_spine_z->CheckStaticParameters();

  mbbFusion<double> * fusion_rel_angle_spine_x = new mbbFusion<double>(this,  "Rel. Angle Spine X", 1, tStimulationMode::ENABLED, 1);
  fusion_rel_angle_spine_x->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
  fusion_rel_angle_spine_x->number_of_inhibition_ports.Set(1);
  fusion_rel_angle_spine_x->CheckStaticParameters();
  mbbFusion<double> * fusion_rel_angle_spine_y = new mbbFusion<double>(this,  "Rel. Angle Spine Y", 1, tStimulationMode::ENABLED, 1);
  fusion_rel_angle_spine_y->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
  fusion_rel_angle_spine_y->number_of_inhibition_ports.Set(1);
  fusion_rel_angle_spine_y->CheckStaticParameters();
//  mbbFusion<double> * fusion_rel_angle_spine_z = new mbbFusion<double>(this, "Rel. Angle Spine Z", 1, tStimulationMode::ENABLED, 1);
//  fusion_rel_angle_spine_z->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
//  fusion_rel_angle_spine_z->number_of_inhibition_ports.Set(1);
//  fusion_rel_angle_spine_z->CheckStaticParameters();
//  this->ci_for_non_used_fusion_modules.ConnectTo(fusion_rel_angle_spine_z->InputActivity(0));
//  this->ci_for_non_used_fusion_modules.ConnectTo(fusion_rel_angle_spine_z->InputTargetRating(0));
//  this->ci_for_non_used_fusion_modules.ConnectTo(fusion_rel_angle_spine_z->InputPort(0, 0));


  mbbFusion<double> * fusion_abs_torque_spine_x = new mbbFusion<double>(this, "Abs. Torque Spine X", 7, tStimulationMode::ENABLED);
  fusion_abs_torque_spine_x->fusion_method.Set(tFusionMethod::WEIGHTED_SUM);
  mbbFusion<double> * fusion_abs_torque_spine_y = new mbbFusion<double>(this, "Abs. Torque Spine Y", 2, tStimulationMode::ENABLED);
  fusion_abs_torque_spine_y->fusion_method.Set(tFusionMethod::WEIGHTED_SUM);
//  this->ci_for_non_used_fusion_modules.ConnectTo(fusion_abs_torque_spine_y->InputActivity(0));
//  this->ci_for_non_used_fusion_modules.ConnectTo(fusion_abs_torque_spine_y->InputTargetRating(0));
//  this->ci_for_non_used_fusion_modules.ConnectTo(fusion_abs_torque_spine_y->InputPort(0, 0));
  mbbFusion<double> * fusion_abs_torque_spine_z = new mbbFusion<double>(this, "Abs. Torque Spine Z", 1, tStimulationMode::ENABLED);
  fusion_abs_torque_spine_z->fusion_method.Set(tFusionMethod::WEIGHTED_SUM);

  //memory unit for activity of sensory output of torque control
  mMemoryUnit * memory_abs_torque_spine_x = new mMemoryUnit(this, "Memory Torque Spine X");

  mMemoryUnit * memory_abs_torque_spine_y = new mMemoryUnit(this, "Memory Torque Spine Y");

  mMemoryUnit * memory_abs_torque_spine_z = new mMemoryUnit(this, "Memory Torque Spine Z");

  // transmission units for lock hip
//
//  mTransmissionUnit * trans_abs_left_lock_hip_torque_sense = new mTransmissionUnit(this, "Transmission Torque Lock Hip Left Sense");
//
//  mTransmissionUnit * trans_abs_right_lock_hip_torque_sense = new mTransmissionUnit(this, "Transmission Torque Lock Hip Right Sense");

  // left hip
  mbbFusion<double> * fusion_abs_angle_left_hip_x = new mbbFusion<double>(this,  "Abs. Angle Left Hip X", 5, tStimulationMode::ENABLED, 1);
  fusion_abs_angle_left_hip_x->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
  fusion_abs_angle_left_hip_x->number_of_inhibition_ports.Set(1);
  fusion_abs_angle_left_hip_x->CheckStaticParameters();
  mbbFusion<double> * fusion_abs_angle_left_hip_y = new mbbFusion<double>(this,  "Abs. Angle Left Hip Y", 3, tStimulationMode::ENABLED, 1);
  fusion_abs_angle_left_hip_y->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
  fusion_abs_angle_left_hip_y->number_of_inhibition_ports.Set(1);
  fusion_abs_angle_left_hip_y->CheckStaticParameters();
  mbbFusion<double> * fusion_abs_angle_left_hip_z = new mbbFusion<double>(this, "Abs. Angle Left Hip Z", 5, tStimulationMode::ENABLED, 1);
  fusion_abs_angle_left_hip_z->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
  fusion_abs_angle_left_hip_z->number_of_inhibition_ports.Set(1);
  fusion_abs_angle_left_hip_z->CheckStaticParameters();

  mbbFusion<double> * fusion_rel_angle_left_hip_x = new mbbFusion<double>(this,  "Rel. Angle Left Hip X", 1, tStimulationMode::ENABLED, 1);
  fusion_rel_angle_left_hip_x->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
  this->ci_for_non_used_fusion_modules.ConnectTo(fusion_rel_angle_left_hip_x->InputActivity(0));
  this->ci_for_non_used_fusion_modules.ConnectTo(fusion_rel_angle_left_hip_x->InputTargetRating(0));
  this->ci_for_non_used_fusion_modules.ConnectTo(fusion_rel_angle_left_hip_x->InputPort(0, 0));

  mbbFusion<double> * fusion_rel_angle_left_hip_y = new mbbFusion<double>(this,  "Rel. Angle Left Hip Y", 2, tStimulationMode::ENABLED, 1);
  fusion_rel_angle_left_hip_y->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
  mbbFusion<double> * fusion_rel_angle_left_hip_z = new mbbFusion<double>(this, "Rel. Angle Left Hip Z", 1, tStimulationMode::ENABLED, 1);
  fusion_rel_angle_left_hip_z->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
  this->ci_for_non_used_fusion_modules.ConnectTo(fusion_rel_angle_left_hip_z->InputActivity(0));
  this->ci_for_non_used_fusion_modules.ConnectTo(fusion_rel_angle_left_hip_z->InputTargetRating(0));
  this->ci_for_non_used_fusion_modules.ConnectTo(fusion_rel_angle_left_hip_z->InputPort(0, 0));


  mbbFusion<double> * fusion_abs_torque_left_hip_x = new mbbFusion<double>(this, "Abs. Torque Left Hip X", 7, tStimulationMode::ENABLED);
  fusion_abs_torque_left_hip_x->fusion_method.Set(tFusionMethod::WEIGHTED_SUM);
  mbbFusion<double> * fusion_abs_torque_left_hip_y = new mbbFusion<double>(this, "Abs. Torque Left Hip Y", 14, tStimulationMode::ENABLED);
  fusion_abs_torque_left_hip_y->fusion_method.Set(tFusionMethod::WEIGHTED_SUM);
  mbbFusion<double> * fusion_abs_torque_left_hip_z = new mbbFusion<double>(this, "Abs. Torque Left Hip Z", 4, tStimulationMode::ENABLED);
  fusion_abs_torque_left_hip_z->fusion_method.Set(tFusionMethod::WEIGHTED_SUM);

  //fusion of cyclic walking and termination
  mbbFusion<double> * fusion_angle_left_hip_x = new mbbFusion<double>(this,  "Fusion Angle Left Hip X", 3, tStimulationMode::ENABLED);
  fusion_angle_left_hip_x->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
  mbbFusion<double> * fusion_angle_left_hip_y = new mbbFusion<double>(this, "Fusion Angle Left Hip Y", 3, tStimulationMode::ENABLED);
  fusion_angle_left_hip_y->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
  mbbFusion<double> * fusion_angle_left_hip_z = new mbbFusion<double>(this, "Fusion Angle Left Hip Z", 3, tStimulationMode::ENABLED);
  fusion_angle_left_hip_z->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);

  mbbFusion<double> * fusion_torque_left_hip_x = new mbbFusion<double>(this, "Fusion Torque Left Hip X", 3, tStimulationMode::ENABLED);
  fusion_torque_left_hip_x->fusion_method.Set(tFusionMethod::WEIGHTED_SUM);
  mbbFusion<double> * fusion_torque_left_hip_y = new mbbFusion<double>(this, "Fusion Torque Left Hip Y", 3, tStimulationMode::ENABLED);
  fusion_torque_left_hip_y->fusion_method.Set(tFusionMethod::WEIGHTED_SUM);
  mbbFusion<double> * fusion_torque_left_hip_z = new mbbFusion<double>(this,  "Fusion Torque Left Hip Z", 3, tStimulationMode::ENABLED);
  fusion_torque_left_hip_z->fusion_method.Set(tFusionMethod::WEIGHTED_SUM);

  //memory unit for activity of sensory output of torque control
  mMemoryUnit * memory_abs_torque_left_hip_x = new mMemoryUnit(this, "Memory Torque Left Hip X");

  mMemoryUnit * memory_abs_torque_left_hip_y = new mMemoryUnit(this, "Memory Torque Left Hip Y");

  mMemoryUnit * memory_abs_torque_left_hip_z = new mMemoryUnit(this, "Memory Torque Left Hip Z");

  // right hip
  mbbFusion<double> * fusion_abs_angle_right_hip_x = new mbbFusion<double>(this,  "Abs. Angle Right Hip X", 5, tStimulationMode::ENABLED, 1);
  fusion_abs_angle_right_hip_x->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
  fusion_abs_angle_right_hip_x->number_of_inhibition_ports.Set(1);
  fusion_abs_angle_right_hip_x->CheckStaticParameters();
  mbbFusion<double> * fusion_abs_angle_right_hip_y = new mbbFusion<double>(this,  "Abs. Angle Right Hip Y", 3, tStimulationMode::ENABLED, 1);
  fusion_abs_angle_right_hip_y->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
  fusion_abs_angle_right_hip_y->number_of_inhibition_ports.Set(1);
  fusion_abs_angle_right_hip_y->CheckStaticParameters();
  mbbFusion<double> * fusion_abs_angle_right_hip_z = new mbbFusion<double>(this, "Abs. Angle Right Hip Z", 5, tStimulationMode::ENABLED, 1);
  fusion_abs_angle_right_hip_z->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
  fusion_abs_angle_right_hip_z->number_of_inhibition_ports.Set(1);
  fusion_abs_angle_right_hip_z->CheckStaticParameters();

  mbbFusion<double> * fusion_rel_angle_right_hip_x = new mbbFusion<double>(this,  "Rel. Angle Right Hip X", 1, tStimulationMode::ENABLED);
  fusion_rel_angle_right_hip_x->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
  this->ci_for_non_used_fusion_modules.ConnectTo(fusion_rel_angle_right_hip_x->InputActivity(0));
  this->ci_for_non_used_fusion_modules.ConnectTo(fusion_rel_angle_right_hip_x->InputTargetRating(0));
  this->ci_for_non_used_fusion_modules.ConnectTo(fusion_rel_angle_right_hip_x->InputPort(0, 0));
  mbbFusion<double> * fusion_rel_angle_right_hip_y = new mbbFusion<double>(this,  "Rel. Angle Right Hip Y", 2, tStimulationMode::ENABLED, 1);
  fusion_rel_angle_right_hip_y->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
  mbbFusion<double> * fusion_rel_angle_right_hip_z = new mbbFusion<double>(this, "Rel. Angle Right Hip Z", 1, tStimulationMode::ENABLED);
  fusion_rel_angle_right_hip_z->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
  this->ci_for_non_used_fusion_modules.ConnectTo(fusion_rel_angle_right_hip_z->InputActivity(0));
  this->ci_for_non_used_fusion_modules.ConnectTo(fusion_rel_angle_right_hip_z->InputTargetRating(0));
  this->ci_for_non_used_fusion_modules.ConnectTo(fusion_rel_angle_right_hip_z->InputPort(0, 0));

  mbbFusion<double> * fusion_abs_torque_right_hip_x = new mbbFusion<double>(this, "Abs. Torque Right Hip X", 7, tStimulationMode::ENABLED);
  fusion_abs_torque_right_hip_x->fusion_method.Set(tFusionMethod::WEIGHTED_SUM);
  mbbFusion<double> * fusion_abs_torque_right_hip_y = new mbbFusion<double>(this, "Abs. Torque Right Hip Y", 14, tStimulationMode::ENABLED);
  fusion_abs_torque_right_hip_y->fusion_method.Set(tFusionMethod::WEIGHTED_SUM);
  mbbFusion<double> * fusion_abs_torque_right_hip_z = new mbbFusion<double>(this, "Abs. Torque Right Hip Z", 4, tStimulationMode::ENABLED);
  fusion_abs_torque_right_hip_z->fusion_method.Set(tFusionMethod::WEIGHTED_SUM);

  //fusion of cyclic walking and termination
  mbbFusion<double> * fusion_angle_right_hip_x = new mbbFusion<double>(this,  "Fusion Angle Right Hip X", 3, tStimulationMode::ENABLED);
  fusion_angle_right_hip_x->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
  mbbFusion<double> * fusion_angle_right_hip_y = new mbbFusion<double>(this, "Fusion Angle Right Hip Y", 3, tStimulationMode::ENABLED);
  fusion_angle_right_hip_y->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
  mbbFusion<double> * fusion_angle_right_hip_z = new mbbFusion<double>(this, "Fusion Angle Right Hip Z", 3, tStimulationMode::ENABLED);
  fusion_angle_right_hip_z->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);

  mbbFusion<double> * fusion_torque_right_hip_x = new mbbFusion<double>(this, "Fusion Torque Right Hip X", 3, tStimulationMode::ENABLED);
  fusion_torque_right_hip_x->fusion_method.Set(tFusionMethod::WEIGHTED_SUM);
  mbbFusion<double> * fusion_torque_right_hip_y = new mbbFusion<double>(this, "Fusion Torque Right Hip Y", 3, tStimulationMode::ENABLED);
  fusion_torque_right_hip_y->fusion_method.Set(tFusionMethod::WEIGHTED_SUM);
  mbbFusion<double> * fusion_torque_right_hip_z = new mbbFusion<double>(this,  "Fusion Torque Right Hip Z", 3, tStimulationMode::ENABLED);
  fusion_torque_right_hip_z->fusion_method.Set(tFusionMethod::WEIGHTED_SUM);

  //memory unit for activity of sensory output of torque control
  mMemoryUnit * memory_abs_torque_right_hip_x = new mMemoryUnit(this, "Memory Torque Right Hip X");

  mMemoryUnit * memory_abs_torque_right_hip_y = new mMemoryUnit(this, "Memory Torque Right Hip Y");

  mMemoryUnit * memory_abs_torque_right_hip_z = new mMemoryUnit(this, "Memory Torque Right Hip Z");

  //fusion of cyclic walking and termination
  mbbFusion<double> * fusion_angle_spine_x = new mbbFusion<double>(this,  "Fusion Angle Spine X", 3, tStimulationMode::ENABLED);
  fusion_angle_spine_x->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
  mbbFusion<double> * fusion_angle_spine_y = new mbbFusion<double>(this, "Fusion Angle Spine Y", 3, tStimulationMode::ENABLED);
  fusion_angle_spine_y->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
  mbbFusion<double> * fusion_angle_spine_z = new mbbFusion<double>(this, "Fusion Angle Spine Z", 3, tStimulationMode::ENABLED);
  fusion_angle_spine_z->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);

  mbbFusion<double> * fusion_torque_spine_x = new mbbFusion<double>(this, "Fusion Torque Spine X", 3, tStimulationMode::ENABLED);
  fusion_torque_spine_x->fusion_method.Set(tFusionMethod::WEIGHTED_SUM);
  mbbFusion<double> * fusion_torque_spine_y = new mbbFusion<double>(this, "Fusion Torque Spine Y", 3, tStimulationMode::ENABLED);
  fusion_torque_spine_y->fusion_method.Set(tFusionMethod::WEIGHTED_SUM);
  mbbFusion<double> * fusion_torque_spine_z = new mbbFusion<double>(this,  "Fusion Torque Spine Z", 3, tStimulationMode::ENABLED);
  fusion_torque_spine_z->fusion_method.Set(tFusionMethod::WEIGHTED_SUM);

  mbbFusion<double> * fusion_brake_torque_left_hip_y = new mbbFusion<double>(this, "(F) Brake Torque Left Hip Y", 1, tStimulationMode::ENABLED);
  fusion_brake_torque_left_hip_y->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
//  mbbFusion<double> * fusion_brake_torque_right_hip_y = new mbbFusion<double>(this, "(F) Brake Torque Right Hip Y", 1, tStimulationMode::ENABLED);
//  fusion_brake_torque_right_hip_y->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
//  this->ci_for_non_used_fusion_modules.ConnectTo(fusion_brake_torque_right_hip_y->InputActivity(0));
//  this->ci_for_non_used_fusion_modules.ConnectTo(fusion_brake_torque_right_hip_y->InputTargetRating(0));
//  this->ci_for_non_used_fusion_modules.ConnectTo(fusion_brake_torque_right_hip_y->InputPort(0, 0));

  // lateral hip shift
  mbbFusion<double> * fusion_lateral_hip_shift_left = new mbbFusion<double>(this, "Lateral Hip Shift Left", 2, tStimulationMode::ENABLED);
  fusion_lateral_hip_shift_left->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
  mbbFusion<double> * fusion_lateral_hip_shift_right = new mbbFusion<double>(this, "Lateral Hip Shift Right", 3, tStimulationMode::ENABLED);
  fusion_lateral_hip_shift_right->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);

  // active hip swing
  mbbFusion<double> * fusion_active_hip_swing_left = new mbbFusion<double>(this, "Active Hip Swing Left", 3, tStimulationMode::ENABLED);
  fusion_active_hip_swing_left->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
  mbbFusion<double> * fusion_active_hip_swing_right = new mbbFusion<double>(this, "Active Hip Swing Right", 3, tStimulationMode::ENABLED);
  fusion_active_hip_swing_right->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);

  // turn motor patterns
  mbbFusion<double> *fusion_mp_turn_left_left = new mbbFusion<double>(this, "MP Turn2Left, Left", 2, tStimulationMode::AUTO);
  fusion_mp_turn_left_left->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
  mbbFusion<double> *fusion_mp_turn_right_left = new mbbFusion<double>(this, "MP Turn2Right, Left", 2, tStimulationMode::DISABLED);
  fusion_mp_turn_right_left->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
  mbbFusion<double> *fusion_mp_turn_left_right = new mbbFusion<double>(this, "MP Turn2Left, Right", 2, tStimulationMode::DISABLED);
  fusion_mp_turn_left_right->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
  mbbFusion<double> *fusion_mp_turn_right_right = new mbbFusion<double>(this, "MP Turn2Right, Right", 2, tStimulationMode::DISABLED);
  fusion_mp_turn_right_right->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
  mbbFusion<double> *fusion_mp_turn_left_spine = new mbbFusion<double>(this, "MP Turn2Left, Spine", 2, tStimulationMode::ENABLED);
  fusion_mp_turn_left_spine->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
  mbbFusion<double> *fusion_mp_turn_right_spine = new mbbFusion<double>(this, "MP Turn2Right, Spine", 2, tStimulationMode::ENABLED);
  fusion_mp_turn_right_spine->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);

  // lock hip
  mbbFusion<double> * fusion_lock_hip_left = new mbbFusion<double>(this, "Lock Hip Left", 2, tStimulationMode::ENABLED);
  fusion_lock_hip_left->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
  mbbFusion<double> * fusion_lock_hip_right = new mbbFusion<double>(this, "Lock Hip Right", 2, tStimulationMode::ENABLED);
  fusion_lock_hip_right->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);


  // upright trunk fusion
  mbbFusion<double> * fusion_upright_trunk_left = new mbbFusion<double>(this, "Upright Trunk Left",  5, tStimulationMode::ENABLED);
  fusion_upright_trunk_left->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
  mbbFusion<double> * fusion_upright_trunk_right = new mbbFusion<double>(this, "Upright Trunk Right",  5, tStimulationMode::ENABLED);
  fusion_upright_trunk_right->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);

  // lateral foot placement fusion
  mbbFusion<double> * fusion_lateral_foot_placement_left = new mbbFusion<double>(this, "Lateral Foot Placement Left",  2, tStimulationMode::ENABLED);
  fusion_lateral_foot_placement_left->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
  mbbFusion<double> * fusion_lateral_foot_placement_right = new mbbFusion<double>(this, "Lateral Foot Placement Right", 2, tStimulationMode::ENABLED);
  fusion_lateral_foot_placement_right->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);

  // control pelvis roll fusion
  mbbFusion<double> * fusion_control_pelvis_roll_left = new mbbFusion<double>(this, "Control Pelvis Roll Left", 2, tStimulationMode::ENABLED);
  fusion_control_pelvis_roll_left->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
  mbbFusion<double> * fusion_control_pelvis_roll_right = new mbbFusion<double>(this, "Control Pelvis Roll Right", 2, tStimulationMode::ENABLED);
  fusion_control_pelvis_roll_right->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);

  //control pelvis yaw fusion
  mbbFusion<double> * fusion_control_pelvis_yaw_left = new mbbFusion<double>(this, "Control Pelvis Yaw Left",  2, tStimulationMode::ENABLED);
  fusion_control_pelvis_yaw_left->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
  mbbFusion<double> * fusion_control_pelvis_yaw_right = new mbbFusion<double>(this, "Control Pelvis Yaw Right", 1, tStimulationMode::ENABLED);
  fusion_control_pelvis_yaw_right->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);


  // compensation hip roll during walking
  mbbFusion<double> * fusion_compensation_hip_roll = new mbbFusion<double>(this, "Compensate Hip Roll", 4, tStimulationMode::DISABLED);
  fusion_compensation_hip_roll->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);

  // lateral walking for hip x
  mbbFusion<double> *fusion_lateral_step_left_hip_x_left = new mbbFusion<double>(this, "Lateral Step Left Hip X Left", 1, tStimulationMode::ENABLED);
  fusion_lateral_step_left_hip_x_left->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
  this->ci_for_non_used_fusion_modules.ConnectTo(fusion_lateral_step_left_hip_x_left->InputActivity(0));
  this->ci_for_non_used_fusion_modules.ConnectTo(fusion_lateral_step_left_hip_x_left->InputTargetRating(0));
  this->ci_for_non_used_fusion_modules.ConnectTo(fusion_lateral_step_left_hip_x_left->InputPort(0, 0));
  mbbFusion<double> *fusion_lateral_step_left_hip_x_right = new mbbFusion<double>(this, "Lateral Step Left Hip X Right", 1, tStimulationMode::ENABLED);
  fusion_lateral_step_left_hip_x_right->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
  this->ci_for_non_used_fusion_modules.ConnectTo(fusion_lateral_step_left_hip_x_right->InputActivity(0));
  this->ci_for_non_used_fusion_modules.ConnectTo(fusion_lateral_step_left_hip_x_right->InputTargetRating(0));
  this->ci_for_non_used_fusion_modules.ConnectTo(fusion_lateral_step_left_hip_x_right->InputPort(0, 0));
  mbbFusion<double> *fusion_lateral_step_right_hip_x_left = new mbbFusion<double>(this, "Lateral Step Right Hip X Left", 1, tStimulationMode::ENABLED);
  fusion_lateral_step_right_hip_x_left->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
  this->ci_for_non_used_fusion_modules.ConnectTo(fusion_lateral_step_right_hip_x_left->InputActivity(0));
  this->ci_for_non_used_fusion_modules.ConnectTo(fusion_lateral_step_right_hip_x_left->InputTargetRating(0));
  this->ci_for_non_used_fusion_modules.ConnectTo(fusion_lateral_step_right_hip_x_left->InputPort(0, 0));
  mbbFusion<double> *fusion_lateral_step_right_hip_x_right = new mbbFusion<double>(this, "Lateral Step Right Hip X Right", 1, tStimulationMode::ENABLED);
  fusion_lateral_step_right_hip_x_right->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
  this->ci_for_non_used_fusion_modules.ConnectTo(fusion_lateral_step_right_hip_x_right->InputActivity(0));
  this->ci_for_non_used_fusion_modules.ConnectTo(fusion_lateral_step_right_hip_x_right->InputTargetRating(0));
  this->ci_for_non_used_fusion_modules.ConnectTo(fusion_lateral_step_right_hip_x_right->InputPort(0, 0));

  //END initiating fusion behaviours

  // STANDING CONTROL UNITS
  // ================================================

  // stand
  mbbLowerTrunkStand* skill_stand = new mbbLowerTrunkStand(this, "(S) Stand", tStimulationMode::AUTO);

  // hold position
  gbbLowerTrunkHoldPosition* hold_position_group = new gbbLowerTrunkHoldPosition(this, "(G) Hold Joint Pos. LowerTrunk", tStimulationMode::AUTO);

  // lateral walking
  gLowerTrunkLateralWalking * lateral_walking_group = new gLowerTrunkLateralWalking(this, "(G) Lateral Walking Group");

  // adjust joint reflexes
  mbbAdjustJointAngle * relax_spine_x = new mbbAdjustJointAngle(this, "(PR) Relax Spine X", tStimulationMode::AUTO);
  mbbAdjustJointAngle * relax_spine_y = new mbbAdjustJointAngle(this, "(PR) Relax Spine Y", tStimulationMode::AUTO);
  mbbAdjustCorrespondingJointAngles * relax_hip_y = new mbbAdjustCorrespondingJointAngles(this, "(PR) Relax Hip Y", tStimulationMode::AUTO);
  mbbAdjustCorrespondingJointAngles* reduce_tension_hip_y = new mbbAdjustCorrespondingJointAngles(this, "(PR) Reduce Tension Hip Y", tStimulationMode::AUTO);


  //Disturbance Estimation and Compensation
  mbbCompensationHipPitch * skill_compensation_hip_pitch = new mbbCompensationHipPitch(this, " (PR) Compensation Hip Pitch", tStimulationMode::DISABLED);

  mbbCompensationHipRoll * skill_compensation_hip_roll = new mbbCompensationHipRoll(this, " (PR) Compensation Hip Roll", tStimulationMode::DISABLED);

  //mbbCompensationSpineRoll *skill_compensation_spine_roll = new mbbCompensationSpineRoll(this, " (PR) Compensation Spine Roll", tStimulationMode::DISABLED);

  // DEC for spine pitch angle
  mbbCompensationSpinePitch * skill_compensation_spine_pitch = new mbbCompensationSpinePitch(this, " (PR) Compensation Spine Pitch", tStimulationMode::DISABLED);


  // WALKING CONTROL UNITS
  // ================================================

  // walking initiation
  mbbStimulationSkill* skill_walking_init_left = new mbbStimulationSkill(this,  "(Ph) Walking Init Left", tStimulationMode::AUTO, 0, 5);
  skill_walking_init_left->SetConfigNode("/LowerTrunk/phases/walking_initiation/");
  skill_walking_init_left->par_stimulation_factor[0].SetConfigEntry("lateral_hip_shift/stimulation_factor");
  skill_walking_init_left->par_min_angle[0].SetConfigEntry("lateral_hip_shift/min_angle");
  skill_walking_init_left->par_max_angle[0].SetConfigEntry("lateral_hip_shift/max_angle");
  skill_walking_init_left->par_target_angle[0].SetConfigEntry("lateral_hip_shift/target_angle");
  skill_walking_init_left->par_stimulation_factor[1].SetConfigEntry("active_hip_swing/stimulation_factor");
  skill_walking_init_left->par_stimulation_factor[2].SetConfigEntry("upright_trunk_left/stimulation_factor");
  skill_walking_init_left->par_stimulation_factor[3].SetConfigEntry("upright_trunk_right/stimulation_factor");
  skill_walking_init_left->par_stimulation_factor[4].SetConfigEntry("lift_leg/stimulation_factor");

  mbbStimulationSkill* skill_walking_init_right = new mbbStimulationSkill(this, "(Ph) Walking Init Right", tStimulationMode::AUTO, 0, 5);
  skill_walking_init_right->SetConfigNode("/LowerTrunk/phases/walking_initiation/");
  skill_walking_init_right->par_stimulation_factor[0].SetConfigEntry("ateral_hip_shift/stimulation_factor");
  skill_walking_init_right->par_min_angle[0].SetConfigEntry("lateral_hip_shift");
  skill_walking_init_right->par_max_angle[0].SetConfigEntry("lateral_hip_shift");
  skill_walking_init_right->par_target_angle[0].SetConfigEntry("lateral_hip_shift");


  skill_walking_init_right->par_stimulation_factor[1].SetConfigEntry("active_hip_swing/stimulation_factor");
  skill_walking_init_right->par_stimulation_factor[2].SetConfigEntry("upright_trunk_left/stimulation_factor");
  skill_walking_init_right->par_stimulation_factor[3].SetConfigEntry("upright_trunk_right/stimulation_factor");
  skill_walking_init_right->par_stimulation_factor[4].SetConfigEntry("lift_leg/stimulation_factor");

  // walking initiation for lateral walking
  mbbStimulationSkill* skill_walking_lateral_init_left = new mbbStimulationSkill(this, "(Ph) Walking Lateral Init Left", tStimulationMode::AUTO, 0, 4);
  skill_walking_lateral_init_left->SetConfigNode("/LowerTrunk/phases/walking_lateral_initiation/");
  skill_walking_lateral_init_left->par_stimulation_factor[0].SetConfigEntry("lateral_hip_shift/");
  skill_walking_lateral_init_left->par_min_angle[0].SetConfigEntry("lateral_hip_shift");
  skill_walking_lateral_init_left->par_max_angle[0].SetConfigEntry("lateral_hip_shift");
  skill_walking_lateral_init_left->par_target_angle[0].SetConfigEntry("lateral_hip_shift");

  skill_walking_lateral_init_left->par_stimulation_factor[1].SetConfigEntry("walking_lateral_initiation/upright_trunk_left");
  skill_walking_lateral_init_left->par_stimulation_factor[2].SetConfigEntry("walking_lateral_initiation/upright_trunk_right");
  skill_walking_lateral_init_left->par_stimulation_factor[3].SetConfigEntry("walking_lateral_initiation/hip_y_torque");


  mbbStimulationSkill* skill_walking_lateral_init_right = new mbbStimulationSkill(this, "(Ph) Walking Lateral Init Right", tStimulationMode::AUTO, 0, 4);
  skill_walking_lateral_init_right->SetConfigNode("/LowerTrunk/phases/walking_lateral_initiation/");
  skill_walking_lateral_init_right->par_stimulation_factor[0].SetConfigEntry("lateral_hip_shift/stimulation_factor");
  skill_walking_lateral_init_right->par_min_angle[0].SetConfigEntry("lateral_hip_shift/min_angle");
  skill_walking_lateral_init_right->par_max_angle[0].SetConfigEntry("lateral_hip_shift/max_angle");
  skill_walking_lateral_init_right->par_target_angle[0].SetConfigEntry("lateral_hip_shift/target_angle");

  skill_walking_lateral_init_right->par_stimulation_factor[1].SetConfigEntry("upright_trunk_left/stimulation_factor");
  skill_walking_lateral_init_right->par_stimulation_factor[2].SetConfigEntry("upright_trunk_right/stimulation_factor");
  skill_walking_lateral_init_right->par_stimulation_factor[3].SetConfigEntry("hip_y_torque/stimulation_factor");

  // walking phases
  mbbStimulationSkill* skill_walking = new mbbStimulationSkill(this, "(Ph) Walking All Phases", tStimulationMode::AUTO, 0, 9);
  skill_walking->SetConfigNode("/LowerTrunk/phases/");
  skill_walking->par_stimulation_factor[0].SetConfigEntry("walking_all_phases/spine_x_angle/stimulation_factor");
  skill_walking->par_min_angle[0].SetConfigEntry("walking_all_phases/spine_x_angle/min_angle");
  skill_walking->par_max_angle[0].SetConfigEntry("walking_all_phases/spine_x_angle/max_angle");
  skill_walking->par_target_angle[0].SetConfigEntry("walking_all_phases/spine_x_angle/target_angle");

  skill_walking->par_stimulation_factor[1].SetConfigEntry("walking_all_phases/spine_y_angle/stimulation_factor");
  skill_walking->par_min_angle[1].SetConfigEntry("walking_all_phases/spine_y_angle/min_angle");
  skill_walking->par_max_angle[1].SetConfigEntry("walking_all_phases/spine_y_angle/max_angle");
  skill_walking->par_target_angle[1].SetConfigEntry("walking_all_phases/spine_y_angle/target_angle");

  skill_walking->par_stimulation_factor[2].SetConfigEntry("walking_all_phases/spine_z_angle/stimulation_factor");
  skill_walking->par_min_angle[2].SetConfigEntry("walking_all_phases/spine_z_angle/min_angle");
  skill_walking->par_max_angle[2].SetConfigEntry("walking_all_phases/spine_z_angle/max_angle");
  skill_walking->par_target_angle[2].SetConfigEntry("walking_all_phases/spine_z_angle/target_angle");

  skill_walking->par_stimulation_factor[3].SetConfigEntry("walking_all_phases/hip_x_angle/stimulation_factor");
  skill_walking->par_min_angle[3].SetConfigEntry("walking_all_phases/hip_x_angle/min_angle");
  skill_walking->par_max_angle[3].SetConfigEntry("walking_all_phases/hip_x_angle/max_angle");
  skill_walking->par_target_angle[3].SetConfigEntry("walking_all_phases/hip_x_angle/target_angle");

  skill_walking->par_stimulation_factor[4].SetConfigEntry("walking_all_phases/hip_z_angle/stimulation_factor");
  skill_walking->par_min_angle[4].SetConfigEntry("walking_all_phases/hip_z_angle/min_angle");
  skill_walking->par_max_angle[4].SetConfigEntry("walking_all_phases/hip_z_angle/max_angle");
  skill_walking->par_target_angle[4].SetConfigEntry("walking_all_phases/hip_z_angle/target_angle");

  skill_walking->par_stimulation_factor[5].SetConfigEntry("walking_all_phases/hold_pos_hip_x/stimulation_factor");
  skill_walking->par_min_angle[5].SetConfigEntry("walking_all_phases/hold_pos_hip_x/min_angle");
  skill_walking->par_max_angle[5].SetConfigEntry("walking_all_phases/hold_pos_hip_x/max_angle");
  skill_walking->par_target_angle[5].SetConfigEntry("walking_all_phases/hold_pos_hip_x/target_angle");

  skill_walking->par_stimulation_factor[6].SetConfigEntry("walking_all_phases/balance_spine_x/stimulation_factor");
  skill_walking->par_stimulation_factor[7].SetConfigEntry("walking_all_phases/upright_trunk_left/stimulation_factor");
  skill_walking->par_stimulation_factor[8].SetConfigEntry("walking_all_phases/upright_trunk_right/stimulation_factor");

  mbbStimulationSkill* skill_walking_phase1_left = new mbbStimulationSkill(this, "(Ph) Walking Phase 1 Left", tStimulationMode::AUTO, 0, 5);
  skill_walking_phase1_left->SetConfigNode("/LowerTrunk/phases/");
  skill_walking_phase1_left->par_stimulation_factor[0].SetConfigEntry("walking_phase_1/control_pelvis_roll/stimulation_factor");
  skill_walking_phase1_left->par_stimulation_factor[1].SetConfigEntry("walking_phase_1/turn_left/stimulation_factor");
  skill_walking_phase1_left->par_stimulation_factor[2].SetConfigEntry("walking_phase_1/turn_right/stimulation_factor");
  skill_walking_phase1_left->par_stimulation_factor[3].SetConfigEntry("walking_phase_1/control_pelvis_yaw/stimulation_factor");
  skill_walking_phase1_left->par_stimulation_factor[4].SetConfigEntry("walking_phase_1/hip_y_brake_torque/stimulation_factor");

  mbbStimulationSkill* skill_walking_phase1_right = new mbbStimulationSkill(this, "(Ph) Walking Phase 1 Right", tStimulationMode::AUTO, 0, 5);
  skill_walking_phase1_right->SetConfigNode("/LowerTrunk/phases");
  skill_walking_phase1_right->par_stimulation_factor[0].SetConfigEntry("walking_phase_1/control_pelvis_roll/stimulation_factor");
  skill_walking_phase1_right->par_stimulation_factor[1].SetConfigEntry("walking_phase_1/turn_left/stimulation_factor");
  skill_walking_phase1_right->par_stimulation_factor[2].SetConfigEntry("walking_phase_1/turn_right/stimulation_factor");
  skill_walking_phase1_right->par_stimulation_factor[3].SetConfigEntry("walking_phase_1/control_pelvis_yaw/stimulation_factor");
  skill_walking_phase1_right->par_stimulation_factor[4].SetConfigEntry("walking_phase_1/hip_y_brake_torque/stimulation_factor");

  mbbStimulationSkill* skill_walking_phase2_left = new mbbStimulationSkill(this, "(Ph) Walking Phase 2 Left", tStimulationMode::AUTO, 0, 5);
  skill_walking_phase2_left->SetConfigNode("/LowerTrunk/phases/");
  skill_walking_phase2_left->par_stimulation_factor[0].SetConfigEntry("walking_phase_2/active_hip_swing/stimulation_factor");
  skill_walking_phase2_left->par_stimulation_factor[1].SetConfigEntry("walking_phase_2/control_pelvis_roll/stimulation_factor");
  skill_walking_phase2_left->par_stimulation_factor[2].SetConfigEntry("walking_phase_2/turn_left/stimulation_factor");
  skill_walking_phase2_left->par_stimulation_factor[3].SetConfigEntry("walking_phase_2/turn_right/stimulation_factor");
  skill_walking_phase2_left->par_stimulation_factor[4].SetConfigEntry("walking_phase_2/hip_y_brake_torque/stimulation_factor");

  mbbStimulationSkill* skill_walking_phase2_right = new mbbStimulationSkill(this, "(Ph) Walking Phase 2 Right", tStimulationMode::AUTO, 0, 5);
  skill_walking_phase2_right->SetConfigNode("/LowerTrunk/phases/");
  skill_walking_phase2_right->par_stimulation_factor[0].SetConfigEntry("walking_phase_2/active_hip_swing/stimulation_factor");
  skill_walking_phase2_right->par_stimulation_factor[1].SetConfigEntry("walking_phase_2/control_pelvis_roll/stimulation_factor");
  skill_walking_phase2_right->par_stimulation_factor[2].SetConfigEntry("walking_phase_2/turn_left/stimulation_factor");
  skill_walking_phase2_right->par_stimulation_factor[3].SetConfigEntry("walking_phase_2/turn_right/stimulation_factor");
  skill_walking_phase2_right->par_stimulation_factor[4].SetConfigEntry("walking_phase_2/hip_y_brake_torque/stimulation_factor");

  mbbStimulationSkill* skill_walking_phase3_left = new mbbStimulationSkill(this, "(Ph) Walking Phase 3 Left", tStimulationMode::AUTO, 0, 4);
  skill_walking_phase3_left->SetConfigNode("/LowerTrunk/phases/");
  skill_walking_phase3_left->par_stimulation_factor[0].SetConfigEntry("walking_phase_3/lateral_hip_shift/stimulation_factor");
  skill_walking_phase3_left->par_min_angle[0].SetConfigEntry("walking_phase_3/lateral_hip_shift/min_angle");
  skill_walking_phase3_left->par_max_angle[0].SetConfigEntry("walking_phase_3/lateral_hip_shift/max_angle");
  skill_walking_phase3_left->par_target_angle[0].SetConfigEntry("walking_phase_3/lateral_hip_shift/target_angle");
  skill_walking_phase3_left->par_stimulation_factor[1].SetConfigEntry("walking_phase_3/active_hip_swing/stimulation_factor");
  skill_walking_phase3_left->par_stimulation_factor[2].SetConfigEntry("walking_phase_3/turn_left/stimulation_factor");
  skill_walking_phase3_left->par_stimulation_factor[3].SetConfigEntry("walking_phase_3/turn_right/stimulation_factor");

  mbbStimulationSkill* skill_walking_phase3_right = new mbbStimulationSkill(this, "(Ph) Walking Phase 3 Right", tStimulationMode::AUTO, 0, 4);
  skill_walking_phase3_right->SetConfigNode("/LowerTrunk/phases/");
  skill_walking_phase3_right->par_stimulation_factor[0].SetConfigEntry("walking_phase_3/lateral_hip_shift/stimulation_factor");
  skill_walking_phase3_right->par_min_angle[0].SetConfigEntry("walking_phase_3/lateral_hip_shift/min_angle");
  skill_walking_phase3_right->par_max_angle[0].SetConfigEntry("walking_phase_3/lateral_hip_shift/max_angle");
  skill_walking_phase3_right->par_target_angle[0].SetConfigEntry("walking_phase_3/lateral_hip_shift/target_angle");
  skill_walking_phase3_right->par_stimulation_factor[1].SetConfigEntry("walking_phase_3/active_hip_swing/stimulation_factor");
  skill_walking_phase3_right->par_stimulation_factor[2].SetConfigEntry("walking_phase_3/turn_left/stimulation_factor");
  skill_walking_phase3_right->par_stimulation_factor[3].SetConfigEntry("walking_phase_3/turn_right/stimulation_factor");

  mbbStimulationSkill* skill_walking_phase4_left = new mbbStimulationSkill(this, "(Ph) Walking Phase 4 Left", tStimulationMode::AUTO, 0, 7);
  skill_walking_phase4_left->SetConfigNode("/LowerTrunk/phases/");
  skill_walking_phase4_left->par_stimulation_factor[0].SetConfigEntry("walking_phase_4/lift_hip_pos/stimulation_factor");
  skill_walking_phase4_left->par_min_angle[0].SetConfigEntry("walking_phase_4/lift_hip_pos/min_angle");
  skill_walking_phase4_left->par_max_angle[0].SetConfigEntry("walking_phase_4/lift_hip_pos/max_angle");
  skill_walking_phase4_left->par_target_angle[0].SetConfigEntry("walking_phase_4/lift_hip_pos/target_angle");
  skill_walking_phase4_left->par_stimulation_factor[1].SetConfigEntry("walking_phase_4/active_hip_swing/stimulation_factor");
  skill_walking_phase4_left->par_stimulation_factor[2].SetConfigEntry("walking_phase_4/lock_hip/stimulation_factor");
  skill_walking_phase4_left->par_stimulation_factor[3].SetConfigEntry("walking_phase_4/lateral_foot_placement/stimulation_factor");
  skill_walking_phase4_left->par_stimulation_factor[4].SetConfigEntry("walking_phase_4/turn_left/stimulation_factor");
  skill_walking_phase4_left->par_stimulation_factor[5].SetConfigEntry("walking_phase_4/turn_right/stimulation_factor");
  skill_walking_phase4_left->par_stimulation_factor[6].SetConfigEntry("walking_phase_4/lock_hip_roll/stimulation_factor");

  mbbStimulationSkill* skill_walking_phase4_right = new mbbStimulationSkill(this, "(Ph) Walking Phase 4 Right", tStimulationMode::AUTO, 0, 7);
  skill_walking_phase4_right->SetConfigNode("/LowerTrunk/phases/");
  skill_walking_phase4_right->par_stimulation_factor[0].SetConfigEntry("walking_phase_4/lift_hip_pos/stimulation_factor");
  skill_walking_phase4_right->par_min_angle[0].SetConfigEntry("walking_phase_4/lift_hip_pos/min_angle");
  skill_walking_phase4_right->par_max_angle[0].SetConfigEntry("walking_phase_4/lift_hip_pos/max_angle");
  skill_walking_phase4_right->par_target_angle[0].SetConfigEntry("walking_phase_4/lift_hip_pos/target_angle");
  skill_walking_phase4_right->par_stimulation_factor[1].SetConfigEntry("walking_phase_4/active_hip_swing/stimulation_factor");
  skill_walking_phase4_right->par_stimulation_factor[2].SetConfigEntry("walking_phase_4/lock_hip/stimulation_factor");
  skill_walking_phase4_right->par_stimulation_factor[3].SetConfigEntry("walking_phase_4/lateral_foot_placement/stimulation_factor");
  skill_walking_phase4_right->par_stimulation_factor[4].SetConfigEntry("walking_phase_4/turn_left/stimulation_factor");
  skill_walking_phase4_right->par_stimulation_factor[5].SetConfigEntry("walking_phase_4/turn_right/stimulation_factor");
  skill_walking_phase4_right->par_stimulation_factor[6].SetConfigEntry("walking_phase_4/lock_hip_roll/stimulation_factor");

  mbbStimulationSkill* skill_walking_phase5_left = new mbbStimulationSkill(this, "(Ph) Walking Phase 5 Left", tStimulationMode::AUTO, 0, 6);
  skill_walking_phase5_left->SetConfigNode("/LowerTrunk/phases/");
  skill_walking_phase5_left->par_stimulation_factor[0].SetConfigEntry("walking_phase_5/angle_hip_y/stimulation_factor");
  skill_walking_phase5_left->par_min_angle[0].SetConfigEntry("walking_phase_5/angle_hip_y/min_angle");
  skill_walking_phase5_left->par_max_angle[0].SetConfigEntry("walking_phase_5/angle_hip_y/max_angle");
  skill_walking_phase5_left->par_target_angle[0].SetConfigEntry("walking_phase_5/angle_hip_y/target_angle");
  skill_walking_phase5_left->par_stimulation_factor[1].SetConfigEntry("walking_phase_5/lift_hip_pos/stimulation_factor");
  skill_walking_phase5_left->par_stimulation_factor[2].SetConfigEntry("walking_phase_5/upright_trunk/stimulation_factor");
  skill_walking_phase5_left->par_stimulation_factor[3].SetConfigEntry("walking_phase_5/lock_hip/stimulation_factor");
  skill_walking_phase5_left->par_stimulation_factor[4].SetConfigEntry("walking_phase_5/lateral_foot_placement/stimulation_factor");
  skill_walking_phase5_left->par_stimulation_factor[5].SetConfigEntry("walking_phase_5/lock_hip_roll/stimulation_factor");

  mbbStimulationSkill* skill_walking_phase5_right = new mbbStimulationSkill(this, "(Ph) Walking Phase 5 Right", tStimulationMode::AUTO, 0, 6);
  skill_walking_phase5_right->SetConfigNode("/LowerTrunk/phases/");
  skill_walking_phase5_right->par_stimulation_factor[0].SetConfigEntry("walking_phase_5/angle_hip_y/stimulation_factor");
  skill_walking_phase5_right->par_min_angle[0].SetConfigEntry("walking_phase_5/angle_hip_y/min_angle");
  skill_walking_phase5_right->par_max_angle[0].SetConfigEntry("walking_phase_5/angle_hip_y/max_angle");
  skill_walking_phase5_right->par_target_angle[0].SetConfigEntry("walking_phase_5/angle_hip_y/target_angle");
  skill_walking_phase5_right->par_stimulation_factor[1].SetConfigEntry("walking_phase_5/lift_hip_pos/stimulation_factor");
  skill_walking_phase5_right->par_stimulation_factor[2].SetConfigEntry("walking_phase_5/upright_trunk/stimulation_factor");
  skill_walking_phase5_right->par_stimulation_factor[3].SetConfigEntry("walking_phase_5/lock_hip/stimulation_factor");
  skill_walking_phase5_right->par_stimulation_factor[4].SetConfigEntry("walking_phase_5/lateral_foot_placement/stimulation_factor");
  skill_walking_phase5_right->par_stimulation_factor[5].SetConfigEntry("walking_phase_5/lock_hip_roll/stimulation_factor");

  //walking termination group
  gLowerTrunkLastStep* termination_group = new gLowerTrunkLastStep(this, " (G) Lower Trunk Termination ");

  //BEGIN reflexes

  mbbLateralFootPlacement* lateral_foot_placement_left = new mbbLateralFootPlacement(this, "(PR) Lat. Foot Placem. Left", tStimulationMode::AUTO, 1.0, 1.0);
  lateral_foot_placement_left->number_of_inhibition_ports.Set(1);
  lateral_foot_placement_left->CheckStaticParameters();
  mbbLateralFootPlacement* lateral_foot_placement_right = new mbbLateralFootPlacement(this, "(PR) Lat. Foot Placem. Right", tStimulationMode::AUTO, 1.0, -1.0);
  lateral_foot_placement_right->number_of_inhibition_ports.Set(1);
  lateral_foot_placement_right->CheckStaticParameters();
  mbbBalanceBodyPitch *upright_trunk_left = new mbbBalanceBodyPitch(this, "(PR) Upright Trunk Left", tStimulationMode::AUTO);
  mbbBalanceBodyPitch *upright_trunk_right = new mbbBalanceBodyPitch(this, "(PR) Upright Trunk Right", tStimulationMode::AUTO);

  mbbAdjustBodyPitch *adjust_body_pitch = new mbbAdjustBodyPitch(this, "(PR) Adjust Body Pitch", tStimulationMode::DISABLED);

  mbbBalanceJoint *balance_spine_x = new mbbBalanceJoint(this, "(PR) Balance Spine X", tStimulationMode::AUTO, 0.0, -5.0, 0.0, 0.8, -2.0);

  mbbLockHip *lock_hip_left = new mbbLockHip(this, "(R) Lock Hip Left", tStimulationMode::AUTO, 0.0, 6.0);
  mbbLockHip *lock_hip_right = new mbbLockHip(this, "(R) Lock Hip Right", tStimulationMode::AUTO, 0.0, 6.0);


  mbbControlPelvisRoll* control_pelvis_roll_left = new mbbControlPelvisRoll(this, "(R) Control Pelvis Roll Left", tStimulationMode::AUTO, 0.0, -1.0);
  mbbControlPelvisRoll* control_pelvis_roll_right = new mbbControlPelvisRoll(this, "(R) Control Pelvis Roll Right", tStimulationMode::AUTO, 0.0, 1.0);

  mbbControlPelvisYaw* control_pelvis_yaw_left = new mbbControlPelvisYaw(this, "(R) Control Pelvis Yaw Left", tStimulationMode::AUTO, 0.0, -1.0);
  mbbControlPelvisYaw* control_pelvis_yaw_right = new mbbControlPelvisYaw(this, "(R) Control Pelvis Yaw Right", tStimulationMode::AUTO, 0.0, 1.0);

  // hold position during walking
  mbbHoldJointPosition* hold_joint_pos_hip_x_left = new mbbHoldJointPosition(this, "(R) Hold Joint Pos Hip X Left", tStimulationMode::AUTO, 0.0, 0.5, 0.0, 0.05, 1.0);
  mbbHoldJointPosition* hold_joint_pos_hip_x_right = new mbbHoldJointPosition(this,  "(R) Hold Joint Pos Hip X Right", tStimulationMode::AUTO, 0.0, 0.5, 0.0, 0.05, 1.0);

  //END reflexes

  //BEGIN motor patterns

  mbbLateralHipShift *lateral_hip_shift_left = new mbbLateralHipShift(this, "(MP) Lat. Hip Shift Left", tStimulationMode::AUTO, 0.0, mMotorPattern::tCurveMode::eSIGMOID, 600, 0.6, 0.7, mbbLateralHipShift::tEnumSide::eSIDE_LEFT);
  mbbLateralHipShift *lateral_hip_shift_right = new mbbLateralHipShift(this, "(MP) Lat. Hip Shift Right", tStimulationMode::AUTO, 0.0, mMotorPattern::tCurveMode::eSIGMOID, 600, 0.6, 0.7, mbbLateralHipShift::tEnumSide::eSIDE_RIGHT);

  // motor pattern only for walking initialization //todo:
  //mbbLateralHipShiftWalkingInit *lateral_hip_shift_right_walking_init = new mbbLateralHipShiftWalkingInit(this, "(MP) Lat. Hip Shift Right Walking Init", tStimulationMode::AUTO, 0.0, 600, 0.6, 0.7, mbbLateralHipShiftWalkingInit::tEnumSide::eSIDE_RIGHT);

  // motor pattern only for hip swing during walking initiation
  //mbbSingleTorquePattern* active_hip_swing_left_init = new mbbSingleTorquePattern(this, "(MP) Hip Swing Left Init",tStimulationMode::AUTO, 0, 1000, 0.85, 0.95, 0.73);

  mbbLateralHipShift *lateral_hip_shift_init_left = new mbbLateralHipShift(this,  "(MP) Lat. Hip Shift Left Init", tStimulationMode::AUTO, 0.0, mMotorPattern::tCurveMode::eSIGMOID, 600, 0.6, 0.7, mbbLateralHipShift::tEnumSide::eSIDE_LEFT);
  mbbLateralHipShift *lateral_hip_shift_init_right = new mbbLateralHipShift(this, "(MP) Lat. Hip Shift Right Init", tStimulationMode::AUTO, 0.0, mMotorPattern::tCurveMode::eSIGMOID, 600, 0.6, 0.7, mbbLateralHipShift::tEnumSide::eSIDE_RIGHT);

  mbbLateralHipShiftPushRecovery *lateral_hip_shift_left_push_recovery = new mbbLateralHipShiftPushRecovery(this, "(MP) Lat. Hip Shift L. Push Recovery", tStimulationMode::AUTO, 0.0, mMotorPattern::tCurveMode::eSIGMOID, 600, 0.1, 0.2, mbbLateralHipShiftPushRecovery::eSIDE_LEFT);
  mbbLateralHipShiftPushRecovery *lateral_hip_shift_right_push_recovery = new mbbLateralHipShiftPushRecovery(this, "(MP) Lat. Hip Shift R. Push Recovery", tStimulationMode::AUTO, 0.0, mMotorPattern::tCurveMode::eSIGMOID, 600, 0.1, 0.2, mbbLateralHipShiftPushRecovery::eSIDE_RIGHT);

  mbbActiveHipSwing *active_hip_swing_left = new mbbActiveHipSwing(this, "(MP) Active Hip Swing Left", tStimulationMode::DISABLED, 1.0, 440, 0.4, 140.824, 1.0); // speed = 0.8 -> 440, 0.35, 150
  active_hip_swing_left->number_of_inhibition_ports.Set(1);
  active_hip_swing_left->CheckStaticParameters();
  mbbActiveHipSwing *active_hip_swing_right = new mbbActiveHipSwing(this, "(MP) Active Hip Swing Right", tStimulationMode::DISABLED, 1.0, 440, 0.4, 140.824, 1.0); // speed = 0.8 -> 440, 0.35, 150
  active_hip_swing_right->number_of_inhibition_ports.Set(1);
  active_hip_swing_right->CheckStaticParameters();

  mbbHipSwingCurveWalking *hip_swing_curve_walking_left = new mbbHipSwingCurveWalking(this, "(MP) Hip Swing Curve Walking Left", tStimulationMode::AUTO, 0, mMotorPattern::tCurveMode::eSIGMOID, 440, 0.05, 0.2, 1.0); // speed = 0.8 -> 440, 0.35, 150
  hip_swing_curve_walking_left->number_of_inhibition_ports.Set(1);
  hip_swing_curve_walking_left->CheckStaticParameters();

  mbbHipSwingCurveWalking *hip_swing_curve_walking_right = new mbbHipSwingCurveWalking(this, "(MP) Hip Swing Curve Walking Right", tStimulationMode::AUTO, 0, mMotorPattern::tCurveMode::eSIGMOID, 440, 0.05, 0.2, 1.0); // speed = 0.8 -> 440, 0.35, 150
  hip_swing_curve_walking_right->number_of_inhibition_ports.Set(1);
  hip_swing_curve_walking_right->CheckStaticParameters();

  mbbActiveHipSwingSin *active_hip_swing_left_sin = new mbbActiveHipSwingSin(this, "(MP) Active Hip Swing Left Sinoid", tStimulationMode::AUTO, 1.0, mMotorPattern::tCurveMode::eSIGMOID, 440, 0.2, 0.5, 1.0); // eSIGMOID, 440, 0.2, 0.5, 1.0
  active_hip_swing_left_sin->number_of_inhibition_ports.Set(1);
  active_hip_swing_left_sin->CheckStaticParameters();
  mbbActiveHipSwingSin *active_hip_swing_right_sin = new mbbActiveHipSwingSin(this, "(MP) Active Hip Swing Right Sinoid", tStimulationMode::AUTO, 1.0, mMotorPattern::tCurveMode::eSIGMOID, 440, 0.2, 0.5, 1.0); // eSIGMOID, 440, 0.2, 0.5, 1.0
  active_hip_swing_right_sin->number_of_inhibition_ports.Set(1);
  active_hip_swing_right_sin->CheckStaticParameters();

  mbbHipSwingBentKnee *hip_swing_bent_knee_left = new mbbHipSwingBentKnee(this,  "(MP) Hip Swing Bent Knee Left", tStimulationMode::AUTO, 1, mMotorPattern::tCurveMode::eSIGMOID, 200, 0.15, 0.2, 1.0);
  hip_swing_bent_knee_left->number_of_inhibition_ports.Set(1);
  hip_swing_bent_knee_left->CheckStaticParameters();
  mbbHipSwingBentKnee *hip_swing_bent_knee_right = new mbbHipSwingBentKnee(this, "(MP) Hip Swing Bent Knee Right", tStimulationMode::AUTO, 1, mMotorPattern::tCurveMode::eSIGMOID, 200, 0.15, 0.2, 1.0);
  hip_swing_bent_knee_right->number_of_inhibition_ports.Set(1);
  hip_swing_bent_knee_right->CheckStaticParameters();
  // active hip swing in lateral initiation
  mbbActiveHipSwingSin* active_hip_swing_init_left = new mbbActiveHipSwingSin(this, "(MP) Active Hip Swing Init Left", tStimulationMode::AUTO, 0.0 , mMotorPattern::tCurveMode::eSIGMOID,  400, 0.1, 0.2 , 1.0); // 400, 0.1, 0.2 , 1.0
  mbbActiveHipSwingSin* active_hip_swing_init_right = new mbbActiveHipSwingSin(this, "(MP) Active Hip Swing Init Right", tStimulationMode::AUTO, 0.0 , mMotorPattern::tCurveMode::eSIGMOID, 400, 0.1, 0.2, 1.0);  // 400, 0.1, 0.2 , 1.0

  //
  mbbDoubleTorquePattern *hip_left_push_recovery_dece = new mbbDoubleTorquePattern(this, " (MP) Hip Swing Push Recovery Left Dece.", tStimulationMode::AUTO, 0.0, mMotorPattern::tCurveMode::eSIGMOID, 440, 0.01, 0.3, -0.8, 0.8, 1, 0.4);
  //mbbDoubleTorquePattern *hip_left_push_recovery_stop = new mbbDoubleTorquePattern(this, " (MP) Hip Swing Push Recovery Left Stop", tStimulationMode::AUTO, 0.0 , 440, 0.01, 0.3, 0.8, -0.8, 1, 0.4);

  mbbLowerTrunkLiftLeg *mp_lift_leg_left = new mbbLowerTrunkLiftLeg(this, "(MP) Lift Leg Left", tStimulationMode::AUTO, 0.0 , mMotorPattern::tCurveMode::eSIGMOID, 500, 0.3, 0.7);
  mbbLowerTrunkLiftLeg *mp_lift_leg_right = new mbbLowerTrunkLiftLeg(this,  "(MP) Lift Leg Right", tStimulationMode::AUTO, 0.0 , mMotorPattern::tCurveMode::eSIGMOID, 500, 0.3, 0.7);

  mbbDoubleLateralTorquePattern *mp_turn_left_left = new mbbDoubleLateralTorquePattern(this , "(MP) Turn2Left, Left", tStimulationMode::AUTO, 0.0, mMotorPattern::tCurveMode::eSIGMOID, 400, 0.1, 0.3, 1.0, 1.0, 1.0, 1.0);
  mbbDoubleLateralTorquePattern *mp_turn_right_left = new mbbDoubleLateralTorquePattern(this, "(MP) Turn2Right, Left", tStimulationMode::AUTO, 0.0 , mMotorPattern::tCurveMode::eSIGMOID, 400, 0.1, 0.3, -1.0, -1.0, 1.0, 1.0);
  mbbDoubleLateralTorquePattern *mp_turn_left_right = new mbbDoubleLateralTorquePattern(this, "(MP) Turn2Left, Right", tStimulationMode::AUTO, 0.0 , mMotorPattern::tCurveMode::eSIGMOID, 400, 0.1, 0.3, -1.0, -1.0, 1.0, 1.0);
  mbbDoubleLateralTorquePattern *mp_turn_right_right = new mbbDoubleLateralTorquePattern(this, "(MP) Turn2Right, Right", tStimulationMode::AUTO, 0.0, mMotorPattern::tCurveMode::eSIGMOID, 400, 0.1, 0.3, 1.0, 1.0, 1.0, 1.0);

  mbbTurnUpperTrunk *mp_turn_spine_left = new mbbTurnUpperTrunk(this, "(MP) Turn2Left, Spine", tStimulationMode::AUTO, 0.0, mMotorPattern::tCurveMode::eSIGMOID, 600, 0.4, 0.6, 1.0, -1.0);
  mbbTurnUpperTrunk *mp_turn_spine_right = new mbbTurnUpperTrunk(this, "(MP) Turn2Right, Spine", tStimulationMode::AUTO, 0.0 , mMotorPattern::tCurveMode::eSIGMOID, 600, 0.4, 0.6, 1.0, 1.0);


  mbbDoubleTorquePattern *mp_turn_leg_left_z = new mbbDoubleTorquePattern(this, "(MP) Turn2Left, Leg Z", tStimulationMode::AUTO, 0.0, mMotorPattern::tCurveMode::eSIGMOID, 500, 0.3, 0.5, -1.0, 1.0, 1.0, 1.0);
  mbbDoubleTorquePattern *mp_turn_leg_right_z = new mbbDoubleTorquePattern(this, "(MP) Turn2Right, Leg Z", tStimulationMode::AUTO, 0.0, mMotorPattern::tCurveMode::eSIGMOID, 500, 0.3, 0.5, -1.0, 1.0, 1.0, 1.0);

  //END motor patters


  //BEGIN fusion behaviours
  // ---------------------------------------------------------------------------------------------

  // fusion of termination and walking phases
  // termination group
  // begin of termination group
  this->GetControllerInputs().ConnectByName(termination_group->GetControllerInputs(), true);

  this->GetSensorInputs().ConnectByName(termination_group->GetSensorInputs(), true);
  //this->GetSensorInputs().ConnectByName(this->GetSensorOutputs(), false);
  this->si_left_hip_x_angle.ConnectTo(this->so_left_hip_x_angle);
  this->si_left_hip_y_angle.ConnectTo(this->so_left_hip_y_angle);
  this->si_left_hip_z_angle.ConnectTo(this->so_left_hip_z_angle);
  this->si_right_hip_x_angle.ConnectTo(this->so_right_hip_x_angle);
  this->si_right_hip_y_angle.ConnectTo(this->so_right_hip_y_angle);
  this->si_right_hip_z_angle.ConnectTo(this->so_right_hip_z_angle);
  this->si_spine_x_angle.ConnectTo(this->so_spine_x_angle);
  this->si_spine_y_angle.ConnectTo(this->so_spine_y_angle);
  this->si_spine_z_angle.ConnectTo(this->so_spine_z_angle);

  this->si_left_hip_x_torque.ConnectTo(this->so_left_hip_x_torque);
  this->si_left_hip_y_torque.ConnectTo(this->so_left_hip_y_torque);
  this->si_left_hip_z_torque.ConnectTo(this->so_left_hip_z_torque);
  this->si_right_hip_x_torque.ConnectTo(this->so_right_hip_x_torque);
  this->si_right_hip_y_torque.ConnectTo(this->so_right_hip_y_torque);
  this->si_right_hip_z_torque.ConnectTo(this->so_right_hip_z_torque);
  this->si_spine_x_torque.ConnectTo(this->so_spine_x_torque);
  this->si_spine_y_torque.ConnectTo(this->so_spine_y_torque);
  this->si_spine_z_torque.ConnectTo(this->so_spine_z_torque);


  fusion_walking_termi_left->co_walking_speed.ConnectTo(termination_group->ci_walking_speed);
  fusion_walking_termi_left->co_termination_phase_1.ConnectTo(termination_group->ci_left_last_step_phase_1);
  fusion_walking_termi_left->co_termination_phase_2.ConnectTo(termination_group->ci_left_last_step_phase_2);
  fusion_walking_termi_left->co_termination_phase_3.ConnectTo(termination_group->ci_left_last_step_phase_3);
  fusion_walking_termi_left->co_termination_phase_4.ConnectTo(termination_group->ci_left_last_step_phase_4);
  fusion_walking_termi_left->co_termination_phase_5.ConnectTo(termination_group->ci_left_last_step_phase_5);

  fusion_walking_termi_right->co_termination_phase_1.ConnectTo(termination_group->ci_right_last_step_phase_1);
  fusion_walking_termi_right->co_termination_phase_2.ConnectTo(termination_group->ci_right_last_step_phase_2);
  fusion_walking_termi_right->co_termination_phase_3.ConnectTo(termination_group->ci_right_last_step_phase_3);
  fusion_walking_termi_right->co_termination_phase_4.ConnectTo(termination_group->ci_right_last_step_phase_4);
  fusion_walking_termi_right->co_termination_phase_5.ConnectTo(termination_group->ci_right_last_step_phase_5);

  this->ci_activity_walking.ConnectTo(fusion_walking_termi_left->stimulation);
  this->ci_last_step_state.ConnectTo(fusion_walking_termi_left->ci_last_step_state);
  this->ci_walking_scale_factor.ConnectTo(fusion_walking_termi_left->ci_walking_scale);
  this->ci_left_stimulation_walking_phase_1.ConnectTo(fusion_walking_termi_left->ci_walking_phase_1);
  this->ci_left_stimulation_walking_phase_2.ConnectTo(fusion_walking_termi_left->ci_walking_phase_2);
  this->ci_left_stimulation_walking_phase_3.ConnectTo(fusion_walking_termi_left->ci_walking_phase_3);
  this->ci_left_stimulation_walking_phase_4.ConnectTo(fusion_walking_termi_left->ci_walking_phase_4);
  this->ci_left_stimulation_walking_phase_5.ConnectTo(fusion_walking_termi_left->ci_walking_phase_5);

  this->ci_activity_walking.ConnectTo(fusion_walking_termi_right->stimulation);
  this->ci_last_step_state.ConnectTo(fusion_walking_termi_right->ci_last_step_state);
  this->ci_right_stimulation_walking_phase_1.ConnectTo(fusion_walking_termi_right->ci_walking_phase_1);
  this->ci_right_stimulation_walking_phase_2.ConnectTo(fusion_walking_termi_right->ci_walking_phase_2);
  this->ci_right_stimulation_walking_phase_3.ConnectTo(fusion_walking_termi_right->ci_walking_phase_3);
  this->ci_right_stimulation_walking_phase_4.ConnectTo(fusion_walking_termi_right->ci_walking_phase_4);
  this->ci_right_stimulation_walking_phase_5.ConnectTo(fusion_walking_termi_right->ci_walking_phase_5);


  // spine x, y ,z

  // angles
  fusion_abs_angle_spine_x->activity.ConnectTo(fusion_angle_spine_x->InputActivity(0));
  fusion_abs_angle_spine_x->OutputPort(0).ConnectTo(fusion_angle_spine_x->InputPort(0, 0));
  fusion_abs_angle_spine_x->target_rating.ConnectTo(fusion_angle_spine_x->InputTargetRating(0));

  termination_group->co_stimulation_spine_x_angle.ConnectTo(fusion_angle_spine_x->InputActivity(1));
  termination_group->co_spine_x_angle.ConnectTo(fusion_angle_spine_x->InputPort(1, 0));
  termination_group->co_stimulation_spine_x_angle.ConnectTo(fusion_angle_spine_x->InputTargetRating(1));

  lateral_walking_group->co_stimulation_spine_x_angle.ConnectTo(fusion_angle_spine_x->InputActivity(2));
  lateral_walking_group->co_spine_x_angle.ConnectTo(fusion_angle_spine_x->InputPort(2, 0));
  lateral_walking_group->co_target_rating.ConnectTo(fusion_angle_spine_x->InputTargetRating(2));


  fusion_abs_angle_spine_y->activity.ConnectTo(fusion_angle_spine_y->InputActivity(0));
  fusion_abs_angle_spine_y->OutputPort(0).ConnectTo(fusion_angle_spine_y->InputPort(0, 0));
  fusion_abs_angle_spine_y->target_rating.ConnectTo(fusion_angle_spine_y->InputTargetRating(0));

  termination_group->co_stimulation_spine_y_angle.ConnectTo(fusion_angle_spine_y->InputActivity(1));
  termination_group->co_spine_y_angle.ConnectTo(fusion_angle_spine_y->InputPort(1, 0));
  termination_group->co_stimulation_spine_y_angle.ConnectTo(fusion_angle_spine_y->InputTargetRating(1));

  lateral_walking_group->co_stimulation_spine_y_angle.ConnectTo(fusion_angle_spine_y->InputActivity(2));
  lateral_walking_group->co_spine_y_angle.ConnectTo(fusion_angle_spine_y->InputPort(2, 0));
  lateral_walking_group->co_target_rating.ConnectTo(fusion_angle_spine_y->InputTargetRating(2));

  fusion_abs_angle_spine_z->activity.ConnectTo(fusion_angle_spine_z->InputActivity(0));
  fusion_abs_angle_spine_z->OutputPort(0).ConnectTo(fusion_angle_spine_z->InputPort(0, 0));
  fusion_abs_angle_spine_z->target_rating.ConnectTo(fusion_angle_spine_z->InputTargetRating(0));

  termination_group->co_stimulation_spine_z_angle.ConnectTo(fusion_angle_spine_z->InputActivity(1));
  termination_group->co_spine_z_angle.ConnectTo(fusion_angle_spine_z->InputPort(1, 0));
  termination_group->co_stimulation_spine_z_angle.ConnectTo(fusion_angle_spine_z->InputTargetRating(1));

  lateral_walking_group->co_stimulation_spine_z_angle.ConnectTo(fusion_angle_spine_z->InputActivity(2));
  lateral_walking_group->co_spine_z_angle.ConnectTo(fusion_angle_spine_z->InputPort(2, 0));
  lateral_walking_group->co_target_rating.ConnectTo(fusion_angle_spine_z->InputTargetRating(2));


  fusion_angle_spine_x->activity.ConnectTo(this->co_stimulation_spine_x_angle);
  fusion_angle_spine_x->OutputPort(0).ConnectTo(this->co_spine_x_angle);

  fusion_angle_spine_y->activity.ConnectTo(this->co_stimulation_spine_y_angle);
  fusion_angle_spine_y->OutputPort(0).ConnectTo(this->co_spine_y_angle);

  fusion_angle_spine_z->activity.ConnectTo(this->co_stimulation_spine_z_angle);
  fusion_angle_spine_z->OutputPort(0).ConnectTo(this->co_spine_z_angle);

  // torques
  fusion_abs_torque_spine_x->activity.ConnectTo(fusion_torque_spine_x->InputActivity(0));
  fusion_abs_torque_spine_x->OutputPort(0).ConnectTo(fusion_torque_spine_x->InputPort(0, 0));
  fusion_abs_torque_spine_x->target_rating.ConnectTo(fusion_torque_spine_x->InputTargetRating(0));

  termination_group->co_stimulation_spine_x_torque.ConnectTo(fusion_torque_spine_x->InputActivity(1));
  termination_group->co_spine_x_torque.ConnectTo(fusion_torque_spine_x->InputPort(1, 0));
  termination_group->co_stimulation_spine_x_torque.ConnectTo(fusion_torque_spine_x->InputTargetRating(1));

  lateral_walking_group->co_stimulation_spine_x_torque.ConnectTo(fusion_torque_spine_x->InputActivity(2));
  lateral_walking_group->co_spine_x_torque.ConnectTo(fusion_torque_spine_x->InputPort(2, 0));
  lateral_walking_group->co_target_rating.ConnectTo(fusion_torque_spine_x->InputTargetRating(2));

  fusion_abs_torque_spine_y->activity.ConnectTo(fusion_torque_spine_y->InputActivity(0));
  fusion_abs_torque_spine_y->OutputPort(0).ConnectTo(fusion_torque_spine_y->InputPort(0, 0));
  fusion_abs_torque_spine_y->target_rating.ConnectTo(fusion_torque_spine_y->InputTargetRating(0));


  termination_group->co_stimulation_spine_y_torque.ConnectTo(fusion_torque_spine_y->InputActivity(1));
  termination_group->co_spine_y_torque.ConnectTo(fusion_torque_spine_y->InputPort(1, 0));
  termination_group->co_stimulation_spine_y_torque.ConnectTo(fusion_torque_spine_y->InputTargetRating(1));


  lateral_walking_group->co_stimulation_spine_y_torque.ConnectTo(fusion_torque_spine_y->InputActivity(2));
  lateral_walking_group->co_spine_y_torque.ConnectTo(fusion_torque_spine_y->InputPort(2, 0));
  lateral_walking_group->co_target_rating.ConnectTo(fusion_torque_spine_y->InputTargetRating(2));


  fusion_abs_torque_spine_z->activity.ConnectTo(fusion_torque_spine_z->InputActivity(0));
  fusion_abs_torque_spine_z->OutputPort(0).ConnectTo(fusion_torque_spine_z->InputPort(0, 0));
  fusion_abs_torque_spine_z->target_rating.ConnectTo(fusion_torque_spine_z->InputTargetRating(0));


  termination_group->co_stimulation_spine_z_torque.ConnectTo(fusion_torque_spine_z->InputActivity(1));
  termination_group->co_spine_z_torque.ConnectTo(fusion_torque_spine_z->InputPort(1, 0));
  termination_group->co_stimulation_spine_z_torque.ConnectTo(fusion_torque_spine_z->InputTargetRating(1));


  lateral_walking_group->co_stimulation_spine_z_torque.ConnectTo(fusion_torque_spine_z->InputActivity(2));
  lateral_walking_group->co_spine_z_torque.ConnectTo(fusion_torque_spine_z->InputPort(2, 0));
  lateral_walking_group->co_target_rating.ConnectTo(fusion_torque_spine_z->InputTargetRating(2));


  fusion_torque_spine_x->activity.ConnectTo(this->co_stimulation_spine_x_torque);
  fusion_torque_spine_x->OutputPort(0).ConnectTo(this->co_spine_x_torque);

  fusion_torque_spine_y->activity.ConnectTo(this->co_stimulation_spine_y_torque);
  fusion_torque_spine_y->OutputPort(0).ConnectTo(this->co_spine_y_torque);

  fusion_torque_spine_z->activity.ConnectTo(this->co_stimulation_spine_z_torque);
  fusion_torque_spine_z->OutputPort(0).ConnectTo(this->co_spine_z_torque);


  // inhibit position by torque
  fusion_abs_torque_spine_x->activity.ConnectTo(memory_abs_torque_spine_x->in_activity);
  memory_abs_torque_spine_x->out_activity.ConnectTo(fusion_abs_angle_spine_x->inhibition[0]);
  fusion_abs_torque_spine_y->activity.ConnectTo(memory_abs_torque_spine_y->in_activity);
  memory_abs_torque_spine_y->out_activity.ConnectTo(fusion_abs_angle_spine_y->inhibition[0]);
  fusion_abs_torque_spine_z->activity.ConnectTo(memory_abs_torque_spine_z->in_activity);
  memory_abs_torque_spine_z->out_activity.ConnectTo(fusion_abs_angle_spine_z->inhibition[0]);

//  lock_hip_left->co_hip_y_torque.ConnectTo(trans_abs_left_lock_hip_torque_sense->in_activity);
//  trans_abs_left_lock_hip_torque_sense->out_activity.ConnectTo(this->so_left_lock_hip_generate);

//  lock_hip_right->co_hip_y_torque.ConnectTo(trans_abs_right_lock_hip_torque_sense->in_activity);
//  trans_abs_right_lock_hip_torque_sense->out_activity.ConnectTo(this->so_right_lock_hip_generate);

  lock_hip_left->co_hip_y_torque.ConnectTo(this->co_lock_hip_torque_left);

  lock_hip_right->co_hip_y_torque.ConnectTo(this->co_lock_hip_torque_right);


  // left hip
  // angles
  fusion_abs_angle_left_hip_x->activity.ConnectTo(fusion_angle_left_hip_x->InputActivity(0));
  fusion_abs_angle_left_hip_x->OutputPort(0).ConnectTo(fusion_angle_left_hip_x->InputPort(0, 0));
  fusion_abs_angle_left_hip_x->target_rating.ConnectTo(fusion_angle_left_hip_x->InputTargetRating(0));

  termination_group->co_left_stimulation_hip_x_angle.ConnectTo(fusion_angle_left_hip_x->InputActivity(1));
  termination_group->co_left_hip_x_angle.ConnectTo(fusion_angle_left_hip_x->InputPort(1, 0));
  termination_group->co_left_stimulation_hip_x_angle.ConnectTo(fusion_angle_left_hip_x->InputTargetRating(1));

  lateral_walking_group->co_left_stimulation_hip_x_angle.ConnectTo(fusion_angle_left_hip_x->InputActivity(2));
  lateral_walking_group->co_left_hip_x_angle.ConnectTo(fusion_angle_left_hip_x->InputPort(2, 0));
  lateral_walking_group->co_target_rating.ConnectTo(fusion_angle_left_hip_x->InputTargetRating(2));

  fusion_abs_angle_left_hip_y->activity.ConnectTo(fusion_angle_left_hip_y->InputActivity(0));
  fusion_abs_angle_left_hip_y->OutputPort(0).ConnectTo(fusion_angle_left_hip_y->InputPort(0, 0));
  fusion_abs_angle_left_hip_y->target_rating.ConnectTo(fusion_angle_left_hip_y->InputTargetRating(0));

  termination_group->co_left_stimulation_hip_y_angle.ConnectTo(fusion_angle_left_hip_y->InputActivity(1));
  termination_group->co_left_hip_y_angle.ConnectTo(fusion_angle_left_hip_y->InputPort(1, 0));
  termination_group->co_left_stimulation_hip_y_angle.ConnectTo(fusion_angle_left_hip_y->InputTargetRating(1));

  lateral_walking_group->co_left_stimulation_hip_y_angle.ConnectTo(fusion_angle_left_hip_y->InputActivity(2));
  lateral_walking_group->co_left_hip_y_angle.ConnectTo(fusion_angle_left_hip_y->InputPort(2, 0));
  lateral_walking_group->co_target_rating.ConnectTo(fusion_angle_left_hip_y->InputTargetRating(2));

  fusion_abs_angle_left_hip_z->activity.ConnectTo(fusion_angle_left_hip_z->InputActivity(0));
  fusion_abs_angle_left_hip_z->OutputPort(0).ConnectTo(fusion_angle_left_hip_z->InputPort(0, 0));
  fusion_abs_angle_left_hip_z->target_rating.ConnectTo(fusion_angle_left_hip_z->InputTargetRating(0));
  termination_group->co_left_stimulation_hip_z_angle.ConnectTo(fusion_angle_left_hip_z->InputActivity(1));
  termination_group->co_left_hip_z_angle.ConnectTo(fusion_angle_left_hip_z->InputPort(1, 0));
  termination_group->co_left_stimulation_hip_z_angle.ConnectTo(fusion_angle_left_hip_z->InputTargetRating(1));

  lateral_walking_group->co_left_stimulation_hip_z_angle.ConnectTo(fusion_angle_left_hip_z->InputActivity(2));
  lateral_walking_group->co_left_hip_z_angle.ConnectTo(fusion_angle_left_hip_z->InputPort(2, 0));
  lateral_walking_group->co_target_rating.ConnectTo(fusion_angle_left_hip_z->InputTargetRating(2));


  fusion_angle_left_hip_x->activity.ConnectTo(this->co_left_stimulation_hip_x_angle);
  fusion_angle_left_hip_x->OutputPort(0).ConnectTo(this->co_left_hip_x_angle);

  fusion_angle_left_hip_y->activity.ConnectTo(this->co_left_stimulation_hip_y_angle);
  fusion_angle_left_hip_y->OutputPort(0).ConnectTo(this->co_left_hip_y_angle);

  fusion_angle_left_hip_z->activity.ConnectTo(this->co_left_stimulation_hip_z_angle);
  fusion_angle_left_hip_z->OutputPort(0).ConnectTo(this->co_left_hip_z_angle);

  //torques
  fusion_abs_torque_left_hip_x->activity.ConnectTo(fusion_torque_left_hip_x->InputActivity(0));
  fusion_abs_torque_left_hip_x->OutputPort(0).ConnectTo(fusion_torque_left_hip_x->InputPort(0, 0));
  fusion_abs_torque_left_hip_x->target_rating.ConnectTo(fusion_torque_left_hip_x->InputTargetRating(0));

  termination_group->co_left_stimulation_hip_x_torque.ConnectTo(fusion_torque_left_hip_x->InputActivity(1));
  termination_group->co_left_hip_x_torque.ConnectTo(fusion_torque_left_hip_x->InputPort(1, 0));
  termination_group->co_left_stimulation_hip_x_torque.ConnectTo(fusion_torque_left_hip_x->InputTargetRating(1));


  lateral_walking_group->co_left_stimulation_hip_x_torque.ConnectTo(fusion_torque_left_hip_x->InputActivity(2));
  lateral_walking_group->co_left_hip_x_torque.ConnectTo(fusion_torque_left_hip_x->InputPort(2, 0));
  lateral_walking_group->co_target_rating.ConnectTo(fusion_torque_left_hip_x->InputTargetRating(2));


  fusion_abs_torque_left_hip_y->activity.ConnectTo(fusion_torque_left_hip_y->InputActivity(0));
  fusion_abs_torque_left_hip_y->OutputPort(0).ConnectTo(fusion_torque_left_hip_y->InputPort(0, 0));
  fusion_abs_torque_left_hip_y->target_rating.ConnectTo(fusion_torque_left_hip_y->InputTargetRating(0));

  termination_group->co_left_stimulation_hip_y_torque.ConnectTo(fusion_torque_left_hip_y->InputActivity(1));
  termination_group->co_left_hip_y_torque.ConnectTo(fusion_torque_left_hip_y->InputPort(1, 0));
  termination_group->co_left_stimulation_hip_y_torque.ConnectTo(fusion_torque_left_hip_y->InputTargetRating(1));


  lateral_walking_group->co_left_stimulation_hip_y_torque.ConnectTo(fusion_torque_left_hip_y->InputActivity(2));
  lateral_walking_group->co_left_hip_y_torque.ConnectTo(fusion_torque_left_hip_y->InputPort(2, 0));
  lateral_walking_group->co_left_stimulation_hip_y_torque.ConnectTo(fusion_torque_left_hip_y->InputTargetRating(2));


  fusion_abs_torque_left_hip_z->activity.ConnectTo(fusion_torque_left_hip_z->InputActivity(0));
  fusion_abs_torque_left_hip_z->OutputPort(0).ConnectTo(fusion_torque_left_hip_z->InputPort(0, 0));
  fusion_abs_torque_left_hip_z->target_rating.ConnectTo(fusion_torque_left_hip_z->InputTargetRating(0));


  termination_group->co_left_stimulation_hip_z_torque.ConnectTo(fusion_torque_left_hip_z->InputActivity(1));
  termination_group->co_left_hip_z_torque.ConnectTo(fusion_torque_left_hip_z->InputPort(1, 0));
  termination_group->co_left_stimulation_hip_z_torque.ConnectTo(fusion_torque_left_hip_z->InputTargetRating(1));


  lateral_walking_group->co_left_stimulation_hip_z_torque.ConnectTo(fusion_torque_left_hip_z->InputActivity(2));
  lateral_walking_group->co_left_hip_z_torque.ConnectTo(fusion_torque_left_hip_z->InputPort(2, 0));
  lateral_walking_group->co_target_rating.ConnectTo(fusion_torque_left_hip_z->InputTargetRating(2));

  fusion_torque_left_hip_x->activity.ConnectTo(this->co_left_stimulation_hip_x_torque);
  fusion_torque_left_hip_x->OutputPort(0).ConnectTo(this->co_left_hip_x_torque);

  fusion_torque_left_hip_y->activity.ConnectTo(this->co_left_stimulation_hip_y_torque);
  fusion_torque_left_hip_y->OutputPort(0).ConnectTo(this->co_left_hip_y_torque);

  fusion_torque_left_hip_z->activity.ConnectTo(this->co_left_stimulation_hip_z_torque);
  fusion_torque_left_hip_z->OutputPort(0).ConnectTo(this->co_left_hip_z_torque);

  // inhibit position by torque
  fusion_abs_torque_left_hip_x->activity.ConnectTo(memory_abs_torque_left_hip_x->in_activity);
  memory_abs_torque_left_hip_x->out_activity.ConnectTo(fusion_abs_angle_left_hip_x->inhibition[0]);
  fusion_abs_torque_left_hip_y->activity.ConnectTo(memory_abs_torque_left_hip_y->in_activity);
  memory_abs_torque_left_hip_y->out_activity.ConnectTo(fusion_abs_angle_left_hip_y->inhibition[0]);
  fusion_abs_torque_left_hip_z->activity.ConnectTo(memory_abs_torque_left_hip_z->in_activity);
  memory_abs_torque_left_hip_z->out_activity.ConnectTo(fusion_abs_angle_left_hip_z->inhibition[0]);

  //right hip
  // angles
  fusion_abs_angle_right_hip_x->activity.ConnectTo(fusion_angle_right_hip_x->InputActivity(0));
  fusion_abs_angle_right_hip_x->OutputPort(0).ConnectTo(fusion_angle_right_hip_x->InputPort(0, 0));
  fusion_abs_angle_right_hip_x->target_rating.ConnectTo(fusion_angle_right_hip_x->InputTargetRating(0));


  termination_group->co_right_stimulation_hip_x_angle.ConnectTo(fusion_angle_right_hip_x->InputActivity(1));
  termination_group->co_right_hip_x_angle.ConnectTo(fusion_angle_right_hip_x->InputPort(1, 0));
  termination_group->co_right_stimulation_hip_x_angle.ConnectTo(fusion_angle_right_hip_x->InputTargetRating(1));


  lateral_walking_group->co_right_stimulation_hip_x_angle.ConnectTo(fusion_angle_right_hip_x->InputActivity(2));
  lateral_walking_group->co_right_hip_x_angle.ConnectTo(fusion_angle_right_hip_x->InputPort(2, 0));
  lateral_walking_group->co_target_rating.ConnectTo(fusion_angle_right_hip_x->InputTargetRating(2));


  fusion_abs_angle_right_hip_y->activity.ConnectTo(fusion_angle_right_hip_y->InputActivity(0));
  fusion_abs_angle_right_hip_y->OutputPort(0).ConnectTo(fusion_angle_right_hip_y->InputPort(0, 0));
  fusion_abs_angle_right_hip_y->target_rating.ConnectTo(fusion_angle_right_hip_y->InputTargetRating(0));

  termination_group->co_right_stimulation_hip_y_angle.ConnectTo(fusion_angle_right_hip_y->InputActivity(1));
  termination_group->co_right_hip_y_angle.ConnectTo(fusion_angle_right_hip_y->InputPort(1, 0));
  termination_group->co_right_stimulation_hip_y_angle.ConnectTo(fusion_angle_right_hip_y->InputTargetRating(1));


  lateral_walking_group->co_right_stimulation_hip_y_angle.ConnectTo(fusion_angle_right_hip_y->InputActivity(2));
  lateral_walking_group->co_right_hip_y_angle.ConnectTo(fusion_angle_right_hip_y->InputPort(2, 0));
  lateral_walking_group->co_target_rating.ConnectTo(fusion_angle_right_hip_y->InputTargetRating(2));

  fusion_abs_angle_right_hip_z->activity.ConnectTo(fusion_angle_right_hip_z->InputActivity(0));
  fusion_abs_angle_right_hip_z->OutputPort(0).ConnectTo(fusion_angle_right_hip_z->InputPort(0, 0));
  fusion_abs_angle_right_hip_z->target_rating.ConnectTo(fusion_angle_right_hip_z->InputTargetRating(0));

  termination_group->co_right_stimulation_hip_z_angle.ConnectTo(fusion_angle_right_hip_z->InputActivity(1));
  termination_group->co_right_hip_z_angle.ConnectTo(fusion_angle_right_hip_z->InputPort(1, 0));
  termination_group->co_right_stimulation_hip_z_angle.ConnectTo(fusion_angle_right_hip_z->InputTargetRating(1));


  lateral_walking_group->co_right_stimulation_hip_z_angle.ConnectTo(fusion_angle_right_hip_z->InputActivity(2));
  lateral_walking_group->co_right_hip_z_angle.ConnectTo(fusion_angle_right_hip_z->InputPort(2, 0));
  lateral_walking_group->co_target_rating.ConnectTo(fusion_angle_right_hip_z->InputTargetRating(2));


  fusion_angle_right_hip_x->activity.ConnectTo(this->co_right_stimulation_hip_x_angle);
  fusion_angle_right_hip_x->OutputPort(0).ConnectTo(this->co_right_hip_x_angle);

  fusion_angle_right_hip_y->activity.ConnectTo(this->co_right_stimulation_hip_y_angle);
  fusion_angle_right_hip_y->OutputPort(0).ConnectTo(this->co_right_hip_y_angle);

  fusion_angle_right_hip_z->activity.ConnectTo(this->co_right_stimulation_hip_z_angle);
  fusion_angle_right_hip_z->OutputPort(0).ConnectTo(this->co_right_hip_z_angle);

  //torques
  fusion_abs_torque_right_hip_x->activity.ConnectTo(fusion_torque_right_hip_x->InputActivity(0));
  fusion_abs_torque_right_hip_x->OutputPort(0).ConnectTo(fusion_torque_right_hip_x->InputPort(0, 0));
  fusion_abs_torque_right_hip_x->target_rating.ConnectTo(fusion_torque_right_hip_x->InputTargetRating(0));


  termination_group->co_right_stimulation_hip_x_torque.ConnectTo(fusion_torque_right_hip_x->InputActivity(1));
  termination_group->co_right_hip_x_torque.ConnectTo(fusion_torque_right_hip_x->InputPort(1, 0));
  termination_group->co_right_stimulation_hip_x_torque.ConnectTo(fusion_torque_right_hip_x->InputTargetRating(1));


  lateral_walking_group->co_right_stimulation_hip_x_torque.ConnectTo(fusion_torque_right_hip_x->InputActivity(2));
  lateral_walking_group->co_right_hip_x_torque.ConnectTo(fusion_torque_right_hip_x->InputPort(2, 0));
  lateral_walking_group->co_target_rating.ConnectTo(fusion_torque_right_hip_x->InputTargetRating(2));


  fusion_abs_torque_right_hip_y->activity.ConnectTo(fusion_torque_right_hip_y->InputActivity(0));
  fusion_abs_torque_right_hip_y->OutputPort(0).ConnectTo(fusion_torque_right_hip_y->InputPort(0, 0));
  fusion_abs_torque_right_hip_y->target_rating.ConnectTo(fusion_torque_right_hip_y->InputTargetRating(0));


  termination_group->co_right_stimulation_hip_y_torque.ConnectTo(fusion_torque_right_hip_y->InputActivity(1));
  termination_group->co_right_hip_y_torque.ConnectTo(fusion_torque_right_hip_y->InputPort(1, 0));
  termination_group->co_right_stimulation_hip_y_torque.ConnectTo(fusion_torque_right_hip_y->InputTargetRating(1));


  lateral_walking_group->co_right_stimulation_hip_y_torque.ConnectTo(fusion_torque_right_hip_y->InputActivity(2));
  lateral_walking_group->co_right_hip_y_torque.ConnectTo(fusion_torque_right_hip_y->InputPort(2, 0));
  lateral_walking_group->co_right_stimulation_hip_y_torque.ConnectTo(fusion_torque_right_hip_y->InputTargetRating(2));


  fusion_abs_torque_right_hip_z->activity.ConnectTo(fusion_torque_right_hip_z->InputActivity(0));
  fusion_abs_torque_right_hip_z->OutputPort(0).ConnectTo(fusion_torque_right_hip_z->InputPort(0, 0));
  fusion_abs_torque_right_hip_z->target_rating.ConnectTo(fusion_torque_right_hip_z->InputTargetRating(0));


  termination_group->co_right_stimulation_hip_z_torque.ConnectTo(fusion_torque_right_hip_z->InputActivity(1));
  termination_group->co_right_hip_z_torque.ConnectTo(fusion_torque_right_hip_z->InputPort(1, 0));
  termination_group->co_right_stimulation_hip_z_torque.ConnectTo(fusion_torque_right_hip_z->InputTargetRating(1));


  lateral_walking_group->co_right_stimulation_hip_z_torque.ConnectTo(fusion_torque_right_hip_z->InputActivity(2));
  lateral_walking_group->co_right_hip_z_torque.ConnectTo(fusion_torque_right_hip_z->InputPort(2, 0));
  lateral_walking_group->co_target_rating.ConnectTo(fusion_torque_right_hip_z->InputTargetRating(2));


  fusion_torque_right_hip_x->activity.ConnectTo(this->co_right_stimulation_hip_x_torque);
  fusion_torque_right_hip_x->OutputPort(0).ConnectTo(this->co_right_hip_x_torque);

  fusion_torque_right_hip_y->activity.ConnectTo(this->co_right_stimulation_hip_y_torque);
  fusion_torque_right_hip_y->OutputPort(0).ConnectTo(this->co_right_hip_y_torque);

  fusion_torque_right_hip_z->activity.ConnectTo(this->co_right_stimulation_hip_z_torque);
  fusion_torque_right_hip_z->OutputPort(0).ConnectTo(this->co_right_hip_z_torque);

  // inhibit position by torque
  fusion_abs_torque_right_hip_x->activity.ConnectTo(memory_abs_torque_right_hip_x->in_activity);
  memory_abs_torque_right_hip_x->out_activity.ConnectTo(fusion_abs_angle_right_hip_x->inhibition[0]);
  fusion_abs_torque_right_hip_y->activity.ConnectTo(memory_abs_torque_right_hip_y->in_activity);
  memory_abs_torque_right_hip_y->out_activity.ConnectTo(fusion_abs_angle_right_hip_y->inhibition[0]);
  fusion_abs_torque_right_hip_z->activity.ConnectTo(memory_abs_torque_right_hip_z->in_activity);
  memory_abs_torque_right_hip_z->out_activity.ConnectTo(fusion_abs_angle_right_hip_z->inhibition[0]);
  // lateral hip shift
  fusion_lateral_hip_shift_left->activity.ConnectTo(lateral_hip_shift_left->stimulation);
  fusion_lateral_hip_shift_left->OutputPort(0).ConnectTo(lateral_hip_shift_left->ci_scale);

  fusion_lateral_hip_shift_right->activity.ConnectTo(lateral_hip_shift_right->stimulation);
  fusion_lateral_hip_shift_right->OutputPort(0).ConnectTo(lateral_hip_shift_right->ci_scale);

  // walking initialization
  //fusion_lateral_hip_shift_right->OutputPort(0).ConnectTo(lateral_hip_shift_right_walking_init->ci_scale);

  fusion_lateral_hip_shift_right->activity.ConnectTo(lateral_hip_shift_right_push_recovery->stimulation);
  fusion_lateral_hip_shift_right->OutputPort(0).ConnectTo(lateral_hip_shift_right_push_recovery->ci_scale);

  // active hip swing
  fusion_active_hip_swing_left->activity.ConnectTo(active_hip_swing_left->stimulation);
  fusion_active_hip_swing_right->activity.ConnectTo(active_hip_swing_right->stimulation);

  fusion_active_hip_swing_left->activity.ConnectTo(active_hip_swing_left_sin->stimulation);
  fusion_active_hip_swing_right->activity.ConnectTo(active_hip_swing_right_sin->stimulation);

  // hip swing bent knee
  fusion_active_hip_swing_left->activity.ConnectTo(hip_swing_bent_knee_left->stimulation);
  fusion_active_hip_swing_right->activity.ConnectTo(hip_swing_bent_knee_right->stimulation);
  fusion_active_hip_swing_left->activity.ConnectTo(mp_turn_leg_left_z->stimulation);
  fusion_active_hip_swing_right->activity.ConnectTo(mp_turn_leg_right_z->stimulation);

  // turn motor patterns
  fusion_mp_turn_left_left->OutputPort(0).ConnectTo(mp_turn_left_left->stimulation);
  fusion_mp_turn_right_left->OutputPort(0).ConnectTo(mp_turn_right_left->stimulation);
  fusion_mp_turn_left_right->OutputPort(0).ConnectTo(mp_turn_left_right->stimulation);
  fusion_mp_turn_right_right->OutputPort(0).ConnectTo(mp_turn_right_right->stimulation);

  fusion_mp_turn_left_spine->OutputPort(0).ConnectTo(mp_turn_spine_left->stimulation);
  fusion_mp_turn_right_spine->OutputPort(0).ConnectTo(mp_turn_spine_right->stimulation);

  this->ci_turn_left_signal_to_left.ConnectTo(mp_turn_leg_left_z->ci_scale);
  this->ci_turn_right_signal_to_right.ConnectTo(mp_turn_leg_right_z->ci_scale);

  // lock hip
  fusion_lock_hip_left->activity.ConnectTo(lock_hip_left->stimulation);
  fusion_lock_hip_right->activity.ConnectTo(lock_hip_right->stimulation);

  // balance body pitch
  fusion_upright_trunk_left->activity.ConnectTo(upright_trunk_left->stimulation);

  this->ci_last_step_state.ConnectTo(upright_trunk_left->ci_last_step_termination);
  this->ci_last_step.ConnectTo(upright_trunk_left->ci_last_step);
  this->ci_maintain_stable.ConnectTo(upright_trunk_left->ci_maintain_stable);
  this->ci_push_recovery_stand.ConnectTo(upright_trunk_left->ci_push_recovery_stand);
  this->ci_reset_simulation_experiments.ConnectTo(upright_trunk_left->ci_reset_simulation_experiments);
  this->ci_walking_scale_factor.ConnectTo(upright_trunk_left->ci_walking_scale);
  this->ci_termination_hold_position.ConnectTo(upright_trunk_left->ci_termination_hold_position);
  this->ci_termination_turn_off_upright.ConnectTo(upright_trunk_left->ci_termination_turn_off_upright);

  fusion_upright_trunk_right->activity.ConnectTo(upright_trunk_right->stimulation);
  this->ci_last_step_state.ConnectTo(upright_trunk_right->ci_last_step_termination);
  this->ci_last_step.ConnectTo(upright_trunk_right->ci_last_step);
  this->ci_maintain_stable.ConnectTo(upright_trunk_right->ci_maintain_stable);
  this->ci_push_recovery_stand.ConnectTo(upright_trunk_right->ci_push_recovery_stand);
  this->ci_reset_simulation_experiments.ConnectTo(upright_trunk_right->ci_reset_simulation_experiments);
  this->ci_walking_scale_factor.ConnectTo(upright_trunk_right->ci_walking_scale);
  this->ci_termination_hold_position.ConnectTo(upright_trunk_right->ci_termination_hold_position);
  this->ci_termination_turn_off_upright.ConnectTo(upright_trunk_right->ci_termination_turn_off_upright);

  // lateral foot placement
  fusion_lateral_foot_placement_left->activity.ConnectTo(lateral_foot_placement_left->stimulation);
  fusion_lateral_foot_placement_right->activity.ConnectTo(lateral_foot_placement_right->stimulation);

  // control pelvis roll
  fusion_control_pelvis_roll_left->activity.ConnectTo(control_pelvis_roll_left->stimulation);
  fusion_control_pelvis_roll_right->activity.ConnectTo(control_pelvis_roll_right->stimulation);

  // control pelvis yaw
  fusion_control_pelvis_yaw_left->activity.ConnectTo(control_pelvis_yaw_left->stimulation);
  fusion_control_pelvis_yaw_right->activity.ConnectTo(control_pelvis_yaw_right->stimulation);



  //END fusion behaviours

  //BEGIN reflexes
  // ---------------------------------------------------------------------------------------------
  // balance body pitch
  // left
//  for (size_t i = 0; i < 3; i++)
//  {
//    ci_actual_roll_accel.emplace_back("Actual Accel" + std::to_string(i), this);
//    this->ci_actual_rot.emplace_back("Actual Rot" + std::to_string(i), this);
//  }
  this->ci_actual_pitch.ConnectTo(upright_trunk_left->ci_body_pitch);
  this->ci_left_ground_contact.ConnectTo(upright_trunk_left->ci_ground_contact);
  this->ci_stop_upright_trunk.ConnectTo(upright_trunk_left->ci_stop_upright_trunk);

  upright_trunk_left->activity.ConnectTo(fusion_abs_torque_left_hip_y->InputActivity(0));
  upright_trunk_left->activity.ConnectTo(fusion_abs_torque_left_hip_y->InputTargetRating(0));
  upright_trunk_left->co_torque_hip_y.ConnectTo(fusion_abs_torque_left_hip_y->InputPort(0, 0));

  // right
  this->ci_actual_pitch.ConnectTo(upright_trunk_right->ci_body_pitch);
  this->ci_right_ground_contact.ConnectTo(upright_trunk_right->ci_ground_contact);
  this->ci_stop_upright_trunk.ConnectTo(upright_trunk_right->ci_stop_upright_trunk);

  upright_trunk_right->activity.ConnectTo(fusion_abs_torque_right_hip_y->InputActivity(0));
  upright_trunk_right->activity.ConnectTo(fusion_abs_torque_right_hip_y->InputTargetRating(0));
  upright_trunk_right->co_torque_hip_y.ConnectTo(fusion_abs_torque_right_hip_y->InputPort(0, 0));

  // adjust body pitch
  this->ci_activity_walking.ConnectTo(adjust_body_pitch->stimulation);
  this->ci_walking_scale_factor.ConnectTo(adjust_body_pitch->ci_scale);
  this->ci_actual_pitch.ConnectTo(adjust_body_pitch->ci_pitch);
  this->ci_forward_velocity_control.ConnectTo(adjust_body_pitch->ci_forward_velocity_control);

  adjust_body_pitch->co_body_pitch.ConnectTo(upright_trunk_left->ci_target_pitch);
  adjust_body_pitch->co_body_pitch.ConnectTo(upright_trunk_right->ci_target_pitch);

  // balance spine x
  this->si_spine_x_angle.ConnectTo(balance_spine_x->si_angle);
  this->ci_actual_roll.ConnectTo(balance_spine_x->ci_tilt);
  this->ci_reset_simulation_experiments.ConnectTo(balance_spine_x->ci_reset_simulation_experiments);

  balance_spine_x->activity.ConnectTo(fusion_abs_torque_spine_x->InputActivity(0));
  balance_spine_x->target_rating.ConnectTo(fusion_abs_torque_spine_x->InputTargetRating(0));
  balance_spine_x->co_torque.ConnectTo(fusion_abs_torque_spine_x->InputPort(0, 0));

  // lock hip
  // left
  this->ci_walking_scale_factor.ConnectTo(lock_hip_left->ci_step_length);
  this->ci_adjusted_pitch.ConnectTo(lock_hip_left->ci_body_pitch);
  this->ci_right_xcom_x.ConnectTo(lock_hip_left->ci_xcom_x_opposite_leg);
  this->ci_termi_lock_hip.ConnectTo(lock_hip_left->ci_termi_lock_hip);
  this->ci_push_recovery_walking.ConnectTo(lock_hip_left->ci_push_recovery_walking);
  this->ci_obstacle_walking_stimulation.ConnectTo(lock_hip_left->ci_obstacle_walking_stimulation);
  this->ci_obstacle_step.ConnectTo(lock_hip_left->ci_obstacle_step);
  this->ci_obstacle_lock_hip_1.ConnectTo(lock_hip_left->ci_obstacle_lock_hip_1);
  this->ci_obstacle_lock_hip_2.ConnectTo(lock_hip_left->ci_obstacle_lock_hip_2);
  this->ci_obstacle_lock_hip_3.ConnectTo(lock_hip_left->ci_obstacle_lock_hip_3);


  // PSO SLOW

  // TODO send stimulation to LH and slow walking on to others
  this->ci_slow_walking_on.ConnectTo(active_hip_swing_left_sin->ci_slow_walking_stimulation);
  this->ci_slow_walking_on.ConnectTo(active_hip_swing_right_sin->ci_slow_walking_stimulation);

  this->ci_fast_walking_on.ConnectTo(active_hip_swing_left_sin->ci_fast_walking_stimulation);
  this->ci_fast_walking_on.ConnectTo(active_hip_swing_right_sin->ci_fast_walking_stimulation);

  this->ci_slow_walking_max_torque.ConnectTo(active_hip_swing_left_sin->ci_max_torque);
  this->ci_slow_walking_max_torque.ConnectTo(active_hip_swing_right_sin->ci_max_torque);
  this->ci_slow_walking_ahs_t1.ConnectTo(active_hip_swing_left_sin->ci_slow_walking_ahs_t1);
  this->ci_slow_walking_ahs_t2.ConnectTo(active_hip_swing_left_sin->ci_slow_walking_ahs_t2);
  this->ci_slow_walking_ahs_t1.ConnectTo(active_hip_swing_right_sin->ci_slow_walking_ahs_t1);
  this->ci_slow_walking_ahs_t2.ConnectTo(active_hip_swing_right_sin->ci_slow_walking_ahs_t2);

  this->ci_slow_walking_stimulation.ConnectTo(lock_hip_left->ci_slow_walking);
  this->ci_slow_walking_lh_par1.ConnectTo(lock_hip_left->ci_slow_walking_par1);
  this->ci_slow_walking_lh_par2.ConnectTo(lock_hip_left->ci_slow_walking_par2);

  this->ci_slow_walking_stimulation.ConnectTo(lock_hip_right->ci_slow_walking);
  this->ci_slow_walking_lh_par1.ConnectTo(lock_hip_right->ci_slow_walking_par1);
  this->ci_slow_walking_lh_par2.ConnectTo(lock_hip_right->ci_slow_walking_par2);

	this->ci_fast_walking_stimulation.ConnectTo(lock_hip_left->ci_fast_walking);
	this->ci_fast_walking_lh_par1.ConnectTo(lock_hip_left->ci_fast_walking_par1);
	this->ci_fast_walking_lh_par2.ConnectTo(lock_hip_left->ci_fast_walking_par2);

	this->ci_fast_walking_stimulation.ConnectTo(lock_hip_right->ci_fast_walking);
	this->ci_fast_walking_lh_par1.ConnectTo(lock_hip_right->ci_fast_walking_par1);
	this->ci_fast_walking_lh_par2.ConnectTo(lock_hip_right->ci_fast_walking_par2);

//  active_hip_swing_left->co_initiation_phase.ConnectTo(lock_hip_left->ci_initiation_activity);
  active_hip_swing_left_sin->co_initiation_phase.ConnectTo(lock_hip_left->ci_initiation_activity);

  this->si_left_hip_y_angle.ConnectTo(lock_hip_left->si_hip_y_angle);

  lock_hip_left->co_stimulation_angle.ConnectTo(fusion_abs_angle_left_hip_y->InputActivity(0));
  lock_hip_left->target_rating.ConnectTo(fusion_abs_angle_left_hip_y->InputTargetRating(0));
  lock_hip_left->co_hip_y_angle.ConnectTo(fusion_abs_angle_left_hip_y->InputPort(0, 0));

  lock_hip_left->co_stimulation_torque.ConnectTo(fusion_abs_torque_left_hip_y->InputActivity(1));
  lock_hip_left->co_stimulation_torque.ConnectTo(fusion_abs_torque_left_hip_y->InputTargetRating(1));
  lock_hip_left->co_hip_y_torque.ConnectTo(fusion_abs_torque_left_hip_y->InputPort(1, 0));

  lock_hip_left->co_last_activity.ConnectTo(active_hip_swing_left->inhibition[0]);//todo
  lock_hip_left->co_last_activity.ConnectTo(active_hip_swing_left_sin->inhibition[0]);//todo
//  lock_hip_left->co_last_activity.ConnectTo(hip_swing_curve_walking_left->inhibition[0]);//todo


  lock_hip_left->co_last_activity.ConnectTo(hip_swing_bent_knee_left->inhibition[0]);

  //lock_hip_left->so_hip_velocity.ConnectTo(this->so_left_lock_hip_generate);
  // right
  this->ci_walking_scale_factor.ConnectTo(lock_hip_right->ci_step_length);
  this->ci_adjusted_pitch.ConnectTo(lock_hip_right->ci_body_pitch);
  this->ci_right_xcom_x.ConnectTo(lock_hip_right->ci_xcom_x_opposite_leg);
  this->ci_termi_lock_hip.ConnectTo(lock_hip_right->ci_termi_lock_hip);
  this->ci_push_recovery_stand.ConnectTo(lock_hip_right->ci_push_recovery_stand);
  this->ci_push_recovery_walking.ConnectTo(lock_hip_right->ci_push_recovery_walking);
  this->ci_left_com_x.ConnectTo(lock_hip_right->ci_left_com_x);
  this->ci_right_com_x.ConnectTo(lock_hip_right->ci_right_com_x);

  this->ci_obstacle_walking_stimulation.ConnectTo(lock_hip_right->ci_obstacle_walking_stimulation);
  this->ci_obstacle_step.ConnectTo(lock_hip_right->ci_obstacle_step);
  this->ci_obstacle_lock_hip_1.ConnectTo(lock_hip_right->ci_obstacle_lock_hip_1);
  this->ci_obstacle_lock_hip_2.ConnectTo(lock_hip_right->ci_obstacle_lock_hip_2);
  this->ci_obstacle_lock_hip_3.ConnectTo(lock_hip_right->ci_obstacle_lock_hip_3);


  this->si_right_hip_y_angle.ConnectTo(lock_hip_right->si_hip_y_angle);

  lock_hip_right->co_stimulation_angle.ConnectTo(fusion_abs_angle_right_hip_y->InputActivity(0));
  lock_hip_right->target_rating.ConnectTo(fusion_abs_angle_right_hip_y->InputTargetRating(0));
  lock_hip_right->co_hip_y_angle.ConnectTo(fusion_abs_angle_right_hip_y->InputPort(0, 0));

  lock_hip_right->co_stimulation_torque.ConnectTo(fusion_abs_torque_right_hip_y->InputActivity(1));
  lock_hip_right->co_stimulation_torque.ConnectTo(fusion_abs_torque_right_hip_y->InputTargetRating(1));
  lock_hip_right->co_hip_y_torque.ConnectTo(fusion_abs_torque_right_hip_y->InputPort(1, 0));

  lock_hip_right->co_last_activity.ConnectTo(active_hip_swing_right->inhibition[0]);
  lock_hip_right->co_last_activity.ConnectTo(active_hip_swing_right_sin->inhibition[0]);

  lock_hip_right->co_last_activity.ConnectTo(hip_swing_bent_knee_right->inhibition[0]);
  //lock_hip_right->co_last_activity.ConnectTo(hip_swing_curve_walking_right->inhibition[0]);//todo


  //lock_hip_right->so_hip_velocity.ConnectTo(this->so_right_lock_hip_generate);

  // hold position group
  skill_stand->activity.ConnectTo(hold_position_group->stimulation);
  skill_walking_init_left->activity.ConnectTo(hold_position_group->ci_walking_init_stimulaton);

  this->ci_stiffness_factor.ConnectTo(hold_position_group->ci_stiffness_factor);
  this->ci_ground_contact.ConnectTo(hold_position_group->ci_ground_contact);
  this->ci_reset_simulation_experiments.ConnectTo(hold_position_group->ci_reset_simulation_experiments);
  this->ci_stimulation_termination_button.ConnectTo(hold_position_group->ci_stimulation_termination_button);

  fusion_rel_angle_left_hip_x->activity.ConnectTo(hold_position_group->ci_left_stimulation_angle_adjustment_hip_x);
  fusion_rel_angle_left_hip_x->OutputPort(0).ConnectTo(hold_position_group->ci_left_angle_adjustment_hip_x);

  fusion_rel_angle_left_hip_y->activity.ConnectTo(hold_position_group->ci_left_stimulation_angle_adjustment_hip_y);
  fusion_rel_angle_left_hip_y->OutputPort(0).ConnectTo(hold_position_group->ci_left_angle_adjustment_hip_y);

  fusion_rel_angle_left_hip_z->activity.ConnectTo(hold_position_group->ci_left_stimulation_angle_adjustment_hip_z);
  fusion_rel_angle_left_hip_z->OutputPort(0).ConnectTo(hold_position_group->ci_left_angle_adjustment_hip_z);

  fusion_rel_angle_right_hip_x->activity.ConnectTo(hold_position_group->ci_right_stimulation_angle_adjustment_hip_x);
  fusion_rel_angle_right_hip_x->OutputPort(0).ConnectTo(hold_position_group->ci_right_angle_adjustment_hip_x);

  fusion_rel_angle_right_hip_y->activity.ConnectTo(hold_position_group->ci_right_stimulation_angle_adjustment_hip_y);
  fusion_rel_angle_right_hip_y->OutputPort(0).ConnectTo(hold_position_group->ci_right_angle_adjustment_hip_y);

  fusion_rel_angle_right_hip_z->activity.ConnectTo(hold_position_group->ci_right_stimulation_angle_adjustment_hip_z);
  fusion_rel_angle_right_hip_z->OutputPort(0).ConnectTo(hold_position_group->ci_right_angle_adjustment_hip_z);

  fusion_rel_angle_spine_x->activity.ConnectTo(hold_position_group->ci_stimulation_angle_adjustment_spine_x);
  fusion_rel_angle_spine_x->OutputPort(0).ConnectTo(hold_position_group->ci_angle_adjustment_spine_x);

  fusion_rel_angle_spine_y->activity.ConnectTo(hold_position_group->ci_stimulation_angle_adjustment_spine_y);
  fusion_rel_angle_spine_y->OutputPort(0).ConnectTo(hold_position_group->ci_angle_adjustment_spine_y);

//  fusion_rel_angle_spine_z->activity.ConnectTo(hold_position_group->ci_stimulation_angle_adjustment_spine_z);
//  fusion_rel_angle_spine_z->OutputPort(0).ConnectTo(hold_position_group->ci_angle_adjustment_spine_z);

  this->si_left_hip_x_angle.ConnectTo(hold_position_group->si_left_hip_x_angle);
  this->si_left_hip_y_angle.ConnectTo(hold_position_group->si_left_hip_y_angle);
  this->si_left_hip_z_angle.ConnectTo(hold_position_group->si_left_hip_z_angle);
  this->si_right_hip_x_angle.ConnectTo(hold_position_group->si_right_hip_x_angle);
  this->si_right_hip_y_angle.ConnectTo(hold_position_group->si_right_hip_y_angle);
  this->si_right_hip_z_angle.ConnectTo(hold_position_group->si_right_hip_z_angle);
  this->si_spine_x_angle.ConnectTo(hold_position_group->si_spine_x_angle);
  this->si_spine_y_angle.ConnectTo(hold_position_group->si_spine_y_angle);
  this->si_spine_z_angle.ConnectTo(hold_position_group->si_spine_z_angle);

  hold_position_group->co_left_activity_hip_x.ConnectTo(fusion_abs_angle_left_hip_x->InputActivity(0));
  hold_position_group->co_left_target_rating_hip_x.ConnectTo(fusion_abs_angle_left_hip_x->InputTargetRating(0));
  hold_position_group->co_left_angle_hip_x.ConnectTo(fusion_abs_angle_left_hip_x->InputPort(0, 0));

  hold_position_group->co_left_activity_hip_y.ConnectTo(fusion_abs_angle_left_hip_y->InputActivity(1));
  hold_position_group->co_left_target_rating_hip_y.ConnectTo(fusion_abs_angle_left_hip_y->InputTargetRating(1));
  hold_position_group->co_left_angle_hip_y.ConnectTo(fusion_abs_angle_left_hip_y->InputPort(1, 0));

  hold_position_group->co_left_activity_hip_z.ConnectTo(fusion_abs_angle_left_hip_z->InputActivity(0));
  hold_position_group->co_left_target_rating_hip_z.ConnectTo(fusion_abs_angle_left_hip_z->InputTargetRating(0));
  hold_position_group->co_left_angle_hip_z.ConnectTo(fusion_abs_angle_left_hip_z->InputPort(0, 0));

  hold_position_group->co_right_activity_hip_x.ConnectTo(fusion_abs_angle_right_hip_x->InputActivity(0));
  hold_position_group->co_right_target_rating_hip_x.ConnectTo(fusion_abs_angle_right_hip_x->InputTargetRating(0));
  hold_position_group->co_right_angle_hip_x.ConnectTo(fusion_abs_angle_right_hip_x->InputPort(0, 0));

  hold_position_group->co_right_activity_hip_y.ConnectTo(fusion_abs_angle_right_hip_y->InputActivity(1));
  hold_position_group->co_right_target_rating_hip_y.ConnectTo(fusion_abs_angle_right_hip_y->InputTargetRating(1));
  hold_position_group->co_right_angle_hip_y.ConnectTo(fusion_abs_angle_right_hip_y->InputPort(1, 0));

  hold_position_group->co_right_activity_hip_z.ConnectTo(fusion_abs_angle_right_hip_z->InputActivity(0));
  hold_position_group->co_right_target_rating_hip_z.ConnectTo(fusion_abs_angle_right_hip_z->InputTargetRating(0));
  hold_position_group->co_right_angle_hip_z.ConnectTo(fusion_abs_angle_right_hip_z->InputPort(0, 0));

  hold_position_group->co_activity_spine_x.ConnectTo(fusion_abs_angle_spine_x->InputActivity(0));
  hold_position_group->co_target_rating_spine_x.ConnectTo(fusion_abs_angle_spine_x->InputTargetRating(0));
  hold_position_group->co_angle_spine_x.ConnectTo(fusion_abs_angle_spine_x->InputPort(0, 0));

  hold_position_group->co_activity_spine_y.ConnectTo(fusion_abs_angle_spine_y->InputActivity(0));
  hold_position_group->co_target_rating_spine_y.ConnectTo(fusion_abs_angle_spine_y->InputTargetRating(0));
  hold_position_group->co_angle_spine_y.ConnectTo(fusion_abs_angle_spine_y->InputPort(0, 0));

  hold_position_group->co_activity_spine_z.ConnectTo(fusion_abs_angle_spine_z->InputActivity(0));
  hold_position_group->co_target_rating_spine_z.ConnectTo(fusion_abs_angle_spine_z->InputTargetRating(0));
  hold_position_group->co_angle_spine_z.ConnectTo(fusion_abs_angle_spine_z->InputPort(0, 0));

  // lateral walking group
  this->GetControllerInputs().ConnectByName(lateral_walking_group->GetControllerInputs(), true);
  this->GetSensorInputs().ConnectByName(lateral_walking_group->GetSensorInputs(), true);

  // hold joint position for walking hip x
  // left
  this->si_left_hip_x_angle.ConnectTo(hold_joint_pos_hip_x_left->si_angle);
  this->ci_left_ground_contact.ConnectTo(hold_joint_pos_hip_x_left->ci_ground_contact);

  hold_joint_pos_hip_x_left->activity.ConnectTo(fusion_abs_angle_left_hip_x->InputActivity(1));
  hold_joint_pos_hip_x_left->target_rating.ConnectTo(fusion_abs_angle_left_hip_x->InputTargetRating(1));
  hold_joint_pos_hip_x_left->co_angle.ConnectTo(fusion_abs_angle_left_hip_x->InputPort(1, 0));

  // right
  this->si_right_hip_x_angle.ConnectTo(hold_joint_pos_hip_x_right->si_angle);
  this->ci_right_ground_contact.ConnectTo(hold_joint_pos_hip_x_right->ci_ground_contact);

  hold_joint_pos_hip_x_right->activity.ConnectTo(fusion_abs_angle_right_hip_x->InputActivity(1));
  hold_joint_pos_hip_x_right->target_rating.ConnectTo(fusion_abs_angle_right_hip_x->InputTargetRating(1));
  hold_joint_pos_hip_x_right->co_angle.ConnectTo(fusion_abs_angle_right_hip_x->InputPort(1, 0));

  // relax spine x
  this->ci_stimulation_relax_spine_x.ConnectTo(relax_spine_x->stimulation);
  this->ci_relax_spine_x_angle_adjustment.ConnectTo(relax_spine_x->ci_angle_adjustment);

  relax_spine_x->activity.ConnectTo(fusion_rel_angle_spine_x->InputActivity(0));
  relax_spine_x->target_rating.ConnectTo(fusion_rel_angle_spine_x->InputTargetRating(0));
  relax_spine_x->co_angle_adjustment.ConnectTo(fusion_rel_angle_spine_x->InputPort(0, 0));

  // relax spine y
  this->ci_stimulation_relax_spine_y.ConnectTo(relax_spine_y->stimulation);
  this->ci_relax_spine_y_angle_adjustment.ConnectTo(relax_spine_y->ci_angle_adjustment);

  relax_spine_y->activity.ConnectTo(fusion_rel_angle_spine_y->InputActivity(0));
  relax_spine_y->target_rating.ConnectTo(fusion_rel_angle_spine_y->InputTargetRating(0));
  relax_spine_y->co_angle_adjustment.ConnectTo(fusion_rel_angle_spine_y->InputPort(0, 0));

  // relax hip y
  this->ci_stimulation_relax_hip_y.ConnectTo(relax_hip_y->stimulation);
  this->ci_left_relax_hip_y_angle_adjustment.ConnectTo(relax_hip_y->ci_left_angle_adjustment);
  this->ci_right_relax_hip_y_angle_adjustment.ConnectTo(relax_hip_y->ci_right_angle_adjustment);

  relax_hip_y->activity.ConnectTo(fusion_rel_angle_left_hip_y->InputActivity(0));
  relax_hip_y->target_rating.ConnectTo(fusion_rel_angle_left_hip_y->InputTargetRating(0));
  relax_hip_y->co_left_angle_adjustment.ConnectTo(fusion_rel_angle_left_hip_y->InputPort(0, 0));

  relax_hip_y->activity.ConnectTo(fusion_rel_angle_right_hip_y->InputActivity(0));
  relax_hip_y->target_rating.ConnectTo(fusion_rel_angle_right_hip_y->InputTargetRating(0));
  relax_hip_y->co_right_angle_adjustment.ConnectTo(fusion_rel_angle_right_hip_y->InputPort(0, 0));

  // reduce tension hip y
  this->ci_stimulation_reduce_tension_hip_y.ConnectTo(reduce_tension_hip_y->stimulation);
  this->ci_left_reduce_tension_hip_y_angle_adjustment.ConnectTo(reduce_tension_hip_y->ci_left_angle_adjustment);
  this->ci_right_reduce_tension_hip_y_angle_adjustment.ConnectTo(reduce_tension_hip_y->ci_right_angle_adjustment);

  reduce_tension_hip_y->activity.ConnectTo(fusion_rel_angle_left_hip_y->InputActivity(1));
  reduce_tension_hip_y->target_rating.ConnectTo(fusion_rel_angle_left_hip_y->InputTargetRating(1));
  reduce_tension_hip_y->co_left_angle_adjustment.ConnectTo(fusion_rel_angle_left_hip_y->InputPort(1, 0));

  reduce_tension_hip_y->activity.ConnectTo(fusion_rel_angle_right_hip_y->InputActivity(1));
  reduce_tension_hip_y->target_rating.ConnectTo(fusion_rel_angle_right_hip_y->InputTargetRating(1));
  reduce_tension_hip_y->co_right_angle_adjustment.ConnectTo(fusion_rel_angle_right_hip_y->InputPort(1, 0));

  // lateral foot placement
  // left
  this->ci_left_ground_contact.ConnectTo(lateral_foot_placement_left->ci_ground_contact);
  this->ci_right_lateral_velocity_correction.ConnectTo(lateral_foot_placement_left->ci_com_error_y);
  this->ci_right_com_y.ConnectTo(lateral_foot_placement_left->ci_com_y);
  this->ci_right_xcom_y.ConnectTo(lateral_foot_placement_left->ci_xcom_y);
  this->ci_actual_roll.ConnectTo(lateral_foot_placement_left->ci_pelvis_roll);
  this->ci_turn_left_signal_to_left.ConnectTo(lateral_foot_placement_left->ci_turn_left_signal);
//  this->ci_turn_right_signal_to_left.ConnectTo(lateral_foot_placement_left->ci_turn_right_signal);
  this->ci_last_step_state.ConnectTo(lateral_foot_placement_left->ci_last_step_state);
  this->ci_reset_simulation_experiments.ConnectTo(lateral_foot_placement_left->ci_reset_simualtion_experiments);

  this->si_left_hip_x_angle.ConnectTo(lateral_foot_placement_left->si_hip_x_angle);

  lateral_foot_placement_left->activity.ConnectTo(fusion_abs_angle_left_hip_x->InputActivity(2));
  lateral_foot_placement_left->target_rating.ConnectTo(fusion_abs_angle_left_hip_x->InputTargetRating(2));
  lateral_foot_placement_left->co_angle_hip_x.ConnectTo(fusion_abs_angle_left_hip_x->InputPort(2, 0));

  //right
  this->ci_right_ground_contact.ConnectTo(lateral_foot_placement_right->ci_ground_contact);
  this->ci_left_lateral_velocity_correction.ConnectTo(lateral_foot_placement_right->ci_com_error_y);
  this->ci_left_com_y.ConnectTo(lateral_foot_placement_right->ci_com_y);
  this->ci_left_xcom_y.ConnectTo(lateral_foot_placement_right->ci_xcom_y);
  this->ci_actual_roll.ConnectTo(lateral_foot_placement_right->ci_pelvis_roll);
// this->ci_turn_left_signal_to_right.ConnectTo(lateral_foot_placement_right->ci_turn_left_signal);
  this->ci_turn_right_signal_to_right.ConnectTo(lateral_foot_placement_right->ci_turn_right_signal);
  this->ci_last_step_state.ConnectTo(lateral_foot_placement_right->ci_last_step_state);
  this->ci_reset_simulation_experiments.ConnectTo(lateral_foot_placement_right->ci_reset_simualtion_experiments);

  this->si_right_hip_x_angle.ConnectTo(lateral_foot_placement_right->si_hip_x_angle);

  lateral_foot_placement_right->activity.ConnectTo(fusion_abs_angle_right_hip_x->InputActivity(2));
  lateral_foot_placement_right->target_rating.ConnectTo(fusion_abs_angle_right_hip_x->InputTargetRating(2));
  lateral_foot_placement_right->co_angle_hip_x.ConnectTo(fusion_abs_angle_right_hip_x->InputPort(2, 0));

  // control pelvis roll
  // left
  this->ci_left_ground_contact.ConnectTo(control_pelvis_roll_left->ci_ground_contact);
  this->ci_actual_roll.ConnectTo(control_pelvis_roll_left->ci_pelvis_roll);

  this->si_left_hip_x_angle.ConnectTo(control_pelvis_roll_left->si_hip_x_angle);
  control_pelvis_roll_left->activity.ConnectTo(fusion_abs_angle_left_hip_x->InputActivity(3));
  control_pelvis_roll_left->target_rating.ConnectTo(fusion_abs_angle_left_hip_x->InputTargetRating(3));
  control_pelvis_roll_left->co_hip_x_angle.ConnectTo(fusion_abs_angle_left_hip_x->InputPort(3, 0));

  //right
  this->ci_right_ground_contact.ConnectTo(control_pelvis_roll_right->ci_ground_contact);
  this->ci_actual_roll.ConnectTo(control_pelvis_roll_right->ci_pelvis_roll);

  this->si_right_hip_x_angle.ConnectTo(control_pelvis_roll_right->si_hip_x_angle);
  control_pelvis_roll_right->activity.ConnectTo(fusion_abs_angle_right_hip_x->InputActivity(3));
  control_pelvis_roll_right->target_rating.ConnectTo(fusion_abs_angle_right_hip_x->InputTargetRating(3));
  control_pelvis_roll_right->co_hip_x_angle.ConnectTo(fusion_abs_angle_right_hip_x->InputPort(3, 0));

  // control pelvis yaw
  // left
  this->ci_left_ground_contact.ConnectTo(control_pelvis_yaw_left->ci_ground_contact);
  this->ci_actual_yaw.ConnectTo(control_pelvis_yaw_left->ci_pelvis_yaw);
  this->ci_turn_left_signal_to_left.ConnectTo(control_pelvis_yaw_left->ci_turn_left_signal);
  this->ci_turn_right_signal_to_left.ConnectTo(control_pelvis_yaw_left->ci_turn_right_signal);

  this->si_left_hip_z_angle.ConnectTo(control_pelvis_yaw_left->si_hip_z_angle);
  control_pelvis_yaw_left->activity.ConnectTo(fusion_abs_angle_left_hip_z->InputActivity(1));
  control_pelvis_yaw_left->target_rating.ConnectTo(fusion_abs_angle_left_hip_z->InputTargetRating(1));
  control_pelvis_yaw_left->co_hip_z_angle.ConnectTo(fusion_abs_angle_left_hip_z->InputPort(1, 0));

  // right
  this->ci_right_ground_contact.ConnectTo(control_pelvis_yaw_right->ci_ground_contact);
  this->ci_actual_yaw.ConnectTo(control_pelvis_yaw_right->ci_pelvis_yaw);
  this->ci_turn_left_signal_to_right.ConnectTo(control_pelvis_yaw_right->ci_turn_left_signal);
  this->ci_turn_right_signal_to_right.ConnectTo(control_pelvis_yaw_right->ci_turn_right_signal);

  this->si_right_hip_z_angle.ConnectTo(control_pelvis_yaw_right->si_hip_z_angle);
  control_pelvis_yaw_right->activity.ConnectTo(fusion_abs_angle_right_hip_z->InputActivity(1));
  control_pelvis_yaw_right->target_rating.ConnectTo(fusion_abs_angle_right_hip_z->InputTargetRating(1));
  control_pelvis_yaw_right->co_hip_z_angle.ConnectTo(fusion_abs_angle_right_hip_z->InputPort(1, 0));
  //END reflexes
  // ---------------------------------------------------------------------------------------------


  //BEGIN motor patterns
  // ---------------------------------------------------------------------------------------------

  // left
  this->si_left_hip_x_angle.ConnectTo(lateral_hip_shift_left->si_hip_angle);
  this->si_right_hip_x_angle.ConnectTo(lateral_hip_shift_left->si_hip_angle_opposite);

  this->si_left_hip_x_angle.ConnectTo(lateral_hip_shift_init_left->si_hip_angle);
  this->si_right_hip_x_angle.ConnectTo(lateral_hip_shift_init_left->si_hip_angle_opposite);

  this->si_left_hip_x_angle.ConnectTo(lateral_hip_shift_left_push_recovery->si_hip_angle);
  this->si_right_hip_x_angle.ConnectTo(lateral_hip_shift_left_push_recovery->si_hip_angle_opposite);

  fusion_lateral_hip_shift_left->activity.ConnectTo(lateral_hip_shift_left_push_recovery->stimulation);
  this->ci_walking_scale_factor.ConnectTo(lateral_hip_shift_left_push_recovery->ci_scale);
  this->ci_adjusted_roll.ConnectTo(lateral_hip_shift_left_push_recovery->ci_ins_roll);
  this->ci_com_error_y.ConnectTo(lateral_hip_shift_left_push_recovery->ci_com_y);


  this->ci_actual_roll.ConnectTo(lateral_hip_shift_left->ci_ins_roll);
  this->ci_com_y.ConnectTo(lateral_hip_shift_left->ci_com_y);
  this->ci_push_recovery_stand.ConnectTo(lateral_hip_shift_left->ci_push_recovery_stand);

  lateral_hip_shift_left->activity.ConnectTo(fusion_abs_torque_left_hip_x->InputActivity(0));
  lateral_hip_shift_left->target_rating.ConnectTo(fusion_abs_torque_left_hip_x->InputTargetRating(0));
  lateral_hip_shift_left->co_left_torque_hip_x.ConnectTo(fusion_abs_torque_left_hip_x->InputPort(0, 0));

  lateral_hip_shift_left->activity.ConnectTo(fusion_abs_torque_right_hip_x->InputActivity(0));
  lateral_hip_shift_left->target_rating.ConnectTo(fusion_abs_torque_right_hip_x->InputTargetRating(0));
  lateral_hip_shift_left->co_right_torque_hip_x.ConnectTo(fusion_abs_torque_right_hip_x->InputPort(0, 0));

  lateral_hip_shift_left->activity.ConnectTo(fusion_abs_torque_spine_x->InputActivity(1));
  lateral_hip_shift_left->target_rating.ConnectTo(fusion_abs_torque_spine_x->InputTargetRating(1));
  lateral_hip_shift_left->co_torque_spine_x.ConnectTo(fusion_abs_torque_spine_x->InputPort(1, 0));

  lateral_hip_shift_init_left->activity.ConnectTo(fusion_abs_torque_left_hip_x->InputActivity(1));
  lateral_hip_shift_init_left->target_rating.ConnectTo(fusion_abs_torque_left_hip_x->InputTargetRating(1));
  lateral_hip_shift_init_left->co_left_torque_hip_x.ConnectTo(fusion_abs_torque_left_hip_x->InputPort(1, 0));

  lateral_hip_shift_init_left->activity.ConnectTo(fusion_abs_torque_right_hip_x->InputActivity(1));
  lateral_hip_shift_init_left->target_rating.ConnectTo(fusion_abs_torque_right_hip_x->InputTargetRating(1));
  lateral_hip_shift_init_left->co_right_torque_hip_x.ConnectTo(fusion_abs_torque_right_hip_x->InputPort(1, 0));

  lateral_hip_shift_init_left->activity.ConnectTo(fusion_abs_torque_spine_x->InputActivity(2));
  lateral_hip_shift_init_left->target_rating.ConnectTo(fusion_abs_torque_spine_x->InputTargetRating(2));
  lateral_hip_shift_init_left->co_torque_spine_x.ConnectTo(fusion_abs_torque_spine_x->InputPort(2, 0));

  // right

  this->si_right_hip_x_angle.ConnectTo(lateral_hip_shift_right->si_hip_angle);
  this->si_left_hip_x_angle.ConnectTo(lateral_hip_shift_right->si_hip_angle_opposite);

  skill_walking_init_left->activity.ConnectTo(lateral_hip_shift_right->ci_initiation_activity); // to calculate the necessary torque during initiation

//  this->si_right_hip_x_angle.ConnectTo(lateral_hip_shift_right_walking_init->si_hip_angle); //todo
//  this->si_left_hip_x_angle.ConnectTo(lateral_hip_shift_right_walking_init->si_hip_angle_opposite);

  this->si_right_hip_x_angle.ConnectTo(lateral_hip_shift_init_right->si_hip_angle);
  this->si_left_hip_x_angle.ConnectTo(lateral_hip_shift_init_right->si_hip_angle_opposite);

  this->si_right_hip_x_angle.ConnectTo(lateral_hip_shift_right_push_recovery->si_hip_angle);
  this->si_left_hip_x_angle.ConnectTo(lateral_hip_shift_right_push_recovery->si_hip_angle_opposite);

  fusion_lateral_hip_shift_right->activity.ConnectTo(lateral_hip_shift_right_push_recovery->stimulation);
  this->ci_walking_scale_factor.ConnectTo(lateral_hip_shift_right_push_recovery->ci_scale);
  this->ci_adjusted_roll.ConnectTo(lateral_hip_shift_right_push_recovery->ci_ins_roll);
  this->ci_com_error_y.ConnectTo(lateral_hip_shift_right_push_recovery->ci_com_y);

  lateral_hip_shift_right_push_recovery->activity.ConnectTo(fusion_abs_torque_right_hip_x->InputActivity(4));
  lateral_hip_shift_right_push_recovery->target_rating.ConnectTo(fusion_abs_torque_right_hip_x->InputTargetRating(4));
  lateral_hip_shift_right_push_recovery->co_right_torque_hip_x.ConnectTo(fusion_abs_torque_right_hip_x->InputPort(4, 0));

  lateral_hip_shift_right_push_recovery->activity.ConnectTo(fusion_abs_torque_left_hip_x->InputActivity(4));
  lateral_hip_shift_right_push_recovery->target_rating.ConnectTo(fusion_abs_torque_left_hip_x->InputTargetRating(4));
  lateral_hip_shift_right_push_recovery->co_left_torque_hip_x.ConnectTo(fusion_abs_torque_left_hip_x->InputPort(4, 0));

  lateral_hip_shift_right_push_recovery->activity.ConnectTo(fusion_abs_torque_spine_x->InputActivity(6));
  lateral_hip_shift_right_push_recovery->target_rating.ConnectTo(fusion_abs_torque_spine_x->InputTargetRating(6));
  lateral_hip_shift_right_push_recovery->co_torque_spine_x.ConnectTo(fusion_abs_torque_spine_x->InputPort(6, 0));

  this->ci_actual_roll.ConnectTo(lateral_hip_shift_right->ci_ins_roll);
  this->ci_com_y.ConnectTo(lateral_hip_shift_right->ci_com_y);
  this->ci_push_recovery_stand.ConnectTo(lateral_hip_shift_right->ci_push_recovery_stand);

//  this->ci_actual_roll.ConnectTo(lateral_hip_shift_right_walking_init->ci_ins_roll);//todo
//  this->ci_com_y.ConnectTo(lateral_hip_shift_right_walking_init->ci_com_y);
//  this->ci_push_recovery_stand.ConnectTo(lateral_hip_shift_right_walking_init->ci_push_recovery_stand);

  lateral_hip_shift_init_right->activity.ConnectTo(fusion_abs_torque_right_hip_x->InputActivity(2));
  lateral_hip_shift_init_right->target_rating.ConnectTo(fusion_abs_torque_right_hip_x->InputTargetRating(2));
  lateral_hip_shift_init_right->co_left_torque_hip_x.ConnectTo(fusion_abs_torque_right_hip_x->InputPort(2, 0));

  lateral_hip_shift_init_right->activity.ConnectTo(fusion_abs_torque_left_hip_x->InputActivity(2));
  lateral_hip_shift_init_right->target_rating.ConnectTo(fusion_abs_torque_left_hip_x->InputTargetRating(2));
  lateral_hip_shift_init_right->co_right_torque_hip_x.ConnectTo(fusion_abs_torque_left_hip_x->InputPort(2, 0));

  lateral_hip_shift_init_right->activity.ConnectTo(fusion_abs_torque_spine_x->InputActivity(3));
  lateral_hip_shift_init_right->target_rating.ConnectTo(fusion_abs_torque_spine_x->InputTargetRating(3));
  lateral_hip_shift_init_right->co_torque_spine_x.ConnectTo(fusion_abs_torque_spine_x->InputPort(3, 0));

  // hip active swing initiation
  // left

  this->ci_walking_scale_laterally.ConnectTo(active_hip_swing_init_left->ci_scale);

  active_hip_swing_init_left->activity.ConnectTo(fusion_abs_torque_left_hip_y->InputActivity(2));
  active_hip_swing_init_left->activity.ConnectTo(fusion_abs_torque_left_hip_y->InputTargetRating(2));
  active_hip_swing_init_left->co_torque_hip_y.ConnectTo(fusion_abs_torque_left_hip_y->InputPort(2, 0));

  active_hip_swing_init_left->activity.ConnectTo(fusion_abs_torque_right_hip_y->InputActivity(2));
  active_hip_swing_init_left->activity.ConnectTo(fusion_abs_torque_right_hip_y->InputTargetRating(2));
  active_hip_swing_init_left->co_torque_opposite_hip_y.ConnectTo(fusion_abs_torque_right_hip_y->InputPort(2, 0));

  // right
  this->ci_walking_scale_laterally.ConnectTo(active_hip_swing_init_right->ci_scale);

  active_hip_swing_init_right->activity.ConnectTo(fusion_abs_torque_right_hip_y->InputActivity(3));
  active_hip_swing_init_right->activity.ConnectTo(fusion_abs_torque_right_hip_y->InputTargetRating(3));
  active_hip_swing_init_right->co_torque_hip_y.ConnectTo(fusion_abs_torque_right_hip_y->InputPort(3, 0));

  active_hip_swing_init_right->activity.ConnectTo(fusion_abs_torque_left_hip_y->InputActivity(3));
  active_hip_swing_init_right->activity.ConnectTo(fusion_abs_torque_left_hip_y->InputTargetRating(3));
  active_hip_swing_init_right->co_torque_opposite_hip_y.ConnectTo(fusion_abs_torque_left_hip_y->InputPort(3, 0));


  // right
  lateral_hip_shift_right->activity.ConnectTo(fusion_abs_torque_right_hip_x->InputActivity(3));
  lateral_hip_shift_right->target_rating.ConnectTo(fusion_abs_torque_right_hip_x->InputTargetRating(3));
  lateral_hip_shift_right->co_right_torque_hip_x.ConnectTo(fusion_abs_torque_right_hip_x->InputPort(3, 0));

//  lateral_hip_shift_right_walking_init->activity.ConnectTo(fusion_abs_torque_right_hip_x->InputActivity(4));//todo
//  lateral_hip_shift_right_walking_init->target_rating.ConnectTo(fusion_abs_torque_right_hip_x->InputTargetRating(4));
//  lateral_hip_shift_right_walking_init->co_right_torque_hip_x.ConnectTo(fusion_abs_torque_right_hip_x->InputPort(4, 0));

  lateral_hip_shift_right->activity.ConnectTo(fusion_abs_torque_left_hip_x->InputActivity(3));
  lateral_hip_shift_right->target_rating.ConnectTo(fusion_abs_torque_left_hip_x->InputTargetRating(3));
  lateral_hip_shift_right->co_left_torque_hip_x.ConnectTo(fusion_abs_torque_left_hip_x->InputPort(3, 0));

//  lateral_hip_shift_right_walking_init->activity.ConnectTo(fusion_abs_torque_left_hip_x->InputActivity(5));
//  lateral_hip_shift_right_walking_init->target_rating.ConnectTo(fusion_abs_torque_left_hip_x->InputTargetRating(5));
//  lateral_hip_shift_right_walking_init->co_left_torque_hip_x.ConnectTo(fusion_abs_torque_left_hip_x->InputPort(5, 0));

  lateral_hip_shift_right->activity.ConnectTo(fusion_abs_torque_spine_x->InputActivity(4));
  lateral_hip_shift_right->target_rating.ConnectTo(fusion_abs_torque_spine_x->InputTargetRating(4));
  lateral_hip_shift_right->co_torque_spine_x.ConnectTo(fusion_abs_torque_spine_x->InputPort(4, 0));

  lateral_hip_shift_right_push_recovery->activity.ConnectTo(fusion_abs_torque_spine_x->InputActivity(5));
  lateral_hip_shift_right_push_recovery->target_rating.ConnectTo(fusion_abs_torque_spine_x->InputTargetRating(5));
  lateral_hip_shift_right_push_recovery->co_torque_spine_x.ConnectTo(fusion_abs_torque_spine_x->InputPort(5, 0));
//  lateral_hip_shift_right_walking_init->activity.ConnectTo(fusion_abs_torque_spine_x->InputActivity(5));
//  lateral_hip_shift_right_walking_init->target_rating.ConnectTo(fusion_abs_torque_spine_x->InputTargetRating(5));
//  lateral_hip_shift_right_walking_init->co_torque_spine_x.ConnectTo(fusion_abs_torque_spine_x->InputPort(5, 0));

  // active hip swing
  // left
  this->ci_walking_scale_factor.ConnectTo(active_hip_swing_left->ci_scale);
  this->ci_right_ground_contact.ConnectTo(active_hip_swing_left->ci_ground_contact);
  this->ci_termi_lock_hip.ConnectTo(active_hip_swing_left->ci_termination_last_step);
  this->ci_turn_left_signal_to_left.ConnectTo(active_hip_swing_left->ci_turn_left_signal);
  this->ci_turn_right_signal_to_left.ConnectTo(active_hip_swing_left->ci_turn_right_signal);
//  this->ci_push_recovery_stand.ConnectTo(active_hip_swing_left->ci_push_recovery_stand);
  //this->ci_push_recovery_walking.ConnectTo(active_hip_swing_left->ci_push_recovery_walking);
//  this->ci_push_detected_walking_left.ConnectTo(active_hip_swing_left->ci_push_recovery_walking_left);
//  this->ci_push_detected_walking_right.ConnectTo(active_hip_swing_left->ci_push_recovery_walking_right);
  this->ci_time_deceleration.ConnectTo(active_hip_swing_left->ci_time_deceleration);
  this->ci_time_stop.ConnectTo(active_hip_swing_left->ci_time_stop);
  this->ci_hip_motor_factor_push_recovery.ConnectTo(active_hip_swing_left->ci_hip_motor_factor_push_recovery);
  this->ci_left_com_x.ConnectTo(active_hip_swing_left->ci_com_x_left);
  this->ci_right_com_x.ConnectTo(active_hip_swing_left->ci_com_x_right);
  this->ci_left_xcom_x.ConnectTo(active_hip_swing_left->ci_xcom_x_left);
  this->ci_right_xcom_x.ConnectTo(active_hip_swing_left->ci_xcom_x_right);
  this->ci_left_forward_velocity_correction.ConnectTo(active_hip_swing_left->ci_forward_velocity_correction);
  skill_walking_init_left->activity.ConnectTo(active_hip_swing_left->ci_initiation_activity);
  this->ci_hip_swing_action1.ConnectTo(active_hip_swing_left->ci_hip_swing_action1);
  this->ci_hip_swing_action2.ConnectTo(active_hip_swing_left->ci_hip_swing_action2);
  this->ci_hip_swing_action3.ConnectTo(active_hip_swing_left->ci_hip_swing_action3);
  this->ci_hip_swing_action4.ConnectTo(active_hip_swing_left->ci_hip_swing_action4);
  this->ci_loop_times.ConnectTo(active_hip_swing_left->ci_loop_times);

  // active hip swing sin
  this->si_left_foot_position_x.ConnectTo(active_hip_swing_left_sin->si_foot_left_position_x);
  this->si_left_foot_position_y.ConnectTo(active_hip_swing_left_sin->si_foot_left_position_y);
  this->si_left_foot_position_z.ConnectTo(active_hip_swing_left_sin->si_foot_left_position_z);
  this->si_right_foot_position_x.ConnectTo(active_hip_swing_left_sin->si_foot_right_position_x);
  this->si_right_foot_position_y.ConnectTo(active_hip_swing_left_sin->si_foot_right_position_y);
  this->si_right_foot_position_z.ConnectTo(active_hip_swing_left_sin->si_foot_right_position_z);

  this->ci_obstacle_walking_stimulation.ConnectTo(active_hip_swing_left_sin->ci_obstacle_walking);
  this->ci_obstacle_step.ConnectTo(active_hip_swing_left_sin->ci_obstacle_step);
  this->ci_obstacle_hip_swing_1.ConnectTo(active_hip_swing_left_sin->ci_obstacle_hip_swing_1);
  this->ci_obstacle_hip_swing_2.ConnectTo(active_hip_swing_left_sin->ci_obstacle_hip_swing_2);
  this->ci_obstacle_hip_swing_3.ConnectTo(active_hip_swing_left_sin->ci_obstacle_hip_swing_3);
  this->ci_obstacle_hip_swing_3_rear.ConnectTo(active_hip_swing_left_sin->ci_obstacle_hip_swing_3_rear);

  this->ci_walking_scale_factor.ConnectTo(active_hip_swing_left_sin->ci_scale);
  this->ci_right_ground_contact.ConnectTo(active_hip_swing_left_sin->ci_ground_contact);
  this->ci_termi_lock_hip.ConnectTo(active_hip_swing_left_sin->ci_termination_last_step);
  this->ci_turn_left_signal_to_left.ConnectTo(active_hip_swing_left_sin->ci_turn_left_signal);
  //this->ci_turn_right_signal_to_left.ConnectTo(active_hip_swing_left_sin->ci_turn_right_signal);

  // push recovery for hip and ankle, uncomment when needed
//  this->ci_left_learning_torque_factor.ConnectTo(active_hip_swing_left_sin->ci_hip_motor_factor_push_recovery);
//  this->ci_push_started_x.ConnectTo(active_hip_swing_left_sin->ci_push_recovery_walking);

//  this->ci_push_recovery_stand.ConnectTo(active_hip_swing_left_sin->ci_push_recovery_stand);
//  this->ci_push_recovery_walking.ConnectTo(active_hip_swing_left_sin->ci_push_recovery_walking);
//  this->ci_push_detected_walking_left.ConnectTo(active_hip_swing_left_sin->ci_push_recovery_walking_left);
//  this->ci_push_detected_walking_right.ConnectTo(active_hip_swing_left_sin->ci_push_recovery_walking_right);
  this->ci_time_deceleration.ConnectTo(active_hip_swing_left_sin->ci_time_deceleration);
  this->ci_time_stop.ConnectTo(active_hip_swing_left_sin->ci_time_stop);
  this->ci_hip_motor_factor_push_recovery.ConnectTo(active_hip_swing_left_sin->ci_hip_motor_factor_push_recovery);
  this->ci_left_com_x.ConnectTo(active_hip_swing_left_sin->ci_com_x_left);
  this->ci_right_com_x.ConnectTo(active_hip_swing_left_sin->ci_com_x_right);
  this->ci_left_xcom_x.ConnectTo(active_hip_swing_left_sin->ci_xcom_x_left);
  this->ci_right_xcom_x.ConnectTo(active_hip_swing_left_sin->ci_xcom_x_right);
  this->ci_left_forward_velocity_correction.ConnectTo(active_hip_swing_left_sin->ci_forward_velocity_correction);
  skill_walking_init_left->activity.ConnectTo(active_hip_swing_left_sin->ci_initiation_activity);
  this->ci_hip_swing_action1.ConnectTo(active_hip_swing_left_sin->ci_hip_swing_action1);
  this->ci_hip_swing_action2.ConnectTo(active_hip_swing_left_sin->ci_hip_swing_action2);
  this->ci_hip_swing_action3.ConnectTo(active_hip_swing_left_sin->ci_hip_swing_action3);
  this->ci_hip_swing_action4.ConnectTo(active_hip_swing_left_sin->ci_hip_swing_action4);
  this->ci_loop_times.ConnectTo(active_hip_swing_left_sin->ci_loop_times);

  // hip swing curve walking
  this->ci_walking_scale_factor.ConnectTo(hip_swing_curve_walking_left->ci_scale);
  this->ci_turn_left_signal_to_left.ConnectTo(hip_swing_curve_walking_left->ci_turn_left_signal);


  // hip swing bent knee
  this->ci_walking_scale_factor.ConnectTo(hip_swing_bent_knee_left->ci_scale);
  this->ci_push_recovery_walking.ConnectTo(hip_swing_bent_knee_left->ci_push_recovery_walking);
  this->ci_bent_knee.ConnectTo(hip_swing_bent_knee_left->ci_bent_knee);
  this->ci_left_xcom_x.ConnectTo(hip_swing_bent_knee_left->ci_xcom_x);

  this->ci_walking_scale_factor.ConnectTo(hip_left_push_recovery_dece->ci_scale);
  this->ci_push_recovery_stand.ConnectTo(hip_left_push_recovery_dece->ci_push_recovery_stand);

  // left
  active_hip_swing_left_sin->activity.ConnectTo(fusion_abs_torque_left_hip_y->InputActivity(12));
  active_hip_swing_left_sin->activity.ConnectTo(fusion_abs_torque_left_hip_y->InputTargetRating(12));
  active_hip_swing_left_sin->co_torque_hip_y.ConnectTo(fusion_abs_torque_left_hip_y->InputPort(12, 0));

  active_hip_swing_right->activity.ConnectTo(fusion_abs_torque_left_hip_y->InputActivity(7));
  active_hip_swing_right->activity.ConnectTo(fusion_abs_torque_left_hip_y->InputTargetRating(7));
  active_hip_swing_right->co_torque_opposite_hip_y.ConnectTo(fusion_abs_torque_left_hip_y->InputPort(7, 0));

  active_hip_swing_right_sin->activity.ConnectTo(fusion_abs_torque_left_hip_y->InputActivity(13));
  active_hip_swing_right_sin->activity.ConnectTo(fusion_abs_torque_left_hip_y->InputTargetRating(13));
  active_hip_swing_right_sin->co_torque_opposite_hip_y.ConnectTo(fusion_abs_torque_left_hip_y->InputPort(13, 0));

  hip_swing_curve_walking_left->co_stimulation_torque_hip_z.ConnectTo(fusion_abs_torque_left_hip_z->InputActivity(2));
  hip_swing_curve_walking_left->co_stimulation_torque_hip_z.ConnectTo(fusion_abs_torque_left_hip_z->InputTargetRating(2));
  hip_swing_curve_walking_left->co_torque_hip_z.ConnectTo(fusion_abs_torque_left_hip_z->InputPort(2, 0));

  hip_swing_curve_walking_left->co_stimulation_torque_hip_x.ConnectTo(fusion_abs_torque_left_hip_x->InputActivity(6));
  hip_swing_curve_walking_left->co_stimulation_torque_hip_x.ConnectTo(fusion_abs_torque_left_hip_x->InputTargetRating(6));
  hip_swing_curve_walking_left->co_torque_hip_x.ConnectTo(fusion_abs_torque_left_hip_x->InputPort(6, 0));

  hip_swing_curve_walking_left->co_stimulation_torque_opposite_hip_z.ConnectTo(fusion_abs_torque_right_hip_z->InputActivity(2));
  hip_swing_curve_walking_left->co_stimulation_torque_opposite_hip_z.ConnectTo(fusion_abs_torque_right_hip_z->InputTargetRating(2));
  hip_swing_curve_walking_left->co_torque_opposite_hip_z.ConnectTo(fusion_abs_torque_right_hip_z->InputPort(2, 0));

  active_hip_swing_left_sin->co_stimulation_angle_hip_z.ConnectTo(fusion_abs_angle_right_hip_z->InputActivity(3));
  active_hip_swing_left_sin->co_stimulation_angle_hip_z.ConnectTo(fusion_abs_angle_right_hip_z->InputTargetRating(3));
  active_hip_swing_left_sin->co_angle_hip_z.ConnectTo(fusion_abs_angle_right_hip_z->InputPort(3, 0));

  active_hip_swing_left_sin->co_stimulation_angle_opposite_hip_z.ConnectTo(fusion_abs_angle_left_hip_z->InputActivity(3));
  active_hip_swing_left_sin->co_stimulation_angle_opposite_hip_z.ConnectTo(fusion_abs_angle_left_hip_z->InputTargetRating(3));
  active_hip_swing_left_sin->co_angle_opposite_hip_z.ConnectTo(fusion_abs_angle_left_hip_z->InputPort(3, 0));



  active_hip_swing_left->activity.ConnectTo(fusion_abs_torque_left_hip_y->InputActivity(4));
  active_hip_swing_left->activity.ConnectTo(fusion_abs_torque_left_hip_y->InputTargetRating(4));
  active_hip_swing_left->co_torque_hip_y.ConnectTo(fusion_abs_torque_left_hip_y->InputPort(4, 0));

  hip_swing_bent_knee_left->activity.ConnectTo(fusion_abs_torque_left_hip_y->InputActivity(5));
  hip_swing_bent_knee_left->activity.ConnectTo(fusion_abs_torque_left_hip_y->InputTargetRating(5));
  hip_swing_bent_knee_left->co_torque_hip_y.ConnectTo(fusion_abs_torque_left_hip_y->InputPort(5, 0));

  hip_left_push_recovery_dece->activity.ConnectTo(fusion_abs_torque_left_hip_y->InputActivity(6));
  hip_left_push_recovery_dece->activity.ConnectTo(fusion_abs_torque_left_hip_y->InputTargetRating(6));
  hip_left_push_recovery_dece->co_torque_one.ConnectTo(fusion_abs_torque_left_hip_y->InputPort(6, 0));

  // active hip swing init
//  active_hip_swing_left_init->activity.ConnectTo(fusion_abs_torque_left_hip_y->InputActivity(11));
//  active_hip_swing_left_init->target_rating.ConnectTo(fusion_abs_torque_left_hip_y->InputTargetRating(11));
//  active_hip_swing_left_init->co_torque_pattern.ConnectTo(fusion_abs_torque_left_hip_y->InputPort(11,0));

  // right
  active_hip_swing_left->activity.ConnectTo(fusion_abs_torque_right_hip_y->InputActivity(4));
  active_hip_swing_left->activity.ConnectTo(fusion_abs_torque_right_hip_y->InputTargetRating(4));
  active_hip_swing_left->co_torque_opposite_hip_y.ConnectTo(fusion_abs_torque_right_hip_y->InputPort(4, 0));

  active_hip_swing_left_sin->activity.ConnectTo(fusion_abs_torque_right_hip_y->InputActivity(12));
  active_hip_swing_left_sin->activity.ConnectTo(fusion_abs_torque_right_hip_y->InputTargetRating(12));
  active_hip_swing_left_sin->co_torque_opposite_hip_y.ConnectTo(fusion_abs_torque_right_hip_y->InputPort(12, 0));

  hip_swing_curve_walking_right->co_stimulation_torque_hip_z.ConnectTo(fusion_abs_torque_right_hip_z->InputActivity(3));
  hip_swing_curve_walking_right->co_stimulation_torque_hip_z.ConnectTo(fusion_abs_torque_right_hip_z->InputTargetRating(3));
  hip_swing_curve_walking_right->co_torque_hip_z.ConnectTo(fusion_abs_torque_right_hip_z->InputPort(3, 0));

  hip_swing_curve_walking_right->co_stimulation_torque_opposite_hip_z.ConnectTo(fusion_abs_torque_left_hip_z->InputActivity(3));
  hip_swing_curve_walking_right->co_stimulation_torque_opposite_hip_z.ConnectTo(fusion_abs_torque_left_hip_z->InputTargetRating(3));
  hip_swing_curve_walking_right->co_torque_opposite_hip_z.ConnectTo(fusion_abs_torque_left_hip_z->InputPort(3, 0));

  hip_swing_curve_walking_right->co_stimulation_torque_hip_x.ConnectTo(fusion_abs_torque_right_hip_x->InputActivity(6));
  hip_swing_curve_walking_right->co_stimulation_torque_hip_x.ConnectTo(fusion_abs_torque_right_hip_x->InputTargetRating(6));
  hip_swing_curve_walking_right->co_torque_hip_x.ConnectTo(fusion_abs_torque_right_hip_x->InputPort(6, 0));

  active_hip_swing_right_sin->co_stimulation_angle_hip_z.ConnectTo(fusion_abs_angle_right_hip_z->InputActivity(4));
  active_hip_swing_right_sin->co_stimulation_angle_hip_z.ConnectTo(fusion_abs_angle_right_hip_z->InputTargetRating(4));
  active_hip_swing_right_sin->co_angle_hip_z.ConnectTo(fusion_abs_angle_right_hip_z->InputPort(4, 0));

  active_hip_swing_right_sin->co_stimulation_angle_opposite_hip_z.ConnectTo(fusion_abs_angle_left_hip_z->InputActivity(4));
  active_hip_swing_right_sin->co_stimulation_angle_opposite_hip_z.ConnectTo(fusion_abs_angle_left_hip_z->InputTargetRating(4));
  active_hip_swing_right_sin->co_angle_opposite_hip_z.ConnectTo(fusion_abs_angle_left_hip_z->InputPort(4, 0));

  hip_swing_bent_knee_left->activity.ConnectTo(fusion_abs_torque_right_hip_y->InputActivity(5));
  hip_swing_bent_knee_left->activity.ConnectTo(fusion_abs_torque_right_hip_y->InputTargetRating(5));
  hip_swing_bent_knee_left->co_torque_opposite_hip_y.ConnectTo(fusion_abs_torque_right_hip_y->InputPort(5, 0));

  hip_left_push_recovery_dece->activity.ConnectTo(fusion_abs_torque_right_hip_y->InputActivity(6));
  hip_left_push_recovery_dece->activity.ConnectTo(fusion_abs_torque_right_hip_y->InputTargetRating(6));
  hip_left_push_recovery_dece->co_torque_two.ConnectTo(fusion_abs_torque_right_hip_y->InputPort(6, 0));

  mp_turn_leg_left_z->activity.ConnectTo(fusion_abs_torque_left_hip_z->InputActivity(0));
  mp_turn_leg_left_z->target_rating.ConnectTo(fusion_abs_torque_left_hip_z->InputTargetRating(0));
  mp_turn_leg_left_z->co_torque_one.ConnectTo(fusion_abs_torque_left_hip_z->InputPort(0, 0));

  mp_turn_leg_left_z->activity.ConnectTo(fusion_abs_torque_right_hip_z->InputActivity(0));
  mp_turn_leg_left_z->target_rating.ConnectTo(fusion_abs_torque_right_hip_z->InputTargetRating(0));
  mp_turn_leg_left_z->co_torque_two.ConnectTo(fusion_abs_torque_right_hip_z->InputPort(0, 0));

  // right hip swing bent knee and push recovery
  this->ci_walking_scale_factor.ConnectTo(active_hip_swing_right->ci_scale);
  this->ci_left_ground_contact.ConnectTo(active_hip_swing_right->ci_ground_contact);
  this->ci_termi_lock_hip.ConnectTo(active_hip_swing_right->ci_termination_last_step);
  this->ci_turn_left_signal_to_right.ConnectTo(active_hip_swing_right->ci_turn_left_signal);
  this->ci_turn_right_signal_to_right.ConnectTo(active_hip_swing_right->ci_turn_right_signal);
//  this->ci_push_recovery_stand.ConnectTo(active_hip_swing_right->ci_push_recovery_stand);
//  this->ci_push_recovery_walking.ConnectTo(active_hip_swing_right->ci_push_recovery_walking);
//  this->ci_push_detected_walking_left.ConnectTo(active_hip_swing_right->ci_push_recovery_walking_left);
//  this->ci_push_detected_walking_right.ConnectTo(active_hip_swing_right->ci_push_recovery_walking_right);
  this->ci_left_com_x.ConnectTo(active_hip_swing_right->ci_com_x_left);
  this->ci_right_com_x.ConnectTo(active_hip_swing_right->ci_com_x_right);
  this->ci_left_xcom_x.ConnectTo(active_hip_swing_right->ci_xcom_x_left);
  this->ci_right_xcom_x.ConnectTo(active_hip_swing_right->ci_xcom_x_right);
  this->ci_right_forward_velocity_correction.ConnectTo(active_hip_swing_right->ci_forward_velocity_correction);
  this->ci_hip_swing_action1.ConnectTo(active_hip_swing_right->ci_hip_swing_action1);
  this->ci_hip_swing_action2.ConnectTo(active_hip_swing_right->ci_hip_swing_action2);
  this->ci_hip_swing_action3.ConnectTo(active_hip_swing_right->ci_hip_swing_action3);
  this->ci_hip_swing_action4.ConnectTo(active_hip_swing_right->ci_hip_swing_action4);
  this->ci_loop_times.ConnectTo(active_hip_swing_right->ci_loop_times);

  // push recovery for hip and ankle, uncomment when needed
  // active hip swing sin
//  this->ci_push_started_x.ConnectTo(active_hip_swing_right_sin->ci_push_recovery_walking);
//  this->ci_right_learning_torque_factor.ConnectTo(active_hip_swing_right_sin->ci_hip_motor_factor_push_recovery);

  this->ci_obstacle_walking_stimulation.ConnectTo(active_hip_swing_right_sin->ci_obstacle_walking);
  this->ci_obstacle_step.ConnectTo(active_hip_swing_right_sin->ci_obstacle_step);
  this->ci_obstacle_hip_swing_1.ConnectTo(active_hip_swing_right_sin->ci_obstacle_hip_swing_1);
  this->ci_obstacle_hip_swing_2.ConnectTo(active_hip_swing_right_sin->ci_obstacle_hip_swing_2);
  this->ci_obstacle_hip_swing_3.ConnectTo(active_hip_swing_right_sin->ci_obstacle_hip_swing_3);
  this->ci_obstacle_hip_swing_3_rear.ConnectTo(active_hip_swing_right_sin->ci_obstacle_hip_swing_3_rear);

  this->ci_walking_scale_factor.ConnectTo(active_hip_swing_right_sin->ci_scale);
  this->ci_left_ground_contact.ConnectTo(active_hip_swing_right_sin->ci_ground_contact);
  this->ci_termi_lock_hip.ConnectTo(active_hip_swing_right_sin->ci_termination_last_step);
  //this->ci_turn_left_signal_to_right.ConnectTo(active_hip_swing_right_sin->ci_turn_left_signal);
  this->ci_turn_right_signal_to_right.ConnectTo(active_hip_swing_right_sin->ci_turn_right_signal);
//  this->ci_push_recovery_stand.ConnectTo(active_hip_swing_right_sin->ci_push_recovery_stand);
//  this->ci_push_recovery_walking.ConnectTo(active_hip_swing_right_sin->ci_push_recovery_walking);
//  this->ci_push_detected_walking_left.ConnectTo(active_hip_swing_right_sin->ci_push_recovery_walking_left);
//  this->ci_push_detected_walking_right.ConnectTo(active_hip_swing_right_sin->ci_push_recovery_walking_right);
  this->ci_left_com_x.ConnectTo(active_hip_swing_right_sin->ci_com_x_left);
  this->ci_right_com_x.ConnectTo(active_hip_swing_right_sin->ci_com_x_right);
  this->ci_left_xcom_x.ConnectTo(active_hip_swing_right_sin->ci_xcom_x_left);
  this->ci_right_xcom_x.ConnectTo(active_hip_swing_right_sin->ci_xcom_x_right);
  this->ci_right_forward_velocity_correction.ConnectTo(active_hip_swing_right_sin->ci_forward_velocity_correction);
  this->ci_hip_swing_action1.ConnectTo(active_hip_swing_right_sin->ci_hip_swing_action1);
  this->ci_hip_swing_action2.ConnectTo(active_hip_swing_right_sin->ci_hip_swing_action2);
  this->ci_hip_swing_action3.ConnectTo(active_hip_swing_right_sin->ci_hip_swing_action3);
  this->ci_hip_swing_action4.ConnectTo(active_hip_swing_right_sin->ci_hip_swing_action4);
  this->ci_loop_times.ConnectTo(active_hip_swing_right_sin->ci_loop_times);

  this->si_left_foot_position_x.ConnectTo(active_hip_swing_right_sin->si_foot_left_position_x);
  this->si_left_foot_position_y.ConnectTo(active_hip_swing_right_sin->si_foot_left_position_y);
  this->si_left_foot_position_z.ConnectTo(active_hip_swing_right_sin->si_foot_left_position_z);
  this->si_right_foot_position_x.ConnectTo(active_hip_swing_right_sin->si_foot_right_position_x);
  this->si_right_foot_position_y.ConnectTo(active_hip_swing_right_sin->si_foot_right_position_y);
  this->si_right_foot_position_z.ConnectTo(active_hip_swing_right_sin->si_foot_right_position_z);

  // hip swing curve walking
  this->ci_walking_scale_factor.ConnectTo(hip_swing_curve_walking_right->ci_scale);
  this->ci_turn_right_signal_to_right.ConnectTo(hip_swing_curve_walking_right->ci_turn_right_signal);


  // hip swing bent knee
  this->ci_walking_scale_factor.ConnectTo(hip_swing_bent_knee_right->ci_scale);
  this->ci_push_recovery_walking.ConnectTo(hip_swing_bent_knee_right->ci_push_recovery_walking);
  this->ci_bent_knee.ConnectTo(hip_swing_bent_knee_right->ci_bent_knee);
  this->ci_right_xcom_x.ConnectTo(hip_swing_bent_knee_right->ci_xcom_x);

  active_hip_swing_right->activity.ConnectTo(fusion_abs_torque_right_hip_y->InputActivity(7));
  active_hip_swing_right->activity.ConnectTo(fusion_abs_torque_right_hip_y->InputTargetRating(7));
  active_hip_swing_right->co_torque_hip_y.ConnectTo(fusion_abs_torque_right_hip_y->InputPort(7, 0));

  active_hip_swing_right_sin->activity.ConnectTo(fusion_abs_torque_right_hip_y->InputActivity(13));
  active_hip_swing_right_sin->activity.ConnectTo(fusion_abs_torque_right_hip_y->InputTargetRating(13));
  active_hip_swing_right_sin->co_torque_hip_y.ConnectTo(fusion_abs_torque_right_hip_y->InputPort(13, 0));

  hip_swing_bent_knee_right->activity.ConnectTo(fusion_abs_torque_right_hip_y->InputActivity(8));
  hip_swing_bent_knee_right->activity.ConnectTo(fusion_abs_torque_right_hip_y->InputTargetRating(8));
  hip_swing_bent_knee_right->co_torque_hip_y.ConnectTo(fusion_abs_torque_right_hip_y->InputPort(8, 0));

  active_hip_swing_right->activity.ConnectTo(fusion_abs_torque_left_hip_y->InputActivity(7));
  active_hip_swing_right->activity.ConnectTo(fusion_abs_torque_left_hip_y->InputTargetRating(7));
  active_hip_swing_right->co_torque_opposite_hip_y.ConnectTo(fusion_abs_torque_left_hip_y->InputPort(7, 0));

  active_hip_swing_right_sin->activity.ConnectTo(fusion_abs_torque_left_hip_y->InputActivity(13));
  active_hip_swing_right_sin->activity.ConnectTo(fusion_abs_torque_left_hip_y->InputTargetRating(13));
  active_hip_swing_right_sin->co_torque_opposite_hip_y.ConnectTo(fusion_abs_torque_left_hip_y->InputPort(13, 0));

  hip_swing_bent_knee_right->activity.ConnectTo(fusion_abs_torque_left_hip_y->InputActivity(8));
  hip_swing_bent_knee_right->activity.ConnectTo(fusion_abs_torque_left_hip_y->InputTargetRating(8));
  hip_swing_bent_knee_right->co_torque_opposite_hip_y.ConnectTo(fusion_abs_torque_left_hip_y->InputPort(8, 0));

  mp_turn_leg_right_z->activity.ConnectTo(fusion_abs_torque_left_hip_z->InputActivity(1));
  mp_turn_leg_right_z->target_rating.ConnectTo(fusion_abs_torque_left_hip_z->InputTargetRating(1));
  mp_turn_leg_right_z->co_torque_two.ConnectTo(fusion_abs_torque_left_hip_z->InputPort(1, 0));

  mp_turn_leg_right_z->activity.ConnectTo(fusion_abs_torque_right_hip_z->InputActivity(1));
  mp_turn_leg_right_z->target_rating.ConnectTo(fusion_abs_torque_right_hip_z->InputTargetRating(1));
  mp_turn_leg_right_z->co_torque_one.ConnectTo(fusion_abs_torque_right_hip_z->InputPort(1, 0));

  // turn
  // left left
  this->ci_left_ground_contact.ConnectTo(mp_turn_left_left->ci_ground_contact);
  this->ci_walking_scale_laterally.ConnectTo(mp_turn_left_left->ci_lateral_scale);

  this->ci_turn_left_signal_to_left.ConnectTo(mp_turn_spine_left->ci_scale);

//  mp_turn_left_left->activity.ConnectTo(fusion_abs_torque_left_hip_x->InputActivity(4));
//  mp_turn_left_left->target_rating.ConnectTo(fusion_abs_torque_left_hip_x->InputTargetRating(4));
//  mp_turn_left_left->co_torque_one.ConnectTo(fusion_abs_torque_left_hip_x->InputPort(4, 0));


  mp_turn_spine_left->activity.ConnectTo(fusion_abs_torque_spine_z->InputActivity(0));
  mp_turn_spine_left->target_rating.ConnectTo(fusion_abs_torque_spine_z->InputTargetRating(0));
  mp_turn_spine_left->co_spine_torque_z.ConnectTo(fusion_abs_torque_spine_z->InputPort(0, 0));

  // todo:only for the test
  mp_turn_spine_left->activity.ConnectTo(fusion_abs_torque_spine_y->InputActivity(0));
  mp_turn_spine_left->target_rating.ConnectTo(fusion_abs_torque_spine_y->InputTargetRating(0));
  mp_turn_spine_left->co_spine_torque_z.ConnectTo(fusion_abs_torque_spine_y->InputPort(0, 0));

  // lift leg
  // left
  mp_lift_leg_left->activity.ConnectTo(fusion_abs_torque_left_hip_y->InputActivity(9));
  mp_lift_leg_left->activity.ConnectTo(fusion_abs_torque_left_hip_y->InputTargetRating(9));
  mp_lift_leg_left->co_torque_hip_y.ConnectTo(fusion_abs_torque_left_hip_y->InputPort(9, 0));

  // right
  mp_lift_leg_right->activity.ConnectTo(fusion_abs_torque_right_hip_y->InputActivity(9));
  mp_lift_leg_right->activity.ConnectTo(fusion_abs_torque_right_hip_y->InputTargetRating(9));
  mp_lift_leg_right->co_torque_hip_y.ConnectTo(fusion_abs_torque_right_hip_y->InputPort(9, 0));

  //BEGIN skills
  // ---------------------------------------------------------------------------------------------

  // stand
  this->ci_stimulation_stand.ConnectTo(skill_stand->stimulation);

  // Compensation hip pitch
  this->ci_disturbance_estimation_activity.ConnectTo(skill_compensation_hip_pitch->stimulation);
  this->ci_angle_trunk_space_prop.ConnectTo(skill_compensation_hip_pitch->ci_angle_trunk_space_prop);
  this->ci_angle_trunk_space_dist.ConnectTo(skill_compensation_hip_pitch->ci_angle_trunk_space_dist);
  this->ci_target_angle_trunk_space.ConnectTo(skill_compensation_hip_pitch->ci_target_angle_trunk_space);
  this->ci_angle_body_space.ConnectTo(skill_compensation_hip_pitch->ci_angle_body_space);

  skill_compensation_hip_pitch->activity.ConnectTo(fusion_abs_torque_right_hip_y->InputActivity(11));
  skill_compensation_hip_pitch->activity.ConnectTo(fusion_abs_torque_right_hip_y->InputTargetRating(11));
  skill_compensation_hip_pitch->co_torque_hip_y.ConnectTo(fusion_abs_torque_right_hip_y->InputPort(11, 0));

  skill_compensation_hip_pitch->activity.ConnectTo(fusion_abs_torque_left_hip_y->InputActivity(11));
  skill_compensation_hip_pitch->activity.ConnectTo(fusion_abs_torque_left_hip_y->InputTargetRating(11));
  skill_compensation_hip_pitch->co_torque_hip_y.ConnectTo(fusion_abs_torque_left_hip_y->InputPort(11, 0));

  // Compensation hip roll
  fusion_compensation_hip_roll->activity.ConnectTo(skill_compensation_hip_roll->stimulation);
  this->ci_activity_walking.ConnectTo(skill_compensation_hip_roll->ci_walking);
  this->ci_angle_body_space_lateral.ConnectTo(skill_compensation_hip_roll->ci_angle_body_space_lateral);
  this->ci_angle_trunk_space_lateral_dist.ConnectTo(skill_compensation_hip_roll->ci_angle_trunk_space_lateral_dist);
  this->ci_angle_trunk_space_lateral_prop.ConnectTo(skill_compensation_hip_roll->ci_angle_trunk_space_lateral_prop);
  this->ci_target_angle_trunk_space_lateral.ConnectTo(skill_compensation_hip_roll->ci_target_trunk_space_angle_lateral);
  skill_compensation_hip_roll->activity.ConnectTo(fusion_abs_torque_right_hip_x->InputActivity(5));
  skill_compensation_hip_roll->target_rating.ConnectTo(fusion_abs_torque_right_hip_x->InputTargetRating(5));
  skill_compensation_hip_roll->co_torque_hip_right_x.ConnectTo(fusion_abs_torque_right_hip_x->InputPort(5, 0));

  skill_compensation_hip_roll->activity.ConnectTo(fusion_abs_torque_left_hip_x->InputActivity(5));
  skill_compensation_hip_roll->target_rating.ConnectTo(fusion_abs_torque_left_hip_x->InputTargetRating(5));
  skill_compensation_hip_roll->co_torque_hip_left_x.ConnectTo(fusion_abs_torque_left_hip_x->InputPort(5, 0));

  // Compensation spine pitch
  skill_walking->activity.ConnectTo(skill_compensation_spine_pitch->stimulation);
  this->si_spine_y_angle.ConnectTo(skill_compensation_spine_pitch->si_angle_spine_pitch);
  skill_compensation_spine_pitch->activity.ConnectTo(fusion_abs_torque_spine_y->InputActivity(1));
  skill_compensation_spine_pitch->target_rating.ConnectTo(fusion_abs_torque_spine_y->InputTargetRating(1));
  skill_compensation_spine_pitch->co_torque_spine_y.ConnectTo(fusion_abs_torque_spine_y->InputPort(1, 0));

  // walking initialization
  // left
  this->ci_left_stimulation_walking_initiation.ConnectTo(skill_walking_init_left->stimulation);

  skill_walking_init_left->co_target_activity[0].ConnectTo(fusion_lateral_hip_shift_right->InputActivity(2));
  skill_walking_init_left->co_target_activity[0].ConnectTo(fusion_lateral_hip_shift_right->InputTargetRating(2));
  skill_walking_init_left->co_target_activity[0].ConnectTo(fusion_lateral_hip_shift_right->InputPort(2, 0));


//  skill_walking_init_left->activity.ConnectTo(active_hip_swing_left_init->stimulation);
//  this->ci_walking_scale_factor.ConnectTo(active_hip_swing_left_init->ci_scale);

  //lateral_hip_shift_right->activity.ConnectTo(skill_walking_init_left->stimulation);
  //lateral_hip_shift_right->target_rating.ConnectTo(skill_walking_init_left->si_target_rating[0]);

  skill_walking_init_left->co_target_activity[2].ConnectTo(fusion_upright_trunk_left->InputActivity(0));
  skill_walking_init_left->co_target_activity[2].ConnectTo(fusion_upright_trunk_left->InputTargetRating(0));//fixme
  skill_walking_init_left->co_target_output[2].ConnectTo(fusion_upright_trunk_left->InputPort(0, 0));

  skill_walking_init_left->co_target_activity[3].ConnectTo(fusion_upright_trunk_right->InputActivity(0));
  skill_walking_init_left->co_target_activity[3].ConnectTo(fusion_upright_trunk_right->InputTargetRating(0));
  skill_walking_init_left->co_target_output[3].ConnectTo(fusion_upright_trunk_right->InputPort(0, 0));


  skill_walking_init_left->activity.ConnectTo(lateral_foot_placement_left->inhibition[0]);


  // right
  this->ci_right_stimulation_walking_initiation.ConnectTo(skill_walking_init_right->stimulation);

  skill_walking_init_right->co_target_activity[0].ConnectTo(fusion_lateral_hip_shift_left->InputActivity(0));
  skill_walking_init_right->co_target_activity[0].ConnectTo(fusion_lateral_hip_shift_left->InputTargetRating(0));
  this->ci_walking_scale_factor.ConnectTo(fusion_lateral_hip_shift_left->InputPort(0, 0));

  //lateral_hip_shift_left->activity.ConnectTo(skill_walking_init_right->stimulation);
  //lateral_hip_shift_left->target_rating.ConnectTo(skill_walking_init_right->si_target_rating[0]);

  skill_walking_init_right->co_target_activity[2].ConnectTo(fusion_upright_trunk_left->InputActivity(1));
  skill_walking_init_right->co_target_activity[2].ConnectTo(fusion_upright_trunk_left->InputTargetRating(1));//fixme
  skill_walking_init_right->co_target_output[2].ConnectTo(fusion_upright_trunk_left->InputPort(1, 0));


  skill_walking_init_right->co_target_activity[3].ConnectTo(fusion_upright_trunk_right->InputActivity(1));
  skill_walking_init_right->co_target_activity[3].ConnectTo(fusion_upright_trunk_right->InputTargetRating(1));
  skill_walking_init_right->co_target_output[3].ConnectTo(fusion_upright_trunk_right->InputPort(1, 0));


  skill_walking_init_right->activity.ConnectTo(lateral_foot_placement_right->inhibition[0]);


  // walking lateral initiation
  // left
  this->ci_left_stimulation_walking_lateral_initiation.ConnectTo(skill_walking_lateral_init_left->stimulation);
  skill_walking_lateral_init_left->co_target_activity[0].ConnectTo(lateral_hip_shift_init_left->stimulation);
  skill_walking_lateral_init_left->activity.ConnectTo(active_hip_swing_init_left->stimulation);

  skill_walking_lateral_init_left->co_target_activity[3].ConnectTo(fusion_abs_torque_left_hip_y->InputActivity(10));
  skill_walking_lateral_init_left->co_target_activity[3].ConnectTo(fusion_abs_torque_left_hip_y->InputTargetRating(10));
  skill_walking_lateral_init_left->co_target_output[3].ConnectTo(fusion_abs_torque_left_hip_y->InputPort(10, 0));

  skill_walking_lateral_init_left->co_target_activity[1].ConnectTo(fusion_upright_trunk_left->InputActivity(2));
  skill_walking_lateral_init_left->co_target_activity[1].ConnectTo(fusion_upright_trunk_left->InputTargetRating(2));//fixme
  skill_walking_lateral_init_left->co_target_output[1].ConnectTo(fusion_upright_trunk_left->InputPort(2, 0));

  skill_walking_lateral_init_left->co_target_activity[2].ConnectTo(fusion_upright_trunk_right->InputActivity(2));
  skill_walking_lateral_init_left->co_target_activity[2].ConnectTo(fusion_upright_trunk_right->InputTargetRating(2));
  skill_walking_lateral_init_left->co_target_output[2].ConnectTo(fusion_upright_trunk_right->InputPort(2, 0));
  // right
  this->ci_right_stimulation_walking_lateral_initiation.ConnectTo(skill_walking_lateral_init_right->stimulation);
  skill_walking_lateral_init_right->activity.ConnectTo(lateral_hip_shift_init_right->stimulation);
  skill_walking_lateral_init_right->activity.ConnectTo(active_hip_swing_init_right->stimulation);

  skill_walking_lateral_init_right->co_target_activity[0].ConnectTo(fusion_lateral_hip_shift_right->InputActivity(0));
  skill_walking_lateral_init_right->co_target_activity[0].ConnectTo(fusion_lateral_hip_shift_right->InputTargetRating(0));
  skill_walking_lateral_init_right->co_target_output[0].ConnectTo(fusion_lateral_hip_shift_right->InputPort(0, 0));


  skill_walking_lateral_init_right->co_target_activity[3].ConnectTo(fusion_abs_torque_right_hip_y->InputActivity(10));
  skill_walking_lateral_init_right->co_target_activity[3].ConnectTo(fusion_abs_torque_right_hip_y->InputTargetRating(10));
  skill_walking_lateral_init_right->co_target_output[3].ConnectTo(fusion_abs_torque_right_hip_y->InputPort(10, 0));

  skill_walking_lateral_init_right->co_target_activity[1].ConnectTo(fusion_upright_trunk_left->InputActivity(3));
  skill_walking_lateral_init_right->co_target_activity[1].ConnectTo(fusion_upright_trunk_left->InputTargetRating(3));//fixme
  skill_walking_lateral_init_right->co_target_output[1].ConnectTo(fusion_upright_trunk_left->InputPort(3, 0));


  skill_walking_lateral_init_right->co_target_activity[2].ConnectTo(fusion_upright_trunk_right->InputActivity(3));
  skill_walking_lateral_init_right->co_target_activity[2].ConnectTo(fusion_upright_trunk_right->InputTargetRating(3));
  skill_walking_lateral_init_right->co_target_output[2].ConnectTo(fusion_upright_trunk_right->InputPort(3, 0));



  // walking all phases
  this->ci_activity_walking.ConnectTo(skill_walking->stimulation);
  this->ci_reset_simulation_experiments.ConnectTo(skill_walking->ci_reset_simulation_experiments);

  skill_walking->co_target_activity[0].ConnectTo(fusion_abs_angle_spine_x->InputActivity(1));
  skill_walking->target_rating.ConnectTo(fusion_abs_angle_spine_x->InputTargetRating(1));
  skill_walking->co_target_output[0].ConnectTo(fusion_abs_angle_spine_x->InputPort(1, 0));

  fusion_abs_angle_spine_x->target_rating.ConnectTo(skill_walking->si_target_rating[0]);

  skill_walking->co_target_activity[1].ConnectTo(fusion_abs_angle_spine_y->InputActivity(1));
  skill_walking->target_rating.ConnectTo(fusion_abs_angle_spine_y->InputTargetRating(1));
  skill_walking->co_target_output[1].ConnectTo(fusion_abs_angle_spine_y->InputPort(1, 0));

  fusion_abs_angle_spine_y->target_rating.ConnectTo(skill_walking->si_target_rating[1]);

  skill_walking->co_target_activity[2].ConnectTo(fusion_abs_angle_spine_z->InputActivity(1));
  skill_walking->target_rating.ConnectTo(fusion_abs_angle_spine_z->InputTargetRating(1));
  skill_walking->co_target_output[2].ConnectTo(fusion_abs_angle_spine_z->InputPort(1, 0));

  fusion_abs_angle_spine_z->target_rating.ConnectTo(skill_walking->si_target_rating[2]);

  skill_walking->co_target_activity[3].ConnectTo(fusion_abs_angle_left_hip_x->InputActivity(4));
  skill_walking->target_rating.ConnectTo(fusion_abs_angle_left_hip_x->InputTargetRating(4));
  skill_walking->co_target_output[3].ConnectTo(fusion_abs_angle_left_hip_x->InputPort(4, 0));

  skill_walking->co_target_activity[3].ConnectTo(fusion_abs_angle_right_hip_x->InputActivity(4));
  skill_walking->target_rating.ConnectTo(fusion_abs_angle_right_hip_x->InputTargetRating(4));
  skill_walking->co_target_output[3].ConnectTo(fusion_abs_angle_right_hip_x->InputPort(4, 0));

  skill_walking->co_target_activity[4].ConnectTo(fusion_abs_angle_left_hip_z->InputActivity(2));
  skill_walking->target_rating.ConnectTo(fusion_abs_angle_left_hip_z->InputTargetRating(2));
  skill_walking->co_target_output[4].ConnectTo(fusion_abs_angle_left_hip_z->InputPort(2, 0));

  skill_walking->co_target_activity[4].ConnectTo(fusion_abs_angle_right_hip_z->InputActivity(2));
  skill_walking->target_rating.ConnectTo(fusion_abs_angle_right_hip_z->InputTargetRating(2));
  skill_walking->co_target_output[4].ConnectTo(fusion_abs_angle_right_hip_z->InputPort(2, 0));

  skill_walking->co_target_activity[5].ConnectTo(hold_joint_pos_hip_x_left->stimulation);
  skill_walking->co_target_output[5].ConnectTo(hold_joint_pos_hip_x_left->ci_stiffness_factor);

  skill_walking->co_target_activity[5].ConnectTo(hold_joint_pos_hip_x_right->stimulation);
  skill_walking->co_target_output[5].ConnectTo(hold_joint_pos_hip_x_right->ci_stiffness_factor);

  skill_walking->co_target_activity[6].ConnectTo(balance_spine_x->stimulation);
  balance_spine_x->target_rating.ConnectTo(skill_walking->si_target_rating[3]);

  skill_walking->co_target_activity[7].ConnectTo(fusion_upright_trunk_left->InputActivity(4));
  skill_walking->co_target_activity[7].ConnectTo(fusion_upright_trunk_left->InputTargetRating(4));//fixme
  skill_walking->co_target_output[7].ConnectTo(fusion_upright_trunk_left->InputPort(4, 0));


  fusion_upright_trunk_left->target_rating.ConnectTo(skill_walking->si_target_rating[4]);

  skill_walking->co_target_activity[8].ConnectTo(fusion_upright_trunk_right->InputActivity(4));
  skill_walking->co_target_activity[8].ConnectTo(fusion_upright_trunk_right->InputTargetRating(4));
  skill_walking->co_target_output[8].ConnectTo(fusion_upright_trunk_right->InputPort(4, 0));


  fusion_upright_trunk_right->target_rating.ConnectTo(skill_walking->si_target_rating[5]);

  // walking phase 1
  // left
  fusion_walking_termi_left->co_walking_phase_1.ConnectTo(skill_walking_phase1_left->stimulation);

  skill_walking_phase1_left->co_target_activity[0].ConnectTo(fusion_control_pelvis_roll_left->InputActivity(0));
  skill_walking_phase1_left->co_target_activity[0].ConnectTo(fusion_control_pelvis_roll_left->InputTargetRating(0));
  skill_walking_phase1_left->co_target_output[0].ConnectTo(fusion_control_pelvis_roll_left->InputPort(0, 0));


  //control_pelvis_roll_left->target_rating.ConnectTo(skill_walking_phase1_left->si_target_rating[0]);

  skill_walking_phase1_left->co_target_activity[1].ConnectTo(fusion_mp_turn_left_spine->InputActivity(0));
  skill_walking_phase1_left->co_target_activity[1].ConnectTo(fusion_mp_turn_left_spine->InputTargetRating(0));
  skill_walking_phase1_left->co_target_output[1].ConnectTo(fusion_mp_turn_left_spine->InputPort(0, 0));


  skill_walking_phase1_left->co_target_activity[3].ConnectTo(fusion_control_pelvis_yaw_left->InputActivity(0));
  skill_walking_phase1_left->co_target_activity[3].ConnectTo(fusion_control_pelvis_yaw_left->InputTargetRating(0));
  skill_walking_phase1_left->co_target_output[3].ConnectTo(fusion_control_pelvis_yaw_left->InputPort(0, 0));


  skill_walking_phase1_left->co_target_activity[4].ConnectTo(fusion_brake_torque_left_hip_y->InputActivity(0));
  skill_walking_phase1_left->co_target_output[4].ConnectTo(fusion_brake_torque_left_hip_y->InputPort(0, 0));
  skill_walking_phase1_left->co_target_activity[4].ConnectTo(fusion_brake_torque_left_hip_y->InputTargetRating(0));

  skill_walking_phase1_left->activity.ConnectTo(fusion_compensation_hip_roll->InputActivity(0));
  skill_walking_phase1_left->activity.ConnectTo(fusion_compensation_hip_roll->InputPort(0, 0));
  skill_walking_phase1_left->activity.ConnectTo(fusion_compensation_hip_roll->InputTargetRating(0));



  //this->ci_push_recovery_stand.ConnectTo(fusion_brake_torque_left_hip_y->stimulation);

  fusion_brake_torque_left_hip_y->activity.ConnectTo(hip_left_push_recovery_dece->stimulation);
  // right
  fusion_walking_termi_right->co_walking_phase_1.ConnectTo(skill_walking_phase1_right->stimulation);

  skill_walking_phase1_right->co_target_activity[0].ConnectTo(fusion_control_pelvis_roll_right->InputActivity(0));
  skill_walking_phase1_right->co_target_activity[0].ConnectTo(fusion_control_pelvis_roll_right->InputTargetRating(0));
  skill_walking_phase1_right->co_target_output[0].ConnectTo(fusion_control_pelvis_roll_right->InputPort(0, 0));


  //control_pelvis_roll_right->target_rating.ConnectTo(skill_walking_phase1_right->si_target_rating[0]);

  skill_walking_phase1_right->co_target_activity[1].ConnectTo(fusion_mp_turn_right_spine->InputActivity(0));
  skill_walking_phase1_right->co_target_activity[1].ConnectTo(fusion_mp_turn_right_spine->InputTargetRating(0));
  skill_walking_phase1_right->co_target_output[1].ConnectTo(fusion_mp_turn_right_spine->InputPort(0, 0));

  skill_walking_phase1_right->activity.ConnectTo(fusion_compensation_hip_roll->InputActivity(1));
  skill_walking_phase1_right->activity.ConnectTo(fusion_compensation_hip_roll->InputPort(1, 0));
  skill_walking_phase1_right->activity.ConnectTo(fusion_compensation_hip_roll->InputTargetRating(1));

  // walking phase 2
  // left
  fusion_walking_termi_left->co_walking_phase_2.ConnectTo(skill_walking_phase2_left->stimulation);

  skill_walking_phase2_left->co_target_activity[0].ConnectTo(fusion_active_hip_swing_left->InputActivity(0));
  skill_walking_phase2_left->co_target_activity[0].ConnectTo(fusion_active_hip_swing_left->InputTargetRating(0));//fixme
  skill_walking_phase2_left->co_target_output[0].ConnectTo(fusion_active_hip_swing_left->InputPort(0, 0));


  //active_hip_swing_left->target_rating.ConnectTo(skill_walking_phase2_left->si_target_rating[0]);

  skill_walking_phase2_left->co_target_activity[1].ConnectTo(fusion_control_pelvis_roll_left->InputActivity(1));
  skill_walking_phase2_left->co_target_activity[1].ConnectTo(fusion_control_pelvis_roll_left->InputTargetRating(1));
  skill_walking_phase2_left->co_target_output[1].ConnectTo(fusion_control_pelvis_roll_left->InputPort(1, 0));


  //control_pelvis_roll_left->target_rating.ConnectTo(skill_walking_phase2_left->si_target_rating[1]);


  skill_walking_phase2_left->co_target_activity[4].ConnectTo(fusion_control_pelvis_yaw_left->InputActivity(1));
  skill_walking_phase2_left->co_target_activity[4].ConnectTo(fusion_control_pelvis_yaw_left->InputTargetRating(1));
  skill_walking_phase2_left->co_target_output[4].ConnectTo(fusion_control_pelvis_yaw_left->InputPort(1, 0));


  skill_walking_phase2_left->co_target_activity[2].ConnectTo(fusion_mp_turn_left_spine->InputActivity(1));
  skill_walking_phase2_left->co_target_activity[2].ConnectTo(fusion_mp_turn_left_spine->InputTargetRating(1));
  skill_walking_phase2_left->co_target_output[2].ConnectTo(fusion_mp_turn_left_spine->InputPort(1, 0));

  skill_walking_phase2_left->activity.ConnectTo(fusion_compensation_hip_roll->InputActivity(2));
  skill_walking_phase2_left->activity.ConnectTo(fusion_compensation_hip_roll->InputPort(2, 0));
  skill_walking_phase2_left->activity.ConnectTo(fusion_compensation_hip_roll->InputTargetRating(2));

  // right

  fusion_walking_termi_right->co_walking_phase_2.ConnectTo(skill_walking_phase2_right->stimulation);

  skill_walking_phase2_right->co_target_activity[0].ConnectTo(fusion_active_hip_swing_right->InputActivity(0));
  skill_walking_phase2_right->co_target_activity[0].ConnectTo(fusion_active_hip_swing_right->InputTargetRating(0));//fixme
  skill_walking_phase2_right->co_target_output[0].ConnectTo(fusion_active_hip_swing_right->InputPort(0, 0));


  //active_hip_swing_right->target_rating.ConnectTo(skill_walking_phase2_right->si_target_rating[0]);

  skill_walking_phase2_right->co_target_activity[1].ConnectTo(fusion_control_pelvis_roll_right->InputActivity(1));
  skill_walking_phase2_right->co_target_activity[1].ConnectTo(fusion_control_pelvis_roll_right->InputTargetRating(1));
  skill_walking_phase2_right->co_target_output[1].ConnectTo(fusion_control_pelvis_roll_right->InputPort(1, 0));


  //control_pelvis_roll_right->target_rating.ConnectTo(skill_walking_phase2_right->si_target_rating[1]);


  skill_walking_phase2_right->co_target_activity[4].ConnectTo(fusion_control_pelvis_yaw_right->InputActivity(0));
  skill_walking_phase2_right->co_target_activity[4].ConnectTo(fusion_control_pelvis_yaw_right->InputTargetRating(0));
  skill_walking_phase2_right->co_target_output[4].ConnectTo(fusion_control_pelvis_yaw_right->InputPort(0, 0));


  skill_walking_phase2_right->co_target_activity[2].ConnectTo(fusion_mp_turn_right_spine->InputActivity(1));
  skill_walking_phase2_right->co_target_activity[2].ConnectTo(fusion_mp_turn_right_spine->InputTargetRating(1));
  skill_walking_phase2_right->co_target_output[2].ConnectTo(fusion_mp_turn_right_spine->InputPort(1, 0));

  skill_walking_phase2_right->activity.ConnectTo(fusion_compensation_hip_roll->InputActivity(3));
  skill_walking_phase2_right->activity.ConnectTo(fusion_compensation_hip_roll->InputPort(3, 0));
  skill_walking_phase2_right->activity.ConnectTo(fusion_compensation_hip_roll->InputTargetRating(3));

  // walking phase 3
  // left
  fusion_walking_termi_left->co_walking_phase_3.ConnectTo(skill_walking_phase3_left->stimulation);

  skill_walking_phase3_left->co_target_activity[0].ConnectTo(fusion_lateral_hip_shift_right->InputActivity(1));
  skill_walking_phase3_left->co_target_activity[0].ConnectTo(fusion_lateral_hip_shift_right->InputTargetRating(1));
  skill_walking_phase3_left->co_target_output[0].ConnectTo(fusion_lateral_hip_shift_right->InputPort(1, 0));

  //lateral_hip_shift_right->target_rating.ConnectTo(skill_walking_phase3_left->si_target_rating[0]);

  skill_walking_phase3_left->co_target_activity[1].ConnectTo(fusion_active_hip_swing_left->InputActivity(1));
  skill_walking_phase3_left->co_target_activity[1].ConnectTo(fusion_active_hip_swing_left->InputTargetRating(1));//fixme
  skill_walking_phase3_left->co_target_output[1].ConnectTo(fusion_active_hip_swing_left->InputPort(1, 0));


  //active_hip_swing_left->target_rating.ConnectTo(skill_walking_phase3_left->si_target_rating[1]);


  skill_walking_phase3_left->co_target_activity[2].ConnectTo(fusion_mp_turn_left_left->InputActivity(0));
  skill_walking_phase3_left->target_rating.ConnectTo(fusion_mp_turn_left_left->InputTargetRating(0));
  skill_walking_phase3_left->co_target_output[2].ConnectTo(fusion_mp_turn_left_left->InputPort(0, 0));


  skill_walking_phase3_left->co_target_activity[3].ConnectTo(fusion_mp_turn_right_left->InputActivity(0));
  skill_walking_phase3_left->target_rating.ConnectTo(fusion_mp_turn_right_left->InputTargetRating(0));
  skill_walking_phase3_left->co_target_output[3].ConnectTo(fusion_mp_turn_right_left->InputPort(0, 0));


  // right
  fusion_walking_termi_right->co_walking_phase_3.ConnectTo(skill_walking_phase3_right->stimulation);

  skill_walking_phase3_right->co_target_activity[0].ConnectTo(fusion_lateral_hip_shift_left->InputActivity(1));
  skill_walking_phase3_right->co_target_activity[0].ConnectTo(fusion_lateral_hip_shift_left->InputTargetRating(1));
  skill_walking_phase3_right->co_target_output[0].ConnectTo(fusion_lateral_hip_shift_left->InputPort(1, 0));

  //lateral_hip_shift_right->target_rating.ConnectTo(skill_walking_phase3_right->si_target_rating[0]);

  skill_walking_phase3_right->co_target_activity[1].ConnectTo(fusion_active_hip_swing_right->InputActivity(1));
  skill_walking_phase3_right->co_target_activity[1].ConnectTo(fusion_active_hip_swing_right->InputTargetRating(1));//fixme
  skill_walking_phase3_right->co_target_output[1].ConnectTo(fusion_active_hip_swing_right->InputPort(1, 0));


  //active_hip_swing_right->target_rating.ConnectTo(skill_walking_phase3_right->si_target_rating[1]);


  skill_walking_phase3_right->co_target_activity[2].ConnectTo(fusion_mp_turn_left_right->InputActivity(0));
  skill_walking_phase3_right->target_rating.ConnectTo(fusion_mp_turn_left_right->InputTargetRating(0));
  skill_walking_phase3_right->co_target_output[2].ConnectTo(fusion_mp_turn_left_right->InputPort(0, 0));


  skill_walking_phase3_right->co_target_activity[3].ConnectTo(fusion_mp_turn_right_right->InputActivity(0));
  skill_walking_phase3_right->target_rating.ConnectTo(fusion_mp_turn_right_right->InputTargetRating(0));
  skill_walking_phase3_right->co_target_output[3].ConnectTo(fusion_mp_turn_right_right->InputPort(0, 0));



  // walking phase 4
  // left
  fusion_walking_termi_left->co_walking_phase_4.ConnectTo(skill_walking_phase4_left->stimulation);

  skill_walking_phase4_left->co_target_activity[1].ConnectTo(fusion_active_hip_swing_left->InputActivity(2));
  skill_walking_phase4_left->co_target_activity[1].ConnectTo(fusion_active_hip_swing_left->InputTargetRating(2));//fixme
  skill_walking_phase4_left->co_target_output[1].ConnectTo(fusion_active_hip_swing_left->InputPort(2, 0));


// active_hip_swing_left->target_rating.ConnectTo(skill_walking_phase4_left->si_target_rating[1]);

  skill_walking_phase4_left->co_target_activity[2].ConnectTo(fusion_lock_hip_left->InputActivity(0));
  skill_walking_phase4_left->co_target_activity[2].ConnectTo(fusion_lock_hip_left->InputTargetRating(0));
  skill_walking_phase4_left->co_target_output[2].ConnectTo(fusion_lock_hip_left->InputPort(0, 0));



  //lock_hip_left->target_rating.ConnectTo(skill_walking_phase4_left->si_target_rating[2]);

  skill_walking_phase4_left->co_target_activity[3].ConnectTo(fusion_lateral_foot_placement_left->InputActivity(0));
  skill_walking_phase4_left->co_target_activity[3].ConnectTo(fusion_lateral_foot_placement_left->InputTargetRating(0));
  skill_walking_phase4_left->co_target_output[3].ConnectTo(fusion_lateral_foot_placement_left->InputPort(0, 0));


  //lateral_foot_placement_left->target_rating.ConnectTo(skill_walking_phase4_left->si_target_rating[3]);

  skill_walking_phase4_left->co_target_activity[4].ConnectTo(fusion_mp_turn_left_left->InputActivity(1));
  skill_walking_phase4_left->target_rating.ConnectTo(fusion_mp_turn_left_left->InputTargetRating(1));
  skill_walking_phase4_left->co_target_output[4].ConnectTo(fusion_mp_turn_left_left->InputPort(1, 0));


  skill_walking_phase4_left->co_target_activity[5].ConnectTo(fusion_mp_turn_right_left->InputActivity(1));
  skill_walking_phase4_left->target_rating.ConnectTo(fusion_mp_turn_right_left->InputTargetRating(1));
  skill_walking_phase4_left->co_target_output[5].ConnectTo(fusion_mp_turn_right_left->InputPort(1, 0));

  skill_walking_phase4_left->activity.ConnectTo(hip_swing_curve_walking_left->stimulation);


  // right
  fusion_walking_termi_right->co_walking_phase_4.ConnectTo(skill_walking_phase4_right->stimulation);

  skill_walking_phase4_right->co_target_activity[1].ConnectTo(fusion_active_hip_swing_right->InputActivity(2));
  skill_walking_phase4_right->co_target_activity[1].ConnectTo(fusion_active_hip_swing_right->InputTargetRating(2));//fixme
  skill_walking_phase4_right->co_target_output[1].ConnectTo(fusion_active_hip_swing_right->InputPort(2, 0));


  //active_hip_swing_right->target_rating.ConnectTo(skill_walking_phase4_right->si_target_rating[1]);

  skill_walking_phase4_right->co_target_activity[2].ConnectTo(fusion_lock_hip_right->InputActivity(0));
  skill_walking_phase4_right->co_target_activity[2].ConnectTo(fusion_lock_hip_right->InputTargetRating(0));
  skill_walking_phase4_right->co_target_output[2].ConnectTo(fusion_lock_hip_right->InputPort(0, 0));

  //lock_hip_right->target_rating.ConnectTo(skill_walking_phase4_right->si_target_rating[2]);

  skill_walking_phase4_right->co_target_activity[3].ConnectTo(fusion_lateral_foot_placement_right->InputActivity(0));
  skill_walking_phase4_right->co_target_activity[3].ConnectTo(fusion_lateral_foot_placement_right->InputTargetRating(0));
  skill_walking_phase4_right->co_target_output[3].ConnectTo(fusion_lateral_foot_placement_right->InputPort(0, 0));

  skill_walking_phase4_right->activity.ConnectTo(hip_swing_curve_walking_right->stimulation);
  //lateral_foot_placement_right->target_rating.ConnectTo(skill_walking_phase4_right->si_target_rating[3]);

  skill_walking_phase4_right->co_target_activity[4].ConnectTo(fusion_mp_turn_left_right->InputActivity(1));
  skill_walking_phase4_right->target_rating.ConnectTo(fusion_mp_turn_left_right->InputTargetRating(1));
  skill_walking_phase4_right->co_target_output[4].ConnectTo(fusion_mp_turn_left_right->InputPort(1, 0));


  skill_walking_phase4_right->co_target_activity[5].ConnectTo(fusion_mp_turn_right_right->InputActivity(1));
  skill_walking_phase4_right->target_rating.ConnectTo(fusion_mp_turn_right_right->InputTargetRating(1));
  skill_walking_phase4_right->co_target_output[4].ConnectTo(fusion_mp_turn_right_right->InputPort(1, 0));


  // walking phase 5
  // left
  fusion_walking_termi_left->co_walking_phase_5.ConnectTo(skill_walking_phase5_left->stimulation);

  skill_walking_phase5_left->co_target_activity[0].ConnectTo(fusion_abs_angle_left_hip_y->InputActivity(2));
  skill_walking_phase5_left->target_rating.ConnectTo(fusion_abs_angle_left_hip_y->InputTargetRating(2));
  skill_walking_phase5_left->co_target_output[0].ConnectTo(fusion_abs_angle_left_hip_y->InputPort(2, 0));

  skill_walking_phase5_left->co_target_activity[3].ConnectTo(fusion_lock_hip_left->InputActivity(1));
  skill_walking_phase5_left->co_target_activity[3].ConnectTo(fusion_lock_hip_left->InputTargetRating(1));
  skill_walking_phase5_left->co_target_output[3].ConnectTo(fusion_lock_hip_left->InputPort(1, 0));


  //lock_hip_left->target_rating.ConnectTo(skill_walking_phase5_left->si_target_rating[3]);

  skill_walking_phase5_left->co_target_activity[4].ConnectTo(fusion_lateral_foot_placement_left->InputActivity(1));
  skill_walking_phase5_left->co_target_activity[4].ConnectTo(fusion_lateral_foot_placement_left->InputTargetRating(1));
  skill_walking_phase5_left->co_target_output[4].ConnectTo(fusion_lateral_foot_placement_left->InputPort(1, 0));

  //lateral_foot_placement_left->target_rating.ConnectTo(skill_walking_phase5_left->si_target_rating[4]);

  // right
  fusion_walking_termi_right->co_walking_phase_5.ConnectTo(skill_walking_phase5_right->stimulation);

  skill_walking_phase5_right->co_target_activity[0].ConnectTo(fusion_abs_angle_right_hip_y->InputActivity(2));
  skill_walking_phase5_right->target_rating.ConnectTo(fusion_abs_angle_right_hip_y->InputTargetRating(2));
  skill_walking_phase5_right->co_target_output[0].ConnectTo(fusion_abs_angle_right_hip_y->InputPort(2, 0));

  skill_walking_phase5_right->co_target_activity[3].ConnectTo(fusion_lock_hip_right->InputActivity(1));
  skill_walking_phase5_right->co_target_activity[3].ConnectTo(fusion_lock_hip_right->InputTargetRating(1));
  skill_walking_phase5_right->co_target_output[3].ConnectTo(fusion_lock_hip_right->InputPort(1, 0));


  //lock_hip_right->target_rating.ConnectTo(skill_walking_phase5_right->si_target_rating[3]);

  skill_walking_phase5_right->co_target_activity[4].ConnectTo(fusion_lateral_foot_placement_right->InputActivity(1));
  skill_walking_phase5_right->co_target_activity[4].ConnectTo(fusion_lateral_foot_placement_right->InputTargetRating(1));
  skill_walking_phase5_right->co_target_output[4].ConnectTo(fusion_lateral_foot_placement_right->InputPort(1, 0));

  //lateral_foot_placement_right->target_rating.ConnectTo(skill_walking_phase5_right->si_target_rating[4]);

}

//----------------------------------------------------------------------
// gLowerTrunkGroup destructor
//----------------------------------------------------------------------
gLowerTrunkGroup::~gLowerTrunkGroup()
{}

//----------------------------------------------------------------------
// End of namespace declaration
//----------------------------------------------------------------------
}
}
}
