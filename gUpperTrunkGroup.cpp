//
// You received this file as part of Finroc
// A Framework for intelligent robot control
//
// Copyright (C) Finroc GbR (finroc.org)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//----------------------------------------------------------------------
/*!\file    projects/biped/control/gUpperTrunkGroup.cpp
 *
 * \author  Tobias Luksch & Jie Zhao
 *
 * \date    2013-10-09
 *
 */
//----------------------------------------------------------------------
#include "projects/biped/control/gUpperTrunkGroup.h"
#include "plugins/ib2c/mbbFusion.h"

#include "projects/biped/control/gbbUpperTrunkHoldPosition.h"
#include "projects/biped/reflexes/mbbLockArm.h"

#include "projects/biped/skills/mbbTerminationFusion.h"
#include "projects/biped/skills/mbbUpperTrunkStand.h"
#include "projects/biped/skills/mbbStimulationSkill.h"
#include "projects/biped/reflexes/mbbAdjustJointAngle.h"
//#include "projects/biped/reflexes/mbbBalanceArm.h"
#include "projects/biped/motor_patterns/mbbActiveArmSwing.h"
#include "projects/biped/utils/mMemoryUnit.h"

#include "projects/biped/dec/mbbCompensationShoulderPitch.h"
#include "projects/biped/dec/mbbCompensationShoulderRoll.h"
//----------------------------------------------------------------------
// External includes (system with <>, local with "")
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Internal includes with ""
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Debugging
//----------------------------------------------------------------------
#include <cassert>

//----------------------------------------------------------------------
// Namespace usage
//----------------------------------------------------------------------
using namespace finroc::ib2c;
using namespace finroc::biped::skills;
using namespace finroc::biped::reflexes;
using namespace finroc::biped::motor_patterns;
using namespace finroc::biped::utils;
using namespace finroc::biped::dec;
//----------------------------------------------------------------------
// Namespace declaration
//----------------------------------------------------------------------
namespace finroc
{
namespace biped
{
namespace control
{

//----------------------------------------------------------------------
// Forward declarations / typedefs / enums
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Const values
//----------------------------------------------------------------------
static runtime_construction::tStandardCreateModuleAction<gUpperTrunkGroup> cCREATE_ACTION_FOR_G_UPPERTRUNKGROUP("UpperTrunkGroup");

//----------------------------------------------------------------------
// Implementation
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// gUpperTrunkGroup constructor
//----------------------------------------------------------------------
gUpperTrunkGroup::gUpperTrunkGroup(core::tFrameworkElement *parent, const std::string &name,
                                   const std::string &structure_config_file) :
  tSenseControlGroup(parent, name, structure_config_file, true) // change to 'true' to make group's ports shared (so that ports in other processes can connect to its sensor outputs and controller inputs)
{

  // add modules
  // ============================================

  //BEGIN fusion behaviours
  //for the walking and hold position
  // left arm
  ib2c::mbbFusion<double> * fusion_abs_angle_left_shoulder_x = new ib2c::mbbFusion<double>(this, "Abs. Angle Left Shoulder X", 3, tStimulationMode::ENABLED);
  fusion_abs_angle_left_shoulder_x->fusion_method.Set(ib2c::tFusionMethod::WEIGHTED_AVERAGE);
  //fusion_abs_angle_left_shoulder_x->number_of_inhibition_ports.Set(1); need to be set if fusion_abs_torque_left_shoulder_x is constructed
  fusion_abs_angle_left_shoulder_x->CheckStaticParameters();
  ib2c::mbbFusion<double> * fusion_abs_angle_left_shoulder_y = new ib2c::mbbFusion<double>(this, "Abs. Angle Left Shoulder Y", 4, tStimulationMode::ENABLED, 1);
  fusion_abs_angle_left_shoulder_y->number_of_inhibition_ports.Set(1);
  fusion_abs_angle_left_shoulder_y->CheckStaticParameters();
  fusion_abs_angle_left_shoulder_y->fusion_method.Set(ib2c::tFusionMethod::WEIGHTED_AVERAGE);
  ib2c::mbbFusion<double> * fusion_abs_angle_left_elbow = new ib2c::mbbFusion<double>(this, "Abs. Angle Left Elbow", 3, tStimulationMode::ENABLED);
  fusion_abs_angle_left_elbow->fusion_method.Set(ib2c::tFusionMethod::WEIGHTED_AVERAGE);
//  fusion_abs_angle_left_elbow->number_of_inhibition_ports.Set(1); need to be set if fusion_abs_torque_left_elbow is constructed
  fusion_abs_angle_left_elbow->CheckStaticParameters();


  ib2c::mbbFusion<double> * fusion_abs_torque_left_shoulder_x = new ib2c::mbbFusion<double>(this, "Abs. Torque Left Shoulder X" , 1, tStimulationMode::ENABLED);
  fusion_abs_torque_left_shoulder_x->fusion_method.Set(ib2c::tFusionMethod::WEIGHTED_SUM);




//  fusion_abs_torque_left_shoulder_x->number_of_inhibition_ports.Set(1);
//  fusion_abs_torque_left_shoulder_x->CheckStaticParameters();
  ib2c::mbbFusion<double> * fusion_abs_torque_left_shoulder_y = new ib2c::mbbFusion<double>(this, "Abs. Torque Left Shoulder Y", 4, tStimulationMode::ENABLED);
  fusion_abs_torque_left_shoulder_y->fusion_method.Set(ib2c::tFusionMethod::WEIGHTED_SUM);
  fusion_abs_torque_left_shoulder_y->number_of_inhibition_ports.Set(1);
  fusion_abs_torque_left_shoulder_y->CheckStaticParameters();

  mMemoryUnit * memory_abs_torque_left_shoulder_y = new mMemoryUnit(this, "Memory Torque Left Shoulder Y");

//  ib2c::mbbFusion<double> * fusion_abs_torque_left_elbow = new ib2c::mbbFusion<double>(this, "Abs. Torque Left Elbow", 1, tStimulationMode::ENABLED);
//  fusion_abs_torque_left_elbow->fusion_method.Set(ib2c::tFusionMethod::WEIGHTED_SUM);
//  fusion_abs_torque_left_elbow->number_of_inhibition_ports.Set(1);
//  fusion_abs_torque_left_elbow->CheckStaticParameters();
//  this->ci_for_non_used_fusion_modules.ConnectTo(fusion_abs_torque_left_elbow->InputActivity(0));
//  this->ci_for_non_used_fusion_modules.ConnectTo(fusion_abs_torque_left_elbow->InputTargetRating(0));
//  this->ci_for_non_used_fusion_modules.ConnectTo(fusion_abs_torque_left_elbow->InputPort(0, 0));

//  mbbFusion<double>  * fusion_rel_angle_left_shoulder_x = new mbbFusion<double> (this, "Rel. Angle Left Shoulder X", 1, tStimulationMode::ENABLED, 0);
//  fusion_rel_angle_left_shoulder_x->fusion_method.Set(ib2c::tFusionMethod::WEIGHTED_AVERAGE);
//  mbbFusion<double>  * fusion_rel_angle_left_shoulder_y = new mbbFusion<double> (this, "Rel. Angle Left Shoulder Y", 1, tStimulationMode::ENABLED, 0);
//  fusion_rel_angle_left_shoulder_y->fusion_method.Set(ib2c::tFusionMethod::WEIGHTED_AVERAGE);
//  mbbFusion<double>  * fusion_rel_angle_left_elbow = new mbbFusion<double> (this, "Rel. Angle Left Elbow", 1, tStimulationMode::ENABLED, 0);
//  fusion_rel_angle_left_elbow->fusion_method.Set(ib2c::tFusionMethod::WEIGHTED_AVERAGE);

  // right arm
  ib2c::mbbFusion<double> * fusion_abs_angle_right_shoulder_x = new ib2c::mbbFusion<double>(this, "Abs. Angle Right Shoulder X", 2, tStimulationMode::ENABLED, 1);
  fusion_abs_angle_right_shoulder_x->fusion_method.Set(ib2c::tFusionMethod::WEIGHTED_AVERAGE);
//  fusion_abs_angle_right_shoulder_x->number_of_inhibition_ports.Set(1);
  fusion_abs_angle_right_shoulder_x->CheckStaticParameters();
  ib2c::mbbFusion<double> * fusion_abs_angle_right_shoulder_y = new ib2c::mbbFusion<double>(this, "Abs. Angle Right Shoulder Y", 4, tStimulationMode::ENABLED, 1);
  fusion_abs_angle_right_shoulder_y->fusion_method.Set(ib2c::tFusionMethod::WEIGHTED_AVERAGE);
  fusion_abs_angle_right_shoulder_y->number_of_inhibition_ports.Set(1);
  fusion_abs_angle_right_shoulder_y->CheckStaticParameters();
  ib2c::mbbFusion<double> * fusion_abs_angle_right_elbow = new  ib2c::mbbFusion<double>(this, "Abs. Angle Right Elbow", 3, tStimulationMode::ENABLED);
  fusion_abs_angle_right_elbow->fusion_method.Set(ib2c::tFusionMethod::WEIGHTED_AVERAGE);
//  fusion_abs_angle_right_elbow->number_of_inhibition_ports.Set(1);
  fusion_abs_angle_right_elbow->CheckStaticParameters();
  ib2c::mbbFusion<double> * fusion_abs_torque_right_shoulder_x = new ib2c::mbbFusion<double>(this, "Abs. Torque Right Shoulder X", 1, tStimulationMode::ENABLED);
  fusion_abs_torque_right_shoulder_x->fusion_method.Set(ib2c::tFusionMethod::WEIGHTED_SUM);
////  fusion_abs_torque_right_shoulder_x->number_of_inhibition_ports.Set(1);
////  fusion_abs_torque_right_shoulder_x->CheckStaticParameters();
//  this->ci_for_non_used_fusion_modules.ConnectTo(fusion_abs_torque_right_shoulder_x->InputActivity(0));
//  this->ci_for_non_used_fusion_modules.ConnectTo(fusion_abs_torque_right_shoulder_x->InputTargetRating(0));
//  this->ci_for_non_used_fusion_modules.ConnectTo(fusion_abs_torque_right_shoulder_x->InputPort(0, 0));

  ib2c::mbbFusion<double> * fusion_abs_torque_right_shoulder_y = new ib2c::mbbFusion<double>(this, "Abs. Torque Right Shoulder Y", 4, tStimulationMode::ENABLED, 1);
  fusion_abs_torque_right_shoulder_y->fusion_method.Set(ib2c::tFusionMethod::WEIGHTED_SUM);
  fusion_abs_torque_right_shoulder_y->number_of_inhibition_ports.Set(1);
  fusion_abs_torque_right_shoulder_y->CheckStaticParameters();
//  ib2c::mbbFusion<double> * fusion_abs_torque_right_elbow = new ib2c::mbbFusion<double>(this, "Abs. Torque Right Elbow", 1, tStimulationMode::ENABLED);
//  fusion_abs_torque_right_elbow->fusion_method.Set(ib2c::tFusionMethod::WEIGHTED_SUM);
////  fusion_abs_torque_right_elbow->number_of_inhibition_ports.Set(1);
////  fusion_abs_torque_right_elbow->CheckStaticParameters();
//  this->ci_for_non_used_fusion_modules.ConnectTo(fusion_abs_torque_right_elbow->InputActivity(0));
//  this->ci_for_non_used_fusion_modules.ConnectTo(fusion_abs_torque_right_elbow->InputTargetRating(0));
//  this->ci_for_non_used_fusion_modules.ConnectTo(fusion_abs_torque_right_elbow->InputPort(0, 0));

//  mbbFusion<double>  * fusion_rel_angle_right_shoulder_x = new mbbFusion<double> (this, "Rel. Angle Right Shoulder X", 1, tStimulationMode::ENABLED, 0);
//  fusion_rel_angle_right_shoulder_x->fusion_method.Set(ib2c::tFusionMethod::WEIGHTED_AVERAGE);
//  mbbFusion<double>  * fusion_rel_angle_right_shoulder_y = new mbbFusion<double> (this, "Rel. Angle Right Shoulder Y", 1, tStimulationMode::ENABLED, 0);
//  fusion_rel_angle_right_shoulder_y->fusion_method.Set(ib2c::tFusionMethod::WEIGHTED_AVERAGE);
//  mbbFusion<double>  * fusion_rel_angle_right_elbow = new mbbFusion<double> (this, "Rel. Angle Right Elbow", 1, tStimulationMode::ENABLED, 0);
//  fusion_rel_angle_right_elbow->fusion_method.Set(ib2c::tFusionMethod::WEIGHTED_AVERAGE);

  mMemoryUnit * memory_abs_torque_right_shoulder_y = new mMemoryUnit(this, "Memory Torque Right Shoulder Y");


  // active arm swing
  ib2c::mbbFusion<double> * fusion_active_arm_swing_left = new ib2c::mbbFusion<double>(this, "Active Arm Swing Left", 2, tStimulationMode::ENABLED);
  fusion_active_arm_swing_left->fusion_method.Set(ib2c::tFusionMethod::WEIGHTED_AVERAGE);
  ib2c::mbbFusion<double> * fusion_active_arm_swing_right = new ib2c::mbbFusion<double>(this, "Active Arm Swing Right", 2, tStimulationMode::ENABLED);
  fusion_active_arm_swing_right->fusion_method.Set(ib2c::tFusionMethod::WEIGHTED_AVERAGE);

  // active arm swing for termination
  ib2c::mbbFusion<double> * fusion_active_arm_swing_left_termi = new ib2c::mbbFusion<double>(this, "Active Arm Swing Left Termination", 2, tStimulationMode::ENABLED);
  fusion_active_arm_swing_left_termi->fusion_method.Set(ib2c::tFusionMethod::WEIGHTED_AVERAGE);
  ib2c::mbbFusion<double> * fusion_active_arm_swing_right_termi = new ib2c::mbbFusion<double>(this, "Active Arm Swing Right Termination", 2, tStimulationMode::ENABLED);
  fusion_active_arm_swing_right_termi->fusion_method.Set(ib2c::tFusionMethod::WEIGHTED_AVERAGE);

  // fusion of lock arm
  ib2c::mbbFusion<double> * fusion_lock_arm_swing = new ib2c::mbbFusion<double>(this, "Lock Arm Swing", 3, tStimulationMode::ENABLED);
  fusion_lock_arm_swing->fusion_method.Set(ib2c::tFusionMethod::WEIGHTED_AVERAGE);

  // reflexes prevent arm overswing
  reflexes::mbbLockArm *lock_arm_swing_left = new reflexes::mbbLockArm(this, "(R) Lock Arm Swing Left", tStimulationMode::AUTO, 0, 0.1);

  reflexes::mbbLockArm *lock_arm_swing_right = new reflexes::mbbLockArm(this, "(R) Lock Arm Swing Right", tStimulationMode::AUTO, 0, 0.1);

  // STANDING CONTROL UNITS
  // ================================================
  mbbUpperTrunkStand* skill_stand = new mbbUpperTrunkStand(this, "(S) Stand");

  // DISTURBANCE ESTIMATION AND COMPENSATION CONTROL
  // ===============================================
  mbbCompensationShoulderPitch* skill_compensation_shoulder_pitch = new mbbCompensationShoulderPitch(this, "(PR) Compensation Shoulder Pitch", tStimulationMode::DISABLED);

  mbbCompensationShoulderRoll* skill_compensation_shoulder_roll = new mbbCompensationShoulderRoll(this, "(PR) Compensation Shoulder Roll", tStimulationMode::DISABLED);

// mbbBalanceArm* balance_arm = new mbbBalanceArm(this, "(R) Balance Arm", tStimulationMode::AUTO);

  // hold position
  gbbUpperTrunkHoldPosition* hold_position_group = new gbbUpperTrunkHoldPosition(this, "Hold Joint Pos. UpperTrunk");

  // WALKING CONTROL UNITS
  // ================================================

  // walking phases

  mbbStimulationSkill* skill_walking = new mbbStimulationSkill(this, "(Ph) Walking All Phases", ib2c::tStimulationMode::AUTO, 0, 2);
  skill_walking->SetConfigNode("/UpperTrunk/phases/walking_all_phases/");
  skill_walking->par_stimulation_factor[0].SetConfigEntry("shoulder_x_angle/stimulation_factor");// TODO emplace_back the parameter
  skill_walking->par_min_angle[0].SetConfigEntry("shoulder_x_angle/min_angle");
  skill_walking->par_max_angle[0].SetConfigEntry("shoulder_x_angle/max_angle");
  skill_walking->par_target_angle[0].SetConfigEntry("shoulder_x_angle/target_angle");

  skill_walking->par_stimulation_factor[1].SetConfigEntry("elbow_angle/stimulation_factor");
  skill_walking->par_min_angle[1].SetConfigEntry("elbow_angle/min_angle");
  skill_walking->par_max_angle[1].SetConfigEntry("elbow_angle/max_angle");
  skill_walking->par_target_angle[1].SetConfigEntry("elbow_angle/target_angle");

  mbbStimulationSkill* skill_walking_phase3_left = new mbbStimulationSkill(this, "(Ph) Walking Phase 3 Left", ib2c::tStimulationMode::AUTO, 0, 1);
  skill_walking_phase3_left->SetConfigNode("/UpperTrunk/walking_phase_3/");
  skill_walking_phase3_left->par_stimulation_factor[0].SetConfigEntry("active_arm_swing/stimulation_factor");

  mbbStimulationSkill* skill_walking_phase3_right = new mbbStimulationSkill(this, "(Ph) Walking Phase 3 Right", ib2c::tStimulationMode::AUTO, 0, 1);
  skill_walking_phase3_right->SetConfigNode("/UpperTrunk/walking_phase_3/");
  skill_walking_phase3_right->par_stimulation_factor[0].SetConfigEntry("active_arm_swing/stimulation_factor");

  mbbStimulationSkill* skill_walking_phase4_left = new mbbStimulationSkill(this, "(Ph) Walking Phase 4 Left", ib2c::tStimulationMode::AUTO, 0, 1);
  skill_walking_phase4_left->SetConfigNode("/UpperTrunk/walking_phase_4/");
  skill_walking_phase4_left->par_stimulation_factor[0].SetConfigEntry("active_arm_swing/stimulation_factor");

  mbbStimulationSkill* skill_walking_phase4_right = new mbbStimulationSkill(this, "(Ph) Walking Phase 4 Right", ib2c::tStimulationMode::AUTO, 0, 1);
  skill_walking_phase4_right->SetConfigNode("/UpperTrunk/walking_phase_4/");
  skill_walking_phase4_right->par_stimulation_factor[0].SetConfigEntry("active_arm_swing/stimulation_factor");

  //fusion of walking termination
  skills::mbbTerminationFusion* fusion_walking_termination_left = new skills::mbbTerminationFusion(this, "(F) Last Step & Walking Left ");

  skills::mbbTerminationFusion* fusion_walking_termination_right = new skills::mbbTerminationFusion(this, "(F) Last Step & Walking Right ");

  mbbStimulationSkill* skill_termination = new mbbStimulationSkill(this, "(Ph)Termination All Phases", ib2c::tStimulationMode::AUTO, 0, 2);
  skill_termination->SetConfigNode("UpperTrunk/phases/termination_all_phases/"); // todo fixme add value name
  skill_termination->par_stimulation_factor[0].SetConfigEntry("termination_all_phases/shoulder_x_angle");
  skill_termination->par_min_angle[0].SetConfigEntry("termination_all_phases/shoulder_x_angle");
  skill_termination->par_max_angle[0].SetConfigEntry("termination_all_phases/shoulder_x_angle");
  skill_termination->par_target_angle[0].SetConfigEntry("termination_all_phases/shoulder_x_angle");

  skill_termination->par_stimulation_factor[1].SetConfigEntry("termination_all_phases/elbow_angle");
  skill_termination->par_min_angle[1].SetConfigEntry("termination_all_phases/elbow_angle");
  skill_termination->par_max_angle[1].SetConfigEntry("termination_all_phases/elbow_angle");
  skill_termination->par_target_angle[1].SetConfigEntry("termination_all_phases/elbow_angle");

  mbbStimulationSkill* skill_termination_phase3_left = new mbbStimulationSkill(this, "(Ph) Termination Phase 3 Left", ib2c::tStimulationMode::AUTO, 0, 1);
  skill_termination_phase3_left->SetConfigNode("UpperTrunk/termination_phase_3/");
  skill_termination_phase3_left->par_stimulation_factor[0].SetConfigEntry("active_arm_swing/stimulation_factor");

  mbbStimulationSkill* skill_termination_phase3_right = new mbbStimulationSkill(this, "(Ph) Termination Phase 3 Right", ib2c::tStimulationMode::AUTO, 0, 1);
  skill_termination_phase3_right->SetConfigNode("UpperTrunk/termination_phase_3/");
  skill_termination_phase3_right->par_stimulation_factor[0].SetConfigEntry("active_arm_swing/stimulation_factor");

  mbbStimulationSkill* skill_termination_phase4_left = new mbbStimulationSkill(this, "(Ph) Termination Phase 4 Left", ib2c::tStimulationMode::AUTO, 0, 2);
  skill_termination_phase4_left->SetConfigNode("UpperTrunk/phases/termination_phase_4/");
  skill_termination_phase4_left->par_stimulation_factor[0].SetConfigEntry("active_arm_swing/stimulation_factor");
  skill_termination_phase4_left->par_stimulation_factor[1].SetConfigEntry("lock_arm/stimulation_factor");

  mbbStimulationSkill* skill_termination_phase5_left = new mbbStimulationSkill(this, "(Ph) Termination Phase 5 Left", ib2c::tStimulationMode::AUTO, 0, 1);
  skill_termination_phase5_left->SetConfigNode("UpperTrunk/termination_phase_5/");
  skill_termination_phase5_left->par_stimulation_factor[0].SetConfigEntry("lock_arm/stimulation_factor");
  skill_termination_phase5_left->par_min_angle[0].SetConfigEntry("lock_arm/min_angle");
  skill_termination_phase5_left->par_max_angle[0].SetConfigEntry("lock_arm/max_angle");
  skill_termination_phase5_left->par_target_angle[0].SetConfigEntry("lock_arm/target_angle");

  mbbStimulationSkill* skill_termination_phase4_right = new mbbStimulationSkill(this, "(Ph) Termination Phase 4 Right", ib2c::tStimulationMode::AUTO, 0, 2);
  skill_termination_phase4_right->SetConfigNode("UpperTrunk/termination_phase_4/");
  skill_termination_phase4_right->par_stimulation_factor[0].SetConfigEntry("active_arm_swing/stimulation_factor");
  skill_termination_phase4_right->par_stimulation_factor[1].SetConfigEntry("lock_arm/stimulation_factor");

  mbbStimulationSkill* skill_termination_phase5_right = new mbbStimulationSkill(this, "(Ph) Termination Phase 5 Right", ib2c::tStimulationMode::AUTO, 0, 1);
  skill_termination_phase5_right->SetConfigNode("UpperTrunk/termination_phase_5/");
  skill_termination_phase5_right->par_stimulation_factor[0].SetConfigEntry("lock_arm/stimulation_factor");
  skill_termination_phase5_right->par_min_angle[0].SetConfigEntry("lock_arm/min_angle");
  skill_termination_phase5_right->par_max_angle[0].SetConfigEntry("lock_arm/max_angle");
  skill_termination_phase5_right->par_target_angle[0].SetConfigEntry("lock_arm/target_angle");


  //BEGIN motor patterns


  //walking phases
  // active arm swing for walking
  motor_patterns::mbbActiveArmSwing* active_arm_swing_left = new motor_patterns::mbbActiveArmSwing(this, "(MP) Active Arm Swing Left", tStimulationMode::AUTO, 1, mMotorPattern::tCurveMode::eSIGMOID, 300, 0.4, 0.6);
  //motor_patterns::mbbActiveArmSwing* active_arm_swing_left = new motor_patterns::mbbActiveArmSwing(this, "(MP) Active Arm Swing Left", tStimulationMode::AUTO, 1, 300, 0.5, 100);
  active_arm_swing_left->number_of_inhibition_ports.Set(1);
  active_arm_swing_left->CheckStaticParameters();
  motor_patterns::mbbActiveArmSwing* active_arm_swing_right = new motor_patterns::mbbActiveArmSwing(this, "(MP) Active Arm Swing Right", tStimulationMode::AUTO, 1, mMotorPattern::tCurveMode::eSIGMOID, 300, 0.4, 0.6);
  //motor_patterns::mbbActiveArmSwing* active_arm_swing_right = new motor_patterns::mbbActiveArmSwing(this, "(MP) Active Arm Swing Right", tStimulationMode::AUTO, 1, 300, 0.5, 100);
  active_arm_swing_right->number_of_inhibition_ports.Set(1);
  active_arm_swing_right->CheckStaticParameters();
  // active arm swing for last step of termination
  motor_patterns::mbbActiveArmSwing* termi_active_arm_swing_left = new motor_patterns::mbbActiveArmSwing(this, "(MP) Active Arm Swing Left Termination", tStimulationMode::AUTO, 1, mMotorPattern::tCurveMode::eSIGMOID, 200, 0.3, 0.4);
  //motor_patterns::mbbActiveArmSwing* termi_active_arm_swing_left = new motor_patterns::mbbActiveArmSwing(this, "(MP) Active Arm Swing Left Termination", tStimulationMode::AUTO, 1, 200, 0.35, 40);
  termi_active_arm_swing_left->number_of_inhibition_ports.Set(1);
  termi_active_arm_swing_left->CheckStaticParameters();
  motor_patterns::mbbActiveArmSwing* termi_active_arm_swing_right = new motor_patterns::mbbActiveArmSwing(this, "(MP) Active Arm Swing Right Termination", tStimulationMode::AUTO, 1, mMotorPattern::tCurveMode::eSIGMOID, 200, 0.3, 0.4);
  //motor_patterns::mbbActiveArmSwing* termi_active_arm_swing_right = new motor_patterns::mbbActiveArmSwing(this, "(MP) Active Arm Swing Right Termination", tStimulationMode::AUTO, 1, 200, 0.35, 40);
  termi_active_arm_swing_right->number_of_inhibition_ports.Set(1);
  termi_active_arm_swing_right->CheckStaticParameters();
  //END motor patterns

  // =======================================
  // create descriptions for controller output log
  /*
  int log_dimension = 0;
  for (unsigned int i = 0; i < module_list.size(); i++)
  {
    log_dimension += module_list[i]->ControllerOutputDimension();
  }
  char** log_description = new char*[log_dimension];
  char buffer[ 100 ];
  int j = 0;
  for (unsigned int k = 0; k < module_list.size(); k++)
  {
    for (int i = 0; i < module_list[k]->ControllerOutputDimension(); i++)
    {
      snprintf(buffer, sizeof(buffer), "%s - %s", module_list[k]->Description(), module_list[k]->ControllerOutputDescription(i));
      buffer[sizeof(buffer) - 1] = 0;
      log_description[j] = strdup(buffer);
      j++;
    }
  }

  snprintf(buffer, sizeof(buffer), "%s_co_log", this->Description());
  buffer[sizeof(buffer) - 1] = 0;

  // create log manager for controller outputs
  upper_trunk_log_bb = new mScalarLogManager(
    this,
    0,
    0,
    log_dimension,
    log_description,
    buffer,
    eBB_EXPORT,
    "Controller Output Log");

  // deleting log_description
  for (int i = 0; i < log_dimension; i++)
  {
    free(log_description[i]);
  }
  delete[] log_description;
  */

  // add connections
  // ======================================================================

  // sensor data
  this->GetSensorInputs().ConnectByName(this->GetSensorOutputs(), false);

  // controller log
  /*
  AddEdgeUp(upper_trunk_log_bb, this, 1, mScalarLogManager::eSO_LOG_CHANGED, eSO_UPPER_TRUNK_CO_LOG_UPDATED);
  AddEdgeDown(this, upper_trunk_log_bb, 1, eCI_SENSOR_MONITOR, mScalarLogManager::eCI_LOG_REQUEST);
  int index = 2;
  for (unsigned int i = 0; i < module_list.size(); i++)
  {
    AddEdgeDown(module_list[i], upper_trunk_log_bb, module_list[i]->ControllerOutputDimension(), 0, index);
    index += module_list[i]->ControllerOutputDimension();
  }
  */

  //BEGIN fusion behaviours
  // ---------------------------------------------------------------------------------------------
  // left arm

  fusion_abs_angle_left_shoulder_x->activity.ConnectTo(this->co_left_stimulation_shoulder_x_angle);
  fusion_abs_angle_left_shoulder_x->OutputPort(0).ConnectTo(this->co_left_shoulder_x_angle);

  fusion_abs_angle_left_shoulder_y->activity.ConnectTo(this->co_left_stimulation_shoulder_y_angle);
  fusion_abs_angle_left_shoulder_y->OutputPort(0).ConnectTo(this->co_left_shoulder_y_angle);

  fusion_abs_angle_left_elbow->activity.ConnectTo(this->co_left_stimulation_elbow_angle);
  fusion_abs_angle_left_elbow->OutputPort(0).ConnectTo(this->co_left_elbow_angle);

//  fusion_abs_torque_left_shoulder_x->activity.ConnectTo(this->co_left_stimulation_shoulder_x_torque);
//  fusion_abs_torque_left_shoulder_x->OutputPort(0).ConnectTo(this->co_left_shoulder_x_torque);

  fusion_abs_torque_left_shoulder_y->activity.ConnectTo(this->co_left_stimulation_shoulder_y_torque);
  fusion_abs_torque_left_shoulder_y->OutputPort(0).ConnectTo(this->co_left_shoulder_y_torque);

//  fusion_abs_torque_left_elbow->activity.ConnectTo(this->co_left_stimulation_elbow_torque);
//  fusion_abs_torque_left_elbow->OutputPort(0).ConnectTo(this->co_left_elbow_torque);

  // inhibit position by torque

  //fusion_abs_torque_left_shoulder_x->activity.ConnectTo(fusion_abs_angle_left_shoulder_x->inhibition[0]);
  fusion_abs_torque_left_shoulder_y->activity.ConnectTo(memory_abs_torque_left_shoulder_y->in_activity);
  memory_abs_torque_left_shoulder_y->out_activity.ConnectTo(fusion_abs_angle_left_shoulder_y->inhibition[0]);
  //fusion_abs_torque_left_elbow->activity.ConnectTo(fusion_abs_angle_left_elbow->inhibition[0]);

  // right arm

  fusion_abs_angle_right_shoulder_x->activity.ConnectTo(this->co_right_stimulation_shoulder_x_angle);
  fusion_abs_angle_right_shoulder_x->OutputPort(0).ConnectTo(this->co_right_shoulder_x_angle);

  fusion_abs_angle_right_shoulder_y->activity.ConnectTo(this->co_right_stimulation_shoulder_y_angle);
  fusion_abs_angle_right_shoulder_y->OutputPort(0).ConnectTo(this->co_right_shoulder_y_angle);

  fusion_abs_angle_right_elbow->activity.ConnectTo(this->co_right_stimulation_elbow_angle);
  fusion_abs_angle_right_elbow->OutputPort(0).ConnectTo(this->co_right_elbow_angle);

//  fusion_abs_torque_right_shoulder_x->activity.ConnectTo(this->co_right_stimulation_shoulder_x_torque);
//  fusion_abs_torque_right_shoulder_x->OutputPort(0).ConnectTo(this->co_right_shoulder_x_torque);

  fusion_abs_torque_right_shoulder_y->activity.ConnectTo(this->co_right_stimulation_shoulder_y_torque);
  fusion_abs_torque_right_shoulder_y->OutputPort(0).ConnectTo(this->co_right_shoulder_y_torque);

//  fusion_abs_torque_right_elbow->activity.ConnectTo(this->co_right_stimulation_elbow_torque);
//  fusion_abs_torque_right_elbow->OutputPort(0).ConnectTo(this->co_right_elbow_torque);

  // inhibit position by torque

  //fusion_abs_torque_right_shoulder_x->activity.ConnectTo(fusion_abs_angle_right_shoulder_x->inhibition[0]);
  fusion_abs_torque_right_shoulder_y->activity.ConnectTo(memory_abs_torque_right_shoulder_y->in_activity);
  memory_abs_torque_right_shoulder_y->out_activity.ConnectTo(fusion_abs_angle_right_shoulder_y->inhibition[0]);  //fusion_abs_torque_right_elbow->activity.ConnectTo(fusion_abs_angle_right_elbow->inhibition[0]);

  // active arm swing

  fusion_active_arm_swing_left->activity.ConnectTo(active_arm_swing_left->stimulation);
  fusion_active_arm_swing_right->activity.ConnectTo(active_arm_swing_right->stimulation);


  // active arm swing for termination

  fusion_active_arm_swing_left_termi->activity.ConnectTo(termi_active_arm_swing_left->stimulation);
  fusion_active_arm_swing_right_termi->activity.ConnectTo(termi_active_arm_swing_right->stimulation);


  //END fusion behaviours

  //BEGIN reflexes

  // lock arm and lock arm termination

  // left arm

  this->ci_termination_scale_factor.ConnectTo(lock_arm_swing_left->ci_termination_scale_factor);
  this->ci_push_recovery_stand.ConnectTo(lock_arm_swing_left->ci_push_recovery);
  this->ci_last_step_state.ConnectTo(lock_arm_swing_left->ci_last_step);

  this->si_left_shoulder_y_angle.ConnectTo(lock_arm_swing_left->si_shoulder_y_angle);

  lock_arm_swing_left->activity.ConnectTo(active_arm_swing_left->inhibition[0]);

  lock_arm_swing_left->activity.ConnectTo(termi_active_arm_swing_left->inhibition[0]);

  fusion_lock_arm_swing->activity.ConnectTo(lock_arm_swing_left->stimulation);

  lock_arm_swing_left->co_stimulation_angle_shoulder.ConnectTo(fusion_abs_angle_left_shoulder_y->InputActivity(0));
  lock_arm_swing_left->co_shoulder_y_angle.ConnectTo(fusion_abs_angle_left_shoulder_y->InputPort(0, 0));
  lock_arm_swing_left->target_rating.ConnectTo(fusion_abs_angle_left_shoulder_y->InputTargetRating(0));


  lock_arm_swing_left->co_stimulation_angle_elbow.ConnectTo(fusion_abs_angle_left_elbow->InputActivity(0));
  lock_arm_swing_left->co_elbow_y_angle.ConnectTo(fusion_abs_angle_left_elbow->InputPort(0, 0));
  lock_arm_swing_left->target_rating.ConnectTo(fusion_abs_angle_left_elbow->InputTargetRating(0));



  // right arm

  this->ci_termination_scale_factor.ConnectTo(lock_arm_swing_right->ci_termination_scale_factor);
  this->ci_push_recovery_stand.ConnectTo(lock_arm_swing_right->ci_push_recovery);
  this->ci_last_step_state.ConnectTo(lock_arm_swing_right->ci_last_step);

  this->si_right_shoulder_y_angle.ConnectTo(lock_arm_swing_right->si_shoulder_y_angle);

  lock_arm_swing_right->activity.ConnectTo(active_arm_swing_right->inhibition[0]);

  lock_arm_swing_right->activity.ConnectTo(termi_active_arm_swing_right->inhibition[0]);

  fusion_lock_arm_swing->activity.ConnectTo(lock_arm_swing_right->stimulation);

  lock_arm_swing_right->co_stimulation_angle_shoulder.ConnectTo(fusion_abs_angle_right_shoulder_y->InputActivity(0));
  lock_arm_swing_right->co_shoulder_y_angle.ConnectTo(fusion_abs_angle_right_shoulder_y->InputPort(0, 0));
  lock_arm_swing_right->target_rating.ConnectTo(fusion_abs_angle_right_shoulder_y->InputTargetRating(0));


  lock_arm_swing_right->co_stimulation_angle_elbow.ConnectTo(fusion_abs_angle_right_elbow->InputActivity(0));
  lock_arm_swing_right->co_elbow_y_angle.ConnectTo(fusion_abs_angle_right_elbow->InputPort(0, 0));
  lock_arm_swing_right->target_rating.ConnectTo(fusion_abs_angle_right_elbow->InputTargetRating(0));


  // hold position group
  //----------------------------------------------------------------------------------------------------------------------------
  skill_stand->activity.ConnectTo(hold_position_group->stimulation);

  this->ci_stiffness_factor.ConnectTo(hold_position_group->ci_stiffness_factor);
  this->ci_ground_contact.ConnectTo(hold_position_group->ci_ground_contact);

//  fusion_rel_angle_left_shoulder_x->activity.ConnectTo(hold_position_group->ci_left_stimulation_angle_adjustment_shoulder_x);
//  fusion_rel_angle_left_shoulder_x->OutputPort(0).ConnectTo(hold_position_group->ci_left_angle_adjustment_shoulder_x);

//  fusion_rel_angle_left_shoulder_y->activity.ConnectTo(hold_position_group->ci_left_stimulation_angle_adjustment_shoulder_y);
//  fusion_rel_angle_left_shoulder_y->OutputPort(0).ConnectTo(hold_position_group->ci_left_angle_adjustment_shoulder_y);
//
//  fusion_rel_angle_left_elbow->activity.ConnectTo(hold_position_group->ci_left_stimulation_angle_adjustment_elbow);
//  fusion_rel_angle_left_elbow->OutputPort(0).ConnectTo(hold_position_group->ci_left_angle_adjustment_elbow);
//
//  fusion_rel_angle_right_shoulder_x->activity.ConnectTo(hold_position_group->ci_right_stimulation_angle_adjustment_shoulder_x);
//  fusion_rel_angle_right_shoulder_x->OutputPort(0).ConnectTo(hold_position_group->ci_right_angle_adjustment_shoulder_x);
//
//  fusion_rel_angle_right_shoulder_y->activity.ConnectTo(hold_position_group->ci_right_stimulation_angle_adjustment_shoulder_y);
//  fusion_rel_angle_right_shoulder_y->OutputPort(0).ConnectTo(hold_position_group->ci_right_angle_adjustment_shoulder_y);
//
//  fusion_rel_angle_right_elbow->activity.ConnectTo(hold_position_group->ci_right_stimulation_angle_adjustment_elbow);
//  fusion_rel_angle_right_elbow->OutputPort(0).ConnectTo(hold_position_group->ci_right_angle_adjustment_elbow);

  this->si_left_shoulder_x_angle.ConnectTo(hold_position_group->si_left_shoulder_x_angle);
  this->si_left_shoulder_y_angle.ConnectTo(hold_position_group->si_left_shoulder_y_angle);
  this->si_left_elbow_angle.ConnectTo(hold_position_group->si_left_elbow_angle);
  this->si_right_shoulder_x_angle.ConnectTo(hold_position_group->si_right_shoulder_x_angle);
  this->si_right_shoulder_y_angle.ConnectTo(hold_position_group->si_right_shoulder_y_angle);
  this->si_right_elbow_angle.ConnectTo(hold_position_group->si_right_elbow_angle);

  // fusion of hold position group
  // left
  hold_position_group->co_left_activity_shoulder_x.ConnectTo(fusion_abs_angle_left_shoulder_x->InputActivity(0));
  //hold_position_group->co_left_target_rating_shoulder_x.ConnectTo(fusion_abs_angle_left_shoulder_x->InputTargetRating(0));
  hold_position_group->target_rating.ConnectTo(fusion_abs_angle_left_shoulder_x->InputTargetRating(0));

  hold_position_group->co_left_angle_shoulder_x.ConnectTo(fusion_abs_angle_left_shoulder_x->InputPort(0, 0));

  hold_position_group->co_left_activity_shoulder_y.ConnectTo(fusion_abs_angle_left_shoulder_y->InputActivity(1));
  //hold_position_group->co_left_activity_shoulder_y.ConnectTo(fusion_abs_angle_left_shoulder_y->InputTargetRating(1));
  hold_position_group->target_rating.ConnectTo(fusion_abs_angle_left_shoulder_y->InputTargetRating(1));

  hold_position_group->co_left_angle_shoulder_y.ConnectTo(fusion_abs_angle_left_shoulder_y->InputPort(1, 0));

  hold_position_group->co_left_activity_elbow.ConnectTo(fusion_abs_angle_left_elbow->InputActivity(1));
  //hold_position_group->co_left_target_rating_elbow.ConnectTo(fusion_abs_angle_left_elbow->InputTargetRating(1));
  hold_position_group->target_rating.ConnectTo(fusion_abs_angle_left_elbow->InputTargetRating(1));

  hold_position_group->co_left_angle_elbow.ConnectTo(fusion_abs_angle_left_elbow->InputPort(1, 0));

  // right
  hold_position_group->co_right_activity_shoulder_x.ConnectTo(fusion_abs_angle_right_shoulder_x->InputActivity(0));
  //hold_position_group->co_right_target_rating_shoulder_x.ConnectTo(fusion_abs_angle_right_shoulder_x->InputTargetRating(0));
  hold_position_group->co_right_activity_shoulder_x.ConnectTo(fusion_abs_angle_right_shoulder_x->InputTargetRating(0));
  hold_position_group->co_right_angle_shoulder_x.ConnectTo(fusion_abs_angle_right_shoulder_x->InputPort(0, 0));

  hold_position_group->co_right_activity_shoulder_y.ConnectTo(fusion_abs_angle_right_shoulder_y->InputActivity(1));
  //hold_position_group->co_right_target_rating_shoulder_y.ConnectTo(fusion_abs_angle_right_shoulder_y->InputTargetRating(1));
  hold_position_group->co_right_activity_shoulder_y.ConnectTo(fusion_abs_angle_right_shoulder_y->InputTargetRating(1));

  hold_position_group->co_right_angle_shoulder_y.ConnectTo(fusion_abs_angle_right_shoulder_y->InputPort(1, 0));

  hold_position_group->co_right_activity_elbow.ConnectTo(fusion_abs_angle_right_elbow->InputActivity(1));
  //hold_position_group->co_right_target_rating_elbow.ConnectTo(fusion_abs_angle_right_elbow->InputTargetRating(1));
  hold_position_group->co_right_activity_elbow.ConnectTo(fusion_abs_angle_right_elbow->InputTargetRating(1));

  hold_position_group->co_right_angle_elbow.ConnectTo(fusion_abs_angle_right_elbow->InputPort(1, 0));

  //END reflexes

  // begin motor patterns
  // active arm swing for walking phases
  // left
  this->ci_walking_scale_factor.ConnectTo(active_arm_swing_left->ci_scale);

  active_arm_swing_left->activity.ConnectTo(fusion_abs_torque_left_shoulder_y->InputActivity(0));
  active_arm_swing_left->target_rating.ConnectTo(fusion_abs_torque_left_shoulder_y->InputTargetRating(0));
  active_arm_swing_left->co_ipsilateral_shoulder_torque.ConnectTo(fusion_abs_torque_left_shoulder_y->InputPort(0, 0));

  active_arm_swing_left->activity.ConnectTo(fusion_abs_torque_right_shoulder_y->InputActivity(0));
  active_arm_swing_left->target_rating.ConnectTo(fusion_abs_torque_right_shoulder_y->InputTargetRating(0));
  active_arm_swing_left->co_contralateral_shoulder_torque.ConnectTo(fusion_abs_torque_right_shoulder_y->InputPort(0, 0));

  active_arm_swing_left->ci_fast_walking.ConnectTo(this->co_fast_walking);
  active_arm_swing_left->ci_fast_walking.ConnectTo(this->co_fast_walking);

  // right
  this->ci_walking_scale_factor.ConnectTo(active_arm_swing_right->ci_scale);
  this->ci_push_recovery_stand.ConnectTo(active_arm_swing_right->ci_push_recovery_stand);

  active_arm_swing_right->activity.ConnectTo(fusion_abs_torque_right_shoulder_y->InputActivity(1));
  active_arm_swing_right->target_rating.ConnectTo(fusion_abs_torque_right_shoulder_y->InputTargetRating(1));
  active_arm_swing_right->co_ipsilateral_shoulder_torque.ConnectTo(fusion_abs_torque_right_shoulder_y->InputPort(1, 0));

  active_arm_swing_right->activity.ConnectTo(fusion_abs_torque_left_shoulder_y->InputActivity(1));
  active_arm_swing_right->target_rating.ConnectTo(fusion_abs_torque_left_shoulder_y->InputTargetRating(1));
  active_arm_swing_right->co_contralateral_shoulder_torque.ConnectTo(fusion_abs_torque_left_shoulder_y->InputPort(1, 0));
  active_arm_swing_right->ci_fast_walking.ConnectTo(this->co_fast_walking);

  //active arm swing for termination phases
  this->ci_termination_scale_factor.ConnectTo(termi_active_arm_swing_left->ci_scale);
  this->ci_last_step_state.ConnectTo(termi_active_arm_swing_left->ci_last_step_state);

  termi_active_arm_swing_left->activity.ConnectTo(fusion_abs_torque_left_shoulder_y->InputActivity(2));
  termi_active_arm_swing_left->activity.ConnectTo(fusion_abs_torque_left_shoulder_y->InputTargetRating(2));
  termi_active_arm_swing_left->co_ipsilateral_shoulder_torque.ConnectTo(fusion_abs_torque_left_shoulder_y->InputPort(2, 0));

  termi_active_arm_swing_left->activity.ConnectTo(fusion_abs_torque_right_shoulder_y->InputActivity(2));
  termi_active_arm_swing_left->activity.ConnectTo(fusion_abs_torque_right_shoulder_y->InputTargetRating(2));
  termi_active_arm_swing_left->co_contralateral_shoulder_torque.ConnectTo(fusion_abs_torque_right_shoulder_y->InputPort(2, 0));

  // END motor patterns

  //BEGIN skills
  //--------------------------------------------------------------------------------------------------------

  //compensation shoulder pitch
  this->ci_actual_omega_y.ConnectTo(skill_compensation_shoulder_pitch->ci_omega_y);
  this->ci_angle_body_space.ConnectTo(skill_compensation_shoulder_pitch->ci_angle_body_space);
  this->ci_actual_pitch.ConnectTo(skill_compensation_shoulder_pitch->ci_angle_trunk_pitch);
  this->ci_target_angle_arm_trunk.ConnectTo(skill_compensation_shoulder_pitch->ci_target_angle_arm_trunk_pitch);

  this->si_left_shoulder_y_angle.ConnectTo(skill_compensation_shoulder_pitch->si_shoulder_y_left);
  this->si_right_shoulder_y_angle.ConnectTo(skill_compensation_shoulder_pitch->si_shoulder_y_right);

  skill_compensation_shoulder_pitch->activity.ConnectTo(fusion_abs_torque_right_shoulder_y->InputActivity(3));
  skill_compensation_shoulder_pitch->target_rating.ConnectTo(fusion_abs_torque_right_shoulder_y->InputTargetRating(3));
  skill_compensation_shoulder_pitch->co_torque_shoulder_right_pitch.ConnectTo(fusion_abs_torque_right_shoulder_y->InputPort(3, 0));

  skill_compensation_shoulder_pitch->activity.ConnectTo(fusion_abs_torque_left_shoulder_y->InputActivity(3));
  skill_compensation_shoulder_pitch->target_rating.ConnectTo(fusion_abs_torque_left_shoulder_y->InputTargetRating(3));
  skill_compensation_shoulder_pitch->co_torque_shoulder_left_pitch.ConnectTo(fusion_abs_torque_left_shoulder_y->InputPort(3, 0));

  // compensation shoulder roll
  this->ci_angle_body_space_lateral.ConnectTo(skill_compensation_shoulder_roll->ci_angle_body_space_lateral);
  skill_compensation_shoulder_roll->activity.ConnectTo(fusion_abs_torque_right_shoulder_x->InputActivity(0));
  skill_compensation_shoulder_roll->target_rating.ConnectTo(fusion_abs_torque_right_shoulder_x->InputTargetRating(0));
  skill_compensation_shoulder_roll->co_torque_shoulder_right_roll.ConnectTo(fusion_abs_torque_right_shoulder_x->InputPort(0, 0));

  skill_compensation_shoulder_roll->activity.ConnectTo(fusion_abs_torque_left_shoulder_x->InputActivity(0));
  skill_compensation_shoulder_roll->target_rating.ConnectTo(fusion_abs_torque_left_shoulder_x->InputTargetRating(0));
  skill_compensation_shoulder_roll->co_torque_shoulder_left_roll.ConnectTo(fusion_abs_torque_left_shoulder_x->InputPort(0, 0));

  // balance arm
  //this->ci_angle_body_space.ConnectTo(balance_arm->ci_angle_body_space);
//  balance_arm->activity.ConnectTo(fusion_abs_angle_right_shoulder_y->InputActivity(2));
//  balance_arm->target_rating.ConnectTo(fusion_abs_angle_right_shoulder_y->InputTargetRating(2));
//  balance_arm->co_target_angle_arm_y.ConnectTo(fusion_abs_angle_right_shoulder_y->InputPort(2, 0));

//  balance_arm->activity.ConnectTo(fusion_abs_angle_left_shoulder_y->InputActivity(2));
//  balance_arm->target_rating.ConnectTo(fusion_abs_angle_left_shoulder_y->InputTargetRating(2));
//  balance_arm->co_target_angle_arm_y.ConnectTo(fusion_abs_angle_left_shoulder_y->InputPort(2, 0));

  // termination and walking
  this->ci_activity_walking.ConnectTo(fusion_walking_termination_left->stimulation);
  this->ci_last_step_state.ConnectTo(fusion_walking_termination_left->ci_last_step_state);
  this->ci_left_stimulation_walking_phase_1.ConnectTo(fusion_walking_termination_left->ci_walking_phase_1);
  this->ci_left_stimulation_walking_phase_2.ConnectTo(fusion_walking_termination_left->ci_walking_phase_2);
  this->ci_left_stimulation_walking_phase_3.ConnectTo(fusion_walking_termination_left->ci_walking_phase_3);
  this->ci_left_stimulation_walking_phase_4.ConnectTo(fusion_walking_termination_left->ci_walking_phase_4);
  this->ci_left_stimulation_walking_phase_5.ConnectTo(fusion_walking_termination_left->ci_walking_phase_5);

  this->ci_activity_walking.ConnectTo(fusion_walking_termination_right->stimulation);
  this->ci_last_step_state.ConnectTo(fusion_walking_termination_right->ci_last_step_state);
  this->ci_right_stimulation_walking_phase_1.ConnectTo(fusion_walking_termination_right->ci_walking_phase_1);
  this->ci_right_stimulation_walking_phase_2.ConnectTo(fusion_walking_termination_right->ci_walking_phase_2);
  this->ci_right_stimulation_walking_phase_3.ConnectTo(fusion_walking_termination_right->ci_walking_phase_3);
  this->ci_right_stimulation_walking_phase_4.ConnectTo(fusion_walking_termination_right->ci_walking_phase_4);
  this->ci_right_stimulation_walking_phase_5.ConnectTo(fusion_walking_termination_right->ci_walking_phase_5);

  // stand
  this->ci_stimulation_stand.ConnectTo(skill_stand->stimulation);

  // walking all phases
  this->ci_activity_walking.ConnectTo(skill_walking->stimulation);

  skill_walking->co_target_activity[0].ConnectTo(fusion_abs_angle_left_shoulder_x->InputActivity(1));
  skill_walking->co_target_activity[0].ConnectTo(fusion_abs_angle_left_shoulder_x->InputTargetRating(1));
  skill_walking->co_target_output[0].ConnectTo(fusion_abs_angle_left_shoulder_x->InputPort(1, 0));

  skill_walking->co_target_activity[0].ConnectTo(fusion_abs_angle_right_shoulder_x->InputActivity(1));
  skill_walking->co_target_activity[0].ConnectTo(fusion_abs_angle_right_shoulder_x->InputTargetRating(1));
  skill_walking->co_target_output[0].ConnectTo(fusion_abs_angle_right_shoulder_x->InputPort(1, 0));

  skill_walking->co_target_activity[0].ConnectTo(fusion_abs_angle_left_elbow->InputActivity(2));
  skill_walking->co_target_activity[0].ConnectTo(fusion_abs_angle_left_elbow->InputTargetRating(2));
  skill_walking->co_target_output[0].ConnectTo(fusion_abs_angle_left_elbow->InputPort(2, 0));

  skill_walking->co_target_activity[0].ConnectTo(fusion_abs_angle_right_elbow->InputActivity(2));
  skill_walking->co_target_activity[0].ConnectTo(fusion_abs_angle_right_elbow->InputTargetRating(2));
  skill_walking->co_target_output[0].ConnectTo(fusion_abs_angle_right_elbow->InputPort(2, 0));

  // walking phase 3
  // left
  fusion_walking_termination_left->co_walking_phase_3.ConnectTo(skill_walking_phase3_left->stimulation);

  skill_walking_phase3_left->activity.ConnectTo(fusion_active_arm_swing_left->InputActivity(0));
  skill_walking_phase3_left->activity.ConnectTo(fusion_active_arm_swing_left->InputTargetRating(0));
  skill_walking_phase3_left->co_target_output[0].ConnectTo(fusion_active_arm_swing_left->InputPort(0, 0));

  //active_arm_swing_left->activity.ConnectTo(skill_walking_phase3_left->stimulation);
  //active_arm_swing_left->target_rating.ConnectTo(skill_walking_phase3_left->si_target_rating[0]);

  // right
  fusion_walking_termination_right->co_walking_phase_3.ConnectTo(skill_walking_phase3_right->stimulation);

  skill_walking_phase3_right->activity.ConnectTo(fusion_active_arm_swing_right->InputActivity(0));
  skill_walking_phase3_right->activity.ConnectTo(fusion_active_arm_swing_right->InputTargetRating(0));//fixme
  skill_walking_phase3_right->co_target_output[0].ConnectTo(fusion_active_arm_swing_right->InputPort(0, 0));

// active_arm_swing_right->activity.ConnectTo(skill_walking_phase3_right->stimulation);
  //active_arm_swing_right->target_rating.ConnectTo(skill_walking_phase3_right->si_target_rating[0]);

  // walking phase 4
  // left
  fusion_walking_termination_left->co_walking_phase_4.ConnectTo(skill_walking_phase4_left->stimulation);

  skill_walking_phase4_left->activity.ConnectTo(fusion_active_arm_swing_left->InputActivity(1));
  skill_walking_phase4_left->activity.ConnectTo(fusion_active_arm_swing_left->InputTargetRating(1));
  skill_walking_phase4_left->co_target_output[0].ConnectTo(fusion_active_arm_swing_left->InputPort(1, 0));


//  active_arm_swing_left->activity.ConnectTo(skill_walking_phase4_left->stimulation);
// active_arm_swing_left->target_rating.ConnectTo(skill_walking_phase4_left->si_target_rating[0]);

  // right

  fusion_walking_termination_right->co_walking_phase_4.ConnectTo(skill_walking_phase4_right->stimulation);

  skill_walking_phase4_right->activity.ConnectTo(fusion_active_arm_swing_right->InputActivity(1));
  skill_walking_phase4_right->activity.ConnectTo(fusion_active_arm_swing_right->InputTargetRating(1));//fixme
  skill_walking_phase4_right->co_target_output[0].ConnectTo(fusion_active_arm_swing_right->InputPort(1, 0));


  //active_arm_swing_right->activity.ConnectTo(skill_walking_phase4_right->stimulation);
// active_arm_swing_right->target_rating.ConnectTo(skill_walking_phase4_right->si_target_rating[0]);

  // walking termination
  this->ci_stimulation_termination.ConnectTo(skill_termination->stimulation);

  skill_termination->co_target_activity[0].ConnectTo(fusion_abs_angle_left_shoulder_x->InputActivity(2));
  skill_termination->target_rating.ConnectTo(fusion_abs_angle_left_shoulder_x->InputTargetRating(2));
  skill_termination->co_target_output[0].ConnectTo(fusion_abs_angle_left_shoulder_x->InputPort(2, 0));

  // termination phase 3
  // left
  fusion_walking_termination_left->co_termination_phase_3.ConnectTo(skill_termination_phase3_left->stimulation);

  skill_termination_phase3_left->co_target_activity[0].ConnectTo(fusion_active_arm_swing_left_termi->InputActivity(0));
  skill_termination_phase3_left->co_target_activity[0].ConnectTo(fusion_active_arm_swing_left_termi->InputTargetRating(0));
  skill_termination_phase3_left->co_target_output[0].ConnectTo(fusion_active_arm_swing_left_termi->InputPort(0, 0));


// termi_active_arm_swing_left->activity.ConnectTo(skill_termination_phase3_left->stimulation);
// termi_active_arm_swing_left->target_rating.ConnectTo(skill_termination_phase3_left->si_target_rating[0]);

  // right
  fusion_walking_termination_right->co_termination_phase_3.ConnectTo(skill_termination_phase3_right->stimulation);

  skill_termination_phase3_right->co_target_activity[0].ConnectTo(fusion_active_arm_swing_right_termi->InputActivity(0));
  skill_termination_phase3_right->co_target_activity[0].ConnectTo(fusion_active_arm_swing_right_termi->InputTargetRating(0));
  skill_termination_phase3_right->co_target_output[0].ConnectTo(fusion_active_arm_swing_right_termi->InputPort(0, 0));


// termi_active_arm_swing_right->activity.ConnectTo(skill_termination_phase3_right->stimulation);
// termi_active_arm_swing_right->target_rating.ConnectTo(skill_termination_phase3_right->si_target_rating[0]);

  // termination phase 4
  // left
  fusion_walking_termination_left->co_termination_phase_4.ConnectTo(skill_termination_phase4_left->stimulation);

  skill_termination_phase4_left->co_target_activity[0].ConnectTo(fusion_active_arm_swing_left_termi->InputActivity(1));
  skill_termination_phase4_left->co_target_activity[0].ConnectTo(fusion_active_arm_swing_left_termi->InputTargetRating(1));
  skill_termination_phase4_left->co_target_output[0].ConnectTo(fusion_active_arm_swing_left_termi->InputPort(1, 0));



  skill_termination_phase4_left->co_target_activity[1].ConnectTo(fusion_lock_arm_swing->InputActivity(0));
  skill_termination_phase4_left->co_target_activity[1].ConnectTo(fusion_lock_arm_swing->InputTargetRating(0));
  skill_termination_phase4_left->co_target_output[1].ConnectTo(fusion_lock_arm_swing->InputPort(0, 0));


  //termi_active_arm_swing_left->activity.ConnectTo(skill_termination_phase4_left->stimulation);
  //termi_active_arm_swing_left->target_rating.ConnectTo(skill_termination_phase4_left->si_target_rating[0]);

  // right
  fusion_walking_termination_right->co_termination_phase_4.ConnectTo(skill_termination_phase4_right->stimulation);

  skill_termination_phase4_right->co_target_activity[0].ConnectTo(fusion_active_arm_swing_right_termi->InputActivity(1));
  skill_termination_phase4_right->co_target_activity[0].ConnectTo(fusion_active_arm_swing_right_termi->InputTargetRating(1));
  skill_termination_phase4_right->co_target_output[0].ConnectTo(fusion_active_arm_swing_right_termi->InputPort(1, 0));


  skill_termination_phase4_right->co_target_activity[1].ConnectTo(fusion_lock_arm_swing->InputActivity(1));
  skill_termination_phase4_right->co_target_activity[1].ConnectTo(fusion_lock_arm_swing->InputTargetRating(1));
  skill_termination_phase4_right->co_target_output[1].ConnectTo(fusion_lock_arm_swing->InputPort(1, 0));


  skill_walking_phase4_right->co_target_activity[0].ConnectTo(fusion_lock_arm_swing->InputActivity(2));
  skill_walking_phase4_right->co_target_activity[0].ConnectTo(fusion_lock_arm_swing->InputTargetRating(2));
  skill_walking_phase4_right->co_target_output[0].ConnectTo(fusion_lock_arm_swing->InputPort(2, 0));




  // left termination phase 5
  this->ci_termination_hold_position.ConnectTo(skill_termination_phase5_left->stimulation);
  skill_termination_phase5_left->co_target_activity[0].ConnectTo(fusion_abs_angle_left_shoulder_y->InputActivity(2));
  skill_termination_phase5_left->co_target_activity[0].ConnectTo(fusion_abs_angle_left_shoulder_y->InputTargetRating(2));
  skill_termination_phase5_left->co_target_output[0].ConnectTo(fusion_abs_angle_left_shoulder_y->InputPort(2, 0));

  skill_termination_phase5_left->co_target_activity[0].ConnectTo(fusion_abs_angle_right_shoulder_y->InputActivity(2));
  skill_termination_phase5_left->co_target_activity[0].ConnectTo(fusion_abs_angle_right_shoulder_y->InputTargetRating(2));
  skill_termination_phase5_left->co_target_output[0].ConnectTo(fusion_abs_angle_right_shoulder_y->InputPort(2, 0));

// termi_active_arm_swing_right->activity.ConnectTo(skill_termination_phase4_right->stimulation);
  //termi_active_arm_swing_right->target_rating.ConnectTo(skill_termination_phase4_right->si_target_rating[0]);

  // right termination phase 5
  this->ci_termination_hold_position.ConnectTo(skill_termination_phase5_right->stimulation);
  skill_termination_phase5_right->co_target_activity[0].ConnectTo(fusion_abs_angle_left_shoulder_y->InputActivity(3));
  skill_termination_phase5_right->co_target_activity[0].ConnectTo(fusion_abs_angle_left_shoulder_y->InputTargetRating(3));
  skill_termination_phase5_right->co_target_output[0].ConnectTo(fusion_abs_angle_left_shoulder_y->InputPort(3, 0));

  skill_termination_phase5_right->co_target_activity[0].ConnectTo(fusion_abs_angle_right_shoulder_y->InputActivity(3));
  skill_termination_phase5_right->co_target_activity[0].ConnectTo(fusion_abs_angle_right_shoulder_y->InputTargetRating(3));
  skill_termination_phase5_right->co_target_output[0].ConnectTo(fusion_abs_angle_right_shoulder_y->InputPort(3, 0));

}

//----------------------------------------------------------------------
// gUpperTrunkGroup destructor
//----------------------------------------------------------------------
gUpperTrunkGroup::~gUpperTrunkGroup()
{}

//----------------------------------------------------------------------
// End of namespace declaration
//----------------------------------------------------------------------
}
}
}
