//
// You received this file as part of Finroc
// A framework for intelligent robot control
//
// Copyright (C) AG Robotersysteme TU Kaiserslautern
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//
//----------------------------------------------------------------------
/*!\file    projects/biped/control/gbbLearningModuleBehaviors.cpp
 *
 * \author  Qi Liu & Jie Zhao
 *
 * \date    2014-12-12
 *
 */
//----------------------------------------------------------------------
#include "projects/biped/control/gbbLearningModuleBehaviors.h"
#include "projects/biped/learning_module/mbbLearningPhase.h"
#include "projects/biped/learning_module/mbbMPOptimization.h"
#include "projects/biped/learning_module/mbbReflexRLPushRecoveryLateralAnkle.h"
#include "projects/biped/learning_module/mbbRLPushRecoveryAnkleHip.h"
#include "projects/biped/learning_module/mbbOptimizationObstacleWalking.h"
#include "projects/biped/learning_module/mbbOptimizationSlowWalking.h"
#include "plugins/ib2c/mbbFusion.h"


//----------------------------------------------------------------------
// External includes (system with <>, local with "")
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Internal includes with ""
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Debugging
//----------------------------------------------------------------------
#include <cassert>

//----------------------------------------------------------------------
// Namespace usage
//----------------------------------------------------------------------
using namespace finroc::biped::learning_module;
using namespace finroc::ib2c;
//----------------------------------------------------------------------
// Namespace declaration
//----------------------------------------------------------------------
namespace finroc
{
namespace biped
{
namespace control
{

//----------------------------------------------------------------------
// Forward declarations / typedefs / enums
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Const values
//----------------------------------------------------------------------
runtime_construction::tStandardCreateModuleAction<gbbLearningModuleBehaviors> cCREATE_ACTION_FOR_GBB_LEARNINGMODULEBEHAVIORS("LearningModuleBehaviors");

//----------------------------------------------------------------------
// Implementation
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// gLearningModuleBehaviors constructor
//----------------------------------------------------------------------
gbbLearningModuleBehaviors::gbbLearningModuleBehaviors(core::tFrameworkElement *parent, const std::string &name,
    ib2c::tStimulationMode stimulation_mode, unsigned int number_of_inhibition_ports,
    const std::string &structure_config_file) :
  tGroup(parent, name, stimulation_mode, number_of_inhibition_ports, structure_config_file, false) // change to 'true' to make group's ports shared (so that ports in other processes can connect to its output and/or input ports)
{
  mbbLearningPhase* learning_phase = new mbbLearningPhase(this, "Learning Phase", ib2c::tStimulationMode::ENABLED);
  mbbMPOptimization* MotorPatternOptimization =  new mbbMPOptimization(this, "Optimization Motor Pattern");
  mbbOptimizationObstacleWalking* OptimizationObstacleWalking = new mbbOptimizationObstacleWalking(this, "Optimization Obstacle Walking");
  mbbOptimizationSlowWalking* OptimizationSlowWalking = new mbbOptimizationSlowWalking(this, "Optimization Slow Walking");
  mbbReflexRLPushRecoveryLateralAnkle* reflex_pr_lateral_ankle_learning = new mbbReflexRLPushRecoveryLateralAnkle(this, "Reflex Ankle Lateral Push Recovery", ib2c::tStimulationMode::DISABLED);
  mbbRLPushRecoveryAnkleHip* push_recovery_ankle_hip_torque_learning = new mbbRLPushRecoveryAnkleHip(this, "Push Recovery Ankle Hip Torque", ib2c::tStimulationMode::ENABLED);
//  mbbFusion<double>* fusion_new_running = new mbbFusion<double>(this,  "New Running", 2, tStimulationMode::ENABLED);
//  fusion_new_running->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);

  this->ci_walking_left_1.ConnectTo(learning_phase->ci_walking_phase_1_left);
  this->ci_walking_left_2.ConnectTo(learning_phase->ci_walking_phase_2_left);
  this->ci_walking_left_3.ConnectTo(learning_phase->ci_walking_phase_3_left);
  this->ci_walking_left_4.ConnectTo(learning_phase->ci_walking_phase_4_left);
  this->ci_walking_left_5.ConnectTo(learning_phase->ci_walking_phase_5_left);

  this->ci_walking_left_1.ConnectTo(MotorPatternOptimization->ci_left_phase1);
  this->ci_walking_left_2.ConnectTo(MotorPatternOptimization->ci_left_phase2);
  this->ci_walking_left_3.ConnectTo(MotorPatternOptimization->ci_left_phase3);
  this->ci_walking_left_4.ConnectTo(MotorPatternOptimization->ci_left_phase4);
  this->ci_walking_left_5.ConnectTo(MotorPatternOptimization->ci_left_phase5);

  this->ci_walking_right_1.ConnectTo(MotorPatternOptimization->ci_right_phase1);
  this->ci_walking_right_2.ConnectTo(MotorPatternOptimization->ci_right_phase2);
  this->ci_walking_right_3.ConnectTo(MotorPatternOptimization->ci_right_phase3);
  this->ci_walking_right_4.ConnectTo(MotorPatternOptimization->ci_right_phase4);
  this->ci_walking_right_5.ConnectTo(MotorPatternOptimization->ci_right_phase5);

  this->ci_com_x.ConnectTo(learning_phase->ci_com_x_fusion);
  this->ci_com_y.ConnectTo(learning_phase->ci_com_y_fusion);
  this->ci_reset_simulation_experiments.ConnectTo(learning_phase->ci_reset_simulation_experiments);
  this->ci_pitch_angle.ConnectTo(learning_phase->ci_pitch_angle);
  this->ci_roll_angle.ConnectTo(learning_phase->ci_roll_angle);
  this->ci_com_x.ConnectTo(learning_phase->ci_com_x_body);

  this->ci_walking_left_phase.ConnectTo(reflex_pr_lateral_ankle_learning->ci_left_phase);
  this->ci_walking_right_phase.ConnectTo(reflex_pr_lateral_ankle_learning->ci_right_phase);
  learning_phase->so_loop_times.ConnectTo(reflex_pr_lateral_ankle_learning->ci_loop_times);
  this->ci_com_y.ConnectTo(reflex_pr_lateral_ankle_learning->ci_com_y_body);
  this->ci_com_x.ConnectTo(reflex_pr_lateral_ankle_learning->ci_com_x_body);
  this->ci_xcom_y_left.ConnectTo(reflex_pr_lateral_ankle_learning->ci_xcom_y_left);
  this->ci_xcom_y_right.ConnectTo(reflex_pr_lateral_ankle_learning->ci_xcom_y_right);
  //this->ci_push_detected_y.ConnectTo(reflex_pr_lateral_ankle_learning->ci_push_detected_y);
  this->si_left_ankle_angle_lateral.ConnectTo(reflex_pr_lateral_ankle_learning->si_left_ankle_angle_lateral);
  this->si_right_ankle_angle_lateral.ConnectTo(reflex_pr_lateral_ankle_learning->si_right_ankle_angle_lateral);

  this->ci_left_correction_lateral.ConnectTo(reflex_pr_lateral_ankle_learning->ci_left_correction_lateral);
  this->ci_right_correction_lateral.ConnectTo(reflex_pr_lateral_ankle_learning->ci_right_correction_lateral);
  this->ci_pitch_angle.ConnectTo(reflex_pr_lateral_ankle_learning->ci_pelvis_pitch);
  this->ci_roll_angle.ConnectTo(reflex_pr_lateral_ankle_learning->ci_pelvis_roll);
  this->ci_torso_force_y.ConnectTo(reflex_pr_lateral_ankle_learning->ci_torso_force_y);
//  reflex_pr_lateral_ankle_learning->co_action.ConnectTo(this->co_lateral_ankle_pr_action);
//  reflex_pr_lateral_ankle_learning->co_left_learning_ankle_stiffness.ConnectTo(this->co_left_learning_ankle_stiffness);
//  reflex_pr_lateral_ankle_learning->co_right_learning_ankle_stiffness.ConnectTo(this->co_right_learning_ankle_stiffness);

  this->ci_walking_left_phase.ConnectTo(push_recovery_ankle_hip_torque_learning->ci_left_phase);
  this->ci_walking_right_phase.ConnectTo(push_recovery_ankle_hip_torque_learning->ci_right_phase);
  learning_phase->so_loop_times.ConnectTo(push_recovery_ankle_hip_torque_learning->ci_loop_times);
  this->ci_com_x.ConnectTo(push_recovery_ankle_hip_torque_learning->ci_com_x_body);
  this->ci_xcom_x_left.ConnectTo(push_recovery_ankle_hip_torque_learning->ci_xcom_x_left);
  this->ci_xcom_x_right.ConnectTo(push_recovery_ankle_hip_torque_learning->ci_xcom_x_right);
  this->ci_push_started_x.ConnectTo(push_recovery_ankle_hip_torque_learning->ci_push_started_x);
  this->ci_push_detected_x.ConnectTo(push_recovery_ankle_hip_torque_learning->ci_push_detected_x);
  this->si_left_ankle_angle.ConnectTo(push_recovery_ankle_hip_torque_learning->si_left_ankle_torque_sagittal);
  this->si_right_ankle_angle.ConnectTo(push_recovery_ankle_hip_torque_learning->si_right_ankle_torque_sagittal);
  this->si_left_knee_angle.ConnectTo(push_recovery_ankle_hip_torque_learning->si_left_knee_angle);
  this->si_right_knee_angle.ConnectTo(push_recovery_ankle_hip_torque_learning->si_right_knee_angle);
  this->si_left_hip_angle.ConnectTo(push_recovery_ankle_hip_torque_learning->si_left_hip_angle);
  this->si_right_hip_angle.ConnectTo(push_recovery_ankle_hip_torque_learning->si_right_hip_angle);
  this->si_left_ankle_angle.ConnectTo(push_recovery_ankle_hip_torque_learning->si_left_ankle_angle);
  this->si_right_ankle_angle.ConnectTo(push_recovery_ankle_hip_torque_learning->si_right_ankle_angle);

  this->ci_left_correction_sagittal.ConnectTo(push_recovery_ankle_hip_torque_learning->ci_left_correction_sagittal);
  this->ci_right_correction_sagittal.ConnectTo(push_recovery_ankle_hip_torque_learning->ci_right_correction_sagittal);
  this->ci_left_correction_lateral.ConnectTo(push_recovery_ankle_hip_torque_learning->ci_left_correction_lateral);
  this->ci_right_correction_lateral.ConnectTo(push_recovery_ankle_hip_torque_learning->ci_right_correction_lateral);
  this->ci_pitch_angle.ConnectTo(push_recovery_ankle_hip_torque_learning->ci_pelvis_pitch);
  this->ci_roll_angle.ConnectTo(push_recovery_ankle_hip_torque_learning->ci_pelvis_roll);
  this->ci_torso_force_x.ConnectTo(push_recovery_ankle_hip_torque_learning->ci_torso_force_x);

  //knee strategy
  push_recovery_ankle_hip_torque_learning->co_left_learning_torque_factor.ConnectTo(this->co_left_learning_ankle_stiffness);
  push_recovery_ankle_hip_torque_learning->co_right_learning_torque_factor.ConnectTo(this->co_right_learning_ankle_stiffness);

  // ankle hip strategy
//  push_recovery_ankle_hip_torque_learning->co_left_learning_torque_factor.ConnectTo(this->co_left_learning_torque_factor);
//  push_recovery_ankle_hip_torque_learning->co_right_learning_torque_factor.ConnectTo(this->co_right_learning_torque_factor);

  learning_phase->so_loop_times.ConnectTo(this->so_loop_times);

  learning_phase->so_loop_times.ConnectTo(MotorPatternOptimization->ci_loop_times);

  this->si_pelvis_position_x.ConnectTo(MotorPatternOptimization->si_pelvis_position_x);
  this->si_lock_hip_left_torque.ConnectTo(MotorPatternOptimization->si_lock_hip_left_torque);
  this->si_lock_hip_right_torque.ConnectTo(MotorPatternOptimization->si_lock_hip_right_torque);

  this->ci_pitch_angle.ConnectTo(MotorPatternOptimization->ci_pitch_angle);
  this->ci_roll_angle.ConnectTo(MotorPatternOptimization->ci_roll_angle);
  this->ci_com_x.ConnectTo(MotorPatternOptimization->ci_com_x_body);
  this->ci_com_y.ConnectTo(MotorPatternOptimization->ci_com_y_body);
  this->ci_com_x_left.ConnectTo(MotorPatternOptimization->ci_com_left_x);
  this->ci_com_x_right.ConnectTo(MotorPatternOptimization->ci_com_right_x);
  this->ci_left_correction_sagittal.ConnectTo(MotorPatternOptimization->ci_left_correction);
  this->ci_right_correction_sagittal.ConnectTo(MotorPatternOptimization->ci_right_correction);
  this->ci_xcom_x_left.ConnectTo(MotorPatternOptimization->ci_xcom_left_x);
  this->ci_xcom_x_right.ConnectTo(MotorPatternOptimization->ci_xcom_right_x);
  this->ci_linear_velocity_x.ConnectTo(MotorPatternOptimization->ci_velocity_x);
  this->ci_walking_scale.ConnectTo(MotorPatternOptimization->ci_walking_scale);

  this->si_left_hip_angle.ConnectTo(MotorPatternOptimization->si_hip_angle);
  this->si_lock_hip_left_torque.ConnectTo(MotorPatternOptimization->si_lock_hip_left_torque);
  this->si_lock_hip_right_torque.ConnectTo(MotorPatternOptimization->si_lock_hip_right_torque);
  //this->si_walking_velocity.ConnectTo(MotorPatternOptimization->si_walking_velocity);
  this->si_left_ankle_angle.ConnectTo(MotorPatternOptimization->si_left_ankle_angle);
  this->si_right_ankle_angle.ConnectTo(MotorPatternOptimization->si_right_ankle_angle);
  this->si_pelvis_position_x.ConnectTo(MotorPatternOptimization->si_pelvis_position_x);
  this->si_pelvis_position_y.ConnectTo(MotorPatternOptimization->si_pelvis_position_y);

  MotorPatternOptimization->co_ankle_output1.ConnectTo(this->co_leg_propel_action1);
  MotorPatternOptimization->co_ankle_output2.ConnectTo(this->co_leg_propel_action2);
  MotorPatternOptimization->co_ankle_output3.ConnectTo(this->co_leg_propel_action3);
  MotorPatternOptimization->co_ankle_output4.ConnectTo(this->co_leg_propel_action4);

  MotorPatternOptimization->co_hip_output1.ConnectTo(this->co_hip_swing_action1);
  MotorPatternOptimization->co_hip_output2.ConnectTo(this->co_hip_swing_action2);
  MotorPatternOptimization->co_hip_output3.ConnectTo(this->co_hip_swing_action3);
  MotorPatternOptimization->co_hip_output4.ConnectTo(this->co_hip_swing_action4);

//  MotorPatternOptimization->so_new_running.ConnectTo(fusion_new_running->InputActivity(0));
//  MotorPatternOptimization->target_rating.ConnectTo(fusion_new_running->InputTargetRating(0));
//  MotorPatternOptimization->activity.ConnectTo(fusion_new_running->InputPort(0, 0));
//
//  reflex_pr_lateral_ankle_learning->so_new_running.ConnectTo(fusion_new_running->InputActivity(1));
//  reflex_pr_lateral_ankle_learning->target_rating.ConnectTo(fusion_new_running->InputTargetRating(1));
//  reflex_pr_lateral_ankle_learning->activity.ConnectTo(fusion_new_running->InputPort(1, 0));

//  fusion_new_running->activity.ConnectTo(this->so_new_running);
//  reflex_pr_lateral_ankle_learning->so_new_running.ConnectTo(this->so_new_running);
//  push_recovery_ankle_hip_torque_learning->so_new_running.ConnectTo(this->so_new_running);

  // Optimization Obstacle Walking
  this->ci_walking_stimulation.ConnectTo(OptimizationObstacleWalking->ci_walking_stimulation);
  this->ci_foot_load_left.ConnectTo(OptimizationObstacleWalking->ci_foot_load_left);
  this->ci_foot_load_right.ConnectTo(OptimizationObstacleWalking->ci_foot_load_right);
  this->ci_walking_left_1.ConnectTo(OptimizationObstacleWalking->ci_left_phase1);
  this->ci_walking_left_2.ConnectTo(OptimizationObstacleWalking->ci_left_phase2);
  this->ci_walking_left_3.ConnectTo(OptimizationObstacleWalking->ci_left_phase3);
  this->ci_walking_left_4.ConnectTo(OptimizationObstacleWalking->ci_left_phase4);
  this->ci_walking_left_5.ConnectTo(OptimizationObstacleWalking->ci_left_phase5);
  this->ci_walking_right_1.ConnectTo(OptimizationObstacleWalking->ci_right_phase1);
  this->ci_walking_right_2.ConnectTo(OptimizationObstacleWalking->ci_right_phase2);
  this->ci_walking_right_3.ConnectTo(OptimizationObstacleWalking->ci_right_phase3);
  this->ci_walking_right_4.ConnectTo(OptimizationObstacleWalking->ci_right_phase4);
  this->ci_walking_right_5.ConnectTo(OptimizationObstacleWalking->ci_right_phase5);
  learning_phase->so_loop_times.ConnectTo(OptimizationObstacleWalking->ci_loop_times);
  this->ci_pitch_angle.ConnectTo(OptimizationObstacleWalking->ci_pitch_angle);
  this->ci_roll_angle.ConnectTo(OptimizationObstacleWalking->ci_roll_angle);
  this->ci_com_x.ConnectTo(OptimizationObstacleWalking->ci_com_x_body);
  this->ci_com_y.ConnectTo(OptimizationObstacleWalking->ci_com_y_body);

  this->si_pelvis_position_x.ConnectTo(OptimizationObstacleWalking->si_pelvis_position_x);
  this->si_pelvis_position_y.ConnectTo(OptimizationObstacleWalking->si_pelvis_position_y);
  this->si_left_foot_position_x.ConnectTo(OptimizationObstacleWalking->si_left_foot_position_x);
  this->si_left_foot_position_y.ConnectTo(OptimizationObstacleWalking->si_left_foot_position_y);
  this->si_left_foot_position_z.ConnectTo(OptimizationObstacleWalking->si_left_foot_position_z);
  this->si_right_foot_position_x.ConnectTo(OptimizationObstacleWalking->si_right_foot_position_x);
  this->si_right_foot_position_y.ConnectTo(OptimizationObstacleWalking->si_right_foot_position_y);
  this->si_right_foot_position_z.ConnectTo(OptimizationObstacleWalking->si_right_foot_position_z);
  this->si_left_hip_angle.ConnectTo(OptimizationObstacleWalking->si_left_hip_angle);
  this->si_right_hip_angle.ConnectTo(OptimizationObstacleWalking->si_right_hip_angle);

  this->co_obstacle_walking_stimulation.ConnectTo(OptimizationObstacleWalking->co_obstacle_walking_stimulation);
  this->co_output_hip_swing_1.ConnectTo(OptimizationObstacleWalking->co_output_hip_swing_1);
  this->co_output_hip_swing_2.ConnectTo(OptimizationObstacleWalking->co_output_hip_swing_2);
  this->co_output_hip_swing_3.ConnectTo(OptimizationObstacleWalking->co_output_hip_swing_3);
  this->co_output_hip_swing_3_rear.ConnectTo(OptimizationObstacleWalking->co_output_hip_swing_3_rear);
  this->co_output_knee_flexion_1.ConnectTo(OptimizationObstacleWalking->co_output_knee_flexion_1);
  this->co_output_knee_flexion_2.ConnectTo(OptimizationObstacleWalking->co_output_knee_flexion_2);
  this->co_output_knee_flexion_2_rear.ConnectTo(OptimizationObstacleWalking->co_output_knee_flexion_2_rear);
  this->co_output_lock_hip_1.ConnectTo(OptimizationObstacleWalking->co_output_lock_hip_1);
  this->co_output_lock_hip_2.ConnectTo(OptimizationObstacleWalking->co_output_lock_hip_2);
  this->co_output_lock_hip_3.ConnectTo(OptimizationObstacleWalking->co_output_lock_hip_3);
  this->co_obstacle_step.ConnectTo(OptimizationObstacleWalking->co_obstacle_step);

  OptimizationObstacleWalking->so_new_running.ConnectTo(this->so_new_running);

  // PSO Slow walking TODO check this
  this->co_slow_walking_stimulation.ConnectTo(OptimizationSlowWalking->co_slow_walking_stimulation);
  this->co_slow_walking_on.ConnectTo(OptimizationSlowWalking->co_slow_walking_on);
  this->co_fast_walking_stimulation.ConnectTo(OptimizationSlowWalking->co_fast_walking_stimulation);
  this->co_fast_walking_on.ConnectTo(OptimizationSlowWalking->co_fast_walking_on);
  this->so_loop_times.ConnectTo(OptimizationSlowWalking->ci_loop_times);
  this->ci_walking_left_2.ConnectTo(OptimizationSlowWalking->ci_phase2_left_stimulation);
  this->ci_walking_right_2.ConnectTo(OptimizationSlowWalking->ci_phase2_right_stimulation);
//  this->si_left_knee_angle.ConnectTo(OptimizationSlowWalking->si_left_knee_angle);
//  this->si_right_knee_angle.ConnectTo(OptimizationSlowWalking->si_right_knee_angle);
  // Lock Knee Reflex
//  this->co_knee_angle.ConnectTo(OptimizationSlowWalking->ci_knee_angle);
//  this->co_knee_velocity.ConnectTo(OptimizationSlowWalking->ci_knee_velocity);
  this->co_walking_velocity.ConnectTo(OptimizationSlowWalking->ci_walking_velocity);
  this->co_walking_distance.ConnectTo(OptimizationSlowWalking->ci_walking_distance);
  this->co_step_length.ConnectTo(OptimizationSlowWalking->ci_step_length);
  this->co_slow_walking_ahs_max_torque.ConnectTo(OptimizationSlowWalking->co_output_ahs_max_torque);
  this->co_slow_walking_ahs_t1.ConnectTo(OptimizationSlowWalking->co_output_ahs_t1);
  this->co_slow_walking_ahs_t2.ConnectTo(OptimizationSlowWalking->co_output_ahs_t2);
  this->co_slow_walking_lp_max_torque.ConnectTo(OptimizationSlowWalking->co_output_lp_max_torque);
  this->co_slow_walking_lp_t1.ConnectTo(OptimizationSlowWalking->co_output_lp_t1);
  this->co_slow_walking_lp_t2.ConnectTo(OptimizationSlowWalking->co_output_lp_t2);
  this->co_slow_walking_lh_par1.ConnectTo(OptimizationSlowWalking->co_output_lh_par1);
  this->co_slow_walking_lh_par2.ConnectTo(OptimizationSlowWalking->co_output_lh_par2);
  this->co_fast_walking_lh_par1.ConnectTo(OptimizationSlowWalking->co_output_lh_par1);
  this->co_fast_walking_lh_par2.ConnectTo(OptimizationSlowWalking->co_output_lh_par2);
  this->co_slow_walking_knee_angle.ConnectTo(OptimizationSlowWalking->co_output_knee_angle);
  this->co_slow_walking_knee_angle_low.ConnectTo(OptimizationSlowWalking->co_output_lk_knee_angle_low);
  this->co_slow_walking_knee_angle_high.ConnectTo(OptimizationSlowWalking->co_output_lk_knee_angle_high);
  this->co_slow_walking_knee_velocity.ConnectTo(OptimizationSlowWalking->co_output_lk_knee_velocity);
  this->co_slow_walking_lk_hip_angle_low.ConnectTo(OptimizationSlowWalking->co_output_lk_hip_angle_low);
  this->co_slow_walking_lk_hip_angle_high.ConnectTo(OptimizationSlowWalking->co_output_lk_hip_angle_high);
  this->co_slow_walking_lk_increment_par1.ConnectTo(OptimizationSlowWalking->co_output_lk_increment_par1);
  this->co_slow_walking_lk_increment_par2.ConnectTo(OptimizationSlowWalking->co_output_lk_increment_par2);
  this->co_slow_walking_max_impulse.ConnectTo(OptimizationSlowWalking->co_output_max_impulse);
  this->co_slow_walk_stimulation_knee_angle.ConnectTo(OptimizationSlowWalking->co_output_stimulation_knee_angle);
  this->co_slow_walk_stimulation_knee_angle_activity.ConnectTo(OptimizationSlowWalking->co_output_stimulation_knee_angle_activity);
  // Active hip swing
//  this->co_left_learning_torque_factor.ConnectTo(OptimizationSlowWalking->co_output_active_hip_swing_max_torque);
  //this->co_right_learning_torque_factor.ConnectTo(OptimizationSlowWalking->co_output_active_hip_swing_right_max_torque);
  this->ci_pitch_angle.ConnectTo(OptimizationSlowWalking->ci_pitch_angle);
  this->ci_roll_angle.ConnectTo(OptimizationSlowWalking->ci_roll_angle);
  this->ci_com_x.ConnectTo(OptimizationSlowWalking->ci_com_x_body);
}

//----------------------------------------------------------------------
// gbbLearningModuleBehaviors destructor
//----------------------------------------------------------------------
gbbLearningModuleBehaviors::~gbbLearningModuleBehaviors()
{}


//----------------------------------------------------------------------
// End of namespace declaration
//----------------------------------------------------------------------
}
}
}
