//
// You received this file as part of Finroc
// A Framework for intelligent robot control
//
// Copyright (C) Finroc GbR (finroc.org)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//----------------------------------------------------------------------
/*!\file    projects/biped/control/gUpperTrunkGroup.h
 *
 * \author  Tobias Luksch
 * \author  Jie Zhao
 *
 * \date    2013-10-09
 *
 * \brief Contains gUpperTrunkGroup
 *
 * \b gUpperTrunkGroup
 *
 *
 * The group for the upper trunk control in bipedal locomotion!
 *
 */
//----------------------------------------------------------------------
#ifndef __projects__biped__control__gUpperTrunkGroup_h__
#define __projects__biped__control__gUpperTrunkGroup_h__

#include "plugins/structure/tSenseControlGroup.h"
//#ifdef ENABLE_LEARNING
//#include "reinforcement_learning/tLearningModuleIndex.h"
//#endif
//----------------------------------------------------------------------
// External includes (system with <>, local with "")
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Internal includes with ""
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Namespace declaration
//----------------------------------------------------------------------
namespace finroc
{
namespace biped
{
namespace control
{

//----------------------------------------------------------------------
// Forward declarations / typedefs / enums
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Class declaration
//----------------------------------------------------------------------
//! SHORT_DESCRIPTION
/*!
 *
 * The group for the upper trunk control in bipedal locomotion!
 */
class gUpperTrunkGroup : public structure::tSenseControlGroup
{

//----------------------------------------------------------------------
// Ports (These are the only variables that may be declared public)
//----------------------------------------------------------------------
public:

  tControllerInput<bool> ci_reset_simulation_experiments;
  tControllerInput<double> ci_stimulation_stand;
  tControllerInput<double> ci_stiffness_factor;
  tControllerInput<double> ci_quiet_stand;
  tControllerInput<double> ci_sensor_monitor;
  tControllerInput<double> ci_ground_contact;
  tControllerInput<double> ci_actual_accel;
  tControllerInput<double>  ci_actual_omega_y;
  tControllerInput<double> ci_actual_pitch;
  tControllerInput<double> ci_adjusted_roll;
  tControllerInput<double> ci_adjusted_pitch;
  tControllerInput<double> ci_left_stimulation_walking_initiation;
  tControllerInput<double> ci_right_stimulation_walking_initiation;
  tControllerInput<double> ci_activity_walking;
  tControllerInput<double> ci_last_step_state;
  tControllerInput<double> ci_walking_scale_factor;
  tControllerInput<double> ci_termination_scale_factor;
  tControllerInput<double> ci_for_non_used_fusion_modules;

  // walking phases
  tControllerInput<double> ci_left_stimulation_walking_phase_1;
  tControllerInput<double> ci_left_stimulation_walking_phase_2;
  tControllerInput<double> ci_left_stimulation_walking_phase_3;
  tControllerInput<double> ci_left_stimulation_walking_phase_4;
  tControllerInput<double> ci_left_stimulation_walking_phase_5;
  tControllerInput<double> ci_left_stimulation_lift_leg;
  tControllerInput<double> ci_right_stimulation_walking_phase_1;
  tControllerInput<double> ci_right_stimulation_walking_phase_2;
  tControllerInput<double> ci_right_stimulation_walking_phase_3;
  tControllerInput<double> ci_right_stimulation_walking_phase_4;
  tControllerInput<double> ci_right_stimulation_walking_phase_5;
  tControllerInput<double> ci_right_stimulation_lift_leg;

  //WALKING TERMINATION
  tControllerInput<double> ci_stimulation_termination;
  tControllerInput<double> ci_termination_hold_position;
  tControllerInput<double> ci_termi_last_step;
  tControllerInput<double> ci_push_recovery_stand;
  tControllerInput<double> ci_push_recovery_walking;
  tControllerInput<double> ci_angle_body_space;
  tControllerInput<double> ci_target_angle_arm_trunk;
  tControllerInput<double> ci_angle_body_space_lateral;



  // controller outputs
  tControllerOutput<double> co_left_stimulation_shoulder_x_torque;
  tControllerOutput<double> co_left_shoulder_x_torque;
  tControllerOutput<double> co_left_stimulation_shoulder_x_angle;
  tControllerOutput<double> co_left_shoulder_x_angle;
  tControllerOutput<double> co_left_stimulation_shoulder_y_torque;
  tControllerOutput<double> co_left_shoulder_y_torque;
  tControllerOutput<double> co_left_stimulation_shoulder_y_angle;
  tControllerOutput<double> co_left_shoulder_y_angle;
  tControllerOutput<double> co_left_stimulation_elbow_torque;
  tControllerOutput<double> co_left_elbow_torque;
  tControllerOutput<double> co_left_stimulation_elbow_angle;
  tControllerOutput<double> co_left_elbow_angle;

  tControllerOutput<double> co_right_stimulation_shoulder_x_torque;
  tControllerOutput<double> co_right_shoulder_x_torque;
  tControllerOutput<double> co_right_stimulation_shoulder_x_angle;
  tControllerOutput<double> co_right_shoulder_x_angle;
  tControllerOutput<double> co_right_stimulation_shoulder_y_torque;
  tControllerOutput<double> co_right_shoulder_y_torque;
  tControllerOutput<double> co_right_stimulation_shoulder_y_angle;
  tControllerOutput<double> co_right_shoulder_y_angle;
  tControllerOutput<double> co_right_stimulation_elbow_torque;
  tControllerOutput<double> co_right_elbow_torque;
  tControllerOutput<double> co_right_stimulation_elbow_angle;
  tControllerOutput<double> co_right_elbow_angle;
  tControllerOutput<bool> co_fast_walking;
  tControllerOutput<int> co_loop_count;

  // sensor inputs
  tSensorInput<double> si_left_shoulder_x_torque;
  tSensorInput<double> si_left_shoulder_x_angle;
  tSensorInput<double> si_left_shoulder_y_torque;
  tSensorInput<double> si_left_shoulder_y_angle;
  tSensorInput<double> si_left_elbow_torque;
  tSensorInput<double> si_left_elbow_angle;

  tSensorInput<double> si_right_shoulder_x_torque;
  tSensorInput<double> si_right_shoulder_x_angle;
  tSensorInput<double> si_right_shoulder_y_torque;
  tSensorInput<double> si_right_shoulder_y_angle;
  tSensorInput<double> si_right_elbow_torque;
  tSensorInput<double> si_right_elbow_angle;

  // sensor outputs
  tSensorOutput<double> so_upper_trunk_co_log_updated;
  tSensorOutput<double> so_left_shoulder_x_torque;
  tSensorOutput<double> so_left_shoulder_x_angle;
  tSensorOutput<double> so_left_shoulder_y_torque;
  tSensorOutput<double> so_left_shoulder_y_angle;
  tSensorOutput<double> so_left_elbow_torque;
  tSensorOutput<double> so_left_elbow_angle;

  tSensorOutput<double> so_right_shoulder_x_torque;
  tSensorOutput<double> so_right_shoulder_x_angle;
  tSensorOutput<double> so_right_shoulder_y_torque;
  tSensorOutput<double> so_right_shoulder_y_angle;
  tSensorOutput<double> so_right_elbow_torque;
  tSensorOutput<double> so_right_elbow_angle;


//----------------------------------------------------------------------
// Public methods and typedefs
//----------------------------------------------------------------------
public:

  gUpperTrunkGroup(core::tFrameworkElement *parent, const std::string &name = "UpperTrunkGroup",
                   const std::string &structure_config_file = __FILE__".xml");

//----------------------------------------------------------------------
// Private fields and methods
//----------------------------------------------------------------------
private:

  /*! Destructor
   *
   * The destructor of modules is declared private to avoid accidental deletion. Deleting
   * modules is already handled by the framework.
   */
  ~gUpperTrunkGroup();

};

//----------------------------------------------------------------------
// End of namespace declaration
//----------------------------------------------------------------------
}
}
}



#endif
