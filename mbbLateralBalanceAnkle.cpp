//
// You received this file as part of Finroc
// A Framework for intelligent robot control
//
// Copyright (C) Finroc GbR (finroc.org)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//----------------------------------------------------------------------
/*!\file    projects/biped/reflexes/mbbLateralBalanceAnkle.cpp
 *
 * \author  Jie Zhao & Tobias Luksch
 *
 * \date    2013-09-25
 *
 */
//----------------------------------------------------------------------
#include "projects/biped/reflexes/mbbLateralBalanceAnkle.h"
#include "rrlib/math/utilities.h"
#include "plugins/data_ports/tChangeContext.h"
#include "plugins/data_ports/definitions.h"
//----------------------------------------------------------------------
// External includes (system with <>, local with "")
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Internal includes with ""
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Debugging
//----------------------------------------------------------------------
#include <cassert>

//----------------------------------------------------------------------
// Namespace usage
//----------------------------------------------------------------------
using namespace rrlib::math;
//----------------------------------------------------------------------
// Namespace declaration
//----------------------------------------------------------------------
namespace finroc
{
namespace biped
{
namespace reflexes
{

//----------------------------------------------------------------------
// Forward declarations / typedefs / enums
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Const values
//----------------------------------------------------------------------
runtime_construction::tStandardCreateModuleAction<mbbLateralBalanceAnkle> cCREATE_ACTION_FOR_MBB_LATERALBALANCEANKLE("LateralBalanceAnkle");

//----------------------------------------------------------------------
// Implementation
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// mbbLateralBalanceAnkle constructor
//----------------------------------------------------------------------
mbbLateralBalanceAnkle::mbbLateralBalanceAnkle(core::tFrameworkElement *parent, const std::string &name,
    ib2c::tStimulationMode stimulation_mode, unsigned int number_of_inhibition_ports) :
  tModule(parent, name, stimulation_mode, number_of_inhibition_ports),
  max_torque(0.0),
  factor(0.0),
  com_error(0.0),
  ankle_angle(0.0),
  walking_scale(0.0),
  target_torque(0.0),
  target_angle(0.0),
  force_factor(0.0),
  integral_factor(0.0),
  integral_part(0.0),
  calculated_activity(0.0),
  turn_signal(0.0),
  slow_walking_max_torque(0),
  slow_walking_factor(0),
  fast_walking_max_torque(0),
  fast_walking_factor(0)
{
  this->par_factor.Set(2.5);//2.5
  this->par_max_torque.Set(0.2);
  this->par_integral_factor.Set(0.0);
  this->par_slow_walking_factor.Set(8);
  this->par_slow_walking_max_torque.Set(0.65);
  this->par_fast_walking_factor.Set(2.5);
  this->par_fast_walking_max_torque.Set(0.2);
  this->ci_reset_simulation.AddPortListenerSimple(*this);
}

//----------------------------------------------------------------------
// mbbLateralBalanceAnkle destructor
//----------------------------------------------------------------------
mbbLateralBalanceAnkle::~mbbLateralBalanceAnkle()
{}

/*
//----------------------------------------------------------------------
// mbbLateralBalanceAnkle OnStaticParameterChange
//----------------------------------------------------------------------
void mbbLateralBalanceAnkle::OnStaticParameterChange()
{
  tModule::OnStaticParameterChange();

  if (this->static_parameter_1.HasChanged())
  {
    As this static parameter has changed, do something with its value!
  }
}
*/
//----------------------------------------------------------------------
// mbbLateralBalanceAnkle OnParameterChange
//----------------------------------------------------------------------
void mbbLateralBalanceAnkle::OnParameterChange()
{
  tModule::OnParameterChange();
  /*
    Add handling of your own parameters here.
    If this method is called, at least on of our parameters has changed. However, each can be checked using its .HasChanged() method.
    */
  this->factor = this->par_factor.Get();
  this->max_torque = this->par_max_torque.Get();
  this->integral_factor = this->par_integral_factor.Get();
  this->slow_walking_factor = this->par_slow_walking_factor.Get();
  this->slow_walking_max_torque = this->par_slow_walking_max_torque.Get();
  this->fast_walking_factor = this->par_fast_walking_factor.Get();
  this->fast_walking_max_torque = this->par_fast_walking_max_torque.Get();
}

void mbbLateralBalanceAnkle::OnPortChange(data_ports::tChangeContext& change_context)
{
  if ((this->ci_reset_simulation == change_context.Origin()))
  {
    this->reset_simulation = true;
  }
}

//----------------------------------------------------------------------
// mbbLateralBalanceAnkle ProcessTransferFunction
//----------------------------------------------------------------------
bool mbbLateralBalanceAnkle::ProcessTransferFunction(double activation)
{
  // inputs
  //Get your special Controller Inputs here
  this->com_error = LimitedValue<double>(this->ci_com_error_y.Get(), -0.2, 0.2);
  this->ankle_angle = this->si_ankle_x_angle.Get();
  this->walking_scale = this->ci_walking_scale.Get();
  this->integral_part += this->com_error * this->integral_factor;
  this->integral_part = LimitedValue<double>(this->integral_part, -0.1, 0.1);
  this->force_factor = LimitedValue<double>(-this->si_force_z.Get() * 2.5, 0.0, 1.0);
  this->com_error = this->com_error + this->integral_part;
  if (this->ci_turn_left_signal.Get() > 0)
    this->turn_signal = this->ci_turn_left_signal.Get();
  else if (this->ci_turn_right_signal.Get() > 0)
    this->turn_signal = this->ci_turn_right_signal.Get();
  else
    this->turn_signal = 0.0;
  // transfer function

  if( this->walking_scale < 0.76 )
  {
	  this->slow_walking_factor = 11;
	  this->slow_walking_max_torque = 0.9;
  }

  if( this->walking_scale < 0.72 )
   {
 	  this->slow_walking_factor = 12;
 	  this->slow_walking_max_torque = 1;
   }

  if( this->ci_slow_walking.Get() )
  {
	  this->max_torque = this->slow_walking_max_torque;
	  this->factor     = this->slow_walking_factor;
  }
  else if( this->ci_fast_walking.Get() )
  {
	  this->max_torque = this->fast_walking_max_torque;
	  this->factor     = this->fast_walking_factor;
  }
  else
  {
	  this->max_torque = 0.2;
	  this->factor     = 2.5;
  }

  if (activation == 0.0)
  {
    this->target_torque = 0.0;
    this->target_angle = 0.5;
    this->co_activity_angle.Publish(0.0);
    this->co_activity_torque.Publish(0.0);
  }
  else
  {
    //if ((this->ankle_angle > 0.485) && (this->ankle_angle < 0.5))
    // {
    this->target_torque = LimitedValue<double>(this->com_error * this->factor * this->force_factor, -this->max_torque, this->max_torque) * this->walking_scale; // * 1.4;
    if (this->turn_signal !=  0.0)
      this->target_torque = this->target_torque * cos(1.57 * this->turn_signal);

    this->co_activity_torque.Publish(LimitedValue<double>(fabs(this->target_torque / this->max_torque), 0.0, 1.0)); // * this->si_ground_contact.Get());
    this->co_activity_angle.Publish(0.0);
    //}
//     if (this->ankle_angle <= 0.49)
//    {
//      this->target_angle = this->ankle_angle + fabs(this->com_error) * 0.4;
//      float control_activity = 1.0 - this->ankle_angle / this->target_angle;
//      control_activity = LimitedValue<double>(control_activity + 0.5, 0.0, 1.0);
//      this->co_activity_angle.Publish(control_activity);
//      this->co_activity_torque.Publish(0.0);
//    }
//    if ((this->ankle_angle < 0.46) && (fabs(this->ci_estim_com_y.Get()) > 0.045))
//    {
//      this->target_angle = this->ankle_angle + fabs(this->ci_estim_com_y.Get());
//      this->target_angle = LimitedValue<double>(this->target_angle, 0.48, 0.55);
//      this->co_activity_angle.Publish(0.5);
//      this->co_activity_torque.Publish(0.0);
//      this->co_activity_torque.Publish(0.0);
//    }
//
//    if ((this->ankle_angle > 0.57) && (fabs(this->ci_estim_com_y.Get()) > 0.045))
//    {
//      this->target_angle = this->ankle_angle - fabs(this->ci_estim_com_y.Get());
//      this->target_angle = LimitedValue<double>(this->target_angle, 0.48, 0.55);
//      this->co_activity_angle.Publish(0.5);
//      this->co_activity_torque.Publish(0.0);
//      this->co_activity_torque.Publish(0.0);
//    }
  }

  //outputs
  this->co_ankle_x_torque.Publish(this->target_torque);
  this->co_ankle_x_angle.Publish(this->target_angle);
  if ((this->reset_simulation.exchange(false)) || (this->ci_reset_simulation_experiments.HasChanged()))
  {
//    FINROC_LOG_PRINT(DEBUG_WARNING, "RESET SIMULATION");
    this->integral_part = 0.0;
    this->target_torque = 0.0;
    this->target_angle = 0.5;
    this->co_activity_angle.Publish(0.0);
    this->co_activity_torque.Publish(0.0);
  }
  this->co_integral.Publish(this->integral_part);

  //Use your input and output ports to transfer data through this module.
  //Return whether this transfer was successful or not.

  return true;
}

//----------------------------------------------------------------------
// mbbLateralBalanceAnkle CalculateActivity
//----------------------------------------------------------------------
ib2c::tActivity mbbLateralBalanceAnkle::CalculateActivity(std::vector<ib2c::tActivity> &derived_activity, double activation) const
{
  //Return a meaningful activity value here. You also want to set the derived activity vector, if you registered derived activities e.g. in the constructor.
  return this->calculated_activity * activation;
}

//----------------------------------------------------------------------
// mbbLateralBalanceAnkle CalculateTargetRating
//----------------------------------------------------------------------
ib2c::tTargetRating mbbLateralBalanceAnkle::CalculateTargetRating(double activation) const
{
  //Return a meaningful target rating here. Choosing 0.5 just because you have no better idea is not "meaningful"!
// If you do not want to use the activation in this calculation remove its name to rid of the warning.
  return LimitedValue<double>(fabs(this->com_error * 2.0), 0.0, 1.0);
}

//----------------------------------------------------------------------
// End of namespace declaration
//----------------------------------------------------------------------
}
}
}
