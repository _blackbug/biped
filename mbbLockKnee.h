//
// You received this file as part of Finroc
// A Framework for intelligent robot control
//
// Copyright (C) Finroc GbR (finroc.org)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//----------------------------------------------------------------------
/*!\file    projects/biped/reflexes/mbbLockKnee.h
 *
 * \author  Tobias Luksch & Jie Zhao
 *
 * \date    2013-10-16
 *
 * \brief Contains mbbLockKnee
 *
 * \b mbbLockKnee
 *
 *
 * Lock the knee joint during walking.
 *
 */
//----------------------------------------------------------------------
#ifndef __projects__biped__reflexes__mbbLockKnee_h__
#define __projects__biped__reflexes__mbbLockKnee_h__

#include "plugins/ib2c/tModule.h"
//----------------------------------------------------------------------
// External includes (system with <>, local with "")
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Internal includes with ""
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Namespace declaration
//----------------------------------------------------------------------
namespace finroc
{
namespace biped
{
namespace reflexes
{

//----------------------------------------------------------------------
// Forward declarations / typedefs / enums
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Class declaration
//----------------------------------------------------------------------
//! SHORT_DESCRIPTION
/*!
 *
 * Lock the knee joint during walking.
 */
class mbbLockKnee : public ib2c::tModule
{

//----------------------------------------------------------------------
// Ports (These are the only variables that may be declared public)
//----------------------------------------------------------------------
public:

  tParameter<double> par_angle_knee;   //Example for a runtime parameter. Replace or delete it!
  tParameter<double> par_activation_angle_knee;
  tParameter<double> par_increment;
  tParameter<double> par_initial_stiffness;
  tParameter<bool> par_slow_walking_on;

  tInput<double> ci_stimulation_init;   //Example for input ports. Replace or delete them!
  tInput<double> si_knee_angle;
  tInput<double> si_ground_contact;
  tInput<double> si_hip_angle;

  tInput<double> ci_slow_walking_knee_angle;
  tInput<double> ci_slow_walking_knee_angle_low;
  tInput<double> ci_slow_walking_knee_angle_high;
  tInput<double> ci_slow_walking_knee_velocity;
  tInput<double> ci_slow_walking_hip_angle_low;
  tInput<double> ci_slow_walking_hip_angle_high;
  tInput<double> ci_slow_walking_increment_par1;
  tInput<double> ci_slow_walking_increment_par2;

  tOutput<double> co_knee_angle;   //Examples for output ports. Replace or delete them!
  tOutput<double> co_knee_velocity;
  tOutput<double> co_hip_angle;

//----------------------------------------------------------------------
// Public methods and typedefs
//----------------------------------------------------------------------
public:

  mbbLockKnee(core::tFrameworkElement *parent, const std::string &name = "LockKnee",
              ib2c::tStimulationMode stimulation_mode = ib2c::tStimulationMode::AUTO, unsigned int number_of_inhibition_ports = 0,
              double activation_angle = 0.0);

//----------------------------------------------------------------------
// Private fields and methods
//----------------------------------------------------------------------
private:
  double knee_angle_par;
  double activation_angle_par;
  double increment;
  double initial_stiffness;
  bool active;
  bool last_active;
  bool flag_slow_walking;
  bool flag_bend_knee_finished;
  double knee_angle;
  double knee_velocity;
  double last_knee_angle;
  double ground_contact;
  bool lock_knee;
  double target_activity;
  double target_activity_1;
  double target_activity_2;
  double calculated_target_rating;
  bool first_step;
  double stimulation_init;
  double threshold_knee_velocity;
  double threshold_knee_angle_low;
  double threshold_knee_angle_high;
  double slow_walking_knee_angle;
  double new_slow_walking_knee_angle;
  double target_knee_angle_slow_walking;
  double threshold_hip_angle_low;
  double threshold_hip_angle_high;
  double increment_par1;
  double increment_par2;
  double hip_angle;
  bool isKneeBend;
  //Here is the right place for your variables. Replace this line by your declarations!

  /*! Destructor
   *
   * The destructor of modules is declared private to avoid accidental deletion. Deleting
   * modules is already handled by the framework.
   */
  ~mbbLockKnee();

  virtual void OnParameterChange();   //Might be needed to react to changes in parameters independent from Update() calls. Delete otherwise!

  virtual bool ProcessTransferFunction(double activation);

  virtual ib2c::tActivity CalculateActivity(std::vector<ib2c::tActivity> &derived_activities, double activation) const;

  virtual ib2c::tTargetRating CalculateTargetRating(double activation) const;

};

//----------------------------------------------------------------------
// End of namespace declaration
//----------------------------------------------------------------------
}
}
}



#endif
