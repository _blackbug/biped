//
// You received this file as part of Finroc
// A Framework for intelligent robot control
//
// Copyright (C) Finroc GbR (finroc.org)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//----------------------------------------------------------------------
/*!\file    projects/biped/reflexes/mbbLockHip.cpp
 *
 * \author  Jie Zhao & Tobias Luksch
 *
 * \date    2013-09-25
 *
 */
//----------------------------------------------------------------------
#include "projects/biped/reflexes/mbbLockHip.h"
#include "rrlib/math/utilities.h"
//----------------------------------------------------------------------
// External includes (system with <>, local with "")
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Internal includes with ""
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Debugging
//----------------------------------------------------------------------
#include <cassert>

//----------------------------------------------------------------------
// Namespace usage
//----------------------------------------------------------------------
using namespace rrlib::math;
//----------------------------------------------------------------------
// Namespace declaration
//----------------------------------------------------------------------
namespace finroc
{
namespace biped
{
namespace reflexes
{

//----------------------------------------------------------------------
// Forward declarations / typedefs / enums
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Const values
//----------------------------------------------------------------------
runtime_construction::tStandardCreateModuleAction<mbbLockHip> cCREATE_ACTION_FOR_MBB_LOCKHIP("LockHip");

//----------------------------------------------------------------------
// Implementation
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// mbbLockHip constructor
//----------------------------------------------------------------------
mbbLockHip::mbbLockHip(core::tFrameworkElement *parent, const std::string &name,
                       ib2c::tStimulationMode stimulation_mode, unsigned int number_of_inhibition_ports,
                       double _torque_factor) :
  tModule(parent, name, stimulation_mode, number_of_inhibition_ports),
  max_angle(0.0),
  activation_zone(0.0),
  pitch_factor(0.0),
  termi_factor(0.0),
  xcom_shift(0.0),
  push_recovery_factor(0.0),
  push_recovery_walking_factor(0.0),
  obstacle_walking_factor(0.0),
  com_x_error_threshold(0.0),
  hold_position_increment(0.0),
  current_angle(0.0),
  last_angle(0.0),
  current_velocity(0.0),
  left_com_x(0.0),
  right_com_x(0.0),
  com_x_error(0.0),
  xcom_x_opposite_leg(0.0),
  target_angle(0.0),
  target_torque(0.0),
  hold_hip_position(0.0),
  step_length(0.0),
  body_pitch(0.0),
  target_activity(0.0),
  termi_lock_hip(false),
  push_recovery_stand(0.0),
  push_recovery_walking(0.0),
  calculated_activity(0.0),
  last_activity(0.0),
  active(false),
  obstacle_walking(false),
  slow_walking(false),
  slow_walking_par1(0.0),
  slow_walking_par2(0.0),
  fast_walking(false),
  fast_walking_par1(0.0),
  fast_walking_par2(0.0)
{
  this->par_activation_zone.Set(0.35);//0.35
  this->par_max_angle.Set(0.76);   //Example for a runtime parameter. Replace or delete it!
  this->par_pitch_factor.Set(0.65);
  this->par_torque_factor.Set(_torque_factor);
  this->par_termin_factor.Set(1.05);
  this->par_xcom_shift.Set(0.01);
  this->par_push_recovery_factor.Set(0.3);
  this->par_push_recovery_walking_factor.Set(1.1);
  this->par_obstacle_walking_factor.Set(1.35);
  this->par_com_x_error_threshold.Set(0.06);
  this->par_hold_position_increment.Set(0.3);
  this->par_torque_activity_factor.Set(0.95);
  this->par_initiation_factor.Set(1.0);
  this->par_obstacle_walking.Set(false);
}

//----------------------------------------------------------------------
// mbbLockHip destructor
//----------------------------------------------------------------------
mbbLockHip::~mbbLockHip()
{}

/*
//----------------------------------------------------------------------
// mbbLockHip OnStaticParameterChange
//----------------------------------------------------------------------
void mbbLockHip::OnStaticParameterChange()
{
  tModule::OnStaticParameterChange();

  if (this->static_parameter_1.HasChanged())
  {
    As this static parameter has changed, do something with its value!
  }
}
*/
//----------------------------------------------------------------------
// mbbLockHip OnParameterChange
//----------------------------------------------------------------------
void mbbLockHip::OnParameterChange()
{
  tModule::OnParameterChange();
  this->activation_zone = this->par_activation_zone.Get();
  this->max_angle = this->par_max_angle.Get();
  this->pitch_factor = this->par_pitch_factor.Get();
  this->torque_factor = this->par_torque_factor.Get();
  this->termi_factor = this->par_termin_factor.Get();
  this->xcom_shift = this->par_xcom_shift.Get();
  this->push_recovery_factor = this->par_push_recovery_factor.Get();
  this->push_recovery_walking_factor = this->par_push_recovery_walking_factor.Get();
  this->obstacle_walking_factor = this->par_obstacle_walking_factor.Get();
  this->com_x_error_threshold = this->par_com_x_error_threshold.Get();
  this->hold_position_increment = this->par_hold_position_increment.Get();
//  this->obstacle_walking = this->par_obstacle_walking.Get();
}

//----------------------------------------------------------------------
// mbbLockHip ProcessTransferFunction
//----------------------------------------------------------------------
bool mbbLockHip::ProcessTransferFunction(double activation)
{
  // inputs
  //Get your special Sensor Inputs here
  this->current_angle = this->si_hip_y_angle.Get();
  this->current_velocity = this->current_angle - this->last_angle;
  this->step_length = this->ci_step_length.Get();
  this->body_pitch = this->ci_body_pitch.Get();
  this->xcom_x_opposite_leg = this->ci_xcom_x_opposite_leg.Get();
  this->termi_lock_hip = this->ci_termi_lock_hip.Get();
  this->push_recovery_stand = this->ci_push_recovery_stand.Get();
  this->push_recovery_walking = this->ci_push_recovery_walking.Get();
  this->left_com_x = this->ci_left_com_x.Get();
  this->right_com_x = this->ci_right_com_x.Get();
  this->com_x_error = this->right_com_x - this->left_com_x;
  double speed_scale = pow(this->step_length / 0.8, 2);
  speed_scale = LimitedValue<double>(speed_scale, 1.0, 1.5);

  this->obstacle_walking = this->ci_obstacle_walking_stimulation.Get();
  this->obstacle_lock_hip_1 = this->ci_obstacle_lock_hip_1.Get();
  this->obstacle_lock_hip_2 = this->ci_obstacle_lock_hip_2.Get();
  this->obstacle_lock_hip_3 = this->ci_obstacle_lock_hip_3.Get();

  this->slow_walking = this->ci_slow_walking.Get();
  this->slow_walking_par1 = this->ci_slow_walking_par1.Get();
  this->slow_walking_par2 = this->ci_slow_walking_par2.Get();

  this->fast_walking = this->ci_fast_walking.Get();
  this->fast_walking_par1 = this->ci_fast_walking_par1.Get();
  this->fast_walking_par2 = this->ci_fast_walking_par2.Get();

  // transfer function
  if (this->calculated_activity == 0.0)
  {
    this->target_angle = this->max_angle * this->step_length + this->body_pitch * this->pitch_factor;
    if ((this->obstacle_walking) && (this->ci_obstacle_step.Get() == 1))
      this->target_angle = (this->max_angle * this->step_length + this->body_pitch * this->pitch_factor) * this->obstacle_lock_hip_1;
    if ((this->obstacle_walking) && (this->ci_obstacle_step.Get() == 2))
      this->target_angle = (this->max_angle * this->step_length + this->body_pitch * this->pitch_factor) * 1.05;
//      this->target_angle = (this->max_angle * this->step_length + this->body_pitch * this->pitch_factor) * this->obstacle_walking_factor;

  }

  if (activation == 0.0)
  {
    this->target_torque = 0.0;
    this->target_angle = 0.0;
    this->hold_hip_position = 0.0;
    this->active = false;
    this->last_activity = 0.0;
    this->calculated_activity = 0.0;
  }

  else
  {
    if ((!this->termi_lock_hip) && (!this->push_recovery_stand))
    {
      this->target_torque = -this->current_velocity * this->torque_factor * speed_scale;
      this->calculated_activity = (this->current_angle - this->target_angle + this->activation_zone) / this->activation_zone;
      this->calculated_activity = LimitedValue<double>(this->calculated_activity, 0.0, 1.0) * activation * this->par_torque_activity_factor.Get();
    }
    else if (this->termi_lock_hip)
      this->target_torque = -this->current_velocity * this->torque_factor * speed_scale;
    else if (this->push_recovery_stand)
    {
      this->target_torque = -fabs(this->current_velocity * this->torque_factor * 1.5);
      if (this->com_x_error < this->com_x_error_threshold)
      {
        this->hold_hip_position += this->hold_position_increment ;
        this->hold_hip_position = LimitedValue(this->hold_hip_position, 0.0, 1.0);
        this->target_angle = (this->max_angle * this->step_length + this->body_pitch * this->pitch_factor) * this->push_recovery_factor;
      }
    }
    if (this->push_recovery_walking)
    {
      this->target_torque = -this->current_velocity * this->torque_factor * speed_scale;
      if (this->com_x_error < this->com_x_error_threshold)
      {
        this->hold_hip_position += this->hold_position_increment ;
        this->hold_hip_position = LimitedValue(this->hold_hip_position, 0.0, 1.0);
        this->target_angle = (this->max_angle * this->step_length  + this->body_pitch * this->pitch_factor) * this->push_recovery_walking_factor;
      }
    }

    if ((this->calculated_activity > 0.0) && (this->current_velocity < 0.0) && (!this->push_recovery_stand) && (!this->obstacle_walking))//&& (!this->step_length))//&& (this->termi_lock_hip))
    {
      this->hold_hip_position = 1.0;
      if (this->termi_lock_hip)
        this->target_angle = (this->max_angle * this->step_length + this->body_pitch * this->pitch_factor) * this->termi_factor;
      //this->target_angle = (this->par_max_angle * this->step_length + this->body_pitch * this->par_pitch_factor) * this->par_termi_factor;
    }

    if ((this->obstacle_walking) && (this->ci_obstacle_step.Get() == 1))
    {
//      this->target_angle = (this->max_angle * this->step_length + this->body_pitch * this->pitch_factor) * this->obstacle_walking_factor;
      this->target_torque = -this->current_velocity * this->torque_factor * speed_scale * this->obstacle_lock_hip_2;
      if ((this->calculated_activity > 0.0) && (this->current_velocity < 0.0))
      {
        this->hold_hip_position += this->obstacle_lock_hip_3;
        this->hold_hip_position = LimitedValue(this->hold_hip_position, 0.0, 1.0);
      }
    }
    if ((this->obstacle_walking) && (this->ci_obstacle_step.Get() == 2))
    {
//      this->target_angle = (this->max_angle * this->step_length + this->body_pitch * this->pitch_factor) * this->obstacle_walking_factor;
      this->target_torque = -this->current_velocity * this->torque_factor * speed_scale * 0.8;
      if ((this->calculated_activity > 0.0) && (this->current_velocity < 0.0))
      {
        this->hold_hip_position += 1.0;
        this->hold_hip_position = LimitedValue(this->hold_hip_position, 0.0, 1.0);
      }
    }

//  PSO
    if ( this->slow_walking && !this->fast_walking )
    {
    	this->target_torque = -this->current_velocity * this->torque_factor * speed_scale * this->slow_walking_par1;
    	this->target_angle = (this->max_angle * this->step_length + this->body_pitch * this->pitch_factor) * this->slow_walking_par2;
    }

    if ( !this->slow_walking && this->fast_walking )
    {
       	this->target_torque = -this->current_velocity * this->torque_factor * speed_scale * this->fast_walking_par1;
       	this->target_angle = (this->max_angle * this->step_length + this->body_pitch * this->pitch_factor) * this->fast_walking_par2;
    }

    this->target_angle = LimitedValue<double>(this->target_angle, 0.0, 0.84);
    this->calculated_activity = (this->current_angle - this->target_angle + this->activation_zone * speed_scale) / (this->activation_zone * speed_scale);
    this->calculated_activity = LimitedValue<double>(this->calculated_activity, 0.0, 1.0) * activation;
  }
  //outputs

  this->co_hip_y_angle.Publish(this->target_angle);
  this->co_hip_y_torque.Publish(this->target_torque);
  this->co_stimulation_angle.Publish(this->calculated_activity * this->hold_hip_position * activation);

  if ((this->obstacle_walking) && (this->ci_obstacle_step.Get() == 1))
  {
    if (this->hold_hip_position > 0)
      this->co_stimulation_torque.Publish(0.0);
    else
      this->co_stimulation_torque.Publish(this->calculated_activity * (1.0 - (this->hold_hip_position)) * activation);
  }
  else
    this->co_stimulation_torque.Publish(this->calculated_activity * (1.0 - this->hold_hip_position) * activation);

  //  this->so_hip_velocity.Publish(this->target_torque);
  this->last_angle = this->current_angle;
  this->co_last_activity.Publish(this->last_activity * activation);
  this->last_activity = this->calculated_activity;
  //this->so_torque.Publish(this->target_torque)

  return true;
}

//----------------------------------------------------------------------
// mbbLockHip CalculateActivity
//----------------------------------------------------------------------
ib2c::tActivity mbbLockHip::CalculateActivity(std::vector<ib2c::tActivity> &derived_activity, double activation) const
{
  return this->calculated_activity * activation;
}

//----------------------------------------------------------------------
// mbbLockHip CalculateTargetRating
//----------------------------------------------------------------------
ib2c::tTargetRating mbbLockHip::CalculateTargetRating(double activation) const
{
  return LimitedValue<double>(fabs(this->current_angle - this->target_angle) * 2.0, 0.0, 1.0);
}

//----------------------------------------------------------------------
// End of namespace declaration
//----------------------------------------------------------------------
}
}
}
