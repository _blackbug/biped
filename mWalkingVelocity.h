//
// You received this file as part of Finroc
// A framework for intelligent robot control
//
// Copyright (C) AG Robotersysteme TU Kaiserslautern
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//
//----------------------------------------------------------------------
/*!\file    projects/biped/skills/mWalkingVelocity.h
 *
 * \author  Kartikeya Karnatak
 *
 * \date    2016-05-12
 *
 * \brief Contains mWalkingVelocity
 *
 * \b mWalkingVelocity
 *
 * Modules publishes COM velocity of the Biped
 *
 */
//----------------------------------------------------------------------
#ifndef __projects__biped__skills__mWalkingVelocity_h__
#define __projects__biped__skills__mWalkingVelocity_h__

#include "plugins/structure/tModule.h"
#include "rrlib/math/tVector.h"
#define MAX_FILTER_LENGTH 100
//----------------------------------------------------------------------
// External includes (system with <>, local with "")
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Internal includes with ""
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Namespace declaration
//----------------------------------------------------------------------
namespace finroc
{
namespace biped
{
namespace skills
{

//----------------------------------------------------------------------
// Forward declarations / typedefs / enums
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Class declaration
//----------------------------------------------------------------------
//! SHORT_DESCRIPTION
/*!
 * Modules publishes COM velocity of the Biped
 */
class mWalkingVelocity : public structure::tModule
{

//----------------------------------------------------------------------
// Ports (These are the only variables that may be declared public)
//----------------------------------------------------------------------
public:

	tParameter<int> par_filter_length;

	tInput<double> si_pelvis_position_x;
	tInput<double> si_pelvis_position_y;

	tInput<bool> ci_simulation_reset;
	tInput<double> ci_loop_times;

	tOutput<double> co_step_length;
	tOutput<double> co_walking_velocity;
	tOutput<double> co_walking_distance;
	tOutput<double> co_time_span;
	tInput<bool> ci_reset_simulation_experiments;
	tInput<data_ports::tEvent> ci_reset_simulation;

//----------------------------------------------------------------------
// Public methods and typedefs
//----------------------------------------------------------------------
public:

  mWalkingVelocity(core::tFrameworkElement *parent, const std::string &name = "WalkingVelocity");
  void OnPortChange(data_ports::tChangeContext& change_context);

//----------------------------------------------------------------------
// Protected methods
//----------------------------------------------------------------------
protected:

  /*! Destructor
   *
   * The destructor of modules is declared protected to avoid accidental deletion. Deleting
   * modules is already handled by the framework.
   */
  ~mWalkingVelocity();

//----------------------------------------------------------------------
// Private fields and methods
//----------------------------------------------------------------------
private:

  rrlib::time::tTimestamp time_start;
  rrlib::time::tTimestamp time_current;
  double time_span;
  double step_length;
  double walking_velocity;
  double walking_velocity_x;
  double walking_velocity_y;
  double walking_distance;
  double loop_times;
  int time_enter;
  int filter_pos;

  double last_walking_position_x[MAX_FILTER_LENGTH];
  double last_walking_position_y[MAX_FILTER_LENGTH];

  double pelvis_position_x;
  double pelvis_position_y;

  double last_pelvis_position_x;
  double last_pelvis_position_y;

  std::atomic<bool> reset_simulation;
  int filter_length;

  rrlib::math::tVec2d adjusted_position;
  rrlib::time::tTimestamp last_looptime;
  rrlib::time::tTimestamp looptime;

  virtual void Update();
  double FilterValue(double* filter_history);
};

//----------------------------------------------------------------------
// End of namespace declaration
//----------------------------------------------------------------------
}
}
}



#endif
