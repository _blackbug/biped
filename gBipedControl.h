//
// You received this file as part of Finroc
// A framework for intelligent robot control
//
// Copyright (C) Finroc GbR (finroc.org)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//
//----------------------------------------------------------------------
/*!\file    projects/biped/control/gBipedControl.h
 *
 * \author  Tobias Luksch and Jie Zhao
 *
 * \date    2014-01-13
 *
 * \brief Contains gBipedControl
 *
 * \b gBipedControl
 *
 * The biped control group for the whole simulation
 *
 */
//----------------------------------------------------------------------
#ifndef __projects__biped__control__gBipedControl_h__
#define __projects__biped__control__gBipedControl_h__

#include "plugins/structure/tSenseControlGroup.h"
#include "rrlib/physics_simulation_newton/tEngineNewton.h"
#include "projects/biped/physics_engine_simulation/gPhysicsEngineNewton.h"
#include "projects/biped/control/gAbstractionLayer.h"
#include "projects/biped/control/gControl.h"
#include "plugins/scheduling/tThreadContainerElement.h"

using namespace rrlib::physics_simulation::newton;
using namespace finroc::biped::physics_engine_simulation;
using namespace finroc::scheduling;
//----------------------------------------------------------------------
// External includes (system with <>, local with "")
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Internal includes with ""
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Namespace declaration
//----------------------------------------------------------------------
namespace finroc
{
namespace biped
{
namespace control
{

//----------------------------------------------------------------------
// Forward declarations / typedefs / enums
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Class declaration
//----------------------------------------------------------------------
//! SHORT_DESCRIPTION
/*!
 * The biped control group for the whole simulation
 */
class gBipedControl : public structure::tSenseControlGroup
{

//----------------------------------------------------------------------
// Ports (These are the only variables that may be declared public)
//----------------------------------------------------------------------
public:

  // sensor inputs

  // for getting sensor input from mca_legacy_biped
  /*
  tSensorInput<double> si_ins_accel_x;
  tSensorInput<double> si_ins_accel_y;
  tSensorInput<double> si_ins_accel_z;
  tSensorInput<double>  si_ins_omega_x;
  tSensorInput<double>  si_ins_omega_y;
  tSensorInput<double>  si_ins_omega_z;
  tSensorInput<double>  si_ins_roll;
  tSensorInput<double>  si_ins_pitch;
  tSensorInput<double>  si_ins_yaw;

  tSensorInput<double> si_left_torque_x;
  tSensorInput<double> si_left_torque_y;
  tSensorInput<double> si_right_torque_x;
  tSensorInput<double> si_right_torque_y;

  //  tSensorInput<rrlib::math::tVec3d> si_ins_accel;

  tSensorInput<double> si_left_shoulder_x_torque;
  tSensorInput<double> si_left_shoulder_x_angle;
  tSensorInput<double> si_left_shoulder_y_torque;
  tSensorInput<double> si_left_shoulder_y_angle;
  tSensorInput<double> si_left_elbow_torque;
  tSensorInput<double> si_left_elbow_angle;
  tSensorInput<double> si_right_shoulder_x_torque;
  tSensorInput<double> si_right_shoulder_x_angle;
  tSensorInput<double> si_right_shoulder_y_torque;
  tSensorInput<double> si_right_shoulder_y_angle;
  tSensorInput<double> si_right_elbow_torque;
  tSensorInput<double> si_right_elbow_angle;

  tSensorInput<double> si_spine_x_torque;
  tSensorInput<double> si_spine_x_angle;
  tSensorInput<double> si_spine_y_torque;
  tSensorInput<double> si_spine_y_angle;
  tSensorInput<double> si_spine_z_torque;
  tSensorInput<double> si_spine_z_angle;

  tSensorInput<double> si_left_hip_x_torque;
  tSensorInput<double> si_left_hip_x_angle;
  tSensorInput<double> si_left_hip_y_torque;
  tSensorInput<double> si_left_hip_y_angle;
  tSensorInput<double> si_left_hip_z_torque;
  tSensorInput<double> si_left_hip_z_angle;

  tSensorInput<double> si_right_hip_x_torque;
  tSensorInput<double> si_right_hip_x_angle;
  tSensorInput<double> si_right_hip_y_torque;
  tSensorInput<double> si_right_hip_y_angle;
  tSensorInput<double> si_right_hip_z_torque;
  tSensorInput<double> si_right_hip_z_angle;

  tSensorInput<double> si_left_knee_torque;
  tSensorInput<double> si_left_knee_angle;
  tSensorInput<double> si_right_knee_torque;
  tSensorInput<double> si_right_knee_angle;

  tSensorInput<double> si_left_ankle_x_torque;
  tSensorInput<double> si_left_ankle_x_angle;
  tSensorInput<double> si_left_ankle_y_torque;
  tSensorInput<double> si_left_ankle_y_angle;

  tSensorInput<double> si_right_ankle_x_torque;
  tSensorInput<double> si_right_ankle_x_angle;
  tSensorInput<double> si_right_ankle_y_torque;
  tSensorInput<double> si_right_ankle_y_angle;

  tSensorInput<double> si_left_inner_toe_force;
  tSensorInput<double> si_right_inner_toe_force;
  tSensorInput<double> si_left_inner_heel_force;
  tSensorInput<double> si_right_inner_heel_force;
  tSensorInput<double> si_left_outer_toe_force;
  tSensorInput<double> si_right_outer_toe_force;
  tSensorInput<double> si_left_outer_heel_force;
  tSensorInput<double> si_right_outer_heel_force;

  tSensorInput<double> si_left_force_z;
  tSensorInput<rrlib::math::tVec2d> si_left_torque;
  tSensorInput<double> si_right_force_z;
  tSensorInput<rrlib::math::tVec2d> si_right_torque;
  */

  tControllerInput<double> ci_start_simulation;
  tControllerInput<double> ci_progress_simulation;
  tControllerInput<double> ci_switch_off_gravity;
  tControllerInput<double> ci_enable_experiments;
  tControllerInput<double> ci_experiment_type;
  tControllerInput<double> ci_desired_duration;
  tControllerInput<double> ci_sphere_movement_step_width;
  tControllerInput<double> ci_plate_movement_step_width;
  tControllerInput<double> ci_plate_rotation_step_width;
  tControllerInput<double> ci_head_force_x;
  tControllerInput<double> ci_head_force_y;
  tControllerInput<double> ci_head_force_z;
  tControllerInput<double> ci_torso_force_x;
  tControllerInput<double> ci_torso_force_y;
  tControllerInput<double> ci_torso_force_z;
  tControllerInput<double> ci_pelvis_force_x;
  tControllerInput<double> ci_pelvis_force_y;
  tControllerInput<double> ci_pelvis_force_z;
  tControllerInput<double> ci_sphere_pose_x;
  tControllerInput<double> ci_sphere_pose_y;
  tControllerInput<double> ci_sphere_pose_z;
  tControllerInput<double> ci_plate_pos_x;
  tControllerInput<double> ci_plate_pos_y;
  tControllerInput<double> ci_plate_pos_z;
  tControllerInput<double> ci_plate_rot_x;
  tControllerInput<double> ci_plate_rot_y;
  tControllerInput<double> ci_plate_rot_z;
  tControllerInput<double> ci_arrow_visulisation;
  tControllerInput<double> ci_sensor_monitor;
  tControllerInput<double> ci_stimulation_walking;
  tControllerInput<double> ci_walking_termination;
  tControllerInput<double> ci_walking_speed;
  tControllerInput<double> ci_walking_direction;
  tControllerInput<double> ci_walking_laterally;
  tControllerInput<bool> ci_reset_simulation;


  tSensorOutput<double> so_run_time;
  tSensorOutput<double> so_total_time;
  tSensorOutput<double> so_max_vel;
  tSensorOutput<double> so_left_footload;
  tSensorOutput<double> so_right_footload;
  tSensorOutput<double> so_left_force_deviation;
  tSensorOutput<double> so_right_force_deviation;
  tSensorOutput<double> so_left_ground_contact;
  tSensorOutput<double> so_right_ground_contact;


  tSensorOutput<double> so_left_force_z;
  tSensorOutput<double> so_right_force_z;


  // controller outputs

  // connect to mca-legacy
  /*
  tControllerOutput<double> co_left_stimulation_shoulder_x_torque;
  tControllerOutput<double> co_left_shoulder_x_torque;
  tControllerOutput<double> co_left_stimulation_shoulder_x_angle;
  tControllerOutput<double> co_left_shoulder_x_angle;

  tControllerOutput<double> co_left_stimulation_shoulder_y_torque;
  tControllerOutput<double> co_left_shoulder_y_torque;
  tControllerOutput<double> co_left_stimulation_shoulder_y_angle;
  tControllerOutput<double> co_left_shoulder_y_angle;

  tControllerOutput<double> co_left_stimulation_elbow_torque;
  tControllerOutput<double> co_left_elbow_torque;
  tControllerOutput<double> co_left_stimulation_elbow_angle;
  tControllerOutput<double> co_left_elbow_angle;

  tControllerOutput<double> co_left_stimulation_hip_x_torque;
  tControllerOutput<double> co_left_hip_x_torque;
  tControllerOutput<double> co_left_stimulation_hip_x_angle;
  tControllerOutput<double> co_left_hip_x_angle;
  tControllerOutput<double> co_left_stimulation_hip_y_torque;
  tControllerOutput<double> co_left_hip_y_torque;
  tControllerOutput<double> co_left_stimulation_hip_y_angle;
  tControllerOutput<double> co_left_hip_y_angle;
  tControllerOutput<double> co_left_stimulation_hip_z_torque;
  tControllerOutput<double> co_left_hip_z_torque;
  tControllerOutput<double> co_left_stimulation_hip_z_angle;
  tControllerOutput<double> co_left_hip_z_angle;
  tControllerOutput<double> co_left_stimulation_knee_torque;
  tControllerOutput<double> co_left_knee_torque;
  tControllerOutput<double> co_left_stimulation_knee_angle;
  tControllerOutput<double> co_left_knee_angle;
  tControllerOutput<double> co_left_stimulation_ankle_x_torque;
  tControllerOutput<double> co_left_ankle_x_torque;
  tControllerOutput<double> co_left_stimulation_ankle_x_angle;
  tControllerOutput<double> co_left_ankle_x_angle;
  tControllerOutput<double> co_left_stimulation_ankle_y_torque;
  tControllerOutput<double> co_left_ankle_y_torque;
  tControllerOutput<double> co_left_stimulation_ankle_y_angle;
  tControllerOutput<double> co_left_ankle_y_angle;

  tControllerOutput<double> co_stimulation_spine_x_torque;
  tControllerOutput<double> co_spine_x_torque;
  tControllerOutput<double> co_stimulation_spine_x_angle;
  tControllerOutput<double> co_spine_x_angle;

  tControllerOutput<double> co_stimulation_spine_y_torque;
  tControllerOutput<double> co_spine_y_torque;
  tControllerOutput<double> co_stimulation_spine_y_angle;
  tControllerOutput<double> co_spine_y_angle;

  tControllerOutput<double> co_stimulation_spine_z_torque;
  tControllerOutput<double> co_spine_z_torque;
  tControllerOutput<double> co_stimulation_spine_z_angle;
  tControllerOutput<double> co_spine_z_angle;

  tControllerOutput<double> co_right_stimulation_shoulder_x_torque;
  tControllerOutput<double> co_right_shoulder_x_torque;
  tControllerOutput<double> co_right_stimulation_shoulder_x_angle;
  tControllerOutput<double> co_right_shoulder_x_angle;

  tControllerOutput<double> co_right_stimulation_shoulder_y_torque;
  tControllerOutput<double> co_right_shoulder_y_torque;
  tControllerOutput<double> co_right_stimulation_shoulder_y_angle;
  tControllerOutput<double> co_right_shoulder_y_angle;

  tControllerOutput<double> co_right_stimulation_elbow_torque;
  tControllerOutput<double> co_right_elbow_torque;
  tControllerOutput<double> co_right_stimulation_elbow_angle;
  tControllerOutput<double> co_right_elbow_angle;

  tControllerOutput<double> co_right_stimulation_hip_x_torque;
  tControllerOutput<double> co_right_hip_x_torque;
  tControllerOutput<double> co_right_stimulation_hip_x_angle;
  tControllerOutput<double> co_right_hip_x_angle;
  tControllerOutput<double> co_right_stimulation_hip_y_torque;
  tControllerOutput<double> co_right_hip_y_torque;
  tControllerOutput<double> co_right_stimulation_hip_y_angle;
  tControllerOutput<double> co_right_hip_y_angle;
  tControllerOutput<double> co_right_stimulation_hip_z_torque;
  tControllerOutput<double> co_right_hip_z_torque;
  tControllerOutput<double> co_right_stimulation_hip_z_angle;
  tControllerOutput<double> co_right_hip_z_angle;
  tControllerOutput<double> co_right_stimulation_knee_torque;
  tControllerOutput<double> co_right_knee_torque;
  tControllerOutput<double> co_right_stimulation_knee_angle;
  tControllerOutput<double> co_right_knee_angle;
  tControllerOutput<double> co_right_stimulation_ankle_x_torque;
  tControllerOutput<double> co_right_ankle_x_torque;
  tControllerOutput<double> co_right_stimulation_ankle_x_angle;
  tControllerOutput<double> co_right_ankle_x_angle;
  tControllerOutput<double> co_right_stimulation_ankle_y_torque;
  tControllerOutput<double> co_right_ankle_y_torque;
  tControllerOutput<double> co_right_stimulation_ankle_y_angle;
  tControllerOutput<double> co_right_ankle_y_angle;
  */

//----------------------------------------------------------------------
// Private fields and methods
//----------------------------------------------------------------------


  /*! Destructor
   *
   * The destructor of modules is declared private to avoid accidental deletion. Deleting
   * modules is already handled by the framework.
   */
public:
  gBipedControl(core::tFrameworkElement *parent, const std::string &name = "BipedControl",
                const std::string &structure_config_file = __FILE__".xml");

private:
  ~gBipedControl();
  virtual void PostChildInit();
  gPhysicsEngineNewton* simulation_group;
  gAbstractionLayer* hal;
  gControl* control;

private:
  tEngineNewton* engine_newton;
// tThreadContainerElement<gPhysicsEngineNewton>* simulation_group;



};

//----------------------------------------------------------------------
// End of namespace declaration
//----------------------------------------------------------------------
}
}
}



#endif
