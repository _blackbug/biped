//
// You received this file as part of Finroc
// A Framework for intelligent robot control
//
// Copyright (C) Finroc GbR (finroc.org)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//----------------------------------------------------------------------
/*!\file    projects/biped/reflexes/mbbLockKnee.cpp
 *
 * \author  Tobias Luksch & Jie Zhao
 *
 * \date    2013-10-16
 *
 */
//----------------------------------------------------------------------
#include "projects/biped/reflexes/mbbLockKnee.h"
#include "rrlib/math/utilities.h"
//----------------------------------------------------------------------
// External includes (system with <>, local with "")
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Internal includes with ""
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Debugging
//----------------------------------------------------------------------
#include <cassert>

//----------------------------------------------------------------------
// Namespace usage
//----------------------------------------------------------------------
using namespace rrlib::math;
//----------------------------------------------------------------------
// Namespace declaration
//----------------------------------------------------------------------
namespace finroc
{
namespace biped
{
namespace reflexes
{

//----------------------------------------------------------------------
// Forward declarations / typedefs / enums
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Const values
//----------------------------------------------------------------------
runtime_construction::tStandardCreateModuleAction<mbbLockKnee> cCREATE_ACTION_FOR_MBB_LOCKKNEE("LockKnee");

//----------------------------------------------------------------------
// Implementation
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// mbbLockKnee constructor
//----------------------------------------------------------------------
mbbLockKnee::mbbLockKnee(core::tFrameworkElement *parent, const std::string &name,
                         ib2c::tStimulationMode stimulation_mode, unsigned int number_of_inhibition_ports,
                         double activation_angle) :
  tModule(parent, name, stimulation_mode, number_of_inhibition_ports),
  knee_angle_par(0.0),
  activation_angle_par(0.0),
  increment(0.0),
  initial_stiffness(0.0),
  active(false),
  last_active(false),
  flag_slow_walking(false),
  flag_bend_knee_finished(false),
  knee_angle(0.0),
  knee_velocity(0.0),
  last_knee_angle(0.0),
  ground_contact(0.0),
  lock_knee(false),
  target_activity(0.0),
  target_activity_1(0.0),
  target_activity_2(0.0),
  calculated_target_rating(0.0),
  first_step(false),
  stimulation_init(0.0),
  threshold_knee_velocity(0.025),
  threshold_knee_angle_low(0.3),
  threshold_knee_angle_high(0.55),
  slow_walking_knee_angle(0.6),
  new_slow_walking_knee_angle(0.0),
  target_knee_angle_slow_walking(0.0),
  threshold_hip_angle_low(0.0),
  threshold_hip_angle_high(0.3),
  increment_par1(0.8),
  increment_par2(0.25),
  hip_angle(0),
  isKneeBend(false)
{
  this->par_angle_knee.Set(0.04);
  this->par_activation_angle_knee.Set(activation_angle);
  this->par_increment.Set(0.1);
  this->par_initial_stiffness.Set(0.05);
}

//----------------------------------------------------------------------
// mbbLockKnee destructor
//----------------------------------------------------------------------
mbbLockKnee::~mbbLockKnee()
{}

/*
//----------------------------------------------------------------------
// mbbLockKnee OnStaticParameterChange
//-----------------------------bool last_active;-----------------------------------------
void mbbLockKnee::OnStaticParameterChange()
{
  tModule::OnStaticParameterChange();

  if (this->static_parameter_1.HasChanged())
  {
    As this static parameter has changed, do something with its value!
  }
}
*/
//----------------------------------------------------------------------
// mbbLockKnee OnParameterChange
//----------------------------------------------------------------------
void mbbLockKnee::OnParameterChange()
{
  tModule::OnParameterChange();
  this->knee_angle_par = this->par_angle_knee.Get();
  this->increment = this->par_increment.Get();
  this->activation_angle_par = this->par_activation_angle_knee.Get();
  this->initial_stiffness = this->par_initial_stiffness.Get();
}

//----------------------------------------------------------------------
// mbbLockKnee ProcessTransferFunction
//----------------------------------------------------------------------
bool mbbLockKnee::ProcessTransferFunction(double activation)
{
  // inputs
  this->stimulation_init = this->ci_stimulation_init.Get();
  this->knee_angle = this->si_knee_angle.Get();
  this->ground_contact = this->si_ground_contact.Get();
  this->slow_walking_knee_angle = this->ci_slow_walking_knee_angle.Get();
  this->threshold_knee_angle_low = this->ci_slow_walking_knee_angle_low.Get();
  this->threshold_knee_angle_high = this->ci_slow_walking_knee_angle_high.Get();
  this->threshold_knee_velocity = this->ci_slow_walking_knee_velocity.Get();
  this->threshold_hip_angle_low = this->ci_slow_walking_hip_angle_low.Get();
  this->threshold_hip_angle_high = this->ci_slow_walking_hip_angle_high.Get();
  this->increment_par1 = this->ci_slow_walking_increment_par1.Get();
  this->increment_par2 = this->ci_slow_walking_increment_par2.Get();
  this->hip_angle = this->si_hip_angle.Get();

  // transfer function

  //TO ASK WHERE THIS COMES FROM
  if (activation == 0.0)
  {
    this->active = false;
    this->lock_knee = false;
    this->target_activity = 0.0;
    this->target_activity_1 = 0.0;
    this->first_step = false;
    this->flag_slow_walking = false;
    this->flag_bend_knee_finished = false;
    if (this->stimulation_init > 0.0)
      this->first_step = true;
    this->co_knee_angle.Publish(this->knee_angle_par);
  }
  else
  {
	  if( (this->ground_contact <= 1) && (this->ground_contact > 0.5 ) )
	  {
		  this->flag_bend_knee_finished = false;
	  }

    if (this->knee_angle > this->activation_angle_par)
    {
      this->active = true;
    }
    if (this->first_step)
    {
      if ((this->ground_contact == 0.0) && (this->target_activity < initial_stiffness))
      {
        this->target_activity = this->initial_stiffness;
      }
      if (((this->ground_contact == 0.0) && (this->target_activity < 1.0) &&
           (this->knee_angle > (this->activation_angle_par))) || (this->target_activity > this->initial_stiffness))
      {
        this->target_activity += this->increment;
        this->target_activity = LimitedValue<double>(this->target_activity, 0.0, 1.0);
      }
    }
    else
    {

    	// TODO last_active works for both legs separately ?? if so this could be a problem
//    	if(( this->active ) && (!this->last_active) && ( this->knee_velocity <= 0.05 ))
//    	{
//    		this->flag_slow_walking = true;
//    	}

    	if( this->slow_walking_knee_angle > 0.04 )
    	{
    		this->flag_slow_walking = true;
    	}

    	if( this->flag_slow_walking )
    	{
			//add the active control of knee joint during slow walking
//			if((this->knee_angle > this->threshold_knee_angle_low) && (this->knee_angle < this->threshold_knee_angle_high)
//					&& (this->knee_velocity > this->threshold_knee_velocity) && (this->flag_bend_knee_finished == false))
    		if( (this->hip_angle > this->threshold_hip_angle_low) && (this->hip_angle < this->threshold_hip_angle_high )
    				&& (this->flag_bend_knee_finished == false))
			{
    			isKneeBend = true;
				this->target_activity_1 += this->increment;
				this->target_activity_1 = LimitedValue<double>(this->target_activity_1, 0.0, 1.0);

				//increment bend the knee joint for ground clearance, upper limit is 0.6
				this->new_slow_walking_knee_angle = ( this->slow_walking_knee_angle != 0.04 ) ? this->ci_slow_walking_knee_angle.Get() : 0.6;
				this->target_knee_angle_slow_walking = this->knee_angle;
				this->target_knee_angle_slow_walking += this->increment * this->increment_par1;
// SOME ISSUES HERE ALWAYS GO IN ELSE
				if( this->knee_angle < this->new_slow_walking_knee_angle )
				{
					this->target_knee_angle_slow_walking = LimitedValue<double>(this->target_knee_angle_slow_walking, this->knee_angle, this->new_slow_walking_knee_angle);
				}
				else
				{
					this->target_knee_angle_slow_walking = LimitedValue<double>(this->target_knee_angle_slow_walking, this->new_slow_walking_knee_angle, this->knee_angle);
				}
				this->co_knee_angle.Publish(this->target_knee_angle_slow_walking);
				this->target_activity = this->target_activity_1;
				FINROC_LOG_PRINT(DEBUG_WARNING, "KKLOG : ENTERED LOCK KNEE 0.6");
			}
//			else if((this->knee_velocity < this->threshold_knee_velocity))
    		else
			{
				this->target_activity_1 = 0.0;
				this->target_activity += this->increment;
				this->target_activity = LimitedValue<double>(this->target_activity, 0.0, 0.9);
				//increment extend the knee joint to 0.04
				this->target_knee_angle_slow_walking = this->knee_angle;
				this->target_knee_angle_slow_walking -= this->increment * this->increment_par2;
				this->target_knee_angle_slow_walking = LimitedValue<double>(this->target_knee_angle_slow_walking, this->knee_angle_par, 0.6);
				this->co_knee_angle.Publish(this->target_knee_angle_slow_walking);
//				this->co_knee_angle.Publish(0.04);
				//target_angel -= 0.05;
				if( isKneeBend)
				{
					this->flag_bend_knee_finished = true;
					isKneeBend = false;
				}
				else
				{
					this->flag_bend_knee_finished = false;
				}

//	    	      if ((this->active) && (this->ground_contact <= 0.5) && (this->target_activity < 1.0) && (this->knee_angle < this->activation_angle_par))
//	    	      {
//	    	        this->target_activity_2 += this->increment;
//	    	        this->target_activity_2 = LimitedValue<double>(this->target_activity_2, 0.0, 1.0);
//	    	        this->co_knee_angle.Publish(this->knee_angle_par);
//	    	    	this->target_activity = this->target_activity_2;
//	    	      }
			}
    	}
    	else
    	{
    	      if ((this->active) && (this->ground_contact <= 0.5) && (this->target_activity < 1.0) && (this->knee_angle < this->activation_angle_par))
    	      {
    	        this->target_activity_2 += this->increment;
    	        this->target_activity_2 = LimitedValue<double>(this->target_activity_2, 0.0, 1.0);
    	        this->co_knee_angle.Publish(this->knee_angle_par);
    	    	this->target_activity = this->target_activity_2;
    	//    	FINROC_LOG_PRINT(DEBUG_WARNING, "target_activity_2", this->target_activity_2);
    	      }
    	}
    }
  }

  if (this->active)
    this->calculated_target_rating = fabs(this->knee_angle - this->knee_angle_par) * 2.0;
  else
    this->calculated_target_rating = 0.5 + 0.5 * fabs(this->knee_angle - this->knee_angle_par);
  //this->target_rating = (1.0 - this->target_activity) * fabs(this->knee_angle - this->par_knee_angle) * 2.0 + this->ground_contact;
  this->calculated_target_rating = LimitedValue<double>(this->calculated_target_rating, 0.0, 1.0);


  //outputs

//  this->co_knee_angle.Publish(this->knee_angle_par);

  //Use your input and output ports to transfer data through this module.
  //Return whether this transfer was successful or not.

  //calculate the knee velocity
  if(this->knee_angle != this->last_knee_angle)
  {
	  this->knee_velocity = this->knee_angle - this->last_knee_angle;
  }
  this->last_knee_angle = this->knee_angle;
  this->last_active = this->active;

  this->co_knee_velocity.Publish(this->knee_velocity);
  this->co_hip_angle.Publish(this->hip_angle);
  //this->co_knee_angle.Publish(this->knee_angle);

  return true;
}

//----------------------------------------------------------------------
// mbbLockKnee CalculateActivity
//----------------------------------------------------------------------
ib2c::tActivity mbbLockKnee::CalculateActivity(std::vector<ib2c::tActivity> &derived_activity, double activation) const
{
  //Return a meaningful activity value here. You also want to set the derived activity vector, if you registered derived activities e.g. in the constructor.
  return this->target_activity * activation;
}

//----------------------------------------------------------------------
// mbbLockKnee CalculateTargetRating
//----------------------------------------------------------------------
ib2c::tTargetRating mbbLockKnee::CalculateTargetRating(double activation) const
{
  //Return a meaningful target rating here. Choosing 0.5 just because you have no better idea is not "meaningful"!
// If you do not want to use the activation in this calculation remove its name to rid of the warning.
  return this->calculated_target_rating;
}

//----------------------------------------------------------------------
// End of namespace declaration
//----------------------------------------------------------------------
}
}
}
