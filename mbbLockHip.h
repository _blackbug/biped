//
// You received this file as part of Finroc
// A Framework for intelligent robot control
//
// Copyright (C) Finroc GbR (finroc.org)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//----------------------------------------------------------------------
/*!\file    projects/biped/reflexes/mbbLockHip.h
 *
 * \author  Jie Zhao & Tobias Luksch
 *
 * \date    2013-09-25
 *
 * \brief Contains mbbLockHip
 *
 * \b mbbLockHip
 *
 *
 * Reflex controller for locking hip at the end of swing phase!
 *
 */
//----------------------------------------------------------------------
#ifndef __projects__biped__reflexes__mbbLockHip_h__
#define __projects__biped__reflexes__mbbLockHip_h__

#include "plugins/ib2c/tModule.h"

//----------------------------------------------------------------------
// External includes (system with <>, local with "")
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Internal includes with ""
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Namespace declaration
//----------------------------------------------------------------------
namespace finroc
{
namespace biped
{
namespace reflexes
{

//----------------------------------------------------------------------
// Forward declarations / typedefs / enums
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Class declaration
//----------------------------------------------------------------------
//! SHORT_DESCRIPTION
/*!
 *
 * Reflex controller for locking hip at the end of swing phase!
 */
class mbbLockHip : public ib2c::tModule
{

//----------------------------------------------------------------------
// Ports (These are the only variables that may be declared public)
//----------------------------------------------------------------------
public:

  tParameter<double> par_max_angle;   //Example for a runtime parameter. Replace or delete it!
  tParameter<double> par_activation_zone;
  tParameter<double> par_pitch_factor;
  tParameter<double> par_torque_factor;
  tParameter<double> par_termin_factor;
  tParameter<double> par_xcom_shift;
  tParameter<double> par_push_recovery_factor;
  tParameter<double> par_push_recovery_walking_factor;
  tParameter<double> par_obstacle_walking_factor;
  tParameter<double> par_com_x_error_threshold;
  tParameter<double> par_hold_position_increment;
  tParameter<double> par_torque_activity_factor;
  tParameter<double> par_initiation_factor;
  tParameter<bool> par_obstacle_walking;

  tInput<double> ci_step_length;   //Example for input ports. Replace or delete them!
  tInput<double> ci_body_pitch;
  tInput<double> ci_xcom_x_opposite_leg;
  tInput<double> ci_termi_lock_hip;
  tInput<double> ci_push_recovery_stand;
  tInput<double> ci_push_recovery_walking;
  tInput<double> ci_left_com_x;
  tInput<double> ci_right_com_x;
  tInput<double> ci_initiation_activity;

  tInput<double> ci_slow_walking_par1;
  tInput<double> ci_slow_walking_par2;
  tInput<bool>   ci_slow_walking;

  tInput<double> ci_fast_walking_par1;
  tInput<double> ci_fast_walking_par2;
  tInput<bool>   ci_fast_walking;

  tInput<bool> ci_obstacle_walking_stimulation;
  tInput<int> ci_obstacle_step;
  tInput<double> ci_obstacle_lock_hip_1;
  tInput<double> ci_obstacle_lock_hip_2;
  tInput<double> ci_obstacle_lock_hip_3;

  tInput<double> si_hip_y_angle;

  tOutput<double> co_hip_y_angle;   //Examples for output ports. Replace or delete them!
  tOutput<double> co_hip_y_torque;
  tOutput<double> co_stimulation_angle;   //Examples for output ports. Replace or delete them!
  tOutput<double> co_stimulation_torque;
//  tOutput<double> so_hip_velocity;
  tOutput<double> co_last_activity;
// tOutput<double> so_torque;
//----------------------------------------------------------------------
// Public methods and typedefs
//----------------------------------------------------------------------
public:

  mbbLockHip(core::tFrameworkElement *parent, const std::string &name = "LockHip",
             ib2c::tStimulationMode stimulation_mode = ib2c::tStimulationMode::AUTO, unsigned int number_of_inhibition_ports = 0,
             double torque_factor = 0.0);

//----------------------------------------------------------------------
// Private fields and methods
//----------------------------------------------------------------------
private:

  //Here is the right place for your variables. Replace this line by your declarations!
  double max_angle;
  double activation_zone;
  double pitch_factor;
  double torque_factor;
  double termi_factor;
  double xcom_shift;
  double push_recovery_factor;
  double push_recovery_walking_factor;
  double obstacle_walking_factor;
  double com_x_error_threshold;
  double hold_position_increment;
  double current_angle;
  double last_angle;
  double current_velocity;
  double left_com_x;
  double right_com_x;
  double com_x_error;
  double xcom_x_opposite_leg;
  double target_angle;
  double target_torque;
  double hold_hip_position;
  double step_length;
  double body_pitch;
  double target_activity;
  double termi_lock_hip;
  double push_recovery_stand;
  double push_recovery_walking;
  double calculated_activity;
  double last_activity;

  double obstacle_lock_hip_1;
  double obstacle_lock_hip_2;
  double obstacle_lock_hip_3;
  bool active;
  bool obstacle_walking;
  bool slow_walking;
  double slow_walking_par1;
  double slow_walking_par2;

  bool fast_walking;
  double fast_walking_par1;
  double fast_walking_par2;
  /*! Destructor
   *
   * The destructor of modules is declared private to avoid accidental deletion. Deleting
   * modules is already handled by the framework.
   */
  ~mbbLockHip();

  virtual void OnParameterChange();   //Might be needed to react to changes in parameters independent from Update() calls. Delete otherwise!

  virtual bool ProcessTransferFunction(double activation);

  virtual ib2c::tActivity CalculateActivity(std::vector<ib2c::tActivity> &derived_activities, double activation) const;

  virtual ib2c::tTargetRating CalculateTargetRating(double activation) const;

};

//----------------------------------------------------------------------
// End of namespace declaration
//----------------------------------------------------------------------
}
}
}



#endif
