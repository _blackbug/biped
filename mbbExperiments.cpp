//
// You received this file as part of Finroc
// A Framework for intelligent robot control
//
// Copyright (C) Finroc GbR (finroc.org)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//----------------------------------------------------------------------
/*!\file    projects/biped/utils/mbbExperiments.cpp
 *
 * \author  Max Steiner
 *
 * \date    2013-10-02
 *
 */
//----------------------------------------------------------------------
#include "projects/biped/utils/mbbExperiments.h"
#include "rrlib/math/utilities.h"
#include "rrlib/time/time.h"
#include "rrlib/math/tVector.h"
#include "rrlib/math/tPose3D.h"
#include "libraries/physics_simulation/mPhysicsSimulationSimVis.h"

//----------------------------------------------------------------------
// External includes (system with <>, local with "")
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Internal includes with ""
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Debugging
//----------------------------------------------------------------------
#include <cassert>
#include <fstream>
#include <random>
#include <chrono>
//----------------------------------------------------------------------
// Namespace usage
//----------------------------------------------------------------------
using namespace rrlib::math;
using namespace rrlib::time;
//----------------------------------------------------------------------
// Namespace declaration
//----------------------------------------------------------------------
namespace finroc
{
namespace biped
{
namespace utils
{

//----------------------------------------------------------------------
// Forward declarations / typedefs / enums
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Const values
//----------------------------------------------------------------------
static runtime_construction::tStandardCreateModuleAction<mbbExperiments> cCREATE_ACTION_FOR_MBB_EXPERIMENTS("Experiments");

//----------------------------------------------------------------------
// Implementation
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// mbbExperiments constructor
//----------------------------------------------------------------------
mbbExperiments::mbbExperiments(core::tFrameworkElement *parent, const std::string &name,
                               ib2c::tStimulationMode stimulation_mode, unsigned int number_of_inhibition_ports) :
  tModule(parent, name, stimulation_mode, number_of_inhibition_ports, false), // change to 'true' to make module's ports shared (so that ports in other processes can connect to its sensor outputs and controller inputs)
  si_new_running(data_ports::tQueueSettings(false, -1)),
  repeat(false),
  times_to_repeat(0),
  log(false),
  enable_experiment(0.0),
  phase_x(0.0),
  phase_y(0.0),
  last_plate_rot_freq_x(0.0),
  last_plate_rot_freq_y(0.0),
  footload_left(0.0),
  footload_right(0.0),
  roll(0.0),
  pitch(0.0),
  new_running(0.0),
  head_force_x(0.0),
  head_force_y(0.0),
  head_force_z(0.0),
  torso_force_x(0.0),
  torso_force_y(0.0),
  torso_force_z(0.0),
  pelvis_force_x(0.0),
  pelvis_force_y(0.0),
  pelvis_force_z(0.0),
  sphere_pos_x(0.0),
  sphere_pos_y(0.0),
  sphere_pos_z(0.0),
  plate_pos_x(0.0),
  plate_pos_y(0.0),
  plate_pos_z(0.0),
  plate_rot_x(0.0),
  plate_rot_y(0.0),
  plate_rot_z(0.0),
  stim_walking(0.0),
  walking_speed(0.0),
  walking_termi(0.0),
  desired_duration(0),
  last_looptime(rrlib::time::cNO_TIME),
  loop_duration(0),
  duration_milliseconds(0.0),
  total_time(0),
  run_time(0),
  exp_type(eEXP_TYPE_NONE),
  state(eEXP_STATE_IDLE),
  run(1),
  last_run(0),
  reset_called(false),
  off_ground(false),
  push_x(false),
  push_force_start(false),
  push_duration(200.0),
  push_detected(false),
  push_starting(false),
  push_loop_time(0.0),
  random_loop_times(0),
  random_start_milliseconds(0),
  random_direction(0),
  random_torso_force_x(0),
  push_force_start_time(rrlib::time::cNO_TIME),
  uniform_distribution(0, 1),
  eng(1234),
  iteration_random_rotation(0),
  reset_started(false),
  reset_time(8),
  restart_waiting_time(50),
  restart_landing_time(1)
{
  this->par_repeat.Set(true);
  this->par_times_to_repeat.Set(10);
  this->par_log.Set(false);
  this->par_stiffness_threshold.Set(0.8);
  this->par_plate_translational_movement_x.Set(0.2);
  this->par_plate_rot_x_freq.Set(0.1);
  this->par_plate_rot_y_freq.Set(0.3);
  this->par_plate_rot_z_freq.Set(0.2);
  this->par_plate_rot_x_offset.Set(0.0);
  this->par_plate_rot_y_offset.Set(0.006);
  this->par_plate_rot_x_angle.Set(0.1);
  this->par_learning_walking_velocity.Set(0.95);
  this->par_torso_force_x.Set(195);

  const char* project_home = getenv("FINROC_PROJECT_HOME");
  std::ifstream infile(std::string(project_home) +  "/etc/stimuli.csv");
  double number = 2420;
  std::string temp;

  while (infile >> number)
  {
//      std::istringstream buffer(temp);
//      std::vector<double> line(std::istream_iterator<double>(buffer),
//                               std::istream_iterator<double>());
    this->plate_rotation.push_back(number);
  }
}

//----------------------------------------------------------------------
// mbbExperiments destructor
//----------------------------------------------------------------------
mbbExperiments::~mbbExperiments()
{}

//----------------------------------------------------------------------
// mbbExperiments OnParameterChange
//----------------------------------------------------------------------
void mbbExperiments::OnParameterChange()
{
  this->repeat = this->par_repeat.Get();
  this->times_to_repeat = this->par_times_to_repeat.Get();
  this->log = this->par_log.Get();
  this->stiffness_threshold = this->par_stiffness_threshold.Get();
  this->plate_translational_movement_x = this->par_plate_translational_movement_x.Get();
  this->plate_rot_x_freq = this->par_plate_rot_x_freq.Get();
  this->plate_rot_y_freq = this->par_plate_rot_y_freq.Get();
  this->plate_rot_z_freq = this->par_plate_rot_z_freq.Get();
  this->plate_rot_x_offset = this->par_plate_rot_x_offset.Get();
  this->plate_rot_y_offset = this->par_plate_rot_y_offset.Get();
  this->plate_rot_x_angle = this->par_plate_rot_x_angle.Get();
  this->fast_walking_speed = this->par_fast_walking.Get();
}

//----------------------------------------------------------------------
// mbbExperiments Control
//----------------------------------------------------------------------
bool mbbExperiments::ProcessTransferFunction(double activation)
{
  this->footload_left = this->si_left_foot_load.Get();
  this->footload_right = this->si_right_foot_load.Get();
  this->roll = this->si_roll.Get();
  this->pitch = this->si_pitch.Get();
  this->new_running = 0;
  data_ports::tPortDataPointer<const double> dequeued_buffer;
  while (dequeued_buffer = si_new_running.Dequeue())
  {
    this->new_running = std::max(this->new_running, *dequeued_buffer);
  }

  if (this->ci_reset.HasChanged())
  {
    this->reset_called = true;
    this->state = eEXP_STATE_RESET;//FIXME if this needed
    this->reset_started = true;
  }
  this->enable_experiment = this->ci_enabled_experiment.Get();
  rrlib::time::tTimestamp current_time = rrlib::time::Now(true);
  rrlib::time::tDuration delta_time = current_time - rrlib::time::cNO_TIME;
  double delta_time_milliseconds(std::chrono::duration_cast<std::chrono::milliseconds>(delta_time).count());
  double timediff_sec(delta_time_milliseconds / 1000);

  this->sphere_pos_x = this->ci_sphere_pos_x.Get();
  this->sphere_pos_y = this->ci_sphere_pos_y.Get();
  this->sphere_pos_z = this->ci_sphere_pos_z.Get();
  this->plate_pos_x = this->ci_plate_pos_x.Get() * sin(this->plate_translational_movement_x * 2 * M_PI * timediff_sec);
  this->plate_pos_y = this->ci_plate_pos_y.Get();
  this->plate_pos_z = this->ci_plate_pos_z.Get();

  /* plate rotate during dec module
  //  this->plate_rot_x_freq = this->plate_rot_x_freq ;
  //  this->phase_x += 2 * M_PI * timediff_sec * (-this->plate_rot_x_freq  + this->last_plate_rot_freq_x);
  //  this->plate_rot_x = this->ci_plate_rot_x.Get() * sin(this->plate_rot_x_freq * 2 * M_PI * timediff_sec + this->phase_x) + this->plate_rot_x_offset * uniform_distribution(eng);
  //  this->last_plate_rot_freq_x = this->plate_rot_x_freq;
  //
  //  this->plate_rot_y = this->ci_plate_rot_y.Get();
  //  this->phase_y += 2 * M_PI * timediff_sec * (-this->plate_rot_y_freq  + this->last_plate_rot_freq_y);
  //  this->plate_rot_y = this->ci_plate_rot_y.Get() * sin(this->plate_rot_y_freq * 2 * M_PI * timediff_sec + this->phase_y) + this->plate_rot_y_offset * uniform_distribution(eng);
  //  this->last_plate_rot_freq_y = this->plate_rot_y_freq;
  * */


//    if (this->ci_plate_rot_y.Get() > 0.0)
//    {
//        this->plate_rot_y = this->plate_rotation[this->iteration_random_rotation]* M_PI/180;
//      this->iteration_random_rotation++;
//      if (this->iteration_random_rotation > 2420)
//      {
//          this->plate_rot_y = 0;
//      }
//    }

  this->plate_rot_z = this->ci_plate_rot_z.Get() * sin(this->plate_rot_z_freq * 2 * M_PI * timediff_sec);

  this->exp_type = (int)this->ci_experiment_type.Get();
  if (this->enable_experiment == 0.0)
    this->state = eEXP_STATE_IDLE;
  else
  {
    // failsafe reset only when enabled
    if ((fabs(this->si_roll.Get()) > 0.4) || (fabs(this->si_pitch.Get()) > 0.4))
    {
      this->co_sensor_monitor.Publish(0.0);
      if ((this->repeat) && ((this->run < this->times_to_repeat + 1) || (this->times_to_repeat == 0)))
      {
        if ((this->repeat) && (this->run >= this->times_to_repeat))
        {
          this->state = eEXP_STATE_RESET;
          this->reset_started = true;
        }
      }
    }
  }
  switch (this->state)
  {
  case eEXP_STATE_IDLE:
  {
    this->desired_duration = (int)this->ci_desired_duration.Get();
    rrlib::time::tTimestamp current_time = rrlib::time::Now(true);
    this->loop_duration = current_time - this->last_looptime;
    this->duration_milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(this->loop_duration).count();
    this->walking_speed = 0.0;
    this->stim_walking = 0.0;
    this->walking_termi = 0.0;

    if (exp_type == eEXP_TYPE_NONE)
    {
      this->head_force_x = this->ci_head_force_x.Get();
      this->head_force_y = this->ci_head_force_y.Get();
      this->head_force_z = this->ci_head_force_z.Get();
      this->torso_force_x = this->ci_torso_force_x.Get();
      this->torso_force_y = this->ci_torso_force_y.Get();
      this->torso_force_z = this->ci_torso_force_z.Get();
      this->pelvis_force_x = this->ci_pelvis_force_x.Get();
      this->pelvis_force_y = this->ci_pelvis_force_y.Get();
      this->pelvis_force_z = this->ci_pelvis_force_z.Get();

      //FIXME to detect the push
//      if (abs(this->torso_force_x) >= 12)
//        this->push_x = true;
//      else
//        this->push_x = false;
    }
    else
    {
      this->head_force_x = 0.0;
      this->head_force_y = 0.0;
      this->head_force_z = 0.0;
      this->torso_force_x = 0.0;
      this->torso_force_y = 0.0;
      this->torso_force_z = 0.0;
      this->pelvis_force_x = 0.0;
      this->pelvis_force_y = 0.0;
      this->pelvis_force_z = 0.0;
    }

    if (this->enable_experiment == 1.0)
    {
      //this->co_sensor_monitor.Publish((float)this->log);
      if (this->si_stand_state.Get() == 2) // 3 TODO
      {
        this->state = eEXP_STATE_START;
        this->run = 1;
        this->run_time = 0;
        this->total_time = 0;
      }
    }
  }
  break;

  case eEXP_STATE_START:
  {

    // ---------------------------------------------------
    // TODO learning for push recovery
    // ---------------------------------------------------
    switch (this->exp_type)
    {
    case eEXP_TYPE_LEARNING:
    {
//      if (this->run_time < this->desired_duration) // first a disturbance of length desired_duration
//      {
//        this->run_time += this->duration_milliseconds;
//        this->total_time += this->duration_milliseconds;
//        // controller input values serve as maximum absolute values
//        // create random number between 0.0 and 1.0 and multiply with Controller Inputs
//        this->head_force_x = this->head_force_x * uniform_distribution(eng);
//        this->head_force_y = this->head_force_y * uniform_distribution(eng);
//        this->head_force_z = this->head_force_z * uniform_distribution(eng);
//        this->torso_force_x = this->torso_force_x * uniform_distribution(eng);
//        this->torso_force_y = this->torso_force_y * uniform_distribution(eng);
//        this->torso_force_z = this->torso_force_z * uniform_distribution(eng);
//        this->pelvis_force_x = this->pelvis_force_x * uniform_distribution(eng);
//        this->pelvis_force_y = this->pelvis_force_y * uniform_distribution(eng);
//        this->pelvis_force_z = this->pelvis_force_z * uniform_distribution(eng);
//      }
//      else // then walking init
//      {
//        this->head_force_x = 0.0;
//        this->head_force_y = 0.0;
//        this->head_force_z = 0.0;
//        this->torso_force_x = 0.0;
//        this->torso_force_y = 0.0;
//        this->torso_force_z = 0.0;
//        this->pelvis_force_x = 0.0;
//        this->pelvis_force_y = 0.0;
//        this->pelvis_force_z = 0.0;

      // walking init
      this->stim_walking = 1.0;
      this->walking_speed = 0.8;
      //this->walking_termi = 0.0;

      // wait several steps until it stands well
      this->restart_waiting_time -= 1;
      // walking init
      if (this->restart_waiting_time < 1)
      {
        this->stim_walking = 1.0;
        this->walking_speed = 0.8;
        this->restart_waiting_time = 50;
      }

      if (this->new_running > 0)
      {
        this->state = eEXP_STATE_RESET;
        FINROC_LOG_PRINT(DEBUG, "reset started due to new running");
        this->reset_started = true;
      }


      if ((this->si_loop_times.Get() >= 6.0) && (!this->push_force_start))
      {
        this->push_force_start = true;
        std::srand((unsigned)std::chrono::system_clock::now().time_since_epoch().count());
        this->random_loop_times = std::rand() % 2;//2
        this->random_direction = std::rand() & 1 ? 1 : -1;
        this->random_start_milliseconds = std::rand() % 250;
        this->random_start_milliseconds = LimitedValue<double>(this->random_start_milliseconds, 190, 220); // ankle hip: 60 - 80, knee: 190 -
        this->random_torso_force_x = std::rand() % 10;
        this->random_torso_force_x += -5;
        this->push_force_start_time = rrlib::time::Now(true);
      }

      if ((this->push_force_start) && (this->push_duration >= 0.0) && (this->si_loop_times.Get() >= 6))  //+his->random_loop_times)))
      {
        rrlib::time::tTimestamp current_time_push = rrlib::time::Now(true);
        rrlib::time::tDuration delta_time_push = current_time_push - this->push_force_start_time;
        double delta_time_milliseconds_push(std::chrono::duration_cast<std::chrono::milliseconds>(delta_time_push).count());
        if (delta_time_milliseconds_push >= this->random_start_milliseconds)
        {
          //this->torso_force_x = this->ci_torso_force_x.Get();
          //this->torso_force_y = this->ci_torso_force_y.Get();
          //this->torso_force_z = this->ci_torso_force_z.Get();
          //this->torso_force_y = 100 * this->random_direction;//80;
          this->torso_force_x = this->par_torso_force_x.Get() + this->random_torso_force_x;
          this->push_duration -= 25;
          //this->co_push_started.Publish(true);//lateral push detected
          this->co_push_started_x.Publish(true); // sagittal push detected
          this->co_push_detected_x.Publish(true);
          this->push_loop_time = this->si_loop_times.Get();
        }
      }
      else if (((this->si_loop_times.Get() - this->push_loop_time) >= 1) && (this->torso_force_x == 0.0))
        this->co_push_detected_x.Publish(false);
      // knee strategy

      // keep the push recovery state
//
      if ((this->torso_force_x != 0.))
        this->co_push_detected_x.Publish(true);
      else
        this->co_push_detected_x.Publish(false);

      if ((this->torso_force_y != 0.))
        this->co_push_detected_y.Publish(true);
      else
        this->co_push_detected_y.Publish(false);

      if (this->push_duration < 0.0)
      {
        this->torso_force_y = 0;
        this->torso_force_x = 0;
      }
//        if ((this->footload_left == 0.0) && (this->footload_right == 1.0) && (!this->off_ground))
//        {
//          this->off_ground = true;
//        }
//
//        if ((this->footload_left == 1.0) && (this->footload_right < 0.3) && (this->off_ground))
//        {
//          this->off_ground = false;
//          this->state = eEXP_STATE_RESET;
//          this->reset_started = true;
//          FINROC_LOG_PRINT(DEBUG, "reset started due to learning");
//        }

//      }
    }
    break;

    case eEXP_TYPE_FORCE_RANDOM:
    {
      if (this->run_time < this->desired_duration)
      {
        this->run_time += this->duration_milliseconds;
        this->total_time += this->duration_milliseconds;
        // controller input values serve as maximum absolute values
        // create random number between 0.0 and 1.0 and multiply with Controller Inputs
        this->head_force_x = this->head_force_x * uniform_distribution(eng);
        this->head_force_y = this->head_force_y * uniform_distribution(eng);
        this->head_force_z = this->head_force_z * uniform_distribution(eng);
        this->torso_force_x = this->torso_force_x * uniform_distribution(eng);
        this->torso_force_y = this->torso_force_y * uniform_distribution(eng);
        this->torso_force_z = this->torso_force_z * uniform_distribution(eng);
        this->pelvis_force_x = this->pelvis_force_x * uniform_distribution(eng);
        this->pelvis_force_y = this->pelvis_force_y * uniform_distribution(eng);
        this->pelvis_force_z = this->pelvis_force_z * uniform_distribution(eng);
      }
      else
        this->state = eEXP_STATE_END;
    }

    break;

    case eEXP_TYPE_PLATE_ROTATING:
    {
      this->desired_duration = (int)(this->ci_desired_duration.Get());

      if (this->run <= this->times_to_repeat)
      {
        if (this->run_time < this->desired_duration)
        {
          this->run_time += this->duration_milliseconds;
          this->total_time += this->duration_milliseconds;
          // controller input values serve as maximum absolute values
          // create random number between 0.0 and 1.0 and multiply with Controller Inputs
          this->phase_y += 2 * M_PI * timediff_sec * (-this->plate_rot_y_freq + this->last_plate_rot_freq_y);
          this->plate_rot_y = this->plate_rot_y_freq * sin(this->plate_rot_y_freq * 2 * M_PI * timediff_sec + this->phase_y);
          this->last_plate_rot_freq_y = this->plate_rot_y_freq;
        }
        else
        {
          this->phase_y = 0.0;
          this->plate_rot_y = 0.0;
          this->last_plate_rot_freq_y = 0.0;
          this->state = eEXP_STATE_RESET;
          this->reset_started = true;
          FINROC_LOG_PRINT(DEBUG, "reset started due to plate rotating");
        }
      }
      else
      {
        this->phase_y = 0.0;
        this->plate_rot_y = 0.0;
      }

    }

    break;

    case eEXP_TYPE_OPTIMIZATION:
    {
      this->head_force_x = 0.0;
      this->head_force_y = 0.0;
      this->head_force_z = 0.0;
      this->torso_force_x = 0.0;
      this->torso_force_y = 0.0;
      this->torso_force_z = 0.0;
      this->pelvis_force_x = 0.0;
      this->pelvis_force_y = 0.0;
      this->pelvis_force_z = 0.0;

      // wait several steps until it stands well
      this->restart_waiting_time -= 1;
      // walking init
      if (this->restart_waiting_time < 1)
      {
        this->restart_waiting_time = 50;
        // initial with several landing
        if (this->restart_landing_time > 0)
        {
          this->state = eEXP_STATE_RESET;
          FINROC_LOG_PRINT(DEBUG, "reset started to initial landing");
          this->reset_started = true;
          this->restart_landing_time = this->restart_landing_time - 1;
          std::cout << "The restart landing time equals " << this->restart_landing_time << std::endl;
        }
        else
        {
          this->stim_walking = 1.0;
          this->walking_speed = 0.8;

          //if ( this->ci_slow_walking.Get() )
          {
        	  if( this->si_loop_times.Get() > 4  && this->si_loop_times.Get() <= 6 )
        	  {
                  this->walking_speed = fast_walking_speed-0.06;
        	  }
        	  else       	  if( this->si_loop_times.Get() > 6 && this->si_loop_times.Get() <= 10 )
        	  {
        		  this->walking_speed = fast_walking_speed-0.04;
        	  }
        	  else if( this->si_loop_times.Get() > 10 && this->si_loop_times.Get() <= 14)
        	  {
        		  this->walking_speed = fast_walking_speed-0.02;
        	  }
        	  else if( this->si_loop_times.Get() > 14 )
        	  {
        		  this->walking_speed = fast_walking_speed;
        	  }

//////
//        	  else       	  if( this->si_loop_times.Get() > 10 && this->si_loop_times.Get() <= 14 )
//        	  {
//        		  this->walking_speed = 0.74;
//        	  }
//        	  else        	  if( this->si_loop_times.Get() > 14 && this->si_loop_times.Get() <= 26 && this->co_actual_speed.Get() > 0.58 )
//        	  {
//        		  this->walking_speed = 0.71;
//        	  }
////
//        	  else        	  if( this->si_loop_times.Get() > 27 && this->si_loop_times.Get() <= 30 )
//        	  {
//        		  this->walking_speed = 0.69;
//        	  }
//        	  else        	  if( this->si_loop_times.Get() > 31 )
//			  {
//				  this->walking_speed = 0.8;
//			  }
//        	  else            if( this->co_actual_speed.Get() < 0.58  && this->si_loop_times.Get() > 12 )
//        	  {
//        		  this->walking_speed = 0.74;
//        	  }
//        	  else            if( this->si_loop_times.Get() > 12 && this->co_actual_speed.Get() > 0.7 )
//        	  {
//        		  this->walking_speed = 0.7;
//        	  }

          }

//          if( !this->ci_slow_walking.Get() && this->si_loop_times.Get() > 4 && this->fast_walking_speed > 0.8 )
//          {
//        	  this->walking_speed = this->fast_walking_speed;
//          }
        }
      }
      // this is only for optimizing the walking speed!
//      if (this->si_loop_times.Get() < 3)
//      {
//        this->walking_speed = 0.8;
//      }
//      else if ((this->si_loop_times.Get() < 5) && (this->si_loop_times.Get() >= 3.0))
//        this->walking_speed = 0.8;
//      else
//      {
//        this->walking_speed = this->par_learning_walking_velocity.Get();
//      }
      if (this->new_running > 0)
      {
        this->state = eEXP_STATE_RESET;
        FINROC_LOG_PRINT(DEBUG, "reset started due to new running");
        this->reset_started = true;
        this->restart_landing_time = 1;
      }
    }
    break;
    default:
      break;
    }
    // ---------------------------------------------------

  }
  break;

  case eEXP_STATE_END:
  {
    this->total_time += this->duration_milliseconds;
    if (this->exp_type == eEXP_TYPE_FORCE_RANDOM)
    {
      this->head_force_x = 0.0;
      this->head_force_y = 0.0;
      this->head_force_z = 0.0;
      this->torso_force_x = 0.0;
      this->torso_force_y = 0.0;
      this->torso_force_z = 0.0;
      this->pelvis_force_x = 0.0;
      this->pelvis_force_y = 0.0;
      this->pelvis_force_z = 0.0;
    }

    if ((this->si_stiffness_factor.Get() < this->stiffness_threshold)
        && this->si_stand_state.Get() == 2) // 3 TODO
    {
      this->co_sensor_monitor.Publish(0.0);
      //        DM("Run %i finished!\n", this->run);
      if ((this->repeat) && ((this->run < this->times_to_repeat) || (this->times_to_repeat == 0)))
      {
        this->state = eEXP_STATE_RESET;
        this->reset_started = true;
        FINROC_LOG_PRINT(DEBUG, "reset started if this is repeat");

      }
    }
  }
  break;

  case eEXP_STATE_RESET:
  {
    this->total_time += this->duration_milliseconds;
    this->head_force_x = 0.0;
    this->head_force_y = 0.0;
    this->head_force_z = 0.0;
    this->torso_force_x = 0.0;
    this->torso_force_y = 0.0;
    this->torso_force_z = 0.0;
    this->pelvis_force_x = 0.0;
    this->pelvis_force_y = 0.0;
    this->pelvis_force_z = 0.0;

    if (this->exp_type == eEXP_TYPE_LEARNING)
    {
      this->stim_walking = 0.0;
      this->walking_speed = 0.0;
      this->walking_termi = 0.0;
      this->off_ground = false;
      this->co_sensor_monitor.Publish(0.0);
      this->push_force_start = false;
      this->push_duration = 180.0;
      this->push_detected = false;
      this->co_push_started.Publish(false);
      this->co_push_started_x.Publish(false);
      this->co_push_detected_x.Publish(false);
      this->push_loop_time = 0.;
    }

    if (this->exp_type == eEXP_TYPE_OPTIMIZATION)
    {
      this->stim_walking = 0.0;
      this->walking_speed = 0.0;
      this->walking_termi = 0.0;
      this->off_ground = false;
      this->co_sensor_monitor.Publish(0.0);
    }
    //FIXME
    if ((!this->reset_started) && (this->reset_time == 0))
    {
      this->state = eEXP_STATE_RESET_DONE;
      this->run = 0;
      this->run++;
      this->reset_called = false;
      this->run_time = 0;
      this->reset_time = 8;
      this->co_reset_simulation.Publish(false);
      FINROC_LOG_PRINTF(DEBUG_WARNING, "reset simulation finished!");

    }
    else if ((this->reset_started) && (this->reset_time != 0))
    {
      this->co_reset_simulation.Publish(true);
      FINROC_LOG_PRINTF(DEBUG_WARNING, "reset simulation experiment is true");
//      FINROC_LOG_PRINT(DEBUG_WARNING, "reset simulation : ", this->co_reset_simulation.Get());
      this->reset_time -= 1;
      if (this->reset_time < 1)
      {
        this->reset_started = false;
      }
    }
  }
  break;

  case eEXP_STATE_RESET_DONE:
  {
    this->total_time += this->duration_milliseconds;
    this->co_sensor_monitor.Publish((float)this->log);
    if (this->si_stand_state.Get() == 2) // 3 TODO
    {
      this->state = eEXP_STATE_START;
    }
  }
  break;

  default:
    RRLIB_LOG_PRINTF(ERROR, "Wrong state in mbbExperiments!");
    break;


  }

  this->so_run_time.Publish(this->run_time);
  this->so_total_time.Publish(this->total_time);
  this->so_experiment_state.Publish(this->state);
  this->so_run.Publish(this->run);


  this->co_sphere_pos_x.Publish(this->sphere_pos_x);
  this->co_sphere_pos_y.Publish(this->sphere_pos_y);
  this->co_sphere_pos_z.Publish(this->sphere_pos_z);
  this->co_plate_pos_x.Publish(this->plate_pos_x);
  this->co_plate_pos_y.Publish(this->plate_pos_y);
  this->co_plate_pos_z.Publish(this->plate_pos_z);
  this->co_plate_rot_x.Publish(this->plate_rot_x);
  this->co_plate_rot_y.Publish(this->plate_rot_y);
  this->co_plate_rot_z.Publish(this->plate_rot_z);
  rrlib::math::tVec3d plate_pos_temp(this->plate_pos_x, this->plate_pos_y, this->plate_pos_z);
  this->co_plate_pos.Publish(plate_pos_temp);
  this->co_plate_rot.Publish(tPose3D(0.0, 0.0, 0.0, tAngleRad(this->plate_rot_x), tAngleRad(this->plate_rot_y), tAngleRad(this->plate_rot_z)));

  this->co_stimulation_walking.Publish(this->stim_walking);
  this->co_walking_speed.Publish(this->walking_speed);
  //this->co_push_detected_x.Publish(this->push_x);

  this->last_looptime = rrlib::time::Now(true);

  rrlib::math::tVec3d head_force_3d;
  head_force_3d.X() = this->head_force_x;
  head_force_3d.Y() = this->head_force_y;
  head_force_3d.Z() = this->head_force_z;
  this->co_head_force.Publish(head_force_3d, this->last_looptime);

  rrlib::math::tVec3d torso_force_3d;
  torso_force_3d.X() = this->torso_force_x;
  torso_force_3d.Y() = this->torso_force_y;
  torso_force_3d.Z() = this->torso_force_z;
  this->co_torso_force.Publish(torso_force_3d, this->last_looptime);

  this->co_torso_force_y.Publish(this->torso_force_y);
  this->co_torso_force_x.Publish(this->torso_force_x);
  rrlib::math::tVec3d pelvis_force_3d;
  pelvis_force_3d.X() = this->pelvis_force_x;
  pelvis_force_3d.Y() = this->pelvis_force_y;
  pelvis_force_3d.Z() = this->pelvis_force_z;
  this->co_pelvis_force.Publish(pelvis_force_3d, this->last_looptime);
  this->co_slow_walking.Publish(ci_slow_walking.Get());
  return true;
}


//----------------------------------------------------------------------
// mbbExperiments CalculateActivity
//----------------------------------------------------------------------
ib2c::tActivity mbbExperiments::CalculateActivity(std::vector<ib2c::tActivity> &derived_activity, double activation) const
{
  return activation;
}

//----------------------------------------------------------------------
// mbbExperiments CalculateTargetRating
//----------------------------------------------------------------------
ib2c::tTargetRating mbbExperiments::CalculateTargetRating(double activation) const
{
  return 0.5;
}
//----------------------------------------------------------------------
// End of namespace declaration
//----------------------------------------------------------------------
}
}
}
