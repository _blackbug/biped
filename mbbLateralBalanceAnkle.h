//
// You received this file as part of Finroc
// A Framework for intelligent robot control
//
// Copyright (C) Finroc GbR (finroc.org)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//----------------------------------------------------------------------
/*!\file    projects/biped/reflexes/mbbLateralBalanceAnkle.h
 *
 * \author  Jie Zhao
 * \author Tobias Luksch
 *
 * \date    2013-09-25
 *
 * \brief Contains mbbLateralBalanceAnkle
 *
 * \b mbbLateralBalanceAnkle
 *
 *
 * Control the ankle roll joint during cyclic walking!
 *
 */
//----------------------------------------------------------------------
#ifndef __projects__biped__reflexes__mbbLateralBalanceAnkle_h__
#define __projects__biped__reflexes__mbbLateralBalanceAnkle_h__

#include "plugins/ib2c/tModule.h"
#include "plugins/data_ports/tGenericPort.h"
#include "plugins/data_ports/tEvent.h"
//----------------------------------------------------------------------
// External includes (system with <>, local with "")
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Internal includes with ""
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Namespace declaration
//----------------------------------------------------------------------
namespace finroc
{
namespace biped
{
namespace reflexes
{

//----------------------------------------------------------------------
// Forward declarations / typedefs / enums
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Class declaration
//----------------------------------------------------------------------
//! SHORT_DESCRIPTION
/*!
 *
 * Control the ankle roll joint during cyclic walking!
 */
class mbbLateralBalanceAnkle : public ib2c::tModule
{

//----------------------------------------------------------------------
// Ports (These are the only variables that may be declared public)
//----------------------------------------------------------------------
public:

  //tStaticParameter<double> static_parameter_1;   //Example for a static parameter. Replace or delete it!

  tParameter<double> par_max_torque;   //Example for a runtime parameter. Replace or delete it!
  tParameter<double> par_factor;
  tParameter<double> par_integral_factor;
  tParameter<bool> par_learning_on;
  tParameter<double> par_slow_walking_max_torque;
  tParameter<double> par_fast_walking_max_torque;
  tParameter<double> par_fast_walking_factor;

  tParameter<double> par_slow_walking_factor;

  tInput<double> ci_com_error_y;   //Example for input ports. Replace or delete them!
  tInput<double> ci_walking_scale;
  tInput<data_ports::tEvent> ci_reset_simulation;
  tInput<bool> ci_reset_simulation_experiments;
  tInput<double> ci_turn_left_signal;
  tInput<double> ci_turn_right_signal;

  //tInput<double> ci_push_detected_walking_left;
  //tInput<double> ci_push_detected_walking_right;
  tInput<double> si_force_z;
  tInput<double> si_ankle_x_angle;
  tInput<double> si_ground_contact;
  tInput<double> ci_estim_com_y;
  tInput<double> ci_torque_activity;
  tInput<bool> ci_slow_walking;
  tInput<bool> ci_fast_walking;

  tOutput<double> co_ankle_x_torque;   //Examples for output ports. Replace or delete them!
  tOutput<double> co_ankle_x_angle;
  tOutput<double> co_activity_torque;
  tOutput<double> co_activity_angle;
  tOutput<double> co_integral;

//----------------------------------------------------------------------
// Public methods and typedefs
//----------------------------------------------------------------------
public:

  mbbLateralBalanceAnkle(core::tFrameworkElement *parent, const std::string &name = "LateralBalanceAnkle",
                         ib2c::tStimulationMode stimulation_mode = ib2c::tStimulationMode::AUTO, unsigned int number_of_inhibition_ports = 0);
  void OnPortChange(data_ports::tChangeContext& change_context);

//----------------------------------------------------------------------
// Private fields and methods
//----------------------------------------------------------------------
private:

  //Here is the right place for your variables. Replace this line by your declarations!
  float max_torque;
  float factor;
  float com_error;
  float ankle_angle;
  float walking_scale;
  float target_torque;
  float target_angle;
  float force_factor;
  float integral_factor;
  float integral_part;
  std::atomic<bool> reset_simulation;
  float calculated_activity;
  float turn_signal;
  float slow_walking_max_torque;
  float slow_walking_factor;
  float fast_walking_max_torque;
  float fast_walking_factor;

  /*! Destructor
   *
   * The destructor of modules is declared private to avoid accidental deletion. Deleting
   * modules is already handled by the framework.
   */
  ~mbbLateralBalanceAnkle();

  virtual void OnParameterChange();   //Might be needed to react to changes in parameters independent from Update() calls. Delete otherwise!

  virtual bool ProcessTransferFunction(double activation);

  virtual ib2c::tActivity CalculateActivity(std::vector<ib2c::tActivity> &derived_activities, double activation) const;

  virtual ib2c::tTargetRating CalculateTargetRating(double activation) const;

};

//----------------------------------------------------------------------
// End of namespace declaration
//----------------------------------------------------------------------
}
}
}



#endif
