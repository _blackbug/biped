//
// You received this file as part of Finroc
// A Framework for intelligent robot control
//
// Copyright (C) Finroc GbR (finroc.org)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//----------------------------------------------------------------------
/*!\file    projects/biped/control/gLowerTrunkLateralWalking.h
 *
 * \author  Jie Zhao
 *
 * \date    2013-10-31
 *
 * \brief Contains gLowerTrunkLateralWalking
 *
 * \b gLowerTrunkLateralWalking
 *
 *
 * Lateral walking for lower trunk
 *
 */
//----------------------------------------------------------------------
#ifndef __projects__biped__control__gLowerTrunkLateralWalking_h__
#define __projects__biped__control__gLowerTrunkLateralWalking_h__

#include "plugins/structure/tSenseControlGroup.h"

//----------------------------------------------------------------------
// External includes (system with <>, local with "")
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Internal includes with ""
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Namespace declaration
//----------------------------------------------------------------------
namespace finroc
{
namespace biped
{
namespace control
{

//----------------------------------------------------------------------
// Forward declarations / typedefs / enums
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Class declaration
//----------------------------------------------------------------------
//! SHORT_DESCRIPTION
/*!
 *
 * Lateral walking for lower trunk
 */
class gLowerTrunkLateralWalking : public structure::tSenseControlGroup
{

//----------------------------------------------------------------------
// Ports (These are the only variables that may be declared public)
//----------------------------------------------------------------------
public:

  tSensorInput<double> si_spine_x_torque;
  tSensorInput<double> si_spine_x_angle;
  tSensorInput<double> si_spine_y_torque;
  tSensorInput<double> si_spine_y_angle;
  tSensorInput<double> si_spine_z_torque;
  tSensorInput<double> si_spine_z_angle;
  tSensorInput<double> si_left_hip_x_torque;
  tSensorInput<double> si_left_hip_x_angle;
  tSensorInput<double> si_left_hip_y_torque;
  tSensorInput<double> si_left_hip_y_angle;
  tSensorInput<double> si_left_hip_z_torque;
  tSensorInput<double> si_left_hip_z_angle;
  tSensorInput<double> si_right_hip_x_torque;
  tSensorInput<double> si_right_hip_x_angle;
  tSensorInput<double> si_right_hip_y_torque;
  tSensorInput<double> si_right_hip_y_angle;
  tSensorInput<double> si_right_hip_z_torque;
  tSensorInput<double> si_right_hip_z_angle;

  tControllerInput<double> ci_ground_contact;
  tControllerInput<double> ci_left_ground_contact;
  tControllerInput<double> ci_right_ground_contact;
  tControllerInput<double> ci_left_foot_load;
  tControllerInput<double> ci_right_foot_load;
  tControllerInput<double> ci_sensor_monitor;

  tControllerInput<double> ci_actual_accel_x;
  tControllerInput<double> ci_actual_accel_y;
  tControllerInput<double> ci_actual_accel_z;
  tControllerInput<double> ci_actual_omega_x;
  tControllerInput<double> ci_actual_omega_y;
  tControllerInput<double> ci_actual_omega_z;
  tControllerInput<double> ci_actual_roll;
  tControllerInput<double> ci_actual_pitch;
  tControllerInput<double> ci_actual_yaw;
  tControllerInput<double> ci_adjusted_accel_x;
  tControllerInput<double> ci_adjusted_accel_y;
  tControllerInput<double> ci_adjusted_accel_z;
  tControllerInput<double> ci_adjusted_roll;
  tControllerInput<double> ci_adjusted_pitch;
  tControllerInput<double> ci_com_x;
  tControllerInput<double> ci_com_y;
  tControllerInput<double> ci_xcom_x;
  tControllerInput<double> ci_xcom_y;
  tControllerInput<double> ci_com_error_y;
  tControllerInput<double> ci_inv_com_error_y;
  tControllerInput<double> ci_left_lateral_velocity_correction;
  tControllerInput<double> ci_right_lateral_velocity_correction;
  tControllerInput<double> ci_left_com_x;
  tControllerInput<double> ci_right_com_x;
  tControllerInput<double> ci_left_com_y;
  tControllerInput<double> ci_right_com_y;
  tControllerInput<double> ci_left_xcom_x;
  tControllerInput<double> ci_right_xcom_x;
  tControllerInput<double> ci_left_xcom_y;
  tControllerInput<double> ci_right_xcom_y;
  tControllerInput<double> ci_left_frontal_velocity_correction;
  tControllerInput<double> ci_right_frontal_velocity_correction;

  tControllerInput<double> ci_walking_scale_laterally;
  tControllerInput<double> ci_walking_lateral_activity;

  tControllerInput<double> ci_left_stimulation_walking_lateral_phase_1;
  tControllerInput<double> ci_left_stimulation_walking_lateral_phase_2;
  tControllerInput<double> ci_left_stimulation_walking_lateral_phase_3;
  tControllerInput<double> ci_left_stimulation_walking_lateral_phase_4;
  tControllerInput<double> ci_left_stimulation_walking_lateral_phase_5;

  tControllerInput<double> ci_right_stimulation_walking_lateral_phase_1;
  tControllerInput<double> ci_right_stimulation_walking_lateral_phase_2;
  tControllerInput<double> ci_right_stimulation_walking_lateral_phase_3;
  tControllerInput<double> ci_right_stimulation_walking_lateral_phase_4;
  tControllerInput<double> ci_right_stimulation_walking_lateral_phase_5;

  tControllerInput<double> ci_stimulation_termination;
  tControllerInput<double> ci_termi_last_step;
  tControllerInput<double> ci_last_step;
  tControllerInput<double> ci_last_step_state;
  tControllerInput<double> ci_termi_lock_hip;
  tControllerInput<double> ci_stop_upright_trunk;
  tControllerInput<double> ci_maintain_stable;
  tControllerInput<double> ci_s_leg_left;
  tControllerInput<double> ci_s_leg_right;
  tControllerInput<double> ci_for_non_used_fusion_modules;

  tSensorOutput<double> so_spine_x_torque;
  tSensorOutput<double> so_spine_x_angle;
  tSensorOutput<double> so_spine_y_torque;
  tSensorOutput<double> so_spine_y_angle;
  tSensorOutput<double> so_spine_z_torque;
  tSensorOutput<double> so_spine_z_angle;
  tSensorOutput<double> so_left_hip_x_torque;
  tSensorOutput<double> so_left_hip_x_angle;
  tSensorOutput<double> so_left_hip_y_torque;
  tSensorOutput<double> so_left_hip_y_angle;
  tSensorOutput<double> so_left_hip_z_torque;
  tSensorOutput<double> so_left_hip_z_angle;
  tSensorOutput<double> so_right_hip_x_torque;
  tSensorOutput<double> so_right_hip_x_angle;
  tSensorOutput<double> so_right_hip_y_torque;
  tSensorOutput<double> so_right_hip_y_angle;
  tSensorOutput<double> so_right_hip_z_torque;
  tSensorOutput<double> so_right_hip_z_angle;
  tSensorOutput<double> so_lower_trunk_co_log_updated;


  tControllerOutput<double> co_stimulation_spine_x_torque;
  tControllerOutput<double> co_spine_x_torque;
  tControllerOutput<double> co_stimulation_spine_x_angle;
  tControllerOutput<double> co_spine_x_angle;
  tControllerOutput<double> co_stimulation_spine_y_torque;
  tControllerOutput<double> co_spine_y_torque;
  tControllerOutput<double> co_stimulation_spine_y_angle;
  tControllerOutput<double> co_spine_y_angle;
  tControllerOutput<double> co_stimulation_spine_z_torque;
  tControllerOutput<double> co_spine_z_torque;
  tControllerOutput<double> co_stimulation_spine_z_angle;
  tControllerOutput<double> co_spine_z_angle;

  tControllerOutput<double> co_left_stimulation_hip_x_torque;
  tControllerOutput<double> co_left_hip_x_torque;
  tControllerOutput<double> co_left_stimulation_hip_x_angle;
  tControllerOutput<double> co_left_hip_x_angle;
  tControllerOutput<double> co_left_stimulation_hip_y_torque;
  tControllerOutput<double> co_left_hip_y_torque;
  tControllerOutput<double> co_left_stimulation_hip_y_angle;
  tControllerOutput<double> co_left_hip_y_angle;
  tControllerOutput<double> co_left_stimulation_hip_z_torque;
  tControllerOutput<double> co_left_hip_z_torque;
  tControllerOutput<double> co_left_stimulation_hip_z_angle;
  tControllerOutput<double> co_left_hip_z_angle;

  tControllerOutput<double> co_right_stimulation_hip_x_torque;
  tControllerOutput<double> co_right_hip_x_torque;
  tControllerOutput<double> co_right_stimulation_hip_x_angle;
  tControllerOutput<double> co_right_hip_x_angle;
  tControllerOutput<double> co_right_stimulation_hip_y_torque;
  tControllerOutput<double> co_right_hip_y_torque;
  tControllerOutput<double> co_right_stimulation_hip_y_angle;
  tControllerOutput<double> co_right_hip_y_angle;
  tControllerOutput<double> co_right_stimulation_hip_z_torque;
  tControllerOutput<double> co_right_hip_z_torque;
  tControllerOutput<double> co_right_stimulation_hip_z_angle;
  tControllerOutput<double> co_right_hip_z_angle;

  tControllerOutput<double> co_target_rating;


//----------------------------------------------------------------------
// Public methods and typedefs
//----------------------------------------------------------------------
public:

  gLowerTrunkLateralWalking(core::tFrameworkElement *parent, const std::string &name = "LowerTrunkLateralWalking",
                            const std::string &structure_config_file = __FILE__".xml");

//----------------------------------------------------------------------
// Private fields and methods
//----------------------------------------------------------------------
private:

  /*! Destructor
   *
   * The destructor of modules is declared private to avoid accidental deletion. Deleting
   * modules is already handled by the framework.
   */
  ~gLowerTrunkLateralWalking();

};

//----------------------------------------------------------------------
// End of namespace declaration
//----------------------------------------------------------------------
}
}
}



#endif
