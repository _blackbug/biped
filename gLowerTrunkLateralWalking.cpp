//
// You received this file as part of Finroc
// A Framework for intelligent robot control
//
// Copyright (C) Finroc GbR (finroc.org)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//----------------------------------------------------------------------
/*!\file    projects/biped/control/gLowerTrunkLateralWalking.cpp
 *
 * \author  Jie Zhao
 *
 * \date    2013-10-31
 *
 */
//----------------------------------------------------------------------
#include "projects/biped/control/gLowerTrunkLateralWalking.h"
#include "plugins/ib2c/mbbFusion.h"

#include "projects/biped/skills/mbbUberSkill.h"
#include "projects/biped/skills/mbbStimulationSkill.h"
#include "projects/biped/motor_patterns/mbbActiveHipSwing.h"
#include "projects/biped/motor_patterns/mbbLateralHipShift.h"
#include "projects/biped/motor_patterns/mbbSingleTorquePattern.h"
#include "projects/biped/motor_patterns/mbbDoubleTorquePattern.h"
#include "projects/biped/motor_patterns/mbbDoubleLateralTorquePattern.h"
#include "projects/biped/motor_patterns/mbbTurnUpperTrunk.h"
#include "projects/biped/reflexes/mbbBalanceBodyRoll.h"
#include "projects/biped/reflexes/mbbAdjustBodyPitch.h"
#include "projects/biped/reflexes/mbbBalanceJoint.h"
#include "projects/biped/reflexes/mbbLockHipRoll.h"
#include "projects/biped/reflexes/mbbLateralFootPlacement.h"
#include "projects/biped/reflexes/mbbFrontalFootPlacement.h"
#include "projects/biped/reflexes/mbbControlPelvisRoll.h"
#include "projects/biped/reflexes/mbbControlPelvisYaw.h"
#include "projects/biped/reflexes/mbbControlPelvisPitch.h"
//----------------------------------------------------------------------
// External includes (system with <>, local with "")
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Internal includes with ""
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Debugging
//----------------------------------------------------------------------
#include <cassert>

//----------------------------------------------------------------------
// Namespace usage
//----------------------------------------------------------------------

using namespace finroc::ib2c;
using namespace finroc::biped::skills;
using namespace finroc::biped::reflexes;
using namespace finroc::biped::motor_patterns;
//----------------------------------------------------------------------
// Namespace declaration
//----------------------------------------------------------------------
namespace finroc
{
namespace biped
{
namespace control
{

//----------------------------------------------------------------------
// Forward declarations / typedefs / enums
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Const values
//----------------------------------------------------------------------
static runtime_construction::tStandardCreateModuleAction<gLowerTrunkLateralWalking> cCREATE_ACTION_FOR_G_LOWERTRUNKLATERALWALKING("LowerTrunkLateralWalking");

//----------------------------------------------------------------------
// Implementation
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// gLowerTrunkLateralWalking constructor
//----------------------------------------------------------------------
gLowerTrunkLateralWalking::gLowerTrunkLateralWalking(core::tFrameworkElement *parent, const std::string &name,
    const std::string &structure_config_file) :
  tSenseControlGroup(parent, name, structure_config_file, false) // change to 'true' to make group's ports shared (so that ports in other processes can connect to its sensor outputs and controller inputs)
{
  //BEGIN  initiating fusion behaviours

//  // spine
//  mbbFusion<double> * fusion_abs_angle_spine_x = new mbbFusion<double>(this,  "Abs. Angle Spine X", 1, tStimulationMode::ENABLED, 1);
//  fusion_abs_angle_spine_x->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
//  this->ci_for_non_used_fusion_modules.ConnectTo(fusion_abs_angle_spine_x->InputActivity(0));
//  mbbFusion<double> * fusion_abs_angle_spine_y = new mbbFusion<double>(this,  "Abs. Angle Spine Y", 1, tStimulationMode::ENABLED, 1);
//  fusion_abs_angle_spine_y->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
//  mbbFusion<double> * fusion_abs_angle_spine_z = new mbbFusion<double>(this, "Abs. Angle Spine Z", 1, tStimulationMode::ENABLED, 1);
//  fusion_abs_angle_spine_z->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
//  this->ci_for_non_used_fusion_modules.ConnectTo(fusion_abs_angle_spine_z->InputActivity(0));
//  mbbFusion<double> * fusion_abs_torque_spine_x = new mbbFusion<double>(this, "Abs. Torque Spine X", 2, tStimulationMode::ENABLED, 1);
//  fusion_abs_torque_spine_x->fusion_method.Set(tFusionMethod::WEIGHTED_SUM);
//  fusion_abs_torque_spine_x->number_of_inhibition_ports.Set(1);
//  fusion_abs_torque_spine_x->CheckStaticParameters();
//  mbbFusion<double> * fusion_abs_torque_spine_y = new mbbFusion<double>(this, "Abs. Torque Spine Y", 1, tStimulationMode::ENABLED, 1);
//  fusion_abs_torque_spine_y->fusion_method.Set(tFusionMethod::WEIGHTED_SUM);
//  fusion_abs_torque_spine_y->number_of_inhibition_ports.Set(1);
//  fusion_abs_torque_spine_y->CheckStaticParameters();
//  mbbFusion<double> * fusion_abs_torque_spine_z = new mbbFusion<double>(this, "Abs. Torque Spine Z", 1, tStimulationMode::ENABLED, 1);
//  fusion_abs_torque_spine_z->fusion_method.Set(tFusionMethod::WEIGHTED_SUM);
//  fusion_abs_torque_spine_z->number_of_inhibition_ports.Set(1);
//  fusion_abs_torque_spine_z->CheckStaticParameters();
//
//
//  // left hip
//  mbbFusion<double> * fusion_abs_angle_left_hip_x = new mbbFusion<double>(this,  "Abs. Angle Left Hip X", 3, tStimulationMode::ENABLED, 1);
//  fusion_abs_angle_left_hip_x->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
//  mbbFusion<double> * fusion_abs_angle_left_hip_y = new mbbFusion<double>(this,  "Abs. Angle Left Hip Y", 3, tStimulationMode::ENABLED, 1);
//  fusion_abs_angle_left_hip_y->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
//  mbbFusion<double> * fusion_abs_angle_left_hip_z = new mbbFusion<double>(this, "Abs. Angle Left Hip Z", 2, tStimulationMode::ENABLED, 1);
//  fusion_abs_angle_left_hip_z->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
//
//  mbbFusion<double> * fusion_abs_torque_left_hip_x = new mbbFusion<double>(this, "Abs. Torque Left Hip X", 6, tStimulationMode::ENABLED, 1);
//  fusion_abs_torque_left_hip_x->fusion_method.Set(tFusionMethod::WEIGHTED_SUM);
//  fusion_abs_torque_left_hip_x->number_of_inhibition_ports.Set(1);
//  fusion_abs_torque_left_hip_x->CheckStaticParameters();
//  mbbFusion<double> * fusion_abs_torque_left_hip_y = new mbbFusion<double>(this, "Abs. Torque Left Hip Y", 1, tStimulationMode::ENABLED, 1);
//  fusion_abs_torque_left_hip_y->fusion_method.Set(tFusionMethod::WEIGHTED_SUM);
//  fusion_abs_torque_left_hip_y->number_of_inhibition_ports.Set(1);
//  fusion_abs_torque_left_hip_y->CheckStaticParameters();
//  mbbFusion<double> * fusion_abs_torque_left_hip_z = new mbbFusion<double>(this, "Abs. Torque Left Hip Z", 1, tStimulationMode::ENABLED, 1);
//  fusion_abs_torque_left_hip_z->fusion_method.Set(tFusionMethod::WEIGHTED_SUM);
//  fusion_abs_torque_left_hip_z->number_of_inhibition_ports.Set(1);
//  fusion_abs_torque_left_hip_z->CheckStaticParameters();
//
//  // right hip
//  mbbFusion<double> * fusion_abs_angle_right_hip_x = new mbbFusion<double>(this,  "Abs. Angle Right Hip X", 3, tStimulationMode::ENABLED, 1);
//  fusion_abs_angle_right_hip_x->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
//  mbbFusion<double> * fusion_abs_angle_right_hip_y = new mbbFusion<double>(this,  "Abs. Angle Right Hip Y", 3, tStimulationMode::ENABLED, 1);
//  fusion_abs_angle_right_hip_y->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
//  mbbFusion<double> * fusion_abs_angle_right_hip_z = new mbbFusion<double>(this, "Abs. Angle Right Hip Z", 2, tStimulationMode::ENABLED, 1);
//  fusion_abs_angle_right_hip_z->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
//
//  mbbFusion<double> * fusion_abs_torque_right_hip_x = new mbbFusion<double>(this, "Abs. Torque Right Hip X", 6, tStimulationMode::ENABLED, 1);
//  fusion_abs_torque_right_hip_x->fusion_method.Set(tFusionMethod::WEIGHTED_SUM);
//  fusion_abs_torque_right_hip_x->number_of_inhibition_ports.Set(1);
//  fusion_abs_torque_right_hip_x->CheckStaticParameters();
//  mbbFusion<double> * fusion_abs_torque_right_hip_y = new mbbFusion<double>(this, "Abs. Torque Right Hip Y", 1, tStimulationMode::ENABLED, 1);
//  fusion_abs_torque_right_hip_y->fusion_method.Set(tFusionMethod::WEIGHTED_SUM);
//  fusion_abs_torque_right_hip_y->number_of_inhibition_ports.Set(1);
//  fusion_abs_torque_right_hip_y->CheckStaticParameters();
//  mbbFusion<double> * fusion_abs_torque_right_hip_z = new mbbFusion<double>(this, "Abs. Torque Right Hip Z", 1, tStimulationMode::ENABLED, 1);
//  fusion_abs_torque_right_hip_z->fusion_method.Set(tFusionMethod::WEIGHTED_SUM);
//  fusion_abs_torque_right_hip_z->number_of_inhibition_ports.Set(1);
//  fusion_abs_torque_right_hip_z->CheckStaticParameters();
//
//  // lateral hip shift
//  mbbFusion<double> * fusion_lateral_hip_shift_left = new mbbFusion<double>(this, "Lateral Hip Shift Left", 2, tStimulationMode::ENABLED);
//  fusion_lateral_hip_shift_left->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
//  mbbFusion<double> * fusion_lateral_hip_shift_right = new mbbFusion<double>(this, "Lateral Hip Shift Right", 2, tStimulationMode::ENABLED);
//  fusion_lateral_hip_shift_right->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
//
//  // active hip swing
//  mbbFusion<double> * fusion_active_hip_swing_left = new mbbFusion<double>(this, "Active Hip Swing Left", 1, tStimulationMode::ENABLED);
//  fusion_active_hip_swing_left->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
//  mbbFusion<double> * fusion_active_hip_swing_right = new mbbFusion<double>(this, "Active Hip Swing Right", 1, tStimulationMode::ENABLED);
//  fusion_active_hip_swing_right->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
//
//  // lock hip roll
//  mbbFusion<double>  * fusion_lock_hip_roll_left = new mbbFusion<double> (this, "Lock Hip Roll Left", 2, tStimulationMode::ENABLED);
//  fusion_lock_hip_roll_left->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
//  mbbFusion<double>  * fusion_lock_hip_roll_right = new mbbFusion<double> (this, "Lock Hip Roll Right", 2, tStimulationMode::ENABLED);
//  fusion_lock_hip_roll_right->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
//
//  // upright trunk fusion
////      mbbFusion<double> * fusion_upright_trunk_roll_left = new mbbFusion<double>(this, "Upright Trunk Roll Left", 1, tStimulationMode::ENABLED);
////      fusion_upright_trunk_roll_left->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
////      mbbFusion<double> * fusion_upright_trunk_roll_right = new mbbFusion<double>(this, "Upright Trunk Roll Right", 1, tStimulationMode::ENABLED);
////      fusion_upright_trunk_roll_right->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
//
//  // control pelvis pitch fusion
//  mbbFusion<double> * fusion_control_pelvis_pitch_left = new mbbFusion<double>(this, "Control Pelvis Pitch Left", 2, tStimulationMode::ENABLED);
//  fusion_control_pelvis_pitch_left->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
//  mbbFusion<double> * fusion_control_pelvis_pitch_right = new mbbFusion<double>(this, "Control Pelvis Pitch Right", 2, tStimulationMode::ENABLED);
//  fusion_control_pelvis_pitch_right->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
//
//  // control pelvis roll fusion
//  mbbFusion<double> * fusion_control_pelvis_roll_left = new mbbFusion<double>(this, "Control Pelvis Roll Left", 1, tStimulationMode::ENABLED);
//  fusion_control_pelvis_roll_left->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
//  mbbFusion<double> * fusion_control_pelvis_roll_right = new mbbFusion<double>(this, "Control Pelvis Roll Right", 1, tStimulationMode::ENABLED);
//  fusion_control_pelvis_roll_right->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
//
//  // control pelvis yaw fusion
//  mbbFusion<double> * fusion_control_pelvis_yaw_left = new mbbFusion<double>(this, "Control Pelvis Yaw Left", 1, tStimulationMode::ENABLED);
//  fusion_control_pelvis_yaw_left->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
//  mbbFusion<double> * fusion_control_pelvis_yaw_right = new mbbFusion<double>(this, "Control Pelvis Yaw Right", 1, tStimulationMode::ENABLED);
//  fusion_control_pelvis_yaw_right->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
//
//  // frontal foot placement fusion
//  mbbFusion<double> * fusion_frontal_foot_placement_left = new mbbFusion<double>(this, "Frontal Foot Placement Left", 2, tStimulationMode::ENABLED);
//  fusion_frontal_foot_placement_left->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
//  mbbFusion<double> * fusion_frontal_foot_placement_right = new mbbFusion<double>(this, "Frontal Foot Placement Right", 2, tStimulationMode::ENABLED);
//  fusion_frontal_foot_placement_right->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
//
//  //todo:
//  // upright trunk fusion
//
//
//  //END initiating fusion behaviours
//
//  // STANDING CONTROL UNITS
//  // ================================================
//  // WALKING CONTROL UNITS
//  // ================================================
//
//  // walking phases
//  mbbStimulationSkill* skill_walking_lateral = new mbbStimulationSkill(this, "(Ph) Walking Lateral All Phases", tStimulationMode::AUTO, 0, 6);
//  skill_walking_lateral->SetConfigNode("LowerTrunk/phases/walking_lateral_all_phases/");
//  skill_walking_lateral->par_stimulation_factor[0].SetConfigEntry("LowerTrunk/phases/walking_lateral_all_phases/hip_y_angle");
//  skill_walking_lateral->par_min_angle[0].SetConfigEntry("LowerTrunk/phases/walking_lateral_all_phases/hip_y_angle");
//  skill_walking_lateral->par_max_angle[0].SetConfigEntry("LowerTrunk/phases/walking_lateral_all_phases/hip_y_angle");
//  skill_walking_lateral->par_target_angle[0].SetConfigEntry("LowerTrunk/phases/walking_lateral_all_phases/hip_y_angle");
//  skill_walking_lateral->par_stimulation_factor[1].SetConfigEntry("LowerTrunk/phases/walking_lateral_all_phases/hip_x_angle");
//  skill_walking_lateral->par_min_angle[1].SetConfigEntry("LowerTrunk/phases/walking_lateral_all_phases/hip_x_angle");
//  skill_walking_lateral->par_max_angle[1].SetConfigEntry("LowerTrunk/phases/walking_lateral_all_phases/hip_x_angle");
//  skill_walking_lateral->par_target_angle[1].SetConfigEntry("LowerTrunk/phases/walking_lateral_all_phases/hip_x_angle");
//
//  skill_walking_lateral->par_stimulation_factor[2].SetConfigEntry("LowerTrunk/phases/walking_lateral_all_phases/hip_z_angle");
//  skill_walking_lateral->par_min_angle[2].SetConfigEntry("LowerTrunk/phases/walking_lateral_all_phases/hip_z_angle");
//  skill_walking_lateral->par_max_angle[2].SetConfigEntry("LowerTrunk/phases/walking_lateral_all_phases/hip_z_angle");
//  skill_walking_lateral->par_target_angle[2].SetConfigEntry("LowerTrunk/phases/walking_lateral_all_phases/hip_z_angle");
//
//  skill_walking_lateral->par_stimulation_factor[3].SetConfigEntry("LowerTrunk/phases/walking_lateral_all_phases/spine_y_angle");
//  skill_walking_lateral->par_min_angle[3].SetConfigEntry("LowerTrunk/phases/walking_lateral_all_phases/spine_y_angle");
//  skill_walking_lateral->par_max_angle[3].SetConfigEntry("LowerTrunk/phases/walking_lateral_all_phases/spine_y_angle");
//  skill_walking_lateral->par_target_angle[3].SetConfigEntry("LowerTrunk/phases/walking_lateral_all_phases/spine_y_angle");
//  skill_walking_lateral->par_stimulation_factor[4].SetConfigEntry("LowerTrunk/phases/walking_lateral_all_phases/upright_trunk_roll_left");
//  skill_walking_lateral->par_stimulation_factor[5].SetConfigEntry("LowerTrunk/phases/walking_lateral_all_phases/upright_trunk_roll_right");
//
//  mbbStimulationSkill* skill_walking_lateral_phase1_left = new mbbStimulationSkill(this, "(Ph) Walking Lateral Phase 1 Left", tStimulationMode::AUTO, 0, 4);
//  skill_walking_lateral_phase1_left->SetConfigNode("LowerTrunk/phases/walking_lateral_phase_1/");
//  skill_walking_lateral_phase1_left->par_stimulation_factor[0].SetConfigEntry("LowerTrunk/phases/walking_lateral_phase_1/control_pelvis_pitch");
//  skill_walking_lateral_phase1_left->par_stimulation_factor[1].SetConfigEntry("LowerTrunk/phases/walking_lateral_phase_1/control_pelvis_yaw");
//  skill_walking_lateral_phase1_left->par_stimulation_factor[2].SetConfigEntry("LowerTrunk/phases/walking_lateral_phase_1/upright_hip_x");
//  skill_walking_lateral_phase1_left->par_min_angle[2].SetConfigEntry("LowerTrunk/phases/walking_lateral_phase_1/upright_hip_x");
//  skill_walking_lateral_phase1_left->par_max_angle[2].SetConfigEntry("LowerTrunk/phases/walking_lateral_phase_1/upright_hip_x");
//  skill_walking_lateral_phase1_left->par_target_angle[2].SetConfigEntry("LowerTrunk/phases/walking_lateral_phase_1/upright_hip_x");
//  skill_walking_lateral_phase1_left->par_stimulation_factor[3].SetConfigEntry("LowerTrunk/phases/walking_lateral_phase_1/lateral_hip_shift");
//
//  mbbStimulationSkill* skill_walking_lateral_phase1_right = new mbbStimulationSkill(this, "(Ph) Walking Lateral Phase 1 Right", tStimulationMode::AUTO, 0, 4);
//  skill_walking_lateral_phase1_right->SetConfigNode("LowerTrunk/phases/walking_lateral_phase_1/");
//  skill_walking_lateral_phase1_right->par_stimulation_factor[0].SetConfigEntry("LowerTrunk/phases/walking_lateral_phase_1/control_pelvis_pitch");
//  skill_walking_lateral_phase1_right->par_stimulation_factor[1].SetConfigEntry("LowerTrunk/phases/walking_lateral_phase_1/control_pelvis_yaw");
//  skill_walking_lateral_phase1_right->par_stimulation_factor[2].SetConfigEntry("LowerTrunk/phases/walking_lateral_phase_1/upright_hip_x");
//  skill_walking_lateral_phase1_right->par_min_angle[2].SetConfigEntry("LowerTrunk/phases/walking_lateral_phase_1/upright_hip_x");
//  skill_walking_lateral_phase1_right->par_max_angle[2].SetConfigEntry("LowerTrunk/phases/walking_lateral_phase_1/upright_hip_x");
//  skill_walking_lateral_phase1_right->par_target_angle[2].SetConfigEntry("LowerTrunk/phases/walking_lateral_phase_1/upright_hip_x");
//  skill_walking_lateral_phase1_right->par_stimulation_factor[3].SetConfigEntry("LowerTrunk/phases/walking_lateral_phase_1/lateral_hip_shift");
//
//  mbbStimulationSkill* skill_walking_lateral_phase2_left = new mbbStimulationSkill(this, "(Ph) Walking Lateral Phase 2 Left", tStimulationMode::AUTO, 0, 3);
//  skill_walking_lateral_phase2_left->SetConfigNode("LowerTrunk/phases/walking_lateral_phase_2/");
//  skill_walking_lateral_phase2_left->par_stimulation_factor[0].SetConfigEntry("LowerTrunk/phases/walking_lateral_phase_2/control_pelvis_pitch");
//  skill_walking_lateral_phase2_left->par_stimulation_factor[1].SetConfigEntry("LowerTrunk/phases/walking_lateral_phase_2/control_pelvis_yaw");
//  skill_walking_lateral_phase2_left->par_stimulation_factor[2].SetConfigEntry("LowerTrunk/phases/walking_lateral_phase_2/lateral_hip_shift");
//
//  mbbStimulationSkill* skill_walking_lateral_phase2_right = new mbbStimulationSkill(this, "(Ph) Walking Lateral Phase 2 Right", tStimulationMode::AUTO, 0, 3);
//  skill_walking_lateral_phase2_right->SetConfigNode("LowerTrunk/phases/walking_lateral_phase_2/");
//  skill_walking_lateral_phase2_right->par_stimulation_factor[0].SetConfigEntry("LowerTrunk/phases/walking_lateral_phase_2/control_pelvis_pitch");
//  skill_walking_lateral_phase2_right->par_stimulation_factor[1].SetConfigEntry("LowerTrunk/phases/walking_lateral_phase_2/control_pelvis_yaw");
//  skill_walking_lateral_phase2_right->par_stimulation_factor[2].SetConfigEntry("LowerTrunk/phases/walking_lateral_phase_2/lateral_hip_shift");
//
//  mbbStimulationSkill* skill_walking_lateral_phase3_left = new mbbStimulationSkill(this, "(Ph) Walking Lateral Phase 3 Left", tStimulationMode::AUTO, 0, 3);
//  skill_walking_lateral_phase3_left->SetConfigNode("LowerTrunk/phases/walking_lateral_phase_2/");
//  skill_walking_lateral_phase3_left->par_stimulation_factor[0].SetConfigEntry("LowerTrunk/phases/walking_lateral_phase_3/active_hip_swing");
//  skill_walking_lateral_phase3_left->par_stimulation_factor[1].SetConfigEntry("LowerTrunk/phases/walking_lateral_phase_3/lock_hip_roll");
//  skill_walking_lateral_phase3_left->par_stimulation_factor[2].SetConfigEntry("LowerTrunk/phases/walking_lateral_phase_3/frontal_foot_placement");
//
//  mbbStimulationSkill* skill_walking_lateral_phase3_right = new mbbStimulationSkill(this, "(Ph) Walking Lateral Phase 3 Right", tStimulationMode::AUTO, 0, 3);
//  skill_walking_lateral_phase3_right->SetConfigNode("LowerTrunk/phases/walking_lateral_phase_2/");
//  skill_walking_lateral_phase3_right->par_stimulation_factor[0].SetConfigEntry("LowerTrunk/phases/walking_lateral_phase_3/active_hip_swing");
//  skill_walking_lateral_phase3_right->par_stimulation_factor[1].SetConfigEntry("LowerTrunk/phases/walking_lateral_phase_3/lock_hip_roll");
//  skill_walking_lateral_phase3_right->par_stimulation_factor[2].SetConfigEntry("LowerTrunk/phases/walking_lateral_phase_3/frontal_foot_placement");
//
//  mbbStimulationSkill* skill_walking_lateral_phase4_left = new mbbStimulationSkill(this, "(Ph) Walking Lateral Phase 4 Left", tStimulationMode::AUTO, 0, 5);
//  skill_walking_lateral_phase4_left->SetConfigNode("LowerTrunk/phases/walking_lateral_phase_2/");
//  skill_walking_lateral_phase4_left->par_stimulation_factor[0].SetConfigEntry("LowerTrunk/phases/walking_lateral_phase_4/lock_hip_roll");
//  skill_walking_lateral_phase4_left->par_stimulation_factor[1].SetConfigEntry("LowerTrunk/phases/walking_lateral_phase_4/frontal_foot_placement");
//  skill_walking_lateral_phase4_left->par_stimulation_factor[2].SetConfigEntry("LowerTrunk/phases/walking_lateral_phase_4/control_pelvis_roll");
//  skill_walking_lateral_phase4_left->par_stimulation_factor[3].SetConfigEntry("LowerTrunk/phases/walking_lateral_phase_4/shift_trunk");
//  skill_walking_lateral_phase4_left->par_stimulation_factor[4].SetConfigEntry("LowerTrunk/phases/walking_lateral_phase_4/lateral_hip_shift");
//
//  mbbStimulationSkill* skill_walking_lateral_phase4_right = new mbbStimulationSkill(this, "(Ph) Walking Lateral Phase 4 Right", tStimulationMode::AUTO, 0, 5);
//  skill_walking_lateral_phase4_right->SetConfigNode("LowerTrunk/phases/walking_lateral_phase_2/");
//  skill_walking_lateral_phase4_right->par_stimulation_factor[0].SetConfigEntry("LowerTrunk/phases/walking_lateral_phase_4/lock_hip_roll");
//  skill_walking_lateral_phase4_right->par_stimulation_factor[1].SetConfigEntry("LowerTrunk/phases/walking_lateral_phase_4/frontal_foot_placement");
//  skill_walking_lateral_phase4_right->par_stimulation_factor[2].SetConfigEntry("LowerTrunk/phases/walking_lateral_phase_4/control_pelvis_roll");
//  skill_walking_lateral_phase4_right->par_stimulation_factor[3].SetConfigEntry("LowerTrunk/phases/walking_lateral_phase_4/shift_trunk");
//  skill_walking_lateral_phase4_right->par_stimulation_factor[4].SetConfigEntry("LowerTrunk/phases/walking_lateral_phase_4/lateral_hip_shift");
//
//  //BEGIN reflexes
//  mbbLockHipRoll * lock_hip_roll_left = new mbbLockHipRoll(this, "(R) Lock Hip Roll Left", tStimulationMode::AUTO, 0, 5); // todo: add mbbLockHipRoll
//
//  mbbLockHipRoll * lock_hip_roll_right = new mbbLockHipRoll(this, "(R) Lock Hip Roll Right", tStimulationMode::AUTO, 0, 5);
//
//  // frontal foot placement
//  mbbFrontalFootPlacement* frontal_foot_placement_left = new mbbFrontalFootPlacement(this, "(PR) Fron. Foot Placem. Left",  tStimulationMode::AUTO, 0, 1);
//  mbbFrontalFootPlacement* frontal_foot_placement_right = new mbbFrontalFootPlacement(this, "(PR) Fron. Foot Placem. Right",  tStimulationMode::AUTO, 0, -1);
//
//  // control pelvis pitch
//  mbbControlPelvisPitch* control_pelvis_pitch_left = new mbbControlPelvisPitch(this, "(R) Control Pelvis Pitch Left",  tStimulationMode::AUTO, 0, 1);
//  mbbControlPelvisPitch* control_pelvis_pitch_right = new mbbControlPelvisPitch(this, "(R) Control Pelvis Pitch Right",  tStimulationMode::AUTO, 0, 1);
//
//  // control pelvis yaw
//  mbbControlPelvisYaw * control_pelvis_yaw_left = new mbbControlPelvisYaw(this,  "(R) Control Pelvis Yaw Left", tStimulationMode::AUTO, 0, 1);
//  mbbControlPelvisYaw * control_pelvis_yaw_right = new mbbControlPelvisYaw(this, "(R) Control Pelvis Yaw Right", tStimulationMode::AUTO, 0, 1);
//
//  // upright trunk roll left
//  mbbBalanceBodyRoll * upright_trunk_roll_left = new mbbBalanceBodyRoll(this, "(PR) Upright Trunk Roll Left", tStimulationMode::AUTO);
//  mbbBalanceBodyRoll * upright_trunk_roll_right = new mbbBalanceBodyRoll(this, "(PR) Upright Trunk Roll Right", tStimulationMode::AUTO);
//
//  // control pelvis roll
//  mbbControlPelvisRoll * control_pelvis_roll_left = new mbbControlPelvisRoll(this, "(PR) Control Pelvis Roll Left", tStimulationMode::AUTO, 0, 1);
//  mbbControlPelvisRoll * control_pelvis_roll_right = new mbbControlPelvisRoll(this, "(PR) Control Pelvis Roll Right", tStimulationMode::AUTO, 0, 1);
//
//  //END reflexes
//
//  //BEGIN motor patterns
//  // lateral hip shift
//  mbbLateralHipShift *lateral_hip_shift_left = new mbbLateralHipShift(this,  "(MP) Lat. Hip Shift Left", tStimulationMode::AUTO, 0, 600, 0.2, 0.5, mbbLateralHipShift::eSIDE_LEFT);
//  mbbLateralHipShift *lateral_hip_shift_right = new mbbLateralHipShift(this, "(MP) Lat. Hip Shift Right", tStimulationMode::AUTO, 0, 600, 0.6, 0.7, mbbLateralHipShift::eSIDE_RIGHT);
//
//  // active hip swing
//  mbbActiveHipSwing *active_hip_swing_left = new mbbActiveHipSwing(this, "(MP) Active Hip Swing Left", tStimulationMode::AUTO, 1, 1.0, 400, 0.05, 0.4);
//  active_hip_swing_left->number_of_inhibition_ports.Set(1);
//  active_hip_swing_left->CheckStaticParameters();
//  mbbActiveHipSwing *active_hip_swing_right = new mbbActiveHipSwing(this, "(MP) Active Hip Swing Right", tStimulationMode::AUTO, 1, -1.0, 400, 0.05, 0.4);
//  active_hip_swing_right->number_of_inhibition_ports.Set(1);
//  active_hip_swing_right->CheckStaticParameters();
//  //END motor patterns
//
//
//
//  //BEGIN fusion behaviours
//  // ---------------------------------------------------------------------------------------------
//
//  // spine x, y ,z
//
//  fusion_abs_torque_spine_x->activity.ConnectTo(this->co_stimulation_spine_x_torque);
//  fusion_abs_torque_spine_x->OutputPort(0).ConnectTo(this->co_spine_x_torque);
//
//  fusion_abs_torque_spine_y->activity.ConnectTo(this->co_stimulation_spine_y_torque);
//  fusion_abs_torque_spine_y->OutputPort(0).ConnectTo(this->co_spine_y_torque);
//
//  fusion_abs_torque_spine_z->activity.ConnectTo(this->co_stimulation_spine_z_torque);
//  fusion_abs_torque_spine_z->OutputPort(0).ConnectTo(this->co_spine_z_torque);
//
//  // inhibit position by torque
//  fusion_abs_torque_spine_x->activity.ConnectTo(fusion_abs_torque_spine_x->inhibition[0]);
//  fusion_abs_torque_spine_y->activity.ConnectTo(fusion_abs_torque_spine_y->inhibition[0]);
//  fusion_abs_torque_spine_z->activity.ConnectTo(fusion_abs_torque_spine_z->inhibition[0]);
//
//  // left hip
//  fusion_abs_angle_left_hip_x->activity.ConnectTo(this->co_left_stimulation_hip_x_angle);
//  fusion_abs_angle_left_hip_x->OutputPort(0).ConnectTo(this->co_left_hip_x_angle);
//
//  fusion_abs_angle_left_hip_y->activity.ConnectTo(this->co_left_stimulation_hip_y_angle);
//  fusion_abs_angle_left_hip_y->OutputPort(0).ConnectTo(this->co_left_hip_y_angle);
//
//  fusion_abs_angle_left_hip_z->activity.ConnectTo(this->co_left_stimulation_hip_z_angle);
//  fusion_abs_angle_left_hip_z->OutputPort(0).ConnectTo(this->co_left_hip_z_angle);
//
//  fusion_abs_torque_left_hip_x->activity.ConnectTo(this->co_left_stimulation_hip_x_torque);
//  fusion_abs_torque_left_hip_x->OutputPort(0).ConnectTo(this->co_left_hip_x_torque);
//
//  fusion_abs_torque_left_hip_y->activity.ConnectTo(this->co_left_stimulation_hip_y_torque);
//  fusion_abs_torque_left_hip_y->OutputPort(0).ConnectTo(this->co_left_hip_y_torque);
//
//  fusion_abs_torque_left_hip_z->activity.ConnectTo(this->co_left_stimulation_hip_z_torque);
//  fusion_abs_torque_left_hip_z->OutputPort(0).ConnectTo(this->co_left_hip_z_torque);
//
//  // inhibit position by torque
//  fusion_abs_angle_left_hip_x->activity.ConnectTo(fusion_abs_torque_left_hip_x->inhibition[0]);
//  fusion_abs_angle_left_hip_y->activity.ConnectTo(fusion_abs_torque_left_hip_y->inhibition[0]);
//  fusion_abs_angle_left_hip_z->activity.ConnectTo(fusion_abs_torque_left_hip_z->inhibition[0]);
//
//  // right hip
//  fusion_abs_angle_right_hip_x->activity.ConnectTo(this->co_right_stimulation_hip_x_angle);
//  fusion_abs_angle_right_hip_x->OutputPort(0).ConnectTo(this->co_right_hip_x_angle);
//
//  fusion_abs_angle_right_hip_y->activity.ConnectTo(this->co_right_stimulation_hip_y_angle);
//  fusion_abs_angle_right_hip_y->OutputPort(0).ConnectTo(this->co_right_hip_y_angle);
//
//  fusion_abs_angle_right_hip_z->activity.ConnectTo(this->co_right_stimulation_hip_z_angle);
//  fusion_abs_angle_right_hip_z->OutputPort(0).ConnectTo(this->co_right_hip_z_angle);
//
//  fusion_abs_torque_right_hip_x->activity.ConnectTo(this->co_right_stimulation_hip_x_torque);
//  fusion_abs_torque_right_hip_x->OutputPort(0).ConnectTo(this->co_right_hip_x_torque);
//
//  fusion_abs_torque_right_hip_y->activity.ConnectTo(this->co_right_stimulation_hip_y_torque);
//  fusion_abs_torque_right_hip_y->OutputPort(0).ConnectTo(this->co_right_hip_y_torque);
//
//  fusion_abs_torque_right_hip_z->activity.ConnectTo(this->co_right_stimulation_hip_z_torque);
//  fusion_abs_torque_right_hip_z->OutputPort(0).ConnectTo(this->co_right_hip_z_torque);
//
//  // inhibit position by torque
//  fusion_abs_angle_right_hip_x->activity.ConnectTo(fusion_abs_torque_right_hip_x->inhibition[0]);
//  fusion_abs_angle_right_hip_y->activity.ConnectTo(fusion_abs_torque_right_hip_y->inhibition[0]);
//  fusion_abs_angle_right_hip_z->activity.ConnectTo(fusion_abs_torque_right_hip_z->inhibition[0]);
//
//  // active hip lateral swing
//  fusion_active_hip_swing_left->activity.ConnectTo(active_hip_swing_left->stimulation);
//  fusion_active_hip_swing_right->activity.ConnectTo(active_hip_swing_right->stimulation);
//
//  // lock hip roll
//  fusion_lock_hip_roll_left->activity.ConnectTo(lock_hip_roll_left->stimulation);
//  fusion_lock_hip_roll_right->activity.ConnectTo(lock_hip_roll_right->stimulation);
//
//  //END fusion behaviors
//
//  //BEGIN reflexes
//  // ---------------------------------------------------------------------------------------------
//  // lock hip roll
//  // left
//  this->ci_walking_scale_laterally.ConnectTo(lock_hip_roll_left->ci_step_length);
//  this->ci_adjusted_roll.ConnectTo(lock_hip_roll_left->ci_body_roll);
//
//  lock_hip_roll_left->activity.ConnectTo(active_hip_swing_left->inhibition[0]);
//
//  this->si_left_hip_x_angle.ConnectTo(lock_hip_roll_left->si_hip_x_angle);
//
//  lock_hip_roll_left->co_stimulation_angle.ConnectTo(fusion_abs_angle_left_hip_x->InputActivity(0));
//  lock_hip_roll_left->target_rating.ConnectTo(fusion_abs_angle_left_hip_x->InputTargetRating(0));
//  lock_hip_roll_left->co_hip_x_angle.ConnectTo(fusion_abs_angle_left_hip_x->InputPort(0, 0));
//
//  lock_hip_roll_left->co_stimulation_torque.ConnectTo(fusion_abs_torque_left_hip_x->InputActivity(0));
//  lock_hip_roll_left->target_rating.ConnectTo(fusion_abs_torque_left_hip_x->InputTargetRating(0));
//  lock_hip_roll_left->co_hip_x_torque.ConnectTo(fusion_abs_torque_left_hip_x->InputPort(0, 0));
//
//  // right
//  this->ci_walking_scale_laterally.ConnectTo(lock_hip_roll_right->ci_step_length);
//  this->ci_adjusted_roll.ConnectTo(lock_hip_roll_right->ci_body_roll);
//
//  lock_hip_roll_right->activity.ConnectTo(active_hip_swing_right->inhibition[0]);
//
//  this->si_right_hip_x_angle.ConnectTo(lock_hip_roll_right->si_hip_x_angle);
//
//  lock_hip_roll_right->co_stimulation_angle.ConnectTo(fusion_abs_angle_right_hip_x->InputActivity(0));
//  lock_hip_roll_right->target_rating.ConnectTo(fusion_abs_angle_right_hip_x->InputTargetRating(0));
//  lock_hip_roll_right->co_hip_x_angle.ConnectTo(fusion_abs_angle_right_hip_x->InputPort(0, 0));
//
//  lock_hip_roll_right->co_stimulation_torque.ConnectTo(fusion_abs_torque_right_hip_x->InputActivity(0));
//  lock_hip_roll_right->target_rating.ConnectTo(fusion_abs_torque_right_hip_x->InputTargetRating(0));
//  lock_hip_roll_right->co_hip_x_torque.ConnectTo(fusion_abs_torque_right_hip_x->InputPort(0, 0));
//
//  // frontal foot placement
//  // left
//  fusion_frontal_foot_placement_left->activity.ConnectTo(frontal_foot_placement_left->stimulation);
//
//  this->ci_left_ground_contact.ConnectTo(frontal_foot_placement_left->ci_ground_contact);
//  this->ci_left_lateral_velocity_correction.ConnectTo(frontal_foot_placement_left->ci_com_error_x);
//  this->ci_left_com_x.ConnectTo(frontal_foot_placement_left->ci_com_x);
//  this->ci_left_xcom_x.ConnectTo(frontal_foot_placement_left->ci_xcom_x);
////  for (size_t i = 0; i < 3; i ++)
////  {
////    this->ci_actual_accel.emplace_back("Actual Accel" + std::to_string(i), this);
////    this->ci_actual_rot.emplace_back("Actual Rot." + std::to_string(i), this);
////    this->ci_actual_omega.emplace_back("Actual Omega" + std::to_string(i), this);
////  }
//  this->ci_actual_pitch.ConnectTo(frontal_foot_placement_left->ci_pelvis_pitch);
//
//  this->si_left_hip_y_angle.ConnectTo(frontal_foot_placement_left->si_hip_y_angle);
//  frontal_foot_placement_left->activity.ConnectTo(fusion_abs_angle_left_hip_y->InputActivity(0));
//  frontal_foot_placement_left->target_rating.ConnectTo(fusion_abs_angle_left_hip_y->InputTargetRating(0));
//  frontal_foot_placement_left->co_angle_hip_y.ConnectTo(fusion_abs_angle_left_hip_y->InputPort(0, 0));
//
//  // right
//  fusion_frontal_foot_placement_right->activity.ConnectTo(frontal_foot_placement_right->stimulation);
//
//  this->ci_right_ground_contact.ConnectTo(frontal_foot_placement_right->ci_ground_contact);
//  this->ci_right_lateral_velocity_correction.ConnectTo(frontal_foot_placement_right->ci_com_error_x);
//  this->ci_right_com_x.ConnectTo(frontal_foot_placement_right->ci_com_x);
//  this->ci_right_xcom_x.ConnectTo(frontal_foot_placement_right->ci_xcom_x);
//  this->ci_actual_pitch.ConnectTo(frontal_foot_placement_right->ci_pelvis_pitch);
//
//  this->si_right_hip_y_angle.ConnectTo(frontal_foot_placement_right->si_hip_y_angle);
//  frontal_foot_placement_right->activity.ConnectTo(fusion_abs_angle_right_hip_y->InputActivity(0));
//  frontal_foot_placement_right->target_rating.ConnectTo(fusion_abs_angle_right_hip_y->InputTargetRating(0));
//  frontal_foot_placement_right->co_angle_hip_y.ConnectTo(fusion_abs_angle_right_hip_y->InputPort(0, 0));
//
//  // control pelvis pitch
//  // left
//  this->ci_left_ground_contact.ConnectTo(control_pelvis_pitch_left->ci_ground_contact);
//  this->ci_actual_pitch.ConnectTo(control_pelvis_pitch_left->ci_pelvis_pitch);
//
//  this->si_left_hip_y_angle.ConnectTo(control_pelvis_pitch_left->si_hip_y_angle);
//
//  control_pelvis_pitch_left->activity.ConnectTo(fusion_abs_angle_left_hip_y->InputActivity(1));
//  control_pelvis_pitch_left->target_rating.ConnectTo(fusion_abs_angle_left_hip_y->InputTargetRating(1));
//  control_pelvis_pitch_left->co_hip_y_angle.ConnectTo(fusion_abs_angle_left_hip_y->InputPort(1, 0));
//
//  // right
//  this->ci_right_ground_contact.ConnectTo(control_pelvis_pitch_right->ci_ground_contact);
//  this->ci_actual_pitch.ConnectTo(control_pelvis_pitch_right->ci_pelvis_pitch);
//
//  this->si_right_hip_y_angle.ConnectTo(control_pelvis_pitch_right->si_hip_y_angle);
//
//  control_pelvis_pitch_right->activity.ConnectTo(fusion_abs_angle_right_hip_y->InputActivity(1));
//  control_pelvis_pitch_right->target_rating.ConnectTo(fusion_abs_angle_right_hip_y->InputTargetRating(1));
//  control_pelvis_pitch_right->co_hip_y_angle.ConnectTo(fusion_abs_angle_right_hip_y->InputPort(1, 0));
//
//  // control pelvis yaw
//  // left
//  this->ci_left_ground_contact.ConnectTo(control_pelvis_yaw_left->ci_ground_contact);
//  this->ci_actual_yaw.ConnectTo(control_pelvis_yaw_left->ci_pelvis_yaw);
//
//  this->si_left_hip_z_angle.ConnectTo(control_pelvis_yaw_left->si_hip_z_angle);
//
//  control_pelvis_yaw_left->activity.ConnectTo(fusion_abs_angle_left_hip_z->InputActivity(0));
//  control_pelvis_yaw_left->target_rating.ConnectTo(fusion_abs_angle_left_hip_z->InputTargetRating(0));
//  control_pelvis_yaw_left->co_hip_z_angle.ConnectTo(fusion_abs_angle_left_hip_z->InputPort(0, 0));
//
//  // right
//  this->ci_right_ground_contact.ConnectTo(control_pelvis_yaw_right->ci_ground_contact);
//  this->ci_actual_yaw.ConnectTo(control_pelvis_yaw_right->ci_pelvis_yaw);
//
//  this->si_right_hip_z_angle.ConnectTo(control_pelvis_yaw_right->si_hip_z_angle);
//
//  control_pelvis_yaw_right->activity.ConnectTo(fusion_abs_angle_right_hip_z->InputActivity(0));
//  control_pelvis_yaw_right->target_rating.ConnectTo(fusion_abs_angle_right_hip_z->InputTargetRating(0));
//  control_pelvis_yaw_right->co_hip_z_angle.ConnectTo(fusion_abs_angle_right_hip_z->InputPort(0, 0));
//
//  // upright trunk roll
//  // left
//  this->ci_left_ground_contact.ConnectTo(upright_trunk_roll_left->ci_ground_contact);
//  this->ci_actual_roll.ConnectTo(upright_trunk_roll_left->ci_body_roll);
//
//  upright_trunk_roll_left->activity.ConnectTo(fusion_abs_torque_left_hip_x->InputActivity(1));
//  upright_trunk_roll_left->target_rating.ConnectTo(fusion_abs_torque_left_hip_x->InputTargetRating(1));
//  upright_trunk_roll_left->co_torque_hip_x.ConnectTo(fusion_abs_torque_left_hip_x->InputPort(1, 0));
//
//  // right
//  this->ci_right_ground_contact.ConnectTo(upright_trunk_roll_right->ci_ground_contact);
//  this->ci_actual_roll.ConnectTo(upright_trunk_roll_right->ci_body_roll);
//
//  upright_trunk_roll_right->activity.ConnectTo(fusion_abs_torque_right_hip_x->InputActivity(1));
//  upright_trunk_roll_right->target_rating.ConnectTo(fusion_abs_torque_right_hip_x->InputTargetRating(1));
//  upright_trunk_roll_right->co_torque_hip_x.ConnectTo(fusion_abs_torque_right_hip_x->InputPort(1, 0));
//
//  // control pelvis roll
//  // left
//  fusion_control_pelvis_roll_left->activity.ConnectTo(control_pelvis_roll_left->stimulation);
//
//  this->si_left_hip_x_angle.ConnectTo(control_pelvis_roll_left->si_hip_x_angle);
//
//  this->ci_left_ground_contact.ConnectTo(control_pelvis_roll_left->ci_ground_contact);
//  this->ci_actual_roll.ConnectTo(control_pelvis_roll_left->ci_pelvis_roll);
//
//  control_pelvis_roll_left->activity.ConnectTo(fusion_abs_angle_left_hip_x->InputActivity(1));
//  control_pelvis_roll_left->target_rating.ConnectTo(fusion_abs_angle_left_hip_x->InputTargetRating(1));
//  control_pelvis_roll_left->co_hip_x_angle.ConnectTo(fusion_abs_angle_left_hip_x->InputPort(1, 0));
//
//  // right
//  fusion_control_pelvis_roll_right->activity.ConnectTo(control_pelvis_roll_right->stimulation);
//
//  this->si_right_hip_x_angle.ConnectTo(control_pelvis_roll_right->si_hip_x_angle);
//
//  this->ci_right_ground_contact.ConnectTo(control_pelvis_roll_right->ci_ground_contact);
//  this->ci_actual_roll.ConnectTo(control_pelvis_roll_right->ci_pelvis_roll);
//
//  control_pelvis_roll_right->activity.ConnectTo(fusion_abs_angle_right_hip_x->InputActivity(1));
//  control_pelvis_roll_right->target_rating.ConnectTo(fusion_abs_angle_right_hip_x->InputTargetRating(1));
//  control_pelvis_roll_right->co_hip_x_angle.ConnectTo(fusion_abs_angle_right_hip_x->InputPort(1, 0));
//
//  //END reflexes
//  // ---------------------------------------------------------------------------------------------
//
//
//  //BEGIN motor patterns
//  // ---------------------------------------------------------------------------------------------
//
//  // active hip lateral swing
//  // left
//
//  this->ci_walking_scale_laterally.ConnectTo(active_hip_swing_left->ci_lateral_scale);
//
//  active_hip_swing_left->activity.ConnectTo(fusion_abs_torque_left_hip_x->InputActivity(2));
//  active_hip_swing_left->target_rating.ConnectTo(fusion_abs_torque_left_hip_x->InputTargetRating(2));
//  active_hip_swing_left->co_torque_hip_x.ConnectTo(fusion_abs_torque_left_hip_x->InputPort(2, 0));
//
//  active_hip_swing_left->activity.ConnectTo(fusion_abs_torque_right_hip_x->InputActivity(2));
//  active_hip_swing_left->target_rating.ConnectTo(fusion_abs_torque_right_hip_x->InputTargetRating(2));
//  active_hip_swing_left->co_torque_opposite_hip_x.ConnectTo(fusion_abs_torque_right_hip_x->InputPort(2, 0));
//
//  // right
//
//  this->ci_walking_scale_laterally.ConnectTo(active_hip_swing_right->ci_lateral_scale);
//
//  active_hip_swing_right->activity.ConnectTo(fusion_abs_torque_right_hip_x->InputActivity(3));
//  active_hip_swing_right->target_rating.ConnectTo(fusion_abs_torque_right_hip_x->InputTargetRating(3));
//  active_hip_swing_right->co_torque_hip_x.ConnectTo(fusion_abs_torque_right_hip_x->InputPort(3, 0));
//
//  active_hip_swing_right->activity.ConnectTo(fusion_abs_torque_left_hip_x->InputActivity(3));
//  active_hip_swing_right->target_rating.ConnectTo(fusion_abs_torque_left_hip_x->InputTargetRating(3));
//  active_hip_swing_right->co_torque_opposite_hip_x.ConnectTo(fusion_abs_torque_left_hip_x->InputPort(3, 0));
//
//  // lateral hip shift
//  // left
//  this->ci_walking_scale_laterally.ConnectTo(lateral_hip_shift_left->ci_scale);
//  this->ci_actual_roll.ConnectTo(lateral_hip_shift_left->ci_ins_roll);
//  this->ci_com_y.ConnectTo(lateral_hip_shift_left->ci_com_y);
//
//  this->si_left_hip_x_angle.ConnectTo(lateral_hip_shift_left->si_hip_angle);
//  this->si_right_hip_x_angle.ConnectTo(lateral_hip_shift_left->si_hip_angle_opposite);
//
//  fusion_lateral_hip_shift_left->activity.ConnectTo(lateral_hip_shift_left->stimulation);
//
//  lateral_hip_shift_left->activity.ConnectTo(fusion_abs_torque_left_hip_x->InputActivity(4));
//  lateral_hip_shift_left->target_rating.ConnectTo(fusion_abs_torque_left_hip_x->InputTargetRating(4));
//  lateral_hip_shift_left->co_left_torque_hip_x.ConnectTo(fusion_abs_torque_left_hip_x->InputPort(4, 0));
//
//  lateral_hip_shift_left->activity.ConnectTo(fusion_abs_torque_right_hip_x->InputActivity(4));
//  lateral_hip_shift_left->target_rating.ConnectTo(fusion_abs_torque_right_hip_x->InputTargetRating(4));
//  lateral_hip_shift_left->co_right_torque_hip_x.ConnectTo(fusion_abs_torque_right_hip_x->InputPort(4, 0));
//
//  lateral_hip_shift_left->activity.ConnectTo(fusion_abs_torque_spine_x->InputActivity(0));
//  lateral_hip_shift_left->target_rating.ConnectTo(fusion_abs_torque_spine_x->InputTargetRating(0));
//  lateral_hip_shift_left->co_torque_spine_x.ConnectTo(fusion_abs_torque_spine_x->InputPort(0, 0));
//
//  // right
//  this->ci_walking_scale_laterally.ConnectTo(lateral_hip_shift_right->ci_scale);
//  this->ci_walking_lateral_activity.ConnectTo(lateral_hip_shift_right->ci_walking_lateral_activity);
//  this->ci_actual_roll.ConnectTo(lateral_hip_shift_right->ci_ins_roll);
//  this->ci_com_y.ConnectTo(lateral_hip_shift_right->ci_com_y);
//
//  this->si_right_hip_x_angle.ConnectTo(lateral_hip_shift_right->si_hip_angle);
//  this->si_left_hip_x_angle.ConnectTo(lateral_hip_shift_left->si_hip_angle_opposite);
//
//  fusion_lateral_hip_shift_right->activity.ConnectTo(lateral_hip_shift_right->stimulation);
//
//  lateral_hip_shift_right->activity.ConnectTo(fusion_abs_torque_right_hip_x->InputActivity(5));
//  lateral_hip_shift_right->target_rating.ConnectTo(fusion_abs_torque_right_hip_x->InputTargetRating(5));
//  lateral_hip_shift_right->co_right_torque_hip_x.ConnectTo(fusion_abs_torque_right_hip_x->InputPort(5, 0));
//
//  lateral_hip_shift_right->activity.ConnectTo(fusion_abs_torque_left_hip_x->InputActivity(5));
//  lateral_hip_shift_right->target_rating.ConnectTo(fusion_abs_torque_left_hip_x->InputTargetRating(5));
//  lateral_hip_shift_right->co_left_torque_hip_x.ConnectTo(fusion_abs_torque_left_hip_x->InputPort(5, 0));
//
//  lateral_hip_shift_right->activity.ConnectTo(fusion_abs_torque_spine_x->InputActivity(1));
//  lateral_hip_shift_right->target_rating.ConnectTo(fusion_abs_torque_spine_x->InputTargetRating(1));
//  lateral_hip_shift_right->co_torque_spine_x.ConnectTo(fusion_abs_torque_spine_x->InputPort(1, 0));
//
//  //BEGIN skills
//  // ---------------------------------------------------------------------------------------------
//
//  // walking lateral all phases
//  this->ci_walking_lateral_activity.ConnectTo(skill_walking_lateral->stimulation);
//
//  skill_walking_lateral->co_target_activity[0].ConnectTo(fusion_abs_angle_left_hip_y->InputActivity(2));
//  skill_walking_lateral->target_rating.ConnectTo(fusion_abs_angle_left_hip_y->InputTargetRating(2));
//  skill_walking_lateral->co_target_output[0].ConnectTo(fusion_abs_angle_left_hip_y->InputPort(2, 0));
//
//  skill_walking_lateral->co_target_activity[0].ConnectTo(fusion_abs_angle_right_hip_y->InputActivity(2));
//  skill_walking_lateral->target_rating.ConnectTo(fusion_abs_angle_right_hip_y->InputTargetRating(2));
//  skill_walking_lateral->co_target_output[0].ConnectTo(fusion_abs_angle_right_hip_y->InputPort(2, 0));
//
//  skill_walking_lateral->co_target_activity[2].ConnectTo(fusion_abs_angle_left_hip_z->InputActivity(1));
//  skill_walking_lateral->target_rating.ConnectTo(fusion_abs_angle_left_hip_z->InputTargetRating(1));
//  skill_walking_lateral->co_target_output[2].ConnectTo(fusion_abs_angle_left_hip_z->InputPort(1, 0));
//
//  skill_walking_lateral->co_target_activity[2].ConnectTo(fusion_abs_angle_right_hip_z->InputActivity(1));
//  skill_walking_lateral->target_rating.ConnectTo(fusion_abs_angle_right_hip_z->InputTargetRating(1));
//  skill_walking_lateral->co_target_output[2].ConnectTo(fusion_abs_angle_right_hip_z->InputPort(1, 0));
//
//  skill_walking_lateral->co_target_activity[3].ConnectTo(fusion_abs_angle_spine_y->InputActivity(0));
//  skill_walking_lateral->target_rating.ConnectTo(fusion_abs_angle_spine_y->InputTargetRating(0));
//  skill_walking_lateral->co_target_output[3].ConnectTo(fusion_abs_angle_spine_y->InputPort(0, 0));
//
//  skill_walking_lateral->co_target_activity[4].ConnectTo(upright_trunk_roll_left->stimulation);
//
//  skill_walking_lateral->co_target_activity[5].ConnectTo(upright_trunk_roll_right->stimulation);
//
//  // walking lateral phase 1
//  // left
//  this->ci_left_stimulation_walking_lateral_phase_1.ConnectTo(skill_walking_lateral_phase1_left->stimulation);
//
//  skill_walking_lateral_phase1_left->co_target_activity[0].ConnectTo(fusion_control_pelvis_pitch_left->InputActivity(0));
//  skill_walking_lateral_phase1_left->target_rating.ConnectTo(fusion_control_pelvis_pitch_left->InputTargetRating(0));
//
//  control_pelvis_pitch_left->target_rating.ConnectTo(skill_walking_lateral_phase1_left->si_target_rating[0]);
//
//  skill_walking_lateral_phase1_left->co_target_activity[2].ConnectTo(fusion_abs_angle_left_hip_x->InputActivity(2));
//  skill_walking_lateral_phase1_left->target_rating.ConnectTo(fusion_abs_angle_left_hip_x->InputTargetRating(2));
//  skill_walking_lateral_phase1_left->co_target_output[2].ConnectTo(fusion_abs_angle_left_hip_x->InputPort(2, 0));
//
//  skill_walking_lateral_phase1_left->co_target_activity[3].ConnectTo(fusion_lateral_hip_shift_left->InputActivity(0));
//  skill_walking_lateral_phase1_left->target_rating.ConnectTo(fusion_lateral_hip_shift_left->InputTargetRating(0));
//
//  // right
//  this->ci_right_stimulation_walking_lateral_phase_1.ConnectTo(skill_walking_lateral_phase1_right->stimulation);
//
//  skill_walking_lateral_phase1_right->co_target_activity[0].ConnectTo(fusion_control_pelvis_pitch_right->InputActivity(0));
//  skill_walking_lateral_phase1_right->target_rating.ConnectTo(fusion_control_pelvis_pitch_right->InputTargetRating(0));
//
//  control_pelvis_pitch_right->target_rating.ConnectTo(skill_walking_lateral_phase1_right->si_target_rating[0]);
//
//  skill_walking_lateral_phase1_right->co_target_activity[2].ConnectTo(fusion_abs_angle_right_hip_x->InputActivity(2));
//  skill_walking_lateral_phase1_right->target_rating.ConnectTo(fusion_abs_angle_right_hip_x->InputTargetRating(2));
//  skill_walking_lateral_phase1_right->co_target_output[2].ConnectTo(fusion_abs_angle_right_hip_x->InputPort(2, 0));
//
//  // walking lateral phase 2
//  // left
//  this->ci_left_stimulation_walking_lateral_phase_2.ConnectTo(skill_walking_lateral_phase2_left->stimulation);
//
//  skill_walking_lateral_phase2_left->co_target_activity[0].ConnectTo(fusion_control_pelvis_pitch_left->InputActivity(1));
//  skill_walking_lateral_phase2_left->target_rating.ConnectTo(fusion_control_pelvis_pitch_left->InputTargetRating(1));
//
//  control_pelvis_pitch_left->target_rating.ConnectTo(skill_walking_lateral_phase2_left->si_target_rating[0]);
//
//  skill_walking_lateral_phase2_left->co_target_activity[1].ConnectTo(fusion_control_pelvis_yaw_left->InputActivity(0));
//  skill_walking_lateral_phase2_left->target_rating.ConnectTo(fusion_control_pelvis_yaw_left->InputTargetRating(0));
//
//  control_pelvis_yaw_left->target_rating.ConnectTo(skill_walking_lateral_phase2_left->si_target_rating[1]);
//
//  lateral_hip_shift_left->target_rating.ConnectTo(skill_walking_lateral_phase2_left->si_target_rating[2]);
//
//  // right
//  this->ci_right_stimulation_walking_lateral_phase_2.ConnectTo(skill_walking_lateral_phase2_right->stimulation);
//
//  skill_walking_lateral_phase2_right->co_target_activity[0].ConnectTo(fusion_control_pelvis_pitch_right->InputActivity(1));
//  skill_walking_lateral_phase2_right->target_rating.ConnectTo(fusion_control_pelvis_pitch_right->InputTargetRating(1));
//
//  control_pelvis_pitch_right->target_rating.ConnectTo(skill_walking_lateral_phase2_right->si_target_rating[0]);
//
//  skill_walking_lateral_phase2_right->co_target_activity[1].ConnectTo(fusion_control_pelvis_yaw_right->InputActivity(0));
//  skill_walking_lateral_phase2_right->target_rating.ConnectTo(fusion_control_pelvis_yaw_right->InputTargetRating(0));
//
//  control_pelvis_yaw_right->target_rating.ConnectTo(skill_walking_lateral_phase2_right->si_target_rating[1]);
//
//  skill_walking_lateral_phase2_right->co_target_activity[2].ConnectTo(fusion_lateral_hip_shift_right->InputActivity(0));
//  skill_walking_lateral_phase2_right->target_rating.ConnectTo(fusion_lateral_hip_shift_right->InputTargetRating(0));
//
//  lateral_hip_shift_right->target_rating.ConnectTo(skill_walking_lateral_phase2_right->si_target_rating[2]);
//
//  // walking lateral phase 3
//  // left
//  this->ci_left_stimulation_walking_lateral_phase_3.ConnectTo(skill_walking_lateral_phase3_left->stimulation);
//
//  skill_walking_lateral_phase3_left->co_target_activity[1].ConnectTo(fusion_lock_hip_roll_left->InputActivity(0));
//  skill_walking_lateral_phase3_left->target_rating.ConnectTo(fusion_lock_hip_roll_left->InputTargetRating(0));
//
//  skill_walking_lateral_phase3_left->co_target_activity[0].ConnectTo(fusion_active_hip_swing_left->InputActivity(0));
//  skill_walking_lateral_phase3_left->target_rating.ConnectTo(fusion_active_hip_swing_left->InputTargetRating(0));
//
//  active_hip_swing_left->target_rating.ConnectTo(skill_walking_lateral_phase3_left->si_target_rating[0]);
//
//  skill_walking_lateral_phase3_left->co_target_activity[2].ConnectTo(fusion_frontal_foot_placement_left->InputActivity(0));
//  skill_walking_lateral_phase3_left->target_rating.ConnectTo(fusion_frontal_foot_placement_left->InputTargetRating(0));
//
//  frontal_foot_placement_left->target_rating.ConnectTo(skill_walking_lateral_phase3_left->si_target_rating[1]);
//
//  // right
//  this->ci_right_stimulation_walking_lateral_phase_3.ConnectTo(skill_walking_lateral_phase3_right->stimulation);
//
//  skill_walking_lateral_phase3_right->co_target_activity[1].ConnectTo(fusion_lock_hip_roll_right->InputActivity(0));
//  skill_walking_lateral_phase3_right->target_rating.ConnectTo(fusion_lock_hip_roll_right->InputTargetRating(0));
//
//  skill_walking_lateral_phase3_right->co_target_activity[0].ConnectTo(fusion_active_hip_swing_right->InputActivity(0));
//  skill_walking_lateral_phase3_right->target_rating.ConnectTo(fusion_active_hip_swing_right->InputTargetRating(0));
//
//  active_hip_swing_right->target_rating.ConnectTo(skill_walking_lateral_phase3_right->si_target_rating[0]);
//
//  skill_walking_lateral_phase3_right->co_target_activity[2].ConnectTo(fusion_frontal_foot_placement_right->InputActivity(0));
//  skill_walking_lateral_phase3_right->target_rating.ConnectTo(fusion_frontal_foot_placement_right->InputTargetRating(0));
//
//  frontal_foot_placement_right->target_rating.ConnectTo(skill_walking_lateral_phase3_right->si_target_rating[1]);
//
//  // walking lateral phase 4
//  // left
//  this->ci_left_stimulation_walking_lateral_phase_4.ConnectTo(skill_walking_lateral_phase4_left->stimulation);
//
//  skill_walking_lateral_phase4_left->co_target_activity[0].ConnectTo(fusion_lock_hip_roll_left->InputActivity(1));
//  skill_walking_lateral_phase4_left->target_rating.ConnectTo(fusion_lock_hip_roll_left->InputTargetRating(1));
//
//  lock_hip_roll_left->target_rating.ConnectTo(skill_walking_lateral_phase4_left->si_target_rating[0]);
//
//  skill_walking_lateral_phase4_left->co_target_activity[2].ConnectTo(fusion_control_pelvis_roll_left->InputActivity(0));
//  skill_walking_lateral_phase4_left->target_rating.ConnectTo(fusion_control_pelvis_roll_left->InputTargetRating(0));
//
//  control_pelvis_roll_left->target_rating.ConnectTo(skill_walking_lateral_phase4_left->si_target_rating[0]);
//
//  skill_walking_lateral_phase4_left->co_target_activity[1].ConnectTo(fusion_frontal_foot_placement_left->InputActivity(1));
//  skill_walking_lateral_phase4_left->target_rating.ConnectTo(fusion_frontal_foot_placement_left->InputTargetRating(1));
//
//  frontal_foot_placement_left->target_rating.ConnectTo(skill_walking_lateral_phase4_left->si_target_rating[1]);
//
//  skill_walking_lateral_phase4_left->co_target_activity[3].ConnectTo(fusion_lateral_hip_shift_left->InputActivity(1));
//  skill_walking_lateral_phase4_left->target_rating.ConnectTo(fusion_lateral_hip_shift_left->InputTargetRating(1));
//
//  // right
//  this->ci_right_stimulation_walking_lateral_phase_4.ConnectTo(skill_walking_lateral_phase4_right->stimulation);
//
//  skill_walking_lateral_phase4_right->co_target_activity[0].ConnectTo(fusion_lock_hip_roll_right->InputActivity(1));
//  skill_walking_lateral_phase4_right->target_rating.ConnectTo(fusion_lock_hip_roll_right->InputTargetRating(1));
//
//  lock_hip_roll_right->target_rating.ConnectTo(skill_walking_lateral_phase4_right->si_target_rating[0]);
//
//  skill_walking_lateral_phase4_right->co_target_activity[2].ConnectTo(fusion_control_pelvis_roll_right->InputActivity(0));
//  skill_walking_lateral_phase4_right->target_rating.ConnectTo(fusion_control_pelvis_roll_right->InputTargetRating(0));
//
//  control_pelvis_roll_right->target_rating.ConnectTo(skill_walking_lateral_phase4_right->si_target_rating[0]);
//
//  skill_walking_lateral_phase4_right->co_target_activity[1].ConnectTo(fusion_frontal_foot_placement_right->InputActivity(1));
//  skill_walking_lateral_phase4_right->target_rating.ConnectTo(fusion_frontal_foot_placement_right->InputTargetRating(1));

//        frontal_foot_placement_right->target_rating.ConnectTo(skill_walking_lateral_phase4_right->si_target_rating[1]);

//        skill_walking_lateral_phase4_right->co_target_activity[3].ConnectTo(fusion_lateral_hip_shift_right->InputActivity(1));
//        skill_walking_lateral_phase4_right->target_rating.ConnectTo(fusion_lateral_hip_shift_right->InputTargetRating(1));


}

//----------------------------------------------------------------------
// gLowerTrunkLateralWalking destructor
//----------------------------------------------------------------------
gLowerTrunkLateralWalking::~gLowerTrunkLateralWalking()
{}

//----------------------------------------------------------------------
// End of namespace declaration
//----------------------------------------------------------------------
}
}
}
