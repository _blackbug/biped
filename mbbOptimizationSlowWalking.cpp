//
// You received this file as part of Finroc
// A framework for intelligent robot control
//
// Copyright (C) AG Robotersysteme TU Kaiserslautern
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//
//----------------------------------------------------------------------
/*!\file    projects/biped/learning_module/mbbOptimizationSlowWalking.cpp
 *
 * \author  KARTIKEYA KARNATAK 
 *
 * \date    2016-05-31
 *
 */
//----------------------------------------------------------------------
#include "projects/biped/learning_module/mbbOptimizationSlowWalking.h"
#include "rrlib/math/utilities.h"
#include "rrlib/time/time.h"
#include "projects/biped/learning_module/tPSOOptimization.h"
#include <math.h>
#include <fstream>
#include <vector>
#include <iostream>
#include <iterator>
#include <random>
#include <chrono>
#include <ctime>
#include <cstdlib>
#include <stdio.h>
//----------------------------------------------------------------------
// External includes (system with <>, local with "")
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Internal includes with ""
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Debugging
//----------------------------------------------------------------------
#include <cassert>

//----------------------------------------------------------------------
// Namespace usage
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Namespace declaration
//----------------------------------------------------------------------
namespace finroc
{
namespace biped
{
namespace learning_module
{

//----------------------------------------------------------------------
// Forward declarations / typedefs / enums
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Const values
//----------------------------------------------------------------------
runtime_construction::tStandardCreateModuleAction<mbbOptimizationSlowWalking> cCREATE_ACTION_FOR_MBB_OPTIMIZATIONSLOWWALKING("OptimizationSlowWalking");

//----------------------------------------------------------------------
// Implementation
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// mbbOptimizationSlowWalking constructor
//----------------------------------------------------------------------
mbbOptimizationSlowWalking::mbbOptimizationSlowWalking( core::tFrameworkElement *parent, const std::string &name,
	    ib2c::tStimulationMode stimulation_mode, unsigned int number_of_inhibition_ports) :
								tModule(parent, name, stimulation_mode, number_of_inhibition_ports, false),
								particle_position(0),
								pre_particle_position(0),
								particle_velocity(0),
								pbest_position(0),
								position_limit(0),

								gbest_position(0),
								cost(0),
								cost_pbest(0),
								velocity_limit(0),

								reset(false),
								new_start_flag(false),
								optimization_flag(false),
								update_flag(false),
								slow_walking_stimulation(false),
								fast_walking_stimulation(false),
								initialized(false),

								particle(0),
								walking_step(0),
								slow_walking_step(0),
								walking_distance(0.0),
								walking_velocity(0.0),
								step_length(0.0),

								cost_gbest(-10.0),
								cost_function1(0.0),
								cost_function2(0.0),
								cost_function3(0.0),
								cost_function4(0.0),
								cost_function5(0.0),

								time_span(0.0),

								// parameters
								walking_simulation(0.0),
								num_particle(10),
								num_dimension(18),

								cost_function1_par(0.0),
								cost_function2_par(0.0),
								cost_function3_par(0.0),
								cost_function4_par(0.0),
								cost_function5_par(0.0),

								optimization_on(false),
								slow_walking_on(false),
								fast_walking_on(false),

								// inputs
								loop_times(0.0),
								left_hip_angle(0.0),
								right_hip_angle(0.0),
								knee_angle(0.0),
								knee_velocity(0.0),

								// outputs
								new_running(0.0),
								lh_par1_output(0.0),
								lh_par2_output(0.0),
								ahs_max_torque_output(0.0),
								ahs_t1_output(0.0),
								ahs_t2_output(0.0),
								lp_t1_output(0.0),
								lp_t2_output(0.0),
								lp_max_torque_output(0.0),
								lock_knee_angle_output(0.0),
								lk_knee_angle_threshold_low(0.0),
								lk_knee_angle_threshold_high(0.0),
								lk_knee_angle_velocity(0.0),
								lk_hip_angle_threshold_low(0.0),
								lk_hip_angle_threshold_high(0.0),
								max_impulse(700.0),
								stimulation_knee_angle(1.0),
								last_recorded_velocity(0.0),
								totalVelocityDifference(0.0),
								totalStepLengthDifference(0.0),
								orig_lh_par1(1),
								orig_lh_par2(1),
								orig_ahs_max_torque(0.73),
								orig_ahs_t1(0.2),
								orig_ahs_t2(0.5),
								orig_lp_max_torque(0.4),
								orig_lp_t1(0.3),
								orig_lp_t2(0.5),
								orig_lock_knee_angle(0.60),
								orig_lk_knee_angle_threshold_low(0.3),
								orig_lk_knee_angle_threshold_high(0.55),
								orig_lk_knee_angle_velocity(0.025),
								orig_lk_hip_angle_threshold_low(0.0),
								orig_lk_hip_angle_threshold_high(0.3),
								orig_lk_increment_par1(0.8),
								orig_lk_increment_par2(0.2),
								orig_max_impulse(700),
								phase2_left_stimulation(0.0),
								phase2_right_stimulation(0.0)
{
	this->optimization = new tPSOOptimization();
	this->par_num_particle.Set(10);
	this->par_num_dimension.Set(18);
	this->par_velocity_parameter.Set(1.0);
	this->par_acceleration_constant1.Set(2.0);
	this->par_acceleration_constant2.Set(2.0);
	this->par_cost_function1.Set(10.0);
	this->par_cost_function2.Set(10.0);
	this->par_cost_function3.Set(10.0);
	this->par_cost_function4.Set(10.0);
	this->par_cost_function5.Set(10.0);

	// Parameters for the fast walking
	this->par_ahs_max_torque_out1.Set(0.910776);
	this->par_ahs_t1_out2.Set(0.223865);
	this->par_ahs_t2_out3.Set(0.66111);
	this->par_lh_par1_out4.Set(0.944565);
	this->par_lh_par2_out5.Set(0.932072);
	this->par_lp_torque_out6.Set(0.562434);
	this->par_lp_t1_out7.Set(0.218472);
	this->par_lp_t2_out8.Set(0.828654);
	this->par_lk_angle_out9.Set(0.04);
	this->par_lk_low_threshold_out10.Set(0.36);
	this->par_lk_high_threshold_out11.Set(0.55);
	this->par_lk_knee_velocity_out12.Set(0.025581);
	this->par_lk_hip_angle_low_out13.Set(0);
	this->par_lk_hip_angle_high_out14.Set(0.4);
	this->par_lk_increment_par1_out15.Set(1.5);
	this->par_lk_increment_par2_out16.Set(1.05936);
	this->par_max_impulse_out17.Set(468.898);
	this->par_stimulation_knee_angle_out18.Set(0.648263);
	this->par_stimulation_knee_angle_activity_out19.Set(1);

   	// Parameters for the slow walking
    this->par_ahs_max_torque_out1.Set(0.5);
   	this->par_ahs_t1_out2.Set(0.2);
   	this->par_ahs_t2_out3.Set(0.6);
   	this->par_lh_par1_out4.Set(0.3);
    this->par_lh_par2_out5.Set(0.9);
   	this->par_lp_torque_out6.Set(0.4);
   	this->par_lp_t1_out7.Set(0.25);
   	this->par_lp_t2_out8.Set(0.7);
   	this->par_lk_angle_out9.Set(0.63);
   	this->par_lk_low_threshold_out10.Set(0.3);
   	this->par_lk_high_threshold_out11.Set(0.5);
   	this->par_lk_knee_velocity_out12.Set(0.025);
   	this->par_lk_hip_angle_low_out13.Set(0);
   	this->par_lk_hip_angle_high_out14.Set(0.45);
   	this->par_lk_increment_par1_out15.Set(2);
   	this->par_lk_increment_par2_out16.Set(1.38);
   	this->par_max_impulse_out17.Set(490);
   	this->par_stimulation_knee_angle_out18.Set(1);
   	this->par_stimulation_knee_angle_activity_out19.Set(1);
   	this->par_optimized_out11.Set(0.55);

	this->par_optimization_on.Set(false);
	this->par_slow_walking_on.Set(false);
	this->par_fast_walking_on.Set(false);

	// Initialize the array
	this->particle_position = new double*[this->par_num_particle.Get()];
	for( int i = 0; i < this->par_num_particle.Get(); i++ )
	{
		this->particle_position[i] = new double[this->num_dimension];
	}

	this->pre_particle_position = new double*[this->par_num_particle.Get()];
	for( int i = 0; i < this->par_num_particle.Get(); i++ )
	{
		this->pre_particle_position[i] = new double[this->num_dimension];
	}

	this->particle_velocity = new double*[this->par_num_particle.Get()];
	for( int i = 0; i < this->par_num_particle.Get(); i++ )
	{
		this->particle_velocity[i] = new double[this->num_dimension];
	}

	this->pbest_position = new double*[this->par_num_particle.Get()];
	for( int i = 0; i < this->par_num_particle.Get(); i++ )
	{
		this->pbest_position[i] = new double[this->num_dimension];
	}

	this->position_limit = new double*[this->par_num_dimension.Get()];
	for( int i = 0; i < this->par_num_dimension.Get(); i++ )
	{
		this->position_limit[i] = new double[2];
	}

	this->gbest_position = new double[this->par_num_particle.Get()];
	this->cost = new double[this->par_num_particle.Get()];
	this->cost_pbest = new double[this->par_num_particle.Get()];
	this->velocity_limit = new double[this->num_dimension];

	for( int i = 0; i < this->par_num_particle.Get(); i++ )
	{
		memset( this->particle_position[i], 0, sizeof(double) * this->num_dimension );
	}
	for( int i = 0; i < this->par_num_particle.Get(); i++ )
	{
		memset( this->pre_particle_position[i], 0, sizeof(double) * this->num_dimension );
	}
	for( int i = 0; i < this->par_num_particle.Get(); i++ )
	{
		memset( this->particle_velocity[i], 0, sizeof(double) * this->num_dimension );
	}
	for( int i = 0; i < this->par_num_particle.Get(); i++ )
	{
		memset( this->pbest_position[i], 0, sizeof(double) * this->num_dimension );
	}

	for( int i = 0; i < this->num_dimension; i++ )
	{
		memset( this->position_limit[i], 0, sizeof(double) * 2 );
	}

	memset( this->gbest_position, 0, sizeof(double) * this->par_num_particle.Get());
	memset( this->cost, 0, sizeof(double) * this->par_num_particle.Get() );
	memset( this->cost_pbest, 0, sizeof(double) * this->par_num_particle.Get() );
	memset( this->velocity_limit, 0, sizeof(double) * this->num_dimension );

	const char* project_home = getenv( "FINROC_PROJECT_HOME");
	this->record_address = std::string( project_home) + "/etc/optimization/slow_walking";

}

//----------------------------------------------------------------------
// mbbOptimizationSlowWalking destructor
//----------------------------------------------------------------------
mbbOptimizationSlowWalking::~mbbOptimizationSlowWalking()
{
	for( int i = 0; i < this->par_num_particle.Get(); i++ )
	{
		delete[] this->particle_position[i];
	}
	delete[] this->particle_position;

	for( int i = 0; i < this->par_num_particle.Get(); i++ )
	{
		delete[] this->pre_particle_position[i];
	}
	delete[] this->pre_particle_position;

	for( int i = 0; i < this->par_num_particle.Get(); i++ )
	{
		delete[] this->particle_velocity[i];
	}
	delete[] this->particle_velocity;

	for( int i = 0; i < this->par_num_particle.Get(); i++ )
	{
		delete[] this->pbest_position[i];
	}
	delete[] this->pbest_position;

	for( int i = 0; i < this->num_dimension; i++ )
	{
		delete[] this->position_limit[i];
	}
	delete[] this->position_limit;

	delete[] this->gbest_position;
	delete[] this->cost;
	delete[] this->cost_pbest;
	delete[] this->velocity_limit;
}


//----------------------------------------------------------------------
// mbbOptimizationSlowWalking OnParameterChange
//----------------------------------------------------------------------
void mbbOptimizationSlowWalking::OnParameterChange()
{
	tModule::OnParameterChange();
	this->num_particle = this->par_num_particle.Get();
	this->num_dimension = this->par_num_dimension.Get();

	this->cost_function1_par = this->par_cost_function1.Get();
	this->cost_function2_par = this->par_cost_function2.Get();
	this->cost_function3_par = this->par_cost_function3.Get();
	this->cost_function4_par = this->par_cost_function4.Get();
	this->cost_function5_par = this->par_cost_function5.Get();
	this->optimization_on = this->par_optimization_on.Get();
	this->slow_walking_on = this->par_slow_walking_on.Get();
	this->fast_walking_on = this->par_fast_walking_on.Get();
}

//----------------------------------------------------------------------
// mbbOptimizationSlowWalking ProcessTransferFunction
//----------------------------------------------------------------------
bool mbbOptimizationSlowWalking::ProcessTransferFunction(double activation)
{
	// initial the velocity limit and position limit
	  if (!this->initialized)
	  {

			double velocity_limited[18] = {0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.02, 0.02, 0.02, 0.01, 0.001, 0.0001, 0.05, 0.05, 0.05, 50, 0.05};
				    double position_limited[18][2] = {{0.7, 1},{0.15, 0.4},{0.4, 0.9},// AHS Torque, T1, T2
				    		{0.9, 1.1}, {0.8, 1},								   // Lock Hip par1, par2
							{0.4, 0.85},{0.2, 0.5},{0.6, 0.9}, 						   // LP  Torque, T1, T2
							{0.62, 0.65}, {0.28, 0.4},{0.48, 0.6},
				    		{0.024, 0.027}, 				   // Lock knee angle, low, high
							{0.0, 0.001},						   // hip angle low
							{0.4, 0.7},						   // hip angle high
							{1.5, 3.5},						   // lock knee increment par1
							{1, 3.5},                      // lock knee increment par2
							{400, 800},
				    		{0.5, 0.7} };

	    for (int i = 0; i < this->num_dimension; i++)
	    {
	      this->velocity_limit[i] = velocity_limited[i];
	      for (int j = 0; j < 2; j++)
	      {
	        this->position_limit[i][j] = position_limited[i][j];
	      }
	    }
	    this->initialized = true;
	    FINROC_LOG_PRINT(DEBUG_WARNING, "SLOW WALKING VELOCITY AND POSITION LIMITATION INITIALIZATION");
	  }

	  //sensor inputs
	  this->left_hip_angle = this->si_left_hip_angle.Get();
	  this->right_hip_angle = this->si_right_hip_angle.Get();
	  this->walking_velocity = this->ci_walking_velocity.Get();
	  this->walking_distance = this->ci_walking_distance.Get();
	  this->phase2_left_stimulation = this->ci_phase2_left_stimulation.Get();
	  this->phase2_right_stimulation = this->ci_phase2_right_stimulation.Get();

	  this->knee_angle = this->ci_knee_angle.Get();
	  this->knee_velocity = this->ci_knee_velocity.Get();

	  //controller inputs
	  this->walking_simulation = this->ci_walking_stimulation.Get();
	  this->loop_times = this->ci_loop_times.Get();
	  this->step_length = this->ci_step_length.Get();
	  this->SlowWalkingStimulation();

	  CalculateSlowWalkingCost();

	  this->co_walking_velocity.Publish(this->walking_velocity);
	  this->co_walking_distance.Publish(this->walking_distance);
	  this->co_step_length.Publish(this->step_length);
	  this->co_loop_times.Publish(this->loop_times);
	  this->co_knee_velocity.Publish(this->knee_velocity);
	  // calculating walking steps and slow walking stimulation


	  // calculating the feet moving speed
	  //this->FootMovingSpeed();
	  // check obstacle contact and if contact, give cost
	  //this->ObstacleContactCost();

	  this->new_running = 0;
	  this->reset = false;

	  // calculate the speed and position of particle
	  if (this->optimization_flag == true)
	  {
	    if (this->loop_times == 0)
	    {
	      this->new_start_flag = false;
	    }
	    if (this->update_flag == false)
	    {
	      //  update velocity of particle
	      this->particle_velocity[this->particle] = this->optimization->UpdateParticleVelocity(this->particle_velocity[this->particle],
	    		  this->par_velocity_parameter.Get(), this->particle_position[this->particle], this->par_acceleration_constant1.Get(),
				  this->par_acceleration_constant2.Get(), this->pbest_position[this->particle],this->gbest_position, this->velocity_limit, this->num_dimension);

	      //  update position of particle
	      this->particle_position[this->particle] = this->optimization->UpdateParticlePosition(this->particle_velocity[this->particle],
	    		  this->particle_position[this->particle], this->position_limit, this->num_dimension);

	      //  set output

	      this->ahs_max_torque_output = this->particle_position[this->particle][0];
	      this->ahs_t1_output = this->particle_position[this->particle][1];
	      this->ahs_t2_output = this->particle_position[this->particle][2];

	      this->lh_par1_output = this->particle_position[this->particle][3];
	      this->lh_par2_output = this->particle_position[this->particle][4];

	      this->lp_max_torque_output = this->particle_position[this->particle][5];
	      this->lp_t1_output = this->particle_position[this->particle][6];
	      this->lp_t2_output = this->particle_position[this->particle][7];

	      this->lock_knee_angle_output = this->particle_position[this->particle][8];
	      this->lk_knee_angle_threshold_low = this->particle_position[this->particle][9];
	      this->lk_knee_angle_threshold_high = this->particle_position[this->particle][10];
	      this->lk_knee_angle_velocity = this->particle_position[this->particle][11];
	      this->lk_hip_angle_threshold_low = this->particle_position[this->particle][12];
	      this->lk_hip_angle_threshold_high = this->particle_position[this->particle][13];
	      this->lk_increment_par1 = this->particle_position[this->particle][14];
	      this->lk_increment_par2 = this->particle_position[this->particle][15];
	      this->max_impulse = this->particle_position[this->particle][16];
	      this->stimulation_knee_angle = this->particle_position[this->particle][17];

	      this->update_flag = true;
	    }

	    this->SetReset();

	    //TODO check this condition later
	    if( this->reset == true )
	    {
	    	this->fast_walking_stimulation = false;
	    	this->slow_walking_step = 0;
	    }

	    if (this->reset == true && this->new_start_flag == false)
	    {
	      if (this->loop_times <= 2)
	      {
	        this->new_running = 1;
	      }
	      else
	      {
	        //  calculate cost function and record
	        this->CostFunction(this->particle);
	        if (this->optimization_on)
	          this->optimization->RecordCostAddress(this->record_address, 3 , this->cost[this->particle], this->cost_function1, this->cost_function2);
	        //    record the position of particle // NOT CLEAR
	        if (this->optimization_on)
	          this->optimization->RecordParticleAddress(this->record_address, this->num_dimension, this->particle_position, this->cost, this->particle, 2,
	        		  (this->walking_velocity > 0 )?this->walking_velocity:this->last_recorded_velocity/* USE ANY of the Angles this->pelvis_position_x*/, this->loop_times);

	        //    update pbest
	        this->optimization->Updatepbest(this->pbest_position, this->particle_position, this->cost, this->cost_pbest, this->num_dimension, this->particle, this->pbest_temp);
	        this->pbest_position = this->optimization->UpdatepbestPosition(this->pbest_position, this->num_dimension, this->particle, this->pbest_temp);
	        this->cost_pbest = this->optimization->UpdatepbestCost(this->cost_pbest, this->num_dimension, this->particle, this->pbest_temp);
	        this->pbest_temp.clear();

	        //    record pbest
	        if (this->optimization_on)
	          this->optimization->RecordPBestAddress(this->record_address, this->num_dimension, this->pbest_position, this->cost_pbest, this->particle);

	        this->particle ++;

	        //    check if update gbest
	        if (this->particle == this->num_particle)
	        {
	          //    update gbest
	          this->optimization->Updategbest(this->gbest_position, this->pbest_position, this->cost_gbest, this->cost_pbest, this->num_dimension, this->num_particle, this->gbest_temp);
	          this->gbest_position = this->optimization->UpdategbestPosition(this->gbest_position, this->num_dimension, this->gbest_temp);
	          this->cost_gbest = this->optimization->UpdategbestCost(this->cost_gbest, this->num_dimension, this->gbest_temp);
	          this->gbest_temp.clear();
	          //  record gbest
	          if (this->optimization_on)
	            this->optimization->RecordGBestAddress(this->record_address, this->num_dimension, this->gbest_position, this->cost_gbest);
	          //  initial particle value as 0
	          this->particle = 0;
	        }

	        //    set flag, set new running, update particle value
	        this->update_flag = false;
	        this->new_running = 1;
	      }
	      this->new_start_flag = true;
	    }
	  }

	  //  optimization in the first iteration, initial particle
	  if (this->optimization_flag == false)
	  {
	    if (this->loop_times == 0)
	    {
	      this->new_start_flag = false;
	    }
	    if (this->update_flag == false)
	    {
	      for (int i = 0; i < this->par_num_particle.Get(); i++)
	      {
	        //    initial the positions
	        for (int j = 0; j < this->num_dimension; j++)
	        {
	          this->particle_position[i][j] = this->optimization->InitialParticle(this->position_limit[j][0], (this->position_limit[j][1] - this->position_limit[j][0]));
	        }
	      }
	      this->update_flag = true;
	    }

	    this->ahs_max_torque_output = this->particle_position[this->particle][0];
	    this->ahs_t1_output = this->particle_position[this->particle][1];
	    this->ahs_t2_output = this->particle_position[this->particle][2];

	    this->lh_par1_output = this->particle_position[this->particle][3];
	    this->lh_par2_output = this->particle_position[this->particle][4];

	    this->lp_max_torque_output = this->particle_position[this->particle][5];
	    this->lp_t1_output = this->particle_position[this->particle][6];
	    this->lp_t2_output = this->particle_position[this->particle][7];

	    this->lock_knee_angle_output = this->particle_position[this->particle][8];
	    this->lk_knee_angle_threshold_low = this->particle_position[this->particle][9];
	    this->lk_knee_angle_threshold_high = this->particle_position[this->particle][10];
	    this->lk_knee_angle_velocity = this->particle_position[this->particle][11];
	    this->lk_hip_angle_threshold_low = this->particle_position[this->particle][12];
	    this->lk_hip_angle_threshold_high = this->particle_position[this->particle][13];
	    this->lk_increment_par1 = this->particle_position[this->particle][14];
	    this->lk_increment_par2 = this->particle_position[this->particle][15];
	    this->max_impulse = this->particle_position[this->particle][16];
	    this->stimulation_knee_angle = this->particle_position[this->particle][17];

	    this->SetReset();
	    if (this->reset == true  && this->new_start_flag == false)
	    {
	      if (this->loop_times <= 2)
	      {
	        this->new_running = 1;
	      }
	      else
	      {
	        //  calculate cost function and record
	        this->CostFunction(this->particle);
	        if (this->optimization_on)
	          this->optimization->RecordCostAddress(this->record_address, 3 , this->cost[this->particle], this->cost_function1, this->cost_function2);
	        //    record particle
	        if (this->optimization_on)
	          this->optimization->RecordParticleAddress(this->record_address, this->num_dimension, this->particle_position, this->cost, this->particle, 2,
	        		  (this->walking_velocity > 0 )?this->walking_velocity:this->last_recorded_velocity/* USE ANY of the Angles this->pelvis_position_x*/, this->loop_times);

	        //    set pbest
	        this->cost_pbest[particle] = this->cost[particle];
	        for (int i = 0; i < this->num_dimension; i++)
	        {
	          this->pbest_position[this->particle][i] = this->particle_position[this->particle][i];
	        }

	        //    record pbest
	        if (this->optimization_on)
	          this->optimization->RecordPBestAddress(this->record_address, this->num_dimension, this->pbest_position, this->cost_pbest, this->particle);

	        this->particle ++;

	        if (this->particle == this->num_particle)
	        {
	          //    update gbest
	          this->optimization->Updategbest(this->gbest_position, this->pbest_position, this->cost_gbest, this->cost_pbest, this->num_dimension, this->num_particle, gbest_temp);
	          this->gbest_position = this->optimization->UpdategbestPosition(this->gbest_position, this->num_dimension, this->gbest_temp);
	          this->cost_gbest = this->optimization->UpdategbestCost(this->cost_gbest, this->num_dimension, this->gbest_temp);
	          this->gbest_temp.clear();

	          //      record gbest
	          if (this->optimization_on)
	            this->optimization->RecordGBestAddress(this->record_address, this->num_dimension, this->gbest_position, this->cost_gbest);

	          this->optimization_flag = true;
	          this->particle = 0;
	          this->update_flag = false;
	        }
	        this->new_running = 1;
	      }
	      this->new_start_flag = true;
	    }
	  }

	  // publish outputs

	  if (this->optimization_on)
	  {
		  if( this->slow_walking_stimulation || this->fast_walking_stimulation )
		  {
			  this->lock_knee_angle_output = 0.04; // fast walking only

			  this->co_output_ahs_max_torque.Publish(this->ahs_max_torque_output);
			  this->co_output_ahs_t1.Publish(this->ahs_t1_output);
			  this->co_output_ahs_t2.Publish(this->ahs_t2_output);
			  this->co_output_lh_par1.Publish(this->lh_par1_output);
			  this->co_output_lh_par2.Publish(this->lh_par2_output);
			  this->co_output_lp_t1.Publish(this->lp_t1_output);
			  this->co_output_lp_t2.Publish(this->lp_t2_output);
			  this->co_output_lp_max_torque.Publish(this->lp_max_torque_output);
			  this->co_output_knee_angle.Publish(this->lock_knee_angle_output);
			  this->co_output_lk_knee_angle_low.Publish(this->lk_knee_angle_threshold_low);
			  this->co_output_lk_knee_angle_high.Publish(this->lk_knee_angle_threshold_high);
			  this->co_output_lk_knee_velocity.Publish(this->lk_knee_angle_velocity);
			  this->co_output_lk_hip_angle_low.Publish(this->lk_hip_angle_threshold_low);
			  this->co_output_lk_hip_angle_high.Publish(this->lk_hip_angle_threshold_high);
			  this->co_output_lk_increment_par1.Publish(this->lk_increment_par1);
			  this->co_output_lk_increment_par2.Publish(this->lk_increment_par2);
			  this->co_output_max_impulse.Publish(this->max_impulse);

			  if( ( this->phase2_left_stimulation > 0 ) || ( this->phase2_right_stimulation > 0 ) )
			  {
				  this->co_output_stimulation_knee_angle.Publish(this->stimulation_knee_angle);
			  }
			  else
			  {
				  this->co_output_stimulation_knee_angle.Publish(0);
			  }

			  this->co_slow_walking_on.Publish(this->slow_walking_on); 
              // publish slow walking on not stimulation TODO remove me
			  this->co_slow_walking_stimulation.Publish(this->slow_walking_stimulation);
			  this->co_fast_walking_stimulation.Publish(this->fast_walking_stimulation);
			  this->co_slow_walking_step.Publish(this->slow_walking_step);
		  }
		  else
		  {
			  this->co_output_ahs_max_torque.Publish(0.73);
			  this->co_output_ahs_t1.Publish(0.2);
			  this->co_output_ahs_t2.Publish(0.5);

			  this->co_output_lh_par1.Publish(1);
			  this->co_output_lh_par2.Publish(1);

			  this->co_output_lp_max_torque.Publish(0.4);
			  this->co_output_lp_t1.Publish(0.48);
			  this->co_output_lp_t2.Publish(0.68);

			  this->co_output_knee_angle.Publish(0.04);
			  this->co_output_lk_knee_angle_low.Publish(0.3);
			  this->co_output_lk_knee_angle_high.Publish(0.55);
			  this->co_output_lk_knee_velocity.Publish(0.025);

			  this->co_output_max_impulse.Publish(700);

			  this->co_slow_walking_on.Publish(this->slow_walking_on);
              // publish slow walking on not stimulation TODO remove me
			  this->co_slow_walking_stimulation.Publish(this->slow_walking_stimulation);
			  this->co_output_stimulation_knee_angle.Publish(0);
		  }
	  }
	  else
	  {
		  if( this->slow_walking_stimulation && this->fast_walking_stimulation == false )
		  {
			  this->ahs_max_torque_output 		= this->par_ahs_max_torque_out1.Get();
			  this->ahs_t1_output		  		= this->par_ahs_t1_out2.Get();
			  this->ahs_t2_output		  		= this->par_ahs_t2_out3.Get();
			  this->lh_par1_output		  		= this->par_lh_par1_out4.Get();
			  this->lh_par2_output		  		= this->par_lh_par2_out5.Get();
			  this->lp_t1_output		  		= this->par_lp_t1_out7.Get();
			  this->lp_t2_output		  		= this->par_lp_t2_out8.Get();
			  this->lp_max_torque_output  		= this->par_lp_torque_out6.Get();
			  this->lock_knee_angle_output		= this->par_lk_angle_out9.Get();
			  this->lk_knee_angle_velocity		= this->par_lk_knee_velocity_out12.Get();
			  this->lk_hip_angle_threshold_high = this->par_lk_hip_angle_high_out14.Get();
			  this->lk_hip_angle_threshold_low  = this->par_lk_hip_angle_low_out13.Get();
			  this->lk_knee_angle_threshold_high= this->par_lk_high_threshold_out11.Get();
			  this->lk_knee_angle_threshold_low = this->par_lk_low_threshold_out10.Get();
			  this->lk_increment_par1			= this->par_lk_increment_par1_out15.Get();
			  this->lk_increment_par2			= this->par_lk_increment_par2_out16.Get();
			  this->max_impulse				    = this->par_max_impulse_out17.Get();
			  this->stimulation_knee_angle      = this->par_stimulation_knee_angle_out18.Get();

			  this->co_output_ahs_max_torque.Publish(this->ahs_max_torque_output);
			  this->co_output_ahs_t1.Publish(this->ahs_t1_output);
			  this->co_output_ahs_t2.Publish(this->ahs_t2_output);
			  this->co_output_lh_par1.Publish(this->lh_par1_output);

			  this->co_output_lp_t1.Publish(this->lp_t1_output);
			  this->co_output_lp_t2.Publish(this->lp_t2_output);
			  this->co_output_lp_max_torque.Publish(this->lp_max_torque_output);

			  this->co_output_lk_knee_angle_low.Publish(this->lk_knee_angle_threshold_low);
			  this->co_output_lk_knee_angle_high.Publish(this->lk_knee_angle_threshold_high);
			  this->co_output_lk_knee_velocity.Publish(this->lk_knee_angle_velocity);
			  this->co_output_lk_hip_angle_low.Publish(this->lk_hip_angle_threshold_low);

			  if( this->walking_velocity < 0.75 && this->walking_velocity > 0.71)
			  {
				  this->co_output_lk_hip_angle_high.Publish(0.35);
//				  this->co_output_knee_angle.Publish(0.64);
				  this->co_output_lh_par2.Publish(0.92);
			  }
			  else if( this->walking_velocity <= 0.71 && this->walking_velocity > 0.6 )
			  {
				  this->co_output_lk_hip_angle_high.Publish(0.28);
//				  this->co_output_knee_angle.Publish(0.64);
				  this->co_output_lh_par2.Publish(.95);
			  }
			  else if( this->walking_velocity <= 0.6 )
			  {
				  this->co_output_lk_hip_angle_high.Publish(0.26);
//				  this->co_output_knee_angle.Publish(0.64);
				  this->co_output_lh_par2.Publish(.99);
			  }
			  else
			  {
				  this->co_output_lk_hip_angle_high.Publish(this->lk_hip_angle_threshold_high);
				  this->co_output_knee_angle.Publish(this->lock_knee_angle_output);
				  this->co_output_lh_par2.Publish(this->lh_par2_output);
			  }

			  this->co_output_lk_increment_par1.Publish(this->lk_increment_par1);
			  this->co_output_lk_increment_par2.Publish(this->lk_increment_par2);
			  this->co_output_max_impulse.Publish(this->max_impulse);

			  // publish slow walking on not stimulation TODO remove me
			  this->co_slow_walking_on.Publish(this->slow_walking_on);
			  this->co_slow_walking_stimulation.Publish(this->slow_walking_stimulation);
			  this->co_fast_walking_stimulation.Publish(this->fast_walking_stimulation);

			  if( ( this->phase2_left_stimulation > 0 ) || ( this->phase2_right_stimulation > 0 ) )
			  {
				  this->co_output_stimulation_knee_angle.Publish(this->stimulation_knee_angle);
				  this->co_output_stimulation_knee_angle_activity.Publish(1);
			  }
			  else
			  {
				  this->co_output_stimulation_knee_angle.Publish(0);
				  this->co_output_stimulation_knee_angle_activity.Publish(1);
			  }

			  this->co_slow_walking_step.Publish(this->slow_walking_step);
		  }
		  else if( !this->slow_walking_stimulation && this->fast_walking_stimulation )
		  {
			  this->ahs_max_torque_output 		= this->par_ahs_max_torque_out1.Get();
						  this->ahs_t1_output		  		= this->par_ahs_t1_out2.Get();
						  this->ahs_t2_output		  		= this->par_ahs_t2_out3.Get();
						  this->lh_par1_output		  		= this->par_lh_par1_out4.Get();
						  this->lh_par2_output		  		= this->par_lh_par2_out5.Get();
						  this->lp_t1_output		  		= this->par_lp_t1_out7.Get();
						  this->lp_t2_output		  		= this->par_lp_t2_out8.Get();
						  this->lp_max_torque_output  		= this->par_lp_torque_out6.Get();
						  this->lock_knee_angle_output		= this->par_lk_angle_out9.Get();
						  this->lk_knee_angle_velocity		= this->par_lk_knee_velocity_out12.Get();
						  this->lk_hip_angle_threshold_high = this->par_lk_hip_angle_high_out14.Get();
						  this->lk_hip_angle_threshold_low  = this->par_lk_hip_angle_low_out13.Get();
						  this->lk_knee_angle_threshold_high= this->par_lk_high_threshold_out11.Get();
						  this->lk_knee_angle_threshold_low = this->par_lk_low_threshold_out10.Get();
						  this->lk_increment_par1			= this->par_lk_increment_par1_out15.Get();
						  this->lk_increment_par2			= this->par_lk_increment_par2_out16.Get();
						  this->max_impulse				    = this->par_max_impulse_out17.Get();
						  this->stimulation_knee_angle      = this->par_stimulation_knee_angle_out18.Get();

						  this->co_output_ahs_max_torque.Publish(this->ahs_max_torque_output);
						  this->co_output_ahs_t1.Publish(this->ahs_t1_output);
						  this->co_output_ahs_t2.Publish(this->ahs_t2_output);
						  this->co_output_lh_par1.Publish(this->lh_par1_output);

						  this->co_output_lp_t1.Publish(this->lp_t1_output);
						  this->co_output_lp_t2.Publish(this->lp_t2_output);
						  this->co_output_lp_max_torque.Publish(this->lp_max_torque_output);

						  this->co_output_lk_knee_angle_low.Publish(this->lk_knee_angle_threshold_low);
						  this->co_output_lk_knee_angle_high.Publish(this->lk_knee_angle_threshold_high);
						  this->co_output_lk_knee_velocity.Publish(this->lk_knee_angle_velocity);
						  this->co_output_lk_hip_angle_low.Publish(this->lk_hip_angle_threshold_low);

						  this->co_output_lk_hip_angle_high.Publish(this->lk_hip_angle_threshold_high);
						  this->co_output_knee_angle.Publish(this->lock_knee_angle_output);
						  this->co_output_lh_par2.Publish(this->lh_par2_output);

						  this->co_output_lk_increment_par1.Publish(this->lk_increment_par1);
						  this->co_output_lk_increment_par2.Publish(this->lk_increment_par2);
						  this->co_output_max_impulse.Publish(this->max_impulse);

						  // publish slow walking on not stimulation TODO remove me
						  this->co_fast_walking_on.Publish(this->fast_walking_on);
						  this->co_fast_walking_stimulation.Publish(this->fast_walking_stimulation);

						  this->co_slow_walking_on.Publish(this->slow_walking_on);
						  this->co_slow_walking_stimulation.Publish(this->slow_walking_stimulation);

						  if( ( this->phase2_left_stimulation > 0 ) || ( this->phase2_right_stimulation > 0 ) )
						  {
							  this->co_output_stimulation_knee_angle.Publish(this->stimulation_knee_angle);
							  this->co_output_stimulation_knee_angle_activity.Publish(1);
						  }
						  else
						  {
							  this->co_output_stimulation_knee_angle.Publish(0);
							  this->co_output_stimulation_knee_angle_activity.Publish(1);
						  }

						  this->co_slow_walking_step.Publish(this->slow_walking_step);
		  }
		  else
		  {
			  this->co_output_ahs_max_torque.Publish(0.73);
			  this->co_output_ahs_t1.Publish(0.2);
			  this->co_output_ahs_t2.Publish(0.5);

			  this->co_output_lh_par1.Publish(1);
			  this->co_output_lh_par2.Publish(1);

			  this->co_output_lp_max_torque.Publish(0.4);
			  this->co_output_lp_t1.Publish(0.48);
			  this->co_output_lp_t2.Publish(0.68);

			  this->co_output_knee_angle.Publish(0.04);
			  this->co_output_lk_knee_angle_low.Publish(0.3);
			  this->co_output_lk_knee_angle_high.Publish(0.55);
			  this->co_output_lk_knee_velocity.Publish(0.025);

			  this->co_output_lk_increment_par1.Publish(1.0);
			  this->co_output_lk_increment_par2.Publish(2.0);
			  this->co_output_max_impulse.Publish(700);

			  this->co_slow_walking_on.Publish(this->slow_walking_on); 
              // publish slow walking on not stimulation TODO remove me
			  this->co_slow_walking_stimulation.Publish(this->slow_walking_stimulation);
			  this->co_fast_walking_stimulation.Publish(this->fast_walking_stimulation);
			  this->co_fast_walking_on.Publish(this->fast_walking_on); 
              // publish slow walking on not stimulation TODO remove me
			  this->co_output_stimulation_knee_angle.Publish(0);
		  }
	  }

	  if(this->particle > 0)
	  {
		  this->co_cost.Publish(this->cost[this->particle-1]);
	  }
	  else if (this->particle == 0)
	  {
		  this->co_cost.Publish(this->cost[this->num_particle]);
	  }

	  this->co_cost_function1.Publish(this->cost_function1);
	  this->co_cost_function2.Publish(this->cost_function2);
	  this->co_cost_function3.Publish(this->cost_function3);
	  this->co_cost_function4.Publish(this->cost_function4);
	  this->co_cost_function5.Publish(this->cost_function5);

	  this->co_particle.Publish(this->particle);

	  this->co_time_span.Publish(this->time_span);

	  this->so_new_running.Publish(this->new_running);
	  return true;
}
//----------------------------------------------------------------------
// class mbbOptimizationSlowWalking CalculateSlowWalkingCost
//----------------------------------------------------------------------

void mbbOptimizationSlowWalking::CalculateSlowWalkingCost()
{
	double velocityDifference           = 0.0;
	double kneeAngleDifference          = 0.0;
	double stepLengthDifference         = 0.0;

	last_recorded_velocity = this->walking_velocity;

	// initial cost functions
	if (this->loop_times == 0)
	{
		velocityDifference    		  			= 0.0;
		this->cost_function1  		  			= 0.0;
		this->cost_function2  		 			= 0.0;
		this->cost_function3 		  			= 0.0;
		//this->cost_function4  		  			= 0.0;
		this->cost_function5  		  			= 0.0;
		this->totalVelocityDifference		  	= 0.0;
		stepLengthDifference          			= 0.0;
		this->totalStepLengthDifference     	= 0.0;
		this->slow_walking_step                 = 0;
	}
	else
	{
		if( this->slow_walking_stimulation && !this->fast_walking_stimulation )
		{
			this->cost_function1 = this->walking_distance;


			velocityDifference = fabs(this->walking_velocity - ExpectedSlowVelocity);
			this->totalVelocityDifference = this->totalVelocityDifference + velocityDifference;

			kneeAngleDifference = fabs( this->knee_angle - 0.6);

			stepLengthDifference = fabs(this->step_length - this->ExpectedStepLength);
			this->totalStepLengthDifference = this->totalStepLengthDifference + stepLengthDifference;

			if( velocityDifference > 0.2 ) // TODO add check if robot fell, if so modify weights accordingly
			{
				velocityDifference *= 10; // how this should change the walking_velocity parameter
			}
			else if( velocityDifference < 0.02 )
			{
				velocityDifference *= 0.5;
			}
			this->cost_function2 = this->totalVelocityDifference;
			this->cost_function3 = this->loop_times * 10;
			//this->cost_function4 = kneeAngleDifference;
			this->cost_function5 = this->totalStepLengthDifference;

			co_velocity_diff.Publish(velocityDifference);
			co_step_difference.Publish(stepLengthDifference);
		}
		else if( !this->slow_walking_stimulation && this->fast_walking_stimulation )
		{
			this->cost_function1 = this->walking_distance;


			velocityDifference = fabs( ExpectedFastVelocity - this->walking_velocity );
			this->totalVelocityDifference = this->totalVelocityDifference + velocityDifference;


			stepLengthDifference = fabs(this->step_length - this->ExpectedStepLength);
			this->totalStepLengthDifference = this->totalStepLengthDifference + stepLengthDifference;

			if( velocityDifference > 0.2 ) // TODO add check if robot fell, if so modify weights accordingly
			{
				velocityDifference *= 10; // how this should change the walking_velocity parameter
			}
			else if( velocityDifference < 0.02 )
			{
				velocityDifference *= 0.5;
			}
			this->cost_function2 = this->totalVelocityDifference;
			this->cost_function3 = this->loop_times * 10;
			this->cost_function5 = this->totalStepLengthDifference;

			co_velocity_diff.Publish(velocityDifference);
			co_step_difference.Publish(stepLengthDifference);
		}
	}
}
//----------------------------------------------------------------------
// class mbbOptimizationSlowWalking slowWalkingStimulation
//----------------------------------------------------------------------
void mbbOptimizationSlowWalking::SlowWalkingStimulation()
{
  //calculate walking steps, calculate the obstacle walking stimulation
  if( (this->slow_walking_on == true) && ( this->fast_walking_on == false ) &&  ( this->loop_times>4 ) )
  {
    	  this->slow_walking_stimulation = true;
    	  this->slow_walking_step = this->loop_times - 4;
  }
  else if( ( this->slow_walking_on == false ) && ( this->fast_walking_on == true ) &&  ( this->loop_times>4 ) )
  {
    	  this->fast_walking_stimulation = true;
    	  this->slow_walking_step = this->loop_times - 4;
  }
  else
  {
    this->walking_step = 0;
    this->slow_walking_step = 0;
    this->slow_walking_stimulation = false;
    this->fast_walking_stimulation = false;
    this->stimulation_knee_angle = 0;
  }
}
//----------------------------------------------------------------------
// class mbbOptimizationSlowWalking slowWalkingStimulation
//----------------------------------------------------------------------
void mbbOptimizationSlowWalking::CalculateSlowWalkingDistance()
{
	// TODO needed or just call from Calculate walking velocity class will do the work
}
//----------------------------------------------------------------------
// mbbOptimizationSlowWalking CalculateActivity
//----------------------------------------------------------------------
ib2c::tActivity mbbOptimizationSlowWalking::CalculateActivity(std::vector<ib2c::tActivity> &derived_activity, double activation) const
{
	return activation;
}

//----------------------------------------------------------------------
// mbbOptimizationSlowWalking CalculateTargetRating
//----------------------------------------------------------------------
ib2c::tTargetRating mbbOptimizationSlowWalking::CalculateTargetRating(double activation) const
{
	return 0.5; // TODO How to decide this?
}

//----------------------------------------------------------------------
// mbbOptimizationSlowWalking CostFunction
//----------------------------------------------------------------------
void mbbOptimizationSlowWalking::CostFunction(int particle)
{

	if( this->walking_distance > 10 )
	{
		this->par_cost_function1.Set(15);
	}
	else if( this->walking_distance > 15 )
	{
		this->par_cost_function1.Set(20);
	}

	if( this->loop_times > 10 )
	{
		this->par_cost_function3.Set(15);
	}

	// compute the avg velocity difference for cost function
	this->cost_function2 = this->cost_function2 / this->loop_times;
	this->cost_function5 = this->cost_function5 / this->loop_times;

	this->cost[this->particle] = this->par_cost_function1.Get() * this->cost_function1
								 + this->par_cost_function3.Get() * this->cost_function3
								 - this->par_cost_function2.Get() * this->cost_function2
								// - this->par_cost_function4.Get() * this->cost_function4
								 - this->par_cost_function5.Get() * this->cost_function5;
}

//----------------------------------------------------------------------
// class mbbOptimizationSlowWalking SetReset
//----------------------------------------------------------------------
void mbbOptimizationSlowWalking::SetReset()
{
  if (this->loop_times > 50 || fabs(this->ci_pitch_angle.Get()) > 0.7 || fabs(this->ci_roll_angle.Get()) > 0.7 || fabs(this->ci_com_x_body.Get()) > 0.65) // TODO Add knee velocity or angle
  {
    this->reset = true;
  }
}

//----------------------------------------------------------------------
// End of namespace declaration
//----------------------------------------------------------------------
}
}
}
