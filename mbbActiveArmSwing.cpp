//
// You received this file as part of Finroc
// A Framework for intelligent robot control
//
// Copyright (C) Finroc GbR (finroc.org)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//----------------------------------------------------------------------
/*!\file    projects/biped/motor_patterns/mbbActiveArmSwing.cpp
 *
 * \author  Tobias Luksch
 *
 * \date    2013-09-26
 *
 */
//----------------------------------------------------------------------
#include "projects/biped/motor_patterns/mbbActiveArmSwing.h"
//#include "rrlib/math/utilities.h"
//----------------------------------------------------------------------
// External includes (system with <>, local with "")
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Internal includes with ""
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Debugging
//----------------------------------------------------------------------
#include <cassert>

//----------------------------------------------------------------------
// Namespace usage
//----------------------------------------------------------------------
//using namespace rrlib::math;
//----------------------------------------------------------------------
// Namespace declaration
//----------------------------------------------------------------------
namespace finroc
{
namespace biped
{
namespace motor_patterns
{

//----------------------------------------------------------------------
// Forward declarations / typedefs / enums
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Const values
//----------------------------------------------------------------------
runtime_construction::tStandardCreateModuleAction<mbbActiveArmSwing> cCREATE_ACTION_FOR_MBB_ACTIVEARMSWING("ActiveArmSwing");

//----------------------------------------------------------------------
// Implementation
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// mbbActiveArmSwing constructor
//----------------------------------------------------------------------
mbbActiveArmSwing::mbbActiveArmSwing(core::tFrameworkElement *parent, const std::string &name,
                                     ib2c::tStimulationMode stimulation_mode, unsigned int number_of_inhibition_ports,
                                     mMotorPattern::tCurveMode mode,
                                     double max_impulse_length,
                                     double impulse_max_start,
                                     double impulse_max_end) :
  mMotorPattern(parent, name, stimulation_mode, number_of_inhibition_ports, mode, max_impulse_length, impulse_max_start, impulse_max_end),

//  mbbActiveArmSwing::mbbActiveArmSwing(core::tFrameworkElement *parent, const std::string &name,
//                                       ib2c::tStimulationMode stimulation_mode, unsigned int number_of_inhibition_ports,
//                                       double max_impulse_length,
//                                       double impulse_mean,
//                                       double impulse_stddev) :
//    mGaussianMotorPattern(parent, name, stimulation_mode, number_of_inhibition_ports, max_impulse_length, impulse_mean, impulse_stddev),
  active(false),
  shoulder_torque(0.0),
  termination_factor(0.0),
  push_recovery_factor(0.0),
  last_step_state(0.0),
  scale(0.0),
  push_recovery_stand(0.0),
  fast_walking_factor(0.11),
  fast_walking(false),
  loop_count(0)
{
  this->par_shoulder_torque.Set(0.08);//0.08
  this->par_termination_factor.Set(0.6);
  this->par_push_recovery_factor.Set(0.2);
  this->par_fast_walking_factor.Set(0.2);
}

//----------------------------------------------------------------------
// mbbActiveArmSwing destructor
//----------------------------------------------------------------------
mbbActiveArmSwing::~mbbActiveArmSwing()
{}


//----------------------------------------------------------------------
// mbbActiveArmSwing OnStaticParameterChange
//----------------------------------------------------------------------
void mbbActiveArmSwing::OnStaticParameterChange()
{
  tModule::OnStaticParameterChange();

}

//----------------------------------------------------------------------
// mbbActiveArmSwing OnParameterChange
//----------------------------------------------------------------------
void mbbActiveArmSwing::OnParameterChange()
{
  tModule::OnParameterChange();
  /*
    Add handling of your own parameters here.
    If this method is called, at least on of our parameters has changed. However, each can be checked using its .HasChanged() method.
    */
  this->shoulder_torque = this->par_shoulder_torque.Get();
  this->termination_factor = this->par_termination_factor.Get();
  this->push_recovery_factor = this->par_push_recovery_factor.Get();
  this->fast_walking_factor = this->par_fast_walking_factor.Get();
}

//----------------------------------------------------------------------
// mbbActiveArmSwing ProcessTransferFunction
//----------------------------------------------------------------------
bool mbbActiveArmSwing::ProcessTransferFunction(double activation)
{
  // inputs
  //Get your special Controller Inputs here
  this->scale = this->ci_scale.Get();
  this->last_step_state = this->ci_last_step_state.Get();
  this->push_recovery_stand = this->ci_push_recovery_stand.Get();
  this->fast_walking = this->ci_fast_walking.Get();
  this->loop_count = this->ci_loop_count.Get();
  // transfer function
  // check if new motor pattern has to be started
  if ((activation > 0.0) && (!this->active))
  {
    // do you transfer function calculations
    // ...

    // set impulse length and start motor pattern
    this->SetPatternLength(this->max_impulse_length, this->impulse_max_start, this->impulse_max_end);
    //  this->SetPatternLength(this->max_impulse_length, this->impulse_mean, this->impulse_stddev);

    this->StartMotorPattern();
  }
  // remember state
  this->active = (activation > 0.0);
  this->GetPatternValue();

  //outputs
  if (this->last_step_state)
  {
    this->co_ipsilateral_shoulder_torque.Publish(-this->shoulder_torque * this->impulse_factor * this->scale);
    this->co_contralateral_shoulder_torque.Publish(this->shoulder_torque * this->impulse_factor * this->scale);
  }
  else if (this->push_recovery_stand)
  {
    this->co_ipsilateral_shoulder_torque.Publish(-this->shoulder_torque * this->impulse_factor * this->scale * this->push_recovery_factor);
    this->co_contralateral_shoulder_torque.Publish(this->shoulder_torque * this->impulse_factor * this->scale * this->push_recovery_factor);
  }
  else if ( this->fast_walking )
  {
	  if( this->loop_count >= 4 && this->loop_count < 6 )
	  {
		  this->fast_walking_factor = 0.1;
	  }
//	  else if( this->loop_count >= 6 && this->loop_count < 8 )
//	  {
//	  		  this->fast_walking_factor = 0.15;
//	  }
	  else if( this->loop_count >= 6 && this->loop_count < 8 )
	  {
		  this->fast_walking_factor = 0.12;
	  }
	  else
	  {
		  this->fast_walking_factor = 0.15;
	  }

	  this->co_ipsilateral_shoulder_torque.Publish(-this->impulse_factor * this->scale * this->fast_walking_factor);
	  this->co_contralateral_shoulder_torque.Publish(this->impulse_factor * this->scale * this->fast_walking_factor);
  }
  else
  {
    this->co_ipsilateral_shoulder_torque.Publish(-this->shoulder_torque * this->impulse_factor * this->scale * this->termination_factor);
    this->co_contralateral_shoulder_torque.Publish(this->shoulder_torque * this->impulse_factor * this->scale * this->termination_factor);
  }

this->co_fast_walking_stimulation.Publish(this->fast_walking);
this->co_loop_count.Publish(this->loop_count);
  //Use your input and output ports to transfer data through this module.
  //Return whether this transfer was successful or not.

  return true;
}

//----------------------------------------------------------------------
// mbbActiveArmSwing CalculateActivity
//----------------------------------------------------------------------
ib2c::tActivity mbbActiveArmSwing::CalculateActivity(std::vector<ib2c::tActivity> &derived_activity, double activation) const
{
  //Return a meaningful activity value here. You also want to set the derived activity vector, if you registered derived activities e.g. in the constructor.
  return this->impulse_factor * activation;
}

//----------------------------------------------------------------------
// mbbActiveArmSwing CalculateTargetRating
//----------------------------------------------------------------------
ib2c::tTargetRating mbbActiveArmSwing::CalculateTargetRating(double activation) const
{
  //Return a meaningful target rating here. Choosing 0.5 just because you have no better idea is not "meaningful"!
// If you do not want to use the activation in this calculation remove its name to rid of the warning.
  return this->impulse_factor * activation;
}

//----------------------------------------------------------------------
// End of namespace declaration
//----------------------------------------------------------------------
}
}
}
