//
// You received this file as part of Finroc
// A Framework for intelligent robot control
//
// Copyright (C) Finroc GbR (finroc.org)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//----------------------------------------------------------------------
/*!\file    projects/biped/control/gLegGroup.cpp
 *
 * \author  Jie Zhao
 * \author  Tobias Luksch
 * \date    2013-10-17
 *
 */
//----------------------------------------------------------------------
#include "projects/biped/control/gLegGroup.h"
#include "plugins/ib2c/mbbFusion.h"

#include "projects/biped/control/gbbLegHoldPosition.h"
#include "projects/biped/control/gLegTermination.h"

#include "projects/biped/utils/mFootLoad.h"
#include "projects/biped/utils/mCenterOfPressure.h"
#include "projects/biped/skills/mAverageFilter.h"
#include "projects/biped/skills/mbbTerminationFusion.h"
#include "projects/biped/skills/mbbStimulationSkill.h"
#include "projects/biped/skills/mbbLegStand.h"
#include "projects/biped/motor_patterns/mbbLegPropel.h"
#include "projects/biped/motor_patterns/mbbLegPropelSin.h"
#include "projects/biped/motor_patterns/mbbWeightAcceptanceMP.h"
#include "projects/biped/motor_patterns/mbbLegLiftLeg.h"
#include "projects/biped/motor_patterns/mbbSingleTorquePattern.h"
#include "projects/biped/reflexes/mbbHeelStrikeReflex2.h"
#include "projects/biped/reflexes/mbbLockKnee.h"
#include "projects/biped/reflexes/mbbAdjustJointAngle.h"
#include "projects/biped/reflexes/mbbReduceTensionAnkleX.h"
#include "projects/biped/reflexes/mbbControlForwardVelocityAnkle.h"
#include "projects/biped/reflexes/mbbControlAnkleRollAngle.h"
//#include "biped/reflexes/mbbControlAnkleRollLat.h"
#include "projects/biped/reflexes/mbbLateralBalanceAnkle.h"
#include "projects/biped/reflexes/mbbFrontalBalanceAnkle.h"
#include "projects/biped/reflexes/mbbCutaneousReflex.h"
#include "projects/biped/reflexes/mbbAnkleBrakeTorque.h"
#include "projects/biped/reflexes/mbbFootContact.h"
#include "projects/biped/reflexes/mbbHoldKnee.h"
#include "projects/biped/reflexes/mbbUprightKnee.h"
#include "projects/biped/reflexes/mbbKneeFlexion.h"
#include "projects/biped/utils/mMemoryUnit.h"
//#include "biped/reflexes/mbbControlKneeAngle.h"
//#include "biped/reflexes/mbbAnkleComShift.h"

//#include "projects/biped/dec/mbbCompensationAnklePitch.h"
#include "projects/biped/dec/mbbCompensationAnkleRoll.h"
#include "projects/biped/dec/mbbCompensationAnklePitch2.h"
#include "projects/biped/dec/mbbCompensationKnee.h"
#include "projects/biped/dec/mbbLateralAdjustJointAngle.h"
#include "projects/biped/reflexes/mbbLateralControlPushRecovery.h"


#include <cassert>

//----------------------------------------------------------------------
// Namespace usage
//----------------------------------------------------------------------
using namespace finroc::biped::motor_patterns;
using namespace finroc::biped::reflexes;
using namespace finroc::biped::skills;
using namespace finroc::biped::utils;
using namespace finroc::biped::dec;
using namespace finroc::ib2c;

//----------------------------------------------------------------------
// Namespace declaration
//----------------------------------------------------------------------
namespace finroc
{
namespace biped
{
namespace control
{

//----------------------------------------------------------------------
// Forward declarations / typedefs / enums
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Const values
//----------------------------------------------------------------------
static runtime_construction::tStandardCreateModuleAction<gLegGroup> cCREATE_ACTION_FOR_G_LEGGROUP("LegGroup");

//----------------------------------------------------------------------
// Implementation
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// gLegGroup constructor
//----------------------------------------------------------------------
gLegGroup::gLegGroup(core::tFrameworkElement *parent, const std::string &name,
                     const std::string &structure_config_file) :
  tSenseControlGroup(parent, name, structure_config_file, false) // change to 'true' to make group's ports shared (so that ports in other processes can connect to its output and/or input ports)
{
  // add modules
  // ============================================

  //BEGIN fusion behaviours
  mbbFusion<double> * fusion_abs_angle_knee = new mbbFusion<double>(this,  "Abs. Angle Knee", 14, tStimulationMode::ENABLED, 1);
  fusion_abs_angle_knee->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
  fusion_abs_angle_knee->number_of_inhibition_ports.Set(1);
  fusion_abs_angle_knee->CheckStaticParameters();

  mbbFusion<double> * fusion_abs_angle_ankle_y = new mbbFusion<double>(this, "Abs. Angle Ankle Y", 6, tStimulationMode::ENABLED, 1);
  fusion_abs_angle_ankle_y->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
  fusion_abs_angle_ankle_y->number_of_inhibition_ports.Set(1);
  fusion_abs_angle_ankle_y->CheckStaticParameters();
  mbbFusion<double> * fusion_abs_angle_ankle_x = new mbbFusion<double>(this, "Abs. Angle Ankle X", 8, tStimulationMode::ENABLED, 1);
  fusion_abs_angle_ankle_x->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
  fusion_abs_angle_ankle_x->number_of_inhibition_ports.Set(1);
  fusion_abs_angle_ankle_x->CheckStaticParameters();

  mbbFusion<double> * fusion_rel_angle_knee = new mbbFusion<double>(this,  "Rel. Angle Knee", 1, tStimulationMode::ENABLED);
  fusion_rel_angle_knee->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
  mbbFusion<double> * fusion_rel_angle_ankle_y = new mbbFusion<double>(this,  "Rel. Angle Ankle Y", 2, tStimulationMode::ENABLED);
  fusion_rel_angle_ankle_y->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
  mbbFusion<double> * fusion_rel_angle_ankle_x = new mbbFusion<double>(this, "Rel. Angle Ankle X", 1, tStimulationMode::ENABLED);
  fusion_rel_angle_ankle_x->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);

  mbbFusion<double> * fusion_abs_torque_knee = new mbbFusion<double>(this, "Abs. Torque Knee", 5, tStimulationMode::ENABLED);
  fusion_abs_torque_knee->fusion_method.Set(tFusionMethod::WEIGHTED_SUM);
  mbbFusion<double> * fusion_abs_torque_ankle_y = new mbbFusion<double>(this,  "Abs. Torque Ankle Y", 7, tStimulationMode::ENABLED);
  fusion_abs_torque_ankle_y->fusion_method.Set(tFusionMethod::WEIGHTED_SUM);
  mbbFusion<double> * fusion_abs_torque_ankle_x = new mbbFusion<double>(this, "Abs. Torque Ankle X", 12, tStimulationMode::ENABLED);
  fusion_abs_torque_ankle_x->fusion_method.Set(tFusionMethod::WEIGHTED_SUM);
  //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  // memory units for delaying the activity of sensor output from fusion units of torque control
  mMemoryUnit * memory_abs_torque_knee = new mMemoryUnit(this, "Memory Torque Knee");

  mMemoryUnit * memory_abs_torque_ankle_y = new mMemoryUnit(this, "Memory Torque Ankle Y");

  mMemoryUnit * memory_abs_torque_ankle_x = new mMemoryUnit(this, "Memory Torque Ankle X");


  // fusion of cyclic walking and termination

  mbbFusion<double> * fusion_torque_ankle_x = new mbbFusion<double>(this, "Torque Ankle X", 2, tStimulationMode::ENABLED);
  fusion_torque_ankle_x->fusion_method.Set(tFusionMethod::WEIGHTED_SUM);

  mbbFusion<double> * fusion_angle_ankle_x = new mbbFusion<double>(this, "Angle Ankle X", 2, tStimulationMode::ENABLED);
  fusion_angle_ankle_x->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);

  mbbFusion<double> * fusion_torque_ankle_y = new mbbFusion<double>(this, "Torque Ankle Y", 2, tStimulationMode::ENABLED);
  fusion_torque_ankle_y->fusion_method.Set(tFusionMethod::WEIGHTED_SUM);

  mbbFusion<double> * fusion_angle_ankle_y = new mbbFusion<double>(this, "Angle Ankle Y", 2, tStimulationMode::ENABLED);
  fusion_angle_ankle_y->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);

  mbbFusion<double> * fusion_torque_knee = new mbbFusion<double>(this, "Torque Knee", 2, tStimulationMode::ENABLED);
  fusion_torque_knee->fusion_method.Set(tFusionMethod::WEIGHTED_SUM);

  mbbFusion<double> * fusion_angle_knee = new mbbFusion<double>(this, "Angle Knee", 3, tStimulationMode::ENABLED); // changed to 3 for PSO
  fusion_angle_knee->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);


  //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  // forward velocity
  mbbFusion<double> * fusion_forward_velocity = new mbbFusion<double>(this, "Forward Velocity", 2, tStimulationMode::ENABLED);
  fusion_forward_velocity->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);

  // lateral velovity
  mbbFusion<double> * fusion_lateral_velocity = new mbbFusion<double>(this, "Lateral Velocity", 2, tStimulationMode::ENABLED);
  fusion_lateral_velocity->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);

  // lateral balance ankle
  mbbFusion<double> * fusion_lateral_balance_ankle = new mbbFusion<double>(this, "Lateral Balance Ankle", 2, tStimulationMode::ENABLED);
  fusion_lateral_balance_ankle->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);

  // leg propel
  mbbFusion<double> * fusion_leg_propel = new mbbFusion<double>(this, "Leg Propel", 3, tStimulationMode::ENABLED);
  fusion_leg_propel->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);

  // weight acceptance
  mbbFusion<double> * fusion_weight_accept = new mbbFusion<double>(this, "Weight Accept", 4, tStimulationMode::ENABLED);
  fusion_weight_accept->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);

  // heel strike
  mbbFusion<double> * fusion_heel_strike = new mbbFusion<double>(this, "Heel Strike", 1, tStimulationMode::ENABLED);
  fusion_heel_strike->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);

  // control ankle pitch during curve walking
  mbbFusion<double> * fusion_control_ankle_roll = new mbbFusion<double>(this, "Control Ankle Roll", 3, tStimulationMode::ENABLED);
  fusion_control_ankle_roll->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);

  // fusion ankle brake
  mbbFusion<double> * fusion_brake_torque_ankle_y = new mbbFusion<double>(this, "Control Ankle", 1, tStimulationMode::AUTO);
  fusion_brake_torque_ankle_y->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);

  // fusion of walking termination and push recovery for ankle brake
  mbbFusion<double> * fusion_ankle_brake = new mbbFusion<double>(this, "Control Ankle Fusion", 2, tStimulationMode::ENABLED);
  fusion_ankle_brake->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);

  // fusion lock knee
  mbbFusion<double>* fusion_lock_knee = new mbbFusion<double>(this, "Lock Knee", 2, tStimulationMode::ENABLED);
  fusion_lock_knee->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);

  // fusion leg propel
  mbbFusion<double> * fusion_leg_propel_lateral = new mbbFusion<double>(this,   "Leg Propel Lateral", 2, tStimulationMode::ENABLED);
  fusion_leg_propel_lateral->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);

  // fusion lift leg
  mbbFusion<double>* fusion_mp_lift_leg = new mbbFusion<double>(this,   "Lift Leg", 3, tStimulationMode::ENABLED);
  fusion_mp_lift_leg->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);

  // fusion hold knee for push recovery
  mbbFusion<double>* fusion_hold_knee = new mbbFusion<double>(this, "Hold Knee", 2, tStimulationMode::ENABLED);
  fusion_hold_knee->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);

  // fusion compensate ankle roll
  mbbFusion<double>* fusion_compensate_ankle_roll = new mbbFusion<double>(this, "Compensate Ankle Roll", 2, tStimulationMode::ENABLED);
  fusion_compensate_ankle_roll->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);

  // fusion lateral stiffness for push recovery
  mbbFusion<double>* fusion_lateral_stiffness_push_recovery_ankle = new mbbFusion<double>(this, "Lateral Stiffness Push Recovery", 2, tStimulationMode::ENABLED);
  fusion_lateral_stiffness_push_recovery_ankle->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);

  // fusion upright knee
//  mbbFusion<double>* fusion_upright_knee = new mbbFusion<double>(this, "Upright Knee Push Recovery", 1, tStimulationMode::ENABLED);
//  fusion_upright_knee->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);


  //END fusion patterns
  // Fusion of termination and walking
  mbbTerminationFusion* skill_walking_termi = new mbbTerminationFusion(this, "Walking & Termination");

  // STANDING CONTROL UNITS
  // ================================================
  mbbLegStand* skill_stand = new mbbLegStand(this, "(Ph) Stand");

  // hold position
  gbbLegHoldPosition* hold_position_group = new gbbLegHoldPosition(this, "(G) Hold Joint Pos. Leg");

  mbbAdjustJointAngle * relax_knee = new mbbAdjustJointAngle(this, "(PR) Relax Knee");
  mbbAdjustJointAngle * balance_ankle_pitch = new mbbAdjustJointAngle(this, "(R) Balance Ankle Pitch");
  mbbAdjustJointAngle * reduce_tension_ankle_y = new mbbAdjustJointAngle(this, "(PR) Reduce Tension Ankle Y");

  mbbReduceTensionAnkleX * reduce_tension_ankle_x = new mbbReduceTensionAnkleX(this, "(PR) Reduce Tension Ankle X");

  // Disturbance Estimation and Compensation
  // ================================================
  //mbbCompensationAnklePitch * skill_compensation_ankle_pitch = new mbbCompensationAnklePitch(this, " (PR) Compensation Ankle Pitch", tStimulationMode::DISABLED);

  mbbCompensationAnkleRoll * skill_compensation_ankle_roll = new mbbCompensationAnkleRoll(this, " (PR) Compensation Ankle Roll", tStimulationMode::DISABLED);

  mbbCompensationAnklePitch2 * skill_compensation_ankle_pitch2 = new mbbCompensationAnklePitch2(this, " (PR) Compensation Ankle Pitch2", tStimulationMode::DISABLED);

  mbbLateralAdjustJointAngle * lateral_adjust_joint_angle = new mbbLateralAdjustJointAngle(this, "(R) Lateral Adjust Joint Angle", tStimulationMode::DISABLED);

  mbbCompensationKnee * skill_compensation_knee = new mbbCompensationKnee(this, "(PR) Compensation Knee", tStimulationMode::DISABLED);

  // push recovery in lateral direction
  mbbLateralControlPushRecovery* lateral_stiffness_control_push_recovery = new mbbLateralControlPushRecovery(this, "(R) Push Recovery Lateral", tStimulationMode::DISABLED);


  // WALKING CONTROL UNITS
  // ================================================

  //walking termination
  gLegTermination* leg_termination  = new gLegTermination(this, "(G) Leg Termination");

  // walking initiation
  mbbStimulationSkill* skill_walking_init = new mbbStimulationSkill(this, "(Ph) Walking Init", tStimulationMode::AUTO, 0, 2);
  skill_walking_init->SetConfigNode("/Legs/phases/walking_initiation/");
  skill_walking_init->par_stimulation_factor[0].SetConfigEntry("knee_angle/stimulation_factor");
  skill_walking_init->par_min_angle[0].SetConfigEntry("knee_angle/min_angle");
  skill_walking_init->par_max_angle[0].SetConfigEntry("knee_angle/max_angle");
  skill_walking_init->par_target_angle[0].SetConfigEntry("knee_angle/target_angle");
  skill_walking_init->par_stimulation_factor[1].SetConfigEntry("ankle_y_torque/stimulation_factor");

  // lateral walking initiation
  mbbStimulationSkill* skill_walking_lateral_init = new mbbStimulationSkill(this,  "(Ph) Walking Lateral Init", tStimulationMode::AUTO, 0, 3);
  skill_walking_lateral_init->SetConfigNode("/Legs/phases/walking_lateral_initiation/");// fixme add value name change the path
  skill_walking_lateral_init->par_stimulation_factor[0].SetConfigEntry("walking_lateral_initiation/knee_angle");
  skill_walking_lateral_init->par_min_angle[0].SetConfigEntry("walking_lateral_initiation/knee_angle");
  skill_walking_lateral_init->par_max_angle[0].SetConfigEntry("walking_lateral_initiation/knee_angle");
  skill_walking_lateral_init->par_target_angle[0].SetConfigEntry("walking_lateral_initiation/knee_angle");
  skill_walking_lateral_init->par_stimulation_factor[1].SetConfigEntry("walking_lateral_initiation/ankle_x_torque");
  skill_walking_lateral_init->par_stimulation_factor[2].SetConfigEntry("walking_lateral_initiation/mp_lift_leg");

  // walking phases
  mbbStimulationSkill* skill_walking = new mbbStimulationSkill(this, "(Ph) Walking All Phases", tStimulationMode::AUTO, 0, 1);
  skill_walking->SetConfigNode("/Legs/phases/walking_all_phases/");
  skill_walking->par_stimulation_factor[0].SetConfigEntry("ankle_x_angle/stimulation_factor");
  skill_walking->par_min_angle[0].SetConfigEntry("ankle_x_angle/min_angle");
  skill_walking->par_max_angle[0].SetConfigEntry("ankle_x_angle/max_angle");
  skill_walking->par_target_angle[0].SetConfigEntry("ankle_x_angle/target_angle");

  mbbStimulationSkill* skill_walking_lateral = new mbbStimulationSkill(this, "(Ph) Walking Lateral All Phases", tStimulationMode::AUTO, 0, 2);
  skill_walking_lateral->SetConfigNode("/Legs/phases/walking_lateral_all_phases/");
  skill_walking_lateral->par_stimulation_factor[0].SetConfigEntry("walking_lateral_all_phases/ankle_y_angle");
  skill_walking_lateral->par_min_angle[0].SetConfigEntry("walking_lateral_all_phases/ankle_y_angle");
  skill_walking_lateral->par_max_angle[0].SetConfigEntry("walking_lateral_all_phases/ankle_y_angle");
  skill_walking_lateral->par_target_angle[0].SetConfigEntry("walking_lateral_all_phases/ankle_y_angle");

  mbbStimulationSkill* skill_walking_phase1 = new mbbStimulationSkill(this,  "(Ph) Walking Phase 1", tStimulationMode::AUTO, 0, 5);
  skill_walking_phase1->SetConfigNode("/Legs/phases/");
  skill_walking_phase1->par_stimulation_factor[0].SetConfigEntry("walking_phase_1/ankle_x_angle/stimulation_factor");
  skill_walking_phase1->par_min_angle[0].SetConfigEntry("walking_phase_1/ankle_x_angle/min_angle");
  skill_walking_phase1->par_max_angle[0].SetConfigEntry("walking_phase_1/ankle_x_angle/max_angle");
  skill_walking_phase1->par_target_angle[0].SetConfigEntry("walking_phase_1/ankle_x_angle/target_angle");
  skill_walking_phase1->par_stimulation_factor[1].SetConfigEntry("walking_phase_1/control_forward_velocity/stimulation_factor");
  skill_walking_phase1->par_stimulation_factor[2].SetConfigEntry("walking_phase_1/weight_acceptance/stimulation_factor");
  skill_walking_phase1->par_stimulation_factor[3].SetConfigEntry("walking_phase_1/lateral_balance_ankle/stimulation_factor");
  skill_walking_phase1->par_stimulation_factor[4].SetConfigEntry("walking_phase_1/control_ankle_pitch/stimulation_factor");

  mbbStimulationSkill* skill_walking_phase2 = new mbbStimulationSkill(this, "(Ph) Walking Phase 2", tStimulationMode::AUTO, 0, 5);
  skill_walking_phase2->SetConfigNode("/Legs/phases/");
  skill_walking_phase2->par_stimulation_factor[0].SetConfigEntry("walking_phase_2/knee_angle/stimulation_factor");
  skill_walking_phase2->par_min_angle[0].SetConfigEntry("walking_phase_2/knee_angle/min_angle");
  skill_walking_phase2->par_max_angle[0].SetConfigEntry("walking_phase_2/knee_angle/max_angle");
  skill_walking_phase2->par_target_angle[0].SetConfigEntry("walking_phase_2/knee_angle/target_angle");
  skill_walking_phase2->par_stimulation_factor[1].SetConfigEntry("walking_phase_2/mp_propel/stimulation_factor");
  skill_walking_phase2->par_stimulation_factor[2].SetConfigEntry("walking_phase_2/control_forward_velocity/stimulation_factor");
  skill_walking_phase2->par_stimulation_factor[3].SetConfigEntry("walking_phase_2/control_ankle_pitch/stimulation_factor");
  skill_walking_phase2->par_stimulation_factor[4].SetConfigEntry("walking_phase_2/latera_balance_ankle/stimulation_factor");

  mbbStimulationSkill* skill_walking_phase3 = new mbbStimulationSkill(this, "(Ph) Walking Phase 3", tStimulationMode::AUTO, 0, 2);
  skill_walking_phase3->SetConfigNode("/Legs/phases/");
  skill_walking_phase3->par_stimulation_factor[0].SetConfigEntry("walking_phase_3/knee_angle/stimulation_factor");
  skill_walking_phase3->par_min_angle[0].SetConfigEntry("walking_phase_3/knee_angle/min_angle");
  skill_walking_phase3->par_max_angle[0].SetConfigEntry("walking_phase_3/knee_angle/max_angle");
  skill_walking_phase3->par_target_angle[0].SetConfigEntry("walking_phase_3/knee_angle/target_angle");
  skill_walking_phase3->par_stimulation_factor[1].SetConfigEntry("walking_phase_3/mp_propel/stimulation_factor");

  mbbStimulationSkill* skill_walking_phase4 = new mbbStimulationSkill(this, "(Ph) Walking Phase 4", tStimulationMode::AUTO, 1, 5);
  skill_walking_phase4->SetConfigNode("/Legs/phases/");
  skill_walking_phase4->par_stimulation_factor[0].SetConfigEntry("walking_phase_4/knee_angle/stimulation_factor");
  skill_walking_phase4->par_min_angle[0].SetConfigEntry("walking_phase_4/knee_angle/min_angle");
  skill_walking_phase4->par_max_angle[0].SetConfigEntry("walking_phase_4/knee_angle/max_angle");
  skill_walking_phase4->par_target_angle[0].SetConfigEntry("walking_phase_4/knee_angle/target_angle");
  skill_walking_phase4->par_stimulation_factor[1].SetConfigEntry("walking_phase_4/ankle_y_angle/stimulation_factor");
  skill_walking_phase4->par_min_angle[1].SetConfigEntry("walking_phase_4/ankle_y_angle/min_angle");
  skill_walking_phase4->par_max_angle[1].SetConfigEntry("walking_phase_4/ankle_y_angle/max_angle");
  skill_walking_phase4->par_target_angle[1].SetConfigEntry("walking_phase_4/ankle_y_angle/target_angle");
  skill_walking_phase4->par_stimulation_factor[2].SetConfigEntry("walking_phase_4/lock_knee/stimulation_factor");
  skill_walking_phase4->par_stimulation_factor[3].SetConfigEntry("walking_phase_4/cutaneous_reflex/stimulation_factor");
  skill_walking_phase4->par_stimulation_factor[4].SetConfigEntry("walking_phase_4/lift_knee/stimulation_factor");
  skill_walking_phase4->number_of_inhibition_ports.Set(1);
  skill_walking_phase4->CheckStaticParameters();

  mbbStimulationSkill* skill_walking_phase5 = new mbbStimulationSkill(this, "(Ph) Walking Phase 5", tStimulationMode::AUTO, 0, 4);
  skill_walking_phase5->SetConfigNode("/Legs/phases/");
  skill_walking_phase5->par_stimulation_factor[0].SetConfigEntry("walking_phase_5/knee_angle/stimulation_factor");
  skill_walking_phase5->par_min_angle[0].SetConfigEntry("walking_phase_5/knee_angle/min_angle");
  skill_walking_phase5->par_max_angle[0].SetConfigEntry("walking_phase_5/knee_angle/max_angle");
  skill_walking_phase5->par_target_angle[0].SetConfigEntry("walking_phase_5/knee_angle/target_anle");
  skill_walking_phase5->par_stimulation_factor[1].SetConfigEntry("walking_phase_5/ankle_y_angle/stimulation_factor");
  skill_walking_phase5->par_min_angle[1].SetConfigEntry("walking_phase_5/ankle_y_angle/min_angle");
  skill_walking_phase5->par_max_angle[1].SetConfigEntry("walking_phase_5/ankle_y_angle/max_angle");
  skill_walking_phase5->par_target_angle[1].SetConfigEntry("walking_phase_5/ankle_y_angle/target_angle");
  skill_walking_phase5->par_stimulation_factor[2].SetConfigEntry("walking_phase_5/heel_strike/stimulation_factor");
  skill_walking_phase5->par_stimulation_factor[3].SetConfigEntry("walking_phase_5/weight_acceptance/stimulation_factor");

  mbbStimulationSkill* skill_walking_lateral_phase1 = new mbbStimulationSkill(this,  "(Ph) Walking Lateral Phase 1", tStimulationMode::AUTO, 0, 4);
  skill_walking_lateral_phase1->SetConfigNode("/Legs/phases/walking_lateral_phase_1/");// fixme
  skill_walking_lateral_phase1->par_stimulation_factor[0].SetConfigEntry("walking_lateral_phase_1/control_lateral_velocity");
  skill_walking_lateral_phase1->par_stimulation_factor[1].SetConfigEntry("walking_lateral_phase_1/weight_acceptance");
  skill_walking_lateral_phase1->par_stimulation_factor[2].SetConfigEntry("walking_lateral_phase_1/mp_ankle_inclination");
  skill_walking_lateral_phase1->par_stimulation_factor[3].SetConfigEntry("walking_lateral_phase_1/control_ankle_pitch");
  skill_walking_lateral_phase1->par_min_angle[3].SetConfigEntry("walking_lateral_phase_1/control_ankle_pitch");
  skill_walking_lateral_phase1->par_max_angle[3].SetConfigEntry("walking_lateral_phase_1/control_ankle_pitch");
  skill_walking_lateral_phase1->par_target_angle[3].SetConfigEntry("walking_lateral_phase_1/control_ankle_pitch");

  mbbStimulationSkill* skill_walking_lateral_phase2 = new mbbStimulationSkill(this, "(Ph) Walking Lateral Phase 2", tStimulationMode::AUTO, 0, 4);
  skill_walking_lateral_phase2->SetConfigNode("/Legs/phases/walking_lateral_phase_2/");
  skill_walking_lateral_phase2->par_stimulation_factor[0].SetConfigEntry("walking_lateral_phase_2/knee_angle");
  skill_walking_lateral_phase2->par_min_angle[0].SetConfigEntry("walking_lateral_phase_2/knee_angle");
  skill_walking_lateral_phase2->par_max_angle[0].SetConfigEntry("walking_lateral_phase_2/knee_angle");
  skill_walking_lateral_phase2->par_target_angle[0].SetConfigEntry("walking_lateral_phase_2/knee_angle");
  skill_walking_lateral_phase2->par_stimulation_factor[1].SetConfigEntry("walking_lateral_phase_2/control_latera_velocity");
  skill_walking_lateral_phase2->par_stimulation_factor[2].SetConfigEntry("walking_lateral_phase_2/mp_propel_lateral");
  skill_walking_lateral_phase2->par_stimulation_factor[3].SetConfigEntry("walking_lateral_phase_2/mp_lift_leg");


  mbbStimulationSkill* skill_walking_lateral_phase3 = new mbbStimulationSkill(this, "(Ph) Walking Lateral Phase 3", tStimulationMode::AUTO, 0, 4);
  skill_walking_lateral_phase3->SetConfigNode("/Legs/phases/walking_lateral_phase_3/");
  skill_walking_lateral_phase3->par_stimulation_factor[0].SetConfigEntry("walking_lateral_phase_3/control_knee_angle");
  skill_walking_lateral_phase3->par_stimulation_factor[1].SetConfigEntry("walking_lateral_phase_3/mp_propel");
  skill_walking_lateral_phase3->par_stimulation_factor[2].SetConfigEntry("walking_lateral_phase_3/mp_lift_leg");
  skill_walking_lateral_phase3->par_stimulation_factor[3].SetConfigEntry("walking_lateral_phase_3/mp_propel_lateral");


  mbbStimulationSkill* skill_walking_lateral_phase4 = new mbbStimulationSkill(this, "(Ph) Walking Lateral Phase 4", tStimulationMode::AUTO, 0, 4);
  skill_walking_lateral_phase4->SetConfigNode("/Legs/phases/walking_lateral_phase_4/");
  skill_walking_lateral_phase4->par_stimulation_factor[0].SetConfigEntry("walking_lateral_phase_4/lock_knee");
  skill_walking_lateral_phase4->par_min_angle[0].SetConfigEntry("walking_lateral_phase_4/lock_knee");
  skill_walking_lateral_phase4->par_max_angle[0].SetConfigEntry("walking_lateral_phase_4/lock_knee");
  skill_walking_lateral_phase4->par_target_angle[0].SetConfigEntry("walking_lateral_phase_4/lock_knee");
  skill_walking_lateral_phase4->par_stimulation_factor[1].SetConfigEntry("walking_lateral_phase_4/foot_contact");
  skill_walking_lateral_phase4->par_stimulation_factor[2].SetConfigEntry("walking_lateral_phase_4/weight_acceptance");
  skill_walking_lateral_phase4->par_stimulation_factor[3].SetConfigEntry("walking_lateral_phase_4/control_ankle_x");

  // lift leg
  mbbStimulationSkill* skill_lift_leg = new mbbStimulationSkill(this, "(Ph) Lift Leg", tStimulationMode::AUTO, 0, 3);
  skill_lift_leg->SetConfigNode("/Legs/phases/");
  skill_lift_leg->par_stimulation_factor[0].SetConfigEntry("lift_leg/knee_angle/stimulation_factor");
  skill_lift_leg->par_min_angle[0].SetConfigEntry("lift_leg/knee_angle/min_angle");
  skill_lift_leg->par_max_angle[0].SetConfigEntry("lift_leg/knee_angle/max_angle");
  skill_lift_leg->par_target_angle[0].SetConfigEntry("lift_leg/knee_angle/target_angle");
  skill_lift_leg->par_stimulation_factor[1].SetConfigEntry("lift_leg/ankle_y_angle/stimulation_factor");
  skill_lift_leg->par_min_angle[1].SetConfigEntry("lift_leg/ankle_y_angle/min_angle");
  skill_lift_leg->par_max_angle[1].SetConfigEntry("lift_leg/ankle_y_angle/max_angle");
  skill_lift_leg->par_target_angle[1].SetConfigEntry("lift_leg/ankle_y_angle/target_angle");
  skill_lift_leg->par_stimulation_factor[2].SetConfigEntry("lift_leg/mp_lift_leg/stimulation_factor");

  //END skills

  //BEGIN Reflexes:

  mbbControlForwardVelocityAnkle* control_forward_velocity_ankle = new mbbControlForwardVelocityAnkle(this, "PR Control Forward Velocity Ankle");

  mbbLateralBalanceAnkle* lateral_balance_ankle = new mbbLateralBalanceAnkle(this, "(PR) Lat. Balance Ankle");

  mbbFrontalBalanceAnkle* frontal_balance_ankle = new mbbFrontalBalanceAnkle(this, "(PR) Fron. Balance Ankle", tStimulationMode::DISABLED);

  mbbHeelStrikeReflex2 *heel_strike_reflex = new mbbHeelStrikeReflex2(this, "(R) Heel Strike");

  mbbLockKnee *lock_knee = new mbbLockKnee(this, "(R) Lock Knee", tStimulationMode::AUTO, 0, 0.2);

  mbbHoldKnee *hold_knee = new mbbHoldKnee(this, "R Hold Knee");

  mbbCutaneousReflex* cutaneous_reflex = new mbbCutaneousReflex(this, "(R) Cutaneous Reflex");

  mbbControlAnkleRollAngle* control_ankle_roll_angle = new mbbControlAnkleRollAngle(this, "(R) Control Ankle Roll");

  mbbAnkleBrakeTorque *ankle_brake = new mbbAnkleBrakeTorque(this, "(R) Ankle Brake", tStimulationMode::AUTO, 0, 0.55);

  mbbUprightKnee * upright_knee = new mbbUprightKnee(this, "R Upright Knee");
  // foot contact
  mbbFootContact * foot_contact = new mbbFootContact(this, "(R) Foot Contact");

  mbbKneeFlexion * knee_flexion = new mbbKneeFlexion(this, "(R) Knee Flexion");

  //END reflexes

  //BEGIN Motor Patterns

  // leg propel in straight walking
  mbbLegPropelSin *leg_propel_sin = new mbbLegPropelSin(this, "MP Leg Propel Sinoid", tStimulationMode::AUTO, 0, mMotorPattern::tCurveMode::eSIGMOID, 460, 0.48, 0.68); // todo:modified in Finroc 0.5 - 0.7
  mbbLegPropel *leg_propel = new mbbLegPropel(this, "(MP) Leg Propel", tStimulationMode::DISABLED, 0, 460, 0.5534, 176.268); //speed = 0.8 -> 460, 0.5, 200
  // lift leg
  mbbLegLiftLeg *mp_lift_leg = new mbbLegLiftLeg(this, "(MP) Lift Leg", tStimulationMode::AUTO, 0, mMotorPattern::tCurveMode::eSIGMOID, 500, 0.1, 0.5); // time duration

  mbbWeightAcceptanceMP *weight_accept_reflex = new mbbWeightAcceptanceMP(this, "(MP) Weight Acceptance", tStimulationMode::AUTO, 0,  mMotorPattern::tCurveMode::eSIGMOID, 700, 0.5, 0.5);

  // ankle x torque
  mbbSingleTorquePattern * ankle_x_torque = new mbbSingleTorquePattern(this, "(MP) Ankle X Torque", tStimulationMode::DISABLED, 0, mMotorPattern::tCurveMode::eSIGMOID, 400, 0.2, 0.8, 0.2);

  //END Motor Patterns


  //BEGIN Other modules:

  tConstDescription loadcell_filter_io[] = {"Force Z", "Torque X", "Torque Y", "Inner Toe Force", "Outer Toe Force", "Inner Heel Force", "Outer Heel Force"};
  enum
  {
    eLC_FILTER_FORCE_Z,
    eLC_FILTER_TORQUE_X,
    eLC_FILTER_TORQUE_Y,
    eLC_FILTER_INNER_TOE_FORCE,
    eLC_FILTER_OUTER_TOE_FORCE,
    eLC_FILTER_INNER_HEEL_FORCE,
    eLC_FILTER_OUTER_HEEL_FORCE
  };

  mAverageFilter* loadcell_filter = new mAverageFilter(this, "Filter Loadcell", 7, 3, mAverageFilter::eAFM_EXACT, mAverageFilter::eSISO, true, loadcell_filter_io);


  mFootLoad* foot_load = new mFootLoad(this, "Foot Load");

  mCenterOfPressure* center_of_pressure = new mCenterOfPressure(this, "Center of Pressure");


  // ==========================================
  // add connections
  // ==========================================
  // controller log
  /*
  AddEdgeUp(leg_co_log_bb, this, 1, mScalarLogManager::eSO_LOG_CHANGED, eSO_LEG_CO_LOG_UPDATED);
  AddEdgeDown(this, leg_co_log_bb, 1, eCI_SENSOR_MONITOR, mScalarLogManager::eCI_LOG_REQUEST);
  int index = 2;
  for (unsigned int i = 0; i < module_list.size(); i++)
  {
    AddEdgeDown(module_list[i], leg_co_log_bb, module_list[i]->ControllerOutputDimension(), 0, index);
    index += module_list[i]->ControllerOutputDimension();
  }
  */
  //BEGIN other modules

  // ---------------------------------------------------------------------------------------------
  this->si_force_z.ConnectTo(loadcell_filter->si_signal[0]);
  this->si_torque_x.ConnectTo(loadcell_filter->si_signal[1]);
  this->si_torque_y.ConnectTo(loadcell_filter->si_signal[2]);
  this->si_inner_toe_force.ConnectTo(loadcell_filter->si_signal[3]);
  this->si_outer_toe_force.ConnectTo(loadcell_filter->si_signal[4]);
  this->si_inner_heel_force.ConnectTo(loadcell_filter->si_signal[5]);
  this->si_outer_heel_force.ConnectTo(loadcell_filter->si_signal[6]);

  this->ci_reset_simulation_experiments.ConnectTo(foot_load->ci_reset_simulation_experiments);
  loadcell_filter->so_signal[0].ConnectTo(foot_load->si_force_z);
  loadcell_filter->so_signal[1].ConnectTo(foot_load->si_torque_x);
  loadcell_filter->so_signal[2].ConnectTo(foot_load->si_torque_y);
  loadcell_filter->so_signal[3].ConnectTo(foot_load->si_inner_toe_force);
  loadcell_filter->so_signal[4].ConnectTo(foot_load->si_outer_toe_force);
  loadcell_filter->so_signal[5].ConnectTo(foot_load->si_inner_heel_force);
  loadcell_filter->so_signal[6].ConnectTo(foot_load->si_outer_heel_force);

//  this->si_force_z.ConnectTo(foot_load->si_force_z);
//  this->si_torque_x.ConnectTo(foot_load->si_torque_x);
//  this->si_torque_y.ConnectTo(foot_load->si_torque_y);
//  this->si_inner_toe_force.ConnectTo(foot_load->si_inner_toe_force);
//  this->si_outer_toe_force.ConnectTo(foot_load->si_outer_toe_force);
//  this->si_inner_heel_force.ConnectTo(foot_load->si_inner_heel_force);
//  this->si_outer_heel_force.ConnectTo(foot_load->si_outer_heel_force);

  foot_load->so_foot_load.ConnectTo(this->so_foot_load);
  foot_load->so_ground_contact.ConnectTo(this->so_ground_contact);

  this->si_inner_toe_force.ConnectTo(center_of_pressure->si_inner_toe_force);
  this->si_outer_toe_force.ConnectTo(center_of_pressure->si_outer_toe_force);
  this->si_inner_heel_force.ConnectTo(center_of_pressure->si_inner_heel_force);
  this->si_outer_heel_force.ConnectTo(center_of_pressure->si_outer_heel_force);

  center_of_pressure->so_relative_center_of_pressure_x.ConnectTo(this->so_force_deviation);
  center_of_pressure->so_center_of_pressure_x.ConnectTo(this->so_cop_x);
  center_of_pressure->so_center_of_pressure_y.ConnectTo(this->so_cop_y);
  //END other modules

  //BEGIN learning
  // ---------------------------------------------------------------------------------------------
  /*
  #ifdef ENABLE_LEARNING

  AddEdgeDown(this, forward_velocity_ankle, 2,
              eCI_STIMULATION_INITIAL_FORWARD_VELOCITY, mbbLegInitialForwardVelocityAnkle::eCI_STIMULATION,
              eCI_INITIAL_FORWARD_VELOCITY_ANKLE, mbbLegInitialForwardVelocityAnkle::eCI_ANKLE_TORQUE);
  index = fusion_abs_torque_ankle_y->RegisterControllingBehaviour(forward_velocity_ankle->Description());
  AddEdgeDown(forward_velocity_ankle, fusion_abs_torque_ankle_y, 3,
              mbbLegInitialForwardVelocityAnkle::eCO_ACTIVITY, index,
              mbbLegInitialForwardVelocityAnkle::eCO_TARGET_RATING, index + 1,
              mbbLegInitialForwardVelocityAnkle::eCO_ANKLE_TORQUE, index + 2);

  //  AddEdgeDownByName(skill_walking_init, forward_velocity_ankle, false, 1,
  //                    "Stimulation MP Forward", mbbForwardVelocityAnkleLearning::eCI_STIMULATION);
  //  AddEdgeDownByName(forward_velocity_ankle, learning_forward_velocity_ankle, false, 3,
  //                    mbbForwardVelocityAnkleLearning::eCO_ACTIVITY, "Activity",
  //                    mbbForwardVelocityAnkleLearning::eCO_TARGET_RATING, "Target Rating",
  //                    mbbForwardVelocityAnkleLearning::eCO_ANKLE_TORQUE, "Ankle Torque");
  //  AddEdgeDown(this, learning_forward_velocity_ankle, 2,
  //              eCI_LEARNING_ACTIVE, mLearning::eCI_ACTIVE,
  //              eCI_LEARNING_GLOBAL_RATING, mLearning::eCI_GLOBAL_RATING);
  //  AddEdgeDownByName(this, learning_forward_velocity_ankle, false, 2,
  //                    eCI_ACTUAL_PITCH, "INS Pitch",
  //                    eCI_LINEAR_VELOCITY_X, "Linear Velocity X");
  //  index = fusion_abs_torque_ankle_y->RegisterControllingBehaviour(learning_forward_velocity_ankle->Description());
  //  AddEdgeDownByName(learning_forward_velocity_ankle, fusion_abs_torque_ankle_y, false, 3,
  //                    "Activity", index,
  //                    "Target Rating", index + 1,
  //                    "Ankle Torque", index + 2);
  //
  //  AddEdgeUp(foot_load, forward_velocity_ankle, 1,
  //            mFootLoad::eSO_GROUND_CONTACT, mbbForwardVelocityAnkleLearning::eSI_GROUND_CONTACT);
  //  AddEdgeUp(this, forward_velocity_ankle, 1,
  //            eSI_KNEE_ANGLE, mbbForwardVelocityAnkleLearning::eSI_KNEE_ANGLE);

  #endif
  //END learning
    */

  //BEGIN fusion behaviours
  // -------------------------------------------------------------------------------------------

  // inhibit position by torque
  fusion_abs_torque_knee->activity.ConnectTo(memory_abs_torque_knee->in_activity);
  memory_abs_torque_knee->out_activity.ConnectTo(fusion_abs_angle_knee->inhibition[0]);

  fusion_abs_torque_ankle_y->activity.ConnectTo(memory_abs_torque_ankle_y->in_activity);
  memory_abs_torque_ankle_y->out_activity.ConnectTo(fusion_abs_angle_ankle_y->inhibition[0]);

  fusion_abs_torque_ankle_x->activity.ConnectTo(memory_abs_torque_ankle_x->in_activity);
  memory_abs_torque_ankle_x->out_activity.ConnectTo(fusion_abs_angle_ankle_x->inhibition[0]);
  // fusion forward velocity
  fusion_forward_velocity->activity.ConnectTo(control_forward_velocity_ankle->stimulation);

  // fusion lateral balance ankle
  fusion_lateral_balance_ankle->activity.ConnectTo(lateral_balance_ankle->stimulation);

  // fusion frontal balance ankle
  fusion_lateral_balance_ankle->activity.ConnectTo(frontal_balance_ankle->stimulation);

  // fusion leg propel
  fusion_leg_propel->activity.ConnectTo(leg_propel->stimulation);
  fusion_leg_propel->activity.ConnectTo(leg_propel_sin->stimulation);

  // fusion weight accept
  fusion_weight_accept->activity.ConnectTo(weight_accept_reflex->stimulation);

  // fusion heel strike
  fusion_heel_strike->activity.ConnectTo(heel_strike_reflex->stimulation);

  // fusion control ankle pitch
  fusion_control_ankle_roll->activity.ConnectTo(control_ankle_roll_angle->stimulation);

  // fusion ankle brake
  this->ci_stimulation_termination.ConnectTo(fusion_ankle_brake->InputActivity(0));
  hold_position_group->co_activity_ankle_x.ConnectTo(fusion_ankle_brake->InputPort(0, 0));
  this->ci_stimulation_termination.ConnectTo(fusion_ankle_brake->InputTargetRating(0));


  this->ci_push_recovery_stand.ConnectTo(fusion_ankle_brake->InputActivity(1));
  hold_position_group->co_activity_ankle_x.ConnectTo(fusion_ankle_brake->InputPort(1, 0));
  this->ci_push_recovery_stand.ConnectTo(fusion_ankle_brake->InputTargetRating(1));

  fusion_ankle_brake->activity.ConnectTo(fusion_brake_torque_ankle_y->stimulation);
  fusion_brake_torque_ankle_y->activity.ConnectTo(ankle_brake->stimulation);

  // fusion hold knee
  fusion_hold_knee->activity.ConnectTo(hold_knee->stimulation);
  //END fusion behaviours

  //BEGIN reflexes
  // ---------------------------------------------------------------------------------------------

  // termination and walking
  this->ci_activity_walking.ConnectTo(skill_walking_termi->stimulation);
  this->ci_last_step_state.ConnectTo(skill_walking_termi->ci_last_step_state);
  this->ci_stimulation_walking_phase_1.ConnectTo(skill_walking_termi->ci_walking_phase_1);
  this->ci_stimulation_walking_phase_2.ConnectTo(skill_walking_termi->ci_walking_phase_2);
  this->ci_stimulation_walking_phase_3.ConnectTo(skill_walking_termi->ci_walking_phase_3);
  this->ci_stimulation_walking_phase_4.ConnectTo(skill_walking_termi->ci_walking_phase_4);
  this->ci_stimulation_walking_phase_5.ConnectTo(skill_walking_termi->ci_walking_phase_5);

  skill_walking_termi->co_termination_phase_1.ConnectTo(leg_termination->ci_termination_phase_1);
  skill_walking_termi->co_termination_phase_2.ConnectTo(leg_termination->ci_termination_phase_2);
  skill_walking_termi->co_termination_phase_3.ConnectTo(leg_termination->ci_termination_phase_3);
  skill_walking_termi->co_termination_phase_4.ConnectTo(leg_termination->ci_termination_phase_4);
  skill_walking_termi->co_termination_phase_5.ConnectTo(leg_termination->ci_termination_phase_5);

  //this->ci_stimulation_walking_phase_5.ConnectTo(skill_walking_phase5->stimulation);//fixme
  // heel strike reflex
  this->si_ankle_y_angle.ConnectTo(heel_strike_reflex->si_ankle_y_angle);
  this->si_inner_toe_force.ConnectTo(heel_strike_reflex->si_inner_toe_force);
  this->si_outer_toe_force.ConnectTo(heel_strike_reflex->si_outer_toe_force);
  this->si_inner_heel_force.ConnectTo(heel_strike_reflex->si_inner_heel_force);
  this->si_outer_heel_force.ConnectTo(heel_strike_reflex->si_outer_heel_force);

  heel_strike_reflex->co_stimulation_ankle_y_stiffness.ConnectTo(fusion_abs_angle_ankle_y->InputActivity(0));
  heel_strike_reflex->target_rating.ConnectTo(fusion_abs_angle_ankle_y->InputTargetRating(0));
  heel_strike_reflex->co_ankle_y_angle.ConnectTo(fusion_abs_angle_ankle_y->InputPort(0, 0));

  // weight acceptance reflex
  this->ci_walking_scale_factor.ConnectTo(weight_accept_reflex->ci_scale);

  this->si_knee_angle.ConnectTo(weight_accept_reflex->si_knee_angle);
  //this->si_inner_heel_force.ConnectTo(weight_accept_reflex->si_inner_heel_force);
  //this->si_outer_heel_force.ConnectTo(weight_accept_reflex->si_outer_heel_force);

  weight_accept_reflex->co_stimulation_knee_angle.ConnectTo(fusion_abs_angle_knee->InputActivity(0));
  weight_accept_reflex->target_rating.ConnectTo(fusion_abs_angle_knee->InputTargetRating(0));
  weight_accept_reflex->co_knee_angle.ConnectTo(fusion_abs_angle_knee->InputPort(0, 0));



  // lock knee reflex
  fusion_lock_knee->activity.ConnectTo(lock_knee->stimulation);
  this->si_knee_angle.ConnectTo(lock_knee->si_knee_angle);
  this->ci_stimulation_walking_initiation.ConnectTo(lock_knee->ci_stimulation_init);
  foot_load->so_ground_contact.ConnectTo(lock_knee->si_ground_contact);
  lock_knee->activity.ConnectTo(fusion_abs_angle_knee->InputActivity(1));
  lock_knee->target_rating.ConnectTo(fusion_abs_angle_knee->InputTargetRating(1));
  lock_knee->co_knee_angle.ConnectTo(fusion_abs_angle_knee->InputPort(1, 0));

  // hold knee reflex -- TODO
  // turn on when knee strategy used
  this->ci_com_error_x.ConnectTo(hold_knee->ci_com_x_error);
  this->ci_forward_velocity_correction.ConnectTo(hold_knee->ci_xcom_x_correction);
  this->ci_push_recovery_walking.ConnectTo(hold_knee->ci_push_recovery_x);
  this->ci_learned_stiffness_ankle_push_recovery.ConnectTo(hold_knee->ci_learned_stiffness); // knee strategy
  this->ci_push_started.ConnectTo(hold_knee->ci_learning_on);

  foot_load->so_ground_contact.ConnectTo(hold_knee->si_ground_contact);
  this->si_knee_angle.ConnectTo(hold_knee->si_knee_angle);
  hold_knee->activity.ConnectTo(fusion_abs_angle_knee->InputActivity(2));
  hold_knee->target_rating.ConnectTo(fusion_abs_angle_knee->InputTargetRating(2));
  hold_knee->co_knee_angle.ConnectTo(fusion_abs_angle_knee->InputPort(2, 0));

  // upright knee reflex --> turn on when knee strategy used
  this->ci_walking_scale_factor.ConnectTo(upright_knee->ci_scale);
  this->ci_bent_knee.ConnectTo(upright_knee->ci_bent_knee);
  this->ci_learned_stiffness_ankle_push_recovery.ConnectTo(upright_knee->ci_learned_stiffness); // knee strategy
  this->ci_push_started.ConnectTo(upright_knee->ci_learning_on); // knee strategy
  this->si_knee_angle.ConnectTo(upright_knee->si_knee_angle);
  this->si_ankle_y_angle.ConnectTo(upright_knee->si_ankle_angle);
  foot_load->so_ground_contact.ConnectTo(upright_knee->si_ground_contact);

  skill_walking_phase1->activity.ConnectTo(upright_knee->stimulation);

  upright_knee->activity.ConnectTo(fusion_abs_angle_knee->InputActivity(3));
  upright_knee->target_rating.ConnectTo(fusion_abs_angle_knee->InputTargetRating(3));
  upright_knee->co_knee_angle.ConnectTo(fusion_abs_angle_knee->InputPort(3, 0));

  // foot contact reflex
  this->si_ankle_x_angle.ConnectTo(foot_contact->si_ankle_x_angle);
  this->si_inner_toe_force.ConnectTo(foot_contact->si_inner_toe_force);
  this->si_outer_toe_force.ConnectTo(foot_contact->si_outer_toe_force);
  this->si_inner_heel_force.ConnectTo(foot_contact->si_inner_heel_force);
  this->si_outer_heel_force.ConnectTo(foot_contact->si_outer_heel_force);

  foot_contact->co_stimulation_ankle_x_stiffness.ConnectTo(fusion_abs_angle_ankle_x->InputActivity(0));
  foot_contact->target_rating.ConnectTo(fusion_abs_angle_ankle_x->InputTargetRating(0));
  foot_contact->co_ankle_x_angle.ConnectTo(fusion_abs_angle_ankle_x->InputPort(0, 0));

  foot_contact->co_stimulation_ankle_x_torque.ConnectTo(fusion_abs_torque_ankle_x->InputActivity(0));
  foot_contact->co_stimulation_ankle_x_torque.ConnectTo(fusion_abs_torque_ankle_x->InputTargetRating(0));
  foot_contact->co_ankle_x_torque.ConnectTo(fusion_abs_torque_ankle_x->InputPort(0, 0));

  // knee flexion reflex
  fusion_lock_knee->activity.ConnectTo(knee_flexion->stimulation);
  this->ci_obstacle_walking_stimulation.ConnectTo(knee_flexion->ci_obstacle_walking_stimulation);
  this->ci_obstacle_step.ConnectTo(knee_flexion->ci_obstacle_step);
  this->ci_obstacle_knee_flexion_1.ConnectTo(knee_flexion->ci_obstacle_knee_flexion_1);
  this->ci_obstacle_knee_flexion_2.ConnectTo(knee_flexion->ci_obstacle_knee_flexion_2);
  this->ci_obstacle_knee_flexion_2_rear.ConnectTo(knee_flexion->ci_obstacle_knee_flexion_2_rear);
  this->si_swing_leg_hip_y.ConnectTo(knee_flexion->si_swing_hip_y_angle);
  this->si_stance_leg_hip_y.ConnectTo(knee_flexion->si_stance_hip_y_angle);
  this->si_foot_left_position_x.ConnectTo(knee_flexion->si_foot_left_position_x);
  this->si_foot_left_position_y.ConnectTo(knee_flexion->si_foot_left_position_y);
  this->si_foot_left_position_z.ConnectTo(knee_flexion->si_foot_left_position_z);
  this->si_foot_right_position_x.ConnectTo(knee_flexion->si_foot_right_position_x);
  this->si_foot_right_position_y.ConnectTo(knee_flexion->si_foot_right_position_y);
  this->si_foot_right_position_z.ConnectTo(knee_flexion->si_foot_right_position_z);

//  this->ci_stimulation_walking_initiation.ConnectTo(knee_flexion->ci_stimulation_init);
  foot_load->so_ground_contact.ConnectTo(knee_flexion->si_ground_contact);
  knee_flexion->activity.ConnectTo(fusion_abs_angle_knee->InputActivity(13));
  knee_flexion->target_rating.ConnectTo(fusion_abs_angle_knee->InputTargetRating(13));
  knee_flexion->co_knee_angle.ConnectTo(fusion_abs_angle_knee->InputPort(13, 0));
//
//  knee_flexion->activity.ConnectTo(fusion_abs_torque_knee->InputActivity(4));
//  knee_flexion->target_rating.ConnectTo(fusion_abs_torque_knee->InputTargetRating(4));
//  knee_flexion->co_knee_torque.ConnectTo(fusion_abs_torque_knee->InputPort(4, 0));

  // hold joint position group
  skill_stand->activity.ConnectTo(hold_position_group->stimulation);
  this->ci_stiffness_factor.ConnectTo(hold_position_group->ci_stiffness_factor);
  this->ci_ground_contact.ConnectTo(hold_position_group->ci_ground_contact);
  this->ci_reset_simulation_experiments.ConnectTo(hold_position_group->ci_reset_simulation_experiments);
  this->ci_stimulation_termination_button.ConnectTo(hold_position_group->ci_stimulation_termination_button);

  fusion_rel_angle_ankle_x->activity.ConnectTo(hold_position_group->ci_stimulation_angle_adjustment_ankle_x);
  fusion_rel_angle_ankle_x->OutputPort(0).ConnectTo(hold_position_group->ci_angle_adjustment_ankle_x);

  fusion_rel_angle_ankle_y->activity.ConnectTo(hold_position_group->ci_stimulation_angle_adjustment_ankle_y);
  fusion_rel_angle_ankle_y->OutputPort(0).ConnectTo(hold_position_group->ci_angle_adjustment_ankle_y);

  fusion_rel_angle_knee->activity.ConnectTo(hold_position_group->ci_stimulation_angle_adjustment_knee);
  fusion_rel_angle_knee->OutputPort(0).ConnectTo(hold_position_group->ci_angle_adjustment_knee);

  this->si_ankle_x_angle.ConnectTo(hold_position_group->si_ankle_x_angle);
  this->si_ankle_y_angle.ConnectTo(hold_position_group->si_ankle_y_angle);
  this->si_knee_angle.ConnectTo(hold_position_group->si_knee_angle);

  hold_position_group->co_activity_ankle_x.ConnectTo(fusion_abs_angle_ankle_x->InputActivity(1));
  hold_position_group->target_rating.ConnectTo(fusion_abs_angle_ankle_x->InputTargetRating(1));
  hold_position_group->co_angle_ankle_x.ConnectTo(fusion_abs_angle_ankle_x->InputPort(1, 0));

  hold_position_group->co_activity_ankle_y.ConnectTo(fusion_abs_angle_ankle_y->InputActivity(1));
  hold_position_group->target_rating.ConnectTo(fusion_abs_angle_ankle_y->InputTargetRating(1));
  hold_position_group->co_angle_ankle_y.ConnectTo(fusion_abs_angle_ankle_y->InputPort(1, 0));

  hold_position_group->co_activity_knee.ConnectTo(fusion_abs_angle_knee->InputActivity(4));
  hold_position_group->target_rating.ConnectTo(fusion_abs_angle_knee->InputTargetRating(4));
  hold_position_group->co_angle_knee.ConnectTo(fusion_abs_angle_knee->InputPort(4, 0));

  // relax knee
  this->ci_stimulation_relax_knee.ConnectTo(relax_knee->stimulation);
  this->ci_relax_knee_angle_adjustment.ConnectTo(relax_knee->ci_angle_adjustment);
  relax_knee->activity.ConnectTo(fusion_rel_angle_knee->InputActivity(0));
  relax_knee->target_rating.ConnectTo(fusion_rel_angle_knee->InputTargetRating(0));
  relax_knee->co_angle_adjustment.ConnectTo(fusion_rel_angle_knee->InputPort(0, 0));

  // balance ankle pitch
  this->ci_stimulation_balance_ankle_pitch.ConnectTo(balance_ankle_pitch->stimulation);
  this->ci_balance_ankle_pitch_angle_adjustment.ConnectTo(balance_ankle_pitch->ci_angle_adjustment);
  balance_ankle_pitch->activity.ConnectTo(fusion_rel_angle_ankle_y->InputActivity(0));
  balance_ankle_pitch->target_rating.ConnectTo(fusion_rel_angle_ankle_y->InputTargetRating(0));
  balance_ankle_pitch->co_angle_adjustment.ConnectTo(fusion_rel_angle_ankle_y->InputPort(0, 0));

  // reduce tension ankle x
  this->ci_quiet_stand.ConnectTo(reduce_tension_ankle_x->ci_quiet_stand);
  this->ci_ground_contact.ConnectTo(reduce_tension_ankle_x->ci_ground_contact);
  this->ci_reset_simulation_experiments.ConnectTo(reduce_tension_ankle_x->ci_reset_simulation_experiments);

  skill_stand->activity.ConnectTo(reduce_tension_ankle_x->stimulation);
  reduce_tension_ankle_x->activity.ConnectTo(fusion_rel_angle_ankle_x->InputActivity(0));
  reduce_tension_ankle_x->target_rating.ConnectTo(fusion_rel_angle_ankle_x->InputTargetRating(0));//fixme
  reduce_tension_ankle_x->co_angle_adjustment.ConnectTo(fusion_rel_angle_ankle_x->InputPort(0, 0));

  this->si_inner_toe_force.ConnectTo(reduce_tension_ankle_x->si_inner_toe_force);
  this->si_outer_toe_force.ConnectTo(reduce_tension_ankle_x->si_outer_toe_force);
  this->si_inner_heel_force.ConnectTo(reduce_tension_ankle_x->si_inner_heel_force);
  this->si_outer_heel_force.ConnectTo(reduce_tension_ankle_x->si_outer_heel_force);

  // reduce tension ankle y
  this->ci_stimulation_reduce_tension_ankle_y.ConnectTo(reduce_tension_ankle_y->stimulation);
  this->ci_reduce_tension_ankle_y_angle_adjustment.ConnectTo(reduce_tension_ankle_y->ci_angle_adjustment);
  reduce_tension_ankle_y->activity.ConnectTo(fusion_rel_angle_ankle_y->InputActivity(1));
  reduce_tension_ankle_y->target_rating.ConnectTo(fusion_rel_angle_ankle_y->InputTargetRating(1));
  reduce_tension_ankle_y->co_angle_adjustment.ConnectTo(fusion_rel_angle_ankle_y->InputPort(1, 0));

  // control forward velocity ankle and toe
  this->ci_forward_velocity_correction.ConnectTo(control_forward_velocity_ankle->ci_scale);
  this->ci_push_recovery_stand.ConnectTo(control_forward_velocity_ankle->ci_push_recovery_stand);
  this->ci_push_recovery_walking.ConnectTo(control_forward_velocity_ankle->ci_push_recovery_walking_x);
  this->ci_last_step.ConnectTo(control_forward_velocity_ankle->ci_last_step);
  this->ci_walking_scale_factor.ConnectTo(control_forward_velocity_ankle->ci_walking_speed);
  this->ci_loop_times.ConnectTo(control_forward_velocity_ankle->ci_loop_times);
  this->si_ankle_y_angle.ConnectTo(control_forward_velocity_ankle->si_ankle_angle);

  control_forward_velocity_ankle->activity.ConnectTo(fusion_abs_torque_ankle_y->InputActivity(0));
  control_forward_velocity_ankle->activity.ConnectTo(fusion_abs_torque_ankle_y->InputTargetRating(0));
  control_forward_velocity_ankle->co_ankle_torque.ConnectTo(fusion_abs_torque_ankle_y->InputPort(0, 0));

  control_forward_velocity_ankle->co_stimulation_ankle_angle.ConnectTo(fusion_abs_angle_ankle_y->InputActivity(5));
  control_forward_velocity_ankle->activity.ConnectTo(fusion_abs_angle_ankle_y->InputTargetRating(5));
  control_forward_velocity_ankle->co_ankle_angle.ConnectTo(fusion_abs_angle_ankle_y->InputPort(5, 0));

  // lateral balance ankle
  this->ci_lateral_velocity_correction.ConnectTo(lateral_balance_ankle->ci_com_error_y);
  this->ci_walking_scale_factor.ConnectTo(lateral_balance_ankle->ci_walking_scale);
  this->si_force_z.ConnectTo(lateral_balance_ankle->si_force_z);
  this->si_ankle_x_angle.ConnectTo(lateral_balance_ankle->si_ankle_x_angle);
  this->ci_reset_simulation_experiments.ConnectTo(lateral_balance_ankle->ci_reset_simulation_experiments);
  this->ci_angle_body_space_lateral.ConnectTo(lateral_balance_ankle->ci_estim_com_y);
  this->ci_learned_stiffness_ankle_push_recovery.ConnectTo(lateral_balance_ankle->ci_torque_activity);// learning input
  foot_load->so_foot_load.ConnectTo(lateral_balance_ankle->si_ground_contact);
  this->ci_turn_left_control_signal.ConnectTo(lateral_balance_ankle->ci_turn_left_signal);
  this->ci_turn_right_control_signal.ConnectTo(lateral_balance_ankle->ci_turn_right_signal);
  this->co_slow_walking.ConnectTo(lateral_balance_ankle->ci_slow_walking);
  this->co_fast_walking.ConnectTo(lateral_balance_ankle->ci_fast_walking);


  lateral_balance_ankle->co_activity_torque.ConnectTo(fusion_abs_torque_ankle_x->InputActivity(1));
  lateral_balance_ankle->co_activity_torque.ConnectTo(fusion_abs_torque_ankle_x->InputTargetRating(1));//fixme
  lateral_balance_ankle->co_ankle_x_torque.ConnectTo(fusion_abs_torque_ankle_x->InputPort(1, 0));

  // position control->don't need now:fixme
//  lateral_balance_ankle->co_activity_angle.ConnectTo(fusion_abs_angle_ankle_x->InputActivity(7));
//  lateral_balance_ankle->activity.ConnectTo(fusion_abs_angle_ankle_x->InputTargetRating(7));//fixme
//  lateral_balance_ankle->co_ankle_x_angle.ConnectTo(fusion_abs_angle_ankle_x->InputPort(7, 0));

  // frontal balance ankle
  this->ci_frontal_velocity_correction.ConnectTo(frontal_balance_ankle->ci_xcom_error_y);
  this->ci_ground_contact.ConnectTo(frontal_balance_ankle->ci_ground_contact);
  this->si_force_z.ConnectTo(frontal_balance_ankle->si_force_z);

  frontal_balance_ankle->activity.ConnectTo(fusion_abs_torque_ankle_x->InputActivity(2));
  frontal_balance_ankle->activity.ConnectTo(fusion_abs_torque_ankle_x->InputTargetRating(2));
  frontal_balance_ankle->co_ankle_x_torque.ConnectTo(fusion_abs_torque_ankle_x->InputPort(2, 0));

  // cutaneous reflex
  this->ci_walking_scale_factor.ConnectTo(cutaneous_reflex->ci_walking_scale);
  this->ci_walking_scale_laterally.ConnectTo(cutaneous_reflex->ci_lateral_scale);

  this->si_inner_toe_force.ConnectTo(cutaneous_reflex->si_inner_toe_force);
  this->si_outer_toe_force.ConnectTo(cutaneous_reflex->si_outer_toe_force);
  this->si_inner_heel_force.ConnectTo(cutaneous_reflex->si_inner_heel_force);
  this->si_outer_heel_force.ConnectTo(cutaneous_reflex->si_outer_heel_force);

  cutaneous_reflex->activity.ConnectTo(fusion_abs_torque_ankle_y->InputActivity(1));
  cutaneous_reflex->activity.ConnectTo(fusion_abs_torque_ankle_y->InputTargetRating(1));
  cutaneous_reflex->co_ankle_y_torque.ConnectTo(fusion_abs_torque_ankle_y->InputPort(1, 0));

  cutaneous_reflex->activity.ConnectTo(fusion_abs_torque_ankle_x->InputActivity(3));
  cutaneous_reflex->activity.ConnectTo(fusion_abs_torque_ankle_x->InputTargetRating(3));
  cutaneous_reflex->co_ankle_x_torque.ConnectTo(fusion_abs_torque_ankle_x->InputPort(3, 0));

  cutaneous_reflex->activity.ConnectTo(fusion_abs_torque_knee->InputActivity(0));
  cutaneous_reflex->co_knee_torque.ConnectTo(fusion_abs_torque_knee->InputTargetRating(0));
  cutaneous_reflex->co_knee_torque.ConnectTo(fusion_abs_torque_knee->InputPort(0, 0));

  // control ankle pitch angle
  this->ci_turn_left_control_signal.ConnectTo(control_ankle_roll_angle->ci_turn_left_signal);
  this->ci_turn_right_control_signal.ConnectTo(control_ankle_roll_angle->ci_turn_right_signal);
  this->ci_com_error_y.ConnectTo(control_ankle_roll_angle->ci_com_error_y);
  this->ci_ground_contact.ConnectTo(control_ankle_roll_angle->ci_ground_contact);

  this->si_ankle_x_angle.ConnectTo(control_ankle_roll_angle->si_ankle_roll_angle);

  control_ankle_roll_angle->co_stimulation_ankle_roll_angle.ConnectTo(fusion_abs_angle_ankle_x->InputActivity(2));
  control_ankle_roll_angle->target_rating.ConnectTo(fusion_abs_angle_ankle_x->InputTargetRating(2));
  control_ankle_roll_angle->co_ankle_roll_angle.ConnectTo(fusion_abs_angle_ankle_x->InputPort(2, 0));

  control_ankle_roll_angle->co_stimulation_ankle_roll_torque.ConnectTo(fusion_abs_torque_ankle_x->InputActivity(4));
  control_ankle_roll_angle->co_stimulation_ankle_roll_torque.ConnectTo(fusion_abs_torque_ankle_x->InputTargetRating(4));
  control_ankle_roll_angle->co_ankle_roll_torque.ConnectTo(fusion_abs_torque_ankle_x->InputPort(4, 0));

  // ankle brake during push recovery
  this->ci_s_leg.ConnectTo(ankle_brake->ci_s_leg);
  this->ci_maintain_stable.ConnectTo(ankle_brake->ci_maintain_stable);
  this->ci_com_error_y.ConnectTo(ankle_brake->ci_com_y_error);

  this->si_inner_toe_force.ConnectTo(ankle_brake->si_inner_toe_force);
  this->si_outer_toe_force.ConnectTo(ankle_brake->si_outer_toe_force);
  this->si_inner_heel_force.ConnectTo(ankle_brake->si_inner_heel_force);
  this->si_outer_heel_force.ConnectTo(ankle_brake->si_outer_heel_force);
  this->si_ankle_y_angle.ConnectTo(ankle_brake->si_angle_ankle_y);
  this->si_ankle_x_angle.ConnectTo(ankle_brake->si_angle_ankle_x);

  ankle_brake->co_stimulation_y_torque.ConnectTo(fusion_abs_torque_ankle_y->InputActivity(2));
  ankle_brake->co_stimulation_y_torque.ConnectTo(fusion_abs_torque_ankle_y->InputTargetRating(2));
  ankle_brake->co_ankle_y_torque.ConnectTo(fusion_abs_torque_ankle_y->InputPort(2, 0));

  ankle_brake->co_stimulation_x_torque.ConnectTo(fusion_abs_torque_ankle_x->InputActivity(5));
  ankle_brake->co_stimulation_x_torque.ConnectTo(fusion_abs_torque_ankle_x->InputTargetRating(5));//fixme
  ankle_brake->co_ankle_x_torque.ConnectTo(fusion_abs_torque_ankle_x->InputPort(5, 0));

  ankle_brake->co_stimulation_x_angle.ConnectTo(fusion_abs_angle_ankle_x->InputActivity(3));
  ankle_brake->target_rating.ConnectTo(fusion_abs_angle_ankle_x->InputTargetRating(3));
  ankle_brake->co_ankle_x_angle.ConnectTo(fusion_abs_angle_ankle_x->InputPort(3, 0));

  ankle_brake->co_stimulation_y_angle.ConnectTo(fusion_abs_angle_ankle_y->InputActivity(2));
  ankle_brake->target_rating.ConnectTo(fusion_abs_angle_ankle_y->InputTargetRating(2));
  ankle_brake->co_ankle_y_angle.ConnectTo(fusion_abs_angle_ankle_y->InputPort(2, 0));

  foot_load->so_ground_contact.ConnectTo(ankle_brake->si_ground_contact);

  // lateral stiffness push recovery

  fusion_lateral_stiffness_push_recovery_ankle->activity.ConnectTo(lateral_stiffness_control_push_recovery->stimulation);
  //this->ci_push_started.ConnectTo(lateral_stiffness_control_push_recovery->ci_push_detected_y);
  this->si_ankle_x_angle.ConnectTo(lateral_stiffness_control_push_recovery->si_current_angle);
  this->ci_learned_stiffness_ankle_push_recovery.ConnectTo(lateral_stiffness_control_push_recovery->ci_stiffness_strength);
  this->ci_loop_times.ConnectTo(lateral_stiffness_control_push_recovery->ci_loop_times);
  this->ci_lateral_velocity_correction.ConnectTo(lateral_stiffness_control_push_recovery->ci_com_error_y);
  this->si_force_z.ConnectTo(lateral_stiffness_control_push_recovery->si_force_z);
  this->ci_com_error_y.ConnectTo(lateral_stiffness_control_push_recovery->ci_com_error_y);

  lateral_stiffness_control_push_recovery->co_target_angle_activity.ConnectTo(fusion_abs_angle_ankle_x->InputActivity(7));
  lateral_stiffness_control_push_recovery->target_rating.ConnectTo(fusion_abs_angle_ankle_x->InputTargetRating(7));
  lateral_stiffness_control_push_recovery->co_target_angle.ConnectTo(fusion_abs_angle_ankle_x->InputPort(7, 0));

  lateral_stiffness_control_push_recovery->co_target_torque_activity.ConnectTo(fusion_abs_torque_ankle_x->InputActivity(11));
  lateral_stiffness_control_push_recovery->target_rating.ConnectTo(fusion_abs_torque_ankle_x->InputTargetRating(11));
  lateral_stiffness_control_push_recovery->co_target_torque.ConnectTo(fusion_abs_torque_ankle_x->InputPort(11, 0));

  // Toe Dorsiflexion
  /*
  AddEdgeDown(this, toe_dorsiflexion, 1,
              eCI_WALKING_SCALE_FACTOR, mbbToeDorsiflexion::eCI_SCALE);
  //eCI_GROUND_CONTACT, mbbToeDorsiflexion::eCI_GROUND_CONTACT);

  AddEdgeUp(this, toe_dorsiflexion, 1,
            eSI_TOE_JOINT_ANGLE, mbbToeDorsiflexion::eSI_TOE_ANGLE);
  AddEdgeUp(foot_load, toe_dorsiflexion, 1,
            mFootLoad::eSO_GROUND_CONTACT, mbbToeDorsiflexion::eSI_GROUND_CONTACT);

  index = fusion_abs_angle_toe->RegisterControllingBehaviour("(R) Toe Dorsiflexion");
  AddEdgeDown(toe_dorsiflexion, fusion_abs_angle_toe, 3,
              mbbToeDorsiflexion::eCO_STIMULATION_TOE_ANGLE, index,
              mbbToeDorsiflexion::eCO_TARGET_RATING, index + 1,
              mbbToeDorsiflexion::eCO_TOE_ANGLE, index + 2);
              */
  //END reflexes

  //BEGIN motor patterns
  // ---------------------------------------------------------------------------------------------
  this->ci_walking_scale_factor.ConnectTo(leg_propel->ci_scale);
  this->ci_forward_velocity_correction.ConnectTo(leg_propel->ci_correction);
  this->ci_com_error_y.ConnectTo(leg_propel->ci_com_y_error);
//  this->ci_turn_left_control_signal.ConnectTo(leg_propel->ci_turn_left_signal);
//  this->ci_turn_right_control_signal.ConnectTo(leg_propel->ci_turn_right_signal);
  this->ci_push_recovery_stand.ConnectTo(leg_propel->ci_push_recovery_stand);
  this->ci_push_recovery_walking.ConnectTo(leg_propel->ci_push_recovery_walking);
  this->ci_leg_propel_action1.ConnectTo(leg_propel->ci_leg_propel_action1);
  this->ci_leg_propel_action2.ConnectTo(leg_propel->ci_leg_propel_action2);
  this->ci_leg_propel_action3.ConnectTo(leg_propel->ci_leg_propel_action3);
  this->ci_leg_propel_action4.ConnectTo(leg_propel->ci_leg_propel_action4);
  this->ci_loop_times.ConnectTo(leg_propel->ci_loop_times);

  this->si_knee_angle.ConnectTo(leg_propel->si_knee_angle);
  this->si_ankle_y_angle.ConnectTo(leg_propel->si_ankle_angle);
  this->si_foot_left_position_x.ConnectTo(leg_propel->si_foot_left_position_x);
  this->si_foot_right_position_x.ConnectTo(leg_propel->si_foot_right_position_x);

  this->ci_walking_scale_factor.ConnectTo(leg_propel_sin->ci_scale);
  this->ci_forward_velocity_correction.ConnectTo(leg_propel_sin->ci_correction);
  this->ci_com_error_y.ConnectTo(leg_propel_sin->ci_com_y_error);
  this->ci_turn_left_control_signal.ConnectTo(leg_propel_sin->ci_turn_left_signal);
  this->ci_turn_right_control_signal.ConnectTo(leg_propel_sin->ci_turn_right_signal);
  this->ci_push_recovery_stand.ConnectTo(leg_propel_sin->ci_push_recovery_stand);
  //this->ci_push_recovery_walking.ConnectTo(leg_propel_sin->ci_push_recovery_walking);
  this->ci_leg_propel_action1.ConnectTo(leg_propel_sin->ci_leg_propel_action1);
  this->ci_leg_propel_action2.ConnectTo(leg_propel_sin->ci_leg_propel_action2);
  this->ci_leg_propel_action3.ConnectTo(leg_propel_sin->ci_leg_propel_action3);
  this->ci_leg_propel_action4.ConnectTo(leg_propel_sin->ci_leg_propel_action4);
  this->ci_loop_times.ConnectTo(leg_propel_sin->ci_loop_times);

  this->si_knee_angle.ConnectTo(leg_propel_sin->si_knee_angle);
  this->si_ankle_y_angle.ConnectTo(leg_propel_sin->si_ankle_angle);
  this->si_foot_left_position_x.ConnectTo(leg_propel_sin->si_foot_left_position_x);
  this->si_foot_right_position_x.ConnectTo(leg_propel_sin->si_foot_right_position_x);

  //push recovery learning for ankle hip strategy; uncomment when they are needed
//  this->ci_leg_learning_torque_factor.ConnectTo(leg_propel_sin->ci_leg_learning_torque_factor);
//  this->ci_push_started.ConnectTo(leg_propel_sin->ci_push_recovery_walking);

  leg_propel->activity.ConnectTo(fusion_abs_torque_ankle_y->InputActivity(3));
  leg_propel->activity.ConnectTo(fusion_abs_torque_ankle_y->InputTargetRating(3));
  leg_propel->co_torque_ankle_y.ConnectTo(fusion_abs_torque_ankle_y->InputPort(3, 0));

  leg_propel->activity.ConnectTo(fusion_abs_torque_ankle_x->InputActivity(6));
  leg_propel->activity.ConnectTo(fusion_abs_torque_ankle_x->InputTargetRating(6));//fixme
  leg_propel->co_torque_ankle_x.ConnectTo(fusion_abs_torque_ankle_x->InputPort(6, 0));

  leg_propel->co_stimulation_knee.ConnectTo(fusion_abs_torque_knee->InputActivity(1));
  leg_propel->co_stimulation_knee.ConnectTo(fusion_abs_torque_knee->InputTargetRating(1));//fixme
  leg_propel->co_torque_knee.ConnectTo(fusion_abs_torque_knee->InputPort(1, 0));

  leg_propel_sin->activity.ConnectTo(fusion_abs_torque_ankle_y->InputActivity(6));
  leg_propel_sin->activity.ConnectTo(fusion_abs_torque_ankle_y->InputTargetRating(6));
  leg_propel_sin->co_torque_ankle_y.ConnectTo(fusion_abs_torque_ankle_y->InputPort(6, 0));

  leg_propel_sin->co_stimulation_ankle_x.ConnectTo(fusion_abs_torque_ankle_x->InputActivity(10));
  leg_propel_sin->co_stimulation_ankle_x.ConnectTo(fusion_abs_torque_ankle_x->InputTargetRating(10));//fixme
  leg_propel_sin->co_torque_ankle_x.ConnectTo(fusion_abs_torque_ankle_x->InputPort(10, 0));

  leg_propel_sin->co_stimulation_knee.ConnectTo(fusion_abs_torque_knee->InputActivity(3));
  leg_propel_sin->co_stimulation_knee.ConnectTo(fusion_abs_torque_knee->InputTargetRating(3));//fixme
  leg_propel_sin->co_torque_knee.ConnectTo(fusion_abs_torque_knee->InputPort(3, 0));


  //PSO
  leg_propel_sin->ci_slow_walking.ConnectTo(this->co_slow_walking);
  leg_propel_sin->ci_fast_walking.ConnectTo(this->co_fast_walking);
  leg_propel_sin->ci_slow_walking_lp_max_torque.ConnectTo(this->co_slow_walking_lp_max_torque);
  leg_propel_sin->ci_slow_walking_lp_t1.ConnectTo(this->co_slow_walking_lp_t1);
  leg_propel_sin->ci_slow_walking_lp_t2.ConnectTo(this->co_slow_walking_lp_t2);
  lock_knee->ci_slow_walking_knee_angle.ConnectTo(this->co_slow_walking_lock_knee);
  lock_knee->ci_slow_walking_knee_angle_low.ConnectTo(this->co_slow_walking_lock_knee_low);
  lock_knee->ci_slow_walking_knee_angle_high.ConnectTo(this->co_slow_walking_lock_knee_high);
  lock_knee->ci_slow_walking_knee_velocity.ConnectTo(this->co_slow_walking_lock_knee_velocity);
  lock_knee->si_hip_angle.ConnectTo(this->si_hip_y_angle);
  lock_knee->ci_slow_walking_hip_angle_low.ConnectTo(this->co_slow_walking_hip_angle_low);
  lock_knee->ci_slow_walking_hip_angle_high.ConnectTo(this->co_slow_walking_hip_angle_high);
  lock_knee->ci_slow_walking_increment_par1.ConnectTo(this->co_slow_walking_increment_par1);
  lock_knee->ci_slow_walking_increment_par2.ConnectTo(this->co_slow_walking_increment_par2);
  this->co_slow_walking.ConnectTo(weight_accept_reflex->ci_slow_walking);
  this->co_slow_walking_max_impulse.ConnectTo(weight_accept_reflex->ci_slow_walking_max_impulse);

  // lift leg todo
//  mp_lift_leg->activity.ConnectTo(fusion_abs_torque_knee->InputActivity(2));
//  mp_lift_leg->activity.ConnectTo(fusion_abs_torque_knee->InputTargetRating(2));//fixme
//  mp_lift_leg->co_torque_knee.ConnectTo(fusion_abs_torque_knee->InputPort(2, 0));

  fusion_mp_lift_leg->activity.ConnectTo(mp_lift_leg->stimulation);
  this->ci_com_y_ratio.ConnectTo(mp_lift_leg->ci_com_y_ratio);

  // ankle x torque
  this->ci_walking_scale_laterally.ConnectTo(ankle_x_torque->ci_scale);

  ankle_x_torque->activity.ConnectTo(fusion_abs_torque_ankle_x->InputActivity(7));
  ankle_x_torque->activity.ConnectTo(fusion_abs_torque_ankle_x->InputTargetRating(7));
  ankle_x_torque->co_torque_pattern.ConnectTo(fusion_abs_torque_ankle_x->InputPort(7, 0));

  //END motor patterns


  //BEGIN skills
  // ---------------------------------------------------------------------------------------------
  // ---------------------------------------------------------------------------------------------
  // disturbance estimation and compensation

//  this->ci_disturbance_estimation_activity.ConnectTo(skill_compensation_ankle_pitch->stimulation);
//  this->ci_body_foot_angle.ConnectTo(skill_compensation_ankle_pitch->ci_body_foot_angle);
//  this->ci_foot_space_angle.ConnectTo(skill_compensation_ankle_pitch->ci_foot_space_angle);
//  this->ci_angle_deviation.ConnectTo(skill_compensation_ankle_pitch->ci_angle_deviation);
//  this->ci_target_body_space_angle.ConnectTo(skill_compensation_ankle_pitch->ci_target_ankle_angle);
//  this->ci_stimulation_walking_initiation.ConnectTo(skill_compensation_ankle_pitch->ci_walking_initiation);
//  this->ci_actual_pitch.ConnectTo(skill_compensation_ankle_pitch->ci_actual_pitch);
//
//  this->si_ankle_y_angle.ConnectTo(skill_compensation_ankle_pitch->si_body_foot_angle);

//  skill_compensation_ankle_pitch->activity.ConnectTo(fusion_abs_torque_ankle_y->InputActivity(5));
//  skill_compensation_ankle_pitch->target_rating.ConnectTo(fusion_abs_torque_ankle_y->InputTargetRating(5));
//  skill_compensation_ankle_pitch->co_torque_ankle_y.ConnectTo(fusion_abs_torque_ankle_y->InputPort(5, 0));

  fusion_compensate_ankle_roll->activity.ConnectTo(skill_compensation_ankle_roll->stimulation);

  this->ci_disturbance_estimation_activity.ConnectTo(skill_compensation_ankle_roll->stimulation);
  this->ci_body_foot_angle_lateral.ConnectTo(skill_compensation_ankle_roll->ci_body_foot_angle_lateral);
  this->ci_foot_space_angle_lateral.ConnectTo(skill_compensation_ankle_roll->ci_foot_space_angle_lateral);
  this->ci_angle_deviation_lateral.ConnectTo(skill_compensation_ankle_roll->ci_angle_deviation_lateral);
  this->ci_angle_body_space_lateral_dist.ConnectTo(skill_compensation_ankle_roll->ci_angle_body_space_dist_lateral);
  this->ci_angle_body_space_lateral_prop.ConnectTo(skill_compensation_ankle_roll->ci_angle_body_space_prop_lateral);
  this->ci_activity_walking.ConnectTo(skill_compensation_ankle_roll->ci_walking);
  this->ci_angle_body_space_lateral.ConnectTo(skill_compensation_ankle_roll->ci_angle_body_space_lateral);

  this->si_ankle_x_angle.ConnectTo(skill_compensation_ankle_roll->si_body_foot_lateral_angle);

  skill_compensation_ankle_roll->activity.ConnectTo(fusion_abs_torque_ankle_x->InputActivity(9));
  skill_compensation_ankle_roll->activity.ConnectTo(fusion_abs_torque_ankle_x->InputTargetRating(9));
  skill_compensation_ankle_roll->co_torque_ankle_x.ConnectTo(fusion_abs_torque_ankle_x->InputPort(9, 0));

  this->ci_disturbance_estimation_activity.ConnectTo(skill_compensation_ankle_pitch2->stimulation);
  this->ci_angle_body_space_dist.ConnectTo(skill_compensation_ankle_pitch2->ci_angle_body_space_dist);
  this->ci_angle_body_space_prop.ConnectTo(skill_compensation_ankle_pitch2->ci_angle_body_space_prop);
  this->ci_foot_space_angle_new.ConnectTo(skill_compensation_ankle_pitch2->ci_angle_foot_space);


  skill_compensation_ankle_pitch2->activity.ConnectTo(fusion_abs_torque_ankle_y->InputActivity(5));
  skill_compensation_ankle_pitch2->target_rating.ConnectTo(fusion_abs_torque_ankle_y->InputTargetRating(5));
  skill_compensation_ankle_pitch2->co_torque_ankle_y.ConnectTo(fusion_abs_torque_ankle_y->InputPort(5, 0));

  this->ci_foot_space_angle_lateral.ConnectTo(lateral_adjust_joint_angle->ci_plate_rotation_angle);

  lateral_adjust_joint_angle->activity.ConnectTo(fusion_abs_angle_knee->InputActivity(12));
  lateral_adjust_joint_angle->target_rating.ConnectTo(fusion_abs_angle_knee->InputTargetRating(12));
  lateral_adjust_joint_angle->co_target_knee_angle.ConnectTo(fusion_abs_angle_knee->InputPort(12, 0));

  this->ci_actual_roll.ConnectTo(skill_compensation_knee->ci_roll_angle);
  this->ci_angle_trunk_thigh_space_gravity.ConnectTo(skill_compensation_knee->ci_angle_trunk_thigh_space_gravity);
  this->ci_foot_space_angle_lateral.ConnectTo(skill_compensation_knee->ci_angle_plate_rotation);

  skill_compensation_knee->activity.ConnectTo(fusion_abs_torque_knee->InputActivity(2));
  skill_compensation_knee->target_rating.ConnectTo(fusion_abs_torque_knee->InputTargetRating(2));
  skill_compensation_knee->co_torque_knee.ConnectTo(fusion_abs_torque_knee->InputPort(2, 0));

  // stand
  this->ci_stimulation_stand.ConnectTo(skill_stand->stimulation);

  this->GetControllerInputs().ConnectByName(leg_termination->GetControllerInputs(), false);

  foot_load->so_ground_contact.ConnectTo(leg_termination->si_ground_contact);

  // walking initiation
  this->ci_stimulation_walking_initiation.ConnectTo(skill_walking_init->stimulation);

  skill_walking_init->co_target_activity[0].ConnectTo(fusion_abs_angle_knee->InputActivity(5));
  skill_walking_init->co_target_activity[0].ConnectTo(fusion_abs_angle_knee->InputTargetRating(5));
  skill_walking_init->co_target_output[0].ConnectTo(fusion_abs_angle_knee->InputPort(5, 0));

  //fusion_abs_angle_knee->activity.ConnectTo(skill_walking_init->stimulation);
  //fusion_abs_angle_knee->target_rating.ConnectTo(skill_walking_init->si_target_rating[0]);

  skill_walking_init->co_target_activity[1].ConnectTo(fusion_abs_torque_ankle_y->InputActivity(4));
  skill_walking_init->co_target_activity[1].ConnectTo(fusion_abs_torque_ankle_y->InputTargetRating(4));
  skill_walking_init->co_target_output[1].ConnectTo(fusion_abs_torque_ankle_y->InputPort(4, 0));

  skill_walking_init->activity.ConnectTo(skill_walking_phase4->inhibition[0]);

  // walking lateral initiation
  this->ci_stimulation_walking_lateral_initiation.ConnectTo(skill_walking_lateral_init->stimulation);

  skill_walking_lateral_init->co_target_activity[0].ConnectTo(fusion_abs_angle_knee->InputActivity(6));
  skill_walking_lateral_init->co_target_activity[0].ConnectTo(fusion_abs_angle_knee->InputTargetRating(6));
  skill_walking_lateral_init->co_target_output[0].ConnectTo(fusion_abs_angle_knee->InputPort(6, 0));

  skill_walking_lateral_init->co_target_activity[1].ConnectTo(fusion_abs_torque_ankle_x->InputActivity(8));
  skill_walking_lateral_init->co_target_activity[1].ConnectTo(fusion_abs_torque_ankle_x->InputTargetRating(8));
  skill_walking_lateral_init->co_target_output[1].ConnectTo(fusion_abs_torque_ankle_x->InputPort(8, 0));

  skill_walking_lateral_init->co_target_activity[2].ConnectTo(fusion_mp_lift_leg->InputActivity(0));
  skill_walking_lateral_init->co_target_activity[2].ConnectTo(fusion_mp_lift_leg->InputTargetRating(0));
  skill_walking_lateral_init->co_target_output[2].ConnectTo(fusion_mp_lift_leg->InputPort(0, 0));


  // walking

  // lateral walking
  // lateral walking phase 1
  this->ci_stimulation_walking_lateral_phase_1.ConnectTo(skill_walking_lateral_phase1->stimulation);

  skill_walking_lateral_phase1->co_target_activity[0].ConnectTo(fusion_lateral_velocity->InputActivity(0));
  skill_walking_lateral_phase1->co_target_activity[0].ConnectTo(fusion_lateral_velocity->InputTargetRating(0));
  skill_walking_lateral_phase1->co_target_output[0].ConnectTo(fusion_lateral_velocity->InputPort(0, 0));


  skill_walking_lateral_phase1->co_target_activity[1].ConnectTo(fusion_weight_accept->InputActivity(0));
  skill_walking_lateral_phase1->co_target_activity[1].ConnectTo(fusion_weight_accept->InputTargetRating(0));
  skill_walking_lateral_phase1->co_target_output[1].ConnectTo(fusion_weight_accept->InputPort(0, 0));


  skill_walking_lateral_phase1->co_target_activity[3].ConnectTo(fusion_control_ankle_roll->InputActivity(0));
  skill_walking_lateral_phase1->co_target_activity[3].ConnectTo(fusion_control_ankle_roll->InputTargetRating(0));
  skill_walking_lateral_phase1->co_target_output[3].ConnectTo(fusion_control_ankle_roll->InputPort(0, 0));


  // lateral walking phase 2
  this->ci_stimulation_walking_lateral_phase_2.ConnectTo(skill_walking_lateral_phase2->stimulation);

  skill_walking_lateral_phase2->co_target_activity[0].ConnectTo(fusion_abs_angle_knee->InputActivity(7));
  skill_walking_lateral_phase2->co_target_activity[0].ConnectTo(fusion_abs_angle_knee->InputTargetRating(7));
  skill_walking_lateral_phase2->co_target_output[0].ConnectTo(fusion_abs_angle_knee->InputPort(7, 0));

  skill_walking_lateral_phase2->co_target_activity[1].ConnectTo(fusion_lateral_velocity->InputActivity(1));
  skill_walking_lateral_phase2->co_target_activity[1].ConnectTo(fusion_lateral_velocity->InputTargetRating(1));
  skill_walking_lateral_phase2->co_target_output[1].ConnectTo(fusion_lateral_velocity->InputPort(1, 0));


  skill_walking_lateral_phase2->co_target_activity[2].ConnectTo(fusion_leg_propel_lateral->InputActivity(0));
  skill_walking_lateral_phase2->co_target_activity[2].ConnectTo(fusion_leg_propel_lateral->InputTargetRating(0));
  skill_walking_lateral_phase2->co_target_output[2].ConnectTo(fusion_leg_propel_lateral->InputPort(0, 0));


  skill_walking_lateral_phase2->co_target_activity[3].ConnectTo(fusion_mp_lift_leg->InputActivity(1));
  skill_walking_lateral_phase2->co_target_activity[3].ConnectTo(fusion_mp_lift_leg->InputTargetRating(1));
  skill_walking_lateral_phase2->co_target_output[3].ConnectTo(fusion_mp_lift_leg->InputPort(1, 0));


  // lateral walking phase 3
  this->ci_stimulation_walking_lateral_phase_3.ConnectTo(skill_walking_lateral_phase3->stimulation);

  skill_walking_lateral_phase3->co_target_activity[1].ConnectTo(fusion_leg_propel->InputActivity(0));
  skill_walking_lateral_phase3->co_target_activity[1].ConnectTo(fusion_leg_propel->InputTargetRating(0));
  skill_walking_lateral_phase3->co_target_output[1].ConnectTo(fusion_leg_propel->InputPort(0, 0));


  skill_walking_lateral_phase3->co_target_activity[2].ConnectTo(fusion_mp_lift_leg->InputActivity(2));
  skill_walking_lateral_phase3->co_target_activity[2].ConnectTo(fusion_mp_lift_leg->InputTargetRating(2));
  skill_walking_lateral_phase3->co_target_output[2].ConnectTo(fusion_mp_lift_leg->InputPort(2, 0));


  skill_walking_lateral_phase3->co_target_activity[3].ConnectTo(fusion_leg_propel_lateral->InputActivity(1));
  skill_walking_lateral_phase3->target_rating.ConnectTo(fusion_leg_propel_lateral->InputTargetRating(1));
  skill_walking_lateral_phase3->co_target_output[3].ConnectTo(fusion_leg_propel_lateral->InputPort(1, 0));


  // lateral walking phase 4
  this->ci_stimulation_walking_lateral_phase_4.ConnectTo(skill_walking_lateral_phase4->stimulation);

  skill_walking_lateral_phase4->co_target_activity[1].ConnectTo(fusion_lock_knee->InputActivity(0));
  skill_walking_lateral_phase4->co_target_activity[1].ConnectTo(fusion_lock_knee->InputTargetRating(0));
  skill_walking_lateral_phase4->co_target_output[1].ConnectTo(fusion_lock_knee->InputPort(0, 0));


  skill_walking_lateral_phase4->co_target_activity[2].ConnectTo(foot_contact->stimulation);

  skill_walking_lateral_phase4->co_target_activity[3].ConnectTo(fusion_weight_accept->InputActivity(1));
  skill_walking_lateral_phase4->co_target_activity[3].ConnectTo(fusion_weight_accept->InputTargetRating(1));
  skill_walking_lateral_phase4->co_target_output[3].ConnectTo(fusion_weight_accept->InputPort(1, 0));


  // all phases
  this->ci_activity_walking.ConnectTo(skill_walking->stimulation);
  skill_walking->co_target_activity[0].ConnectTo(fusion_abs_angle_ankle_x->InputActivity(4));
  skill_walking->co_target_activity[0].ConnectTo(fusion_abs_angle_ankle_x->InputTargetRating(4));
  skill_walking->co_target_output[0].ConnectTo(fusion_abs_angle_ankle_x->InputPort(4, 0));

  //fusion_abs_angle_ankle_x->activity.ConnectTo(skill_walking->stimulation);
// fusion_abs_angle_ankle_x->target_rating.ConnectTo(skill_walking->si_target_rating[0]);

  // walking lateral all phases
  this->ci_walking_lateral_activity.ConnectTo(skill_walking_lateral->stimulation);
  skill_walking_lateral->co_target_activity[0].ConnectTo(fusion_abs_angle_ankle_y->InputActivity(3));
  skill_walking_lateral->co_target_activity[0].ConnectTo(fusion_abs_angle_ankle_y->InputTargetRating(3));
  skill_walking_lateral->co_target_output[0].ConnectTo(fusion_abs_angle_ankle_y->InputPort(3, 0));

  //fusion_abs_angle_ankle_y->target_rating.ConnectTo(skill_walking_lateral->si_target_rating[0]);

  skill_walking_lateral->co_target_activity[1].ConnectTo(fusion_abs_angle_ankle_x->InputActivity(5));
  skill_walking_lateral->target_rating.ConnectTo(fusion_abs_angle_ankle_x->InputTargetRating(5));
  skill_walking_lateral->co_target_output[1].ConnectTo(fusion_abs_angle_ankle_x->InputPort(5, 0));

  //fusion_abs_angle_ankle_x->target_rating.ConnectTo(skill_walking_lateral->si_target_rating[1]);

  // phase 1
  skill_walking_termi->co_walking_phase_1.ConnectTo(skill_walking_phase1->stimulation);

  skill_walking_phase1->co_target_activity[0].ConnectTo(fusion_abs_angle_ankle_x->InputActivity(6));
  skill_walking_phase1->target_rating.ConnectTo(fusion_abs_angle_ankle_x->InputTargetRating(6));
  skill_walking_phase1->co_target_output[0].ConnectTo(fusion_abs_angle_ankle_x->InputPort(6, 0));

//  fusion_abs_angle_ankle_x->activity.ConnectTo(skill_walking_phase1->stimulation);
//  fusion_abs_angle_ankle_x->target_rating.ConnectTo(skill_walking_phase1->si_target_rating[0]);

  skill_walking_phase1->co_target_activity[1].ConnectTo(fusion_forward_velocity->InputActivity(0));
  skill_walking_phase1->co_target_activity[1].ConnectTo(fusion_forward_velocity->InputTargetRating(0));
  skill_walking_phase1->co_target_output[1].ConnectTo(fusion_forward_velocity->InputPort(0, 0));

  skill_walking_phase1->co_target_activity[2].ConnectTo(fusion_weight_accept->InputActivity(2));
  skill_walking_phase1->co_target_activity[2].ConnectTo(fusion_weight_accept->InputTargetRating(2));
  skill_walking_phase1->co_target_output[2].ConnectTo(fusion_weight_accept->InputPort(2, 0));

//  weight_accept_reflex->activity.ConnectTo(skill_walking_phase1->stimulation);
//  weight_accept_reflex->target_rating.ConnectTo(skill_walking_phase1->si_target_rating[1]);

  skill_walking_phase1->co_target_activity[3].ConnectTo(fusion_lateral_balance_ankle->InputActivity(0));
  skill_walking_phase1->co_target_activity[3].ConnectTo(fusion_lateral_balance_ankle->InputTargetRating(0));
  skill_walking_phase1->co_target_output[3].ConnectTo(fusion_lateral_balance_ankle->InputPort(0, 0));


//  lateral_balance_ankle->activity.ConnectTo(skill_walking_phase1->stimulation);
//  lateral_balance_ankle->target_rating.ConnectTo(skill_walking_phase1->si_target_rating[2]);

  skill_walking_phase1->co_target_activity[4].ConnectTo(fusion_control_ankle_roll->InputActivity(1));
  skill_walking_phase1->co_target_activity[4].ConnectTo(fusion_control_ankle_roll->InputTargetRating(1));
  skill_walking_phase1->co_target_output[4].ConnectTo(fusion_control_ankle_roll->InputPort(1, 0));


  skill_walking_phase1->activity.ConnectTo(fusion_brake_torque_ankle_y->InputActivity(0));
  skill_walking_phase1->activity.ConnectTo(fusion_brake_torque_ankle_y->InputTargetRating(0));
  skill_walking_phase1->co_target_output[4].ConnectTo(fusion_brake_torque_ankle_y->InputPort(0, 0));

  skill_walking_phase1->activity.ConnectTo(ankle_brake->ci_standing_leg);

  skill_walking_phase1->activity.ConnectTo(fusion_hold_knee->InputActivity(0));
  skill_walking_phase1->activity.ConnectTo(fusion_hold_knee->InputTargetRating(0));
  skill_walking_phase1->co_target_output[4].ConnectTo(fusion_hold_knee->InputPort(0, 0));


//  skill_walking_phase1->activity.ConnectTo(fusion_upright_knee->InputActivity(0));
//  skill_walking_phase1->activity.ConnectTo(fusion_upright_knee->InputTargetRating(0));
//  skill_walking_phase1->activity.ConnectTo(fusion_upright_knee->InputPort(0, 0));

  skill_walking_phase1->activity.ConnectTo(fusion_compensate_ankle_roll->InputActivity(0));
  skill_walking_phase1->activity.ConnectTo(fusion_compensate_ankle_roll->InputTargetRating(0));
  skill_walking_phase1->activity.ConnectTo(fusion_compensate_ankle_roll->InputPort(0, 0));

  skill_walking_phase1->activity.ConnectTo(fusion_lateral_stiffness_push_recovery_ankle->InputActivity(0));
  skill_walking_phase1->activity.ConnectTo(fusion_lateral_stiffness_push_recovery_ankle->InputTargetRating(0));
  skill_walking_phase1->activity.ConnectTo(fusion_lateral_stiffness_push_recovery_ankle->InputPort(0, 0));


  // phase 2
  skill_walking_termi->co_walking_phase_2.ConnectTo(skill_walking_phase2->stimulation);

  skill_walking_phase2->co_target_activity[0].ConnectTo(fusion_abs_angle_knee->InputActivity(8));
  skill_walking_phase2->co_target_activity[0].ConnectTo(fusion_abs_angle_knee->InputTargetRating(8));
  skill_walking_phase2->co_target_output[0].ConnectTo(fusion_abs_angle_knee->InputPort(8, 0));
// fusion_abs_angle_knee->activity.ConnectTo(skill_walking_phase2->stimulation);
// fusion_abs_angle_knee->target_rating.ConnectTo(skill_walking_phase2->si_target_rating[0]);

  skill_walking_phase2->co_target_activity[1].ConnectTo(fusion_leg_propel->InputActivity(1));
  skill_walking_phase2->co_target_activity[1].ConnectTo(fusion_leg_propel->InputTargetRating(1));
  skill_walking_phase2->co_target_output[1].ConnectTo(fusion_leg_propel->InputPort(1, 0));


//  leg_propel->activity.ConnectTo(skill_walking_phase2->stimulation);
//  leg_propel->target_rating.ConnectTo(skill_walking_phase2->si_target_rating[1]);

  skill_walking_phase2->co_target_activity[2].ConnectTo(fusion_forward_velocity->InputActivity(1));
  skill_walking_phase2->co_target_activity[2].ConnectTo(fusion_forward_velocity->InputTargetRating(1));
  skill_walking_phase2->co_target_output[2].ConnectTo(fusion_forward_velocity->InputPort(1, 0));


  skill_walking_phase2->co_target_activity[3].ConnectTo(fusion_lateral_balance_ankle->InputActivity(1));
  skill_walking_phase2->co_target_activity[3].ConnectTo(fusion_lateral_balance_ankle->InputTargetRating(1));
  skill_walking_phase2->co_target_output[3].ConnectTo(fusion_lateral_balance_ankle->InputPort(1, 0));


  skill_walking_phase2->co_target_activity[4].ConnectTo(fusion_control_ankle_roll->InputActivity(2));
  skill_walking_phase2->co_target_activity[4].ConnectTo(fusion_control_ankle_roll->InputTargetRating(2));
  skill_walking_phase2->co_target_output[4].ConnectTo(fusion_control_ankle_roll->InputPort(2, 0));

  skill_walking_phase2->activity.ConnectTo(fusion_compensate_ankle_roll->InputActivity(1));
  skill_walking_phase2->activity.ConnectTo(fusion_compensate_ankle_roll->InputTargetRating(1));
  skill_walking_phase2->activity.ConnectTo(fusion_compensate_ankle_roll->InputPort(1, 0));

  skill_walking_phase2->activity.ConnectTo(fusion_lateral_stiffness_push_recovery_ankle->InputActivity(1));
  skill_walking_phase2->activity.ConnectTo(fusion_lateral_stiffness_push_recovery_ankle->InputTargetRating(1));
  skill_walking_phase2->activity.ConnectTo(fusion_lateral_stiffness_push_recovery_ankle->InputPort(1, 0));

  // phase 3
  skill_walking_termi->co_walking_phase_3.ConnectTo(skill_walking_phase3->stimulation);

  skill_walking_phase3->co_target_activity[0].ConnectTo(fusion_abs_angle_knee->InputActivity(9));
  skill_walking_phase3->co_target_activity[0].ConnectTo(fusion_abs_angle_knee->InputTargetRating(9));
  skill_walking_phase3->co_target_output[0].ConnectTo(fusion_abs_angle_knee->InputPort(9, 0));

//  fusion_abs_angle_knee->activity.ConnectTo(skill_walking_phase3->stimulation);
//  fusion_abs_angle_knee->target_rating.ConnectTo(skill_walking_phase3->si_target_rating[0]);

  skill_walking_phase3->co_target_activity[1].ConnectTo(fusion_leg_propel->InputActivity(2));
  skill_walking_phase3->co_target_activity[1].ConnectTo(fusion_leg_propel->InputTargetRating(2));
  skill_walking_phase3->co_target_output[1].ConnectTo(fusion_leg_propel->InputPort(2, 0));


//  leg_propel->activity.ConnectTo(skill_walking_phase3->stimulation);
//  leg_propel->target_rating.ConnectTo(skill_walking_phase3->si_target_rating[1]);

  // phase 4
  skill_walking_termi->co_walking_phase_4.ConnectTo(skill_walking_phase4->stimulation);

  skill_walking_phase4->activity.ConnectTo(this->so_activity_walking_phase4);
  lock_knee->target_rating.ConnectTo(this->so_target_rating_walking_phase4);

  //skill_walking_phase4->target_rating.ConnectTo(this->so_target_rating_walking_phase4);// fixme

  skill_walking_phase4->co_target_activity[0].ConnectTo(fusion_abs_angle_knee->InputActivity(10));
  skill_walking_phase4->co_target_activity[0].ConnectTo(fusion_abs_angle_knee->InputTargetRating(10));
  skill_walking_phase4->co_target_output[0].ConnectTo(fusion_abs_angle_knee->InputPort(10, 0));

  skill_walking_phase4->co_target_activity[1].ConnectTo(fusion_abs_angle_ankle_y->InputActivity(4));
  skill_walking_phase4->co_target_activity[1].ConnectTo(fusion_abs_angle_ankle_y->InputTargetRating(4));
  skill_walking_phase4->co_target_output[1].ConnectTo(fusion_abs_angle_ankle_y->InputPort(4, 0));

  skill_walking_phase4->co_target_activity[2].ConnectTo(fusion_lock_knee->InputActivity(1));
  skill_walking_phase4->co_target_activity[2].ConnectTo(fusion_lock_knee->InputTargetRating(1));
  skill_walking_phase4->co_target_output[2].ConnectTo(fusion_lock_knee->InputPort(1, 0));


  //lock_knee->activity.ConnectTo(skill_walking_phase4->stimulation);

  skill_walking_phase4->co_target_activity[3].ConnectTo(cutaneous_reflex->stimulation);

  // phase 5
  skill_walking_termi->co_walking_phase_5.ConnectTo(skill_walking_phase5->stimulation);
// this->ci_stimulation_walking_phase_5.ConnectTo(skill_walking_phase5->stimulation); // fixme
//  skill_walking_phase5->activity.ConnectTo(fusion_upright_knee->InputActivity(1));
//  skill_walking_phase5->activity.ConnectTo(fusion_upright_knee->InputTargetRating(1));
//  skill_walking_phase5->activity.ConnectTo(fusion_upright_knee->InputPort(1, 0));

  //heel_strike_reflex->activity.ConnectTo(skill_walking_phase5->stimulation);
  //heel_strike_reflex->target_rating.ConnectTo(skill_walking_phase5->si_target_rating[0]);
  skill_walking_phase5->co_target_activity[0].ConnectTo(fusion_abs_angle_knee->InputActivity(11));
  skill_walking_phase5->co_target_activity[0].ConnectTo(fusion_abs_angle_knee->InputTargetRating(11));
  skill_walking_phase5->co_target_output[0].ConnectTo(fusion_abs_angle_knee->InputPort(11, 0));

  skill_walking_phase5->activity.ConnectTo(fusion_heel_strike->InputActivity(0));//fixme
  skill_walking_phase5->co_target_activity[2].ConnectTo(fusion_heel_strike->InputTargetRating(0));
  skill_walking_phase5->co_target_output[2].ConnectTo(fusion_heel_strike->InputPort(0, 0));


  skill_walking_phase5->co_target_activity[3].ConnectTo(fusion_weight_accept->InputActivity(3));
  skill_walking_phase5->co_target_activity[3].ConnectTo(fusion_weight_accept->InputTargetRating(3));
  skill_walking_phase5->co_target_output[3].ConnectTo(fusion_weight_accept->InputPort(3, 0));


  skill_walking_phase5->activity.ConnectTo(fusion_hold_knee->InputActivity(1));
  skill_walking_phase5->activity.ConnectTo(fusion_hold_knee->InputTargetRating(1));
  skill_walking_phase5->co_target_output[3].ConnectTo(fusion_hold_knee->InputPort(1, 0));


  skill_walking_phase5->activity.ConnectTo(this->so_activity_walking_phase5);
  heel_strike_reflex->target_rating.ConnectTo(this->so_target_rating_walking_phase5); //fixme
  //skill_walking_phase5->target_rating.ConnectTo(this->so_target_rating_walking_phase5);
  // lift leg
  this->ci_stimulation_lift_leg.ConnectTo(skill_lift_leg->stimulation);
  //END skills

  //Begin fusion of cyclic walking and termination
  //Knee angle and torque
  fusion_abs_torque_knee->activity.ConnectTo(fusion_torque_knee->InputActivity(0));
  fusion_abs_torque_knee->OutputPort(0).ConnectTo(fusion_torque_knee->InputPort(0, 0));
  fusion_abs_torque_knee->activity.ConnectTo(fusion_torque_knee->InputTargetRating(0));//fixme


  leg_termination->co_stimulation_knee_torque.ConnectTo(fusion_torque_knee->InputActivity(1));
  leg_termination->co_knee_torque.ConnectTo(fusion_torque_knee->InputPort(1, 0));
  leg_termination->co_stimulation_knee_torque.ConnectTo(fusion_torque_knee->InputTargetRating(1));


  fusion_abs_angle_knee->activity.ConnectTo(fusion_angle_knee->InputActivity(0));
  fusion_abs_angle_knee->OutputPort(0).ConnectTo(fusion_angle_knee->InputPort(0, 0));
  fusion_abs_angle_knee->target_rating.ConnectTo(fusion_angle_knee->InputTargetRating(0));

  leg_termination->co_stimulation_knee_angle.ConnectTo(fusion_angle_knee->InputActivity(1));
  leg_termination->co_knee_angle.ConnectTo(fusion_angle_knee->InputPort(1, 0));
  leg_termination->co_stimulation_knee_angle.ConnectTo(fusion_angle_knee->InputTargetRating(1));

  //PSO

//  if( this->co_slow_walking.Get() && !this->co_fast_walking.Get() )
  {
	  this->ci_slow_walking_stimulation_knee_angle.ConnectTo(fusion_angle_knee->InputActivity(2));
	  this->co_knee_angle.ConnectTo(fusion_angle_knee->InputPort(2, 0));
	  this->ci_slow_walking_stimulation_knee_angle.ConnectTo(fusion_angle_knee->InputTargetRating(2));
  }

//  if( !this->co_slow_walking.Get() && this->co_fast_walking.Get() )
//  {
//	  this->ci_fast_walking_stimulation_knee_angle.ConnectTo(fusion_angle_knee->InputActivity(3));
//	  this->co_knee_angle.ConnectTo(fusion_angle_knee->InputPort(2, 0));
//	  this->ci_fast_walking_stimulation_knee_angle.ConnectTo(fusion_angle_knee->InputTargetRating(3));
//  }

  //Ankle X torque and angle
  fusion_abs_torque_ankle_x->activity.ConnectTo(fusion_torque_ankle_x->InputActivity(0));
  fusion_abs_torque_ankle_x->OutputPort(0).ConnectTo(fusion_torque_ankle_x->InputPort(0, 0));
  fusion_abs_torque_ankle_x->activity.ConnectTo(fusion_torque_ankle_x->InputTargetRating(0));


  leg_termination->co_stimulation_ankle_x_torque.ConnectTo(fusion_torque_ankle_x->InputActivity(1));
  leg_termination->co_ankle_x_torque.ConnectTo(fusion_torque_ankle_x->InputPort(1, 0));
  leg_termination->co_stimulation_ankle_x_torque.ConnectTo(fusion_torque_ankle_x->InputTargetRating(1));


  fusion_abs_angle_ankle_x->activity.ConnectTo(fusion_angle_ankle_x->InputActivity(0));
  fusion_abs_angle_ankle_x->OutputPort(0).ConnectTo(fusion_angle_ankle_x->InputPort(0, 0));
  fusion_abs_angle_ankle_x->target_rating.ConnectTo(fusion_angle_ankle_x->InputTargetRating(0));


  leg_termination->co_stimulation_ankle_x_angle.ConnectTo(fusion_angle_ankle_x->InputActivity(1));
  leg_termination->co_ankle_x_angle.ConnectTo(fusion_angle_ankle_x->InputPort(1, 0));
  leg_termination->co_stimulation_ankle_x_angle.ConnectTo(fusion_angle_ankle_x->InputTargetRating(1));


  //Ankle Y torque and angle
  fusion_abs_torque_ankle_y->activity.ConnectTo(fusion_torque_ankle_y->InputActivity(0));
  fusion_abs_torque_ankle_y->OutputPort(0).ConnectTo(fusion_torque_ankle_y->InputPort(0, 0));
  fusion_abs_torque_ankle_y->target_rating.ConnectTo(fusion_torque_ankle_y->InputTargetRating(0));


  leg_termination->co_stimulation_ankle_y_torque.ConnectTo(fusion_torque_ankle_y->InputActivity(1));
  leg_termination->co_ankle_y_torque.ConnectTo(fusion_torque_ankle_y->InputPort(1, 0));
  leg_termination->co_stimulation_ankle_y_torque.ConnectTo(fusion_torque_ankle_y->InputTargetRating(1));


  fusion_abs_angle_ankle_y->activity.ConnectTo(fusion_angle_ankle_y->InputActivity(0));
  fusion_abs_angle_ankle_y->OutputPort(0).ConnectTo(fusion_angle_ankle_y->InputPort(0, 0));
  fusion_abs_angle_ankle_y->activity.ConnectTo(fusion_angle_ankle_y->InputTargetRating(0));


  leg_termination->co_stimulation_ankle_y_angle.ConnectTo(fusion_angle_ankle_y->InputActivity(1));
  leg_termination->co_ankle_y_angle.ConnectTo(fusion_angle_ankle_y->InputPort(1, 0));
  leg_termination->co_stimulation_ankle_y_angle.ConnectTo(fusion_angle_ankle_y->InputTargetRating(1));


  //End fusion of cyclic walking and termination
  // Fusions and controller output
  fusion_angle_knee->activity.ConnectTo(this->co_stimulation_knee_angle);
  fusion_angle_knee->OutputPort(0).ConnectTo(this->co_knee_angle);

  fusion_torque_knee->activity.ConnectTo(this->co_stimulation_knee_torque);
  fusion_torque_knee->OutputPort(0).ConnectTo(this->co_knee_torque);

  fusion_angle_ankle_x->activity.ConnectTo(this->co_stimulation_ankle_x_angle);
  fusion_angle_ankle_x->OutputPort(0).ConnectTo(this->co_ankle_x_angle);

  fusion_torque_ankle_x->activity.ConnectTo(this->co_stimulation_ankle_x_torque);
  fusion_torque_ankle_x->OutputPort(0).ConnectTo(this->co_ankle_x_torque);

  fusion_angle_ankle_y->activity.ConnectTo(this->co_stimulation_ankle_y_angle);
  fusion_angle_ankle_y->OutputPort(0).ConnectTo(this->co_ankle_y_angle);

  fusion_torque_ankle_y->activity.ConnectTo(this->co_stimulation_ankle_y_torque);
  fusion_torque_ankle_y->OutputPort(0).ConnectTo(this->co_ankle_y_torque);

  leg_termination->so_activity_termination_phase4.ConnectTo(this->so_activity_termination_phase4);
  leg_termination->so_activity_termination_phase5.ConnectTo(this->so_activity_termination_phase5);
  leg_termination->so_target_rating_termination_phase4.ConnectTo(this->so_target_rating_termination_phase4);
  leg_termination->so_target_rating_termination_phase5.ConnectTo(this->so_target_rating_termination_phase5);

  this->GetSensorInputs().ConnectByName(this->GetSensorOutputs(), false);
  this->GetSensorInputs().ConnectByName(leg_termination->GetSensorInputs(), false);
  this->ci_termination_scale_factor.ConnectTo(leg_termination->ci_termination_scale_factor);
  this->ci_termination_hold_position.ConnectTo(leg_termination->ci_termination_hold_position);
  this->ci_termination_turn_off_upright.ConnectTo(leg_termination->ci_termination_turn_off_upright);

}

//----------------------------------------------------------------------
// gLegGroup destructor
//----------------------------------------------------------------------
gLegGroup::~gLegGroup()
{}

//----------------------------------------------------------------------
// End of namespace declaration
//----------------------------------------------------------------------
}
}
}
