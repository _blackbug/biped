//
// You received this file as part of Finroc
// A Framework for intelligent robot control
//
// Copyright (C) Finroc GbR (finroc.org)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//----------------------------------------------------------------------
/*!\file    projects/biped/control/gLegGroup.h
 *
 * \author  Tobias Luksch
 * \author  Jie Zhao
 *
 * \date    2013-10-17
 *
 * \brief Contains gLegGroup
 *
 * \b gLegGroup
 *
 *
 * The leg group for the walking, termination and lateral walking!
 *
 */
//----------------------------------------------------------------------
#ifndef __projects__biped__control__gLegGroup_h__
#define __projects__biped__control__gLegGroup_h__

#include "plugins/structure/tSenseControlGroup.h"

//----------------------------------------------------------------------
// External includes (system with <>, local with "")
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Internal includes with ""
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Namespace declaration
//----------------------------------------------------------------------
namespace finroc
{
namespace biped
{
namespace control
{

//----------------------------------------------------------------------
// Forward declarations / typedefs / enums
//----------------------------------------------------------------------
typedef const char *tConstDescription;
//----------------------------------------------------------------------
// Class declaration
//----------------------------------------------------------------------
//! SHORT_DESCRIPTION
/*!
 *
 * The leg group for the walking, termination and lateral walking!
 */
class gLegGroup : public structure::tSenseControlGroup
{

//----------------------------------------------------------------------
// Ports (These are the only variables that may be declared public)
//----------------------------------------------------------------------
public:

  tControllerInput<double> ci_stimulation_stand;
  tControllerInput<double> ci_stiffness_factor;
  tControllerInput<double> ci_quiet_stand;
  tControllerInput<double> ci_sensor_monitor;
  tControllerInput<double> ci_ground_contact;
  tControllerInput<double> ci_com_error_y;
  tControllerInput<double> ci_com_error_x;
  tControllerInput<double> ci_com_y_ratio;
  tControllerInput<double> ci_stimulation_relax_knee;
  tControllerInput<double> ci_relax_knee_angle_adjustment;
  tControllerInput<double> ci_stimulation_balance_ankle_pitch;
  tControllerInput<double> ci_balance_ankle_pitch_angle_adjustment;
  tControllerInput<double> ci_stimulation_reduce_tension_ankle_y;
  tControllerInput<double> ci_reduce_tension_ankle_y_angle_adjustment;
  tControllerInput<double> ci_stimulation_walking_initiation;
  tControllerInput<double> ci_stimulation_walking_lateral_initiation;
  tControllerInput<double> ci_activity_walking;
  tControllerInput<double> ci_walking_scale_factor;
  tControllerInput<double> ci_walking_scale_laterally;
  tControllerInput<double> ci_walking_lateral_activity;
  tControllerInput<double> ci_termination_scale_factor;
  tControllerInput<double> ci_stimulation_termination;
  tControllerInput<double> ci_last_step_state;
  tControllerInput<double> ci_last_step;
  tControllerInput<double> ci_push_recovery_stand;
  tControllerInput<double> ci_push_recovery_walking;
  tControllerInput<double> ci_bent_knee;
  tControllerInput<double> ci_push_detected_walking_left;
  tControllerInput<double> ci_push_detected_walking_right;
  tControllerInput<bool> ci_push_detected_walking_lateral;
  tControllerInput<bool> ci_push_started;
  tControllerInput<double> ci_leg_angle_offset;
  tControllerInput<double> ci_forward_velocity_correction;
  tControllerInput<double> ci_lateral_velocity_correction;
  tControllerInput<double> ci_frontal_velocity_correction;
  tControllerInput<double> ci_stimulation_walking_phase_1;
  tControllerInput<double> ci_stimulation_walking_phase_2;
  tControllerInput<double> ci_stimulation_walking_phase_3;
  tControllerInput<double> ci_stimulation_walking_phase_4;
  tControllerInput<double> ci_stimulation_walking_phase_5;
  tControllerInput<double> ci_stimulation_walking_lateral_phase_1;
  tControllerInput<double> ci_stimulation_walking_lateral_phase_2;
  tControllerInput<double> ci_stimulation_walking_lateral_phase_3;
  tControllerInput<double> ci_stimulation_walking_lateral_phase_4;

  tControllerInput<double> ci_turn_left_control_signal;
  tControllerInput<double> ci_turn_right_control_signal;
  tControllerInput<double> ci_s_leg;
  tControllerInput<double> ci_maintain_stable;
  tControllerInput<double> ci_termination_hold_position;
  tControllerInput<double> ci_termination_turn_off_upright;
  tControllerInput<double> ci_stimulation_termination_button;

  tControllerInput<double> ci_stimulation_lift_leg;
  tControllerInput<double> ci_learned_stiffness_ankle_push_recovery;

  tControllerInput<double> ci_obstacle_knee_flexion_1;
  tControllerInput<double> ci_obstacle_knee_flexion_2;
  tControllerInput<double> ci_obstacle_knee_flexion_2_rear;
  tControllerInput<bool> ci_obstacle_walking_stimulation;
  tControllerInput<int> ci_obstacle_step;

  // ankle hip strategy in push recovery
  tControllerInput<double> ci_leg_learning_torque_factor;


  //DEC
  tControllerInput<double> ci_disturbance_estimation_activity;
  tControllerInput<double> ci_body_foot_angle;
  tControllerInput<double> ci_foot_space_angle;
  tControllerInput<double> ci_actual_pitch;
  tControllerInput<double> ci_actual_roll;
  tControllerInput<double> ci_angle_deviation;
  tControllerInput<double> ci_angle_body_space_lateral;
  tControllerInput<double> ci_body_foot_angle_lateral;
  tControllerInput<double> ci_foot_space_angle_lateral;
  tControllerInput<double> ci_angle_deviation_lateral;
  tControllerInput<double> ci_target_body_space_angle;
  tControllerInput<double> ci_angle_body_space_prop;
  tControllerInput<double> ci_angle_body_space_dist;
  tControllerInput<double> ci_angle_body_space_lateral_prop;
  tControllerInput<double> ci_angle_body_space_lateral_dist;
  tControllerInput<double> ci_angle_trunk_thigh_space_gravity;
  tControllerInput<double> ci_foot_space_angle_new;
  tControllerInput<double> ci_leg_propel_action1;
  tControllerInput<double> ci_leg_propel_action2;
  tControllerInput<double> ci_leg_propel_action3;
  tControllerInput<double> ci_leg_propel_action4;
  tControllerInput<bool> ci_reset_simulation_experiments;
  tControllerInput<double> ci_loop_times;
  tControllerInput<double> ci_slow_walking_stimulation_knee_angle;
  tControllerInput<double> ci_fast_walking_stimulation_knee_angle;
  tControllerInput<double> ci_slow_walking_stimulation_knee_angle_activity;

  tSensorInput<double> si_knee_torque;
  tSensorInput<double> si_knee_angle;
  tSensorInput<double> si_ankle_y_torque;
  tSensorInput<double> si_ankle_y_angle;
  tSensorInput<double> si_ankle_x_torque;
  tSensorInput<double> si_ankle_x_angle;
  tSensorInput<double> si_force_z;
  tSensorInput<double> si_torque_x;
  tSensorInput<double> si_torque_y;
  tSensorInput<double> si_inner_toe_force;
  tSensorInput<double> si_outer_toe_force;
  tSensorInput<double> si_inner_heel_force;
  tSensorInput<double> si_outer_heel_force;
  tSensorInput<double> si_foot_left_position_x;
  tSensorInput<double> si_foot_left_position_y;
  tSensorInput<double> si_foot_left_position_z;
  tSensorInput<double> si_foot_right_position_x;
  tSensorInput<double> si_foot_right_position_y;
  tSensorInput<double> si_foot_right_position_z;
  tSensorInput<double> si_swing_leg_hip_y;
  tSensorInput<double> si_stance_leg_hip_y;
  tSensorInput<double> si_hip_y_angle;

  tControllerOutput<double> co_stimulation_knee_torque;
  tControllerOutput<double> co_knee_torque;
  tControllerOutput<double> co_stimulation_knee_angle;
  tControllerOutput<double> co_knee_angle;
  tControllerOutput<double> co_stimulation_ankle_y_torque;
  tControllerOutput<double> co_ankle_y_torque;
  tControllerOutput<double> co_stimulation_ankle_y_angle;
  tControllerOutput<double> co_ankle_y_angle;
  tControllerOutput<double> co_stimulation_ankle_x_torque;
  tControllerOutput<double> co_ankle_x_torque;
  tControllerOutput<double> co_stimulation_ankle_x_angle;
  tControllerOutput<double> co_ankle_x_angle;

  tSensorOutput<double> so_leg_co_log_updated;
  tSensorOutput<double> so_foot_load;
  tSensorOutput<double> so_activity_walking_phase4;
  tSensorOutput<double> so_target_rating_walking_phase4;
  tSensorOutput<double> so_activity_walking_phase5;
  tSensorOutput<double> so_target_rating_walking_phase5;
  tSensorOutput<double> so_activity_termination_phase4;
  tSensorOutput<double> so_target_rating_termination_phase4;
  tSensorOutput<double> so_activity_termination_phase5;
  tSensorOutput<double> so_target_rating_termination_phase5;
  tSensorOutput<double> so_knee_torque;
  tSensorOutput<double> so_knee_angle;
  tSensorOutput<double> so_ankle_y_torque;
  tSensorOutput<double> so_ankle_y_angle;
  tSensorOutput<double> so_ankle_x_torque;
  tSensorOutput<double> so_ankle_x_angle;
  tSensorOutput<double> so_force_z;
  tSensorOutput<double> so_torque_x;
  tSensorOutput<double> so_torque_y;
  tSensorOutput<double> so_inner_toe_force;
  tSensorOutput<double> so_outer_toe_force;
  tSensorOutput<double> so_inner_heel_force;
  tSensorOutput<double> so_outer_heel_force;
  tSensorOutput<double> so_force_deviation;
  tSensorOutput<double> so_ground_contact;
  tSensorOutput<double> so_cop_x;
  tSensorOutput<double> so_cop_y;

  //PSO

  tControllerOutput<double> co_slow_walking_lp_t1;
  tControllerOutput<double> co_slow_walking_lp_t2;
  tControllerOutput<double> co_slow_walking_lp_max_torque;
  tControllerOutput<double> co_slow_walking_lock_knee;
  tControllerOutput<double> co_slow_walking_lock_knee_low;
  tControllerOutput<double> co_slow_walking_lock_knee_high;
  tControllerOutput<double> co_slow_walking_lock_knee_velocity;
  tControllerOutput<double> co_slow_walking_hip_angle_low;
  tControllerOutput<double> co_slow_walking_hip_angle_high;
  tControllerOutput<double> co_slow_walking_increment_par1;
  tControllerOutput<double> co_slow_walking_increment_par2;
  tControllerOutput<double> co_slow_walking_max_impulse;
  tControllerOutput<bool> co_slow_walking;
  tControllerOutput<bool> co_fast_walking;
//----------------------------------------------------------------------
// Public methods and typedefs
//----------------------------------------------------------------------
public:

  gLegGroup(core::tFrameworkElement *parent, const std::string &name = "LegGroup",
            const std::string &structure_config_file = __FILE__".xml");

//----------------------------------------------------------------------
// Private fields and methods
//----------------------------------------------------------------------
private:

  /*! Destructor
   *
   * The destructor of modules is declared private to avoid accidental deletion. Deleting
   * modules is already handled by the framework.
   */
  ~gLegGroup();

};

//----------------------------------------------------------------------
// End of namespace declaration
//----------------------------------------------------------------------
}
}
}



#endif
