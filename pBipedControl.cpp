//
// You received this file as part of Finroc
// A framework for intelligent robot control
//
// Copyright (C) Finroc GbR (finroc.org)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//
//----------------------------------------------------------------------
/*!\file    projects/biped/control/pBipedControl.cpp
 *
 * \author  Jie Zhao
 *
 * \date    2014-01-13
 *
 * \brief Contains mBipedControl
 *
 * \b pBipedControl
 *
 * Part file for biped control.part file fot the biped control project
 *
 */
//----------------------------------------------------------------------
#include "plugins/structure/default_main_wrapper.h"

//----------------------------------------------------------------------
// External includes (system with <>, local with "")
//----------------------------------------------------------------------
#include <chrono>

//----------------------------------------------------------------------
// Internal includes with ""
//----------------------------------------------------------------------
#include "projects/biped/control/gBipedControl.h"
#include "projects/biped/utils/tApplicationTimeInputPorts.h"
//#include "plugins/data_recording/tDataRecording.h"
//#include "rrlib/data_recording/backend/impl/gnuplot/tBackend.h"

//----------------------------------------------------------------------
// Debugging
//----------------------------------------------------------------------
#include <cassert>

//----------------------------------------------------------------------
// Namespace usage
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Forward declarations / typedefs / enums
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Const values
//----------------------------------------------------------------------
const std::string cPROGRAM_DESCRIPTION = "This program executes the BipedControl module/group.";
const std::string cCOMMAND_LINE_ARGUMENTS = "";
const std::string cADDITIONAL_HELP_TEXT = "";
bool make_all_port_links_unique = true;

bool use_simulation_time = false;
//rrlib::data_recording::backend::impl::gnuplot::tBackend recording_sink("/home/jie/gnuplot", false);
//----------------------------------------------------------------------
// Implementation
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// OptionsHandler
//----------------------------------------------------------------------
bool OptionsHandler(const rrlib::getopt::tNameToOptionMap &name_to_option_map)
{
  // pause
  rrlib::getopt::tOption use_simulation_time_option(name_to_option_map.at("use-simulation-time"));
  use_simulation_time = use_simulation_time_option->IsActive();
  return true;
}

//----------------------------------------------------------------------
// StartUp
//----------------------------------------------------------------------
void StartUp()
{
  rrlib::getopt::AddFlag("use-simulation-time", 0, "Use time received from simulation as 'application time' (allows to perform time steps)", &OptionsHandler);
}

//----------------------------------------------------------------------
// CreateMainGroup
//----------------------------------------------------------------------
void CreateMainGroup(const std::vector<std::string> &remaining_arguments)
{
  finroc::structure::tTopLevelThreadContainer<> *main_thread = new finroc::structure::tTopLevelThreadContainer<>("Main Thread", __FILE__".xml", true, make_all_port_links_unique);

//  recording_sink.OpenWrite();
//  finroc::data_recording::tPortSetRecordingControl recording(finroc::data_recording::tRegex("/Main Thread/BipedControl/Spinal Cord/(PR)_Lateral_Velocity_Left/Output/.*"));
//  recording.Record(recording_sink);
  new finroc::biped::control::gBipedControl(main_thread, "BipedControl");
  //finroc_control->co_left_ankle_x_angle.ConnectTo("/MCA/Controller Input/ankle_x_left - Stimulation Position");

  // position control inputs
//  finroc_control->co_left_stimulation_ankle_y_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/ankle_y_left - Stimulation Position");
//  finroc_control->co_left_stimulation_ankle_x_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/ankle_x_left - Stimulation Position");
//  finroc_control->co_right_stimulation_ankle_y_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/ankle_y_right - Stimulation Position");
//  finroc_control->co_right_stimulation_ankle_x_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/ankle_x_right - Stimulation Position");
//
//  finroc_control->co_left_ankle_y_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/ankle_y_left - Target Position");
//  finroc_control->co_left_ankle_x_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/ankle_x_left - Target Position");
//  finroc_control->co_right_ankle_y_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/ankle_y_right - Target Position");
//  finroc_control->co_right_ankle_x_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/ankle_x_right - Target Position");
//
//  finroc_control->co_right_stimulation_hip_y_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/hip_y_right - Stimulation Position");
//  finroc_control->co_right_stimulation_hip_x_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/hip_x_right - Stimulation Position");
//  finroc_control->co_right_stimulation_hip_z_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/hip_z_right - Stimulation Position");
//  finroc_control->co_right_stimulation_shoulder_y_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/shoulder_y_right - Stimulation Position");
//  finroc_control->co_right_stimulation_shoulder_x_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/shoulder_x_right - Stimulation Position");
//  finroc_control->co_right_stimulation_elbow_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/elbow_right - Stimulation Position");
//  finroc_control->co_right_stimulation_knee_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/knee_right - Stimulation Position");
//
//  finroc_control->co_right_hip_y_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/hip_y_right - Target Position");
//  finroc_control->co_right_hip_x_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/hip_x_right - Target Position");
//  finroc_control->co_right_hip_z_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/hip_z_right - Target Position");
//  finroc_control->co_right_knee_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/knee_right - Target Position");
//  finroc_control->co_right_elbow_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/elbow_right - Target Position");
//  finroc_control->co_right_shoulder_y_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/shoulder_y_right - Target Position");
//  finroc_control->co_right_shoulder_x_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/shoulder_x_right - Target Position");
//
//  finroc_control->co_left_stimulation_hip_y_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/hip_y_left - Stimulation Position");
//  finroc_control->co_left_stimulation_hip_x_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/hip_x_left - Stimulation Position");
//  finroc_control->co_left_stimulation_hip_z_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/hip_z_left - Stimulation Position");
//  finroc_control->co_left_stimulation_shoulder_y_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/shoulder_y_left - Stimulation Position");
//  finroc_control->co_left_stimulation_shoulder_x_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/shoulder_x_left - Stimulation Position");
//  finroc_control->co_left_stimulation_elbow_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/elbow_left - Stimulation Position");
//  finroc_control->co_left_stimulation_knee_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/knee_left - Stimulation Position");
//
//  finroc_control->co_left_hip_y_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/hip_y_left - Target Position");
//  finroc_control->co_left_hip_x_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/hip_x_left - Target Position");
//  finroc_control->co_left_hip_z_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/hip_z_left - Target Position");
//  finroc_control->co_left_knee_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/knee_left - Target Position");
//  finroc_control->co_left_elbow_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/elbow_left - Target Position");
//  finroc_control->co_left_shoulder_y_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/shoulder_y_left - Target Position");
//  finroc_control->co_left_shoulder_x_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/shoulder_x_left - Target Position");
//
//  finroc_control->co_spine_x_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/spine_x - Target Position");
//  finroc_control->co_stimulation_spine_x_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/spine_x - Stimulation Position");
//  finroc_control->co_spine_y_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/spine_y - Target Position");
//  finroc_control->co_stimulation_spine_y_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/spine_y - Stimulation Position");
//  finroc_control->co_spine_z_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/spine_z - Target Position");
//  finroc_control->co_stimulation_spine_z_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/spine_z - Stimulation Position");
//
//  // torque control inputs
//
//  finroc_control->co_left_stimulation_ankle_y_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/ankle_y_left - Stimulation Torque");
//  finroc_control->co_left_stimulation_ankle_x_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/ankle_x_left - Stimulation Torque");
//  finroc_control->co_right_stimulation_ankle_y_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/ankle_y_right - Stimulation Torque");
//  finroc_control->co_right_stimulation_ankle_x_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/ankle_x_right - Stimulation Torque");
//
//  finroc_control->co_left_ankle_y_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/ankle_y_left - Target Torque");
//  finroc_control->co_left_ankle_x_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/ankle_x_left - Target Torque");
//  finroc_control->co_right_ankle_y_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/ankle_y_right - Target Torque");
//  finroc_control->co_right_ankle_x_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/ankle_x_right - Target Torque");
//
//  finroc_control->co_right_stimulation_hip_y_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/hip_y_right - Stimulation Torque");
//  finroc_control->co_right_stimulation_hip_x_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/hip_x_right - Stimulation Torque");
//  finroc_control->co_right_stimulation_hip_z_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/hip_z_right - Stimulation Torque");
//  finroc_control->co_right_stimulation_shoulder_y_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/shoulder_y_right - Stimulation Torque");
//  finroc_control->co_right_stimulation_shoulder_x_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/shoulder_x_right - Stimulation Torque");
//  finroc_control->co_right_stimulation_elbow_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/elbow_right - Stimulation Torque");
//  finroc_control->co_right_stimulation_knee_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/knee_right - Stimulation Torque");
//
//  finroc_control->co_right_hip_y_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/hip_y_right - Target Torque");
//  finroc_control->co_right_hip_x_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/hip_x_right - Target Torque");
//  finroc_control->co_right_hip_z_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/hip_z_right - Target Torque");
//  finroc_control->co_right_knee_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/knee_right - Target Torque");
//  finroc_control->co_right_elbow_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/elbow_right - Target Torque");
//  finroc_control->co_right_shoulder_y_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/shoulder_y_right - Target Torque");
//  finroc_control->co_right_shoulder_x_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/shoulder_x_right - Target Torque");
//
//  finroc_control->co_left_stimulation_hip_y_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/hip_y_left - Stimulation Torque");
//  finroc_control->co_left_stimulation_hip_x_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/hip_x_left - Stimulation Torque");
//  finroc_control->co_left_stimulation_hip_z_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/hip_z_left - Stimulation Torque");
//  finroc_control->co_left_stimulation_shoulder_y_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/shoulder_y_left - Stimulation Torque");
//  finroc_control->co_left_stimulation_shoulder_x_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/shoulder_x_left - Stimulation Torque");
//  finroc_control->co_left_stimulation_elbow_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/elbow_left - Stimulation Torque");
//  finroc_control->co_left_stimulation_knee_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/knee_left - Stimulation Torque");
//
//  finroc_control->co_left_hip_y_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/hip_y_left - Target Torque");
//  finroc_control->co_left_hip_x_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/hip_x_left - Target Torque");
//  finroc_control->co_left_hip_z_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/hip_z_left - Target Torque");
//  finroc_control->co_left_knee_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/knee_left - Target Torque");
//  finroc_control->co_left_elbow_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/elbow_left - Target Torque");
//  finroc_control->co_left_shoulder_y_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/shoulder_y_left - Target Torque");
//  finroc_control->co_left_shoulder_x_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/shoulder_x_left - Target Torque");
//
//  finroc_control->co_spine_x_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/spine_x - Target Torque");
//  finroc_control->co_stimulation_spine_x_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/spine_x - Stimulation Torque");
//  finroc_control->co_spine_y_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/spine_y - Target Torque");
//  finroc_control->co_stimulation_spine_y_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/spine_y - Stimulation Torque");
//  finroc_control->co_spine_z_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/spine_z - Target Torque");
//  finroc_control->co_stimulation_spine_z_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/spine_z - Stimulation Torque");
//
//  // other control inputs from GUI
//  finroc_control->ci_head_force_x.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/head - Force X");
//  finroc_control->ci_head_force_y.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/head - Force Y");
//  finroc_control->ci_head_force_z.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/head - Force Z");
//  finroc_control->ci_torso_force_x.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/torso - Force X");
//  finroc_control->ci_torso_force_y.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/torso - Force Y");
//  finroc_control->ci_torso_force_z.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/torso - Force Z");
//  finroc_control->ci_pelvis_force_x.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/pelvis - Force X");
//  finroc_control->ci_pelvis_force_y.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/pelvis - Force Y");
//  finroc_control->ci_pelvis_force_z.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/pelvis - Force Z");
//  finroc_control->ci_plate_pos_x.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/plate pos - Target Pos X");
//  finroc_control->ci_plate_pos_y.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/plate pos - Target Pos Y");
//  finroc_control->ci_plate_pos_z.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/plate pos - Target Pos Z");
//
//  finroc_control->ci_plate_rot_x.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/plate rot - Target Angle X");
//  finroc_control->ci_plate_rot_y.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/plate rot - Target Angle Y");
//  finroc_control->ci_plate_rot_z.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/plate rot - Target Angle Z");
//  finroc_control->ci_switch_off_gravity.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Controller Input/Switch Off Gravity");
//
//
//  // sensor outputs
//  finroc_control->si_right_force_z.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/loadcell_right - Force Z");
//  finroc_control->si_left_force_z.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/loadcell_left - Force Z");
//
//  finroc_control->si_left_inner_heel_force.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/inner_heel_force_left - Force");
//  finroc_control->si_left_inner_toe_force.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/inner_toe_force_left - Force");
//  finroc_control->si_left_outer_heel_force.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/outer_heel_force_left - Force");
//  finroc_control->si_left_outer_toe_force.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/outer_toe_force_left - Force");
//
//  finroc_control->si_right_inner_heel_force.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/inner_heel_force_right - Force");
//  finroc_control->si_right_inner_toe_force.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/inner_toe_force_right - Force");
//  finroc_control->si_right_outer_heel_force.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/outer_heel_force_right - Force");
//  finroc_control->si_right_outer_toe_force.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/outer_toe_force_right - Force");
//
//  finroc_control->si_ins_accel_x.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/ins - Accel X");
//  finroc_control->si_ins_accel_y.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/ins - Accel Y");
//  finroc_control->si_ins_accel_z.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/ins - Accel Z");
//
//
//  finroc_control->si_ins_omega_x.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/ins - Omega X");
//  finroc_control->si_ins_omega_y.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/ins - Omega Y");
//  finroc_control->si_ins_omega_z.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/ins - Omega Z");
//
//  finroc_control->si_ins_roll.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/ins - Roll");
//  finroc_control->si_ins_pitch.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/ins - Pitch");
//  finroc_control->si_ins_yaw.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/ins - Yaw");
//
//
//
//  finroc_control->si_left_ankle_x_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/ankle_x_left - Angle");
//  finroc_control->si_left_ankle_x_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/ankle_x_left - Torque");
//  finroc_control->si_left_ankle_y_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/ankle_y_left - Angle");
//  finroc_control->si_left_ankle_y_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/ankle_y_left - Torque");
//
//  finroc_control->si_left_knee_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/knee_left - Angle");
//  finroc_control->si_left_knee_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/knee_left - Torque");
//  finroc_control->si_left_hip_x_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/hip_x_left - Angle");
//  finroc_control->si_left_hip_x_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/hip_x_left - Torque");
//  finroc_control->si_left_hip_y_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/hip_y_left - Angle");
//  finroc_control->si_left_hip_y_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/hip_y_left - Torque");
//  finroc_control->si_left_hip_z_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/hip_y_left - Angle");
//  finroc_control->si_left_hip_z_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/hip_y_left - Torque");
//
//  finroc_control->si_left_shoulder_x_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/shoulder_x_left - Angle");
//  finroc_control->si_left_shoulder_x_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/shoulder_x_left - Torque");
//  finroc_control->si_left_shoulder_y_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/shoulder_y_left - Angle");
//  finroc_control->si_left_shoulder_y_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/shoulder_y_left - Torque");
//  finroc_control->si_left_elbow_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/elbow_y_left - Angle");
//  finroc_control->si_left_elbow_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/elbow_y_left - Torque");
//
//  //right
//  finroc_control->si_right_ankle_x_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/ankle_x_right - Angle");
//  finroc_control->si_right_ankle_x_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/ankle_x_right - Torque");
//  finroc_control->si_right_ankle_y_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/ankle_y_right - Angle");
//  finroc_control->si_right_ankle_y_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/ankle_y_right - Torque");
//
//  finroc_control->si_right_knee_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/knee_right - Angle");
//  finroc_control->si_right_knee_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/knee_right - Torque");
//  finroc_control->si_right_hip_x_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/hip_x_right - Angle");
//  finroc_control->si_right_hip_x_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/hip_x_right - Torque");
//  finroc_control->si_right_hip_y_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/hip_y_right - Angle");
//  finroc_control->si_right_hip_y_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/hip_y_right - Torque");
//  finroc_control->si_right_hip_z_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/hip_y_right - Angle");
//  finroc_control->si_right_hip_z_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/hip_y_right - Torque");
//
//  finroc_control->si_right_shoulder_x_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/shoulder_x_right - Angle");
//  finroc_control->si_right_shoulder_x_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/shoulder_x_right - Torque");
//  finroc_control->si_right_shoulder_y_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/shoulder_y_right - Angle");
//  finroc_control->si_right_shoulder_y_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/shoulder_y_right - Torque");
//  finroc_control->si_right_elbow_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/elbow_y_right - Angle");
//  finroc_control->si_right_elbow_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/elbow_y_right - Torque");
//
//  finroc_control->si_spine_x_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/spine_x - Angle");
//  finroc_control->si_spine_x_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/spine_x - Torque");
//  finroc_control->si_spine_y_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/spine_y - Angle");
//  finroc_control->si_spine_y_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/spine_y - Torque");
//  finroc_control->si_spine_z_angle.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/spine_z - Angle");
//  finroc_control->si_spine_z_torque.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/spine_z - Torque");

  main_thread->SetCycleTime(std::chrono::milliseconds(25));

//  if (use_simulation_time)
//  {
//    finroc::biped::utils::tApplicationTimeInputPorts* time_inputs = new finroc::biped::utils::tApplicationTimeInputPorts();
//    time_inputs->Init();
//    time_inputs->sec.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/Timestamp Sec");
//    time_inputs->msec.ConnectTo("/MCA/PhysicsEngine/<PhysicsEngine>/Sensor Output/Timestamp Msec");
//  }
}
