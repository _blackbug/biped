//
// You received this file as part of Finroc
// A framework for intelligent robot control
//
// Copyright (C) Finroc GbR (finroc.org)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//
//----------------------------------------------------------------------
/*!\file    projects/biped/control/gSpinalCord.cpp
 *
 * \author  Jie Zhao
 *
 * \date    2014-01-06
 *
 */
//----------------------------------------------------------------------
#include "projects/biped/control/gSpinalCord.h"
#include "projects/biped/control/gbbDisturbanceEstimationCompensationHip.h"
#include "projects/biped/control/gbbLearningModuleBehaviors.h"

#include "plugins/ib2c/mbbFusion.h"

#include "projects/biped/skills/mAverageFilter.h"
#include "projects/biped/skills/mMaxFilter.h"

#include "projects/biped/skills/mbbUberSkill.h"
#include "projects/biped/skills/mbbStimulationSkill.h"
#include "projects/biped/skills/mbbWalkingCPG.h"
//#include "projects/biped/skills/mbbWalkingLatCPG.h"
#include "projects/biped/skills/mbbSpinalCordWalkingInitiation.h"
#include "projects/biped/skills/mbbSpinalCordWalkingTermination.h"
#include "projects/biped/skills/mbbBalanceAnklePitch.h"
#include "projects/biped/skills/mbbRelaxKnee.h"
#include "projects/biped/skills/mbbRelaxHipY.h"
#include "projects/biped/skills/mbbRelaxSpine.h"
#include "projects/biped/skills/mbbReduceTensionY.h"
#include "projects/biped/utils/mGroundContact.h"
#include "projects/biped/utils/mCoMEstimation.h"
#include "projects/biped/utils/mCoMFusion.h"
#include "projects/biped/utils/mFlyWheelMotorTime.h"
#include "projects/biped/utils/mStabilityAnalysis.h"
#include "projects/biped/skills/mbbControlAnklePitch.h"
#include "projects/biped/skills/mbbControlForwardVelocity.h"
#include "projects/biped/skills/mbbForwardVelocity.h"
#include "projects/biped/skills/mbbLateralVelocity.h"
#include "projects/biped/skills/mbbFrontalVelocity.h"
#include "projects/biped/skills/mbbControlFootGesture.h"
#include "projects/biped/skills/mWalkingVelocity.h"
#include "projects/biped/utils/mConvertVectorToSingleValue.h"

#include "projects/biped/dec/mbbEstimationGravityTorque.h"
#include "projects/biped/dec/mbbEstimationPlateRotation.h"
#include "projects/biped/dec/mbbEstimationTorqueToAngle.h"
#include "projects/biped/dec/mbbEstimationExternalTorque.h"
#include "projects/biped/dec/mbbEstimationTranslationalMovement.h"
#include "projects/biped/dec/mbbEstimationGravityTorqueLateral.h"
#include "projects/biped/dec/mbbEstimationPlateRotationLateral.h"
#include "projects/biped/dec/mbbEstimationTorqueToAngleLateral.h"
#include "projects/biped/dec/mbbEstimationExternalTorqueLateral.h"
#include "projects/biped/dec/mbbEstimationComTranslationLateral.h"

#include "projects/biped/reflexes/mbbLockKnee.h"
//----------------------------------------------------------------------
// External includes (system with <>, local with "")
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Internal includes with ""
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Debugging
//----------------------------------------------------------------------
#include <cassert>

// Namespace usage
//----------------------------------------------------------------------
using namespace finroc::ib2c;
using namespace finroc::biped::skills;
using namespace finroc::biped::utils;
using namespace finroc::biped::dec;
using namespace finroc::biped::reflexes;
//----------------------------------------------------------------------
// Namespace declaration
//----------------------------------------------------------------------
namespace finroc
{
namespace biped
{
namespace control
{

//----------------------------------------------------------------------
// Forward declarations / typedefs / enums
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Const values
//----------------------------------------------------------------------
static runtime_construction::tStandardCreateModuleAction<gSpinalCord> cCREATE_ACTION_FOR_G_SPINALCORD("SpinalCord");

//----------------------------------------------------------------------
// Implementation
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// gSpinalCord constructor
//----------------------------------------------------------------------
gSpinalCord::gSpinalCord(core::tFrameworkElement *parent, const std::string &name,
                         const std::string &structure_config_file) :
  tSenseControlGroup(parent, name, structure_config_file, true) // change to 'true' to make group's ports shared (so that ports in other processes can connect to its output and/or input ports)
{
  // group
  gbbLearningModuleBehaviors * learning_module_group = new gbbLearningModuleBehaviors(this, "(G) Learning Module Lower Trunk ");

  // add modules
  // ============================================

  //   STANDING CONTROL UNITS
  // ================================================
  mbbUberSkill* skill_stand = new mbbUberSkill(this, "(LM) Stand", tStimulationMode::AUTO, 1);
  skill_stand->number_of_inhibition_ports.Set(1);
  skill_stand->CheckStaticParameters();

  mbbBalanceAnklePitch * skill_balance_ankle_pitch = new mbbBalanceAnklePitch(this, "(PR) Balance Ankle Pitch", tStimulationMode::AUTO);

  mbbRelaxKnee * skill_relax_knee = new mbbRelaxKnee(this, "(PR) Relax Knee", tStimulationMode::AUTO);

  mbbRelaxHipY * skill_relax_hip_y = new mbbRelaxHipY(this, "(PR) Relax Hip Y", tStimulationMode::AUTO);

  mbbRelaxSpine * skill_relax_spine_x = new mbbRelaxSpine(this, "(PR) Relax Spine X", tStimulationMode::AUTO);

  mbbRelaxSpine * skill_relax_spine_y = new mbbRelaxSpine(this, "(PR) Relax Spine Y", tStimulationMode::AUTO);

  mWalkingVelocity* skill_walking_velocity = new mWalkingVelocity( this, "Walking Velocity");

  // reduce tension

  mbbReduceTensionY * skill_reduce_tension_ankle_y = new mbbReduceTensionY(this, "(PR) Reduce Tension Ankle Y", tStimulationMode::AUTO);

  mbbReduceTensionY * skill_reduce_tension_hip_y = new mbbReduceTensionY(this, "(PR) Reduce Tension Hip Y", tStimulationMode::AUTO);


  // disturbance estimation

  gbbDisturbanceEstimationCompensationHip * estimation_hip_group = new gbbDisturbanceEstimationCompensationHip(this, "(G) DEC Double Inverted Pendulum", tStimulationMode::AUTO);


  mbbEstimationGravityTorque * skill_estimation_gravi_torque = new mbbEstimationGravityTorque(this, "(PR) Gravity Torque Estim.", tStimulationMode::AUTO);

  mbbEstimationPlateRotation * skill_estimation_plate_rotation = new mbbEstimationPlateRotation(this, "(PR) Plate Rotation Estim.", tStimulationMode::AUTO);

  mbbEstimationTorqueToAngle * skill_estimation_torque_to_angle = new mbbEstimationTorqueToAngle(this, " Estim. Torque to Angle", tStimulationMode::AUTO);

  mbbEstimationExternalTorque * skill_estimation_external_torque = new mbbEstimationExternalTorque(this, " (PR) External Torque Estim.", tStimulationMode::AUTO);

  mbbEstimationTranslationalMovement * skill_estimation_translational_movement = new mbbEstimationTranslationalMovement(this, " (PR) Translational Movement Estim.", tStimulationMode::AUTO);

  mbbEstimationGravityTorqueLateral * skill_estimation_gravi_torque_lateral = new mbbEstimationGravityTorqueLateral(this, " (PR) Gravity Torque Lateral Estim.", tStimulationMode::ENABLED);

  mbbEstimationTorqueToAngleLateral * skill_estimation_torque_to_angle_lateral = new mbbEstimationTorqueToAngleLateral(this, " Estim. Torque to Angle Lateral", tStimulationMode::ENABLED);

  mbbEstimationPlateRotationLateral * skill_estimation_plate_rotation_lateral = new mbbEstimationPlateRotationLateral(this, " (PR) Plate Rotation Lateral Estim.", tStimulationMode::ENABLED);

  mbbEstimationExternalTorqueLateral * skill_estimation_external_torque_lateral = new mbbEstimationExternalTorqueLateral(this, " (PR) External Torque Lateral Estim.", tStimulationMode::ENABLED);

  mbbEstimationComTranslationLateral * skill_estimation_com_translation_lateral = new mbbEstimationComTranslationLateral(this, " (PR) Estim. Com Transformation Lateral", tStimulationMode::AUTO);

  // WALKING CONTROL UNITS
  // ================================================

  mbbSpinalCordWalkingInitiation* skill_walking_init_left = new mbbSpinalCordWalkingInitiation(this, "(Ph) Walking Initiation Left", tStimulationMode::AUTO);
  mbbSpinalCordWalkingInitiation* skill_walking_init_right = new mbbSpinalCordWalkingInitiation(this, "(Ph) Walking Initiation Right", tStimulationMode::AUTO);

  mbbWalkingCPG* skill_walking_cpg = new mbbWalkingCPG(this, "(SPG) Walking", tStimulationMode::AUTO);

  //mbbWalkingLatCPG* skill_walking_lateral_cpg = new mbbWalkingLatCPG(this, "(SPG) Walking Lateral", tStimulationMode::AUTO);

  mbbSpinalCordWalkingTermination* skill_walking_termination = new mbbSpinalCordWalkingTermination(this, "(Ph) Walking Termination", tStimulationMode::AUTO);

  // left
  mbbUberSkill* skill_walking_phase1_left = new mbbUberSkill(this, "(Ph) Walking Phase 1 Left", tStimulationMode::AUTO);

  mbbUberSkill* skill_walking_phase2_left = new mbbUberSkill(this, "(Ph) Walking Phase 2 Left", tStimulationMode::AUTO);

  mbbUberSkill* skill_walking_phase3_left = new mbbUberSkill(this, "(Ph) Walking Phase 3 Left", tStimulationMode::AUTO);

  mbbUberSkill* skill_walking_phase4_left = new mbbUberSkill(this, "(Ph) Walking Phase 4 Left", tStimulationMode::AUTO);
  //  mbbStimulationSkill* skill_walking_phase4_left = new mbbStimulationSkill(this, "(Ph) Walking Phase 4 Left", beh_bb_handler);
  //  skill_walking_phase4_left->RegisterStimulatedBehaviour("Lift Leg Left", 0.0);
  ////  skill_walking_phase4_left->RegisterStimulatedBehaviour("Control Ankle Pitch", 0.4);
  //  skill_walking_phase4_left->SetStimulationMode(tBehaviourDefinitions::eSM_AUTO);
  //  skill_walking_phase4_left->FinalizePortsAndEdges();

  mbbUberSkill* skill_walking_phase5_left = new mbbUberSkill(this, "(Ph) Walking Phase 5 Left", tStimulationMode::AUTO);

  // right
  mbbUberSkill* skill_walking_phase1_right = new mbbUberSkill(this, "(Ph) Walking Phase 1 Right", tStimulationMode::AUTO);

  mbbUberSkill* skill_walking_phase2_right = new mbbUberSkill(this, "(Ph) Walking Phase 2 Right", tStimulationMode::AUTO);

  mbbUberSkill* skill_walking_phase3_right = new mbbUberSkill(this, "(Ph) Walking Phase 3 Right", tStimulationMode::AUTO);

  mbbUberSkill* skill_walking_phase4_right = new mbbUberSkill(this, "(Ph) Walking Phase 4 Right", tStimulationMode::AUTO);
  //  mbbStimulationSkill* skill_walking_phase4_right = new mbbStimulationSkill(this, "(Ph) Walking Phase 4 Right", beh_bb_handler);
  //  skill_walking_phase4_right->RegisterStimulatedBehaviour("Lift Leg Right", 0.0);
  ////  skill_walking_phase4_right->RegisterStimulatedBehaviour("Control Ankle Pitch", 0.5);
  //  skill_walking_phase4_right->SetStimulationMode(tBehaviourDefinitions::eSM_AUTO);
  //  skill_walking_phase4_right->FinalizePortsAndEdges();

  mbbUberSkill* skill_walking_phase5_right = new mbbUberSkill(this, "(Ph) Walking Phase 5 Right", tStimulationMode::AUTO);

  // walking lateral control units
  mbbUberSkill* skill_walking_lateral_phase1_left = new mbbUberSkill(this, "(Ph) Walking Lateral Ph.1 Left", tStimulationMode::AUTO);

  mbbUberSkill* skill_walking_lateral_phase2_left = new mbbUberSkill(this, "(Ph) Walking Lateral Ph.2 Left", tStimulationMode::AUTO);

  mbbUberSkill* skill_walking_lateral_phase3_left = new mbbUberSkill(this, "(Ph) Walking Lateral Ph.3 Left", tStimulationMode::AUTO);

  mbbUberSkill* skill_walking_lateral_phase4_left = new mbbUberSkill(this, "(Ph) Walking Lateral Ph.4 Left", tStimulationMode::AUTO);

  // right
  mbbUberSkill* skill_walking_lateral_phase1_right = new mbbUberSkill(this, "(Ph) Walking Lateral Ph.1 Right", tStimulationMode::AUTO);

  mbbUberSkill* skill_walking_lateral_phase2_right = new mbbUberSkill(this, "(Ph) Walking Lateral Ph.2 Right", tStimulationMode::AUTO);

  mbbUberSkill* skill_walking_lateral_phase3_right = new mbbUberSkill(this, "(Ph) Walking Lateral Ph.3 Right", tStimulationMode::AUTO);

  mbbUberSkill* skill_walking_lateral_phase4_right = new mbbUberSkill(this, "(Ph) Walking Lateral Ph.4 Right", tStimulationMode::AUTO);

  mbbLockKnee* skill_walking_lock_knee = new mbbLockKnee(this, "(R) Lock Knee", tStimulationMode::AUTO, 0, 0.2);

  // forward velocity correction
  mbbForwardVelocity* postural_reflex_forward_velocity_left = new mbbForwardVelocity(this, "(PR) Forward Velocity Left", tStimulationMode::AUTO);
  mbbForwardVelocity* postural_reflex_forward_velocity_right = new mbbForwardVelocity(this, "(PR) Forward Velocity Right", tStimulationMode::AUTO);
  // lateral velocity correction
  mbbLateralVelocity* postural_reflex_lateral_velocity_left = new mbbLateralVelocity(this,  "(PR) Lateral Velocity Left", tStimulationMode::AUTO, 0, -1.0);
  mbbLateralVelocity* postural_reflex_lateral_velocity_right = new mbbLateralVelocity(this, "(PR) Lateral Velocity Right", tStimulationMode::AUTO, 0, 1.0);
  //frontal velocity correction
  mbbFrontalVelocity* postural_reflex_frontal_velocity_left = new mbbFrontalVelocity(this, "(PR) Frontal Velocity Left", tStimulationMode::AUTO, 1);
  postural_reflex_frontal_velocity_left->number_of_inhibition_ports.Set(1);
  postural_reflex_frontal_velocity_left->CheckStaticParameters();
  mbbFrontalVelocity* postural_reflex_frontal_velocity_right = new mbbFrontalVelocity(this, "(PR) Frontal Velocity Right", tStimulationMode::AUTO, 1);
  postural_reflex_frontal_velocity_right->number_of_inhibition_ports.Set(1);
  postural_reflex_frontal_velocity_right->CheckStaticParameters();

//  mbbUberSkill* skill_lift_leg_left = new mbbUberSkill(this, "(S) Lift Leg Left", tStimulationMode::AUTO);
//
//  mbbUberSkill* skill_lift_leg_right = new mbbUberSkill(this, "(S) Lift Leg Right", tStimulationMode::AUTO);

  // lateral walking //todo
//  mbbUberSkill* skill_step_left_leg_left = new mbbUberSkill(this, "(S) Step Left Leg Left", tStimulationMode::AUTO);
//
//  mbbUberSkill* skill_step_right_leg_left = new mbbUberSkill(this, "(S) Step Right Leg Left", tStimulationMode::AUTO);
//
//  mbbUberSkill* skill_step_left_leg_right = new mbbUberSkill(this, "(S) Step Left Leg Right", tStimulationMode::AUTO);
//
//  mbbUberSkill* skill_step_right_leg_right = new mbbUberSkill(this, "(S) Step Right Leg Right", tStimulationMode::AUTO);

  //todo : adding fusion blocks

  // calculate the foot gesture
  mbbControlFootGesture* skill_calc_foot_gesture_left = new mbbControlFootGesture(this, "(R) Calculate Foot Gesture Left", tStimulationMode::AUTO);

  mbbControlFootGesture* skill_calc_foot_gesture_right = new mbbControlFootGesture(this, "(R) Calculate Foot Gesture Right", tStimulationMode::AUTO);
  //END skills

  //BEGIN fusion behaviours
//  mbbFusion<double>* fusion_lift_leg_left = new mbbFusion<double>(this,  "Lift Leg Left", 0, tStimulationMode::ENABLED);
//  fusion_lift_leg_left->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
//  mbbFusion<double>* fusion_lift_leg_right = new mbbFusion<double>(this,  "Lift Leg Right",  0, tStimulationMode::ENABLED);
//  fusion_lift_leg_right->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);

  mbbFusion<double>* fusion_forward_velocity_left = new mbbFusion<double>(this,  "Forward Velocity Left",  2, tStimulationMode::ENABLED);
  fusion_forward_velocity_left->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
  mbbFusion<double>* fusion_forward_velocity_right = new mbbFusion<double>(this,  "Forward Velocity Right",  2, tStimulationMode::ENABLED);
  fusion_forward_velocity_right->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);

  mbbFusion<double>* fusion_lateral_velocity_left = new mbbFusion<double>(this,  "Lateral Velocity Left",  2, tStimulationMode::ENABLED);
  fusion_lateral_velocity_left->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
  mbbFusion<double>* fusion_lateral_velocity_right = new mbbFusion<double>(this,  "Lateral Velocity Right", 2, tStimulationMode::ENABLED);
  fusion_lateral_velocity_right->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);

  mbbFusion<double>* fusion_frontal_velocity_left = new mbbFusion<double>(this,  "Frontal Velocity Left", 2, tStimulationMode::ENABLED);
  fusion_frontal_velocity_left->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);
  mbbFusion<double>* fusion_frontal_velocity_right = new mbbFusion<double>(this,  "Frontal Velocity Right", 2, tStimulationMode::ENABLED);
  fusion_frontal_velocity_right->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);

  //Fusion of termination and walking cyclic
//  mbbFusion<double>* fusion_walking_termination = new mbbFusion<double>(this, "(F) Walking CPG Fusion", 2, tStimulationMode::ENABLED);
//  fusion_walking_termination->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);

  mMaxFilter* fusion_walking_termination = new mMaxFilter(this, "(F) Walking CPG Fusion", 1, 2);



  //END fusion behaviours

  // fusion of walking cyclic and lateral
  mbbFusion<double>* fusion_stand_inhibition = new mbbFusion<double>(this,  "Inhibiting Stand", 1, tStimulationMode::ENABLED);
  fusion_stand_inhibition->fusion_method.Set(tFusionMethod::WEIGHTED_AVERAGE);

  //BEGIN other modules

  mGroundContact * ground_contact = new mGroundContact(this, "Ground Contact");

  mFlyWheelMotorTime * fly_wheel_motor_time = new mFlyWheelMotorTime(this, "Fly Wheel");

  // stability analysis
  mStabilityAnalysis * left_stability_analysis = new mStabilityAnalysis(this, "Left Stability Analysis");

  mStabilityAnalysis * right_stability_analysis = new mStabilityAnalysis(this, "Right Stability Analysis");


  mCoMEstimation* com_estim_x_left = new mCoMEstimation(this, "CoM Estim. X Left", 0.14, 0.045, 0.15, -0.85, false, 3.3);
  mCoMEstimation* com_estim_x_right = new mCoMEstimation(this, "CoM Estim. X Right", 0.14, 0.045, 0.15, -0.85, false, 3.3);

  mCoMEstimation* com_estim_y_left = new mCoMEstimation(this, "CoM Estim. Y Left", 0.14, -0.12, 1.0, -1.0, true, 4.4);
  mCoMEstimation* com_estim_y_right = new mCoMEstimation(this, "CoM Estim. Y Right", 0.14, 0.12, -1.0, 1.0, true, 4.4);

  tConstDescription com_filter_io_ctrl[] = {"CoM X Left", "XcoM X Left", "CoM X Right", "XcoM X Right", "CoM Y Left", "XcoM Y Left", "CoM Y Right", "XcoM Y Right"};
  enum
  {
    eCOMFILTER_COM_X_LEFT,
    eCOMFILTER_XCOM_X_LEFT,
    eCOMFILTER_COM_X_RIGHT,
    eCOMFILTER_XCOM_X_RIGHT,
    eCOMFILTER_COM_Y_LEFT,
    eCOMFILTER_XCOM_Y_LEFT,
    eCOMFILTER_COM_Y_RIGHT,
    eCOMFILTER_XCOM_Y_RIGHT,
  };
  mAverageFilter* com_filter = new mAverageFilter(this, "CoM Filter Control", 8, 3, mAverageFilter::eAFM_EXACT, mAverageFilter::eCICO, true,  com_filter_io_ctrl);
  mCoMFusion* com_estim_fusion = new mCoMFusion(this, "CoM Estim. Fusion");


  tConstDescription com_filter_io_sense[] = {"CoM X Left", "XcoM X Left", "CoM X Right", "XcoM X Right", "CoM Y Left", "XcoM Y Left", "CoM Y Right", "XcoM Y Right"};
  enum
  {
    eSENSE_COMFILTER_COM_X_LEFT,
    eSENSE_COMFILTER_XCOM_X_LEFT,
    eSENSE_COMFILTER_COM_X_RIGHT,
    eSENSE_COMFILTER_XCOM_X_RIGHT,
    eSENSE_COMFILTER_COM_Y_LEFT,
    eSENSE_COMFILTER_XCOM_Y_LEFT,
    eSENSE_COMFILTER_COM_Y_RIGHT,
    eSENSE_COMFILTER_XCOM_Y_RIGHT,
  };
  mAverageFilter* com_filter_sense = new mAverageFilter(this, "CoM Filter Sense", 8, 3, mAverageFilter::eAFM_EXACT, mAverageFilter::eSISO, true, com_filter_io_sense);


  // fusion of target ratings of relax behaviours
  mMaxFilter* fusion_max_adjust_angle = new mMaxFilter(this, "(F) Max Target Rating Adjust Angle", 1, 7);

  //BEGIN controller propagation todo:find the ib2c violation
  this->GetControllerInputs().ConnectByName(this->GetControllerOutputs(), false);
// this->ci_walking_scale_factor.ConnectTo(this->co_walking_scale_factor);
// this->ci_walking_laterally.ConnectTo(this->co_walking_scale_laterally);
//  this->ci_stiffness_factor.ConnectTo(this->co_stiffness_factor);
//  this->ci_quiet_stand.ConnectTo(this->co_quiet_stand);
//  this->ci_push_recovery_stand.ConnectTo(this->co_push_recovery_stand);
//  this->ci_actual_accel_x.ConnectTo(this->co_actual_accel_x);
//  this->ci_actual_accel_y.ConnectTo(this->co_actual_accel_y);
//  this->ci_actual_accel_z.ConnectTo(this->co_actual_accel_z);
//  this->ci_actual_omega_x.ConnectTo(this->co_actual_omega_x);
//  this->ci_actual_omega_y.ConnectTo(this->co_actual_omega_y);
//  this->ci_actual_omega_z.ConnectTo(this->co_actual_omega_z);
//  this->ci_actual_roll.ConnectTo(this->co_actual_roll);
//  this->ci_actual_pitch.ConnectTo(this->co_actual_pitch);
//  this->ci_actual_yaw.ConnectTo(this->co_actual_yaw);
//  this->ci_adjusted_accel_x.ConnectTo(this->co_adjusted_accel_x);
//  this->ci_adjusted_accel_y.ConnectTo(this->co_adjusted_accel_y);
//  this->ci_adjusted_accel_z.ConnectTo(this->co_adjusted_accel_z);
//  this->ci_adjusted_roll.ConnectTo(this->co_adjusted_roll);
//  this->ci_adjusted_pitch.ConnectTo(this->co_adjusted_pitch);
  //END controller propagation


  // connections to learning module group
  skill_walking_cpg->activity.ConnectTo(learning_module_group->ci_walking_stimulation);
  ground_contact->out_left_foot_load.ConnectTo(learning_module_group->ci_foot_load_left);
  ground_contact->out_right_foot_load.ConnectTo(learning_module_group->ci_foot_load_right);
  this->si_left_hip_y_angle.ConnectTo(learning_module_group->si_left_hip_angle);
  this->si_lock_hip_left_torque.ConnectTo(learning_module_group->si_lock_hip_left_torque);
  this->si_lock_hip_right_torque.ConnectTo(learning_module_group->si_lock_hip_right_torque);
  this->ci_walking_scale_factor.ConnectTo(learning_module_group->si_walking_velocity);
  this->si_pelvis_position_x.ConnectTo(learning_module_group->si_pelvis_position_x);
  this->si_pelvis_position_y.ConnectTo(learning_module_group->si_pelvis_position_y);
  this->si_left_foot_position_x.ConnectTo(learning_module_group->si_left_foot_position_x);
  this->si_left_foot_position_y.ConnectTo(learning_module_group->si_left_foot_position_y);
  this->si_left_foot_position_z.ConnectTo(learning_module_group->si_left_foot_position_z);
  this->si_right_foot_position_x.ConnectTo(learning_module_group->si_right_foot_position_x);
  this->si_right_foot_position_y.ConnectTo(learning_module_group->si_right_foot_position_y);
  this->si_right_foot_position_z.ConnectTo(learning_module_group->si_right_foot_position_z);
  this->si_left_ankle_x_angle.ConnectTo(learning_module_group->si_left_ankle_angle_lateral);
  this->si_right_ankle_x_angle.ConnectTo(learning_module_group->si_right_ankle_angle_lateral);
  this->si_left_knee_angle.ConnectTo(learning_module_group->si_left_knee_angle);
  this->si_right_knee_angle.ConnectTo(learning_module_group->si_right_knee_angle);
  this->si_left_ankle_y_angle.ConnectTo(learning_module_group->si_left_ankle_angle);
  this->si_right_ankle_y_angle.ConnectTo(learning_module_group->si_right_ankle_angle);
  this->si_left_hip_y_angle.ConnectTo(learning_module_group->si_left_hip_angle);
  this->si_right_hip_y_angle.ConnectTo(learning_module_group->si_right_hip_angle);

  //this->ci_push_detected_walking_lateral.ConnectTo(learning_module_group->ci_push_detected_y);
  this->ci_push_started.ConnectTo(learning_module_group->ci_push_detected_y);
  this->ci_push_started_x.ConnectTo(learning_module_group->ci_push_started_x);
  this->ci_push_detected_x.ConnectTo(learning_module_group->ci_push_detected_x);
  this->ci_torso_force_y.ConnectTo(learning_module_group->ci_torso_force_y);
  this->ci_torso_force_x.ConnectTo(learning_module_group->ci_torso_force_x);

  skill_walking_cpg->co_left_phase.ConnectTo(learning_module_group->ci_walking_left_phase);
  skill_walking_cpg->co_right_phase.ConnectTo(learning_module_group->ci_walking_right_phase);
  learning_module_group->so_new_running.ConnectTo(this->so_new_running);
  this->ci_actual_pitch.ConnectTo(learning_module_group->ci_pitch_angle);
  this->ci_actual_roll.ConnectTo(learning_module_group->ci_roll_angle);
  this->ci_linear_velocity_x.ConnectTo(learning_module_group->ci_linear_velocity_x);
  this->ci_reset_simulation_experiments.ConnectTo(learning_module_group->ci_reset_simulation_experiments);
  this->ci_walking_scale_factor.ConnectTo(learning_module_group->ci_walking_scale);
  postural_reflex_lateral_velocity_left->co_correction.ConnectTo(learning_module_group->ci_left_correction_lateral);
  postural_reflex_lateral_velocity_right->co_correction.ConnectTo(learning_module_group->ci_right_correction_lateral);
  postural_reflex_forward_velocity_left->co_correction.ConnectTo(learning_module_group->ci_left_correction_sagittal);
  postural_reflex_forward_velocity_right->co_correction.ConnectTo(learning_module_group->ci_right_correction_sagittal);

  skill_walking_cpg->co_left_stimulation_phase_1.ConnectTo(learning_module_group->ci_walking_left_1);
  skill_walking_cpg->co_left_stimulation_phase_2.ConnectTo(learning_module_group->ci_walking_left_2);
  skill_walking_cpg->co_left_stimulation_phase_3.ConnectTo(learning_module_group->ci_walking_left_3);
  skill_walking_cpg->co_left_stimulation_phase_4.ConnectTo(learning_module_group->ci_walking_left_4);
  skill_walking_cpg->co_left_stimulation_phase_5.ConnectTo(learning_module_group->ci_walking_left_5);
  skill_walking_cpg->co_left_phase.ConnectTo(learning_module_group->ci_walking_left_phase);
  skill_walking_cpg->co_right_phase.ConnectTo(learning_module_group->ci_walking_right_phase);

  skill_walking_cpg->co_right_stimulation_phase_1.ConnectTo(learning_module_group->ci_walking_right_1);
  skill_walking_cpg->co_right_stimulation_phase_2.ConnectTo(learning_module_group->ci_walking_right_2);
  skill_walking_cpg->co_right_stimulation_phase_3.ConnectTo(learning_module_group->ci_walking_right_3);
  skill_walking_cpg->co_right_stimulation_phase_4.ConnectTo(learning_module_group->ci_walking_right_4);
  skill_walking_cpg->co_right_stimulation_phase_5.ConnectTo(learning_module_group->ci_walking_right_5);

  com_estim_fusion->co_com_x.ConnectTo(learning_module_group->ci_com_x);
  com_estim_fusion->co_com_y.ConnectTo(learning_module_group->ci_com_y);
  com_estim_fusion->co_xcom_x.ConnectTo(learning_module_group->ci_xcom_x);

  com_estim_x_left->co_xcom.ConnectTo(learning_module_group->ci_xcom_x_left);
  com_estim_x_left->co_com_angle.ConnectTo(learning_module_group->ci_com_x_left);

  com_estim_x_right->co_xcom.ConnectTo(learning_module_group->ci_xcom_x_right);
  com_estim_x_right->co_com_angle.ConnectTo(learning_module_group->ci_com_x_right);

  com_estim_y_left->co_xcom.ConnectTo(learning_module_group->ci_xcom_y_left);
  com_estim_y_right->co_xcom.ConnectTo(learning_module_group->ci_xcom_y_right);

  learning_module_group->so_loop_times.ConnectTo(this->so_loop_times);
  learning_module_group->co_hip_swing_action1.ConnectTo(this->co_hip_swing_action1);
  learning_module_group->co_hip_swing_action2.ConnectTo(this->co_hip_swing_action2);
  learning_module_group->co_hip_swing_action3.ConnectTo(this->co_hip_swing_action3);
  learning_module_group->co_hip_swing_action4.ConnectTo(this->co_hip_swing_action4);

  learning_module_group->co_leg_propel_action1.ConnectTo(this->co_leg_propel_action1);
  learning_module_group->co_leg_propel_action2.ConnectTo(this->co_leg_propel_action2);
  learning_module_group->co_leg_propel_action3.ConnectTo(this->co_leg_propel_action3);
  learning_module_group->co_leg_propel_action4.ConnectTo(this->co_leg_propel_action4);
  learning_module_group->so_loop_times.ConnectTo(this->co_loop_times);
  postural_reflex_forward_velocity_left->co_correction.ConnectTo(learning_module_group->ci_left_correction_sagittal);
  postural_reflex_forward_velocity_right->co_correction.ConnectTo(learning_module_group->ci_right_correction_sagittal);
  learning_module_group->co_left_learning_ankle_stiffness.ConnectTo(this->co_left_learning_ankle_stiffness);
  learning_module_group->co_right_learning_ankle_stiffness.ConnectTo(this->co_right_learning_ankle_stiffness);

  learning_module_group->co_left_learning_torque_factor.ConnectTo(this->co_left_learning_torque_factor);
  learning_module_group->co_right_learning_torque_factor.ConnectTo(this->co_right_learning_torque_factor);

  learning_module_group->co_obstacle_walking_stimulation.ConnectTo(this->co_obstacle_walking_stimulation);
  learning_module_group->co_obstacle_step.ConnectTo(this->co_obstacle_step);
  learning_module_group->co_output_hip_swing_1.ConnectTo(this->co_obstacle_hip_swing_1);
  learning_module_group->co_output_hip_swing_2.ConnectTo(this->co_obstacle_hip_swing_2);
  learning_module_group->co_output_hip_swing_3.ConnectTo(this->co_obstacle_hip_swing_3);
  learning_module_group->co_output_hip_swing_3_rear.ConnectTo(this->co_obstacle_hip_swing_3_rear);
  learning_module_group->co_output_knee_flexion_1.ConnectTo(this->co_obstacle_knee_flexion_1);
  learning_module_group->co_output_knee_flexion_2.ConnectTo(this->co_obstacle_knee_flexion_2);
  learning_module_group->co_output_knee_flexion_2_rear.ConnectTo(this->co_obstacle_knee_flexion_2_rear);
  learning_module_group->co_output_lock_hip_1.ConnectTo(this->co_obstacle_lock_hip_1);
  learning_module_group->co_output_lock_hip_2.ConnectTo(this->co_obstacle_lock_hip_2);
  learning_module_group->co_output_lock_hip_3.ConnectTo(this->co_obstacle_lock_hip_3);

  // PSO for Slow walking
//  skill_walking_lock_knee->co_knee_velocity.ConnectTo(learning_module_group->co_knee_velocity);
//  skill_walking_lock_knee->co_knee_angle.ConnectTo(learning_module_group->co_knee_angle);
  skill_walking_velocity->co_walking_velocity.ConnectTo(learning_module_group->co_walking_velocity);
  skill_walking_velocity->co_walking_distance.ConnectTo(learning_module_group->co_walking_distance);
  skill_walking_velocity->co_step_length.ConnectTo(learning_module_group->co_step_length);
  this->ci_reset_simulation_experiments.ConnectTo(skill_walking_velocity->ci_reset_simulation_experiments);
  learning_module_group->co_slow_walking_stimulation.ConnectTo(this->co_slow_walking_stimulation);
  learning_module_group->co_slow_walking_on.ConnectTo(this->co_slow_walking_on);
  learning_module_group->co_fast_walking_stimulation.ConnectTo(this->co_fast_walking_stimulation);
  learning_module_group->co_fast_walking_on.ConnectTo(this->co_fast_walking_on);
  learning_module_group->co_slow_walking_ahs_max_torque.ConnectTo(this->co_slow_walking_ahs_max_torque);
  learning_module_group->co_slow_walking_ahs_t1.ConnectTo(this->co_slow_walking_ahs_t1);
  learning_module_group->co_slow_walking_ahs_t2.ConnectTo(this->co_slow_walking_ahs_t2);
  learning_module_group->co_slow_walking_lp_max_torque.ConnectTo(this->co_slow_walking_lp_max_torque);
  learning_module_group->co_slow_walking_lp_t1.ConnectTo(this->co_slow_walking_lp_t1);
  learning_module_group->co_slow_walking_lp_t2.ConnectTo(this->co_slow_walking_lp_t2);
  learning_module_group->co_slow_walking_lh_par1.ConnectTo(this->co_slow_walking_lh_par1);
  learning_module_group->co_slow_walking_lh_par2.ConnectTo(this->co_slow_walking_lh_par2);
  learning_module_group->co_fast_walking_lh_par1.ConnectTo(this->co_fast_walking_lh_par1);
  learning_module_group->co_fast_walking_lh_par2.ConnectTo(this->co_fast_walking_lh_par2);
  learning_module_group->co_slow_walking_knee_angle.ConnectTo(this->co_slow_walking_knee_angle);
  learning_module_group->co_slow_walking_knee_angle_low.ConnectTo(this->co_slow_walking_knee_angle_low);
  learning_module_group->co_slow_walking_knee_angle_high.ConnectTo(this->co_slow_walking_knee_angle_high);
  learning_module_group->co_slow_walking_knee_velocity.ConnectTo(this->co_slow_walking_knee_velocity);
  learning_module_group->co_slow_walking_lk_hip_angle_low.ConnectTo(this->co_slow_walking_lk_hip_angle_low);
  learning_module_group->co_slow_walking_lk_hip_angle_high.ConnectTo(this->co_slow_walking_lk_hip_angle_high);
  learning_module_group->co_slow_walking_lk_increment_par1.ConnectTo(this->co_slow_walking_lk_increment_par1);
  learning_module_group->co_slow_walking_lk_increment_par2.ConnectTo(this->co_slow_walking_lk_increment_par2);
  learning_module_group->co_slow_walking_max_impulse.ConnectTo(this->co_slow_walking_max_impulse);
  learning_module_group->co_slow_walk_stimulation_knee_angle.ConnectTo(this->co_slow_walk_stimulation_knee_angle);
  learning_module_group->co_slow_walk_stimulation_knee_angle.ConnectTo(this->co_fast_walk_stimulation_knee_angle);
  learning_module_group->co_slow_walk_stimulation_knee_angle_activity.ConnectTo(this->co_slow_walk_stimulation_knee_angle_activity);

  //DEC double inverted pendulum
  this->ci_disturbance_estimation_activity.ConnectTo(estimation_hip_group->ci_disturbance_estimation_hip_activity);
  this->ci_disturbance_estimation_activity.ConnectTo(estimation_hip_group->ci_foot_space_tilt_activity);
  this->ci_disturbance_estimation_activity.ConnectTo(estimation_hip_group->ci_gravity_torque_leg_activity);
  this->ci_disturbance_estimation_activity.ConnectTo(estimation_hip_group->ci_gravity_torque_trunk_activity);
  this->ci_disturbance_estimation_activity.ConnectTo(estimation_hip_group->ci_inertial_torque_trunk_activity);
  this->ci_disturbance_estimation_activity.ConnectTo(estimation_hip_group->ci_leg_space_tilt_activity);
  this->ci_actual_pitch.ConnectTo(estimation_hip_group->ci_angle_trunk_space);
  this->ci_actual_roll.ConnectTo(estimation_hip_group->ci_angle_trunk_space_lateral);
  this->ci_actual_omega_y.ConnectTo(estimation_hip_group->ci_trunk_omega_y);
  this->ci_actual_accel_x.ConnectTo(estimation_hip_group->ci_body_accel_x);

  this->si_left_ankle_x_angle.ConnectTo(estimation_hip_group->si_angle_ankle_left_lateral);
  this->si_right_ankle_x_angle.ConnectTo(estimation_hip_group->si_angle_ankle_right_lateral);
  this->si_left_ankle_y_angle.ConnectTo(estimation_hip_group->si_angle_ankle_left);
  this->si_right_ankle_y_angle.ConnectTo(estimation_hip_group->si_angle_ankle_right);
  this->si_left_hip_x_angle.ConnectTo(estimation_hip_group->si_angle_hip_left_lateral);
  this->si_right_hip_x_angle.ConnectTo(estimation_hip_group->si_angle_hip_right_lateral);
  this->si_left_hip_y_angle.ConnectTo(estimation_hip_group->si_angle_hip_left);
  this->si_right_hip_y_angle.ConnectTo(estimation_hip_group->si_angle_hip_right);
  this->si_left_knee_angle.ConnectTo(estimation_hip_group->si_angle_knee_left);
  this->si_right_knee_angle.ConnectTo(estimation_hip_group->si_angle_knee_right);
  this->si_spine_x_angle.ConnectTo(estimation_hip_group->si_angle_spine_x);
  this->si_spine_y_angle.ConnectTo(estimation_hip_group->si_angle_spine_y);
  this->si_left_shoulder_x_angle.ConnectTo(estimation_hip_group->si_angle_shoulder_x_left);
  this->si_right_hip_x_angle.ConnectTo(estimation_hip_group->si_angle_shoulder_x_right);
  this->si_left_shoulder_y_angle.ConnectTo(estimation_hip_group->si_angle_shoulder_y_left);
  this->si_right_shoulder_y_angle.ConnectTo(estimation_hip_group->si_angle_shoulder_y_right);

  this->si_left_inner_toe_force.ConnectTo(estimation_hip_group->si_left_inner_toe_force);
  this->si_left_inner_heel_force.ConnectTo(estimation_hip_group->si_left_inner_heel_force);

  this->si_right_inner_toe_force.ConnectTo(estimation_hip_group->si_right_inner_toe_force);
  this->si_right_inner_heel_force.ConnectTo(estimation_hip_group->si_right_inner_heel_force);

  this->si_left_outer_toe_force.ConnectTo(estimation_hip_group->si_left_outer_toe_force);
  this->si_left_outer_heel_force.ConnectTo(estimation_hip_group->si_left_outer_heel_force);

  this->si_right_outer_toe_force.ConnectTo(estimation_hip_group->si_right_outer_toe_force);
  this->si_right_outer_heel_force.ConnectTo(estimation_hip_group->si_right_outer_heel_force);

  estimation_hip_group->co_angle_trunk_space_prop.ConnectTo(this->co_angle_trunk_space_prop);
  estimation_hip_group->co_angle_trunk_space_dist.ConnectTo(this->co_angle_trunk_space_dist);
  estimation_hip_group->co_angle_trunk_space_lateral_prop.ConnectTo(this->co_angle_trunk_space_lateral_prop);
  estimation_hip_group->co_angle_trunk_space_lateral_dist.ConnectTo(this->co_angle_trunk_space_lateral_dist);
  estimation_hip_group->co_angle_body_space_prop.ConnectTo(this->co_angle_body_space_prop);
  estimation_hip_group->co_angle_body_space_dist.ConnectTo(this->co_angle_body_space_dist);

  estimation_hip_group->co_angle_body_space_lateral_prop_left.ConnectTo(this->co_angle_body_space_lateral_prop_left);
  estimation_hip_group->co_angle_body_space_lateral_prop_right.ConnectTo(this->co_angle_body_space_lateral_prop_right);
  estimation_hip_group->co_angle_body_space_lateral_dist_left.ConnectTo(this->co_angle_body_space_lateral_dist_left);
  estimation_hip_group->co_angle_body_space_lateral_dist_right.ConnectTo(this->co_angle_body_space_lateral_dist_right);

  estimation_hip_group->co_estim_com_x.ConnectTo(this->co_angle_body_space);
  estimation_hip_group->co_target_angle_trunk_space.ConnectTo(this->co_target_angle_trunk_space);
  estimation_hip_group->co_target_angle_trunk_space_lateral.ConnectTo(this->co_target_angle_trunk_space_lateral);
  estimation_hip_group->co_estim_foot_space_angle.ConnectTo(this->co_estim_foot_space_angle_new);
  estimation_hip_group->co_angle_trunk_thigh_space_gravity.ConnectTo(this->co_angle_trunk_thigh_space_gravity);
  estimation_hip_group->co_target_angle_arm_trunk.ConnectTo(this->co_target_angle_arm_trunk);
  estimation_hip_group->co_estim_com_y.ConnectTo(this->co_angle_body_space_lateral);






  // begin com estimation and fusion
  this->si_left_hip_x_angle.ConnectTo(com_estim_y_left->si_hip_angle);
  this->si_spine_x_angle.ConnectTo(com_estim_y_left->si_spine_angle);

//  for (size_t i = 0; i < 3; i++)
//  {
//    this->ci_actual_accel.emplace_back("Actual Accel " + std::to_string(i), this);
//    this->ci_actual_rot.emplace_back("Actual Rot. " + std::to_string(i), this);
//    this->ci_actual_omega.emplace_back("Actual Omega " + std::to_string(i), this);
//    this->ci_adjusted_accel.emplace_back("Adjusted Accel " + std::to_string(i), this);
//    this->co_actual_accel.emplace_back("Out Actual Accel " + std::to_string(i), this);
//    this->co_actual_rot.emplace_back("Out Actual Rot. " + std::to_string(i), this);
//    this->co_actual_omega.emplace_back("Out Actual Omega " + std::to_string(i), this);
//    this->co_adjusted_accel.emplace_back("Out Adjusted Accel " + std::to_string(i), this);
//  }
  this->ci_actual_roll.ConnectTo(com_estim_y_left->ci_tilt_angle);

  this->si_right_hip_x_angle.ConnectTo(com_estim_y_right->si_hip_angle);
  this->si_spine_x_angle.ConnectTo(com_estim_y_right->si_spine_angle);

  this->ci_actual_roll.ConnectTo(com_estim_y_right->ci_tilt_angle);

  this->si_left_hip_y_angle.ConnectTo(com_estim_x_left->si_hip_angle);
  this->si_spine_y_angle.ConnectTo(com_estim_x_left->si_spine_angle);

  this->ci_actual_pitch.ConnectTo(com_estim_x_left->ci_tilt_angle);

  this->si_right_hip_y_angle.ConnectTo(com_estim_x_right->si_hip_angle);
  this->si_spine_y_angle.ConnectTo(com_estim_x_right->si_spine_angle);

  this->ci_actual_pitch.ConnectTo(com_estim_x_right->ci_tilt_angle);

  skill_walking_cpg->co_left_phase.ConnectTo(com_estim_fusion->ci_left_walking_phase);
  skill_walking_cpg->co_right_phase.ConnectTo(com_estim_fusion->ci_right_walking_phase);

  ground_contact->out_left_foot_load.ConnectTo(com_estim_fusion->ci_left_foot_load);
  ground_contact->out_right_foot_load.ConnectTo(com_estim_fusion->ci_right_foot_load);

  com_estim_x_left->co_com_position.ConnectTo(com_filter->ci_signal[0]);
  com_estim_x_left->co_xcom.ConnectTo(com_filter->ci_signal[1]);
  com_estim_x_right->co_com_position.ConnectTo(com_filter->ci_signal[2]);
  com_estim_x_right->co_xcom.ConnectTo(com_filter->ci_signal[3]);
  com_estim_y_left->co_com_position.ConnectTo(com_filter->ci_signal[4]);
  com_estim_y_left->co_xcom.ConnectTo(com_filter->ci_signal[5]);
  com_estim_y_right->co_com_position.ConnectTo(com_filter->ci_signal[6]);
  com_estim_y_right->co_xcom.ConnectTo(com_filter->ci_signal[7]);

  com_estim_x_left->so_com_position.ConnectTo(com_filter_sense->si_signal[0]);
  com_estim_x_left->so_xcom.ConnectTo(com_filter_sense->si_signal[1]);
  com_estim_x_right->so_com_position.ConnectTo(com_filter_sense->si_signal[2]);
  com_estim_x_right->so_xcom.ConnectTo(com_filter_sense->si_signal[3]);
  com_estim_y_left->so_com_position.ConnectTo(com_filter_sense->si_signal[4]);
  com_estim_y_left->so_xcom.ConnectTo(com_filter_sense->si_signal[5]);
  com_estim_y_right->so_com_position.ConnectTo(com_filter_sense->si_signal[6]);
  com_estim_y_right->so_xcom.ConnectTo(com_filter_sense->si_signal[7]);

  com_filter->co_signal[0].ConnectTo(com_estim_fusion->ci_left_com_x);
  com_filter->co_signal[1].ConnectTo(com_estim_fusion->ci_left_xcom_x);
  com_filter->co_signal[2].ConnectTo(com_estim_fusion->ci_right_com_x);
  com_filter->co_signal[3].ConnectTo(com_estim_fusion->ci_right_xcom_x);

  com_filter->co_signal[4].ConnectTo(com_estim_fusion->ci_left_com_y);
  com_filter->co_signal[5].ConnectTo(com_estim_fusion->ci_left_xcom_y);
  com_filter->co_signal[6].ConnectTo(com_estim_fusion->ci_right_com_y);
  com_filter->co_signal[7].ConnectTo(com_estim_fusion->ci_right_xcom_y);

  com_filter->co_signal[0].ConnectTo(skill_walking_cpg->ci_com_x_left);
  com_filter->co_signal[2].ConnectTo(skill_walking_cpg->ci_com_x_right);

  com_estim_fusion->co_com_x.ConnectTo(this->co_com_x);
  com_estim_fusion->co_com_y.ConnectTo(this->co_com_y);
  com_estim_fusion->co_xcom_x.ConnectTo(this->co_xcom_x);
  com_estim_fusion->co_xcom_y.ConnectTo(this->co_xcom_y);
  com_estim_fusion->co_com_error_y.ConnectTo(this->co_com_error_y);
  com_estim_fusion->co_inv_com_error_y.ConnectTo(this->co_inv_com_error_y);

  com_filter->co_signal[0].ConnectTo(this->co_left_com_x);
  com_filter->co_signal[1].ConnectTo(this->co_left_xcom_x);
  com_filter->co_signal[2].ConnectTo(this->co_right_com_x);
  com_filter->co_signal[3].ConnectTo(this->co_right_xcom_x);

  com_filter->co_signal[4].ConnectTo(this->co_left_com_y);
  com_filter->co_signal[5].ConnectTo(this->co_left_xcom_y);
  com_filter->co_signal[6].ConnectTo(this->co_right_com_y);
  com_filter->co_signal[7].ConnectTo(this->co_right_xcom_y);

  com_filter_sense->so_signal[0].ConnectTo(this->so_left_com_x);
  com_filter_sense->so_signal[1].ConnectTo(this->so_right_com_x);
  com_filter_sense->so_signal[2].ConnectTo(this->so_left_xcom_x);
  com_filter_sense->so_signal[3].ConnectTo(this->so_right_xcom_x);
  fly_wheel_motor_time->co_stimulate_motor_deceleration.ConnectTo(this->co_time_deceleration);
  fly_wheel_motor_time->co_time_stop.ConnectTo(this->co_time_stop);


  //BEGIN skills

  // stand
  skill_stand->activity.ConnectTo(this->co_stimulation_stand);
  this->ci_stimulation_stand.ConnectTo(skill_stand->stimulation);
  skill_stand->activity.ConnectTo(this->so_activity_stand);
  skill_stand->target_rating.ConnectTo(this->so_target_rating_stand);


  // dec
  this->ci_disturbance_estimation_activity.ConnectTo(this->co_disturbance_estimation_activity);

  this->si_right_ankle_y_angle.ConnectTo(skill_estimation_gravi_torque->si_right_ankle_angle_y);
  this->si_left_ankle_y_angle.ConnectTo(skill_estimation_gravi_torque->si_left_ankle_angle_y);
  this->si_right_knee_angle.ConnectTo(skill_estimation_gravi_torque->si_right_knee_angle_y);
  this->si_left_knee_angle.ConnectTo(skill_estimation_gravi_torque->si_left_knee_angle_y);
  this->si_right_hip_y_angle.ConnectTo(skill_estimation_gravi_torque->si_right_hip_angle_y);
  this->si_left_hip_y_angle.ConnectTo(skill_estimation_gravi_torque->si_left_hip_angle_y);
  this->si_spine_y_angle.ConnectTo(skill_estimation_gravi_torque->si_spine_angle_y);
  this->si_right_shoulder_y_angle.ConnectTo(skill_estimation_gravi_torque->si_right_shoulder_angle_y);
  this->si_left_shoulder_y_angle.ConnectTo(skill_estimation_gravi_torque->si_left_shoulder_angle_y);

  this->ci_gravity_torque_activity.ConnectTo(skill_estimation_gravi_torque->stimulation);
  this->ci_actual_pitch.ConnectTo(skill_estimation_gravi_torque->ci_angle_body_space);

  com_estim_x_left->co_com_angle.ConnectTo(skill_estimation_gravi_torque->ci_angle_com_left);
  com_estim_x_right->co_com_angle.ConnectTo(skill_estimation_gravi_torque->ci_angle_com_right);

  this->ci_space_rotation_activity.ConnectTo(skill_estimation_plate_rotation->stimulation);
  this->ci_actual_pitch.ConnectTo(skill_estimation_plate_rotation->ci_angle_body_space);

  this->si_right_ankle_y_angle.ConnectTo(skill_estimation_plate_rotation->si_right_ankle_angle_y);
  this->si_left_ankle_y_angle.ConnectTo(skill_estimation_plate_rotation->si_left_ankle_angle_y);
  this->si_right_knee_angle.ConnectTo(skill_estimation_plate_rotation->si_right_knee_angle_y);
  this->si_left_knee_angle.ConnectTo(skill_estimation_plate_rotation->si_left_knee_angle_y);
  this->si_right_hip_y_angle.ConnectTo(skill_estimation_plate_rotation->si_right_hip_angle_y);
  this->si_left_hip_y_angle.ConnectTo(skill_estimation_plate_rotation->si_left_hip_angle_y);
  this->si_spine_y_angle.ConnectTo(skill_estimation_plate_rotation->si_spine_angle_y);
  this->si_right_shoulder_y_angle.ConnectTo(skill_estimation_plate_rotation->si_right_shoulder_angle_y);
  this->si_left_shoulder_y_angle.ConnectTo(skill_estimation_plate_rotation->si_left_shoulder_angle_y);

  skill_estimation_plate_rotation->co_angle_foot_space.ConnectTo(this->co_estim_foot_space_angle);
  skill_estimation_plate_rotation->co_angle_body_foot.ConnectTo(this->co_angle_body_foot);

  this->ci_disturbance_estimation_activity.ConnectTo(skill_estimation_torque_to_angle->stimulation);
  skill_estimation_gravi_torque->co_gravity_torque.ConnectTo(skill_estimation_torque_to_angle->ci_torque_gravity);
  skill_estimation_external_torque->co_external_torque.ConnectTo(skill_estimation_torque_to_angle->ci_torque_external);

  skill_estimation_torque_to_angle->co_angle_deviation.ConnectTo(this->co_estim_angle_deviation);

  this->ci_external_torque_activity.ConnectTo(skill_estimation_external_torque->stimulation);
  this->ci_actual_omega_y.ConnectTo(skill_estimation_external_torque->ci_body_omega_y);

  com_estim_x_left->co_com_angle.ConnectTo(skill_estimation_external_torque->ci_com_angle_left);
  com_estim_x_right->co_com_angle.ConnectTo(skill_estimation_external_torque->ci_com_angle_right);
  this->si_right_ankle_y_torque.ConnectTo(skill_estimation_external_torque->si_right_angkle_y_torque);
  this->si_left_ankle_y_torque.ConnectTo(skill_estimation_external_torque->si_left_angkle_y_torque);

  this->ci_space_translational_acc_activity.ConnectTo(skill_estimation_translational_movement->stimulation);
  this->ci_actual_accel_x.ConnectTo(skill_estimation_translational_movement->ci_body_accel_x);
  com_estim_x_left->co_com_angle.ConnectTo(skill_estimation_translational_movement->ci_com_angle_left);
  com_estim_x_right->co_com_angle.ConnectTo(skill_estimation_translational_movement->ci_com_angle_right);

  skill_estimation_translational_movement->co_internal_torque.ConnectTo(skill_estimation_torque_to_angle->ci_torque_inertial);

  com_estim_y_left->co_com_position.ConnectTo(skill_estimation_gravi_torque_lateral->ci_com_left);
  com_estim_y_right->co_com_position.ConnectTo(skill_estimation_gravi_torque_lateral->ci_com_right);

  skill_estimation_com_translation_lateral->co_angle_body_space_gravity.ConnectTo(skill_estimation_gravi_torque_lateral->ci_angle_gravity_torque_lateral);

  skill_estimation_gravi_torque_lateral->co_gravity_torque_lateral.ConnectTo(skill_estimation_torque_to_angle_lateral->ci_torque_gravity_lateral);
  skill_estimation_torque_to_angle_lateral->co_angle_deviation_lateral_left.ConnectTo(this->co_estim_angle_deviation_lateral_left);
  skill_estimation_torque_to_angle_lateral->co_angle_deviation_lateral_right.ConnectTo(this->co_estim_angle_deviation_lateral_right);

  com_estim_y_left->co_com_position.ConnectTo(skill_estimation_plate_rotation_lateral->ci_com_y_left);
  com_estim_y_right->co_com_position.ConnectTo(skill_estimation_plate_rotation_lateral->ci_com_y_right);

  skill_estimation_plate_rotation_lateral->co_angle_foot_space_left_lateral.ConnectTo(this->co_estim_foot_space_angle_lateral_left);
  skill_estimation_plate_rotation_lateral->co_angle_foot_space_right_lateral.ConnectTo(this->co_estim_foot_space_angle_lateral_right);
  skill_estimation_plate_rotation_lateral->co_angle_body_foot_left_lateral.ConnectTo(this->co_angle_body_foot_left_lateral);
  skill_estimation_plate_rotation_lateral->co_angle_body_foot_right_lateral.ConnectTo(this->co_angle_body_foot_right_lateral);

  this->ci_actual_roll.ConnectTo(skill_estimation_plate_rotation_lateral->ci_body_roll);
  this->si_right_ankle_x_angle.ConnectTo(skill_estimation_plate_rotation_lateral->si_right_ankle_angle_x);
  this->si_left_ankle_x_angle.ConnectTo(skill_estimation_plate_rotation_lateral->si_left_ankle_angle_x);
  this->si_right_hip_x_angle.ConnectTo(skill_estimation_plate_rotation_lateral->si_hip_right_angle_x);
  this->si_left_hip_x_angle.ConnectTo(skill_estimation_plate_rotation_lateral->si_hip_left_angle_x);
  this->si_spine_x_angle.ConnectTo(skill_estimation_plate_rotation_lateral->si_spine_angle_x);

  this->ci_actual_omega_x.ConnectTo(skill_estimation_external_torque_lateral->ci_body_omega_x);
  skill_estimation_gravi_torque_lateral->co_gravity_torque_lateral.ConnectTo(skill_estimation_external_torque_lateral->ci_gravity_torque_lateral);
  this->si_right_ankle_x_torque.ConnectTo(skill_estimation_external_torque_lateral->si_right_ankle_x_torque);
  this->si_left_ankle_x_torque.ConnectTo(skill_estimation_external_torque_lateral->si_left_ankle_x_torque);

  this->ci_actual_roll.ConnectTo(skill_estimation_com_translation_lateral->ci_angle_trunk_space_lateral);

  this->si_right_ankle_x_angle.ConnectTo(skill_estimation_com_translation_lateral->si_angle_ankle_x_right);
  this->si_left_ankle_x_angle.ConnectTo(skill_estimation_com_translation_lateral->si_angle_ankle_x_left);
  this->si_left_shoulder_x_angle.ConnectTo(skill_estimation_com_translation_lateral->si_angle_shoulder_x_left);
  this->si_right_shoulder_x_angle.ConnectTo(skill_estimation_com_translation_lateral->si_angle_shoulder_x_right);

  skill_estimation_plate_rotation_lateral->co_angle_leg_space_left_lateral.ConnectTo(skill_estimation_com_translation_lateral->ci_angle_leg_space_left_lateral);
  skill_estimation_plate_rotation_lateral->co_angle_leg_space_right_lateral.ConnectTo(skill_estimation_com_translation_lateral->ci_angle_leg_space_right_lateral);
  skill_estimation_plate_rotation_lateral->co_angle_foot_space_left_lateral2.ConnectTo(skill_estimation_com_translation_lateral->ci_angle_foot_space_left_lateral);
  skill_estimation_plate_rotation_lateral->co_angle_foot_space_right_lateral2.ConnectTo(skill_estimation_com_translation_lateral->ci_angle_foot_space_right_lateral);

  // end of dec


  // postural reflex forward velocity
  // left
  this->ci_push_recovery_stand.ConnectTo(postural_reflex_forward_velocity_left->ci_push_recovery);

  skill_walking_cpg->co_walking_scale_factor.ConnectTo(postural_reflex_forward_velocity_left->ci_walking_velocity);
  skill_walking_cpg->co_last_step.ConnectTo(postural_reflex_forward_velocity_left->ci_last_step);
  com_filter->co_signal[0].ConnectTo(postural_reflex_forward_velocity_left->ci_com_x);
  com_filter->co_signal[1].ConnectTo(postural_reflex_forward_velocity_left->ci_xcom_x);

  ground_contact->out_left_ground_contact.ConnectTo(postural_reflex_forward_velocity_left->si_ground_contact);

  skill_walking_init_right->co_activity_walking_straight.ConnectTo(postural_reflex_forward_velocity_left->ci_initiation);

  postural_reflex_forward_velocity_left->co_correction.ConnectTo(this->co_left_forward_velocity_correction);

  postural_reflex_forward_velocity_left->so_correction.ConnectTo(this->so_xcom_x_left_correction);

  // right
  this->ci_push_recovery_stand.ConnectTo(postural_reflex_forward_velocity_right->ci_push_recovery);

  skill_walking_cpg->co_walking_scale_factor.ConnectTo(postural_reflex_forward_velocity_right->ci_walking_velocity);
  skill_walking_cpg->co_last_step.ConnectTo(postural_reflex_forward_velocity_right->ci_last_step);
  com_filter->co_signal[2].ConnectTo(postural_reflex_forward_velocity_right->ci_com_x);
  com_filter->co_signal[3].ConnectTo(postural_reflex_forward_velocity_right->ci_xcom_x);

  ground_contact->out_right_ground_contact.ConnectTo(postural_reflex_forward_velocity_right->si_ground_contact);

  skill_walking_init_left->co_activity_walking_straight.ConnectTo(postural_reflex_forward_velocity_right->ci_initiation);

  postural_reflex_forward_velocity_right->co_correction.ConnectTo(this->co_right_forward_velocity_correction);

  postural_reflex_forward_velocity_right->so_correction.ConnectTo(this->so_xcom_x_right_correction);

  // postural reflex lateral velocity
  // left
  this->ci_push_recovery_stand.ConnectTo(postural_reflex_lateral_velocity_left->ci_push_recovery);

  skill_walking_cpg->co_walking_scale_factor.ConnectTo(postural_reflex_lateral_velocity_left->ci_walking_velocity);
  skill_walking_cpg->co_termi_last_step.ConnectTo(postural_reflex_lateral_velocity_left->ci_termi_last_step);

  com_filter->co_signal[4].ConnectTo(postural_reflex_lateral_velocity_left->ci_com_y);
  com_filter->co_signal[5].ConnectTo(postural_reflex_lateral_velocity_left->ci_xcom_y);

  ground_contact->out_left_ground_contact.ConnectTo(postural_reflex_lateral_velocity_left->ci_ground_contact);

  skill_walking_init_right->co_activity_walking_straight.ConnectTo(postural_reflex_lateral_velocity_left->ci_initiation);

  postural_reflex_lateral_velocity_left->co_correction.ConnectTo(this->co_left_lateral_velocity_correction);

  postural_reflex_lateral_velocity_left->so_correction.ConnectTo(this->so_xcom_y_left_correction);

  // right
  this->ci_push_recovery_stand.ConnectTo(postural_reflex_lateral_velocity_right->ci_push_recovery);

  skill_walking_cpg->co_walking_scale_factor.ConnectTo(postural_reflex_lateral_velocity_right->ci_walking_velocity);
  skill_walking_cpg->co_termi_last_step.ConnectTo(postural_reflex_lateral_velocity_right->ci_termi_last_step);

  com_filter->co_signal[6].ConnectTo(postural_reflex_lateral_velocity_right->ci_com_y);
  com_filter->co_signal[7].ConnectTo(postural_reflex_lateral_velocity_right->ci_xcom_y);

  ground_contact->out_right_ground_contact.ConnectTo(postural_reflex_lateral_velocity_right->ci_ground_contact);

  skill_walking_init_left->co_activity_walking_straight.ConnectTo(postural_reflex_lateral_velocity_right->ci_initiation);

  postural_reflex_lateral_velocity_right->co_correction.ConnectTo(this->co_right_lateral_velocity_correction);

  postural_reflex_lateral_velocity_right->so_correction.ConnectTo(this->so_xcom_y_right_correction);


  // postural reflex frontal velocity
  // left
  skill_walking_cpg->co_lateral_scale_factor.ConnectTo(postural_reflex_frontal_velocity_left->ci_frontal_walking_velocity);
  com_filter->co_signal[4].ConnectTo(postural_reflex_frontal_velocity_left->ci_com_y);
  com_filter->co_signal[5].ConnectTo(postural_reflex_frontal_velocity_left->ci_xcom_y);

  ground_contact->out_left_ground_contact.ConnectTo(postural_reflex_frontal_velocity_left->si_ground_contact);

  postural_reflex_frontal_velocity_left->co_correction.ConnectTo(this->co_left_frontal_velocity_correction);

  // right
  skill_walking_cpg->co_lateral_scale_factor.ConnectTo(postural_reflex_frontal_velocity_right->ci_frontal_walking_velocity);
  com_filter->co_signal[6].ConnectTo(postural_reflex_frontal_velocity_right->ci_com_y);
  com_filter->co_signal[7].ConnectTo(postural_reflex_frontal_velocity_right->ci_xcom_y);

  ground_contact->out_right_ground_contact.ConnectTo(postural_reflex_frontal_velocity_right->si_ground_contact);

  postural_reflex_frontal_velocity_right->co_correction.ConnectTo(this->co_right_frontal_velocity_correction);
  // calculate the foot gesture
  //Left
  this->si_left_hip_y_angle.ConnectTo(skill_calc_foot_gesture_left->si_hip_y_angle);
  this->si_left_knee_angle.ConnectTo(skill_calc_foot_gesture_left->si_knee_angle);
  this->si_left_ankle_y_angle.ConnectTo(skill_calc_foot_gesture_left->si_ankle_y_angle);

  this->ci_actual_pitch.ConnectTo(skill_calc_foot_gesture_left->ci_actual_pitch);

  com_filter->co_signal[0].ConnectTo(skill_calc_foot_gesture_left->ci_current_com_x);
  com_filter->co_signal[2].ConnectTo(skill_calc_foot_gesture_left->ci_opposite_com_x);

  skill_calc_foot_gesture_left->co_leg_angle_offset.ConnectTo(this->co_left_leg_angle_offset);
  skill_calc_foot_gesture_left->co_com_x_error.ConnectTo(this->co_com_error_x_left);

  // right
  this->si_right_hip_y_angle.ConnectTo(skill_calc_foot_gesture_right->si_hip_y_angle);
  this->si_right_knee_angle.ConnectTo(skill_calc_foot_gesture_right->si_knee_angle);
  this->si_right_ankle_y_angle.ConnectTo(skill_calc_foot_gesture_right->si_ankle_y_angle);

  this->ci_actual_pitch.ConnectTo(skill_calc_foot_gesture_right->ci_actual_pitch);

  com_filter->co_signal[2].ConnectTo(skill_calc_foot_gesture_right->ci_current_com_x);
  com_filter->co_signal[0].ConnectTo(skill_calc_foot_gesture_right->ci_opposite_com_x);

  skill_calc_foot_gesture_right->co_leg_angle_offset.ConnectTo(this->co_right_leg_angle_offset);
  skill_calc_foot_gesture_right->co_com_x_error.ConnectTo(this->co_com_error_x_right);

  //BEGIN walking stuff
  // walking cpg
  this->ci_walking_scale_factor.ConnectTo(skill_walking_cpg->ci_walking_scale_factor);
  this->ci_turn_left_modulation_signal.ConnectTo(skill_walking_cpg->ci_turn_left_modulation_signal);
  this->ci_turn_right_modulation_signal.ConnectTo(skill_walking_cpg->ci_turn_right_modulation_signal);
  this->ci_push_recovery_stand.ConnectTo(skill_walking_cpg->ci_push_recovery_stand);
  this->ci_push_recovery_walking.ConnectTo(skill_walking_cpg->ci_push_recovery_walking);
  this->ci_push_detected_walking_left.ConnectTo(skill_walking_cpg->ci_push_detected_walking_left);
  this->ci_push_detected_walking_right.ConnectTo(skill_walking_cpg->ci_push_detected_walking_right);


  //  AddEdgeDown(skill_walking_cpg, skill_walking_termination, 1,
  //          mbbWalkingCPG::eCO_LAST_STEP, mbbSpinalCordWalkingTermination::eCI_LAST_STEP);

  skill_walking_termination->activity.ConnectTo(skill_walking_cpg->ci_activity_walking_termination);

  skill_walking_cpg->activity.ConnectTo(this->co_activity_walking);
  skill_walking_cpg->co_termination_state.ConnectTo(this->co_walking_termination);
  skill_walking_cpg->co_termi_last_step.ConnectTo(this->co_termi_last_step);
  skill_walking_cpg->co_last_step.ConnectTo(this->co_last_step);
  skill_walking_cpg->co_last_step_state.ConnectTo(this->co_last_step_state);
  skill_walking_cpg->co_step_left_leg_left.ConnectTo(this->co_step_left_leg_left);
  skill_walking_cpg->co_step_left_leg_right.ConnectTo(this->co_step_left_leg_right);
  skill_walking_cpg->co_step_right_leg_left.ConnectTo(this->co_step_right_leg_left);
  skill_walking_cpg->co_step_right_leg_right.ConnectTo(this->co_step_right_leg_right);
  skill_walking_cpg->co_termination_scale_factor.ConnectTo(this->co_termination_scale_factor);
  skill_walking_cpg->co_termi_lock_hip.ConnectTo(this->co_termi_lock_hip);
  skill_walking_cpg->co_stop_upright_trunk.ConnectTo(this->co_stop_upright_trunk);
  skill_walking_cpg->co_maintain_stable.ConnectTo(this->co_maintain_stable);
  skill_walking_cpg->co_s_leg_left.ConnectTo(this->co_s_leg_left);
  skill_walking_cpg->co_s_leg_right.ConnectTo(this->co_s_leg_right);
  skill_walking_cpg->co_turn_left_signal_to_left.ConnectTo(this->co_turn_left_signal_to_left);
  skill_walking_cpg->co_turn_left_signal_to_right.ConnectTo(this->co_turn_left_signal_to_right);
  skill_walking_cpg->co_turn_right_signal_to_left.ConnectTo(this->co_turn_right_signal_to_left);
  skill_walking_cpg->co_turn_right_signal_to_right.ConnectTo(this->co_turn_right_signal_to_right);
  skill_walking_cpg->co_hip_motor_factor_push_recovery.ConnectTo(this->co_hip_motor_factor_push_recovery);
  skill_walking_cpg->co_push_recovery_walking.ConnectTo(this->co_push_recovery_walking);
  skill_walking_cpg->co_bent_knee.ConnectTo(this->co_bent_knee);
  skill_walking_cpg->co_push_detected_walking_left.ConnectTo(this->co_push_detected_walking_left);
  skill_walking_cpg->co_push_detected_walking_right.ConnectTo(this->co_push_detected_walking_right);
  skill_walking_cpg->co_termination_hold_position.ConnectTo(this->co_termination_hold_position);
  skill_walking_cpg->co_termination_turn_off_upright.ConnectTo(this->co_termination_turn_off_upright);

  skill_walking_cpg->activity.ConnectTo(this->so_activity_walking);
  skill_walking_cpg->target_rating.ConnectTo(this->so_target_rating_walking);
  skill_walking_cpg->so_termination_finished.ConnectTo(this->so_termination_finished);
  skill_walking_cpg->so_target_rating_termination.ConnectTo(this->so_target_rating_walking_termination);
  skill_walking_cpg->so_falling_down.ConnectTo(this->so_falling_down);

  skill_walking_cpg->activity.ConnectTo(fusion_stand_inhibition->InputActivity(0));
  skill_walking_cpg->activity.ConnectTo(fusion_stand_inhibition->InputTargetRating(0));
  skill_walking_cpg->activity.ConnectTo(fusion_stand_inhibition->InputPort(0, 0));



  fusion_stand_inhibition->activity.ConnectTo(skill_stand->inhibition[0]);


  ground_contact->out_ground_contact.ConnectTo(skill_walking_cpg->si_ground_contact);

  this->si_left_leg_foot_load.ConnectTo(skill_walking_cpg->si_left_foot_load);
  this->si_right_leg_foot_load.ConnectTo(skill_walking_cpg->si_right_foot_load);

  this->si_left_inner_heel_force.ConnectTo(skill_walking_cpg->si_left_inner_heel_force);
  this->si_left_inner_toe_force.ConnectTo(skill_walking_cpg->si_left_inner_toe_force);
  this->si_right_inner_heel_force.ConnectTo(skill_walking_cpg->si_right_inner_heel_force);
  this->si_right_inner_toe_force.ConnectTo(skill_walking_cpg->si_right_inner_toe_force);
  this->si_left_ankle_y_angle.ConnectTo(skill_walking_cpg->si_left_ankle_y_angle);
  this->si_right_ankle_y_angle.ConnectTo(skill_walking_cpg->si_right_ankle_y_angle);
  this->si_left_hip_y_angle.ConnectTo(skill_walking_cpg->si_left_hip_y_angle);
  this->si_right_hip_y_angle.ConnectTo(skill_walking_cpg->si_right_hip_y_angle);
  this->si_left_knee_angle.ConnectTo(skill_walking_cpg->si_left_knee_angle);
  this->si_right_knee_angle.ConnectTo(skill_walking_cpg->si_right_knee_angle);
  this->si_left_leg_activity_walking_phase_4.ConnectTo(skill_walking_cpg->si_left_leg_activity_walking_phase_4);
  this->si_left_leg_target_rating_walking_phase_4.ConnectTo(skill_walking_cpg->si_left_leg_target_rating_walking_phase_4);
  this->si_right_leg_activity_walking_phase_4.ConnectTo(skill_walking_cpg->si_right_leg_activity_walking_phase_4);
  this->si_right_leg_target_rating_walking_phase_4.ConnectTo(skill_walking_cpg->si_right_leg_target_rating_walking_phase_4);
  this->si_left_leg_activity_walking_phase_5.ConnectTo(skill_walking_cpg->si_left_leg_activity_walking_phase_5);
  this->si_left_leg_target_rating_walking_phase_5.ConnectTo(skill_walking_cpg->si_left_leg_target_rating_walking_phase_5);
  this->si_right_leg_activity_walking_phase_5.ConnectTo(skill_walking_cpg->si_right_leg_activity_walking_phase_5);
  this->si_right_leg_target_rating_walking_phase_5.ConnectTo(skill_walking_cpg->si_right_leg_target_rating_walking_phase_5);

  skill_walking_cpg->co_left_stimulation_phase_1.ConnectTo(skill_walking_phase1_left->stimulation);
  skill_walking_cpg->co_left_stimulation_phase_2.ConnectTo(skill_walking_phase2_left->stimulation);
  skill_walking_cpg->co_left_stimulation_phase_3.ConnectTo(skill_walking_phase3_left->stimulation);
  skill_walking_cpg->co_left_stimulation_phase_4.ConnectTo(skill_walking_phase4_left->stimulation);
  skill_walking_cpg->co_left_stimulation_phase_5.ConnectTo(skill_walking_phase5_left->stimulation);

  skill_walking_cpg->co_right_stimulation_phase_1.ConnectTo(skill_walking_phase1_right->stimulation);
  skill_walking_cpg->co_right_stimulation_phase_2.ConnectTo(skill_walking_phase2_right->stimulation);
  skill_walking_cpg->co_right_stimulation_phase_3.ConnectTo(skill_walking_phase3_right->stimulation);
  skill_walking_cpg->co_right_stimulation_phase_4.ConnectTo(skill_walking_phase4_right->stimulation);
  skill_walking_cpg->co_right_stimulation_phase_5.ConnectTo(skill_walking_phase5_right->stimulation);

  // walking lateral cpg
  /*
    AddEdgeDown(this, skill_walking_lateral_cpg, 2,
                eCI_STIMULATION_WALKING_LATERAL, mbbWalkingLatCPG::eCI_STIMULATION,
                eCI_WALKING_LATERALLY, mbbWalkingLatCPG::eCI_WALKING_SCALE_LATERAL);
    AddEdgeDown(ground_contact, skill_walking_lateral_cpg, 1,
                mGroundContact::eCO_GROUND_CONTACT, mbbWalkingLatCPG::eCI_GROUND_CONTACT);
    index = fusion_stand_inhibition->RegisterSensorInput("walking lateral cpg");
    AddEdgeUp(skill_walking_lateral_cpg, fusion_stand_inhibition, 1,
              mbbWalkingLatCPG::eSO_ACTIVITY, index);
    AddEdgeUp(fusion_stand_inhibition, skill_stand, 1,
              mbbFusionBehaviour::eSO_ACTIVITY, mbbUberSkill::eSI_INHIBITION);
    AddEdgeUp(this, skill_walking_lateral_cpg, 20,
              eSI_LEFT_LEG_FOOT_LOAD, mbbWalkingLatCPG::eSI_LEFT_FOOT_LOAD,
              eSI_RIGHT_LEG_FOOT_LOAD, mbbWalkingLatCPG::eSI_RIGHT_FOOT_LOAD,
              eSI_LEFT_INNER_HEEL_FORCE, mbbWalkingLatCPG::eSI_LEFT_INNER_HEEL_FORCE,
              eSI_LEFT_OUTER_HEEL_FORCE, mbbWalkingLatCPG::eSI_LEFT_OUTER_HEEL_FORCE,
              eSI_RIGHT_INNER_HEEL_FORCE, mbbWalkingLatCPG::eSI_RIGHT_INNER_HEEL_FORCE,
              eSI_RIGHT_OUTER_HEEL_FORCE, mbbWalkingLatCPG::eSI_RIGHT_OUTER_HEEL_FORCE,
              eSI_LEFT_INNER_TOE_FORCE, mbbWalkingLatCPG::eSI_LEFT_INNER_TOE_FORCE,
              eSI_LEFT_OUTER_TOE_FORCE, mbbWalkingLatCPG::eSI_LEFT_OUTER_TOE_FORCE,
              eSI_RIGHT_INNER_TOE_FORCE, mbbWalkingLatCPG::eSI_RIGHT_INNER_TOE_FORCE,
              eSI_RIGHT_OUTER_TOE_FORCE, mbbWalkingLatCPG::eSI_RIGHT_OUTER_TOE_FORCE,
              eSI_LEFT_ANKLE_Y_ANGLE, mbbWalkingLatCPG::eSI_LEFT_ANKLE_Y_ANGLE,
              eSI_RIGHT_ANKLE_Y_ANGLE, mbbWalkingLatCPG::eSI_RIGHT_ANKLE_Y_ANGLE,
              eSI_LEFT_ANKLE_X_ANGLE, mbbWalkingLatCPG::eSI_LEFT_ANKLE_X_ANGLE,
              eSI_RIGHT_ANKLE_X_ANGLE, mbbWalkingLatCPG::eSI_RIGHT_ANKLE_X_ANGLE,
              eSI_LEFT_HIP_ANGLE_Y, mbbWalkingLatCPG::eSI_LEFT_HIP_Y_ANGLE,
              eSI_RIGHT_HIP_ANGLE_Y, mbbWalkingLatCPG::eSI_RIGHT_HIP_Y_ANGLE,
              eSI_LEFT_HIP_ANGLE_X, mbbWalkingLatCPG::eSI_LEFT_HIP_X_ANGLE,
              eSI_RIGHT_HIP_ANGLE_X, mbbWalkingLatCPG::eSI_RIGHT_HIP_X_ANGLE,
              eSI_LEFT_KNEE_ANGLE, mbbWalkingLatCPG::eSI_LEFT_KNEE_ANGLE,
              eSI_RIGHT_KNEE_ANGLE, mbbWalkingLatCPG::eSI_RIGHT_KNEE_ANGLE);

    AddEdgeDown(skill_walking_lateral_cpg, skill_walking_lateral_phase1_left, 1,
                mbbWalkingLatCPG::eCO_LEFT_STIMULATION_PHASE_1, mbbUberSkill::eCI_STIMULATION);
    AddEdgeDown(skill_walking_lateral_cpg, skill_walking_lateral_phase2_left, 1,
                mbbWalkingLatCPG::eCO_LEFT_STIMULATION_PHASE_2, mbbUberSkill::eCI_STIMULATION);
    AddEdgeDown(skill_walking_lateral_cpg, skill_walking_lateral_phase3_left, 1,
                mbbWalkingLatCPG::eCO_LEFT_STIMULATION_PHASE_3, mbbUberSkill::eCI_STIMULATION);
    AddEdgeDown(skill_walking_lateral_cpg, skill_walking_lateral_phase4_left, 1,
                mbbWalkingLatCPG::eCO_LEFT_STIMULATION_PHASE_4, mbbUberSkill::eCI_STIMULATION);

    AddEdgeDown(skill_walking_lateral_cpg, skill_walking_lateral_phase1_right, 1,
                mbbWalkingLatCPG::eCO_RIGHT_STIMULATION_PHASE_1, mbbUberSkill::eCI_STIMULATION);
    AddEdgeDown(skill_walking_lateral_cpg, skill_walking_lateral_phase2_right, 1,
                mbbWalkingLatCPG::eCO_RIGHT_STIMULATION_PHASE_2, mbbUberSkill::eCI_STIMULATION);
    AddEdgeDown(skill_walking_lateral_cpg, skill_walking_lateral_phase3_right, 1,
                mbbWalkingLatCPG::eCO_RIGHT_STIMULATION_PHASE_3, mbbUberSkill::eCI_STIMULATION);
    AddEdgeDown(skill_walking_lateral_cpg, skill_walking_lateral_phase4_right, 1,
                mbbWalkingLatCPG::eCO_RIGHT_STIMULATION_PHASE_4, mbbUberSkill::eCI_STIMULATION);

    AddEdgeDown(skill_walking_lateral_cpg, this, 1,
                mbbWalkingLatCPG::eCO_WALKING_LATERAL_ACTIVITY, eCO_WALKING_LATERAL_ACTIVITY);
    //            mbbWalkingLatCPG::eCO_COM_Y_RATIO, eCO_COM_Y_RATIO);
     * */

  // walking initiation
  // left
  this->ci_left_stimulation_walking_initiation.ConnectTo(skill_walking_init_left->stimulation);
  this->ci_push_recovery_stand.ConnectTo(skill_walking_init_left->ci_push_recovery_stand);
  this->ci_walking_scale_factor.ConnectTo(skill_walking_init_left->ci_walking_scale);
  this->ci_walking_scale_laterally.ConnectTo(skill_walking_init_left->ci_lateral_scale);

  skill_walking_init_left->co_activity_walking_straight.ConnectTo(this->co_left_stimulation_walking_initiation);
  skill_walking_init_left->co_activity_walking_lateral.ConnectTo(this->co_left_stimulation_walking_lateral_initiation);
  skill_walking_init_left->co_activity_walking_straight.ConnectTo(skill_walking_cpg->ci_left_activity_walking_initiation);

  skill_walking_init_left->activity.ConnectTo(this->so_left_activity_walking_initiation);
  skill_walking_init_left->target_rating.ConnectTo(this->so_left_target_rating_walking_initiation);

  this->si_left_force_z.ConnectTo(skill_walking_init_left->si_force_z_swing_leg);
  this->si_right_force_z.ConnectTo(skill_walking_init_left->si_force_z_stance_leg);
  this->si_left_knee_angle.ConnectTo(skill_walking_init_left->si_knee_angle_swing_leg);
  this->si_right_knee_angle.ConnectTo(skill_walking_init_left->si_knee_angle_stance_leg);
  this->si_left_ankle_y_angle.ConnectTo(skill_walking_init_left->si_ankle_y_angle_left);
  this->si_right_ankle_y_angle.ConnectTo(skill_walking_init_left->si_ankle_y_angle_right);

  com_filter_sense->so_signal[4].ConnectTo(skill_walking_init_left->si_com_y_left);
  com_filter_sense->so_signal[6].ConnectTo(skill_walking_init_left->si_com_y_right);
  // right
  this->ci_right_stimulation_walking_initiation.ConnectTo(skill_walking_init_right->stimulation);
  //this->ci_push_recovery_stand.ConnectTo(skill_walking_init_right->ci_push_recovery_stand);
  this->ci_walking_scale_factor.ConnectTo(skill_walking_init_right->ci_walking_scale);
  this->ci_walking_scale_laterally.ConnectTo(skill_walking_init_right->ci_lateral_scale);

  skill_walking_init_right->co_activity_walking_straight.ConnectTo(this->co_right_stimulation_walking_initiation);
  skill_walking_init_right->co_activity_walking_lateral.ConnectTo(this->co_right_stimulation_walking_lateral_initiation);
  skill_walking_init_right->co_activity_walking_straight.ConnectTo(skill_walking_cpg->ci_right_activity_walking_initiation);

  skill_walking_init_right->activity.ConnectTo(this->so_right_activity_walking_initiation);
  skill_walking_init_right->target_rating.ConnectTo(this->so_right_target_rating_walking_initiation);

  this->si_right_force_z.ConnectTo(skill_walking_init_right->si_force_z_swing_leg);
  this->si_left_force_z.ConnectTo(skill_walking_init_right->si_force_z_stance_leg);
  this->si_right_knee_angle.ConnectTo(skill_walking_init_right->si_knee_angle_swing_leg);
  this->si_left_knee_angle.ConnectTo(skill_walking_init_right->si_knee_angle_stance_leg);
//      this->si_left_ankle_y_angle.ConnectTo(skill_walking_init_right->si_ankle_y_angle_left);
//      this->si_right_ankle_y_angle.ConnectTo(skill_walking_init_right->si_ankle_y_angle_right);

//      com_filter_sense->so_signal[4].ConnectTo(skill_walking_init_right->si_com_y_left);
//      com_filter_sense->so_signal[6].ConnectTo(skill_walking_init_right->si_com_y_right);
  // walking phases
  // left
  // phase 1
  skill_walking_phase1_left->activity.ConnectTo(this->co_left_stimulation_walking_phase_1);
  skill_walking_phase1_left->activity.ConnectTo(fusion_forward_velocity_left->InputActivity(0));
  skill_walking_phase1_left->activity.ConnectTo(fusion_forward_velocity_left->InputTargetRating(0));//fixme
  skill_walking_phase1_left->activity.ConnectTo(fusion_forward_velocity_left->InputPort(0, 0));


  skill_walking_phase1_left->activity.ConnectTo(fusion_lateral_velocity_left->InputActivity(0));
  skill_walking_phase1_left->activity.ConnectTo(fusion_lateral_velocity_left->InputTargetRating(0));//fixme
  skill_walking_phase1_left->activity.ConnectTo(fusion_lateral_velocity_left->InputPort(0, 0));


  skill_walking_phase1_left->activity.ConnectTo(fusion_frontal_velocity_left->InputActivity(0));
  skill_walking_phase1_left->activity.ConnectTo(fusion_frontal_velocity_left->InputTargetRating(0));//fixme
  skill_walking_phase1_left->activity.ConnectTo(fusion_frontal_velocity_left->InputPort(0, 0));


  // phase 2
  skill_walking_phase2_left->activity.ConnectTo(this->co_left_stimulation_walking_phase_2);

  skill_walking_phase2_left->activity.ConnectTo(fusion_forward_velocity_left->InputActivity(1));
  skill_walking_phase2_left->activity.ConnectTo(fusion_forward_velocity_left->InputTargetRating(1));//fixme
  skill_walking_phase2_left->activity.ConnectTo(fusion_forward_velocity_left->InputPort(1, 0));


  skill_walking_phase2_left->activity.ConnectTo(fusion_lateral_velocity_left->InputActivity(1));
  skill_walking_phase2_left->activity.ConnectTo(fusion_lateral_velocity_left->InputTargetRating(1));//fixme
  skill_walking_phase2_left->activity.ConnectTo(fusion_lateral_velocity_left->InputPort(1, 0));


  skill_walking_phase2_left->activity.ConnectTo(fusion_frontal_velocity_left->InputActivity(1));
  skill_walking_phase2_left->activity.ConnectTo(fusion_frontal_velocity_left->InputTargetRating(1));//fixme
  skill_walking_phase2_left->activity.ConnectTo(fusion_frontal_velocity_left->InputPort(1, 0));


  // phase 3
  skill_walking_phase3_left->activity.ConnectTo(this->co_left_stimulation_walking_phase_3);

  // phase 4
  skill_walking_phase4_left->activity.ConnectTo(this->co_left_stimulation_walking_phase_4);
  skill_walking_phase4_left->activity.ConnectTo(skill_calc_foot_gesture_left->stimulation);

  // phase 5
  skill_walking_phase5_left->activity.ConnectTo(this->co_left_stimulation_walking_phase_5);

  // right
  // phase 1
  skill_walking_phase1_right->activity.ConnectTo(this->co_right_stimulation_walking_phase_1);
  skill_walking_phase1_right->activity.ConnectTo(fusion_forward_velocity_right->InputActivity(0));
  skill_walking_phase1_right->activity.ConnectTo(fusion_forward_velocity_right->InputTargetRating(0));
  skill_walking_phase1_right->activity.ConnectTo(fusion_forward_velocity_right->InputPort(0, 0));


  skill_walking_phase1_right->activity.ConnectTo(fusion_lateral_velocity_right->InputActivity(0));
  skill_walking_phase1_right->activity.ConnectTo(fusion_lateral_velocity_right->InputTargetRating(0));
  skill_walking_phase1_right->activity.ConnectTo(fusion_lateral_velocity_right->InputPort(0, 0));

  skill_walking_phase1_right->activity.ConnectTo(fusion_frontal_velocity_right->InputActivity(0));
  skill_walking_phase1_right->activity.ConnectTo(fusion_frontal_velocity_right->InputTargetRating(0));
  skill_walking_phase1_right->activity.ConnectTo(fusion_frontal_velocity_right->InputPort(0, 0));

  // phase 2
  skill_walking_phase2_right->activity.ConnectTo(this->co_right_stimulation_walking_phase_2);

  skill_walking_phase2_right->activity.ConnectTo(fusion_forward_velocity_right->InputActivity(1));
  skill_walking_phase2_right->activity.ConnectTo(fusion_forward_velocity_right->InputTargetRating(1));
  skill_walking_phase2_right->activity.ConnectTo(fusion_forward_velocity_right->InputPort(1, 0));

  skill_walking_phase2_right->activity.ConnectTo(fusion_lateral_velocity_right->InputActivity(1));
  skill_walking_phase2_right->activity.ConnectTo(fusion_lateral_velocity_right->InputTargetRating(1));
  skill_walking_phase2_right->activity.ConnectTo(fusion_lateral_velocity_right->InputPort(1, 0));

  skill_walking_phase2_right->activity.ConnectTo(fusion_frontal_velocity_right->InputActivity(1));
  skill_walking_phase2_right->activity.ConnectTo(fusion_frontal_velocity_right->InputTargetRating(1));
  skill_walking_phase2_right->activity.ConnectTo(fusion_frontal_velocity_right->InputPort(1, 0));

  // phase 3
  skill_walking_phase3_right->activity.ConnectTo(this->co_right_stimulation_walking_phase_3);

  // phase 4
  skill_walking_phase4_right->activity.ConnectTo(this->co_right_stimulation_walking_phase_4);
  //skill_walking_phase4_right->activity.ConnectTo(skill_calc_foot_gesture_right->stimulation);

  // phase 5
  skill_walking_phase5_right->activity.ConnectTo(this->co_right_stimulation_walking_phase_5);

  // lift leg
  // left
//  fusion_lift_leg_left->activity.ConnectTo(skill_lift_leg_left->stimulation);
//  skill_lift_leg_left->activity.ConnectTo(this->co_left_stimulation_lift_leg);
  // right
//  fusion_lift_leg_right->activity.ConnectTo(skill_lift_leg_right->stimulation);
//  skill_lift_leg_right->activity.ConnectTo(this->co_right_stimulation_lift_leg);

  // Walking Termination
  this->si_left_force_z.ConnectTo(skill_walking_termination->si_force_z_left);
  this->si_right_force_z.ConnectTo(skill_walking_termination->si_force_z_right);

  this->si_left_ground_contact.ConnectTo(skill_walking_termination->si_left_ground_contact);
  this->si_right_ground_contact.ConnectTo(skill_walking_termination->si_right_ground_contact);

  this->ci_stimulation_walking_termination.ConnectTo(skill_walking_termination->stimulation);
  this->ci_stimulation_walking_termination.ConnectTo(this->so_activity_walking_termination);

  skill_walking_termination->activity.ConnectTo(fusion_walking_termination->ci_signal[0]);
// fixme for walking termination!
  this->ci_stimulation_walking.ConnectTo(fusion_walking_termination->ci_signal[1]);


  fusion_walking_termination->co_signal[0].ConnectTo(skill_walking_cpg->stimulation);
  //this->ci_stimulation_walking.ConnectTo(skill_walking_cpg->stimulation);

  // walking lateral phases
  // left
  skill_walking_lateral_phase1_left->activity.ConnectTo(this->co_left_stimulation_walking_lateral_phase_1);
  skill_walking_lateral_phase2_left->activity.ConnectTo(this->co_left_stimulation_walking_lateral_phase_2);
  skill_walking_lateral_phase3_left->activity.ConnectTo(this->co_left_stimulation_walking_lateral_phase_3);
  skill_walking_lateral_phase4_left->activity.ConnectTo(this->co_left_stimulation_walking_lateral_phase_4);
  // right
  skill_walking_lateral_phase1_right->activity.ConnectTo(this->co_right_stimulation_walking_lateral_phase_1);
  skill_walking_lateral_phase2_right->activity.ConnectTo(this->co_right_stimulation_walking_lateral_phase_2);
  skill_walking_lateral_phase3_right->activity.ConnectTo(this->co_right_stimulation_walking_lateral_phase_3);
  skill_walking_lateral_phase4_right->activity.ConnectTo(this->co_right_stimulation_walking_lateral_phase_4);

  //END walking stuff

  // balance ankle pitch
  this->ci_quiet_stand.ConnectTo(skill_balance_ankle_pitch->ci_quiet_stand);

  skill_stand->activity.ConnectTo(skill_balance_ankle_pitch->stimulation);

//  skill_balance_ankle_pitch->activity.ConnectTo(this->co_stimulation_balance_ankle_pitch);
//  skill_balance_ankle_pitch->co_left_angle_adjustment.ConnectTo(this->co_left_balance_ankle_pitch_angle_adjustment);
//  skill_balance_ankle_pitch->co_right_angle_adjustment.ConnectTo(this->co_right_balance_ankle_pitch_angle_adjustment);
  this->ci_reset_simulation_experiments.ConnectTo(skill_balance_ankle_pitch->ci_reset_simulation_experiments);
  this->si_left_ankle_y_angle.ConnectTo(skill_balance_ankle_pitch->si_angle_left_ankle);
  this->si_right_ankle_y_angle.ConnectTo(skill_balance_ankle_pitch->si_angle_right_ankle);
  this->si_left_force_deviation.ConnectTo(skill_balance_ankle_pitch->si_force_deviation_left);
  this->si_right_force_deviation.ConnectTo(skill_balance_ankle_pitch->si_force_deviation_right);
  ground_contact->out_ground_contact.ConnectTo(skill_balance_ankle_pitch->si_ground_contact);

  // relax knee
  this->ci_quiet_stand.ConnectTo(skill_relax_knee->ci_quiet_stand);
  this->ci_reset_simulation_experiments.ConnectTo(skill_relax_knee->ci_reset_simulation_experiments);
  skill_stand->activity.ConnectTo(skill_relax_knee->stimulation);

  skill_relax_knee->activity.ConnectTo(this->co_stimulation_relax_knee);
  skill_relax_knee->co_left_angle_adjustment.ConnectTo(this->co_left_relax_knee_angle_adjustment);
  skill_relax_knee->co_right_angle_adjustment.ConnectTo(this->co_right_relax_knee_angle_adjustment);
  this->si_left_ankle_y_angle.ConnectTo(skill_relax_knee->si_left_knee_angle);
  this->si_right_ankle_y_angle.ConnectTo(skill_relax_knee->si_right_knee_angle);
  this->si_left_ankle_y_torque.ConnectTo(skill_relax_knee->si_left_knee_torque);
  this->si_right_ankle_y_torque.ConnectTo(skill_relax_knee->si_right_knee_torque);
  ground_contact->out_ground_contact.ConnectTo(skill_relax_knee->si_ground_contact);

  // relax hip y
  this->ci_quiet_stand.ConnectTo(skill_relax_hip_y->ci_quiet_stand);
  this->ci_reset_simulation_experiments.ConnectTo(skill_relax_hip_y->ci_reset_simulation_experiments);

  skill_stand->activity.ConnectTo(skill_relax_hip_y->stimulation);

  skill_relax_hip_y->activity.ConnectTo(this->co_stimulation_relax_hip_y);
  skill_relax_hip_y->co_left_angle_adjustment.ConnectTo(this->co_left_relax_hip_y_angle_adjustment);
  skill_relax_hip_y->co_right_angle_adjustment.ConnectTo(this->co_right_relax_hip_y_angle_adjustment);
  this->si_left_ankle_y_angle.ConnectTo(skill_relax_hip_y->si_left_hip_angle);
  this->si_right_ankle_y_angle.ConnectTo(skill_relax_hip_y->si_right_hip_angle);
  this->si_left_ankle_y_torque.ConnectTo(skill_relax_hip_y->si_left_hip_torque);
  this->si_right_ankle_y_torque.ConnectTo(skill_relax_hip_y->si_right_hip_torque);
  ground_contact->out_ground_contact.ConnectTo(skill_relax_hip_y->si_ground_contact);
  // relax spine
  // x

  this->ci_quiet_stand.ConnectTo(skill_relax_spine_x->ci_quiet_stand);
  this->ci_reset_simulation_experiments.ConnectTo(skill_relax_spine_x->ci_reset_simulation_experiments);

  skill_stand->activity.ConnectTo(skill_relax_spine_x->stimulation);

  skill_relax_spine_x->activity.ConnectTo(this->co_stimulation_relax_spine_x);
  skill_relax_spine_x->co_angle_adjustment.ConnectTo(this->co_relax_spine_x_angle_adjustment);
  this->si_spine_x_angle.ConnectTo(skill_relax_spine_x->si_spine_angle);
  this->si_spine_x_torque.ConnectTo(skill_relax_spine_x->si_spine_torque);
  ground_contact->out_ground_contact.ConnectTo(skill_relax_spine_x->si_ground_contact);

  // y
  this->ci_quiet_stand.ConnectTo(skill_relax_spine_y->ci_quiet_stand);
  this->ci_reset_simulation_experiments.ConnectTo(skill_relax_spine_y->ci_reset_simulation_experiments);

  skill_stand->activity.ConnectTo(skill_relax_spine_y->stimulation);

  skill_relax_spine_y->activity.ConnectTo(this->co_stimulation_relax_spine_x);
  skill_relax_spine_y->co_angle_adjustment.ConnectTo(this->co_relax_spine_x_angle_adjustment);
  this->si_spine_x_angle.ConnectTo(skill_relax_spine_y->si_spine_angle);
  this->si_spine_x_torque.ConnectTo(skill_relax_spine_y->si_spine_torque);
  ground_contact->out_ground_contact.ConnectTo(skill_relax_spine_y->si_ground_contact);

  // skill walking velocity
  skill_walking_velocity->co_walking_velocity.ConnectTo(this->co_walking_velocity);
  skill_walking_velocity->co_step_length.ConnectTo(this->co_step_length);
  skill_walking_velocity->co_walking_distance.ConnectTo(this->co_walking_distance);
  this->ci_reset_simulation_experiments.ConnectTo(skill_walking_velocity->ci_simulation_reset);
  learning_module_group->so_loop_times.ConnectTo(skill_walking_velocity->ci_loop_times);
  learning_module_group->si_pelvis_position_x.ConnectTo(skill_walking_velocity->si_pelvis_position_x);
  learning_module_group->si_pelvis_position_y.ConnectTo(skill_walking_velocity->si_pelvis_position_y);

  // reduce tension
  // ankle y
  this->ci_quiet_stand.ConnectTo(skill_reduce_tension_ankle_y->ci_quiet_stand);
  this->ci_reset_simulation_experiments.ConnectTo(skill_reduce_tension_ankle_y->ci_reset_simulation_experiments);

  skill_stand->activity.ConnectTo(skill_reduce_tension_ankle_y->stimulation);

  skill_reduce_tension_ankle_y->activity.ConnectTo(this->co_stimulation_reduce_tension_ankle_y);
  skill_reduce_tension_ankle_y->co_left_angle_adjustment.ConnectTo(this->co_left_reduce_tension_ankle_y_angle_adjustment);
  skill_reduce_tension_ankle_y->co_right_angle_adjustment.ConnectTo(this->co_right_reduce_tension_ankle_y_angle_adjustment);

  this->si_left_ankle_y_angle.ConnectTo(skill_reduce_tension_ankle_y->si_left_angle);
  this->si_right_ankle_y_angle.ConnectTo(skill_reduce_tension_ankle_y->si_right_angle);
  this->si_left_ankle_y_torque.ConnectTo(skill_reduce_tension_ankle_y->si_left_torque);
  this->si_right_ankle_y_torque.ConnectTo(skill_reduce_tension_ankle_y->si_right_torque);
  ground_contact->out_ground_contact.ConnectTo(skill_reduce_tension_ankle_y->si_ground_contact);

  // hip y
  this->ci_quiet_stand.ConnectTo(skill_reduce_tension_hip_y->ci_quiet_stand);
  this->ci_reset_simulation_experiments.ConnectTo(skill_reduce_tension_hip_y->ci_reset_simulation_experiments);

  skill_stand->activity.ConnectTo(skill_reduce_tension_hip_y->stimulation);

  skill_reduce_tension_hip_y->activity.ConnectTo(this->co_stimulation_reduce_tension_hip_y);
  skill_reduce_tension_hip_y->co_left_angle_adjustment.ConnectTo(this->co_left_reduce_tension_hip_y_angle_adjustment);
  skill_reduce_tension_hip_y->co_right_angle_adjustment.ConnectTo(this->co_right_reduce_tension_hip_y_angle_adjustment);

  this->si_left_hip_y_angle.ConnectTo(skill_reduce_tension_hip_y->si_left_angle);
  this->si_right_hip_y_angle.ConnectTo(skill_reduce_tension_hip_y->si_right_angle);
  this->si_left_hip_y_torque.ConnectTo(skill_reduce_tension_hip_y->si_left_torque);
  this->si_right_hip_y_torque.ConnectTo(skill_reduce_tension_hip_y->si_right_torque);
  ground_contact->out_ground_contact.ConnectTo(skill_reduce_tension_hip_y->si_ground_contact);
  //END skills

  //BEGIN fusion behaviours
  fusion_forward_velocity_left->activity.ConnectTo(postural_reflex_forward_velocity_left->stimulation);
  fusion_forward_velocity_right->activity.ConnectTo(postural_reflex_forward_velocity_right->stimulation);

  fusion_lateral_velocity_left->activity.ConnectTo(postural_reflex_lateral_velocity_left->stimulation);
  fusion_lateral_velocity_right->activity.ConnectTo(postural_reflex_lateral_velocity_right->stimulation);

  fusion_frontal_velocity_left->activity.ConnectTo(postural_reflex_frontal_velocity_left->stimulation);
  fusion_frontal_velocity_left->activity.ConnectTo(postural_reflex_frontal_velocity_right->stimulation);

  //END fusion behaviours

  //BEGIN other modules

  // ground contact
  this->si_left_ground_contact.ConnectTo(ground_contact->in_left_ground_contact);
  this->si_right_ground_contact.ConnectTo(ground_contact->in_right_ground_contact);
  this->si_left_leg_foot_load.ConnectTo(ground_contact->in_left_foot_load);
  this->si_right_leg_foot_load.ConnectTo(ground_contact->in_right_foot_load);
  this->si_left_force_z.ConnectTo(ground_contact->in_left_force_z);
  this->si_right_force_z.ConnectTo(ground_contact->in_right_force_z);

  ground_contact->out_ground_contact.ConnectTo(this->so_ground_contact);
  ground_contact->out_left_foot_load.ConnectTo(this->so_left_foot_load);
  ground_contact->out_right_foot_load.ConnectTo(this->so_right_foot_load);

  ground_contact->out_ground_contact.ConnectTo(this->co_ground_contact);
  ground_contact->out_left_ground_contact.ConnectTo(this->co_left_ground_contact);
  ground_contact->out_right_ground_contact.ConnectTo(this->co_right_ground_contact);
  ground_contact->out_left_foot_load.ConnectTo(this->co_left_foot_load);
  ground_contact->out_right_foot_load.ConnectTo(this->co_right_foot_load);
  ground_contact->out_left_force_z.ConnectTo(this->co_left_force_z);
  ground_contact->out_right_force_z.ConnectTo(this->co_right_force_z);
  // stability analysis
  // left
  this->si_left_cop_x.ConnectTo(left_stability_analysis->si_cop_x);
  this->si_left_cop_y.ConnectTo(left_stability_analysis->si_cop_y);
  this->si_left_ground_contact.ConnectTo(left_stability_analysis->si_ground_contact);

  com_filter_sense->so_signal[1].ConnectTo(left_stability_analysis->si_xcom_x);
  com_filter_sense->so_signal[5].ConnectTo(left_stability_analysis->si_xcom_y);

  left_stability_analysis->so_stability_x.ConnectTo(this->so_left_stability_x);
  left_stability_analysis->so_stability_y.ConnectTo(this->so_left_stability_y);

  // right
  this->si_right_cop_x.ConnectTo(right_stability_analysis->si_cop_x);
  this->si_right_cop_y.ConnectTo(right_stability_analysis->si_cop_y);
  this->si_right_ground_contact.ConnectTo(right_stability_analysis->si_ground_contact);

  com_filter_sense->so_signal[3].ConnectTo(right_stability_analysis->si_xcom_x);
  com_filter_sense->so_signal[7].ConnectTo(right_stability_analysis->si_xcom_y);

  right_stability_analysis->so_stability_x.ConnectTo(this->so_right_stability_x);
  right_stability_analysis->so_stability_y.ConnectTo(this->so_right_stability_y);

  // fusion target ratings adjust angle
  skill_balance_ankle_pitch->target_rating.ConnectTo(fusion_max_adjust_angle->si_signal[0]);
  skill_relax_knee->target_rating.ConnectTo(fusion_max_adjust_angle->si_signal[1]);
  skill_relax_hip_y->target_rating.ConnectTo(fusion_max_adjust_angle->si_signal[2]);
  skill_relax_spine_x->target_rating.ConnectTo(fusion_max_adjust_angle->si_signal[3]);
  skill_relax_spine_y->target_rating.ConnectTo(fusion_max_adjust_angle->si_signal[4]);
  skill_reduce_tension_ankle_y->target_rating.ConnectTo(fusion_max_adjust_angle->si_signal[5]);
  skill_reduce_tension_hip_y->target_rating.ConnectTo(fusion_max_adjust_angle->si_signal[6]);

  fusion_max_adjust_angle->so_signal[0].ConnectTo(this->so_target_rating_fusion_adjust_angle);

  //END other modules

  //BEGIN sensor propagation
  this->si_left_force_z.ConnectTo(this->so_left_force_z);
  this->si_left_torque_x.ConnectTo(this->so_left_torque_x);
  this->si_left_torque_y.ConnectTo(this->so_left_torque_y);

  this->si_right_force_z.ConnectTo(this->so_right_force_z);
  this->si_right_torque_x.ConnectTo(this->so_right_torque_x);
  this->si_right_torque_y.ConnectTo(this->so_right_torque_y);

  this->si_left_inner_heel_force.ConnectTo(this->so_left_inner_heel_force);
  this->si_left_inner_toe_force.ConnectTo(this->so_left_inner_toe_force);
  this->si_right_inner_heel_force.ConnectTo(this->so_right_inner_heel_force);
  this->si_right_inner_toe_force.ConnectTo(this->so_right_inner_toe_force);

  this->si_left_outer_heel_force.ConnectTo(this->so_left_outer_heel_force);
  this->si_left_outer_toe_force.ConnectTo(this->so_left_outer_toe_force);
  this->si_right_outer_heel_force.ConnectTo(this->so_right_outer_heel_force);
  this->si_right_outer_toe_force.ConnectTo(this->so_right_outer_toe_force);

  this->si_left_force_deviation.ConnectTo(this->so_left_force_deviation);
  this->si_right_force_deviation.ConnectTo(this->so_right_force_deviation);
}

//----------------------------------------------------------------------
// gSpinalCord destructor
//----------------------------------------------------------------------
gSpinalCord::~gSpinalCord()
{}

//----------------------------------------------------------------------
// End of namespace declaration
//----------------------------------------------------------------------
}
}
}
