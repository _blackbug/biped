//
// You received this file as part of Finroc
// A Framework for intelligent robot control
//
// Copyright (C) Finroc GbR (finroc.org)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//----------------------------------------------------------------------
/*!\file    projects/biped/control/gLowerTrunkGroup.h
 *
 * \author  Jie Zhao
 *
 * \date    2013-12-16
 *
 * \brief Contains gLowerTrunkGroup
 *
 * \b gLowerTrunkGroup
 *
 * Lower trunk group foe the spine and hip joints
 *
 */
//----------------------------------------------------------------------
#ifndef __projects__biped__control__gLowerTrunkGroup_h__
#define __projects__biped__control__gLowerTrunkGroup_h__

#include "plugins/structure/tSenseControlGroup.h"
//----------------------------------------------------------------------
// External includes (system with <>, local with "")
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Internal includes with ""
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Namespace declaration
//----------------------------------------------------------------------
namespace finroc
{
namespace biped
{
namespace control
{

//----------------------------------------------------------------------
// Forward declarations / typedefs / enums
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Class declaration
//----------------------------------------------------------------------
//! SHORT_DESCRIPTION
/*!
 * Lower trunk group foe the spine and hip joints
 */
class gLowerTrunkGroup : public structure::tSenseControlGroup
{

//----------------------------------------------------------------------
// Ports (These are the only variables that may be declared public)
//----------------------------------------------------------------------
public:

  tSensorInput<double> si_spine_x_torque;
  tSensorInput<double> si_spine_x_angle;
  tSensorInput<double> si_spine_y_torque;
  tSensorInput<double> si_spine_y_angle;
  tSensorInput<double> si_spine_z_torque;
  tSensorInput<double> si_spine_z_angle;
  tSensorInput<double> si_left_hip_x_torque;
  tSensorInput<double> si_left_hip_x_angle;
  tSensorInput<double> si_left_hip_y_torque;
  tSensorInput<double> si_left_hip_y_angle;
  tSensorInput<double> si_left_hip_z_torque;
  tSensorInput<double> si_left_hip_z_angle;
  tSensorInput<double> si_right_hip_x_torque;
  tSensorInput<double> si_right_hip_x_angle;
  tSensorInput<double> si_right_hip_y_torque;
  tSensorInput<double> si_right_hip_y_angle;
  tSensorInput<double> si_right_hip_z_torque;
  tSensorInput<double> si_right_hip_z_angle;
  tSensorInput<double> si_left_foot_position_x;
  tSensorInput<double> si_left_foot_position_y;
  tSensorInput<double> si_left_foot_position_z;
  tSensorInput<double> si_right_foot_position_x;
  tSensorInput<double> si_right_foot_position_y;
  tSensorInput<double> si_right_foot_position_z;


  tSensorOutput<double> so_spine_x_torque;
  tSensorOutput<double> so_spine_x_angle;
  tSensorOutput<double> so_spine_y_torque;
  tSensorOutput<double> so_spine_y_angle;
  tSensorOutput<double> so_spine_z_torque;
  tSensorOutput<double> so_spine_z_angle;
  tSensorOutput<double> so_left_hip_x_torque;
  tSensorOutput<double> so_left_hip_x_angle;
  tSensorOutput<double> so_left_hip_y_torque;
  tSensorOutput<double> so_left_hip_y_angle;
  tSensorOutput<double> so_left_hip_z_torque;
  tSensorOutput<double> so_left_hip_z_angle;
  tSensorOutput<double> so_right_hip_x_torque;
  tSensorOutput<double> so_right_hip_x_angle;
  tSensorOutput<double> so_right_hip_y_torque;
  tSensorOutput<double> so_right_hip_y_angle;
  tSensorOutput<double> so_right_hip_z_torque;
  tSensorOutput<double> so_right_hip_z_angle;
  tSensorOutput<double> so_left_lock_hip_generate;
  tSensorOutput<double> so_right_lock_hip_generate;

  tControllerInput<double> ci_stimulation_stand;
  tControllerInput<double> ci_stiffness_factor;
  tControllerInput<double> ci_quiet_stand;
  tControllerInput<double> ci_ground_contact;
  tControllerInput<double> ci_left_ground_contact;
  tControllerInput<double> ci_right_ground_contact;
  tControllerInput<double> ci_left_foot_load;
  tControllerInput<double> ci_right_foot_load;
  tControllerInput<double> ci_left_force_z;
  tControllerInput<double> ci_right_force_z;
  tControllerInput<double> ci_sensor_monitor;
  tControllerInput<double> ci_stimulation_relax_hip_y;
  tControllerInput<double> ci_left_relax_hip_y_angle_adjustment;
  tControllerInput<double> ci_right_relax_hip_y_angle_adjustment;
  tControllerInput<double> ci_stimulation_reduce_tension_hip_y;
  tControllerInput<double> ci_left_reduce_tension_hip_y_angle_adjustment;
  tControllerInput<double> ci_right_reduce_tension_hip_y_angle_adjustment;
  tControllerInput<double> ci_stimulation_relax_spine_x;
  tControllerInput<double> ci_relax_spine_x_angle_adjustment;
  tControllerInput<double> ci_stimulation_relax_spine_y;
  tControllerInput<double> ci_relax_spine_y_angle_adjustment;
  tControllerInput<double> ci_linear_velocity_x;
  tControllerInput<double> ci_linear_velocity_y;
  tControllerInput<double> ci_actual_accel_x;
  tControllerInput<double> ci_actual_accel_y;
  tControllerInput<double> ci_actual_accel_z;
  tControllerInput<double> ci_actual_omega_x;
  tControllerInput<double> ci_actual_omega_y;
  tControllerInput<double> ci_actual_omega_z;
  tControllerInput<double> ci_actual_roll;
  tControllerInput<double> ci_actual_pitch;
  tControllerInput<double> ci_actual_yaw;
  tControllerInput<double> ci_adjusted_accel_x;
  tControllerInput<double> ci_adjusted_accel_y;
  tControllerInput<double> ci_adjusted_accel_z;
  tControllerInput<double> ci_adjusted_roll;
  tControllerInput<double> ci_adjusted_pitch;

  tControllerInput<double> ci_com_x;
  tControllerInput<double> ci_com_y;
  tControllerInput<double> ci_xcom_x;
  tControllerInput<double> ci_xcom_y;
  tControllerInput<double> ci_com_error_y;
  tControllerInput<double> ci_inv_com_error_y;
  tControllerInput<double> ci_left_forward_velocity_correction;
  tControllerInput<double> ci_right_forward_velocity_correction;
  tControllerInput<double> ci_left_lateral_velocity_correction;
  tControllerInput<double> ci_right_lateral_velocity_correction;
  tControllerInput<double> ci_left_frontal_velocity_correction;
  tControllerInput<double> ci_right_frontal_velocity_correction;
  tControllerInput<double> ci_left_com_x;
  tControllerInput<double> ci_right_com_x;
  tControllerInput<double> ci_left_com_y;
  tControllerInput<double> ci_right_com_y;
  tControllerInput<double> ci_left_xcom_x;
  tControllerInput<double> ci_right_xcom_x;
  tControllerInput<double> ci_left_xcom_y;
  tControllerInput<double> ci_right_xcom_y;

  tControllerInput<double> ci_left_stimulation_walking_initiation;
  tControllerInput<double> ci_right_stimulation_walking_initiation;
  tControllerInput<double> ci_left_stimulation_walking_lateral_initiation;
  tControllerInput<double> ci_right_stimulation_walking_lateral_initiation;
  tControllerInput<double> ci_push_recovery_stand;
  tControllerInput<double> ci_push_recovery_walking;
  tControllerInput<double> ci_push_detected_walking_left;
  tControllerInput<double> ci_push_detected_walking_right;
  tControllerInput<double> ci_bent_knee;
  tControllerInput<double> ci_time_deceleration;
  tControllerInput<double> ci_time_stop;
  tControllerInput<double> ci_hip_motor_factor_push_recovery;
  tControllerInput<double> ci_activity_walking;
  tControllerInput<double> ci_walking_scale_factor;
  tControllerInput<double> ci_walking_scale_laterally;
  tControllerInput<double> ci_walking_lateral_activity;
  tControllerInput<double> ci_turn_left_signal_to_left;
  tControllerInput<double> ci_turn_left_signal_to_right;
  tControllerInput<double> ci_turn_right_signal_to_left;
  tControllerInput<double> ci_turn_right_signal_to_right;
  tControllerInput<double> ci_forward_velocity_control;

  tControllerInput<double> ci_left_stimulation_walking_phase_1;
  tControllerInput<double> ci_left_stimulation_walking_phase_2;
  tControllerInput<double> ci_left_stimulation_walking_phase_3;
  tControllerInput<double> ci_left_stimulation_walking_phase_4;
  tControllerInput<double> ci_left_stimulation_walking_phase_5;
  tControllerInput<double> ci_left_stimulation_lift_leg;

  tControllerInput<double> ci_right_stimulation_walking_phase_1;
  tControllerInput<double> ci_right_stimulation_walking_phase_2;
  tControllerInput<double> ci_right_stimulation_walking_phase_3;
  tControllerInput<double> ci_right_stimulation_walking_phase_4;
  tControllerInput<double> ci_right_stimulation_walking_phase_5;
  tControllerInput<double> ci_right_stimulation_lift_leg;

  tControllerInput<double> ci_left_stimulation_walking_lateral_phase_1;
  tControllerInput<double> ci_left_stimulation_walking_lateral_phase_2;
  tControllerInput<double> ci_left_stimulation_walking_lateral_phase_3;
  tControllerInput<double> ci_left_stimulation_walking_lateral_phase_4;
  tControllerInput<double> ci_left_stimulation_walking_lateral_phase_5;

  tControllerInput<double> ci_right_stimulation_walking_lateral_phase_1;
  tControllerInput<double> ci_right_stimulation_walking_lateral_phase_2;
  tControllerInput<double> ci_right_stimulation_walking_lateral_phase_3;
  tControllerInput<double> ci_right_stimulation_walking_lateral_phase_4;
  tControllerInput<double> ci_right_stimulation_walking_lateral_phase_5;

  //DEC
  tControllerInput<double> ci_angle_body_space_lateral;
  tControllerInput<double> ci_disturbance_estimation_activity;
  tControllerInput<double> ci_angle_body_space;
  tControllerInput<double> ci_angle_trunk_space_prop;
  tControllerInput<double> ci_angle_trunk_space_dist;
  tControllerInput<double> ci_angle_trunk_space_lateral_prop;
  tControllerInput<double> ci_angle_trunk_space_lateral_dist;
  tControllerInput<double> ci_target_angle_trunk_space;
  tControllerInput<double> ci_target_angle_trunk_space_lateral;

  tControllerInput<double> ci_stimulation_termination;
  tControllerInput<double> ci_termi_last_step;
  tControllerInput<double> ci_last_step;
  tControllerInput<double> ci_last_step_state;
  tControllerInput<double> ci_termi_lock_hip;
  tControllerInput<double> ci_stop_upright_trunk;
  tControllerInput<double> ci_maintain_stable;
  tControllerInput<double> ci_termination_hold_position;
  tControllerInput<double> ci_termination_turn_off_upright;
  tControllerInput<double> ci_s_leg_left;
  tControllerInput<double> ci_s_leg_right;
  tControllerInput<double> ci_for_non_used_fusion_modules;
  tControllerInput<double> ci_stimulation_termination_button;

  // optimization and learning
  tControllerInput<double> ci_hip_swing_action1;
  tControllerInput<double> ci_hip_swing_action2;
  tControllerInput<double> ci_hip_swing_action3;
  tControllerInput<double> ci_hip_swing_action4;
  tControllerInput<bool> ci_reset_simulation_experiments;
  tControllerInput<double> ci_loop_times;

  tControllerInput<bool> ci_obstacle_walking_stimulation;
  tControllerInput<int> ci_obstacle_step;
  tControllerInput<double> ci_obstacle_hip_swing_1;
  tControllerInput<double> ci_obstacle_hip_swing_2;
  tControllerInput<double> ci_obstacle_hip_swing_3;
  tControllerInput<double> ci_obstacle_hip_swing_3_rear;
  tControllerInput<double> ci_obstacle_lock_hip_1;
  tControllerInput<double> ci_obstacle_lock_hip_2;
  tControllerInput<double> ci_obstacle_lock_hip_3;

  tControllerInput<bool> ci_slow_walking_stimulation;
  tControllerInput<bool> ci_slow_walking_on;
  tControllerInput<bool> ci_fast_walking_stimulation;
  tControllerInput<bool> ci_fast_walking_on;
  tControllerInput<double> ci_slow_walking_max_torque;
  tControllerInput<double> ci_slow_walking_ahs_t1;
  tControllerInput<double> ci_slow_walking_ahs_t2;
  tControllerInput<double> ci_slow_walking_lh_par1;
  tControllerInput<double> ci_slow_walking_lh_par2;
  tControllerInput<double> ci_fast_walking_lh_par1;
  tControllerInput<double> ci_fast_walking_lh_par2;

  // ankle hip strategy in push recovery
  tControllerInput<bool> ci_push_started_x;
  tControllerInput<double> ci_left_learning_torque_factor;
  tControllerInput<double> ci_right_learning_torque_factor;

  tControllerOutput<double> co_stimulation_spine_x_torque;
  tControllerOutput<double> co_spine_x_torque;
  tControllerOutput<double> co_stimulation_spine_x_angle;
  tControllerOutput<double> co_spine_x_angle;
  tControllerOutput<double> co_stimulation_spine_y_torque;
  tControllerOutput<double> co_spine_y_torque;
  tControllerOutput<double> co_stimulation_spine_y_angle;
  tControllerOutput<double> co_spine_y_angle;
  tControllerOutput<double> co_stimulation_spine_z_torque;
  tControllerOutput<double> co_spine_z_torque;
  tControllerOutput<double> co_stimulation_spine_z_angle;
  tControllerOutput<double> co_spine_z_angle;

  tControllerOutput<double> co_left_stimulation_hip_x_torque;
  tControllerOutput<double> co_left_hip_x_torque;
  tControllerOutput<double> co_left_stimulation_hip_x_angle;
  tControllerOutput<double> co_left_hip_x_angle;
  tControllerOutput<double> co_left_stimulation_hip_y_torque;
  tControllerOutput<double> co_left_hip_y_torque;
  tControllerOutput<double> co_left_stimulation_hip_y_angle;
  tControllerOutput<double> co_left_hip_y_angle;
  tControllerOutput<double> co_left_stimulation_hip_z_torque;
  tControllerOutput<double> co_left_hip_z_torque;
  tControllerOutput<double> co_left_stimulation_hip_z_angle;
  tControllerOutput<double> co_left_hip_z_angle;

  tControllerOutput<double> co_right_stimulation_hip_x_torque;
  tControllerOutput<double> co_right_hip_x_torque;
  tControllerOutput<double> co_right_stimulation_hip_x_angle;
  tControllerOutput<double> co_right_hip_x_angle;
  tControllerOutput<double> co_right_stimulation_hip_y_torque;
  tControllerOutput<double> co_right_hip_y_torque;
  tControllerOutput<double> co_right_stimulation_hip_y_angle;
  tControllerOutput<double> co_right_hip_y_angle;
  tControllerOutput<double> co_right_stimulation_hip_z_torque;
  tControllerOutput<double> co_right_hip_z_torque;
  tControllerOutput<double> co_right_stimulation_hip_z_angle;
  tControllerOutput<double> co_right_hip_z_angle;

  tControllerOutput<double> co_lock_hip_torque_left;
  tControllerOutput<double> co_lock_hip_torque_right;

//----------------------------------------------------------------------
// Public methods and typedefs
//----------------------------------------------------------------------
public:

  gLowerTrunkGroup(core::tFrameworkElement *parent, const std::string &name = "LowerTrunkGroup",
                   const std::string &structure_config_file = __FILE__".xml");

//----------------------------------------------------------------------
// Private fields and methods
//----------------------------------------------------------------------
private:

  /*! Destructor
   *
   * The destructor of modules is declared private to avoid accidental deletion. Deleting
   * modules is already handled by the framework.
   */
  ~gLowerTrunkGroup();

};

//----------------------------------------------------------------------
// End of namespace declaration
//----------------------------------------------------------------------
}
}
}



#endif
