//
// You received this file as part of Finroc
// A Framework for intelligent robot control
//
// Copyright (C) Finroc GbR (finroc.org)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//----------------------------------------------------------------------
/*!\file    projects/biped/motor_patterns/mbbLegPropelSin.h
 *
 * \author  Jie Zhao
 *
 * \date    2013-09-26
 *
 * \brief Contains mbbLegPropelSin
 *
 * \b mbbLegPropelSin
 *
 *
 * Knee active swing during walking termination!
 *
 */
//----------------------------------------------------------------------
#ifndef __projects__biped__motor_patterns__mbbLegPropelSin_h__
#define __projects__biped__motor_patterns__mbbLegPropelSin_h__

#include "plugins/ib2c/tModule.h"
#include "projects/biped/motor_patterns/mMotorPattern.h"
#include "rrlib/time/time.h"

//----------------------------------------------------------------------
// External includes (system with <>, local with "")
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Internal includes with ""
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Namespace declaration
//----------------------------------------------------------------------
namespace finroc
{
namespace biped
{
namespace motor_patterns
{

//----------------------------------------------------------------------
// Forward declarations / typedefs / enums
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Class declaration
//----------------------------------------------------------------------
//! SHORT_DESCRIPTION
/*!
 *
 * Hip active swing motor pattern for the cyclic walking & walking termination!
 */
class mbbLegPropelSin : public mMotorPattern
{

//----------------------------------------------------------------------
// Ports (These are the only variables that may be declared public)
//----------------------------------------------------------------------
public:

  tParameter<double> par_max_torque_ankle_y;   //Example for a runtime parameter. Replace or delete it!
  tParameter<double> par_max_torque_ankle_x;
  tParameter<double> par_stimulation_knee;
  tParameter<double> par_max_torque_knee;
  tParameter<double> par_correction_factor;
  tParameter<double> par_inner_torque_factor;
  tParameter<double> par_push_recovery_factor;
  tParameter<double> par_first_step_factor;
  tParameter<double> par_push_recovery_walking_factor;
  tParameter<bool> par_learning_on;
  tParameter<double> par_test_impulse_length;

  tInput<double> ci_scale;
  tInput<double> ci_lateral_scale;
  tInput<double> ci_correction;
  tInput<double> ci_com_y_error;
  tInput<double> ci_stimulation_termination;
  tInput<double> ci_turn_left_signal;
  tInput<double> ci_turn_right_signal;
  tInput<double> ci_push_recovery_stand;
  tInput<bool> ci_push_recovery_walking;
  tInput<double> ci_learning_ankle_torque;
  tInput<double> ci_learning_weight_ankle;
  tInput<double> ci_leg_propel_action1;
  tInput<double> ci_leg_propel_action2;
  tInput<double> ci_leg_propel_action3;
  tInput<double> ci_leg_propel_action4;
  tInput<double> ci_loop_times;
  tInput<double> ci_leg_learning_torque_factor;
  tInput<double> ci_slow_walking_lp_t1;
  tInput<double> ci_slow_walking_lp_t2;
  tInput<double> ci_slow_walking_lp_max_torque;
  tInput<bool> ci_slow_walking;
  tInput<bool> ci_fast_walking;

  tInput<double> si_knee_angle;
  tInput<double> si_ankle_angle;
  tInput<double> si_foot_left_position_x;
  tInput<double> si_foot_right_position_x;

  tOutput<double> co_stimulation_knee;
  tOutput<double> co_torque_knee;
  tOutput<double> co_torque_ankle_y;
  tOutput<double> co_stimulation_ankle_x;
  tOutput<double> co_torque_ankle_x;

//  tOutput<double> co_learning_weight_ankle;
//  tOutput<double> co_ankle_angle_scale;

//----------------------------------------------------------------------
// Public methods and typedefs
//----------------------------------------------------------------------
public:

  mbbLegPropelSin(core::tFrameworkElement *parent,
                  const std::string &name = "LegPropel",
                  ib2c::tStimulationMode stimulation_mode = ib2c::tStimulationMode::AUTO,
                  unsigned int number_of_inhibition_ports = 0,
                  mMotorPattern::tCurveMode mode = tCurveMode::eSIGMOID,
                  double max_impulse_length = 0.0,
                  double impulse_max_start = 0.0,
                  double impulse_max_end = 0.0);

//  mbbLegPropelSin(core::tFrameworkElement *parent,
//               const std::string &name = "LegPropelSin",
//               ib2c::tStimulationMode stimulation_mode = ib2c::tStimulationMode::AUTO,
//               unsigned int number_of_inhibition_ports = 0,
//               double max_impulse_length = 0.0,
//               double impulse_mean = 0.0,
//               double impulse_stddev = 0.0);

//----------------------------------------------------------------------
// Private fields and methods
//----------------------------------------------------------------------
private:
  bool active;
  double max_torque_ankle_y;
  double max_torque_ankle_x;
  double stimulation_knee;
  double max_torque_knee;
  double correction_factor;
  double inner_torque_factor;
  double push_recovery_factor;
  double first_step_factor;
  double push_recovery_walking_factor;
  double scale;
  double lateral_scale;
  double turn_to_left;
  double turn_to_right;
  double correction;
  double com_y_error;
  double torque_ankle_y;
  double torque_ankle_x;
  double torque_knee;
  double current_knee_angle;
  double current_ankle_angle;
  double termination_state;
  double push_recovery_stand;
  bool push_recovery_walking;
  double left_foot_position_x;
  double right_foot_position_x;
  double min_foot_position_x;
  double calculated_activity;
  double angle_scale;
  double learning_ankle_torque;
  double learning_weight_ankle;
  bool learning_on;
  double test_impulse_length;
  double leg_propel_action1;
  double leg_propel_action2;
  double leg_propel_action3;
  double leg_propel_action4;
  bool slow_walking;
  bool fast_walking;
  //Here is the right place for your variables. Replace this line by your declarations!

  /*! Destructor
   *
   * The destructor of modules is declared private to avoid accidental deletion. Deleting
   * modules is already handled by the framework.
   */
  ~mbbLegPropelSin();

  virtual void OnParameterChange();   //Might be needed to react to changes in parameters independent from Update() calls. Delete otherwise!

  virtual bool ProcessTransferFunction(double activation);

  virtual ib2c::tActivity CalculateActivity(std::vector<ib2c::tActivity> &derived_activities, double activation) const;

  virtual ib2c::tTargetRating CalculateTargetRating(double activation) const;

};

//----------------------------------------------------------------------
// End of namespace declaration
//----------------------------------------------------------------------
}
}
}



#endif
