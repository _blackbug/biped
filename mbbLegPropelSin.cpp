//
// You received this file as part of Finroc
// A Framework for intelligent robot control
//
// Copyright (C) Finroc GbR (finroc.org)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//----------------------------------------------------------------------
/*!\file    projects/biped/motor_patterns/mbbLegPropelSin.cpp
 *
 * \author  Tobias Luksch
 *
 * \date    2013-09-26
 *
 */
//----------------------------------------------------------------------
#include "projects/biped/motor_patterns/mbbLegPropelSin.h"
#include "rrlib/math/utilities.h"
//----------------------------------------------------------------------
// External includes (system with <>, local with "")
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Internal includes with ""
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Debugging
//----------------------------------------------------------------------
#include <cassert>

//----------------------------------------------------------------------
// Namespace usage
//----------------------------------------------------------------------
using namespace rrlib::math;
//----------------------------------------------------------------------
// Namespace declaration
//----------------------------------------------------------------------
namespace finroc
{
namespace biped
{
namespace motor_patterns
{

//----------------------------------------------------------------------
// Forward declarations / typedefs / enums
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Const values
//----------------------------------------------------------------------
runtime_construction::tStandardCreateModuleAction<mbbLegPropelSin> cCREATE_ACTION_FOR_MBB_LEGPROPELSIN("LegPropelSin");

//----------------------------------------------------------------------
// Implementation
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// mbbLegPropelSin constructor
//----------------------------------------------------------------------
mbbLegPropelSin::mbbLegPropelSin(core::tFrameworkElement *parent, const std::string &name,
                                 ib2c::tStimulationMode stimulation_mode, unsigned int number_of_inhibition_ports,
                                 mMotorPattern::tCurveMode mode,
                                 double max_impulse_length,
                                 double impulse_max_start,
                                 double impulse_max_end) :
  mMotorPattern(parent, name, stimulation_mode, number_of_inhibition_ports, mode, max_impulse_length, impulse_max_start, impulse_max_end),

//  mbbLegPropelSin::mbbLegPropelSin(core::tFrameworkElement *parent, const std::string &name,
//                             ib2c::tStimulationMode stimulation_mode, unsigned int number_of_inhibition_ports,
//                             double max_impulse_length,
//                             double impulse_mean,
//                             double impulse_stddev) :
//    mGaussianMotorPattern(parent, name, stimulation_mode, number_of_inhibition_ports, max_impulse_length, impulse_mean, impulse_stddev),
  active(false),
  max_torque_ankle_y(0.0),
  max_torque_ankle_x(0.0),
  stimulation_knee(0.0),
  max_torque_knee(0.0),
  correction_factor(0.0),
  inner_torque_factor(0.0),
  push_recovery_factor(0.0),
  first_step_factor(0.0),
  push_recovery_walking_factor(0.0),
  scale(0.0),
  lateral_scale(9.9),
  turn_to_left(0.0),
  turn_to_right(0.0),
  correction(0.0),
  com_y_error(0.0),
  torque_ankle_y(0.0),
  torque_ankle_x(0.0),
  torque_knee(0.0),
  current_knee_angle(0.0),
  current_ankle_angle(0.0),
  termination_state(0.0),
  push_recovery_stand(0.0),
  push_recovery_walking(false),
  left_foot_position_x(0.0),
  right_foot_position_x(0.0),
  min_foot_position_x(0.0),
  calculated_activity(0.0),
  angle_scale(0.0),
  learning_ankle_torque(0.0),
  learning_weight_ankle(0.0),
  learning_on(false),
  test_impulse_length(0.0),
  leg_propel_action1(0.0),
  leg_propel_action2(0.0),
  leg_propel_action3(0.0),
  leg_propel_action4(0.0)
{
  this->par_max_torque_ankle_y.Set(0.65);
  this->par_max_torque_ankle_x.Set(0.1);
  this->par_stimulation_knee.Set(0.0);
  this->par_max_torque_knee.Set(0.4);
  this->par_correction_factor.Set(0.8);//0.8
  this->par_inner_torque_factor.Set(0.5);
  this->par_push_recovery_factor.Set(0.1);
  this->par_first_step_factor.Set(1.1);
  this->par_push_recovery_walking_factor.Set(1.1);
  this->par_learning_on.Set(false);
  this->par_test_impulse_length.Set(350);
}

//----------------------------------------------------------------------
// mbbLegPropelSin destructor
//----------------------------------------------------------------------
mbbLegPropelSin::~mbbLegPropelSin()
{}

//----------------------------------------------------------------------
// mbbLegPropelSin OnParameterChange
//----------------------------------------------------------------------
void mbbLegPropelSin::OnParameterChange()
{
  tModule::OnParameterChange();
  this->max_torque_ankle_y = this->par_max_torque_ankle_y.Get();
  this->max_torque_ankle_x = this->par_max_torque_ankle_x.Get();
  this->stimulation_knee = this->par_stimulation_knee.Get();
  this->max_torque_knee = this->par_max_torque_knee.Get();
  this->correction_factor = this->par_correction_factor.Get();
  this->inner_torque_factor = this->par_inner_torque_factor.Get();
  this->push_recovery_factor = this->par_push_recovery_factor.Get();
  this->first_step_factor = this->par_first_step_factor.Get();
  this->push_recovery_walking_factor = this->par_push_recovery_walking_factor.Get();
  this->learning_on = this->par_learning_on.Get();
  this->test_impulse_length = this->par_test_impulse_length.Get();
}

//----------------------------------------------------------------------
// mbbLegPropelSin ProcessTransferFunction
//----------------------------------------------------------------------
bool mbbLegPropelSin::ProcessTransferFunction(double activation)
{
  // inputs
  //Get your special Controller Inputs here
  this->scale = LimitedValue<double>(this->ci_scale.Get(), 0.0, 1.2);
  this->lateral_scale = this->ci_lateral_scale.Get();
  this->turn_to_left = this->ci_turn_left_signal.Get();
  this->turn_to_right = this->ci_turn_right_signal.Get();
  this->correction = LimitedValue(this->ci_correction.Get(), -1.0, 1.0);
  this->com_y_error = fabs(this->ci_com_y_error.Get());
  this->push_recovery_stand = this->ci_push_recovery_stand.Get();
  this->push_recovery_walking = this->ci_push_recovery_walking.Get();
  this->learning_ankle_torque = this->ci_learning_ankle_torque.Get();
  this->learning_weight_ankle = this->ci_learning_weight_ankle.Get();
  this->leg_propel_action1 = this->ci_leg_propel_action1.Get();
  this->leg_propel_action2 = this->ci_leg_propel_action2.Get();
  this->leg_propel_action3 = this->ci_leg_propel_action3.Get();
  this->leg_propel_action4 = this->ci_leg_propel_action4.Get();
  //Get your special Sensor Inputs here
  this->current_knee_angle = this->si_knee_angle.Get();
  this->current_ankle_angle = this->si_ankle_angle.Get();
  this->left_foot_position_x = this->si_foot_left_position_x.Get();
  this->right_foot_position_x = this->si_foot_right_position_x.Get();
  this->min_foot_position_x = fmin(this->left_foot_position_x, this->right_foot_position_x);
  this->slow_walking     = this->ci_slow_walking.Get();
  this->fast_walking     = this->ci_fast_walking.Get();

  // transfer function
  if ((activation > 0.0) && (!this->active))// && (this->ci_loop_times.Get() <= 4.0))
  {

    float angle_scale = (this->current_ankle_angle - 0.4) * 2.5; // 1.0 at angle=0.8, zero at 0.4
    angle_scale = LimitedValue<double>(angle_scale, 0.0, 1.0);

    if (!this->push_recovery_stand)
    {
      this->torque_ankle_y = this->scale * (-this->max_torque_ankle_y - this->correction * this->correction_factor) * angle_scale * cos(fmax(fabs(this->turn_to_left), fabs(this->turn_to_right)));
      if (this->ci_loop_times.Get() == 1.0)
      {
        this->torque_ankle_y *= this->first_step_factor;
      }

      if (this->learning_on)
      {
        this->torque_ankle_y = -this->scale * this->leg_propel_action3 * angle_scale;
      }
      else
      {
        this->torque_ankle_y = -this->scale * this->max_torque_ankle_y * angle_scale;
        //        this->torque_ankle_y = this->scale * (-this->par_max_torque_ankle_y - this->correction * this->par_correction_factor) * angle_scale * cos(fmax(fabs(this->turn_to_left), fabs(this->turn_to_right)));
      }

      if ((this->turn_to_left > 0) || (this->turn_to_right > 0))
      {
//        this->torque_ankle_x = this->scale * sin(1.57 * fmax(this->turn_to_left, this->turn_to_right)) * (this->max_torque_ankle_x - this->correction * this->correction_factor) * cos(1.57 * fmax(this->turn_to_left, this->turn_to_right));
        this->torque_ankle_y = this->scale * (-this->max_torque_ankle_y - this->correction * this->correction_factor) * angle_scale * cos(fmax(fabs(this->turn_to_left), fabs(this->turn_to_right))) * this->inner_torque_factor;
//        this->torque_ankle_x = this->max_torque_ankle_x * cos(fmax(this->turn_to_left, this->turn_to_right));

      } // inner limbs when turning

      else if ((this->turn_to_left < 0) || (this->turn_to_right < 0))
      {
//        this->torque_ankle_x = this->scale * sin(1.57 * fmin(this->turn_to_left, this->turn_to_right)) * (this->max_torque_ankle_x - this->correction * this->correction_factor);
        this->torque_ankle_y = this->scale * (-this->max_torque_ankle_y - this->correction * this->correction_factor) * angle_scale * cos(fmax(fabs(this->turn_to_left), fabs(this->turn_to_right)));
      }//outer limbs when turning



      if (this->lateral_scale != 0)
      {
        this->torque_ankle_x = this->lateral_scale * this->max_torque_ankle_x;
      }
      this->torque_knee = this->scale * this->max_torque_knee * angle_scale;
      // this->torque_ankle_y = -this->par_max_torque_ankle_y;
      // this->torque_knee = this->par_max_torque_knee;
      if (this->push_recovery_walking)
      {
        this->torque_ankle_y *= LimitedValue<double>(this->ci_leg_learning_torque_factor.Get(), 1.0, 1.5); // not to propel too much otherwise it will make the upper trunk lean too forwards.
      }
      // set impulse length (if needed) and start motor pattern
      this->SetPatternLength(this->max_impulse_length, this->impulse_max_start , this->impulse_max_end);
      //this->SetPatternLength(this->max_impulse_length,this->impulse_mean , this->impulse_stddev);
      //this->SetPatternLength(this->par_max_impulse_length);
      this->StartMotorPattern();
    }
    else if (this->push_recovery_stand)
    {
      this->torque_ankle_y = this->scale * (-this->max_torque_ankle_y - this->correction * this->correction_factor) * angle_scale * this->push_recovery_factor;
      this->torque_knee = this->scale * this->max_torque_knee * angle_scale * this->push_recovery_factor;
      this->SetPatternLength(this->max_impulse_length, this->impulse_max_start , this->impulse_max_end);
      //this->SetPatternLength(this->max_impulse_length,this->impulse_mean , this->impulse_stddev);
      this->StartMotorPattern();
    }
    else if (this->slow_walking || this->fast_walking )
    {
    	FINROC_LOG_PRINT(DEBUG_WARNING, "KKLOG: starting LG motor pattern");
    	this->SetPatternLength(this->max_impulse_length, this->ci_slow_walking_lp_t1.Get(), this->ci_slow_walking_lp_t2.Get());
    	this->StartMotorPatternOP(this->ci_slow_walking_lp_t1.Get(), this->ci_slow_walking_lp_t2.Get(), this->max_impulse_length);
    	this->torque_ankle_y = this->scale * (-this->ci_slow_walking_lp_max_torque.Get() - this->correction * this->correction_factor) * angle_scale;
    }
  }
  // remember state
  this->active = (activation > 0.0);

  this->calculated_activity = this->impulse_factor * activation;

  //outputs
  this->GetPatternValue();

  this->co_torque_ankle_y.Publish(this->torque_ankle_y * impulse_factor);
  this->co_stimulation_ankle_x.Publish(this->torque_ankle_x * impulse_factor * 0);
  this->co_torque_ankle_x.Publish(this->torque_ankle_x  * 0);
  this->co_torque_knee.Publish(LimitedValue<double>(1.0 - (this->current_knee_angle * 2.0), 0.0, 1.0) * this->torque_knee * this->impulse_factor);
  this->co_stimulation_knee.Publish(this->calculated_activity * this->stimulation_knee);


  //Use your input and output ports to transfer data through this module.
  //Return whether this transfer was successful or not.

  return true;
}

//----------------------------------------------------------------------
// mbbLegPropelSin CalculateActivity
//----------------------------------------------------------------------
ib2c::tActivity mbbLegPropelSin::CalculateActivity(std::vector<ib2c::tActivity> &derived_activity, double activation) const
{
  //Return a meaningful activity value here. You also want to set the derived activity vector, if you registered derived activities e.g. in the constructor.
  return this->calculated_activity;
}

//----------------------------------------------------------------------
// mbbLegPropelSin CalculateTargetRating
//----------------------------------------------------------------------
ib2c::tTargetRating mbbLegPropelSin::CalculateTargetRating(double activation) const
{
  //Return a meaningful target rating here. Choosing 0.5 just because you have no better idea is not "meaningful"!
// If you do not want to use the activation in this calculation remove its name to rid of the warning.
  return this->calculated_activity;
}

//----------------------------------------------------------------------
// End of namespace declaration
//----------------------------------------------------------------------
}
}
}
