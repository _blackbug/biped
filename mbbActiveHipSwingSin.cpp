//
// You received this file as part of Finroc
// A Framework for intelligent robot control
//
// Copyright (C) Finroc GbR (finroc.org)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//----------------------------------------------------------------------
/*!\file    projects/biped/motor_patterns/mbbActiveHipSwingSin.cpp
 *
 * \author  Jie Zhao
 *
 * \date    2013-09-26
 *
 */
//----------------------------------------------------------------------
#include "projects/biped/motor_patterns/mbbActiveHipSwingSin.h"
#include "plugins/scheduling/tThreadContainerThread.h"

//#include "rrlib/math/utilities.h"
//----------------------------------------------------------------------
// External includes (system with <>, local with "")
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Internal includes with ""
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Debugging
//----------------------------------------------------------------------
#include <cassert>

//----------------------------------------------------------------------
// Namespace usage
//----------------------------------------------------------------------
//using namespace rrlib::math;
//----------------------------------------------------------------------
// Namespace declaration
//----------------------------------------------------------------------
namespace finroc
{
namespace biped
{
namespace motor_patterns
{

static const rrlib::time::tTimestamp application_start = rrlib::time::Now(true);

//----------------------------------------------------------------------
// Forward declarations / typedefs / enums
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Const values
//----------------------------------------------------------------------
runtime_construction::tStandardCreateModuleAction<mbbActiveHipSwingSin> cCREATE_ACTION_FOR_MBB_ACTIVEHIPSWINGSIN("ActiveHipSwingSin");

//----------------------------------------------------------------------
// Implementation
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// mbbActiveHipSwingSin constructor
//----------------------------------------------------------------------
mbbActiveHipSwingSin::mbbActiveHipSwingSin(core::tFrameworkElement *parent, const std::string &name,
    ib2c::tStimulationMode stimulation_mode, unsigned int number_of_inhibition_ports,
    mMotorPattern::tCurveMode mode,
    double max_impulse_length,
    double impulse_max_start,
    double impulse_max_end,
    double _direction_factor
                                          ) :
  mMotorPattern(parent, name, stimulation_mode, number_of_inhibition_ports, mode, max_impulse_length, impulse_max_start, impulse_max_end),
  active(false),
  max_torque(0.0),
  opposite_hip_factor(0.0),
  initiation_factor(0.0),
  hip_x_torque(0.0),
  hip_z_torque(0.0),
  ground_contact_threshold(0.0),
  last_two_factor(0.0),
  last_step_factor(0.0),
  push_recovery_factor(0.0),
  push_recovery_factor_stop(0.0),
  push_recovery_factor_walking(0.0),
  com_factor_par(0.0),
  xcom_factor(0.0),
  lateral_walking_factor(0.0),
  push_lateral_factor(0.0),
  direction_factor(0.0),
  turn_left_signal(0.0),
  turn_right_signal(0.0),
  torque_hip_y(0.0),
  torque_hip_x(0.0),
  torque_hip_z(0.0),
  scale(0.0),
  lateral_scale(0.0),
  ground_contact(0.0),
  inititation_phase(false),
  push_recovery_stand(0.0),
  push_recovery_walking(false),
  hip_motor_factor_push_recovery(0.0),
  termi_last_step(0.0),
  last_step(0.0),
// start_time(0.0),
  start_time_stamp(rrlib::time::cNO_TIME),
  time_deceleration(0.0),
  time_stop(0.0),
  stimulate_motor_deceleration(0.0),
  stimulate_motor_stop(0.0),
  com_factor(0.0),
  com_x_left(0.0),
  com_x_right(0.0),
  xcom_x_left(0.0),
  xcom_x_right(0.0),
  com_x_diff(0.0),
  xcom_x_diff(0.0),
  forward_velocity_correction(0.0),
  learning_on(false),
  obstacle_walking(false),
  slow_walking(false),
  fast_walking(false),
  fast_hip_factor(0.0),
  hip_swing_action1(0.0),
  hip_swing_action2(0.0),
  hip_swing_action3(0.0),
  hip_swing_action4(0.0),
  turn_signal(0.0)
{
  this->par_max_torque.Set(0.73);
  this->par_opposite_hip_factor.Set(0.4);
  this->par_initiation_factor.Set(1.48);
  this->par_hip_x_torque.Set(0.8);
  this->par_hip_z_torque.Set(0.15);
  this->par_ground_contact_threshold.Set(0.85);
  this->par_last_two_factor.Set(0.97);
  this->par_last_step_factor.Set(0.25);
  this->par_push_recovery_factor.Set(1.4);
  this->par_push_recovery_factor_stop.Set(0.25);
  this->par_push_recovery_factor_walking.Set(1.3);
  this->par_com_factor.Set(0.1);
  this->par_xcom_factor.Set(0.08);
  this->par_lateral_factor.Set(-1.0);
  this->par_push_lateral_factor.Set(0.5);
  this->par_direction_factor.Set(_direction_factor);
  this->par_learning_on.Set(false);
  this->par_obstacle_walking.Set(false);
  this->par_fast_hip_factor.Set(1);
  //this->par_slow_walking.Set(false);
}

//----------------------------------------------------------------------
// mbbActiveHipSwingSin destructor
//----------------------------------------------------------------------
mbbActiveHipSwingSin::~mbbActiveHipSwingSin()
{}

/*
//----------------------------------------------------------------------
// mbbActiveHipSwingSin OnStaticParameterChange
//----------------------------------------------------------------------
void mbbActiveHipSwingSin::OnStaticParameterChange()
{
  tModule::OnStaticParameterChange();

  if (this->static_parameter_1.HasChanged())
  {
    As this static parameter has changed, do something with its value!
  }
}
*/
//----------------------------------------------------------------------
// mbbActiveHipSwingSin OnParameterChange
//----------------------------------------------------------------------
void mbbActiveHipSwingSin::OnParameterChange()
{
  tModule::OnParameterChange();
  this->max_torque = this->par_max_torque.Get();
  this->opposite_hip_factor = this->par_opposite_hip_factor.Get();
  this->initiation_factor = this->par_initiation_factor.Get();
  this->hip_x_torque = this->par_hip_x_torque.Get();
  this->hip_z_torque = this->par_hip_z_torque.Get();
  this->ground_contact_threshold = this->par_ground_contact_threshold.Get();
  this->last_two_factor = this->par_last_two_factor.Get() ;
  this->last_step_factor = this->par_last_step_factor.Get() ;
  this->push_recovery_factor = this->par_push_recovery_factor.Get() ;
  this->push_recovery_factor_stop = this->par_push_recovery_factor_stop.Get() ;
  this->push_recovery_factor_walking = this->par_push_recovery_factor_walking.Get();
  this->com_factor_par = this->par_com_factor.Get() ;
  this->xcom_factor = this->par_xcom_factor.Get() ;
  this->lateral_walking_factor = this->par_lateral_factor.Get();
  this->push_lateral_factor = this->par_push_lateral_factor.Get() ;
  this->direction_factor = this->par_direction_factor.Get() ;
  this->learning_on = this->par_learning_on.Get();
  this->fast_hip_factor = this->par_fast_hip_factor.Get();
//  this->obstacle_walking = this->par_obstacle_walking.Get();
}

//----------------------------------------------------------------------
// mbbActiveHipSwingSin ProcessTransferFunction
//----------------------------------------------------------------------
bool mbbActiveHipSwingSin::ProcessTransferFunction(double activation)
{
  // inputs
  //Get your special Sensor Inputs here
  this->inititation_phase = false;

  if (this->ci_initiation_activity.Get() > 0.0)
  {
    this->inititation_phase = true;
  }
  //Get your special Controller Inputs here
  this->scale = this->ci_scale.Get();
  this->lateral_scale = this->ci_lateral_scale.Get();
  this->ground_contact = this->ci_ground_contact.Get();
  this->last_step = this->ci_last_step.Get();
  this->termi_last_step = this->ci_termination_last_step.Get();
  this->turn_left_signal = this->ci_turn_left_signal.Get();
  this->turn_right_signal = this->ci_turn_right_signal.Get();
  this->push_recovery_stand = this->ci_push_recovery_stand.Get();
  this->push_recovery_walking = this->ci_push_recovery_walking.Get();
  this->push_detected_walking_left = this->ci_push_recovery_walking_left.Get();
  this->push_detected_walking_right = this->ci_push_recovery_walking_right.Get();
  this->time_deceleration = this->ci_time_deceleration.Get();
  this->time_stop = this->ci_time_stop.Get();
  this->motor_deceleration_active = 0.0;
  this->hip_motor_factor_push_recovery = this->ci_hip_motor_factor_push_recovery.Get();
  this->com_x_left = this->ci_com_x_left.Get();
  this->com_x_right = this->ci_com_x_right.Get();
  this->xcom_x_left = this->ci_xcom_x_left.Get();
  this->xcom_x_right = this->ci_xcom_x_right.Get();
  this->com_x_diff = this->com_x_left - this->com_x_right;
  this->xcom_x_diff = this->xcom_x_left - this->xcom_x_right;
  this->com_factor = this->com_factor_par * fabs(this->com_x_diff) + this->xcom_factor * fabs(this->xcom_x_diff);
  this->forward_velocity_correction = this->ci_forward_velocity_correction.Get();
  this->hip_swing_action1 = this->ci_hip_swing_action1.Get();
  this->hip_swing_action2 = this->ci_hip_swing_action2.Get();
  this->hip_swing_action3 = this->ci_hip_swing_action3.Get();
  this->hip_swing_action4 = this->ci_hip_swing_action4.Get();
  this->obstacle_walking = this->ci_obstacle_walking.Get();

  this->slow_walking     = this->ci_slow_walking_stimulation.Get();
  this->fast_walking     = this->ci_fast_walking_stimulation.Get();
  this->max_torque = this->ci_max_torque.Get();

  if (this->ci_turn_left_signal.Get() > 0)
    this->turn_signal = this->ci_turn_left_signal.Get();
  else if (this->ci_turn_right_signal.Get() > 0)
    this->turn_signal = this->ci_turn_right_signal.Get();
  else
    this->turn_signal = 0.0;

  if (this->ci_motor_deceleration.Get() > 0.0)
    this->motor_deceleration_active = this->ci_motor_deceleration.Get();
  this->motor_stop_active = 0.0;
  if (this->ci_motor_stop.Get() > 0.0)
    this->motor_stop_active = this->ci_motor_stop.Get();

  if ((activation > 0.0) && (!this->active))// && (this->ci_loop_times.Get() <= 4.0))
  {

    this->torque_hip_y = this->max_torque * (1.0 + this->com_factor) ;

    if (this->inititation_phase)
    {
      this->torque_hip_y *= this->initiation_factor;
      this->torque_hip_x = this->torque_hip_x * this->initiation_factor * sin(1.57 * this->turn_signal);

//      this->torque_hip_z *= this->initiation_factor * sin(1.57 * this->turn_signal);
    }
    else
    {
      this->torque_hip_x = this->hip_x_torque * sin(1.57 * this->turn_signal);
//      this->torque_hip_z = this->hip_z_torque * sin(1.57 * fabs(this->turn_signal));
    }

    // push recovery in sagittal plane
    if (this->push_recovery_walking)
      this->torque_hip_y *= this->ci_hip_motor_factor_push_recovery.Get();
    // do you transfer function calculations
    // ...

    if( this->fast_walking )
    	this->torque_hip_y *= this->fast_hip_factor;

    // set impulse length and start motor pattern
    if ((!this->obstacle_walking) && (!this->slow_walking))
    {
        this->SetPatternLength(this->max_impulse_length, this->impulse_max_start, this->impulse_max_end);
        this->StartMotorPattern();
    }
    else if (this->obstacle_walking)
      {
    	this->SetPatternLength(this->max_impulse_length, this->ci_obstacle_hip_swing_1.Get(), this->ci_obstacle_hip_swing_2.Get());
        this->StartMotorPattern();
      }
    else if (this->slow_walking || this->fast_walking )
    {
    	this->SetPatternLength(this->max_impulse_length, this->ci_slow_walking_ahs_t1.Get(), this->ci_slow_walking_ahs_t2.Get());
    	this->StartMotorPatternOP(this->ci_slow_walking_ahs_t1.Get(), this->ci_slow_walking_ahs_t2.Get(), this->max_impulse_length);
    }

    //this->SetPatternLength(this->max_impulse_length, this->impulse_mean, this->impulse_stddev);
    this->co_initiation_phase.Publish(this->inititation_phase);

  }
  this->active = (activation > 0.0);


  if (activation == 0)
  {
    this->torque_hip_x = 0.;
    this->torque_hip_y = 0.;
  }

  // remember state

  this->GetPatternValue();

  //outputs

  if (this->termi_last_step)
  {
    this->torque_hip_y *= this->last_two_factor;
  }

  if (this->obstacle_walking)
  {
    if (this->ci_obstacle_step.Get() == 1)
    {
//      FINROC_LOG_PRINT(DEBUG_WARNING, "hip swing 1 front");
      this->torque_hip_y = this->ci_obstacle_hip_swing_3.Get() * (1.0 + this->com_factor);
    }
    else if (this->ci_obstacle_step.Get() == 2)
    {
      if ((this->si_foot_left_position_z.Get() < 0.35) && (this->si_foot_left_position_x.Get() < 5.1)) // NEED MORE
      {
//        FINROC_LOG_PRINT(DEBUG_WARNING, "HIP SWING 0.4 BEFORE");
        this->torque_hip_y = this->ci_obstacle_hip_swing_3_rear.Get() * (1.0 + this->com_factor);
      }
      else
      {
        this->torque_hip_y = this->ci_obstacle_hip_swing_3.Get() * (1.0 + this->com_factor);
//        FINROC_LOG_PRINT(DEBUG_WARNING, "HIP SWING 1 AFTER");
      }
    }
  }

  this->co_stimulate_motor_deceleration.Publish(this->stimulate_motor_deceleration);
  this->co_stimulate_motor_stop.Publish(this->stimulate_motor_stop);
  //this->co_start_time.Publish(start_time);
  this->co_torque_hip_y.Publish(this->torque_hip_y * impulse_factor);
  this->co_torque_hip_y_max.Publish(this->torque_hip_y);
  this->co_torque_opposite_hip_y.Publish(-this->torque_hip_y * this->opposite_hip_factor * impulse_factor);


  if (this->turn_signal > 0.0)
  {
    this->co_torque_hip_x.Publish(-this->torque_hip_x  * 0.1);
//    this->co_torque_opposite_hip_x.Publish(-this->torque_hip_x * this->opposite_hip_factor * impulse_factor*0.1);
    this->co_stimulation_torque_hip_x.Publish(activation * impulse_factor * this->turn_signal);
//    this->co_stimulation_torque_opposite_hip_x.Publish(activation * this->opposite_hip_factor * 0.1 * this->torque_hip_x);
//  this->co_stimulation_torque_hip_z.Publish(activation * this->turn_signal);
//    this->co_stimulation_torque_opposite_hip_z.Publish(activation *impulse_factor); // fabs(this->turn_signal)
//   this->co_stimulation_angle_hip_z.Publish(activation);
//   this->co_angle_hip_z.Publish(0.5 - this->turn_signal);
//   this->co_stimulation_angle_opposite_hip_z.Publish(activation * 0.4);
//   this->co_angle_opposite_hip_z.Publish(0.5 + 0.4 * this->turn_signal);
  }
  else
  {
    this->co_stimulation_torque_opposite_hip_z.Publish(0.0);
    this->co_stimulation_torque_hip_z.Publish(0.0);
    this->co_stimulation_angle_hip_z.Publish(0.0);
    this->co_angle_hip_z.Publish(0.0);
    this->co_stimulation_angle_opposite_hip_z.Publish(0.0);
    this->co_angle_opposite_hip_z.Publish(0.0);
    this->co_torque_hip_x.Publish(0.0);
    this->co_stimulation_torque_hip_x.Publish(0.0);
  }

//  if (this->GetName() == "(MP) Active Hip Swing Left Sinoid")
//       FINROC_LOG_PRINTF(DEBUG, "Active Hip Swing left hip x is %f ", this->torque_hip_x);
  this->co_com_factor.Publish(this->com_factor);


  //Use your input and output ports to transfer data through this module.
  //Return whether this transfer was successful or not.

  return true;
}

//----------------------------------------------------------------------
// mbbActiveHipSwingSin CalculateActivity
//----------------------------------------------------------------------
ib2c::tActivity mbbActiveHipSwingSin::CalculateActivity(std::vector<ib2c::tActivity> &derived_activity, double activation) const
{
  //Return a meaningful activity value here. You also want to set the derived activity vector, if you registered derived activities e.g. in the constructor.
  return this->impulse_factor * activation;
}

//----------------------------------------------------------------------
// mbbActiveHipSwingSin CalculateTargetRating
//----------------------------------------------------------------------
ib2c::tTargetRating mbbActiveHipSwingSin::CalculateTargetRating(double activation) const
{
  //Return a meaningful target rating here. Choosing 0.5 just because you have no better idea is not "meaningful"!
// If you do not want to use the activation in this calculation remove its name to rid of the warning.
  return activation;
}

//----------------------------------------------------------------------
// End of namespace declaration
//----------------------------------------------------------------------
}
}
}
