# README #

#Sample code from the master thesis work (c++ based)
======================================================

* Core implementation related to the slow walking of the biped robot is in "mbbOptimizationSlowWalking" class.

* mbbActiveHipSwingSin, mbbActiveArmSwing, mbbActiveArmSwing, mbbLockKnee, mbbLateralBalanceAnkle classes are modified to implement the biologically inspired control strategies to let the Biped walk with variable speed.

* Other files are related to the hierarchial design changes needed to implement the variable speed walking on the Biped robot.

