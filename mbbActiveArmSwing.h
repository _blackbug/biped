//
// You received this file as part of Finroc
// A Framework for intelligent robot control
//
// Copyright (C) Finroc GbR (finroc.org)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//----------------------------------------------------------------------
/*!\file    projects/biped/motor_patterns/mbbActiveArmSwing.h
 *
 * \author  Tobias Luksch
 * \author  Jie zhao
 *
 * \date    2013-09-26
 *
 * \brief Contains mbbActiveArmSwing
 *
 * \b mbbActiveArmSwing
 *
 *
 * The motor pattern for the arm swinging during cyclic walking!
 *
 */
//----------------------------------------------------------------------
#ifndef __projects__biped__motor_patterns__mbbActiveArmSwing_h__
#define __projects__biped__motor_patterns__mbbActiveArmSwing_h__

#include "plugins/ib2c/tModule.h"
#include "projects/biped/motor_patterns/mMotorPattern.h"
//#include "projects/biped/motor_patterns/mGaussianMotorPattern.h"

//----------------------------------------------------------------------
// External includes (system with <>, local with "")
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Internal includes with ""
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Namespace declaration
//----------------------------------------------------------------------
namespace finroc
{
namespace biped
{
namespace motor_patterns
{

//----------------------------------------------------------------------
// Forward declarations / typedefs / enums
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Class declaration
//----------------------------------------------------------------------
//! SHORT_DESCRIPTION
/*!
 *
 * The motor pattern for the arm swinging during cyclic walking!
 */
class mbbActiveArmSwing : public mMotorPattern
{

//----------------------------------------------------------------------
// Ports (These are the only variables that may be declared public)
//----------------------------------------------------------------------
public:

  tParameter<double> par_shoulder_torque;   //Example for a runtime parameter. Replace or delete it!
  tParameter<double> par_termination_factor;
  tParameter<double> par_push_recovery_factor;
  tParameter<double> par_fast_walking_factor;

  tInput<double> ci_scale;   //Example for input ports. Replace or delete them!
  tInput<double> ci_last_step_state;
  tInput<double> ci_push_recovery_stand;
  tInput<bool> ci_fast_walking;
  tInput<int> ci_loop_count;

  tOutput<double> co_ipsilateral_shoulder_torque;   //Examples for output ports. Replace or delete them!
  tOutput<double> co_contralateral_shoulder_torque;
  tOutput<bool> co_fast_walking_stimulation;
  tOutput<bool> co_loop_count;

//----------------------------------------------------------------------
// Public methods and typedefs
//----------------------------------------------------------------------
public:

  mbbActiveArmSwing(core::tFrameworkElement *parent,
                    const std::string &name = "ActiveArmSwing",
                    ib2c::tStimulationMode stimulation_mode = ib2c::tStimulationMode::AUTO,
                    unsigned int number_of_inhibition_ports = 0,
                    mMotorPattern::tCurveMode mode = tCurveMode::eSIGMOID,
                    double max_impulse_length = 0.0,
                    double impulse_max_start = 0.0,
                    double impulse_max_end = 0.0);

//  mbbActiveArmSwing(core::tFrameworkElement *parent,
//                    const std::string &name = "ActiveArmSwing",
//                    ib2c::tStimulationMode stimulation_mode = ib2c::tStimulationMode::AUTO,
//                    unsigned int number_of_inhibition_ports = 0,
//                    double max_impulse_length = 0.0,
//                    double impulse_mean = 0.0,
//                    double impulse_stddev = 0.0);

//----------------------------------------------------------------------
// Private fields and methods
//----------------------------------------------------------------------
private:
  bool active;
  double shoulder_torque;
  double termination_factor;
  double push_recovery_factor;
  double last_step_state;
  double scale;
  double push_recovery_stand;
  double fast_walking_factor;
  bool fast_walking;
  int loop_count;
  //Here is the right place for your variables. Replace this line by your declarations!

  /*! Destructor
   *
   * The destructor of modules is declared private to avoid accidental deletion. Deleting
   * modules is already handled by the framework.
   */
  ~mbbActiveArmSwing();

  virtual void OnStaticParameterChange();

  virtual void OnParameterChange();   //Might be needed to react to changes in parameters independent from Update() calls. Delete otherwise!

  virtual bool ProcessTransferFunction(double activation);

  virtual ib2c::tActivity CalculateActivity(std::vector<ib2c::tActivity> &derived_activities, double activation) const;

  virtual ib2c::tTargetRating CalculateTargetRating(double activation) const;

};

//----------------------------------------------------------------------
// End of namespace declaration
//----------------------------------------------------------------------
}
}
}



#endif
