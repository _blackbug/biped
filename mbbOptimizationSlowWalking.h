//
// You received this file as part of Finroc
// A framework for intelligent robot control
//
// Copyright (C) AG Robotersysteme TU Kaiserslautern
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//
//----------------------------------------------------------------------
/*!\file    projects/biped/learning_module/mbbOptimizationSlowWalking.h
 *
 * \author  KARTIKEYA KARNATAK 
 *
 * \date    2016-05-31
 *
 * \brief Contains mbbOptimizationSlowWalking
 *
 * \b mbbOptimizationSlowWalking
 *
 * This module has the PSO implementation to induce stable slow walking capacility to the biped
 *
 */
//----------------------------------------------------------------------
#ifndef __projects__biped__learning_module__mbbOptimizationSlowWalking_h__
#define __projects__biped__learning_module__mbbOptimizationSlowWalking_h__

#include "plugins/ib2c/tModule.h"
#include "rrlib/time/time.h"
#include "projects/biped/learning_module/tPSOOptimization.h"
//----------------------------------------------------------------------
// External includes (system with <>, local with "")
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Internal includes with ""
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Namespace declaration
//----------------------------------------------------------------------
namespace finroc
{
namespace biped
{
namespace learning_module
{

//----------------------------------------------------------------------
// Forward declarations / typedefs / enums
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Class declaration
//----------------------------------------------------------------------
//! SHORT_DESCRIPTION
/*!
 * This module has the PSO implementation to induce stable slow walking capability to the biped
 */
class mbbOptimizationSlowWalking : public ib2c::tModule
{
public:
	tParameter<int> par_num_particle;
	tParameter<int> par_num_dimension;
	tParameter<double> par_velocity_parameter;
	tParameter<double> par_acceleration_constant1;
	tParameter<double> par_acceleration_constant2;


	tParameter<double> par_cost_function1;
	tParameter<double> par_cost_function2;
	tParameter<double> par_cost_function3;
	tParameter<double> par_cost_function4;
	tParameter<double> par_cost_function5;
	tParameter<double> par_ahs_max_torque_out1;
	tParameter<double> par_ahs_t1_out2;
	tParameter<double> par_ahs_t2_out3;
	tParameter<double> par_lh_par1_out4;
	tParameter<double> par_lh_par2_out5;
	tParameter<double> par_lp_torque_out6;
	tParameter<double> par_lp_t1_out7;
	tParameter<double> par_lp_t2_out8;
	tParameter<double> par_lk_angle_out9;
	tParameter<double> par_lk_low_threshold_out10;
	tParameter<double> par_lk_high_threshold_out11;
	tParameter<double> par_lk_knee_velocity_out12;
	tParameter<double> par_lk_hip_angle_low_out13;
	tParameter<double> par_lk_hip_angle_high_out14;
	tParameter<double> par_lk_increment_par1_out15;
	tParameter<double> par_lk_increment_par2_out16;
	tParameter<double> par_max_impulse_out17;
	tParameter<double> par_stimulation_knee_angle_out18;
	tParameter<double> par_stimulation_knee_angle_activity_out19;

	tParameter<bool> par_optimization_on;
	tParameter<bool> par_slow_walking_on;
	tParameter<bool> par_fast_walking_on;

	// SIGNAL INPUT PARAMETERS
	// TODO: Need to add more for different reflexes and motor patterns
	tInput<double> si_left_hip_angle;
	tInput<double> si_right_hip_angle;
	tInput<double> ci_knee_velocity;
	tInput<double> ci_knee_angle;
	tInput<double> ci_walking_velocity;
	tInput<double> ci_walking_distance;
	tInput<double> ci_com_x_body;

	// CONTROL PARAMETERS
	tInput<double> ci_walking_stimulation;
	tInput<double> ci_loop_times;
	tInput<double> ci_pitch_angle;
	tInput<double> ci_roll_angle;
	tInput<double> ci_step_length;
	tInput<double> ci_phase2_left_stimulation;
	tInput<double> ci_phase2_right_stimulation;

//	tInput<double> ci_knee_angle;

	// SIGNAL OUTPUT
	tOutput<double> so_new_running;

	// CONTROL OUTPUT

	tOutput<bool> co_slow_walking_stimulation;
	tOutput<bool> co_fast_walking_stimulation;
	tOutput<bool> co_slow_walking_on;
	tOutput<bool> co_fast_walking_on;
	tOutput<int> co_slow_walking_step;

	// Lock Knee (R)
	tOutput<double> co_output_knee_angle;
	tOutput<double> co_output_lk_knee_angle_low;
	tOutput<double> co_output_lk_knee_angle_high;
	tOutput<double> co_output_lk_knee_velocity;
	tOutput<double> co_output_lk_hip_angle_low;
	tOutput<double> co_output_lk_hip_angle_high;
	tOutput<double> co_output_lk_increment_par1;
	tOutput<double> co_output_lk_increment_par2;

	// Weight acceptance
	tOutput<double> co_output_max_impulse;
	tOutput<double> co_output_stimulation_knee_angle;
	tOutput<double> co_output_stimulation_knee_angle_activity;

	// Lock Hip (R) Same angle for left and right
	tOutput<double> co_output_lh_par1;
	tOutput<double> co_output_lh_par2;

	// Leg Propel (MP)
	tOutput<double> co_output_lp_t1;
	tOutput<double> co_output_lp_t2;
	tOutput<double> co_output_lp_max_torque;

	// Active Hip Swing (MP)
	tOutput<double> co_output_ahs_max_torque;
	tOutput<double> co_output_ahs_t1;
	tOutput<double> co_output_ahs_t2;


	tOutput<double> co_cost;
	tOutput<double> co_cost_function1;
	tOutput<double> co_cost_function2;
	tOutput<double> co_cost_function3;
	tOutput<double> co_cost_function4;
	tOutput<double> co_cost_function5;
	tOutput<double> co_particle;
	tOutput<double> co_walking_step;
	tOutput<double> co_time_span;

	tOutput<double> co_walking_distance;
	tOutput<double> co_walking_velocity;
	tOutput<double> co_loop_times;
	tOutput<double> co_knee_velocity;
	tOutput<double> co_velocity_diff;
	tOutput<double> co_step_length;
	tOutput<double> co_step_difference;

	//----------------------------------------------------------------------
	// Public methods and typedefs
	//----------------------------------------------------------------------
public:

	mbbOptimizationSlowWalking(core::tFrameworkElement *parent, const std::string &name = "OptimizationSlowWalking",
            ib2c::tStimulationMode stimulation_mode = ib2c::tStimulationMode::AUTO, unsigned int number_of_inhibition_ports = 0);


	//----------------------------------------------------------------------
	// Protected methods
	//----------------------------------------------------------------------
protected:

	/*! Destructor
	 *
	 * The destructor of modules is declared protected to avoid accidental deletion. Deleting
	 * modules is already handled by the framework.
	 */
	~mbbOptimizationSlowWalking();

	//----------------------------------------------------------------------
	// Private fields and methods
	//----------------------------------------------------------------------
private:
	static constexpr double ExpectedSlowVelocity = 0.78;
	static constexpr double ExpectedFastVelocity = 1.6;
	static constexpr double ExpectedStepLength   = 0.015;
	void CostFunction(int particle);
	void CalculateSlowWalkingCost();
	void SetReset();

	void SlowWalkingStimulation();
	void CalculateSlowWalkingDistance();

	tPSOOptimization * optimization;
	double **particle_position;
	double **pre_particle_position;
	double **particle_velocity;
	double **pbest_position;
	double **position_limit;

	double *gbest_position;
	double *cost;
	double *cost_pbest;
	double *velocity_limit;

	bool reset;
	bool new_start_flag;
	bool optimization_flag;
	bool update_flag;
	bool slow_walking_stimulation;
	bool fast_walking_stimulation;
	bool initialized;

	int particle;
	int walking_step;
	int slow_walking_step;
	double walking_distance;
	double walking_velocity;
	double step_length;

	double cost_gbest;
	double cost_function1; // distance
	double cost_function2; // delta velocity
	double cost_function3; // delta velocity
	double cost_function4; // delta angle
	double cost_function5; // step length

	rrlib::time::tTimestamp time_current;
	rrlib::time::tTimestamp last_time;
	double time_span;

	std::vector<double> pbest_temp;
	std::vector<double> gbest_temp;
	std::string record_address;

	double walking_simulation;
	//parameters
	int num_particle;
	int num_dimension;

	double cost_function1_par;
	double cost_function2_par;
	double cost_function3_par;
	double cost_function4_par;
	double cost_function5_par;

	bool optimization_on;
	bool slow_walking_on;
	bool fast_walking_on;
	double loop_times;

	// Inputs
	double left_hip_angle;
	double right_hip_angle;
	double knee_angle;
	double knee_velocity;

	// Output
	//  NOTE: Same values for left and right leg both

	double new_running;

	// Lock Hip (R)
	double lh_par1_output;
	double lh_par2_output;

	// Active Hip Swing (MP)
	double ahs_max_torque_output;
	double ahs_t1_output;
	double ahs_t2_output;

	// Leg Propel (MP)
	double lp_t1_output;
	double lp_t2_output;
	double lp_max_torque_output;

	// Lock Knee (R)
	double lock_knee_angle_output;
	double lk_knee_angle_threshold_low;
	double lk_knee_angle_threshold_high;
	double lk_knee_angle_velocity;
	double lk_hip_angle_threshold_low;
	double lk_hip_angle_threshold_high;
	double lk_increment_par1;
	double lk_increment_par2;

	// weight acceptance
	double max_impulse;
	double stimulation_knee_angle;
	double stimulation_knee_angle_activity;

	double last_recorded_velocity;
	double totalVelocityDifference;
	double totalStepLengthDifference;


	// Original parameter values
	double orig_lh_par1;
	double orig_lh_par2;

	// Active Hip Swing (MP)
	double orig_ahs_max_torque;
	double orig_ahs_t1;
	double orig_ahs_t2;

	// Leg Propel (MP)
	double orig_lp_max_torque;
	double orig_lp_t1;
	double orig_lp_t2;

	// Lock Knee (R)
	double orig_lock_knee_angle;
	double orig_lk_knee_angle_threshold_low;
	double orig_lk_knee_angle_threshold_high;
	double orig_lk_knee_angle_velocity;
	double orig_lk_hip_angle_threshold_low;
	double orig_lk_hip_angle_threshold_high;
	double orig_lk_increment_par1;
	double orig_lk_increment_par2;

	// weight acceptance
	double orig_max_impulse;


	double delta_lh_par1;
				  double delta_lh_par2;

				  // Active Hip Swing (MP)
				  double delta_ahs_max_torque;
				  double delta_ahs_t1;
				  double delta_ahs_t2;

				  // Leg Propel (MP)
				  double delta_lp_t1;
				  double delta_lp_t2;
				  double delta_lp_max_torque;

				  // Lock Knee (R)
				  double delta_lock_knee_angle;
				  double delta_lk_knee_angle_threshold_low;
				  double delta_lk_knee_angle_threshold_high;
				  double delta_lk_knee_angle_velocity;
				  double delta_lk_hip_angle_threshold_low;
				  double delta_lk_hip_angle_threshold_high;
				  double delta_lk_increment_par1;
				  double delta_lk_increment_par2;

				  // weight acceptance
				  double delta_max_impulse;

	double phase2_left_stimulation;
	double phase2_right_stimulation;

	virtual void OnParameterChange() override;

	virtual bool ProcessTransferFunction(double activation) override;

	virtual ib2c::tActivity CalculateActivity(std::vector<ib2c::tActivity> &derived_activities, double activation) const override;

	virtual ib2c::tTargetRating CalculateTargetRating(double activation) const override;

};

//----------------------------------------------------------------------
// End of namespace declaration
//----------------------------------------------------------------------
}
}
}



#endif
